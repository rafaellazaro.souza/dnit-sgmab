<div class="authentication">
  
      
            <div class="container col-sm-6 col-md-6 col-lg-3 col-12 fas fa-lock wow fadeIn" data-wow-duration="3s" data-wow-offset="2" data-wow-iteration="2" id="login">
                <form class="card auth_form">
                    <div class="header">
                        <img class="img-responsive" src="<?= base_url("webroot/arquivos/images/barra-dnit.png");?>" alt="">
                        <h5>SGMAB</h5>
                    </div>
                    <div class="body">
                        <div class="input-group mb-3">
                            <input type="email" v-model="dadosLogin.Email" class="form-control" placeholder="Email">
                            <div class="input-group-append">
                                <span class="input-group-text"><a href="#" class="forgot" title="Digite o login"><i class="zmdi zmdi-account-circle"></i></a></span>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password"  v-model="dadosLogin.Senha" class="form-control" placeholder="Senha">
                            <div class="input-group-append">                                
                                <span class="input-group-text"><a href="#" class="forgot" title="Digite a Senha"><i class="zmdi zmdi-lock"></i></a></span>
                            </div>                            
                        </div>
                        <div class="col-12 mt-5 mb-3 text-right" v-if="dadosLogin.Email && dadosLogin.Senha">
                           <label class="btn">Quanto é {{numero1}} {{conta}} {{numero2}} ?</label>
                            <input type="number" @blur="validarCaptcha($event)"  class="w-25" placeholder="resultado">
                                                      
                        </div>

                        <a href="#" class="mt-2 mb-2 wow pulse">Para entrar digite seu e-mail e Senha!</a>
                        <button v-if="dadosLogin.Email && dadosLogin.Senha && mostrarBtnSalvar" type="button"  @click="efetuarLogin" class="btn btn-success  btn-r waves-effect waves-light mr-3 wow pulse">ENTRAR</button>                        
                        <div class="signin_with mt-3">
                            <!-- <p class="mb-0">Dificuldade?</p> -->
                            <!-- <button class="btn btn-primary btn-icon btn-icon-mini btn-round facebook"><i class="zmdi zmdi-facebook"></i></button>
                            <button class="btn btn-primary btn-icon btn-icon-mini btn-round twitter"><i class="zmdi zmdi-twitter"></i></button>
                            <button class="btn btn-primary btn-icon btn-icon-mini btn-round google"><i class="zmdi zmdi-google-plus"></i></button> -->
                        </div>
                    </div>
                </form>
                <div class="copyright text-center">
                    &copy;
                    <script>document.write(new Date().getFullYear())</script>,
                    <span>Desenvolvido por <a href="https://dnit.gov.br" target="_blank">DNIT</a></span>
                </div>
            </div>
</div>

<?php $this->load->view('cgmab/Login/script.php')?>
