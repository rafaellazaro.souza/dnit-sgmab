<script>
    const vmLogin = new Vue({
        el: '#login',
        data: {
            mostrarBtnSalvar: false,
            dadosLogin: {},
            aleatorio: 1,
            numero1: 0,
            numero2: 0,
            conta: '+',
            resposta: 0
        },
        mounted() {
            this.gerarNumeros();



        },
        methods: {

            async efetuarLogin() {
                var resp = await vmGlobal.getFromController('Login/login', this.dadosLogin);
                console.log(resp);
                if (resp.status == false || resp.status == 'false') {
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Erro!',
                        text: resp.result,
                        showConfirmButton: true,
                    });
                    this.gerarNumeros();
                    this.dadosLogin = {}
                } else {
                    //redireciona
                    if (resp.tipoPerfil === 'dnit') {
                        window.location.href = base_url + 'licenciamentoambiental'
                    } else {
                        window.location.href = base_url + 'contratos/listar'
                    }

                }


            },
            gerarNumeros() {
                this.numero1 = Math.floor(Math.random() * 10);
                this.numero2 = Math.floor(Math.random() * 10);
                this.aleatorio = Math.floor((Math.random() * 3) + 1);

                if (this.aleatorio == 1) {
                    this.conta = '*';
                    this.resposta = this.numero1 * this.numero2;
                } else if (this.aleatorio == 2) {
                    this.conta = '+';
                    this.resposta = this.numero1 + this.numero2;
                } else if (this.aleatorio == 3) {
                    this.conta = '-';
                    this.resposta = this.numero1 - this.numero2;
                }
            },
            validarCaptcha(event) {
                if (parseInt(event.target.value) == parseInt(this.resposta)) {
                    this.mostrarBtnSalvar = true;
                } else {

                    // //armazenar erros     
                    // locastorage.setItem('attempts', 1);
                    // if (locastorage.getItem('attmpsts') > 3) {
                    //     this.mostrarBtnSalvar = false;
                    // }
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Erro!',
                        text: 'O resultado não confere! Tente novamente',
                        showConfirmButton: true,

                    });
                    this.mostrarBtnSalvar = false;
                    this.gerarNumeros();
                }
            }
        }
    });
</script>