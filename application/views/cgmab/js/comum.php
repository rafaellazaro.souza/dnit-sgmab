<script>
    var Acoes = {
        abrir: function(id) {
            $(id).removeClass('invisivel');
        },
        fechar: function(id) {
            $(id).addClass('invisivel');
        },

    };

    //textarea
    $('.textarea').jqte();
    //lightbox
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            alwaysShowClose: true
        });
    });
</script>