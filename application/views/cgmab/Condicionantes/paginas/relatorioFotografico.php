<div id="relatorioFotograficoAtividades" class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Relatório Fotográfico de </strong> Atividades </h2>
                    </div>
                    <div style="min-height: 100px;" class="body">
                        <div class="btn-group" role="group" aria-label="Grupo de botões com dropdown aninhado">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-filter"></i> Filtrar Por Atividade
                                </button>
                                <div class="dropdown-menu" style="left: -112px;" aria-labelledby="btnGroupDrop1">
                                    <a class="dropdown-item" href="javascript:;" @click="NomePontoColeta = i" v-for="i in listaPontosColetaFiltro" v-text="i"></a>
                                    <a class="dropdown-item" href="javascript:;"  @click="NomePontoColeta = 'sem-foto'">Sem Fotos</a>
                                </div>
                            </div>
                        </div>
                        <a download="Recursos_hidricos.xlsx" href="http://localhost:8080/sgmab/webroot/uploads/modelos-excel/RecursosHidricos.xlsx" class="float-right btn btn-info"><i class="fas fa-download"></i> Baixar modelo .xlsx</a>
                        <button data-toggle="modal" data-target="#defaultModal" class="float-right btn btn-success mb-3"><i class="fas fa-upload"></i> Importar dados dos pontos de coleta .xlsx</button>
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>Atividade</th>
                                    <th>Nome da Foto</th>
                                    <th>Zona</th>
                                    <th>Cn</th>
                                    <th>CE</th>
                                    <th>Elevação (m)</th>
                                    <th>Data</th>
                                    <th>Obs</th>
                                    <th>-</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="i in listaPontosColeta">
                                    <td v-text="i.NomePontoColeta"></td>
                                    <td v-text="i.Classe1"></td>
                                    <td v-text="i.Classe2"></td>
                                    <td v-text="i.Classe3"></td>
                                    <td v-text="i.Classe4"></td>
                                    <td v-text="i.MassaAgua"></td>
                                    <td v-text="i.TipoMassaAgua"></td>
                                    <td v-text="i.ClasseHidrica"></td>
                                    <td v-text="i.TipoAmostra"></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2><strong>{{totalFotos}}</strong> fotos <i class="fas fa-images"></i></h2>
                    </div>
                    <form enctype="multipart/form-data" method="POST">
                        <div class="body">
                            <input type="file" id="imagemMaterial" @change="qtdFotos" name="fotos[]" class="dropify " multiple>
                        </div>
                    </form>
                    <button class="btn btn-primary float-right" @click="addMateiralOpoioArquivo">IMPORTAR</button>
                </div>
                <div class="row file_manager ">
                    <div v-for="i in listaPontosColeta" class="col-lg-4 col-md-4 col-sm-12">
                        <div class="card " :class="[i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000' ? 'borderRed' : 'borderGreen']">
                            <div class="file">
                                <div class="hover">
                                    <button @click="deletarFotos(i.CodigoConfiguracaoRecursosHidricosFotos)" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                </div>
                                <div class="image">
                                    <img :src="i.Caminho" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
                                </div>
                                <div class="file-name">
                                    <small>Nome do Ponto de Coleta<span class="date text-info"><b>{{i.NomePontoColeta}}</b></span></small>
                                    <small>Nome da Foto<span class="date text-info"><b>{{i.NomeFoto}}</b></span></small>
                                    <small>Tipo de Ponto de Amostra<span class="date text-info"><b>{{i.TipoPontoAmostra}}</b></span></small>
                                    <small v-if="i.Lat != ''">Latitude<span class="date text-info">{{i.Lat}}</span></small>
                                    <small v-else>Latitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                    <small v-if="i.Lng != ''">Longitude<span class="date text-info">{{i.Lng}}</span></small>
                                    <small v-else>Longitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                    <small v-if="i.DataFoto != '1900-01-01 00:00:00.000'">Data da fotografia <span class="date text-info">{{i.DataFotoF}}</span></small>
                                    <small v-else>Data da fotografia<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                </div>
                                <div class="file-name">
                                    <label cla>UTM (Data Sirgas 2000)</label><br>
                                    <div class="row">
                                        <div class=" col-md-3">
                                            <label for="email_address">Zona</label>
                                            <div class="form-group">
                                                <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">
                                                <input v-else class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class=" col-md-3">
                                            <label for="email_address">CN</label>
                                            <div class="form-group  ">
                                                <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">
                                                <input v-else class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class=" col-md-3">
                                            <label for="email_address">CE</label>
                                            <div class="form-group  ">
                                                <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">
                                                <input v-else class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class=" col-md-3">
                                            <label for="email_address  ">Elevação (m)</label>
                                            <div class="form-group ">
                                                <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">
                                                <input v-else class="form-control" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="file-name">
                                    <label for="email_address">Observações</label>
                                    <div class="form-group">
                                        <textarea v-if="i.Lat != '' && i.DataFoto != '1900-01-01 00:00:00.000'" rows="4" class="form-control" placeholder=""></textarea>
                                        <textarea v-else rows="4" disabled class="form-control" placeholder="A foto não está de acordo com o manual"></textarea>
                                    </div>
                                </div>
                                <div class="file-name">
                                    <button v-if="i.Lat != '' && i.DataFoto != '1900-01-01 00:00:00.000'" class="btn btn-success">Salvar</button>
                                    <button @click="deletarFotos(i.CodigoConfiguracaoRecursosHidricosFotos)" v-else class="btn btn-danger">Excluir</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-xl" id="defaultModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="title" id="defaultModalLabel">Importa dados dos Pontos de Coleta
                            <br /><small>Utilizar o modelo excel disponibilizado</small>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <template v-show="mostrarListaExcel === false">
                            <input type="file" id="file" ref="file" name="" class="dropify" multiple>
                        </template>

                        <div class="w-100 h450-r">
                            <table class="table table-striped" v-show="mostrarListaExcel" id="listaPontosExcel">
                                <thead>
                                    <tr>
                                        <th>Nome de Ponto de Coleta</th>
                                        <th>Class 01</th>
                                        <th>Class 02</th>
                                        <th>Class 03</th>
                                        <th>Class 04</th>
                                        <th>Origem da massa d'água</th>
                                        <th>Tipo de massa d'água</th>
                                        <th>Clase Hídrica</th>
                                        <th>Tipo de Amostra</th>
                                        <th>Tipo de Ambiente</th>
                                        <th>UF</th>
                                        <th>Município</th>
                                        <th>Bacia</th>
                                        <th>Km I</th>
                                        <th>Km F</th>
                                        <th>Estaca</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="i in listaExcel">
                                        <td v-text="i.NomePontoColeta"></td>
                                        <td v-text="i.Classe1"></td>
                                        <td v-text="i.Classe2"></td>
                                        <td v-text="i.Classe3"></td>
                                        <td v-text="i.Classe4"></td>
                                        <td v-text="i.MassaAgua"></td>
                                        <td v-text="i.TipoMassaAgua"></td>
                                        <td v-text="i.ClasseHidrica"></td>
                                        <td v-text="i.TipoAmostra"></td>
                                        <td v-text="i.TipoAmbiente"></td>
                                        <td v-text="i.UF"></td>
                                        <td v-text="i.Municipio"></td>
                                        <td v-text="i.Bacia"></td>
                                        <td v-text="i.KmInicial"></td>
                                        <td v-text="i.KmFinal"></td>
                                        <td v-text="i.Estaca"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button @click="importarDadosPontosColeta()" type="button" class="btn btn-default btn-round waves-effect" v-show="mostrarListaExcel === false">Importar</button>
                        <button @click="salvarPontosColeta()" type="button" class="btn btn-default btn-round waves-effect" v-show="mostrarListaExcel">Confirmar e Salvar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>