<style>
    .f-left-quadro {
        width: 75%;
        float: left;
        margin-top: 5px;
        font-size: 11px;
    }

    .bt-red {
        border-top: 3px solid #ff2424 !important;
    }

    .theme-dark .bt-red {
        border-top: 3px solid #ff2424 !important;
    }

    .bt-green {
        border-top: 3px solid #58c136 !important;
    }

    .theme-dark .bt-green {
        border-top: 3px solid #58c136 !important;
    }

    .bt-blue {
        border-top: 3px solid #3641c1 !important;
    }

    .theme-dark .bt-blue {
        border-top: 3px solid #3641c1 !important;
    }

    .bt-gray {
        border-top: 3px solid #a7a7a7 !important;
    }

    .theme-dark .bt-gray {
        border-top: 3px solid #a7a7a7 !important;
    }

    .bt-purple1 {
        border-top: 3px solid #6536c1 !important;
    }

    .theme-dark .bt-purple1 {
        border-top: 3px solid #6536c1 !important;
    }

    .bt-purple2 {
        border-top: 3px solid #bf36c1 !important;
    }

    .theme-dark .bt-purple2 {
        border-top: 3px solid #bf36c1 !important;
    }
</style>
<div id="quadroAtividadesPrograma">
    <!-- <button class="btn btn-outline-secondary btn-sm btn-r"><i class="fas fa-angle-left"></i> Voltar</button> -->
    <div class="row mt-4 p-4">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h4>Quadro de Atividades</h4>
        </div>

    </div>
    <div class="row mt-5">
        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-5  p-3 border-top border-bottom border-right border-left bt-red">
            <h5 class="cl-1">Pendetes</h5>
            <div class="list-group">
                <a href="#" class="list-group-item mb-1 list-group-item-action">
                    <div class="f-left-quadro"> Atividade teste de tamanho texto espaço um pouco maior que o normal</div>
                    <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">Atacar Atividade</button>
                            <button class="dropdown-item" type="button">Relatório Fotográfico</button>
                        </div>
                    </div>
                </a>
            </div>

        </div>


        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-4 p-3 border-top border-bottom border-right bt-gray">
            <h5>Previstas</h5>
            <div class="list-group">
                <a href="#" class="list-group-item mb-1 list-group-item-action">
                    <div class="f-left-quadro"> Atividade 1</div>
                    <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">Atacar Atividade</button>
                            <button class="dropdown-item" type="button">Relatório Fotográfico</button>
                        </div>
                    </div>
                </a>
                <a href="#" class="list-group-item mb-1 list-group-item-action">
                    <div class="f-left-quadro"> Atividade 1</div>
                    <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">Atacar Atividade</button>
                            <button class="dropdown-item" type="button">Relatório Fotográfico</button>
                        </div>
                    </div>
                </a>
                <a href="#" class="list-group-item mb-1 list-group-item-action">
                    <div class="f-left-quadro"> Atividade 1</div>
                    <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">Atacar Atividade</button>
                            <button class="dropdown-item" type="button">Relatório Fotográfico</button>
                        </div>
                    </div>
                </a>
            </div>

        </div>
        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-7 p-3 border-top border-bottom border-right bt-blue">
            <h5>Atacadas</h5>
            <div class="list-group">
                <a href="#" class="list-group-item mb-1 list-group-item-action">
                    <div class="f-left-quadro">Atividade teste de tamanho texto espaço</div>
                    <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">Adicionar p/ realizadas</button>
                            <button class="dropdown-item" type="button">Relatório Fotográfico</button>
                        </div>
                    </div>
                </a>
                <a href="#" class="list-group-item mb-1 list-group-item-action">
                    <div class="f-left-quadro">Atividade 1</div>
                    <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">Adicionar p/ realizadas</button>
                            <button class="dropdown-item" type="button">Relatório Fotográfico</button>
                        </div>
                    </div>
                </a>
                <a href="#" class="list-group-item mb-1 list-group-item-action">
                    <div class="f-left-quadro">Atividade 1</div>
                    <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">Adicionar p/ realizadas</button>
                            <button class="dropdown-item" type="button">Relatório Fotográfico</button>
                        </div>
                    </div>
                </a>
                <a href="#" class="list-group-item mb-1 list-group-item-action">
                    <div class="f-left-quadro">Atividade 1</div>
                    <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">Action</button>
                            <button class="dropdown-item" type="button">Another action</button>
                            <button class="dropdown-item" type="button">Something else here</button>
                        </div>
                    </div>
                </a>
                <a href="#" class="list-group-item mb-1 list-group-item-action">
                    <div class="f-left-quadro">Atividade 1</div>
                    <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">Action</button>
                            <button class="dropdown-item" type="button">Another action</button>
                            <button class="dropdown-item" type="button">Something else here</button>
                        </div>
                    </div>
                </a>
            </div>

        </div>
        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-6 p-3 border-top border-bottom border-right bt-green">
            <h5>Realizadas</h5>
            <div class="list-group">
                <a href="#" class="list-group-item mb-1 list-group-item-action">
                    <div class="f-left-quadro">Atividade 1</div>
                    <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">Action</button>
                            <button class="dropdown-item" type="button">Another action</button>
                            <button class="dropdown-item" type="button">Something else here</button>
                        </div>
                    </div>
                </a>
                <a href="#" class="list-group-item mb-1 list-group-item-action">
                    <div class="f-left-quadro">Atividade 1</div>
                    <div class="dropdown">
                        <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button">Action</button>
                            <button class="dropdown-item" type="button">Another action</button>
                            <button class="dropdown-item" type="button">Something else here</button>
                        </div>
                    </div>
                </a>
            </div>

        </div>
        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-4 p-3 border-top border-bottom border-right bt-purple1">
            <h5>P/ Mês Seg.</h5>

        </div>
        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-4 p-3 border-top border-bottom border-right bt-purple2">
            <h5>Planejadas Mês Seg.</h5>
            <a href="#" class="list-group-item mb-1 list-group-item-action">
                <div class="f-left-quadro">Atividade 1</div>
                <div class="dropdown">
                    <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bars"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <button class="dropdown-item" type="button">Action</button>
                        <button class="dropdown-item" type="button">Another action</button>
                        <button class="dropdown-item" type="button">Something else here</button>
                    </div>
                </div>
            </a>
            <a href="#" class="list-group-item mb-1 list-group-item-action">
                <div class="f-left-quadro">Atividade 1</div>
                <div class="dropdown">
                    <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bars"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <button class="dropdown-item" type="button">Action</button>
                        <button class="dropdown-item" type="button">Another action</button>
                        <button class="dropdown-item" type="button">Something else here</button>
                    </div>
                </div>
            </a>
            <a href="#" class="list-group-item mb-1 list-group-item-action">
                <div class="f-left-quadro">Atividade 1</div>
                <div class="dropdown">
                    <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bars"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <button class="dropdown-item" type="button">Action</button>
                        <button class="dropdown-item" type="button">Another action</button>
                        <button class="dropdown-item" type="button">Something else here</button>
                    </div>
                </div>
            </a>
            <a href="#" class="list-group-item mb-1 list-group-item-action">
                <div class="f-left-quadro">Atividade 1</div>
                <div class="dropdown">
                    <button class="btn btn-outline-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bars"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <button class="dropdown-item" type="button">Action</button>
                        <button class="dropdown-item" type="button">Another action</button>
                        <button class="dropdown-item" type="button">Something else here</button>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>






<div id="RelatorioAgua">
    <!-- <button class="btn btn-outline-secondary btn-sm btn-r"><i class="fas fa-angle-left"></i> Voltar</button> -->
    <div class="row mt-3 p-4">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h4>Relatório Técnico</h4>
        </div>

    </div>
    <div class="row mt-2 border-top">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Relatório</th>
                    <th>Resumo Relatório</th>
                    <th>Anexos</th>
                    <th>Status/ Preenchimento do Relatório</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Relatorio Monitoramento de Água</td>
                    <td class="w-50">
                        Mussum Ipsum, cacilds vidis litro abertis. Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis! Interessantiss
                            quisso pudia ce receita de bolis, mais bolis eu num gostis. Casamentiss faiz malandris se pirulitá. Detraxit consequat
                            et quo num tendi nada.
                    </td>
                    <td>2</td>
                    <td class="w-25">
                        <button class="btn btn-danger">Preencher Relatório do Mês</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>