<script>
    vmListaCondicionantes = new Vue({
        el: '#ListagemCondicionantes',
        data: {
            listaCondicionantes: {},
            listaProgramas: '',
            condicionanteSelecionada: '',
            mostraListaProgramas: false,
            paramsCondicionantes: {
                // StatusCondicionante: 1,
                CodigoLicenca: <?php echo isset($_GET['codigoLicenca']) ? $_GET['codigoLicenca'] : 0  ?>,
            },
            NumeroLicenca: '<?php echo isset($_GET['NumeroLicenca']) ? $_GET['NumeroLicenca'] : 0  ?>',
        },
        async mounted() {
            await this.getCondicionantes();
            await vmGlobal.dadosUsuario();
        },

        methods: {
            async setStatus(event) {
                this.paramsCondicionantes.StatusCondicionante = event.target.value;
                await this.getCondicionantes();
            },
            async setCodigoLicenca(codigo) {
                this.paramsCondicionantes.CodigoLicenca = codigo;
                await this.getCondicionantes();
            },
            async getCondicionantes(getNumeroLicenca = null) {
                $('#tblListaCondicionantes').DataTable().destroy();
                if (getNumeroLicenca) {
                    this.codigoLicenca = getNumeroLicenca
                    vmBusca.codigoLicenca = getNumeroLicenca
                }

                var controller = 'Condicionantes/getCondicionantes'
                $('#tblListaCondicionantes').DataTable().destroy();
                this.listaCondicionantes = await vmGlobal.getFromController(controller, this.paramsCondicionantes);
                vmGlobal.montaDatatable('#tblListaCondicionantes', true);
            },

            async getProgramas(condicionante) {
                this.condicionanteSelecionada = condicionante;
                var params = {
                    table: 'programas',
                    joinMult: [{
                            table: 'ListagemCondicionantes',
                            on: 'CodigoCondicionante'
                        },
                        {
                            table: 'tipos',
                            on: 'CodigoTipo'
                        },
                        {
                            table: 'subtipos',
                            on: 'CodigoSubtipo'
                        },
                        {
                            table: 'temas',
                            on: 'CodigoTema'
                        }
                    ],
                    where: {
                        CodigoCondicionante: this.condicionanteSelecionada
                    }
                }
                this.listaProgramas = await vmGlobal.getFromAPI(params);
                this.mostraListaProgramas = true;
            },
            async aprovarCondicionante(codigo, licenca) {
                await vmGlobal.updateFromAPI({
                    table: 'ListagemCondicionantes',
                    data: {
                        StatusCondicionante: 1
                    },
                    where: {
                        CodigoCondicionante: codigo
                    }
                }, null);
                this.getCondicionantes(licenca);
            },

            async deletar(codigo) {
                const params = {
                    table: 'ListagemCondicionantes',
                    where: {
                        CodigoCondicionante: codigo
                    }
                }

                await vmGlobal.deleteFromAPI(params);
                this.getCondicionantes();
            }
        }
    });

    var vmBusca = new Vue({
        el: '#buscaLicencas',
        data: {
            dadoBusca: '',
            resultadoFiltroNumero: [],
            mostarResultados: false,
            mostrarTemaTipo: false,
            codigoLicenca: <?php echo isset($_GET['codigoLicenca']) ? $_GET['codigoLicenca'] : 0  ?>,
            mostrarFormAdd: false,
            dadosAddCondicionante: {},
            dadosExcel: {},
            linhas: false,
            file: '',
            listaTipos: {},
            listaTemas: {},
            listaSubTipos: {},
            errorsAddCondicionante: 0
        },
        methods: {
            async getSelect(tabela, propriedade) {
                var params = {
                    table: tabela,
                    where: {
                        Status: 1
                    }
                }
                if (propriedade === 'listaTipos') {
                    this.listaTipos = await vmGlobal.getFromAPI(params, 'cgmab');
                }
                if (propriedade === 'listaTemas') {
                    this.listaTemas = await vmGlobal.getFromAPI(params, 'cgmab');
                }

                if (parseInt(this.dadosAddCondicionante.CoditoTipo) != 1) {
                    this.mostrarTemaTipo = true;
                } else {
                    this.mostrarTemaTipo = false;
                }
            },
            async getSubTipos(event) {
                var params = {
                    table: 'subtipos',
                    where: {
                        CodigoTema: event.target.value,
                        Status: 1
                    }
                }

                this.listaSubTipos = await vmGlobal.getFromAPI(params, 'cgmab');
            },

            async getLicencas() {
                var params = {
                    table: 'licencas',
                    joinMult: [{
                            table: 'orgaosExpeditores',
                            on: 'CodigoOrgaoExpeditor'
                        },
                        {
                            table: 'tiposLicencas',
                            on: 'CodigoLicencaTipo'
                        },

                    ],
                    like: {
                        NumeroLicenca: this.dadoBusca
                    }
                }
                this.resultadoFiltroNumero = await vmGlobal.getFromAPI(params, 'cgmab');
                this.mostarResultados = true;
            },
            async salvarCondicionante() {
                if (vmGlobal.dadosDoUsuario.CodigoPerfil === 1 || vmGlobal.dadosDoUsuario.CodigoPerfil === '1') {
                    this.dadosAddCondicionante.StatusCondicionante = 1;
                }
                delete this.dadosAddCondicionante.CodigoTema;
                this.dadosAddCondicionante.CodigoLicenca = vmListaCondicionantes.paramsCondicionantes.CodigoLicenca;

                // verifica se faz update ou insert 
                if (this.dadosAddCondicionante.CodigoCondicionante) { //update
                    const condigoCond = this.dadosAddCondicionante.CodigoCondicionante;
                    delete this.dadosAddCondicionante.CodigoCondicionante;
                    var params = {
                        table: 'condicionantes',
                        data: this.dadosAddCondicionante,
                        where: {
                            CodigoCondicionante: condigoCond
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null, 'cgmab');
                } else { //insert
                    var params = {
                        table: 'condicionantes',
                        data: this.dadosAddCondicionante
                    }
                    await vmGlobal.insertFromAPI(params, null, 'cgmab');
                }



                this.mostrarFormAdd = false;
                this.dadosAddCondicionante = {}
                vmListaCondicionantes.getCondicionantes();

            },
            async importarCondicionantes() {

                //criar sessao com dados da condicionante
                this.armazenarDadosCondicionante();

                //upload do arquivo excel
                this.file = this.$refs.file.files[0];

                let formData = new FormData();
                formData.append('file', this.file);

                await axios.post('<?php echo base_url('condicionantes/uploadCsv') ?>', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        this.dadosExcel = response.data
                    })
                    .catch((error) => {
                        console.log(error);
                    })
                    .finally(() => {
                        this.mostrarFormAdd = false;
                        this.linhas = true;
                    });

            },
            async armazenarDadosCondicionante() {
                await axios.get('<?php echo base_url('condicionantes/armazenaDadosCondicionante?CodigoLicenca=') ?>' + this.codigoLicenca, {
                        params: this.dadosAddCondicionante
                    })
                    .then(function(response) {
                        console.log(response);
                    })
                    .catch(function(error) {
                        console.log(error);
                    })
                    .finally(function() {

                    });
            },
            async gravarCondicionantes() {
                await axios.get('<?php echo base_url('condicionantes/gravarCondicionantes') ?>')
                    .then(function(response) {
                        console.log(response);
                    })
                    .catch(function(error) {
                        console.log(error);
                    })
                    .finally(function() {
                        vmListaCondicionantes.paramsCondicionantes.StatusCondicionante = 0;
                        vmListaCondicionantes.getCondicionantes();
                    });
            },
            checkformAddCondicionantes(e, upload = false) {
                if (this.dadosAddCondicionante.NumeroCondicional && this.dadosAddCondicionante.CodigoTipo && this.dadosAddCondicionante.Titulo && this.dadosAddCondicionante.Prazo && this.dadosAddCondicionante.Recorrencia && this.dadosAddCondicionante.Descricao) {
                    this.salvarCondicionante()
                    return true;
                }

                this.errorsAddCondicionante = [];

                if (!this.dadosAddCondicionante.CodigoTipo) {
                    this.errorsAddCondicionante.push('O Tipo é obrigatório.');
                }
                if (!this.dadosAddCondicionante.NumeroCondicional) {
                    this.errorsAddCondicionante.push('O Numero Condicional é obrigatório.');
                }
                if (!this.dadosAddCondicionante.Titulo) {
                    this.errorsAddCondicionante.push('A Título é obrigatória.');
                }
                if (!this.dadosAddCondicionante.Prazo) {
                    this.errorsAddCondicionante.push('A Prazo é obrigatória.');
                }
                if (!this.dadosAddCondicionante.Recorrencia) {
                    this.errorsAddCondicionante.push('A Recorrencia é obrigatória.');
                }
                if (!this.dadosAddCondicionante.Descricao) {
                    this.errorsAddCondicionante.push('A Descricao é obrigatória.');
                }

                e.preventDefault();
            },

        }
    });
</script>