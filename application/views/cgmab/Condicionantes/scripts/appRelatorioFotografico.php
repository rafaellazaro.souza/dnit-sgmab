<script>
    vmRelatorioFotografico = new Vue({
        el: '#relatorioFotograficoAtividades',
        data: {
            CodigoEscopoPrograma: vmSituacaoExecucao.DadosCondicionante.CodigoEscopoPrograma,
            NomePontoColeta: "",
            listaPontosColeta: {},
            listaPontosColetaFiltro: 0,
            listaExcel: {},
            file: [],
            fotos: {
                lat: '',
                lng: ''
            },
            totalFotos: 0,
            mostrarListaExcel: false

        },
        watch: {
            NomePontoColeta: function(val) {
                this.getPontos();
            }
        },
        async mounted() {
            await this.getPontos();
        },

        methods: {
            async getPontos() {
                let params, where;
                if (this.NomePontoColeta != "") {
                    where = {
                        // CodigoEscopoPrograma: this.CodigoEscopoPrograma,
                        NomePontoColeta: this.NomePontoColeta
                    }
                } else {
                    where = {
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma
                    }
                }
                params = {
                    table: 'pontosColetaAgua',
                    join: {
                        table: 'fotosConfiguracaoRecursosHidricos',
                        on: 'CodigoConfiguracaoRecursosHidricosPontoColeta'
                    },
                    where: where
                }
                //lista pontos
                this.listaPontosColeta = await vmGlobal.getFromAPI(params, 'cgmab');

                //lista para filtro dos pontos
                if (this.listaPontosColetaFiltro === 0) {
                    this.listaPontosColetaFiltro = this.listaPontosColeta.map(x => x.NomePontoColeta).filter(function(elem, index, self) {
                        return index === self.indexOf(elem);
                    });
                }
            },
            async salvarPontosColeta() {
                this.listaExcel.CodigoEscopoPrograma = this.CodigoEscopoPrograma;
                var formData = new FormData();
                formData.append('Dados', JSON.stringify(this.listaExcel));
                await axios.post(base_url + 'ExecucaoProgramas/salvarPontosColeta',formData)
                .then((resp)=>{
                    if(resp.data.status === true){
                        Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: 'Salvo!',
                        text: 'Dados inseridos com sucesso.',
                        showConfirmButton: true,
                    });
                    }else{
                        Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Erro!',
                        text: "Erro ao tentar salvar arquivo. " + e,
                        showConfirmButton: true,

                    });
                    }
                });
                this.mostrarListaExcel = false;
                //this.listaExcel = {};
                $(".dropify-wrapper").removeClass('invisivel');
                $('.modal').hide();

            },
            deletarFotos(codigo) {
                let fd = new FormData();
                fd.append('Codigo', codigo)
                var txt;
                var r = confirm("Deseja deletar a foto?");
                if (r == true) {
                    axios.post('<?= base_url('ConfiguracaoProgramas/deletarFotos') ?>', fd)
                        .then(response => {
                            this.getFotos()
                        }).catch(function(error) {
                            console.log(error)
                        }).finally(() => {

                        })
                }

            },
            qtdFotos() {
                var myfiles = document.getElementById("imagemMaterial");
                this.totalFotos = myfiles.files.length;
            },
            addMateiralOpoioArquivo() {

                var myfiles = document.getElementById("imagemMaterial");
                var files = myfiles.files;
                var data = new FormData();
                for (i = 0; i < files.length; i++) {
                    data.append(i, files[i]);
                }
                $.ajax({
                    url: '<?= base_url('ConfiguracaoProgramas/getGps') ?>',
                    method: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        vm.fotos = data;
                        vm.getFotos()
                        document.getElementById('imagemMaterial').value = null;
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            },
            async importarDadosPontosColeta() {

                //upload do arquivo excel
                this.file = this.$refs.file.files[0];

                let formData = new FormData();
                formData.append('file', this.file);

                await axios.post('<?= base_url('API/uploadXLS') ?>', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        this.listaExcel = response.data
                    })
                    .catch((error) => {
                        console.log(error);
                    })
                    .finally(() => {
                        this.mostrarListaExcel = true;
                        $(".dropify-wrapper").addClass('invisivel');
                    });

            },
        }
    })
    $(function() {
        "use strict";
        $('.dropify').dropify({
            messages: {
                default: "Solte ou clique para anexar arquivo."
            }
        });


    });
</script>