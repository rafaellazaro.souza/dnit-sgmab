<?php $this->load->view('cgmab/Condicionantes/css/css'); ?>
<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Condicionantes</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>><i class=" zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item ">Licenças</li>
                    <li class="breadcrumb-item active">Condicionantes</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                    </div>
                    <div class="body">
                        <div class="row mb-4" id="buscaLicencas">
                            <div class="col-12 col-sm-8">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Filtrar p/ Licença</span>
                                    </div>
                                    <input type="text" class="form-control" v-model="dadoBusca" placeholder="Filtrar pela licença. Digite o número ou ano aqui!s">
                                    <button @click="getLicencas()" class="btn btn-outline-secondary" v-if="dadoBusca.length > 3">Buscar</button>
                                    <div class="lista-licencas sombra-2" v-show="mostarResultados">
                                        <ul class="list-group mb-4 ">
                                            <li class="list-group-item bg-1">Selecione a Licença <i @click="mostarResultados = !mostarResultados"class="fa fa-times btn-r mr-3"></i></li>
                                            <button @click="vmListaCondicionantes.setCodigoLicenca(i.CodigoLicenca), mostarResultados = false, codigoLicenca = i.CodigoLicenca; vmListaCondicionantes.NumeroLicenca = i.NumeroLicenca" type="button" class="list-group-item list-group-item-action" v-for="i in resultadoFiltroNumero">{{i.NumeroLicenca}} - {{i.Sigla}} - {{i.NomeOrgaoExpeditor}}</button>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-2">
                                <select class="form-control show-tick ms select2" @change="vmListaCondicionantes.setStatus($event)">
                                    <option selected>Filtrar por Status</option>
                                    <option value="0">Pendentes</option>
                                    <option value="1">Aprovadas</option>
                                </select>
                            </div>
                            <div class="col-12 col-sm-2">
                                <button @click="mostrarFormAdd = !mostrarFormAdd, getSelect('tipos', 'listaTipos')" v-if="codigoLicenca || vmListaCondicionantes.paramsCondicionantes.CodigoLicenca" class="btn btn-primary"><i class="fa fa-plus"></i> Adicionar Condicionante</button>

                                <!-- 
                                =======================================================    
                                formulario adicionar condicionantes  
                                =======================================================-->
                                <div class="modal-w98-h50 sombra-2 p-5 w-50 sombra-2" v-show="mostrarFormAdd">
    <h4>Adicionar Condicionante para Licença {{vmListaCondicionantes.NumeroLicenca}}</h4>
                                    <div class="alert alert-warning p-3 w-100" v-if="errorsAddCondicionante.length">
                                        <h4>Erro ao salvar. Por favor Preencha os campos abaixo:</h4>
                                        <ul>
                                            <li v-for="i in errorsAddCondicionante">{{i}}</li>
                                        </ul>

                                    </div>
                                    <!--v-show="upload"-->
                                    <template v-if="!dadosAddCondicionante.CodigoCondicionante">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupFileAddon01">Tipo de Condicionante</span>
                                        </div>
                                        <div class="custom-file">
                                            <select v-model="dadosAddCondicionante.CodigoTipo" class="form-control show-tick ms select2" @change="getSelect('temas', 'listaTemas')">
                                                <option value="">Selecione o Tipo</option>
                                                <option v-for="i in listaTipos" :value="i.CodigoTipo">{{i.NomeTipo}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3" v-show="dadosAddCondicionante.CodigoTipo != 1">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupFileAddon01">Tema</span>
                                        </div>
                                        <div class="custom-file">
                                            <select v-model="dadosAddCondicionante.CodigoTema" class="form-control show-tick ms select2" @change="getSubTipos($event)">
                                                <option v-for="i in listaTemas" :value="i.CodigoTema">{{i.NomeTema}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3" v-show="dadosAddCondicionante.CodigoTipo != 1">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupFileAddon01">Sub Tipo</span>
                                        </div>
                                        <div class="custom-file">
                                            <select v-model="dadosAddCondicionante.CodigoSubtipo" class="form-control show-tick ms select2">
                                                <option v-for="i in listaSubTipos" :value="i.CodigoSubtipo">{{i.NomeSubtipo}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    </template>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupFileAddon01">Numero Condicional</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="Number" v-model="dadosAddCondicionante.NumeroCondicional" class="form-control ">
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupFileAddon01">Título</span>
                                        </div>
                                        <div class="custom-file">
                                            <input v-model="dadosAddCondicionante.Titulo" class="form-control ">
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupFileAddon01">Prazo</span>
                                        </div>
                                        <div class="custom-file">
                                            <input v-model="dadosAddCondicionante.Prazo" class="form-control ">
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupFileAddon01">DataEstimada</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="date" v-model="dadosAddCondicionante.DataEstimada" class="form-control ">
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupFileAddon01">Recorrencia</span>
                                        </div>
                                        <div class="custom-file">
                                            <select v-model="dadosAddCondicionante.Recorrencia" class="form-control show-tick ms select2">
                                                <option value="">Selecione</option>
                                                <option value="Evento Unico">Evento Único</option>
                                                <option value="Evento Periodico">Evento Periódico</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupFileAddon01">Descricao</span>
                                        </div>
                                        <div class="custom-file">
                                            <textarea v-model="dadosAddCondicionante.Descricao" class="form-control "></textarea>
                                        </div>
                                    </div>





                                    <button @click="mostrarFormAdd = !mostrarFormAdd; dadosAddCondicionante = {}" class="btn btn-outline-secondary">Cancelar</button>
                                    <button @click="checkformAddCondicionantes()" class="btn btn-outline-secondary">Salvar</button>
                                </div>
                                <!-- 
                                =======================================================    
                                formulario adicionar condicionantes  
                                =======================================================-->

                            </div>
                           
                        </div>

                        <div class="row" id="ListagemCondicionantes">
                            <div class="w-100">
                                <table class="table table-striped" id="tblListaCondicionantes">
                                    <thead>
                                        <tr>
                                            <th>Validação</th>
                                            <th>Número Licenca</th>
                                            <th>Núm. Condicional</th>
                                            <th>Tipo</th>
                                            <th>Tema</th>
                                            <th>SubTipo</th>
                                            <th>Titulo</th>
                                            <th>Descrição</th>
                                            <th>Responsável</th>
                                            <th>Acões</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="i in listaCondicionantes">
                                            <td>
                                                <span class="p-1 mr-2" :class="parseInt(i.StatusCondicionante) == 1  ? 'btn-success' : 'btn-danger'"></span>
                                                <span v-text="parseInt(i.StatusCondicionante) == 1  ? 'Aprovada' : 'Pendente'"></span>
                                            </td>
                                            <td>{{i.NumeroLicenca}}</td>
                                            <td>{{i.NumeroCondicional}}</td>
                                            <td>{{i.NomeTipo}}</td>
                                            <td>{{i.NomeTema}}</td>
                                            <td>{{i.NomeSubtipo}}</td>
                                            <td>{{i.Titulo}}</td>
                                            <td>{{i.Descricao}}</td>
                                            <td>
                                                {{i.Responsavel}}<br />
                                                {{i.EmailResponsavel}}<br />
                                                {{i.TelefoneResponsavel}}<br />
                                            </td>
                                            <td>
                                                <!-- <button v-if="parseInt(i.StatusCondicionante) == 1 && vmGlobal.dadosDoUsuario.CodigoPerfil) == 5" class="btn btn-sm btn-outline-danger"><i class="fa fa-times"></i> Reprovar</button> -->

                                                <button @click="aprovarCondicionante(i.CodigoCondicionante, i.NumeroLicenca)" v-if="parseInt(i.StatusCondicionante) == 0 && parseInt(vmGlobal.dadosDoUsuario.CodigoPerfil) == 1 " class="btn btn-sm btn-outline-success "><i class="fa fa-check"></i> Aprovar</button>

                                                <button @click="aprovarCondicionante(i.CodigoCondicionante, i.NumeroLicenca)" v-if="parseInt(i.StatusCondicionante) == 0 && parseInt(vmGlobal.dadosDoUsuario.CodigoPerfil) == 5 " class="btn btn-sm btn-outline-success "><i class="fa fa-check"></i> Aprovar</button>

                                                
                                                <button v-if="parseInt(i.StatusCondicionante) == 0 || parseInt(vmGlobal.dadosDoUsuario.CodigoPerfil) == 1 || parseInt(vmGlobal.dadosDoUsuario.CodigoPerfil) == 3" @click="vmBusca.mostrarFormAdd = true, vmBusca.getSelect('tipos', 'listaTipos');  
                                                vmBusca.dadosAddCondicionante.CodigoTipo = i.CodigoTipo;
                                                vmBusca.dadosAddCondicionante.NumeroCondicional = i.NumeroCondicional;
                                                vmBusca.dadosAddCondicionante.Titulo = i.Titulo;
                                                vmBusca.dadosAddCondicionante.Prazo = i.Prazo;
                                                vmBusca.dadosAddCondicionante.DataEstimada = i.DataEstimada;
                                                vmBusca.dadosAddCondicionante.Recorrencia = i.Recorrencia;
                                                vmBusca.dadosAddCondicionante.Descricao = i.Descricao;
                                                vmBusca.dadosAddCondicionante.CodigoCondicionante = i.CodigoCondicionante;
                                                " class="btn btn-info btn-sm"><i class="fa fa-pen"></i></button>
                                                
                                                <button @click="deletar(i.CodigoCondicionante)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>


                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
               

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<?php $this->load->view('cgmab/Condicionantes/scripts/app'); ?>