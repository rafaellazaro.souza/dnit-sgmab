<style>
    .lista-licencas {
        width: 306px;
        position: absolute;
        right: 0px;
        z-index: 5;
        top: 39px;
        height: 246px;
        overflow: auto;
        background-color: #fff;
    }

    .upload-condicionantes {
        width: 500px;
        position: absolute;
        right: 224px;
        z-index: 5;
        top: 0px;
        padding: 25px;
        border: 1px solid #828282;
        background-color: #fff;
    }

    .confirma-linhas {
        position: absolute;
        background: #fff;
        z-index: 5;
        border: 1px solid #ffffff;
        right: 10px;
        height: 400px;
        overflow: auto;
        top: 0;
    }

    .bg-1 {
        background-color: #e9ecef;
    }
</style>