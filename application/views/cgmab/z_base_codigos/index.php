<?php $this->load->view('cgmab/Empreendimentos/css/css');?>
<div class="container-fluid">
    <section class="content-header">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Empreendimentos</h1>
            </div>
            <div class="col-sm-6 text-right">
                <a href="<?php echo site_url('cgmab/home') ?>"><i class="fa fa-times"></i></a>
            </div>
            <div class="col-sm-6 text-right">

            </div>

        </div>
    </section>

    <section class="content mb-5">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-list mr-3"></i> Lista de Empreendimentos Cadastrados</h3>

                <div class="card-tools">

                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <!-- botão expandir -->
                    <button type="button" class="btn btn-tool" v-if="vmGlobal.expandSection === ''" @click="vmGlobal.expandirCards('listaLicencas') ">
                        <i class="fas fa-expand"></i></button>
                    <button type="button" class="btn btn-tool" v-else @click="vmGlobal.expandirCards('') ">
                        <i class="fas fa-expand"></i></button>
                    <!-- botão expandir -->

                </div>
            </div>
            <div class="card-body">
                <div class="row" id="ListagemEmpreendimentos">
                    <table class="table table-striped" id="tblListaEmpreendimentos">
                        <thead>
                            <tr>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                            </tr>
                        </tbody>

                    </table>
                </div>




            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>

</div>

<?php $this->load->view('cgmab/Empreendimentos/scripts/app');?>