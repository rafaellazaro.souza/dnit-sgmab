<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }

    .borderRed {
        border: 1px solid red
    }

    .borderGreen {
        border: 1px solid green
    }

    .nav-tabs>.nav-item>.nav-link {
        border: 1px solid #dfdfdf !important;
        border-radius: 0px;
    }

    .nav-tabs>.nav-item>.nav-link.active {
        border: 1px solid #dfdfdf !important;
        border-radius: 0px;
        background-color: #e2e2e2;
        color: #6d6d6d !important;
        font-weight: bold;
    }
</style>

<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Configuração de Recursos Hídricos</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Home</li>
                    <li class="breadcrumb-item active">Contratos</li>
                    <li class="breadcrumb-item active">Dados do Contrato</li>
                    <li class="breadcrumb-item active">Execução de Programas Ambientais - Lista de Condicionantes</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>
    <div id="fotos" class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-md-9">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="qualidade-tab" data-toggle="tab" href="#qualidade-agua" role="tab" aria-controls="passagem-tab" aria-selected=" true">Qualidade da Água</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="parecer-tab" data-toggle="tab" href="#parecer-fiscal" role="tab" aria-controls="parecer-tab" aria-selected="true">Parecer</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 d-flex align-items-center flex-row-reverse">
                                <button class="btn btn-sm btn-success d-flex align-items-center" v-if="!isEmValidacaoOuAprovado && !isFiscal" @click="solicitarValidacao()">Enviar para Validação</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="qualidade-agua" role="tabpanel" aria-labelledby="qualidade-tab">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="">
                                                    <div class="header">
                                                        <h2><strong>Recursos</strong> Hídricos</h2>
                                                    </div>
                                                    <div style="min-height: 100px;" class="body table-responsive">
                                                        <div class="btn-group" role="group" aria-label="Grupo de botões com dropdown aninhado">
                                                            <div class="btn-group" role="group">
                                                                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="fa fa-filter"></i> Filtrar Pontos de Coleta
                                                                </button>
                                                                <div class="dropdown-menu" style="left: -112px;" aria-labelledby="btnGroupDrop1">
                                                                    <a class="dropdown-item" href="javascript:;" @click="NomePontoColeta = i" v-for="i in listaPontosColetaFiltro" v-text="i"></a>
                                                                    <a class="dropdown-item" href="javascript:;" @click="NomePontoColeta = 'sem-foto'">Sem Fotos</a>
                                                                    <a class="dropdown-item" href="javascritp:window.location.reload();" @click="NomePontoColeta = 'sem-foto'">Resetar Filtro</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button v-if="!isEmValidacaoOuAprovado" @click="iniciarParametros(CodigoEscopoPrograma)" data-toggle="modal" v-if="listaPontosColeta.length > 0" data-target="#modalListaParametros" class="btn btn-primary mt-3 mb-3"><i class="fa fa-plus"></i> Adicionar Listas de Parametros</button>
                                                        <a v-if="!isEmValidacaoOuAprovado" download="Recursos_hidricos.xlsx" href="http://localhost:8080/sgmab/webroot/uploads/modelos-excel/RecursosHidricos.xlsx" class="float-right btn btn-info"><i class="fas fa-download"></i> Baixar modelo .xlsx</a>
                                                        <button v-if="!isEmValidacaoOuAprovado" data-toggle="modal" data-target="#defaultModal" class="float-right btn btn-success mb-3"><i class="fas fa-upload"></i> Importar dados dos pontos de coleta .xlsx</button>
                                                        
                                                                                                               
                                                        <table class="table table-bordered table-striped table-hover js-basic-example ">
                                                            <thead>
                                                                <tr>
                                                                    <th>Código Ponto</th>
                                                                    <th>Nome de Ponto de Coleta</th>
                                                                    <th>Class 01</th>
                                                                    <th>Class 02</th>
                                                                    <th>Class 03</th>
                                                                    <th>Class 04</th>
                                                                    <th>Origem da massa d'água</th>
                                                                    <th>Tipo de massa d'água</th>
                                                                    <th>Clase Hídrica</th>
                                                                    <th>Tipo de Amostra</th>
                                                                    <th>Tipo de Ambiente</th>
                                                                    <th>UF</th>
                                                                    <th>Município</th>
                                                                    <th>Bacia</th>
                                                                    <th>Km I</th>
                                                                    <th>Km F</th>
                                                                    <th>Estaca</th>
                                                                    <th>Obs</th>
                                                                    <th>Ações</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr v-for="i in listaPontosColeta">
                                                                    <td>PT{{i.CodigoConfiguracaoRecursosHidricosPontoColeta}}</td>
                                                                    <td v-text="i.NomePontoColeta"></td>
                                                                    <td v-text="i.Classe1"></td>
                                                                    <td v-text="i.Classe2"></td>
                                                                    <td v-text="i.Classe3"></td>
                                                                    <td v-text="i.classe4"></td>
                                                                    <td v-text="i.MassaAgua"></td>
                                                                    <td v-text="i.TipoMassaAgua"></td>
                                                                    <td v-text="i.ClasseHidrica"></td>
                                                                    <td v-text="i.TipoAmostra"></td>
                                                                    <td v-text="i.TipoAmbiente"></td>
                                                                    <td v-text="i.UF"></td>
                                                                    <td v-text="i.Municipio"></td>
                                                                    <td v-text="i.Bacia"></td>
                                                                    <td v-text="i.KmInicial"></td>
                                                                    <td v-text="i.KmFinal"></td>
                                                                    <td v-text="i.Estaca"></td>
                                                                    <td v-text="i.Observacoes"></td>
                                                                    <td>
                                                                        <button @click="deletar(i.CodigoConfiguracaoRecursosHidricosPontoColeta)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>
                                                                    </td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="" v-if="!isEmValidacaoOuAprovado">
                                                    <div class="header">
                                                        <div class="alert alert-info mt-4 mb-4">
                                                            <p><b>Atenção!</b> As fotos devem ser georreferenciadas. Deve-se utilizar os padrões corretos de nomenclatura das fotos para associação adequada entre Código do ponto, o tipo de ponto de coleta e o nome do ponto. Padrão: Código do ponto_ Letra do tipo de ponto de coleta_ nome do ponto. Exemplo: renomear foto para “PT120_J_Pontoteste.jpg”. Qualquer dúvida consulte o manual do usuário</p>
                                                        </div>
                                                        <h2><strong>{{totalFotos}}</strong> fotos dos pontos de coleta <i class="fas fa-images"></i></h2>
                                                    </div>
                                                    <form enctype="multipart/form-data" method="POST">
                                                        <div class="body">
                                                            <input type="file" id="imagemMaterial" name="fotos[]" class="dropify " multiple>
                                                        </div>
                                                    </form>
                                                    <button class="btn btn-primary float-right" @click="addFotos">IMPORTAR</button>
                                                </div>
                                                <div class="row file_manager mt-4" v-show="NomePontoColeta != 'sem-foto'">
                                                    <div v-for="i in listaFotosPontosColeta" class="col-lg-4 col-md-4 col-sm-12">
                                                        <div class=" " :class="[i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000' ? 'borderRed' : 'borderGreen']">
                                                            <div class="file">
                                                                <div class="hover">
                                                                    <button :disabled="isEmValidacaoOuAprovado" @click="deletarFotos(i.CodigoConfiguracaoRecursosHidricosFotos)" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger">
                                                                        <i class="zmdi zmdi-delete"></i>
                                                                    </button>
                                                                </div>
                                                                <div class="image">
                                                                    <img :src="i.Caminho" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
                                                                </div>
                                                                <div class="file-name">
                                                                    <small>Código do Ponto<span class="date text-info"><b>{{i.CodigoConfiguracaoRecursosHidricosPontoColeta}}</b></span></small>
                                                                    <small>Nome do Ponto de Coleta<span class="date text-info"><b>{{i.NomePontoColeta}}</b></span></small>
                                                                    <small>Nome da Foto<span class="date text-info"><b>{{i.NomeFoto}}</b></span></small>
                                                                    <small class="border p-1">Tipo de Ponto de Amostra<span class="date text-info"><b>{{i.TipoPontoAmostra}}</b></span></small>
                                                                    <small v-if="i.Lat != ''">Latitude<span class="date text-info">{{i.Lat}}</span></small>
                                                                    <small v-else>Latitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                                                    <small v-if="i.Lng != ''">Longitude<span class="date text-info">{{i.Lng}}</span></small>
                                                                    <small v-else>Longitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                                                    <small v-if="i.DataFoto != '1900-01-01 00:00:00.000'">Data da fotografia <span class="date text-info">{{i.DataFotoF}}</span></small>
                                                                    <small v-else>Data da fotografia<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                                                </div>
                                                                <div class="file-name">
                                                                    <label cla>UTM (Data Sirgas 2000)</label><br>
                                                                    <div class="row">
                                                                        <div class=" col-md-3">
                                                                            <label for="email_address">Zona</label>
                                                                            <div class="form-group">
                                                                                <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">
                                                                                <input :disabled="isEmValidacaoOuAprovado" v-else @blur="setDadosCard('zona', $event)" class="form-control" placeholder="" :value="i.Zona">
                                                                            </div>
                                                                        </div>
                                                                        <div class=" col-md-3">
                                                                            <label for="email_address">CN</label>
                                                                            <div class="form-group  ">
                                                                                <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">
                                                                                <input :disabled="isEmValidacaoOuAprovado" v-else @blur="setDadosCard('cn', $event)" class="form-control" placeholder="" :value="i.CN">
                                                                            </div>
                                                                        </div>
                                                                        <div class=" col-md-3">
                                                                            <label for="email_address">CE</label>
                                                                            <div class="form-group  ">
                                                                                <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">
                                                                                <input :disabled="isEmValidacaoOuAprovado" v-else @blur="setDadosCard('ce', $event)" class="form-control" placeholder="" :value="i.CE">
                                                                            </div>
                                                                        </div>
                                                                        <div class=" col-md-3">
                                                                            <label for="email_address  ">Elevação (m)</label>
                                                                            <div class="form-group ">
                                                                                <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">
                                                                                <input :disabled="isEmValidacaoOuAprovado" v-else @blur="setDadosCard('elevacao', $event)" class="form-control" placeholder="" :value="i.Elevacao">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="file-name">
                                                                    <label for="email_address">Observações</label>
                                                                    <div class="form-group">
                                                                        <textarea :disabled="isEmValidacaoOuAprovado" @blur="setDadosCard('obs', $event)" v-if="i.Lat != '' && i.DataFoto != '1900-01-01 00:00:00.000'" rows="4" class="form-control" placeholder="">{{i.Observacoes}}</textarea>
                                                                        <textarea v-else rows="4" disabled class="form-control" placeholder="A foto não está de acordo com o manual"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="file-name">
                                                                    <button :disabled="isEmValidacaoOuAprovado" @click="AtualizarFoto(i.CodigoConfiguracaoRecursosHidricosFotos)" v-if="i.Lat != '' && i.DataFoto != '1900-01-01 00:00:00.000'" class="btn btn-success">Salvar</button>
                                                                    <button :disabled="isEmValidacaoOuAprovado" @click="deletarFotos(i.CodigoConfiguracaoRecursosHidricosFotos)" v-else class="btn btn-danger">Excluir</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="parecer-fiscal" role="tabpanel" aria-labelledby="parecer-tab">
                                        <?php $this->load->view('cgmab/ConfiguracaoProgramas/paginas/parecerTab.php') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- modal importar pontos de coleta  -->

        <div class="modal fade bd-example-modal-xl" id="defaultModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="title" id="defaultModalLabel">Importa dados dos Pontos de Coleta
                            <br /><small>Utilizar o modelo excel disponibilizado</small>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <template v-show="mostrarListaExcel === false">
                            <input type="file" id="file" ref="file" name="" class="dropify" multiple>
                        </template>
                        <div class="w-100 h450-r ">
                            <table class="table table-striped" v-show="mostrarListaExcel" id="listaPontosExcel">
                                <thead>
                                    <tr>
                                        <th>Nome de Ponto de Coleta</th>
                                        <th>Class 01</th>
                                        <th>Class 02</th>
                                        <th>Class 03</th>
                                        <th>Class 04</th>
                                        <th>Origem da massa d'água</th>
                                        <th>Tipo de massa d'água</th>
                                        <th>Clase Hídrica</th>
                                        <th>Tipo de Amostra</th>
                                        <th>Tipo de Ambiente</th>
                                        <th>UF</th>
                                        <th>Município</th>
                                        <th>Bacia</th>
                                        <th>Km I</th>
                                        <th>Km F</th>
                                        <th>Estaca</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="i in listaExcel">
                                        <td v-text="i.NomePontoColeta"></td>
                                        <td v-text="i.Classe1"></td>
                                        <td v-text="i.Classe2"></td>
                                        <td v-text="i.Classe3"></td>
                                        <td v-text="i.Classe4"></td>
                                        <td v-text="i.MassaAgua"></td>
                                        <td v-text="i.TipoMassaAgua"></td>
                                        <td v-text="i.ClasseHidrica"></td>
                                        <td v-text="i.TipoAmostra"></td>
                                        <td v-text="i.TipoAmbiente"></td>
                                        <td v-text="i.UF"></td>
                                        <td v-text="i.Municipio"></td>
                                        <td v-text="i.Bacia"></td>
                                        <td v-text="i.KmInicial"></td>
                                        <td v-text="i.KmFinal"></td>
                                        <td v-text="i.Estaca"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button v-if="!isEmValidacaoOuAprovado" @click="importarDadosPontosColeta()" type="button" class="btn btn-default btn-round waves-effect" v-show="mostrarListaExcel === false">Importar</button>
                        <button v-if="!isEmValidacaoOuAprovado" @click="salvarPontosColeta()" type="button" class="btn btn-default btn-round waves-effect" v-show="mostrarListaExcel">Confirmar e Salvar</button>
                    </div>
                </div>
            </div>
        </div> <!-- modal importar pontos de coleta -->
    </div>
</div>


<?php $this->load->view('cgmab/ConfiguracaoProgramas/paginas/parametrosAgua'); ?>
<?php $this->load->view('cgmab/ConfiguracaoProgramas/scripts/appParametros.php') ?>
<?php $this->load->view('cgmab/ConfiguracaoProgramas/scripts/appRecursosHidricos.php') ?>