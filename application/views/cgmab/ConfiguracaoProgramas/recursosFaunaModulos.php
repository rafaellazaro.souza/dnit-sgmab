<div class="body_scroll" id="faunaModulos">

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <!-- Formulario Adicionar/Editar Modulos -->
                    <div class="modal-w98-h50 sombra-2 p-5 w-50" v-show="mostrarAddFaunaModulos">
                        <div class="col-12">
                            <button @click="mostrarAddFaunaModulos = false;dadosFaunaModulos = {}; erros = 0" class="btn btn-secondary btn-r"><i class="fa fa-times"></i></button>
                        </div>
                        <h5 class="w-100 text-left mb-3">Adicionar Modulo</h5>
                        <div class="row mt-3">
                            <div class="alert alert-warning p-3 text-left w-100" v-show="erros.length">
                                <b>Por Favor Preencha o(s) campo(s) abaixo:</b>
                                <ul>
                                    <li v-for="i in erros" v-text="i"></li>
                                </ul>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Nome Modulo Amostragem</span>
                                </div>
                                <input type="text" v-model="dadosFaunaModulos.NomeModuloAmostragem" class="form-control" placeholder="Digite o Nome Modulo Amostragem" require>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Tipo Modulo</span>
                                </div>
                                <input type="text" v-model="dadosFaunaModulos.TipoModulo" class="form-control" placeholder="Digite o Tipo Modulo" require>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Área</span>
                                </div>
                                <input type="number" v-model="dadosFaunaModulos.Area" class="form-control" placeholder="Digite a Área - apenas números" require>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Descrição Fitofisionomia</span>
                                </div>
                                <textarea type="text" v-model="dadosFaunaModulos.DescricaoFitoFisionomia" class="form-control" placeholder="Digite a Fitofisionomia" require></textarea>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Município</span>
                                </div>
                                <input type="text" v-model="dadosFaunaModulos.Municipio" class="form-control" placeholder="Digite o Município" require>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Observações</span>
                                </div>
                                <textarea type="text" v-model="dadosFaunaModulos.Observacoes" class="form-control" placeholder="Digite as Observações" require></textarea>
                            </div>
                            <div class="col-12 text-right">
                                <button class="btn btn-outline-secondary">Cancelar</button>
                                <button type="button" id="btn-salvar-profissional" class="btn btn-secondary ml-2">Salvar</button>
                            </div>
                        </div>
                    </div>
                    <!-- FIM do Formulario Adicionar/Editar Modulos -->
                    <div class="header">
                        <h2><strong>Modulos</strong> Fauna</h2>

                    </div>
                    <div class="body" style="min-height: 100px;">
                        <button @click="mostrarAddFaunaModulos = true" class="btn btn-secondary"><i class="fa fa-plus"></i>
                            Adicionar Modulo</button>

                        <table class="table table-striped" id="listadeModulos">
                            <thead>
                                <th>Nome Modulo</th>
                                <th>Tipo de Modulo </th>
                                <th>Área</th>
                                <th>Descrição Fitofisionomia</th>
                                <th>Município</th>
                                <th>Observações</th>
                                <th>Ação</th>
                            </thead>
                            <tbody>
                                <tr v-for="(i, index) in listaFaunaModulos">
                                    <td v-text="i.NomeModuloAmostragem"></td>
                                    <td v-text="i.TipoModulo"></td>
                                    <td v-text="i.Area"></td>
                                    <td v-text="i.DescricaoFitoFisionomia"></td>
                                    <td v-text="i.Municipio"></td>
                                    <td v-text="i.Observacoes"></td>
                                    <td>
                                        <button @click="deletar(i.CodigoModulo, i.index)" class="btn btn-outline-danger btn-sm"><i class="fa fa-times"></i></button>
                                        <button @click="getProfissional(i.CodigoRecursosHumanos)" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('cgmab/ConfiguracaoProgramas/scripts/appRecursosFaunaModulos') ?>