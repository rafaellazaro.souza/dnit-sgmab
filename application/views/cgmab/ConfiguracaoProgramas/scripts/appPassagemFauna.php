<script>
    var parametrosEscopo = location.search.slice(1);
    let codigoEscopo = parametrosEscopo.split('&')[0].split('99cfb275a87d383fa2912331e3117e48')[1].replace("=", "");
    let codigoContrato = parametrosEscopo.split('&')[1].split('99cfb275a87d383fa2912331e3117e48')[1].replace("=", "");

    // console.log(parametrosEscopo.split('&')[1].split('99cfb275a87d383fa2912331e3117e48')[1].replace("=",""));

    // console.log('99cfb275a87d383fa2912331e3117e48=Vm10ak1XRnJPVmRSYkVwUlZrUkJPUT09&99cfb275a87d383fa2912331e3117e48=Vm10YWEyRnJPVmRSYkVwUlZrUkJPUT09'.split('&')[0].split('99cfb275a87d383fa2912331e3117e48')[1].replace("=",""))



    var vmAppPassagemFauna = new Vue({
        el: '#estruturas',
        data() {
            return {
                mostrarFormAddArmadilhas: false,
                mostrarAddFotos: false,
                mostrarArmadilhas: false,
                mostrarListaEstruturas: true,
                codigoContrato: vmGlobal.codificarDecodificarParametroUrl(codigoContrato, 'decode'),
                codigoEscopo: vmGlobal.codificarDecodificarParametroUrl(codigoEscopo, 'decode'),
                listaAreasApoio: [],
                listaArmadilhas: [],
                listaEstruturaSelecionada: [],
                listaPontosColetaFiltro: {},
                listaPontosColeta: [],
                ListaEquipamentos: [],
                listaEquipamentosSelecionados: [],
                listaEquipamentosInsert: [],
                totalFotos: 0,
                NomePontoColeta: '',
                pontoSelecionado: 0,
                model: {
                    Zona: '',
                    CN: '',
                    CE: '',
                    Elevacao: ''
                },
                status: {
                    CodigoConfiguracaoProgramasStatus: '',
                    CodigoEscopoPrograma: '',
                    Status: 1,
                    DataEnvio: '',
                    PrazoCorrecao: ''
                },
                parecer: {
                    CodigoParecerFiscal: '',
                    CodigoFiscal: '',
                    CodigoEscopoPrograma: '',
                    StatusFiscal: '',
                    Parecer: '',
                    DataEnvio: '',
                    DataParecer: '',
                    AntigaDataCorrecao: null,
                    PrazoCorrecao: ''
                },
                listaParecer: [],
                selectedParecer: {},
                usuario: {
                    NomeUsuario: '',
                    CodigoUsuario: '',
                    Perfil: ''
                },
                showCorrecao: false,
                showParecer: false,
                ParecerFiscal: ''
            }
        },
        async mounted() {
            await this.getEstruturas();
            await this.getStatus();
            await this.getParecer();
        },
        watch: {
            'usuario.Perfil'(val) {
                if (this.habilitarParecer) {

                    this.$nextTick(() => {
                        CKEDITOR.replace('textParecer');

                        CKEDITOR.instances['textParecer'].on("change", (evt) => {
                            this.ParecerFiscal = evt.editor.getData();
                        });
                    })
                }
            },
            'usuario.CodigoUsuario'(val) {
                this.parecer.CodigoFiscal = val;
            },
            ParecerFiscal(val) {
                this.parecer.Parecer = String(btoa(val));
            },
            'status.DataEnvio'(val) {
                if (val) {
                    this.parecer.DataEnvio = moment(val).format('YYYY-MM-DD');
                }
            },
            'status.PrazoCorrecao'(val) {
                if (val) {
                    this.parecer.AntigaDataCorrecao = moment(val).format('YYYY-MM-DD');
                }
            },

        },
        computed: {
            toDayDate() {
                return moment().format('YYYY-MM-DD');
            },
            habilitarParecer() {
                if (this.isFiscal && this.status.Status == 2) {
                    return true;
                } else if (this.isFiscal &&
                    this.status.Status == 5 &&
                    this.contratoAtrazado(this.status.PrazoCorrecao)) {
                    return true;
                }
                return false;
            },
            isFiscal() {
                return this.usuario.Perfil.includes('Fiscal');
            },
            isEmValidacaoOuAprovado() {
                return (this.status.Status == 2 || this.status.Status == 4 || this.isEmAtrazo) && !this.isFiscal;
            },
            isEmAtrazo() {
                return this.contratoAtrazado(this.status.PrazoCorrecao);
            }
        },
        methods: {
            async getEstruturas() {
                let params = {
                    table: 'areasApoio',
                    where: {
                        CodigoContrato: this.codigoContrato
                    }
                }
                this.listaAreasApoio = await vmGlobal.getFromAPI(params);
            },
            async getEquipamentos() {
                let params = {
                    table: 'escopoDeRecursos',
                    join: {
                        table: 'equipamentos',
                        on: 'CodigoEquipamento'
                    },
                    where: {
                        CodigoEscopoPrograma: this.codigoEscopo
                    }
                }
                this.ListaEquipamentos = await vmGlobal.getFromAPI(params);
            },
            async getArmadilhas() {

                await axios.post(base_url + 'ConfiguracaoProgramas/getArmadilhasPassagem', $.param({
                        CodigoAreaApoio: this.listaEstruturaSelecionada[0].CodigoAreaApoio
                    }))
                    .then((resp) => {
                        this.listaArmadilhas = resp.data;
                    })

            },
            iniciarArmadilhas(dados) {
                $("#ListaAreasApoiosSelecionada").DataTable().destroy();
                this.mostrarArmadilhas = true;
                this.mostrarAddFotos = true;
                this.mostrarListaEstruturas = false;
                this.listaEstruturaSelecionada.push(dados);
                this.getArmadilhas();
                this.getEquipamentos();
            },
            fecharArmadilhas() {
                this.mostrarArmadilhas = false;
                this.mostrarAddFotos = false;
                this.mostrarListaEstruturas = true;
                this.listaEstruturaSelecionada = []
            },

            addFotos() {
                var myfiles = document.getElementById("imagemMaterial");

                var files = myfiles.files;
                var data = new FormData();
                for (i = 0; i < files.length; i++) {
                    data.append(i, files[i]);
                }

                data.append('codigoAreaApoio', parseInt(this.listaEstruturaSelecionada[0].CodigoAreaApoio));



                $.ajax({
                    url: '<?= base_url('ConfiguracaoProgramas/getGpsPassagemFauna') ?>',
                    method: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Salvo!',
                            text: 'Fotos salvas com sucesso.',
                            showConfirmButton: true,
                        });
                        document.getElementById('imagemMaterial').value = null;
                    },
                    error: function(error) {
                        console.log(error);
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao tentar salvar arquivo. " + e,
                            showConfirmButton: true,

                        });
                    },
                    complete: function() {
                        vmAppPassagemFauna.getArmadilhas();
                        vmAppPassagemFauna.mostrarFormAddArmadilhas = false;
                        vmAppPassagemFauna.NomePontoColeta = '';
                        vmAppPassagemFauna.totalFotos = 0
                        $('.dropify-clear').click();
                    }
                });
            },

            deletarFotos(codigo, caminho) {
                novoCaminho = caminho.split('webroot');
                let fd = $.param({
                    Codigo: codigo,
                    Caminho: 'webroot' + novoCaminho[1]
                });
                var txt;
                var r = confirm("Deseja deletar a foto?");
                if (r == true) {
                    axios.post('<?= base_url('ConfiguracaoProgramas/deletarFotosPassagem') ?>', fd)
                        .then(response => {
                            this.getArmadilhas();
                        }).catch(function(error) {
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: 'Erro!',
                                text: "Erro ao tentar  ardeletar arquivo. " + e,
                                showConfirmButton: true,

                            });
                        }).finally(() => {

                        })
                }

            },
            setDadosUpdate(campo, event, update = false, codigo = null) {

                if (campo == 'Zona') {
                    this.model.Zona = event.target.value;
                }
                if (campo == 'CN') {
                    this.model.CN = event.target.value;

                }
                if (campo == 'CE') {
                    this.model.CE = event.target.value;

                }
                if (campo == 'Elevacao') {
                    this.model.Elevacao = event.target.value;

                }
                if (campo == 'Observacao') {
                    this.model.Observacao = event.target.value;

                }
                if (campo == 'CodigoRecurso') {
                    this.model.CodigoRecurso = event.target.value;

                }
                if (update == true) {
                    if (this.model.Observacao) {
                        this.updateDados(codigo, true);
                    } else {
                        this.updateDados(codigo);
                    }
                }

            },
            async updateDados(codigo, observacao = false) {
                var params;
                if (observacao == false) {
                    params = {
                        table: 'armadilhasDaPassagem',
                        data: this.model,
                        where: {
                            CodigoConfiguracaoPassagemFaunaArmadilha: codigo
                        }
                    }
                } else {
                    params = {
                        table: 'armadilhasDaPassagem',
                        data: {
                            Observacao: this.model.Observacao
                        },
                        where: {
                            CodigoConfiguracaoPassagemFaunaArmadilha: codigo
                        }
                    }
                }

                await vmGlobal.updateFromAPI(params, null);
                this.getArmadilhas();
                this.model = {
                    Zona: '',
                    CN: '',
                    CE: '',
                    Elevacao: ''
                }

            },
            async salvarEquipamentoPonto(codigoRecurso) {
                let params = {
                    table: 'confPassagemEquipamentos',
                    data: {
                        CodigoConfiguracaoPassagemFaunaArmadilha: this.pontoSelecionado,
                        CodigoRecurso: codigoRecurso
                    },
                }
                await vmGlobal.insertFromAPI(params, null, false, false);
                this.getEquipamentosPonto();

            },
            async deletarEquipamentoPonto(codigo, index) {
                let params = {
                    table: 'confPassagemEquipamentos',
                    where: {
                        CodigoConfPassFaunaEquipamentosUtilizados: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                this.listaEquipamentosSelecionados.splice(index, 1);
            },


            async getStatus() {
                var status = await vmGlobal.getFromAPI({
                    table: 'contratosProgramasStatus',
                    where: {
                        CodigoEscopoPrograma: this.codigoEscopo,
                        // CodigoContrato: this.codigoContrato
                    }
                })

                if (status.length != 0) {
                    this.status = status[0];
                }
            },
            async getParecer() {
                await axios.post(base_url + 'ParecerFiscal/getParecerProgramas', $.param({
                        CodigoEscopoPrograma: this.codigoEscopo,
                        // CodigoContrato: this.codigoContrato
                    }))
                    .then((response) => {
                        this.listaParecer = response.data.parecer;
                        this.usuario = response.data.usuario;

                        this.refreshTable('#tblParecerFiscal');
                    });
            },
            salvarParecer(statusFiscal) {
                var parecer = Object.assign({}, this.parecer);
                delete parecer.CodigoParecerFiscal;
                delete parecer.DataParecer;
                parecer.CodigoEscopoPrograma = this.codigoEscopo;
                // parecer.CodigoContrato = this.codigoContrato;
                parecer.StatusFiscal = statusFiscal;

                if (statusFiscal != 5) {
                    parecer.PrazoCorrecao = null;
                    this.showCorrecao = false;
                } else if (statusFiscal == 5 && parecer.PrazoCorrecao.length == 0) {
                    this.showCorrecao = true;
                    return;
                }

                Swal.fire({
                    title: 'Salvar parecer?',
                    text: "Está ação não pode ser alterada.",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Salvar'
                }).then(async (result) => {
                    if (result.value) {
                        await vmGlobal.insertFromAPI({
                            table: 'parecerProgramas',
                            data: parecer
                        });

                        await vmGlobal.updateFromAPI({
                            table: 'contratosProgramasStatus',
                            data: {
                                Status: statusFiscal,
                                PrazoCorrecao: parecer.PrazoCorrecao
                            },
                            where: {
                                // CodigoContrato: this.codigoContrato,
                                CodigoEscopoPrograma: this.codigoEscopo
                            }
                        });

                        window.location.reload();
                    }
                })
            },
            loadParecer(val) {
                this.showParecer = true;
                this.selectedParecer = Object.assign({}, this.parecer);

                this.selectedParecer.StatusFiscal = val.StatusFiscal;
                this.selectedParecer.NomeUsuario = val.NomeUsuario;
                this.selectedParecer.DataParecer = (val.DataParecer) ? moment(val.DataParecer).format('DD/MM/YYYY') : '';
                this.selectedParecer.DataEnvio = (val.DataEnvio) ? moment(val.DataEnvio).format('DD/MM/YYYY') : '';
                this.selectedParecer.AntigaDataCorrecao = (val.AntigaDataCorrecao) ? moment(val.AntigaDataCorrecao).format('DD/MM/YYYY') : '';
                this.selectedParecer.PrazoCorrecao = (val.PrazoCorrecao) ? moment(val.PrazoCorrecao).format('DD/MM/YYYY') : '';

                if (CKEDITOR.instances['txtParecerTecnicoView']) CKEDITOR.instances['txtParecerTecnicoView'].destroy();
                this.$nextTick(() => {
                    CKEDITOR.replace('txtParecerTecnicoView', {
                        height: '400px',
                        readOnly: true
                    });
                });

                this.$nextTick(() => {
                    CKEDITOR.instances['txtParecerTecnicoView'].setData(String(atob(val.Parecer)));
                });
            },
            constroiTable(id = "", pageLength = 10) {
                $('table' + id, this.$el).DataTable({
                    language: translateDataTable,
                    lengthChange: false,
                    destroy: true,
                    pageLength: pageLength
                });
            },
            async refreshTable(id = "") {
                $('table' + id, this.$el).DataTable().destroy();
                await this.$nextTick(() => {
                    this.constroiTable();
                });
            },
            formataData(val) {
                if (val) {
                    return moment(val).format('DD/MM/YYYY');
                }

                return '';
            },
            contratoAtrazado(val) {
                if (val) {
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var prazoCorrecao = moment(val).toDate();
                    prazoCorrecao.setHours(0, 0, 0, 0);

                    return prazoCorrecao.getTime() < today.getTime();
                }

                return false;
            },
            async solicitarValidacao() {
                if (this.status.CodigoConfiguracaoProgramasStatus.length == 0) {
                    await vmGlobal.insertFromAPI({
                        table: 'contratosProgramasStatus',
                        data: {
                            CodigoEscopoPrograma: this.codigoEscopo,
                            // CodigoContrato: this.codigoContrato,
                            Status: 2,
                            DataEnvio: moment().format('YYYY-MM-DD HH:mm:ss')
                        }
                    });

                    await this.getStatus();

                    return;
                }

                await vmGlobal.updateFromAPI({
                    table: 'contratosProgramasStatus',
                    where: {
                        CodigoEscopoPrograma: this.codigoEscopo,
                        // CodigoContrato: this.codigoContrato,
                    },
                    data: {
                        Status: 2,
                        DataEnvio: moment().format('YYYY-MM-DD HH:mm:ss')
                    }
                });

                await this.getStatus();
            },
            getClasse(val) {
                return (parseInt(val) == 4) ? 'badge-success' : 'btn-rejeitado';
            },
            getStatusDescricao(val) {
                return (parseInt(val) == 4) ? 'Aprovado' : 'Parcialmente Aprovado';
            },
        },
    });
    $(function() {
        "use strict";
        $('.dropify').dropify({
            messages: {
                default: "Solte ou clique para anexar arquivo."
            }
        });


    });
</script>