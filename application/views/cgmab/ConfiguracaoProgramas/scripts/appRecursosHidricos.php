<script>
    var vmCofiguracaoRecHidricos = new Vue({
        el: "#fotos",
        data: {
            CodigoEscopoPrograma: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('=')[1], 'decode'),
            NomePontoColeta: "",
            listaPontosColeta: [],
            listaFotosPontosColeta: [],
            listaPontosColetaFiltro: 0,
            dadosCard: {},
            listaExcel: {},
            file: [],
            fotos: {
                lat: '',
                lng: ''
            },
            totalFotos: 0,
            tituloParametros: 'Para continuar Selecione um parâmetro',
            CodigoPontoSelecionado: 0,
            mostrarListaExcel: false,
            mostrarListaDeListas: true,
            mostrarListaParametrosMedicaoAgua: false,
            mostrarSelecaoListaPontos: false,

            status: {
                CodigoConfiguracaoProgramasStatus: '',
                CodigoEscopoPrograma: '',
                Status: 1,
                DataEnvio: '',
                PrazoCorrecao: ''
            },
            parecer: {
                CodigoParecerFiscal: '',
                CodigoFiscal: '',
                CodigoEscopoPrograma: '',
                StatusFiscal: '',
                Parecer: '',
                DataEnvio: '',
                DataParecer: '',
                AntigaDataCorrecao: null,
                PrazoCorrecao: ''
            },
            listaParecer: [],
            selectedParecer: {},
            usuario: {
                NomeUsuario: '',
                CodigoUsuario: '',
                Perfil: ''
            },
            showCorrecao: false,
            showParecer: false,
            ParecerFiscal: ''
        },

        watch: {
            NomePontoColeta: function(val) {
                this.getPontos();
            },
            'usuario.Perfil'(val) {
                if (this.habilitarParecer) {

                    this.$nextTick(() => {
                        CKEDITOR.replace('textParecer');

                        CKEDITOR.instances['textParecer'].on("change", (evt) => {
                            this.ParecerFiscal = evt.editor.getData();
                        });
                    })
                }
            },
            'usuario.CodigoUsuario'(val) {
                this.parecer.CodigoFiscal = val;
            },
            ParecerFiscal(val) {
                this.parecer.Parecer = String(btoa(val));
            },
            'status.DataEnvio'(val) {
                if (val) {
                    this.parecer.DataEnvio = moment(val).format('YYYY-MM-DD');
                }
            },
            'status.PrazoCorrecao'(val) {
                if (val) {
                    this.parecer.AntigaDataCorrecao = moment(val).format('YYYY-MM-DD');
                }
            },
        },
        computed: {
            toDayDate() {
                return moment().format('YYYY-MM-DD');
            },
            habilitarParecer() {
                if (this.isFiscal && this.status.Status == 2) {
                    return true;
                } else if (this.isFiscal &&
                    this.status.Status == 5 &&
                    this.contratoAtrazado(this.status.PrazoCorrecao)) {
                    return true;
                }
                return false;
            },
            isFiscal() {
                return this.usuario.Perfil.includes('Fiscal');
            },
            isEmValidacaoOuAprovado() {
                return (this.status.Status == 2 || this.status.Status == 4 || this.isEmAtrazo) && !this.isFiscal;
            },
            isEmAtrazo() {
                return this.contratoAtrazado(this.status.PrazoCorrecao);
            }
        },
        async mounted() {
            await this.getPontos();
            await this.getStatus();
            await this.getParecer();
            this.criarSessaoCodigoPrograma();
        },

        methods: {
            async getPontos() {
                let params = {
                    table: 'pontosColetaAgua',
                    // joinLeft: [{
                    //     table1: 'pontosColetaAgua',
                    //     table2: 'fotosConfiguracaoRecursosHidricos',
                    //     on: 'CodigoConfiguracaoRecursosHidricosPontoColeta'
                    // }],
                    where: {
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma
                    }

                }

                //lista pontos
                this.listaPontosColeta = await vmGlobal.getFromAPI(params, 'cgmab');
                this.listaPontosColetaParametros = this.listaPontosColeta;
                //lista para filtro dos pontos
                if (this.listaPontosColetaFiltro === 0) {
                    this.listaPontosColetaFiltro = this.listaPontosColeta.map(x => x.NomePontoColeta).filter(function(elem, index, self) {
                        return index === self.indexOf(elem);
                    });
                }
                this.getFotosPontos();
            },

            async getFotosPontos() {
                let params = {
                    table: 'pontosColetaAgua',
                    joinLeft: [{
                        table1: 'pontosColetaAgua',
                        table2: 'fotosConfiguracaoRecursosHidricos',
                        on: 'CodigoConfiguracaoRecursosHidricosPontoColeta'
                    }],
                    where: {
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma
                    }

                }

                //lista pontos
                this.listaFotosPontosColeta = await vmGlobal.getFromAPI(params, 'cgmab');

            },

            async salvarPontosColeta() {
                this.listaExcel.CodigoEscopoPrograma = this.CodigoEscopoPrograma;
                console.log(this.listaExcel);
                var formData = new FormData();
                formData.append('Dados', JSON.stringify(this.listaExcel));
                await axios.post(base_url + 'ExecucaoProgramas/salvarPontosColeta', formData)
                    .then((resp) => {
                        console.log(resp.data);
                        if (resp.data.status === true) {
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Salvo!',
                                text: 'Dados inseridos com sucesso.',
                                showConfirmButton: true,
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: 'Erro!',
                                text: "Erro ao tentar salvar arquivo. " + e,
                                showConfirmButton: true,

                            });
                        }
                    });
                this.mostrarListaExcel = false;
                this.getPontos();
                $('.dropify-clear').click();
                $(".dropify-wrapper").removeClass('invisivel');
                $('.modal').hide();
                $('.modal-backdrop').remove();

            },
            deletarFotos(codigo) {
                let fd = new FormData();
                fd.append('Codigo', codigo)
                var txt;
                var r = confirm("Deseja deletar a foto?");
                if (r == true) {
                    axios.post('<?= base_url('ConfiguracaoProgramas/deletarFotos') ?>', fd)
                        .then(response => {
                            this.getFotosPontos();
                        }).catch(function(error) {
                            console.log(error)
                        }).finally(() => {

                        })
                }

            },
            qtdFotos() {
                var myfiles = document.getElementById("imagemMaterial");
                this.totalFotos = myfiles.files.length;
            },
            addFotos() {
                $(".page-loader-wrapper").css('display', 'block');
                var myfiles = document.getElementById("imagemMaterial");
                var files = myfiles.files;
                var data = new FormData();
                for (i = 0; i < files.length; i++) {
                    data.append(i, files[i]);
                }
                $.ajax({
                    url: '<?= base_url('ConfiguracaoProgramas/getGps') ?>',
                    method: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Salvo!',
                            text: 'Fotos salvas com sucesso.',
                            showConfirmButton: true,
                        });
                        document.getElementById('imagemMaterial').value = null;
                    },
                    error: function(error) {
                        console.log(error);
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao tentar salvar arquivo. " + e,
                            showConfirmButton: true,

                        });
                    },
                    complete: function() {
                        vmCofiguracaoRecHidricos.getPontos();
                        vmCofiguracaoRecHidricos.NomePontoColeta = '';
                        vmCofiguracaoRecHidricos.totalFotos = 0
                        $('.dropify-clear').click();
                        $(".page-loader-wrapper").css('display', 'none');
                    }

                });

            },
            async importarDadosPontosColeta() {
                $(".page-loader-wrapper").css('display', 'block');

                //upload do arquivo excel
                this.file = this.$refs.file.files[0];

                let formData = new FormData();
                formData.append('file', this.file);

                await axios.post('<?= base_url('API/uploadXLS') ?>', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then((response) => {
                        this.listaExcel = response.data
                    })
                    .catch((error) => {
                        console.log(error);
                    })
                    .finally(() => {
                        this.mostrarListaExcel = true;
                        $(".dropify-wrapper").addClass('invisivel');

                    });
                $(".page-loader-wrapper").css('display', 'none');

            },
            criarSessaoCodigoPrograma() {
                axios.post(base_url + 'ConfiguracaoProgramas/criarSessaoCodigoPrograma', $.param({
                    CodigoEscopoPrograma: this.CodigoEscopoPrograma
                }));
            },
            async iniciarParametros(codigoParametro) {
                vmParametrosAgua.CodigoEscopoPrograma = codigoParametro;
                vmParametrosAgua.listaPontosColetaParametros = this.listaPontosColeta;
                await vmParametrosAgua.getListas();
            },
            setDadosCard(campo, event) {
                switch (campo) {
                    case 'zona':
                        this.dadosCard.Zona = event.target.value;
                        break;
                    case 'cn':
                        this.dadosCard.CN = event.target.value;
                        break;
                    case 'ce':
                        this.dadosCard.CE = event.target.value;
                        break;
                    case 'elevacao':
                        this.dadosCard.Elevacao = event.target.value;
                        break;
                    case 'osb':
                        this.dadosCard.Observacoes = event.target.value;
                        break;
                }

            },
            async AtualizarFoto(codigo) {
                let params = {
                    table: 'fotosConfiguracaoRecursosHidricos',
                    data: this.dadosCard,
                    where: {
                        CodigoConfiguracaoRecursosHidricosFotos: codigo
                    }
                }
                await vmGlobal.updateFromAPI(params, null);
                this.getFotosPontos();
            },

            //-------------------------------- Parecer Fiscal -------------------------------
            async getStatus() {
                var status = await vmGlobal.getFromAPI({
                    table: 'contratosProgramasStatus',
                    where: {
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma,
                    }
                });

                if (status.length != 0) {
                    this.status = status[0];
                }
            },
            async getParecer() {
                await axios.post(base_url + 'ParecerFiscal/getParecerProgramas', $.param({
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma,
                        // CodigoContrato: this.codigoContrato
                    }))
                    .then((response) => {
                        this.listaParecer = response.data.parecer;
                        this.usuario = response.data.usuario;

                        this.refreshTable('#tblParecerFiscal');
                    });
            },
            salvarParecer(statusFiscal) {
                var parecer = Object.assign({}, this.parecer);
                delete parecer.CodigoParecerFiscal;
                delete parecer.DataParecer;
                parecer.CodigoEscopoPrograma = this.CodigoEscopoPrograma;
                // parecer.CodigoContrato = this.codigoContrato;
                parecer.StatusFiscal = statusFiscal;

                if (statusFiscal != 5) {
                    parecer.PrazoCorrecao = null;
                    this.showCorrecao = false;
                } else if (statusFiscal == 5 && parecer.PrazoCorrecao.length == 0) {
                    this.showCorrecao = true;
                    return;
                }

                Swal.fire({
                    title: 'Salvar parecer?',
                    text: "Está ação não pode ser alterada.",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Salvar'
                }).then(async (result) => {
                    if (result.value) {
                        await vmGlobal.insertFromAPI({
                            table: 'parecerProgramas',
                            data: parecer
                        });

                        await vmGlobal.updateFromAPI({
                            table: 'contratosProgramasStatus',
                            data: {
                                Status: statusFiscal,
                                PrazoCorrecao: parecer.PrazoCorrecao
                            },
                            where: {
                                // CodigoContrato: this.codigoContrato,
                                CodigoEscopoPrograma: this.CodigoEscopoPrograma
                            }
                        });

                        window.location.reload();
                    }
                })
            },
            loadParecer(val) {
                this.showParecer = true;
                this.selectedParecer = Object.assign({}, this.parecer);

                this.selectedParecer.StatusFiscal = val.StatusFiscal;
                this.selectedParecer.NomeUsuario = val.NomeUsuario;
                this.selectedParecer.DataParecer = (val.DataParecer) ? moment(val.DataParecer).format('DD/MM/YYYY') : '';
                this.selectedParecer.DataEnvio = (val.DataEnvio) ? moment(val.DataEnvio).format('DD/MM/YYYY') : '';
                this.selectedParecer.AntigaDataCorrecao = (val.AntigaDataCorrecao) ? moment(val.AntigaDataCorrecao).format('DD/MM/YYYY') : '';
                this.selectedParecer.PrazoCorrecao = (val.PrazoCorrecao) ? moment(val.PrazoCorrecao).format('DD/MM/YYYY') : '';

                if (CKEDITOR.instances['txtParecerTecnicoView']) CKEDITOR.instances['txtParecerTecnicoView'].destroy();
                this.$nextTick(() => {
                    CKEDITOR.replace('txtParecerTecnicoView', {
                        height: '400px',
                        readOnly: true
                    });
                });

                this.$nextTick(() => {
                    CKEDITOR.instances['txtParecerTecnicoView'].setData(String(atob(val.Parecer)));
                });
            },
            constroiTable(id = "", pageLength = 10) {
                $('table' + id, this.$el).DataTable({
                    language: translateDataTable,
                    lengthChange: false,
                    destroy: true,
                    pageLength: pageLength
                });
            },
            async refreshTable(id = "") {
                $('table' + id, this.$el).DataTable().destroy();
                await this.$nextTick(() => {
                    this.constroiTable();
                });
            },
            formataData(val) {
                if (val) {
                    return moment(val).format('DD/MM/YYYY');
                }

                return '';
            },
            contratoAtrazado(val) {
                if (val) {
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var prazoCorrecao = moment(val).toDate();
                    prazoCorrecao.setHours(0, 0, 0, 0);

                    return prazoCorrecao.getTime() < today.getTime();
                }

                return false;
            },
            async solicitarValidacao() {
                if (this.status.CodigoConfiguracaoProgramasStatus.length == 0) {
                    await vmGlobal.insertFromAPI({
                        table: 'contratosProgramasStatus',
                        data: {
                            CodigoEscopoPrograma: this.CodigoEscopoPrograma,
                            // CodigoContrato: this.codigoContrato,
                            Status: 2,
                            DataEnvio: moment().format('YYYY-MM-DD HH:mm:ss')
                        }
                    });

                    await this.getStatus();

                    return;
                }

                await vmGlobal.updateFromAPI({
                    table: 'contratosProgramasStatus',
                    where: {
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma,
                        // CodigoContrato: this.codigoContrato,
                    },
                    data: {
                        Status: 2,
                        DataEnvio: moment().format('YYYY-MM-DD HH:mm:ss')
                    }
                });

                await this.getStatus();
            },
            getClasse(val) {
                return (val == 4) ? 'badge-success' : 'btn-rejeitado';
            },
            getStatusDescricao(val) {
                return (val == 4) ? 'Aprovado' : 'Parcialmente Aprovado';
            },
            contratoAtrazado(val) {
                if (val) {
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var prazoCorrecao = moment(val).toDate();
                    prazoCorrecao.setHours(0, 0, 0, 0);

                    return prazoCorrecao.getTime() < today.getTime();
                }

                return false;
            },
            async deletar(ponto) {
                const params = {
                    table: 'pontosColetaAgua',
                    where: {
                        CodigoConfiguracaoRecursosHidricosPontoColeta: ponto
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                await this.getPontos();
            }
        }
    })
    $(function() {
        "use strict";
        $('.dropify').dropify({
            messages: {
                default: "Solte ou clique para anexar arquivo."
            }
        });


    });
</script>