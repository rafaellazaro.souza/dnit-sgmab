<script>
    const PerfilUsuario = '<?= $this->session->PerfilLogin ?>';

    var vmCofiguracaoFaunas = new Vue({
        el: "#fotos",
        data() {
            return {
                CodigoModulo: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[0].split('=')[1], 'decode'),
                CodigoPrograma: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[1].split('=')[1], 'decode'),
                NomeModuloAmostragem: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),
                NumeroPonto: "",
                listaMonitoramentoFauna: {},
                listaGrupoFaunistico: {},
                listaEquipamentos: {},
                // file: [],
                fotos: {
                    lat: '',
                    lng: ''
                },
                totalFotos: 0,
                modelDadosFotos: [],
                errorsAddCondicionante: [],
                usuario: {
                    Perfil: PerfilUsuario
                },
                status: {
                    CodigoConfiguracaoProgramasStatus: '',
                    CodigoEscopoPrograma: '',
                    Status: 1,
                    DataEnvio: '',
                    PrazoCorrecao: ''
                },
            }
        },

        watch: {
            NumeroPonto: function(val) {
                this.getPontos();
            },
        },
        computed: {
            isFiscal() {
                return this.usuario.Perfil.includes('Fiscal');
            },
            isEmValidacaoOuAprovado() {
                return (this.status.Status == 2 || this.status.Status == 4 || this.isEmAtrazo) && !this.isFiscal;
            },
            isEmAtrazo() {
                return this.contratoAtrazado(this.status.PrazoCorrecao);
            }
        },

        async mounted() {
            await this.getPontos();
            this.criarSessaoCodigoPrograma();
            await this.getEquipamentos();
            await this.getGruposFaunisticos();
            await this.getStatus();

            this.$nextTick(() => {
                $('.dropify').dropify({
                    messages: {
                        default: "Solte ou clique para anexar arquivo."
                    }
                });
            });
        },

        filters: {
            singleName(valor) {
                return valor.replace(/.\w+$/, "");
            }
        },

        methods: {
            async getPontos() {
                let params = {
                    table: 'configFaunaFotos',
                    where: {
                        CodigoModulo: this.CodigoModulo
                    }
                }
                this.listaMonitoramentoFauna = await vmGlobal.getFromAPI(params, null);

                for (let i = 0; i < this.listaMonitoramentoFauna.length; i++) {
                    this.listaMonitoramentoFauna[i].modelDadosFotosInformacao = []
                    this.errorsAddCondicionante[i] = []
                }
            },

            async getEquipamentos() {
                //Lista os equipamentos
                return this.listaEquipamentos = await vmGlobal.getFromController('ConfiguracaoProgramas/getListasEquipamentoFauna?CodigoPrograma=' + this.CodigoPrograma)
            },

            async getGruposFaunisticos() {
                //Lista os grupos Faunisticos
                var params = {
                    table: 'gruposFaunisticos'
                }
                return this.listaGrupoFaunistico = await vmGlobal.getFromAPI(params, 'cgmab');
            },

            deletarFotos(codigo, caminho) {
                //Exclui as fotografias
                Swal.fire({
                    title: 'Você tem certeza?',
                    text: "Você não poderá reverter isso!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim, excluir!'
                }).then((result) => {
                    if (result.value) {
                        let fd = new FormData();
                        fd.append('Codigo', codigo)
                        var txt;

                        axios.post('<?= base_url('ConfiguracaoProgramas/deletarFotosFauna') ?>', fd)
                            .then(response => {
                                vmGlobal.deleteFile(caminho);
                                vmCofiguracaoFaunas.getPontos();
                                Swal.fire(
                                    'Excluído!',
                                    'Sua fotografia foi excluída com sucesso.',
                                    'success'
                                )
                            }).catch(function(error) {
                                console.log(error)
                            }).finally(() => {})
                    }
                })
            },

            qtdFotos() {
                var myfiles = document.getElementById("imagemMaterial");
                this.totalFotos = myfiles.files.length;
            },

            verificaInformacoesFotos(i, codigo) {
                //Verifica as informações das fotografias
                $("#error-" + i).html('').stop()
                var verificaInformacao = true;
                var html = ''
                if (!this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['Zona'] ||
                    !this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CN'] ||
                    !this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CE'] ||
                    !this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['Elevacao'] ||
                    !this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoRecurso'] ||
                    !this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoGrupoFaunistico']) {
                    html += '<div class="alert alert-warning p-3 w-100 " >' +
                        '<h5>Erro ao salvar. Por favor Preencha os campos abaixo:</h5>'
                    '<ul>'

                    if (this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['Zona'] == undefined) {
                        verificaInformacao = false;
                        html += '<li>A Zona é obrigatório.</li>';
                    }
                    if (this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CN'] == undefined) {
                        verificaInformacao = false;
                        html += '<li>O CN é obrigatório.</li>';
                    }
                    if (this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CE'] == undefined) {
                        verificaInformacao = false;
                        html += '<li>O CE é obrigatório.</li>';
                    }
                    if (this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['Elevacao'] == undefined) {
                        verificaInformacao = false;
                        html += '<li>A Elevação(m) é obrigatório.</li>';
                    }
                    if (this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoRecurso'] ==
                        undefined) {
                        verificaInformacao = false;
                        html += '<li>O Equipamento Técnico é obrigatório.</li>';
                    }
                    if (this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoGrupoFaunistico'] ==
                        undefined) {
                        verificaInformacao = false;
                        html += '<li>O Grupo Faunístico é obrigatório..</li>';
                    }
                    if (html != '') {
                        html += '</ul>' +
                            '</div>'
                    }
                    $("#error-" + i).html(html)
                    return;
                }
                vmCofiguracaoFaunas.salvarInformacoesFotos(i, codigo, verificaInformacao);
            },

            async salvarInformacoesFotos(i, codigo, verificaInformacao) {
                //Salva as informações das fotografias
                var params = {
                    table: 'configFaunaFotos',
                    data: {
                        Zona: this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['Zona'],
                        CN: this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CN'],
                        CE: this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CE'],
                        Elevacao: this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['Elevacao'],
                        CodigoRecurso: this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoRecurso'],
                        CodigoGrupoFaunistico: this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoGrupoFaunistico'],
                        ObservacoesFoto: this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['ObservacoesFoto'],
                    },
                    where: {
                        CodigoConfiguracaoMonitoramentoFaunaFotos: codigo
                    }
                }

                if (this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['ObservacoesFoto'] == null) {
                    if (verificaInformacao == true)
                        Swal.fire({
                            title: 'Deseja salvar sem observações?',
                            text: "Você não poderá reverter isso!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#1e7e34',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Sim, salvar!'
                        }).then(async (result) => {
                            if (result.value) {
                                if (verificaInformacao == true) {
                                    await vmGlobal.updateFromAPI(params, null, 'cgmab')
                                    vmCofiguracaoFaunas.getPontos();
                                }
                            }
                        });
                } else {
                    await vmGlobal.updateFromAPI(params, true, 'cgmab')
                    vmCofiguracaoFaunas.getPontos();
                }
            },

            async verificaFotos() {
                //Adiciona as fotografias
                var myfiles = document.getElementById("imagemMaterial");
                var files = myfiles.files;
                var data = new FormData();

                //Listo os IDs do Registros do banco de dados
                let arrayArmadilhas = [];
                this.listaMonitoramentoFauna.forEach((valor, index) => arrayArmadilhas.push(valor.NomeFoto));

                let strError = "";
                let numArray = 0;

                for (i = 0; i < files.length; i++) {
                    //verifica o nome do arquivo
                    let verificaNome = files[i].name.split("_");

                    //verifica se o nome do arquivo já existe
                    // let verificaArquivo = files[i].name.includes();
                    let verificaArquivo = arrayArmadilhas.includes(files[i].name);

                    if (verificaNome.length != 2 || verificaArquivo) {
                        strError += files[i].name + "<br>";

                    } else {
                        data.append(numArray, files[i]);
                        data.append('CodigoModulo', parseInt(this.CodigoModulo));
                        numArray += 1;
                    }
                }
                if (strError != "") {
                    await Swal.fire({
                        type: 'error',
                        title: 'Nome da Imagem está fora dos padrões',
                        text: 'Por favor Corrigir!',
                        html: 'Por favor Corrigir! </br>' + '</br>' +
                            strError,
                    })
                }
                await vmCofiguracaoFaunas.addFotos(data);
            },

            addFotos(data) {
                $.ajax({
                    url: '<?= base_url('ConfiguracaoProgramas/getGpsFauna') ?>',
                    method: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: async function(data) {
                        await Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Salvo!',
                            text: 'Fotos salvas com sucesso.',
                            timer: 1500,
                            showConfirmButton: true,
                        });
                        document.getElementById('imagemMaterial').value = null;
                    },
                    error: function(error) {
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao tentar salvar arquivo. " + e,
                            showConfirmButton: true,

                        });
                    },
                    complete: function() {
                        vmCofiguracaoFaunas.getPontos();
                        vmCofiguracaoFaunas.NumeroPonto = '';
                        vmCofiguracaoFaunas.totalFotos = 0
                        $('.dropify-clear').click();
                    }
                });
            },

            buscaNomeEquipamento(listaEquipamentos, Codigo) {
                //Busca o nome do equipamento
                for (let equipamento of Object.keys(listaEquipamentos)) {
                    let equipamentoSelecionado = listaEquipamentos[equipamento]
                    if (Codigo === equipamentoSelecionado.CodigoEquipamento) {
                        return equipamentoSelecionado.NomeEquipamento
                    }
                }
            },

            buscaGrupoFaunitico(listaGrupoFaunistico, CodigoGrupo) {
                //Busca o nome do grupo faunistico
                for (let grupo of Object.keys(listaGrupoFaunistico)) {
                    let grupoSelecionado = listaGrupoFaunistico[grupo]
                    if (CodigoGrupo === grupoSelecionado.CodigoGrupoFaunistico) {
                        return grupoSelecionado.NomeGrupoFaunistico
                    }
                }
            },

            criarSessaoCodigoPrograma() {
                axios.post(base_url + 'ConfiguracaoProgramas/criarSessaoCodigoPrograma', $.param({
                    CodigoModulo: this.CodigoModulo
                }));
            },
            async getStatus() {
                var status = await vmGlobal.getFromAPI({
                    table: 'contratosProgramasStatus',
                    where: {
                        CodigoEscopoPrograma: this.CodigoPrograma,
                    }
                });

                if (status.length != 0) {
                    this.status = status[0];
                }
            },
            contratoAtrazado(val) {
                if (val) {
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var prazoCorrecao = moment(val).toDate();
                    prazoCorrecao.setHours(0, 0, 0, 0);

                    return prazoCorrecao.getTime() < today.getTime();
                }

                return false;
            },

        }
    });
    // $(function() {
    //     "use strict";

    // });
</script>