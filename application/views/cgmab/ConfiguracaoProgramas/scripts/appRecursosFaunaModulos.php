<script>
    var vmConfiguracaoFaunaModulos = new Vue({
        el: '#faunaModulos',
        data: {
            CodigoEscopoPrograma: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('=')[1],
                'decode'),
            listaFaunaModulos: 0,
            dadosFaunaModulos: {},
            mostrarAddFaunaModulos: false,
            erros: 0,

            status: {
                CodigoConfiguracaoProgramasStatus: '',
                CodigoEscopoPrograma: '',
                Status: 1,
                DataEnvio: '',
                PrazoCorrecao: ''
            },
            parecer: {
                CodigoParecerFiscal: '',
                CodigoFiscal: '',
                CodigoEscopoPrograma: '',
                StatusFiscal: '',
                Parecer: '',
                DataEnvio: '',
                DataParecer: '',
                AntigaDataCorrecao: null,
                PrazoCorrecao: ''
            },
            listaParecer: [],
            selectedParecer: {},
            usuario: {
                NomeUsuario: '',
                CodigoUsuario: '',
                Perfil: ''
            },
            showCorrecao: false,
            showParecer: false,
            ParecerFiscal: ''
        },
        async mounted() {
            await this.getFaunaModulos();
            await this.getStatus();
            await this.getParecer();
        },
        watch: {
            'usuario.Perfil'(val) {
                if (this.habilitarParecer) {

                    this.$nextTick(() => {
                        CKEDITOR.replace('textParecer');

                        CKEDITOR.instances['textParecer'].on("change", (evt) => {
                            this.ParecerFiscal = evt.editor.getData();
                        });
                    })
                }
            },
            'usuario.CodigoUsuario'(val) {
                this.parecer.CodigoFiscal = val;
            },
            ParecerFiscal(val) {
                this.parecer.Parecer = String(btoa(val));
            },
            'status.DataEnvio'(val) {
                if (val) {
                    this.parecer.DataEnvio = moment(val).format('YYYY-MM-DD');
                }
            },
            'status.PrazoCorrecao'(val) {
                if (val) {
                    this.parecer.AntigaDataCorrecao = moment(val).format('YYYY-MM-DD');
                }
            },
        },
        computed: {
            toDayDate() {
                return moment().format('YYYY-MM-DD');
            },
            habilitarParecer() {
                if (this.isFiscal && this.status.Status == 2) {
                    return true;
                } else if (this.isFiscal &&
                    this.status.Status == 5 &&
                    this.contratoAtrazado(this.status.PrazoCorrecao)) {
                    return true;
                }
                return false;
            },
            isFiscal() {
                return this.usuario.Perfil.includes('Fiscal');
            },
            isEmValidacaoOuAprovado() {
                return (this.status.Status == 2 || this.status.Status == 4 || this.isEmAtrazo) && !this.isFiscal;
            },
            isEmAtrazo() {
                return this.contratoAtrazado(this.status.PrazoCorrecao);
            }
        },
        methods: {
            async getFaunaModulos() {
                $("#listadeModulos").DataTable().destroy();
                const params = {
                    table: 'configFaunaModulos',
                    where: {
                        Status: 1,
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma
                    }
                }
                this.listaFaunaModulos = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#listadeModulos', true);
            },

            checkForm: function(e) {
                if (this.dadosFaunaModulos.NomeModuloAmostragem && this.dadosFaunaModulos.TipoModulo && this
                    .dadosFaunaModulos.Area && this.dadosFaunaModulos.DescricaoFitoFisionomia &&
                    this.dadosFaunaModulos.Municipio && this.dadosFaunaModulos.Observacoes) {
                    this.salvarModulo();
                    return true;
                }

                this.erros = [];

                if (!this.dadosFaunaModulos.NomeModuloAmostragem) {
                    this.erros.push('Nome Modulo Amostragem é obrigatório!');
                }
                if (!this.dadosFaunaModulos.TipoModulo) {
                    this.erros.push('Tipo Modulo é obrigatório!');
                }
                if (!this.dadosFaunaModulos.Area) {
                    this.erros.push('Área é obrigatório!');
                }
                if (!this.dadosFaunaModulos.DescricaoFitoFisionomia) {
                    this.erros.push('Descrição Fitofisionomia é obrigatório!');
                }
                if (!this.dadosFaunaModulos.Municipio) {
                    this.erros.push('Município é obrigatório!');
                }
                if (!this.dadosFaunaModulos.Observacoes) {
                    this.erros.push('Observações é obrigatório!');
                }

                e.preventDefault();
            },

            async salvarModulo() {
                this.dadosFaunaModulos.CodigoEscopoPrograma = this.CodigoEscopoPrograma;
                this.dadosFaunaModulos.DataCadastro = new Date();

                const params = {
                    table: 'configFaunaModulos',
                    data: this.dadosFaunaModulos
                }
                await vmGlobal.insertFromAPI(params, 'cgmab');
                this.mostrarAddFaunaModulos = false;
                this.getFaunaModulos();
                this.dadosFaunaModulos = {};
            },

            async deletar(codigo, index) {
                params = {
                    table: 'configFaunaModulos',
                    where: {
                        CodigoModulo: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                this.listaFaunaModulos.splice(index, 1);
            },

            async getStatus() {
                var status = await vmGlobal.getFromAPI({
                    table: 'contratosProgramasStatus',
                    where: {
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma,
                    }
                });

                if (status.length != 0) {
                    this.status = status[0];
                }
            },
            async getParecer() {
                await axios.post(base_url + 'ParecerFiscal/getParecerProgramas', $.param({
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma,
                        // CodigoContrato: this.codigoContrato
                    }))
                    .then((response) => {
                        this.listaParecer = response.data.parecer;
                        this.usuario = response.data.usuario;

                        this.refreshTable('#tblParecerFiscal');
                    });
            },
            salvarParecer(statusFiscal) {
                var parecer = Object.assign({}, this.parecer);
                delete parecer.CodigoParecerFiscal;
                delete parecer.DataParecer;
                parecer.CodigoEscopoPrograma = this.CodigoEscopoPrograma;
                // parecer.CodigoContrato = this.codigoContrato;
                parecer.StatusFiscal = statusFiscal;

                if (statusFiscal != 5) {
                    parecer.PrazoCorrecao = null;
                    this.showCorrecao = false;
                } else if (statusFiscal == 5 && parecer.PrazoCorrecao.length == 0) {
                    this.showCorrecao = true;
                    return;
                }

                Swal.fire({
                    title: 'Salvar parecer?',
                    text: "Está ação não pode ser alterada.",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Salvar'
                }).then(async (result) => {
                    if (result.value) {
                        await vmGlobal.insertFromAPI({
                            table: 'parecerProgramas',
                            data: parecer
                        });

                        await vmGlobal.updateFromAPI({
                            table: 'contratosProgramasStatus',
                            data: {
                                Status: statusFiscal,
                                PrazoCorrecao: parecer.PrazoCorrecao
                            },
                            where: {
                                // CodigoContrato: this.codigoContrato,
                                CodigoEscopoPrograma: this.CodigoEscopoPrograma
                            }
                        });

                        window.location.reload();
                    }
                })
            },
            loadParecer(val) {
                this.showParecer = true;
                this.selectedParecer = Object.assign({}, this.parecer);

                this.selectedParecer.StatusFiscal = val.StatusFiscal;
                this.selectedParecer.NomeUsuario = val.NomeUsuario;
                this.selectedParecer.DataParecer = (val.DataParecer) ? moment(val.DataParecer).format('DD/MM/YYYY') : '';
                this.selectedParecer.DataEnvio = (val.DataEnvio) ? moment(val.DataEnvio).format('DD/MM/YYYY') : '';
                this.selectedParecer.AntigaDataCorrecao = (val.AntigaDataCorrecao) ? moment(val.AntigaDataCorrecao).format('DD/MM/YYYY') : '';
                this.selectedParecer.PrazoCorrecao = (val.PrazoCorrecao) ? moment(val.PrazoCorrecao).format('DD/MM/YYYY') : '';

                if (CKEDITOR.instances['txtParecerTecnicoView']) CKEDITOR.instances['txtParecerTecnicoView'].destroy();
                this.$nextTick(() => {
                    CKEDITOR.replace('txtParecerTecnicoView', {
                        height: '400px',
                        readOnly: true
                    });
                });

                this.$nextTick(() => {
                    CKEDITOR.instances['txtParecerTecnicoView'].setData(String(atob(val.Parecer)));
                });
            },
            constroiTable(id = "", pageLength = 10) {
                $('table' + id, this.$el).DataTable({
                    language: translateDataTable,
                    lengthChange: false,
                    destroy: true,
                    pageLength: pageLength
                });
            },
            async refreshTable(id = "") {
                $('table' + id, this.$el).DataTable().destroy();
                await this.$nextTick(() => {
                    this.constroiTable();
                });
            },
            formataData(val) {
                if (val) {
                    return moment(val).format('DD/MM/YYYY');
                }

                return '';
            },
            contratoAtrazado(val) {
                if (val) {
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var prazoCorrecao = moment(val).toDate();
                    prazoCorrecao.setHours(0, 0, 0, 0);

                    return prazoCorrecao.getTime() < today.getTime();
                }

                return false;
            },
            async solicitarValidacao() {
                if (this.status.CodigoConfiguracaoProgramasStatus.length == 0) {
                    await vmGlobal.insertFromAPI({
                        table: 'contratosProgramasStatus',
                        data: {
                            CodigoEscopoPrograma: this.CodigoEscopoPrograma,
                            // CodigoContrato: this.codigoContrato,
                            Status: 2,
                            DataEnvio: moment().format('YYYY-MM-DD HH:mm:ss')
                        }
                    });

                    await this.getStatus();

                    return;
                }

                await vmGlobal.updateFromAPI({
                    table: 'contratosProgramasStatus',
                    where: {
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma,
                        // CodigoContrato: this.codigoContrato,
                    },
                    data: {
                        Status: 2,
                        DataEnvio: moment().format('YYYY-MM-DD HH:mm:ss')
                    }
                });

                await this.getStatus();
            },
            getClasse(val) {
                return (val == 4) ? 'badge-success' : 'btn-rejeitado';
            },
            getStatusDescricao(val) {
                return (val == 4) ? 'Aprovado' : 'Parcialmente Aprovado';
            },
            contratoAtrazado(val) {
                if (val) {
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var prazoCorrecao = moment(val).toDate();
                    prazoCorrecao.setHours(0, 0, 0, 0);

                    return prazoCorrecao.getTime() < today.getTime();
                }

                return false;
            },

        },
    })
</script>