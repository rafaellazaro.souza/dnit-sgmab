<script>
    var vmParametrosAgua = new Vue({
        el: '#modalListaParametros',
        data: {
            tituloParametro: '',
            CodigoEscopoPrograma: 0,
            CodigoPontoSelecionado: 0,
            CodigoParametroSelecionado: 0,
            mostrarListas: true,
            mostrarListaParametrosMedicaoAgua: false,
            mostrarSelecaoListaPontos: false,
            mostrarFormAddLista: false,
            mostrarParametrosSistema: false,
            mostrarPontosSistema: false,
            mostrarEquipamentosUtilizados: false,
            listaParamestrosLista: [],
            listaParamestros: [],
            listaParamestrosSelecionados: [],
            listaPontosSelecionados: [],
            listaPontosColetaParametros: [],
            listaPontosColetaDaLista: [],
            listaEquipamentosNoEscopo: [],
            listaEquipamentos: [],
            listas: [],
            listaSelecionada: {},
            dadosLista: {},
            dadosListaParametro: {},
            dadosAtualizarParametroLista: {},
            modelEquipamentoEscopo: {}
        },
        watch: {
            CodigoPontoSelecionado: function(val) {

                this.listaSelecionada = val;
                this.mostrarListaParametrosMedicaoAgua = true;
                this.mostrarListas = false;
                this.getParamestros();
                this.getParamestrosLista();
            }
        },
        methods: {
            async getListas() {
                this.listas = await vmGlobal.getFromController('ConfiguracaoProgramas/getListas', {
                    CodigoEscopoPrograma: this.CodigoEscopoPrograma
                });
            },
            async getParamestrosLista() {
                let params = {
                    table: 'listaParametrosPrograma',
                    join: {
                        table: 'parametrosAgua',
                        on: 'CodigoParametrosMedicaoAgua'
                    },
                    where: {
                        CodigoLista: this.listaSelecionada.CodigoLista
                    }
                }
                this.listaParamestrosLista = await vmGlobal.getFromAPI(params, 'cgmab');
                $('#parametros-tab').click();
            },
            async getPontosLista() {
                let params = {
                    table: 'listaPontosProgramaLista',
                    join: {
                        table: 'pontosColetaAgua',
                        on: 'CodigoConfiguracaoRecursosHidricosPontoColeta'
                    },
                    where: {
                        CodigoLista: this.listaSelecionada.CodigoLista
                    }
                }
                this.listaPontosColetaDaLista = await vmGlobal.getFromAPI(params, 'cgmab');
                $('#pontos-tab').click();
            },
            async getParamestros() {
                let params = {
                    table: 'parametrosAgua',
                    where: {
                        Status: 1
                    }
                }
                this.listaParamestros = await vmGlobal.getFromAPI(params, 'cgmab');
            },
            async salvarlista() {
                $(".page-loader-wrapper").css('display', 'block');

                if (this.dadosLista.CodigoLista) { //update
                    this.dadosLista.CodigoEscopoPrograma = this.CodigoEscopoPrograma;
                    let params = {
                        table: 'listasParametros',
                        data: {
                            NomeLista: this.dadosLista.nomeLista
                        },
                        where: {
                            CodigoLista: this.dadosLista.CodigoLista
                        }

                    }
                    await vmGlobal.updateFromAPI(params, null, true);
                } else { //insert
                    this.dadosLista.CodigoEscopoPrograma = this.CodigoEscopoPrograma;
                    let params = {
                        table: 'listasParametros',
                        data: this.dadosLista
                    }
                    await vmGlobal.insertFromAPI(params, null, true);
                }

                await this.getListas();
                this.mostrarFormAddLista = false;
                this.dadosLista = {};
                $(".page-loader-wrapper").css('display', 'none');
            },
            async salvarParametrosNaLista() {
                $(".page-loader-wrapper").css('display', 'block');
                var formData = new FormData();
                formData.append('Dados', JSON.stringify(this.listaParamestrosSelecionados));
                this.listaParamestrosSelecionados = [];
                await axios.post(base_url + 'ConfiguracaoProgramas/salvarParametrosNaLista', formData)
                    .then((resp) => {
                        if (resp.data.status === true) {
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Salvo!',
                                text: 'Dados inseridos com sucesso.',
                                showConfirmButton: true,
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: 'Erro!',
                                text: "Erro ao tentar salvar arquivo. " + e,
                                showConfirmButton: true,

                            });
                        }
                    })
                    .finally(() => {
                        this.getParamestrosLista();
                        this.getListas();
                        this.mostrarParametrosSistema = false;
                        $(".page-loader-wrapper").css('display', 'none');
                    })
            },
            async salvarPontosNaLista() {

                $(".page-loader-wrapper").css('display', 'block');

                var formData = new FormData();
                formData.append('Dados', JSON.stringify(this.listaPontosSelecionados));
                this.listaPontosSelecionados = [];
                await axios.post(base_url + 'ConfiguracaoProgramas/salvarPontosNaLista', formData)
                    .then((resp) => {
                        if (resp.data.status === true) {
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Salvo!',
                                text: 'Dados inseridos com sucesso.',
                                showConfirmButton: true,
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: 'Erro!',
                                text: "Erro ao tentar salvar arquivo. " + e,
                                showConfirmButton: true,

                            });
                        }

                    })
                    .finally(() => {
                        window.location.reload();
                        // this.getPontosLista();
                        // this.getListas();
                        // this.mostrarParametrosSistema = false;
                        // this.mostrarPontosSistema = false;
                    })
            },
            async atualizarParametroDaLista(codigo, event, campo) {


                if (campo === 'Limite') {
                    this.dadosAtualizarParametroLista.Limite = event.target.value;
                } else if (campo === 'ValorLimite') {
                    this.dadosAtualizarParametroLista.ValorLimite = event.target.value;
                } else if (campo === 'MetodologiaAnalise') {
                    this.dadosAtualizarParametroLista.MetodologiaAnalise = event.target.value;
                } else {
                    this.dadosAtualizarParametroLista.ObservacoesEspecificas = event.target.value;
                }
                let params = {
                    table: 'listaParametrosPrograma',
                    data: this.dadosAtualizarParametroLista,
                    where: {
                        CodigoRecursosHidricosParametros: codigo
                    }
                }
                await vmGlobal.updateFromAPI(params, null, false);
                delete this.dadosAtualizarParametroLista.Limite;
                delete this.dadosAtualizarParametroLista.ValorLimite;
                delete this.dadosAtualizarParametroLista.MetodologiaAnalise;
                delete this.dadosAtualizarParametroLista.ObservacoesEspecificas;
                this.getParamestrosLista();
            },
            resetConfParametros() {
                this.mostrarListas = true;
                this.mostrarListaParametrosMedicaoAgua = false;
                this.mostrarSelecaoListaPontos = false;
                this.mostrarFormAddLista = false;
                this.mostrarParametrosSistema = false;
                this.getListas();
            },
            setParametroPontosLista(codigo, nome, codigoArray, tipoDado) {
                var lista, listaSelecionados;
                if (tipoDado === 'parametro') {
                    lista = this.listaPontosColetaParametros;
                    listaSelecionados = this.listaParamestrosSelecionados;
                } else {
                    lista = this.listaPontosColetaParametros;
                    listaSelecionados = this.listaPontosSelecionados;
                }
                //remover da lista principal
                lista.splice(codigoArray, 1);
                //Adicionar na lista dos selecionados
                this.incluirNaListaDeSelecionados(codigo, nome, tipoDado);
            },
            incluirNaListaDeSelecionados(codigo, nome, tipoDado) {
                if (tipoDado === 'parametro') {
                    if (nome === 'IQA') {
                        this.listaParamestrosSelecionados.push({
                            CodigoLista: this.listaSelecionada.CodigoLista,
                            CodigoParametrosMedicaoAgua: codigo,
                            NomeParametro: nome,
                            Limite: 'Fórmula'
                        });
                    } else {
                        this.listaParamestrosSelecionados.push({
                            CodigoLista: this.listaSelecionada.CodigoLista,
                            CodigoParametrosMedicaoAgua: codigo,
                            NomeParametro: nome,
                            Limite: ''
                        });
                    }
                } else {
                    this.listaPontosSelecionados.push({
                        CodigoLista: this.listaSelecionada.CodigoLista,
                        CodigoConfiguracaoRecursosHidricosPontoColeta: codigo.split("-")[0],
                        NomePonto: nome
                    });
                }

            },
            removerParametroPontosLista(codigoArray, tipoDado) {
                var lista, listaSelecionados;
                if (tipoDado === 'parametro') {
                    listaSelecionados = this.listaParamestrosSelecionados;
                } else {
                    listaSelecionados = this.listaPontosSelecionados;
                }
                listaSelecionados.splice(codigoArray, 1);
                if (listaSelecionados.length === 0) {
                    this.getParamestros();
                }
            },

            async iniciarEquipamentosParametro(nomeParametro, codigoListaParametro) {
                this.mostrarEquipamentosUtilizados = true;
                this.tituloParametro = nomeParametro;
                this.CodigoParametroSelecionado = codigoListaParametro;
                await this.getEquipamentosNoEscopo();
                await this.getEquipamentos();
            },

            async getEquipamentos() {

                let params = {
                    table: 'equipamentosUtilizados',
                    joinLeft: [{
                            table1: 'equipamentosUtilizados',
                            table2: 'escopoDeRecursos',
                            on: 'CodigoRecurso',
                        },
                        {
                            table1: 'escopoDeRecursos',
                            table2: 'equipamentos',
                            on: 'CodigoEquipamento',
                        }
                    ],
                    where: {
                        CodigoRecursosHidricosParametros: this.CodigoParametroSelecionado
                    }
                }
                this.listaEquipamentos = await vmGlobal.getFromAPI(params, 'cgmab');
            },
            async getEquipamentosNoEscopo() {
                let params = {
                    table: 'escopoDeRecursos',
                    join: {
                        table: 'equipamentos',
                        on: 'CodigoEquipamento'
                    },
                    where: {
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma
                    }
                }
                this.listaEquipamentosNoEscopo = await vmGlobal.getFromAPI(params, 'cgmab');
            },
            async adicionarEquipamentoParametro() {
                let params = {
                    table: 'equipamentosUtilizados',
                    data: {
                        CodigoRecurso: this.modelEquipamentoEscopo.CodigoRecurso,
                        CodigoRecursosHidricosParametros: this.CodigoParametroSelecionado
                    }
                }
                await vmGlobal.insertFromAPI(params, null, 'cgmab');
                this.getEquipamentos();
            },
            async deletarEquipamentoNoParametro(codigo) {
                let params = {
                    table: 'equipamentosUtilizados',
                    where: {
                        CodigoRecursosHidricosEquipamentos: codigo

                    }
                }
                await vmGlobal.deleteFromAPI(params, 'cgmab');
                this.getEquipamentos();
            },
            async deletarLista(codigo) {
                $(".page-loader-wrapper").css('display', 'block');
                let params = {
                    table: 'listasParametros',
                    where: {
                        CodigoLista: codigo

                    }
                }
                await vmGlobal.deleteFromAPI(params, 'cgmab');
                await this.getListas();
                $(".page-loader-wrapper").css('display', 'none');
            },
            async deletarParametroLista(CodigoParametrosMedicaoAgua, CodigoLista) {
                let params = {
                    table: 'listaParametrosPrograma',
                    where: {
                        CodigoLista: CodigoLista,
                        CodigoParametrosMedicaoAgua: CodigoParametrosMedicaoAgua
                    }
                }
                await vmGlobal.deleteFromAPI(params, 'cgmab');
                this.getParamestrosLista();
            },
            async deletarPontoLista(CodigoLista, CodigoConfiguracaoRecursosHidricosPontoColeta) {
                let params = {
                    table: 'listaPontosProgramaLista',
                    where: {
                        CodigoLista: CodigoLista,
                        CodigoConfiguracaoRecursosHidricosPontoColeta: CodigoConfiguracaoRecursosHidricosPontoColeta
                    }
                }
                await vmGlobal.deleteFromAPI(params, 'cgmab');
                this.getPontosLista();
            },
        }
    });
</script>