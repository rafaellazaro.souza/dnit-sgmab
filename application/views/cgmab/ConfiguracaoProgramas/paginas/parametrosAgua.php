   <?php $this->load->view('cgmab/ConfiguracaoProgramas/css/parametrosAgua') ?>
   <!-- modal Adicionar mais paramestros de agua -->
   <div class="modal fade bd-example-modal-xl" id="modalListaParametros" tabindex="-1" role="dialog">
     <div class="modal-dialog modal-xl" role="document">
       <div class="modal-content">
         <div class="modal-body">

           <ul class="nav nav-tabs" id="myTab" role="tablist">
             <li class="nav-item">
               <a :class="mostrarListas ? 'nav-link border active' : 'nav-link border'" id="home-tab" data-toggle="tab" href="#listas" role="tab" aria-controls="home" aria-selected="true">Listas de Parametros</a>
             </li>
             <li class="nav-item" v-show="mostrarListaParametrosMedicaoAgua">
               <a :class="mostrarListaParametrosMedicaoAgua ? 'nav-link border active' : 'nav-link border'" id="parametros-tab" data-toggle="tab" href="#parametros" role="tab" aria-controls="profile" aria-selected="false">Parâmetros</a>
             </li>
             <li class="nav-item" v-show="mostrarListaParametrosMedicaoAgua">
               <a @click="getPontosLista()" :class="mostrarSelecaoListaPontos ? 'nav-link border active' : 'nav-link border'" id="pontos-tab" data-toggle="tab" href="#pontos" role="tab" aria-controls="contact" aria-selected="false">Pontos de Coleta</a>
             </li>
           </ul>
           <div class="tab-content" id="myTabContent">
             <!-- listas de paramestros -->
             <div :class="mostrarListas ? 'tab-pane ml-3 fade show active' : 'tab-pane ml-3 fade'" id="listas" role="tabpanel" aria-labelledby="home-tab">
               <div class="w-100 h450-r">
                 <table class="table table-striped" id="listaPontosExcel">
                   <thead>
                     <tr>
                       <th>Lista</th>
                       <th>Quantidade Parâmetros</th>
                       <th>Quantidade Pontos</th>
                       <th><i class="fa fa-gears"></i></th>

                     </tr>
                   </thead>
                   <tbody>

                     <tr v-for="i in listas">
                       <td v-text="i.NomeLista"></td>
                       <td v-text="i.TotalParametros"></td>
                       <td v-text="i.TotalPontos"></td>
                       <th>
                         <button @click="CodigoPontoSelecionado = i" class="btn btn-outline-secondary btn-sm"><i class="fa fa-check"></i></button>

                         <button @click="mostrarFormAddLista = true; dadosLista.nomeLista = i.NomeLista; dadosLista.CodigoLista = i.CodigoLista" class="btn btn-outline-info btn-sm"><i class="fa fa-pen"></i></button>

                         <button @click="deletarLista(i.CodigoLista)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                       </th>
                     </tr>
                   </tbody>
                 </table>
               </div>

               

               <!-- formulario add listas  -->
               <div class="desc-programas p-4 sombra-2 border" v-show="mostrarFormAddLista">
                 <h5 v-if="dadosLista.CodigoLista">Atualizar Lista</h5>
                 <h5 v-else>Adicioanr Lista</h5>
                 <div class="input-group mb-3 mt-3">
                   <div class="input-group-prepend">
                     <span id="basic-addon1" class="input-group-text">Nome da Lista</span>
                   </div>
                   <div class="form-control lh-40">
                     <input type="text" v-model="dadosLista.nomeLista" class="form-control">
                   </div>
                 </div>
                 <button @click="dadosLista.nomeLista = ''; mostrarFormAddLista = false" class="btn btn-outline-secondary">Cancelar</button>
                 <button @click="salvarlista()" class="btn btn-secondary">Salvar</button>
               </div>
               <!-- Fim formulario add listas  -->
               <button @click="mostrarFormAddLista = true" class="btn btn-success"><i class="fa fa-plus"></i> Adicionar Lista</button>
             </div>
             <!-- fim listas de paramestros -->

             <!-- configuração de Parametros  -->
             <div :class="mostrarListaParametrosMedicaoAgua ? 'tab-pane ml-3 fade show active' : 'tab-pane ml-3 fade'" id="parametros" role="tabpanel" aria-labelledby="profile-tab">
               <!-- lista de parametros sistema  -->
               <h5 class="mt-3" v-text="listaSelecionada.NomeLista"></h5>
               <div class="w-100 h450-r">
                 <table class="table table-striped" v-if="mostrarEquipamentosUtilizados === false">
                   <thead>
                     <tr>
                       <th>Parâmetro</th>
                       <th>Unidade</th>
                       <th>Tipo Parm.</th>
                       <th>Limite</th>
                       <th>Valor Limite</th>
                       <th>Metodologia de Análise</th>
                       <th>Equipamentos Utilizados</th>
                       <th>Observações</th>
                       <th>Ações</th>
                     </tr>
                   </thead>
                   <tbody>
                     <tr v-for="i in listaParamestrosLista">
                       <td v-text="i.NomeParametro"></td>
                       <td v-text="i.UnidadeMedida"></td>
                       <td v-text="i.TipoParametro"></td>
                       <td>

                         <template v-if="i.Limite">
                           <button v-if="i.NomeParametro != 'IQA'" @click="i.Limite = 0" class="btn btn-sm">{{i.Limite}}</button> 
                           <button v-else class="btn btn-sm">{{i.Limite}}</button> 
                         </template>
                         <template v-else>
                           <select @change="atualizarParametroDaLista(i.CodigoRecursosHidricosParametros, $event, 'Limite')" class="form-control">
                             <option value="Valor">Valor</option>
                             <option value="Fórmula">Fórmula</option>
                             <option value="Maior Que">Maior Que</option>
                             <option value="Menor Que">Menor Que</option>
                           </select>
                         </template>


                       </td>
                       <td>
                     
                          <input @blur="atualizarParametroDaLista(i.CodigoRecursosHidricosParametros, $event, 'ValorLimite')" :value="i.ValorLimite" type="text" class="form-control">

                       </td>
                       <td>      
                          <textarea @blur="atualizarParametroDaLista(i.CodigoRecursosHidricosParametros, $event, 'MetodologiaAnalise')" class="form-control">{{i.MetodologiaAnalise}}</textarea>
                       </td>
                       <td>
                         <button @click="iniciarEquipamentosParametro(i.NomeParametro, i.CodigoRecursosHidricosParametros)" class="btn bnt-outline-secondary"><i class="fa fa-list"></i></button>
                       </td>
                       <td>
                        
                         <input @blur="atualizarParametroDaLista(i.CodigoRecursosHidricosParametros, $event, 'ObservacoesEspecificas')" :value="i.ObservacoesEspecificas" type="text" class="form-control">
                       </td>
                       <td>
                         <button @click="deletarParametroLista(i.CodigoParametrosMedicaoAgua, i.CodigoLista)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>
                       </td>
                     </tr>
                   </tbody>
                 </table>

                 <!-- lista de equipamentos utilizados  -->

                 <button @click="mostrarEquipamentosUtilizados = false" class="btn btn-outline-secondary btn-r mr-4" v-if="mostrarEquipamentosUtilizados === true"><i class="fa fa-times"></i> Voltar para parâmetros</button>

                 <h4 v-text="'Equipamentos Utilizados para '+tituloParametro" v-if="mostrarEquipamentosUtilizados === true"></h4>


                 <div class="body todo_list p-3 mt-5 border" v-if="mostrarEquipamentosUtilizados">
                   <div class="row">
                     <div class="col-6">
                      <h5>Adicionar Equipamento</h5>
                       <!-- <select v-model="modelEquipamentoEscopo.CodigoRecurso" @change="adicionarEquipamentoParametro()" class="form-control show-tick ms select2">
                         <option v-for="i in listaEquipamentosNoEscopo" :value="i.CodigoRecurso" v-text="i.NomeEquipamento"></option>
                       </select> -->

                       <table class="table table-striped">
                         <tbody>
                         <tr v-for="i in listaEquipamentosNoEscopo">
                           <td>{{i.NomeEquipamento}} </td>
                           <td> <span @click="modelEquipamentoEscopo.CodigoRecurso = i.CodigoRecurso; adicionarEquipamentoParametro()" class="badge badge-success badge-pill"><i class="fa fa-check"></i></span></td>
                         </tr>
                         </tbody>
                        
                       </table>


                     </div>
                     <div class="col-6">
                     <h5>Equipamentos Selecionados</h5>
                     <ul class="list-group">
                     <li class="list-group-item d-flex justify-content-between align-items-center" v-for="i in listaEquipamentos">
                       <img :src="i.Foto" class="img-w100">
                       {{i.NomeEquipamento}}
                       <span @click="deletarEquipamentoNoParametro(i.CodigoRecursosHidricosEquipamentos)" class="badge badge-primary badge-pill"><i class="fa fa-times"></i></span>
                     </li>

                   </ul>
                     </div>
                   </div>
                   
                 </div>
               </div>



               <!-- Fim da lista de equipamentos utilizados  -->
               <button v-if="mostrarEquipamentosUtilizados === false" @click="mostrarParametrosSistema = true" class="btn btn-success"><i class="fa fa-plus"></i> Adicionar Parâmetros</button>

             </div>
             <!-- fim lista de parametros sistema  -->

             <div class="desc-programas fix-20 left-1 sombra-2 p-5" v-if="mostrarParametrosSistema">
               <div class="row mb-5 border-bottom">
                 <div class="col-12 col-sm-7">
                   <h5>Selecione os parâmetros desejados</h5>
                   <div class="w-100 h450-r">
                     <table class="table table-striped" id="listaPontosExcel">
                       <thead>
                         <tr>
                           <th>Parametro</th>
                           <th>Unidade</th>
                           <th>Tipo Parâmetro</th>
                           <th>Selecionar</th>

                         </tr>
                       </thead>
                       <tbody>

                         <tr v-for="(i, index) in listaParamestros">
                           <td v-text="i.NomeParametro"></td>
                           <td v-text="i.UnidadeMedida"></td>
                           <td v-text="i.TipoParametro"></td>
                           <th>
                             <button @click="setParametroPontosLista(i.CodigoParametrosMedicaoAgua, i.NomeParametro, index, 'parametro')" class="btn btn-outline-secondary"><i class="fa fa-check"></i></button>
                           </th>
                         </tr>
                       </tbody>
                     </table>
                   </div>
                 </div>
                 <div class="col-12 col-sm-5 border-left">
                   <h5>Parâmetros Selecionados</h5>
                   <div class="w-100 h450-r">
                     <table class="table table-striped" id="listaPontosExcel">
                       <thead>
                         <tr>
                           <th>Parâmetro</th>
                           <th>-</th>

                         </tr>
                       </thead>
                       <tbody>

                         <tr v-for="(i, index) in listaParamestrosSelecionados">
                           <td v-text="i.NomeParametro"></td>
                           <td><button @click="removerParametroPontosLista(index, 'parametro')" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
                         </tr>
                       </tbody>
                     </table>
                   </div>
                 </div>
               </div>
               <button @click="mostrarParametrosSistema = false" class="btn btn-outline-secondary  mr-4"><i class="fa fa-times"></i> Cancelar</button>
               <button @click="salvarParametrosNaLista()" class="btn btn-default btn-r mr-4"><i class="fa fa-save"></i> Salvar Parâmetros</button>

             </div>




             <!-- fim configuração de Parametros  -->

             <!-- assossiar pontos  -->
             <!-- <div :class="mostrarSelecaoListaPontos ? 'tab-pane ml-3 fade show active' : 'tab-pane ml-3 fade'" id="pontos" role="tabpanel" aria-labelledby="contact-tab"> -->
             <div class="tab-pane ml-3 fade" id="pontos" role="tabpanel" aria-labelledby="contact-tab">
               <!-- lista de pontos de coleta  -->
               <h5 class="mt-3" v-text="listaSelecionada.NomeLista"></h5>
               <div class="w-100 h450-r">


                 <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                   <thead>
                     <tr>
                       <th>Código Ponto</th>
                       <th>Nome de Ponto de Coleta</th>
                       <th>Class 01</th>
                       <th>Class 02</th>
                       <th>Class 03</th>
                       <th>Class 04</th>
                       <th>Origem da massa d'água</th>
                       <th>Tipo de massa d'água</th>
                       <th>Clase Hídrica</th>
                       <th>Tipo de Amostra</th>
                       <th>Tipo de Ambiente</th>
                       <th>UF</th>
                       <th>Município</th>
                       <th>Bacia</th>
                       <th>Km I</th>
                       <th>Km F</th>
                       <th>Estaca</th>
                       <th>ações</th>
                     </tr>
                   </thead>
                   <tbody>
                     <tr v-for="i in listaPontosColetaDaLista">
                       <td>PT{{i.CodigoConfiguracaoRecursosHidricosPontoColeta}}</td>
                       <td v-text="i.NomePontoColeta"></td>
                       <td v-text="i.Classe1"></td>
                       <td v-text="i.Classe2"></td>
                       <td v-text="i.Classe3"></td>
                       <td v-text="i.Classe4"></td>
                       <td v-text="i.MassaAgua"></td>
                       <td v-text="i.TipoMassaAgua"></td>
                       <td v-text="i.ClasseHidrica"></td>
                       <td v-text="i.TipoAmostra"></td>
                       <td v-text="i.TipoAmbiente"></td>
                       <td v-text="i.UF"></td>
                       <td v-text="i.Municipio"></td>
                       <td v-text="i.Bacia"></td>
                       <td v-text="i.KmInicial"></td>
                       <td v-text="i.KmFinal"></td>
                       <td v-text="i.Estaca"></td>
                       <td>
                               <button @click="deletarPontoLista(i.CodigoLista, i.CodigoConfiguracaoRecursosHidricosPontoColeta)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>
                             
                       </td>
                     </tr>

                   </tbody>
                 </table>
               </div><!-- fim lista de pontos de coleta  -->


               <!-- lista de pontos de coleta cadastrados no programa  -->
               <div class="desc-programas fix-20 left-1 sombra-2 p-5" v-if="mostrarPontosSistema">
                 <div class="row mb-5 border-bottom">
                   <div class="col-12 col-sm-6">
                     <h5>Selecione os pontos de coleta desejados</h5>
                     <div class="w-100 h450-r">
                       <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                         <thead>
                           <tr>
                             <th><i class="fa fa-check"></i></th>
                             <th>Código Ponto</th>
                             <th>Nome de Ponto de Coleta</th>
                             <th>Class 01</th>
                             <th>Class 02</th>
                             <th>Class 03</th>
                             <th>Class 04</th>
                             <th>Origem da massa d'água</th>
                             <th>Tipo de massa d'água</th>
                             <th>Clase Hídrica</th>
                             <th>Tipo de Amostra</th>
                             <th>Tipo de Ambiente</th>
                             <th>UF</th>
                             <th>Município</th>
                             <th>Bacia</th>
                             <th>Km I</th>
                             <th>Km F</th>
                             <th>Estaca</th>

                           </tr>
                         </thead>
                         <tbody>

                           <tr v-for="(i, index) in listaPontosColetaParametros">
                             <td>
                               <button @click="setParametroPontosLista(i.CodigoConfiguracaoRecursosHidricosPontoColeta+' - '+i.NomePontoColeta, index, 'ponto')" class="btn btn-success"><i class="fa fa-check"></i></button>
                             </td>
                             <td>PT{{i.CodigoConfiguracaoRecursosHidricosPontoColeta}}</td>
                             <td v-text="i.NomePontoColeta"></td>
                             <td v-text="i.Classe1"></td>
                             <td v-text="i.Classe2"></td>
                             <td v-text="i.Classe3"></td>
                             <td v-text="i.Classe4"></td>
                             <td v-text="i.MassaAgua"></td>
                             <td v-text="i.TipoMassaAgua"></td>
                             <td v-text="i.ClasseHidrica"></td>
                             <td v-text="i.TipoAmostra"></td>
                             <td v-text="i.TipoAmbiente"></td>
                             <td v-text="i.UF"></td>
                             <td v-text="i.Municipio"></td>
                             <td v-text="i.Bacia"></td>
                             <td v-text="i.KmInicial"></td>
                             <td v-text="i.KmFinal"></td>
                             <td v-text="i.Estaca"></td>

                           </tr>

                         </tbody>
                       </table>
                     </div>
                   </div>
                   <div class="col-12 col-sm-6 border-left">
                     <h5>Pontos Selecionados</h5>
                     <div class="w-100 h450-r">
                       <table class="table table-striped" id="listaPontosExcel">
                         <thead>
                           <tr>
                             <th>Código Ponto</th>
                             <th>Nome do Ponto</th>
                             <th>-</th>

                           </tr>
                         </thead>
                         <tbody>
                           <tr v-for="(i, index) in listaPontosSelecionados">
                             <td>PT{{i.CodigoConfiguracaoRecursosHidricosPontoColeta}}</td>
                             <td v-text="i.NomePontoColeta"></td>
                             <td><button @click="removerParametroPontosLista(index, 'ponto')" class="btn btn-danger"><i class="fa fa-times"></i></button></td>
                           </tr>
                         </tbody>
                       </table>
                     </div>
                   </div>
                 </div>
                 <button @click="mostrarPontosSistema = false" class="btn btn-outline-secondary  mr-4"><i class="fa fa-times"></i> Cancelar</button>
                 <button @click="salvarPontosNaLista()" class="btn btn-default btn-r mr-4"><i class="fa fa-save"></i> Salvar Pontos</button>

               </div>
               <!-- lista de pontos de coleta cadastrados no programa  -->
               <button @click="listaPontosSelecionados = []; mostrarPontosSistema = true" class="btn btn-success"><i class="fa fa-plus"></i> Adicionar Pontos</button>
             </div>
             <!-- assossiar pontos  -->

           </div>
         </div>
       </div>
       <div class="modal-footer">
         <button data-dismiss="modal" aria-label="Close" class="btn btn-default btn-round waves-effect">Fechar</button>
       </div>
     </div>
   </div>
   </div> <!-- modal Adicionar mais paramestros de agua  -->