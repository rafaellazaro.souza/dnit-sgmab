<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }

    .borderRed {
        border: 1px solid red
    }

    .borderGreen {
        border: 1px solid green
    }

    .nav-tabs>.nav-item>.nav-link {
        border: 1px solid #dfdfdf !important;
        border-radius: 0px;
    }

    .nav-tabs>.nav-item>.nav-link.active {
        border: 1px solid #dfdfdf !important;
        border-radius: 0px;
        background-color: #e2e2e2;
        color: #6d6d6d !important;
        font-weight: bold;
    }

    th {
        text-align: center
    }
</style>

<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Configuração Monitoramento de Fauna</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Home</li>
                    <li class="breadcrumb-item active">Contratos</li>
                    <li class="breadcrumb-item active">Dados do Contrato</li>
                    <li class="breadcrumb-item active">Execução de Programas Ambientais - Lista de Condicionantes</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>

    <div class="body_scroll" id="faunaModulos">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div class="row">
                                <div class="col-md-9">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="monitoramento-tab" data-toggle="tab" href="#monitoramento-fauna" role="tab" aria-controls="passagem-tab" aria-selected=" true">Monitoramento de Fauna</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="parecer-tab" data-toggle="tab" href="#parecer-fiscal" role="tab" aria-controls="parecer-tab" aria-selected="true">Parecer</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 d-flex align-items-center flex-row-reverse">
                                    <button class="btn btn-sm btn-success d-flex align-items-center" v-if="!isEmValidacaoOuAprovado && !isFiscal" @click="solicitarValidacao()">Enviar para Validação</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="monitoramento-fauna" role="tabpanel" aria-labelledby="monitoramento-tab">
                                            <div class="card">
                                                <!-- Formulario Adicionar/Editar Modulos -->
                                                <div class="modal-w98-h50 sombra-2 p-5 w-50" v-show="mostrarAddFaunaModulos">
                                                    <div class="col-12">
                                                        <button @click="mostrarAddFaunaModulos = false; dadosFaunaModulos = {}; erros = 0" class="btn btn-secondary btn-r"><i class="fa fa-times"></i></button>
                                                    </div>

                                                    <h5 class="w-100 text-left mb-3">Adicionar Modulo </h5>
                                                    <div class="row mt-3">
                                                        <div class="alert alert-warning p-3 text-left w-100" v-show="erros.length">
                                                            <b>Por Favor Preencha o(s) campo(s) abaixo:</b>
                                                            <ul>
                                                                <li v-for="i in erros" v-text="i"></li>
                                                            </ul>
                                                        </div>

                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">Nome Modulo Amostragem</span>
                                                            </div>
                                                            <input type="text" v-model="dadosFaunaModulos.NomeModuloAmostragem" class="form-control" placeholder="Digite o Nome Modulo Amostragem" require>
                                                        </div>

                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">Tipo Modulo</span>
                                                            </div>
                                                            <input type="text" v-model="dadosFaunaModulos.TipoModulo" class="form-control" placeholder="Digite o Tipo Modulo" require>
                                                        </div>

                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">Área</span>
                                                            </div>
                                                            <input type="number" v-model="dadosFaunaModulos.Area" class="form-control" placeholder="Digite a Área - apenas números" require>
                                                        </div>

                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">Descrição Fitofisionomia</span>
                                                            </div>
                                                            <textarea type="text" v-model="dadosFaunaModulos.DescricaoFitoFisionomia" class="form-control" placeholder="Digite a Fitofisionomia" require></textarea>
                                                        </div>

                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">Município</span>
                                                            </div>
                                                            <input type="text" v-model="dadosFaunaModulos.Municipio" class="form-control" placeholder="Digite o Município" require>
                                                        </div>

                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">Observações</span>
                                                            </div>
                                                            <textarea type="text" v-model="dadosFaunaModulos.Observacoes" class="form-control" placeholder="Digite as Observações" require></textarea>
                                                        </div>
                                                        <div class="col-12 text-right">
                                                            <button @click="mostrarAddFaunaModulos = false; dadosFaunaModulos = {}; erros = []" class="btn btn-outline-secondary">Cancelar</button>
                                                            <button type="button" @click="checkForm(1)" id="btn-salvar-profissional" class="btn btn-secondary ml-2">Salvar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- FIM do Formulario Adicionar/Editar Modulos -->
                                                <div class="header">
                                                    <h2><strong>Modulos</strong> Fauna</h2>
                                                </div>
                                                <div class="body" style="min-height: 100px;">
                                                    <button v-if="!isEmValidacaoOuAprovado" @click="mostrarAddFaunaModulos = true" class="btn btn-secondary"><i class="fa fa-plus"></i>
                                                        Adicionar Modulo</button>
                                                    <table class="table table-striped" id="listadeModulos">
                                                        <thead>
                                                            <th>Nome Modulo</th>
                                                            <th>Tipo de Modulo </th>
                                                            <th>Área</th>
                                                            <th>Descrição Fitofisionomia</th>
                                                            <th>Município</th>
                                                            <th>Observações</th>
                                                            <th>Data de Cadastro</th>
                                                            <th>Ação</th>
                                                        </thead>
                                                        <tbody>

                                                            <tr v-for="(i, index) in listaFaunaModulos">
                                                                <td v-text="i.NomeModuloAmostragem"></td>
                                                                <td v-text="i.TipoModulo"></td>
                                                                <td v-text="i.Area"></td>
                                                                <td v-text="i.DescricaoFitoFisionomia"></td>
                                                                <td v-text="i.Municipio"></td>
                                                                <td v-text="i.Observacoes"></td>
                                                                <td v-text="i.DataCadastro"></td>
                                                                <td>
                                                                    <button v-if="!isEmValidacaoOuAprovado" @click="deletar(i.CodigoModulo, index)" class="btn btn-outline-danger btn-sm"><i class="fa fa-times"></i></button>
                                                                    <a :href="'<?= base_url('ConfiguracaoProgramas/recursosFauna/?codigoModulo=') ?>'+ vmGlobal.codificarDecodificarParametroUrl(i.CodigoModulo, 'encode')+'&codigoPrograma='+vmGlobal.codificarDecodificarParametroUrl(i.CodigoEscopoPrograma, 'encode')+'&comeModulo='+vmGlobal.codificarDecodificarParametroUrl(i.NomeModuloAmostragem, 'encode')" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="parecer-fiscal" role="tabpanel" aria-labelledby="parecer-tab">
                                            <?php $this->load->view('cgmab/ConfiguracaoProgramas/paginas/parecerTab.php') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('cgmab/ConfiguracaoProgramas/scripts/appRecursosFaunaModulos') ?>