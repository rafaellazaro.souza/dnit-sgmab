<div class="card">
    <div class="body">
        <div class="p-4" id="parecerFiscal">
            <h4 class="w-100 mb-4 border p-3">Parecer do Fiscal</h4>
            <div class="row" v-if="habilitarParecer">
                <div class="col-md-10">
                    <div class="input-group mb-3" v-if="showCorrecao">
                        <div class="input-group-prepend">
                            <span id="basic-addon1" class="input-group-text">Prazo de Correção</span>
                        </div>
                        <input type="date" class="form-control  not-hide" :min="toDayDate" v-model="parecer.PrazoCorrecao" required>
                    </div>
                    <div class="form-group">
                        <div class="form-line">
                            <label for="">Parecer Técnico</label>
                            <textarea rows="4" class="form-control no-resize" id="textParecer"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <br>
                    <div class="btn-group-vertical">
                        <button class="btn btn-sm btn-success not-hide" @click="salvarParecer(4)" type="button">Aprovado</button>
                        <button class="btn btn-sm btn-rejeitado mt-2 not-hide" type="button" @click="salvarParecer(5)">Parcialmente Aprovado</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-sm table-striped table-hover compact" style="width: 100%;" id="tblParecerFiscal">
                        <thead>
                            <tr>
                                <th>Situação</th>
                                <th>Envio para Validação</th>
                                <th>Data Parecer</th>
                                <th>Prazo de Correção</th>
                                <th>Prazo de Correção Anterior</th>
                                <th>-</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="par in listaParecer">
                                <td> <span class="badge " :class="getClasse(par.StatusFiscal)"> {{getStatusDescricao(par.StatusFiscal)}} </span></td>
                                <td>{{formataData(par.DataEnvio)}}</td>
                                <td>{{formataData(par.DataParecer)}}</td>
                                <td>{{formataData(par.PrazoCorrecao)}}</td>
                                <td>{{formataData(par.AntigaDataCorrecao)}}</td>
                                <td>
                                    <button class="btn btn-sm btn-outline-secondary" @click="loadParecer(par)"><i class="zmdi zmdi-eye"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row mt-2" v-if="showParecer">
                <div class="col-md-12 border border-secondary">
                    <button class="float-right btn btn-sm btn-primary" @click="showParecer=false">x</button>
                    <div class="form-group mt-2">
                        <p class="p-0 m-0"><strong>Situação:</strong> <span class="badge " :class="getClasse(selectedParecer.StatusFiscal)">{{getStatusDescricao(selectedParecer.StatusFiscal)}}</span></p>
                        <p class="p-0 m-0"><strong>Fiscal:</strong> {{selectedParecer.NomeUsuario}}</p>
                        <p class="p-0 m-0"><strong>Data do Parecer:</strong> {{selectedParecer.DataParecer}}</p>
                        <p class="p-0 m-0"><strong>Data do Envio:</strong> {{selectedParecer.DataEnvio}}</p>
                        <p class="p-0 m-0"><strong>Prazo de Correção:</strong> {{selectedParecer.PrazoCorrecao}}</p>
                        <p class="p-0 m-0"><strong>Prazo de Correção Anterior:</strong> {{selectedParecer.AntigaDataCorrecao}}</p>
                        <label for="">Parecer Técnico:</label>
                        <textarea class="form-control" rows="4" id="txtParecerTecnicoView"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>