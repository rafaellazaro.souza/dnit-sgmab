<div class="row">
    <div class="col-lg-12">
        <div class="card" v-show="mostrarListaEstruturas">
            <div class="header pl-4 pr-4">
                <h2 class="card-title"><strong>Passagem</strong> Fauna</h2>
            </div>
            <div style="min-height: 100px;" class="body">
                <!-- <div class="btn-group" role="group" aria-label="Grupo de botões com dropdown aninhado">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-filter"></i> Filtrar Pontos de Coleta
                                </button>
                                <div class="dropdown-menu" style="left: -112px;" aria-labelledby="btnGroupDrop1">
                                    <a class="dropdown-item" href="javascript:;" @click="NomePontoColeta = i" v-for="i in listaPontosColetaFiltro" v-text="i"></a>
                                    <a class="dropdown-item" href="javascript:;" @click="NomePontoColeta = 'sem-foto'">Sem Fotos</a>
                                    <a class="dropdown-item" href="javascritp:window.location.reload();" @click="NomePontoColeta = 'sem-foto'">Resetar Filtro</a>
                                </div>
                            </div>
                        </div> -->

                <table class="table table-striped" id="ListaAreasApoios">
                    <thead>
                        <tr>

                            <th>Código Estrutura</th>
                            <th>Estrutura</th>
                            <th>Tipo Estrutura</th>
                            <th>Nome</th>
                            <th>UF</th>
                            <th>BR</th>
                            <th>Tipo Trecho</th>
                            <th>KM Inicial</th>
                            <th>KmFinal</th>
                            <th>Estaca</th>
                            <th>Obs</th>
                            <th>Dimensões</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(i, index) in listaAreasApoio">

                            <td v-text="i.CodigoAreaApoio"></td>
                            <td v-text="i.Estrutura"></td>
                            <td v-text="i.TipoEstrutura"></td>
                            <td v-text="i.Nome"></td>
                            <td v-text="i.UF"></td>
                            <td v-text="i.BR"></td>
                            <td v-text="i.TipoTrecho"></td>
                            <td v-text="i.KmInicial"></td>
                            <td v-text="i.KmFinal"></td>
                            <td v-text="i.NumeroEstaca"></td>
                            <td v-text="i.Observacao"></td>
                            <td>
                                <template v-if="i.Diametro">
                                    Diametro: {{i.Diametro}}
                                </template>
                                <template v-else>
                                    Altura: {{i.Altura}} <br /> Largura: {{i.Largura}}
                                </template>
                            </td>
                            <td>
                                <button @click="iniciarArmadilhas(i)" class="btn btn-sm" title="Listar Armadilha"><i class="fa fa-eye"></i></button>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card" v-show="mostrarAddFotos">
            <div class="header pl-4 pr-4">
                <h2><strong>{{totalFotos}}</strong> fotos <i class="fas fa-images"></i></h2>
            </div>
            <div class="body">
                <button @click="fecharArmadilhas()" class="btn btn-info btn-r"><i class="fas fa-arrow-left"></i> Selenionar outro ponto</button>
                <h4>Dados da estrutura selecionada</h4>
                <table class="table table-striped" id="ListaAreasApoiosSelecionada">
                    <thead>
                        <tr>
                            <th>Código Estrutura</th>
                            <th>Estrutura</th>
                            <th>Tipo Estrutura</th>
                            <th>Nome</th>
                            <th>UF</th>
                            <th>BR</th>
                            <th>Tipo Trecho</th>
                            <th>KM Inicial</th>
                            <th>KmFinal</th>
                            <th>Estaca</th>
                            <th>Obs</th>
                            <th>Dimensões</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(i, index) in listaEstruturaSelecionada">

                            <td v-text="i.CodigoAreaApoio"></td>
                            <td v-text="i.Estrutura"></td>
                            <td v-text="i.TipoEstrutura"></td>
                            <td v-text="i.Nome"></td>
                            <td v-text="i.UF"></td>
                            <td v-text="i.BR"></td>
                            <td v-text="i.TipoTrecho"></td>
                            <td v-text="i.KmInicial"></td>
                            <td v-text="i.KmFinal"></td>
                            <td v-text="i.NumeroEstaca"></td>
                            <td v-text="i.Observacao"></td>
                            <td>
                                <template v-if="i.Diametro">
                                    Diametro: {{i.Diametro}}
                                </template>
                                <template v-else>
                                    Altura: {{i.Altura}} <br /> Largura: {{i.Largura}}
                                </template>
                            </td>


                        </tr>
                    </tbody>
                </table>


            </div>

            <div class="row" v-show="mostrarArmadilhas">

                <div class="col-12 col-sm-6 mt-4 pl-4 pr-4">
                    <h4>Armadilha cadastradas</h4>

                </div>
                <div class="col-12 col-sm-6 text-right mt-5" v-if="!isEmValidacaoOuAprovado">
                    <button @click="mostrarFormAddArmadilhas = !mostrarFormAddArmadilhas" class="btn btn-success"><i class="fa fa-plus"></i> Adicionar Armadilhas</button>
                </div>
                <div class="alert alert-warning p-4 text-center col-12" v-if="listaArmadilhas.length == 0">
                    <p>Nenhuma armadilha foi cadastrada. Clique em "+ Adicionar Armadilhas", para adicionar!</p>
                </div>

            </div>
            <div class="row file_manager pl-2 pr-2 ">

                <div v-for="i in listaArmadilhas" class="col-lg-4 col-md-4 col-sm-12" v-if="i.Caminho">
                    <div class="card " :class="[i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000' ? 'borderRed' : 'borderGreen']">
                        <div class="file">
                            <div class="hover">
                                <button :disabled="isEmValidacaoOuAprovado" @click="deletarFotos(i.CodigoConfiguracaoPassagemFaunaArmadilha, i.Caminho)" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                            </div>
                            <div class="image">
                                <img :src="i.Caminho" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
                            </div>
                            <div class="file-name">
                                <small>Codigo Armadilha<span class="date text-info"><b>{{i.CodigoConfiguracaoPassagemFaunaArmadilha}}</b></span></small>
                                <small v-if="i.Lat != ''">Latitude<span class="date text-info">{{i.Lat}}</span></small>
                                <small v-else>Latitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                <small v-if="i.Lng != ''">Longitude<span class="date text-info">{{i.Long}}</span></small>
                                <small v-else>Longitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                <small v-if="i.DataFoto != '1900-01-01 00:00:00.000'">Data da fotografia <span class="date text-info">{{i.DataFoto}}</span></small>
                                <small v-else>Data da fotografia<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                            </div>
                            <div class="file-name">
                                <label cla>UTM (Data Sirgas 2000)</label><br>
                                <div class="row">
                                    <div class=" col-md-3">
                                        <label for="email_address">Zona</label>
                                        <div class="form-group">
                                            <template v-if="i.Zona">
                                                <div class="form-control">{{i.Zona}}</div>
                                            </template>
                                            <input v-else :disabled="isEmValidacaoOuAprovado" class="form-control" @blur="setDadosUpdate('Zona', $event)" placeholder="">
                                        </div>
                                    </div>
                                    <div class=" col-md-3">
                                        <label for="email_address">CN</label>
                                        <div class="form-group  ">
                                            <template v-if="i.CN">
                                                <div class="form-control">{{i.Elevacao}}</div>
                                            </template>
                                            <input v-else :disabled="isEmValidacaoOuAprovado" class="form-control" @blur="setDadosUpdate('CN', $event)" placeholder="">
                                        </div>
                                    </div>
                                    <div class=" col-md-3">
                                        <label for="email_address">CE</label>
                                        <div class="form-group  ">
                                            <template v-if="i.CE">
                                                <div class="form-control">{{i.CE}}</div>
                                            </template>
                                            <input v-else :disabled="isEmValidacaoOuAprovado" class="form-control" @blur="setDadosUpdate('CE', $event)" placeholder="">
                                        </div>
                                    </div>
                                    <div class=" col-md-3">
                                        <label for="email_address  ">Elevação (m)</label>
                                        <div class="form-group ">
                                            <template v-if="i.Elevacao">
                                                <div class="form-control">{{i.Elevacao}}</div>
                                            </template>
                                            <input v-else :disabled="isEmValidacaoOuAprovado" class="form-control" @blur="setDadosUpdate('Elevacao', $event, true, i.CodigoConfiguracaoPassagemFaunaArmadilha)" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="file-name">
                                <label for="email_address">Equipamentos</label>
                                <template v-if="i.CodigoRecurso">
                                    <table class="table-striped">
                                        <tbody>
                                            <tr>
                                                <td class="w-25">
                                                    <img :src="i.Foto" class="img-responsive" alt="">
                                                <td>
                                                <td>{{i.NomeEquipamento}}
                                                <td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </template>
                                <template>
                                    <select :disabled="isEmValidacaoOuAprovado" @change="setDadosUpdate('CodigoRecurso', $event, true, i.CodigoConfiguracaoPassagemFaunaArmadilha)" class="form-control mt-3">
                                        <option value="">Selecione um equipamento</option>
                                        <option :value="i.CodigoRecurso" v-for="i in ListaEquipamentos" v-text="i.NomeEquipamento"></option>
                                    </select>
                                </template>


                            </div>
                            <div class="file-name">
                                <label for="email_address">Observações</label>
                                <div class="form-group">
                                    <template v-if="i.ObservacaoFoto">
                                        <div class="form-control pb-4">{{i.Observacao}}</div>
                                    </template>
                                    <textarea v-else :disabled="isEmValidacaoOuAprovado" rows="4" @blur="setDadosUpdate('Observacao', $event, true, i.CodigoConfiguracaoPassagemFaunaArmadilha)" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="file-name">
                                <!-- <button v-if="i.Lat != '' && i.DataFoto != '1900-01-01 00:00:00.000'" class="btn btn-success">Salvar</button> -->
                                <button :disabled="isEmValidacaoOuAprovado" @click="deletarFotos(i.CodigoConfiguracaoPassagemFaunaArmadilha , i.Caminho)" class="btn btn-danger">Excluir</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- modal equipamentos cadastrados no serviço  -->
    <div class="modal-w98-h50 sombra-2 p-5" v-show="mostrarFormAddArmadilhas">
        <div class="row">
            <div class="col-12">
                <form enctype="multipart/form-data" method="POST">

                    <input type="file" id="imagemMaterial" name="fotos[]" class="dropify " multiple>

                </form>
            </div>



            <div class="col-12 border-top text-right mt-4">
                <button @click="mostrarFormAddArmadilhas = false" class="btn">Cancelar</button>
                <button class="btn btn-primary float-right" @click="addFotos()">Adicionar</button>

            </div>
        </div>

    </div>
    <!-- modal equipamentos cadastrados no serviço  -->
</div>