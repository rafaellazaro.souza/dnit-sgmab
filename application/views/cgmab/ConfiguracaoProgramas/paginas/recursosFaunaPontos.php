<?php $this->load->view('cgmab/ConfiguracaoProgramas/css/recursosFaunaPontos') ?>

<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Configuração Monitoramento de Fauna</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Home</li>
                    <li class="breadcrumb-item active">Contratos</li>
                    <li class="breadcrumb-item active">Dados do Contrato</li>
                    <li class="breadcrumb-item active">Execução de Programas Ambientais - Lista de Condicionantes</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card" id="fotos">
                    <div class="body">
                        <div class="row mb-1">
                            <div class="col-12">
                                <h4 class="w-100 mb-4 border p-3">Configuração de Monitoramento de Fauna Inserção de Armadilhas - {{NomeModuloAmostragem}}</h4>
                            </div>
                        </div>
                        <div class="card" v-if="!isEmValidacaoOuAprovado">
                            <div class="header">
                                <h2><strong>{{totalFotos}}</strong> Armadilha <i class="fas fa-images"></i></h2>
                            </div>
                            <div class="body no-shadow border">
                                <form enctype="multipart/form-data" method="POST">
                                    <input type="file" id="imagemMaterial" @change="qtdFotos" name="fotos[]" class="dropify " multiple>
                                </form>
                            </div>
                            <button class="btn btn-outline-secondary mt-2 float-right" @click="verificaFotos">IMPORTAR</button>
                        </div>

                        <div class="row file_manager ">
                            <div v-for="(i, index) in listaMonitoramentoFauna" :key="index" class="col-lg-4 col-md-4 col-sm-12">
                                <div class="" :class="[i.Lat == '' || i.Lat == null ? 'borderRed' : 'borderGreen']">
                                    <div class="file">
                                        <div class="hover">
                                            <button :disabled="isEmValidacaoOuAprovado" @click="deletarFotos(i.CodigoConfiguracaoMonitoramentoFaunaFotos, i.Caminho)" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger">
                                                <i class="zmdi zmdi-delete"></i>
                                            </button>
                                        </div>
                                        <div class="image">
                                            <img :src="i.Caminho" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
                                        </div>
                                        <div class="file-name d-flex justify-content-between">
                                            <label for="email_address">ID - Armadilha</label>
                                            <span class="date text-info"><b>{{i.CodigoConfiguracaoMonitoramentoFaunaFotos}}</b></span>
                                        </div>
                                        <div class="file-name">
                                            <small>Nome do Ponto Monitoramento<span class="date text-info"><b>{{i.NomePontoMonitoramento | singleName}}</b></span></small>
                                            <small>Nome da Armadilha<span class="date text-info"><b>{{i.NomeFoto | singleName}}</b></span></small>
                                            <small v-if="i.Lat != ''">Latitude<span class="date text-info">{{i.Lat}}</span></small>
                                            <small v-else>Latitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                            <small v-if="i.Lng != ''">Longitude<span class="date text-info">{{i.Lng}}</span></small>
                                            <small v-else>Longitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                            <small v-if="i.DataFoto != '1900-01-01 00:00:00.000'">Data da fotografia <span class="date text-info">{{i.DataFoto}}</span></small>
                                            <small v-else>Data da fotografia<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                        </div>
                                        <div class="file-name">
                                            <label cla>UTM (Data Sirgas 2000)</label><br>
                                            <div class="row">
                                                <div class=" col-md-3">
                                                    <label for="email_address">Zona</label>
                                                    <div class="form-group">
                                                        <input v-if="i.Lng == '' && i.Lat == ''" disabled class="form-control">
                                                        <input v-else-if="i.Zona != null" class="form-control" disabled :placeholder="i.Zona">
                                                        <input :disabled="isEmValidacaoOuAprovado" v-else class="form-control" v-model="i.modelDadosFotosInformacao['Zona']">
                                                    </div>
                                                </div>
                                                <div class=" col-md-3">
                                                    <label for="email_address">CN</label>
                                                    <div class="form-group">
                                                        <input v-if="i.Lng == '' && i.Lat == ''" disabled class="form-control">
                                                        <input v-else-if="i.CN != null" class="form-control" disabled :placeholder="i.CN">
                                                        <input :disabled="isEmValidacaoOuAprovado" v-else class="form-control" v-model="i.modelDadosFotosInformacao['CN']">
                                                    </div>
                                                </div>
                                                <div class=" col-md-3">
                                                    <label for="email_address">CE</label>
                                                    <div class="form-group">
                                                        <input v-if="i.Lng == '' && i.Lat == ''" disabled class="form-control">
                                                        <input v-else-if="i.CE != null" class="form-control" disabled :placeholder="i.CE">
                                                        <input :disabled="isEmValidacaoOuAprovado" v-else class="form-control" v-model="i.modelDadosFotosInformacao['CE']">
                                                    </div>
                                                </div>
                                                <div class=" col-md-3">
                                                    <label for="email_address">Elevação(m)</label>
                                                    <div class="form-group">
                                                        <input v-if="i.Lng == '' && i.Lat == ''" disabled class="form-control">
                                                        <input v-else-if="i.Elevacao != null" class="form-control" disabled :placeholder="i.Elevacao">
                                                        <input :disabled="isEmValidacaoOuAprovado" v-else class="form-control" v-model="i.modelDadosFotosInformacao['Elevacao']">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="file-name">
                                            <label for="email_address">Equipamento Técnico (Armadilha)</label>
                                            <div v-if="i.Lng == '' && i.Lat == ''" class="form-group">
                                                <input disabled rows="1" class="form-control" value="">
                                            </div>
                                            <div v-else-if="i.CodigoRecurso == null" class="custom-file">
                                                <select :disabled="isEmValidacaoOuAprovado" v-model="i.modelDadosFotosInformacao.CodigoRecurso" class="form-control show-tick ms select2" @change="getEquipamentos($event)">
                                                    <option v-for="t in listaEquipamentos" :value="t.CodigoRecurso">{{t.NomeEquipamento}}</option>
                                                </select>
                                            </div>
                                            <div v-else-if="i.CodigoRecurso != null">
                                                <option disabled rows="1" class="form-control">{{buscaNomeEquipamento(listaEquipamentos, i.NomeEquipamento)}}</option>
                                            </div>
                                        </div>
                                        <div class="file-name">
                                            <label for="email_address">Grupo Faunístico</label>
                                            <div v-if="i.Lng == '' && i.Lat == ''" class="form-group">
                                                <input disabled rows="1" class="form-control" value="">
                                            </div>
                                            <div v-else-if="i.CodigoGrupoFaunistico == null" class="custom-file">
                                                <select :disabled="isEmValidacaoOuAprovado" v-model="i.modelDadosFotosInformacao.CodigoGrupoFaunistico" class="form-control show-tick ms select2" @change="getGruposFaunisticos($event)">
                                                    <option v-for="k in listaGrupoFaunistico" :value="k.CodigoGrupoFaunistico">{{k.NomeGrupoFaunistico}}</option>
                                                </select>
                                            </div>
                                            <div v-if="i.CodigoRecurso != null">
                                                <option disabled rows="1" class="form-control">{{buscaGrupoFaunitico(listaGrupoFaunistico, i.CodigoGrupoFaunistico)}}</option>
                                            </div>
                                        </div>
                                        <div class="file-name">
                                            <label for="email_address">Fotografia Observações</label>
                                            <div class="form-group">
                                                <textarea v-if="i.ObservacoesFoto != null" rows="4" disabled class="form-control" :placeholder="i.ObservacoesFoto"></textarea>
                                                <textarea v-else-if="i.ObservacoesFoto == null && i.CodigoGrupoFaunistico != null" disabled rows="4" class="form-control" placeholder="Foto salvo sem observações"></textarea>
                                                <textarea :disabled="isEmValidacaoOuAprovado" v-else-if="i.Lat != '' && i.DataFoto != '1900-01-01 00:00:00.000' " rows="4" v-model="i.modelDadosFotosInformacao.ObservacoesFoto" class="form-control"></textarea>
                                                <textarea v-else rows="4" disabled class="form-control" placeholder="A foto não está de acordo com o manual"></textarea>
                                            </div>
                                        </div>
                                        <div v-bind:id="'error-'+index">

                                        </div>
                                        <div class="file-name">
                                            <button :disabled="isEmValidacaoOuAprovado" v-if="i.Lat != '' && i.DataFoto != '1900-01-01 00:00:00.000' && i.CodigoGrupoFaunistico == null " @click="verificaInformacoesFotos(index, i.CodigoConfiguracaoMonitoramentoFaunaFotos)" class="btn btn-success">Salvar</button>
                                            <button :disabled="isEmValidacaoOuAprovado" @click="deletarFotos(i.CodigoConfiguracaoMonitoramentoFaunaFotos, i.Caminho)" class="btn btn-danger">Excluir</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('cgmab/ConfiguracaoProgramas/scripts/appRecursosFaunasPontos.php') ?>
