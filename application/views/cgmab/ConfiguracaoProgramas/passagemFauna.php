<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }

    .borderRed {
        border: 1px solid red
    }

    .borderGreen {
        border: 1px solid green
    }

    .nav-tabs>.nav-item>.nav-link {
        border: 1px solid #dfdfdf !important;
        border-radius: 0px;
    }

    .nav-tabs>.nav-item>.nav-link.active {
        border: 1px solid #dfdfdf !important;
        border-radius: 0px;
        background-color: #e2e2e2;
        color: #6d6d6d !important;
        font-weight: bold;
    }

    .modal-w98-h50 {
        height: 50% !important;
    }
</style>

<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Configuração de Passagem de fauna</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Home</li>
                    <li class="breadcrumb-item active">Contratos</li>
                    <li class="breadcrumb-item active">Configuração de Programas</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>
    <div id="estruturas" class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-md-9">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="passagem-tab" data-toggle="tab" href="#passagem-fauna" role="tab" aria-controls="passagem-tab" aria-selected=" true">Passagem de Fauna</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="parecer-tab" data-toggle="tab" href="#parecer-fiscal" role="tab" aria-controls="parecer-tab" aria-selected="true">Parecer</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 d-flex align-items-center flex-row-reverse">
                                <button class="btn btn-sm btn-success d-flex align-items-center" v-if="!isEmValidacaoOuAprovado && !isFiscal" @click="solicitarValidacao()">Enviar para Validação</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="passagem-fauna" role="tabpanel" aria-labelledby="passagem-tab">
                                        <?php $this->load->view('cgmab/ConfiguracaoProgramas/paginas/passagemFaunaTab.php') ?>
                                    </div>
                                    <div class="tab-pane fade" id="parecer-fiscal" role="tabpanel" aria-labelledby="parecer-tab">
                                        <?php $this->load->view('cgmab/ConfiguracaoProgramas/paginas/parecerTab.php') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('cgmab/ConfiguracaoProgramas/scripts/appPassagemFauna.php') ?>