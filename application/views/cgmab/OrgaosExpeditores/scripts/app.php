<script>
    vmListaOrgaoExpeditor = new Vue({
        el: '#ListagemOrgaoExpeditor',
        data: {
            listaOrgaoExpeditor: []
        },
        mounted(){
            this.getOrgaoExpeditor();
        },
        methods: {
            async getOrgaoExpeditor(){
                var params = {
                    table: 'ListagemOrgaoExpeditor',
                    where: {
                        Status: 1
                    }
                }
                this.listaOrgaoExpeditor = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#tblListaOrgaoExpeditores');
            }
        }
    });
</script>
