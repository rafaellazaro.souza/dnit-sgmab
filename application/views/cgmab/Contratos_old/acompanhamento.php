<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Acompanhamento de Contratos</h1>
            </div>

        </div>
    </div>
</section>
<!-- /.card-body -->
<div class="card mt-4">
        <div class="card-header">
            <h3 class="card-title"><i class="fa fa-list mr-3"></i> Escopo dos Serviços Contratados</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fas fa-times"></i></button>-->
            </div>
        </div>
        <div class="card-body">
            <!-- Tabela de Aditivos-->
            <table class="table table-striped border-bottom mt-3 bg-c1 p-3">
                <thead>
                    <th>Id</th>
                    <th>Serviço</th>
                    <th>Detalhe do Escopo do Serviço</th>
                    <th>Licença/ Autorização</th>
                    <th>Lista de condicionantes Associadas</th>
                    <th>Ações</th>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Gerenciamento Ambiental</td>
                        <td>Detalhes 1</td>
                        <td>LI - 7575859 - IBAMA</td>
                        <td>
                            <div class="col-resp">
                                Condicionante 1<br />
                                Estudo 2<br />
                                Projeto 3<br />
                                Lisa de Condicionanes <br />
                                Programa PGR/PAE <br />
                                Sub Progema 2.2 <br />
                                Sub Progema 2.3 <br />
                                Projeto 3<br />
                                Lisa de Condicionanes <br />
                                Programa PGR/PAE <br />
                                Sub Progema 2.2 <br />
                                Projeto 3<br />
                                Lisa de Condicionanes <br />
                                Programa PGR/PAE <br />
                                Sub Progema 2.2 <br />
                                Projeto 3<br />
                                Lisa de Condicionanes <br />
                                Programa PGR/PAE <br />
                                Sub Progema 2.2 <br />
                            </div>
                        </td>
                        <td>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Gerenciamento Ambiental</td>
                        <td>Detalhes 2</td>
                        <td>LI - 7575859 - IBAMA</td>
                        <td>
                            <div class="col-resp">
                                Projeto 3<br />
                                Lisa de Condicionanes <br />
                                Programa PGR/PAE <br />
                                Sub Progema 2.2 <br />
                                Condicionante 1<br />
                                Estudo 2<br />
                                Projeto 3<br />
                                Lisa de Condicionanes <br />
                                Programa PGR/PAE <br />
                                Sub Progema 2.2 <br />
                                Sub Progema 2.3 <br />
                            </div>
                        </td>
                        <td>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Gerenciamento Ambiental</td>
                        <td>Detalhes 3</td>
                        <td>LI - 7575859 - IBAMA</td>
                        <td>
                            <div class="col-resp">
                                Condicionante 1<br />
                                Estudo 2<br />
                                Projeto 3<br />
                                Lisa de Condicionanes <br />
                                Programa PGR/PAE <br />
                                Sub Progema 2.2 <br />
                                Sub Progema 2.3 <br />
                            </div>
                        </td>
                        <td>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>


                </tbody>
            </table>
            <a href="javascript:;" class="btn btn-outline-secondary btn-sm mt-4" data-toggle="modal" data-target="#addServicoContratado"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <!-- /.card-body -->