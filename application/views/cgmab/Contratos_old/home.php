<?php $this->load->view('cgmab/home/css.php') ?>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>00 000035/2013 - EMPRESA</h1>
            </div>

        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner p-4">
                        <h4>Valor Total</h4>

                        <p class="m-0">R$ 198.655.000,00</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <div class="small-box-footer">76,6% Valor PI</div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner p-4">
                        <h4>Total Medido (Pi+A+R)</h4>

                        <p class="m-0">R$ 148.262.705,00</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <div class="small-box-footer">49,2% Medido</div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner p-4">
                        <h4>Total Empenhado</h4>

                        <p class="m-0">R$ 166.835.244,98</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <div class="small-box-footer">55,8% Empenhado</div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner p-4">
                        <h4>Dias a Vencer</h4>

                        <p class="m-0">Vencido</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <div class="small-box-footer">100% Concluídos</div>
                </div>
            </div>
            <!-- ./col -->
        </div>
    </div>
</section>


<!-- Main content col-12 col-sm-6-->
<section class="content mt-4">
    <div class="container-fluid">
        <div class="row">

            <div class="col-12 col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Item de Contrato 1 - SIAC</h4>
                    </div>
                    <div class="card-body">
                        <div class="progress mb-3">
                            <span class="mr-4">Previsto</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                <span class="ml-4">80%</span>
                            </div>
                        </div>
                        <div class="progress mb-3">
                            <span class="mr-4">Atacado</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                <span class="ml-4">70%</span>
                            </div>

                        </div>
                        <div class="progress mb-3">
                            <span class="mr-4">Concluído</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                <span class="ml-4">20%</span>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Item de Contrato 1 - SIAC</h4>
                    </div>
                    <div class="card-body">
                        <div class="progress mb-3">
                            <span class="mr-4">Previsto</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                <span class="ml-4">80%</span>
                            </div>
                        </div>
                        <div class="progress mb-3">
                            <span class="mr-4">Atacado</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                <span class="ml-4">70%</span>
                            </div>

                        </div>
                        <div class="progress mb-3">
                            <span class="mr-4">Concluído</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                <span class="ml-4">20%</span>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Item de Contrato 1 - SIAC</h4>
                    </div>
                    <div class="card-body">
                        <div class="progress mb-3">
                            <span class="mr-4">Previsto</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                <span class="ml-4">80%</span>
                            </div>
                        </div>
                        <div class="progress mb-3">
                            <span class="mr-4">Atacado</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                <span class="ml-4">70%</span>
                            </div>

                        </div>
                        <div class="progress mb-3">
                            <span class="mr-4">Concluído</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                <span class="ml-4">20%</span>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Item de Contrato 1 - SIAC</h4>
                    </div>
                    <div class="card-body">
                        <div class="progress mb-3">
                            <span class="mr-4">Previsto</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                <span class="ml-4">80%</span>
                            </div>
                        </div>
                        <div class="progress mb-3">
                            <span class="mr-4">Atacado</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                <span class="ml-4">70%</span>
                            </div>

                        </div>
                        <div class="progress mb-3">
                            <span class="mr-4">Concluído</span>
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                <span class="ml-4">20%</span>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
</section>
