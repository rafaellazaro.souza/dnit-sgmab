<?php $this->load->view('cgmab/css/comum.php'); ?>
<div id="vueApp">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Cadastro de Contratos</h1>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="<?php echo site_url('cgmab/configuracao') ?>"><i class="fa fa-times"></i></a>
                </div>

            </div>
        </div>
    </section>

    <section class="content mb-5">
        <div class="container-fluid">
            <!-- Default box -->
            <div class="card" :class="expandIdentificacontrato">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa fa-copy mr-3"></i> Identificação do contrato</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!-- botão expandir -->
                        <button type="button" class="btn btn-tool" v-if="expandIdentificacontrato === ''" @click="expandIdentificacontrato= 'card full-card' ">
                            <i class="fas fa-expand"></i></button>
                        <button type="button" class="btn btn-tool" v-else @click="expandIdentificacontrato= '' ">
                            <i class="fas fa-expand"></i></button>
                        <!-- botão expandir -->
                    </div>
                </div>
                <div class="card-body">
                    <form action="#">
                        <div class="row">
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Processo Administrativo</span>
                                </div>
                                <input type="text" aria-label="First name" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Tipo de Instrumento Contratual</span>
                                </div>
                                <select class="form-control"></select>
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Edital: Seleção SIAC</span>
                                </div>
                                <input type="text" aria-label="First name" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Data da Publicação do DOU</span>
                                </div>
                                <input type="text" aria-label="First name" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Data da Assinatura do Contrato</span>
                                </div>
                                <input type="text" aria-label="First name" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Tipo de Licitação</span>
                                </div>
                                <select class="form-control"></select>
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Contrato Número</span>
                                </div>
                                <input type="text" aria-label="First name" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Modalidade de Contratação</span>
                                </div>
                                <select class="form-control"></select>
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Objeto Contratual</span>
                                </div>
                                <input type="text" aria-label="First name" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Link DNIT Informações do edital</span>
                                </div>
                                <input type="text" aria-label="First name" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Descrição Escopo Contratual</span>
                                </div>
                                <textarea class="form-control"></textarea>
                            </div>

                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Proposta Aprovada Pela Empresa</span>
                                </div>
                                <input type="file" aria-label="First name" class="form-control p-5">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Prazo de Execução (dias)</span>
                                </div>
                                <input type="text" aria-label="First name" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Valor do Contrato (R$)</span>
                                </div>
                                <input type="text" aria-label="First name" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Anexar Apólice</span>
                                </div>
                                <input type="file" aria-label="First name" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Número da Guia de Caução</span>
                                </div>
                                <input type="text" aria-label="First name" class="form-control">
                            </div>


                            <!-- Tabela de Aditivos-->

                            <hr class="divider">
                            <h4 class="mt-5">Aditivos</h4>
                            <table class="table table-striped border-bottom mt-3 bg-c1 p-3">
                                <thead>
                                    <th>Id</th>
                                    <th>Aditivo</th>
                                    <th>Data Início</th>
                                    <th>Data Publicação</th>
                                    <th>Valor R$</th>
                                    <th>Anexo Aditivo Contratual</th>
                                    <th>Ações</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Inclusão programa</td>
                                        <td>21/03/2020</td>
                                        <td>20/03/2020</td>
                                        <td>20MM</td>
                                        <td>Processo SEI | Número SEI</td>
                                        <td>
                                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Extensão do Prazo</td>
                                        <td>21/03/2020</td>
                                        <td>20/03/2020</td>
                                        <td>20MM</td>
                                        <td>Processo SEI | Número SEI</td>
                                        <td>
                                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="input-group col-12 ">
                                <a href="javascript:;" class="btn btn-outline-secondary btn-sm" @click="expandIdentificacontrato= ''" data-toggle="modal" data-target="#addAditivo"><i class="fa fa-plus"></i></a>
                            </div>

                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Valor do Contrato Atual</span>
                                </div>
                                <input type="text" aria-label="First name" class="form-control p-5">
                            </div>
                            <div class="input-group col-12 col-sm-6 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">observações</span>
                                </div>
                                <textarea type="text" aria-label="First name" class="form-control"></textarea>
                            </div>
                            <div class="input-group col-12 col-sm-8 mt-5"></div>
                            <div class="input-group col-12 col-sm-8 mt-5">
                                <button type="button" class="btn btn-outline-secondary"><i class="fa fa-times"></i> Cancelar</button>
                                <button type="button" class="btn btn-secondary ml-2 w-25"><i class="far fa-save"></i> Salvar</button>
                            </div>

                        </div>


                    </form>

                </div>
            </div> <!-- /.card-body -->

            <!-- /.card-body -->
            <div class="card mt-4" :class="expandCadastroApostilamento">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa fa-list mr-3"></i> Cadastro de Apostilamento</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                         <!-- botão expandir -->
                         <button type="button" class="btn btn-tool" v-if="expandCadastroApostilamento === ''" @click="expandCadastroApostilamento= 'card full-card' ">
                            <i class="fas fa-expand"></i></button>
                        <button type="button" class="btn btn-tool" v-else @click="expandCadastroApostilamento= '' ">
                            <i class="fas fa-expand"></i></button>
                        <!-- botão expandir -->
                    </div>
                </div>
                <div class="card-body">
                    <!-- Tabela de Aditivos-->
                    <table class="table table-striped border-bottom mt-3 bg-c1 p-3">
                        <thead>
                            <th>Id</th>
                            <th>Apostilamento Número</th>
                            <th>Escopo</th>
                            <th>Processo SEI</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>PP - 022/2017</td>
                                <td>Escopo 1</td>
                                <td>23456</td>
                                <td>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>PP - 022/2017</td>
                                <td>Escopo 2</td>
                                <td>23452</td>
                                <td>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>PP - 022/2017</td>
                                <td>Escopo 3</td>
                                <td>23453</td>
                                <td>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <a href="javascript:;" class="btn btn-outline-secondary btn-sm mt-4" @click="expandCadastroApostilamento= ''" data-toggle="modal"  data-target="#addApostilamento"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <!-- /.card-body -->
            <!-- /.card-body -->
            <div class="card mt-4" :class="expandEscopoServicos">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa fa-list mr-3"></i> Escopo dos Serviços Contratados</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!-- botão expandir -->
                        <button type="button" class="btn btn-tool" v-if="expandEscopoServicos === ''" @click="expandEscopoServicos= 'card full-card' ">
                            <i class="fas fa-expand"></i></button>
                        <button type="button" class="btn btn-tool" v-else @click="expandEscopoServicos= '' ">
                            <i class="fas fa-expand"></i></button>
                        <!-- botão expandir -->
                    </div>
                </div>
                <div class="card-body">
                    <!-- Tabela de Aditivos-->
                    <table class="table table-striped border-bottom mt-3 bg-c1 p-3">
                        <thead>
                            <th>Id</th>
                            <th>Serviço</th>
                            <th>Detalhe do Escopo do Serviço</th>
                            <th>Licença/ Autorização</th>
                            <th>Lista de condicionantes Associadas</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Gerenciamento Ambiental</td>
                                <td>Detalhes 1</td>
                                <td>LI - 7575859 - IBAMA</td>
                                <td>
                                    <div class="col-resp">
                                        Condicionante 1<br />
                                        Estudo 2<br />
                                        Projeto 3<br />
                                        Lisa de Condicionanes <br />
                                        Programa PGR/PAE <br />
                                        Sub Progema 2.2 <br />
                                        Sub Progema 2.3 <br />
                                        Projeto 3<br />
                                        Lisa de Condicionanes <br />
                                        Programa PGR/PAE <br />
                                        Sub Progema 2.2 <br />
                                        Projeto 3<br />
                                        Lisa de Condicionanes <br />
                                        Programa PGR/PAE <br />
                                        Sub Progema 2.2 <br />
                                        Projeto 3<br />
                                        Lisa de Condicionanes <br />
                                        Programa PGR/PAE <br />
                                        Sub Progema 2.2 <br />
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Gerenciamento Ambiental</td>
                                <td>Detalhes 2</td>
                                <td>LI - 7575859 - IBAMA</td>
                                <td>
                                    <div class="col-resp">
                                        Projeto 3<br />
                                        Lisa de Condicionanes <br />
                                        Programa PGR/PAE <br />
                                        Sub Progema 2.2 <br />
                                        Condicionante 1<br />
                                        Estudo 2<br />
                                        Projeto 3<br />
                                        Lisa de Condicionanes <br />
                                        Programa PGR/PAE <br />
                                        Sub Progema 2.2 <br />
                                        Sub Progema 2.3 <br />
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Gerenciamento Ambiental</td>
                                <td>Detalhes 3</td>
                                <td>LI - 7575859 - IBAMA</td>
                                <td>
                                    <div class="col-resp">
                                        Condicionante 1<br />
                                        Estudo 2<br />
                                        Projeto 3<br />
                                        Lisa de Condicionanes <br />
                                        Programa PGR/PAE <br />
                                        Sub Progema 2.2 <br />
                                        Sub Progema 2.3 <br />
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                                    <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>


                        </tbody>
                    </table>
                    <a href="javascript:;" class="btn btn-outline-secondary btn-sm mt-4" data-toggle="modal" data-target="#addServicosContratados" @click="expandEscopoServicos= ''"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <!-- /.card-body -->






        </div>
    </section>
</div>
<!-- Modais -->
<?php $this->load->view('cgmab/Contratos/modais/modalCadastroAditivos.php'); ?>
<?php $this->load->view('cgmab/Contratos/modais/modalCadastroApostilamento.php'); ?>
<?php $this->load->view('cgmab/Contratos/modais/modalServicosContratados.php'); ?>
<?php $this->load->view('cgmab/Contratos/script.php'); ?>
<!-- /.Modais -->