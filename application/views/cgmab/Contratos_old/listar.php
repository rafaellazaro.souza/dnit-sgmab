<section class="content-header mb-2">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-10">
                <h1>Lista de Contratos CGMAB(Gestão, Supervisão e Execução de Programas Ambientais)</h1>
            </div>
            <div class="col-sm-2 text-right">
                <a href="<?php echo site_url('cgmab/configuracao') ?>"><i class="fa fa-times"></i></a>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Mapa</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                </div>
            </div>
            <div class="card-body">
                <div class="row mt-5 ">
                    <div class="col-12 mt-4">
                        <!-- mapa  -->
                        <img src="<?php echo base_url('img_dpp/mapapadrao.jpg'); ?>" class="w-100" alt="">
                        <!-- fim mapa  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Selecione um Contrato</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                </div>
            </div>
            <div class="card-body">
                <div class="row mt-5 ">
                    <div class="col-12 mt-4">
                        <table class="table table-striped text-center">
                            <thead>
                                <tr>
                                    <th>Contrato</th>
                                    <th>BR/ UF</th>
                                    <th>Status</th>
                                    <th>Prazo</th>
                                    <th>Fase Licenciamento Ambiental</th>
                                    <th>Projeto de Estudos</th>
                                    <th>Supervisão Ambiental</th>
                                    <th>Gerenciamento Ambiental</th>
                                    <th>Execução de Programas</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                    <a href="<?php echo base_url('contratos/home')?>">000035/2019 - ECOMPLAN</a>    
                                    </td>
                                    <td>103/ PA</td>
                                    <td><span class="btn btn-success btn-sm">Ativo</span></td>
                                    <td>21/02/2023</td>
                                    <td><i class="fa fa-times btn-danger btn-sm"></i></td>
                                    <td><i class="fa fa-check btn-success btn-sm"></i></td>
                                    <td><i class="fa fa-times btn-danger btn-sm"></i></td>
                                    <td><i class="fa fa-times btn-danger btn-sm"></i></td>
                                    <td><i class="fa fa-check btn-success btn-sm"></i></td>
                                </tr>
                                <tr>
                                    <td>
                                    <a href="#">000035/2019 - Três Irmãos</a>    
                                    </td>
                                    <td>429/ RO</td>
                                    <td><span class="btn btn-warning btn-sm">Paralizado</span></td>
                                    <td>21/02/2023</td>
                                    <td><i class="fa fa-times btn-danger btn-sm"></i></td>
                                    <td><i class="fa fa-check btn-success btn-sm"></i></td>
                                    <td><i class="fa fa-times btn-danger btn-sm"></i></td>
                                    <td><i class="fa fa-times btn-danger btn-sm"></i></td>
                                    <td><i class="fa fa-check btn-success btn-sm"></i></td>
                                </tr>
                                </thead>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>