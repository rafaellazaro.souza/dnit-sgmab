<!-- Modal -->
<div class="modal fade" id="addAditivo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Adicionar Aditivo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="#">
          <div class="row">

            <div class="input-group col-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Aditivo Escopo</span>
              </div>
              <input type="text" aria-label="First name" class="form-control">
            </div>

            <div class="input-group col-12 col-sm-6 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Data Início</span>
              </div>
              <input type="text" aria-label="First name" class="form-control">
            </div>

            <div class="input-group col-12 col-sm-6 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Data Fim</span>
              </div>
              <input type="text" aria-label="First name" class="form-control">
            </div>
            
            <div class="input-group col-12 col-sm-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Valor</span>
              </div>
              <input type="text" aria-label="First name" class="form-control">
            </div>

            <div class="input-group col-12 col-sm-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Processo SEI</span>
              </div>
              <input type="text" aria-label="First name" class="form-control">
            </div>

            <div class="input-group col-12 col-sm-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Link Processo SEI</span>
              </div>
              <input type="text" aria-label="First name" class="form-control">
            </div>

            <div class="input-group col-12 col-sm-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Número SEI</span>
              </div>
              <input type="text" aria-label="First name" class="form-control">
            </div>

            <div class="input-group col-12 col-sm-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Link Número SEI</span>
              </div>
              <input type="text" aria-label="First name" class="form-control">
            </div>

            <div class="input-group col-12 col-sm-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Tipo de Adtivo</span>
              </div>
              <select class="form-control"></select>
            </div>

          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>