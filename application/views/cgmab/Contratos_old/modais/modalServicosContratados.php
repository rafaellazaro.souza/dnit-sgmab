<!-- Modal -->
<div class="modal fade" id="addServicosContratados" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Adicionar Aditivo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="#">
          <div class="row">
            <div class="input-group col-12 col-sm-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Atividade Ambiental</span>
              </div>
              <select class="form-control"></select>
            </div>

            <div class="input-group col-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Detalhamento do Escopo do Serviço</span>
              </div>
              <textarea class="form-control"></textarea>
            </div>

            <div class="input-group col-12 col-sm-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Licença/ Autorização</span>
              </div>
              <select class="form-control"></select>
            </div>

            <div class="input-group col-12 mt-2">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th><a href="javascript:;" id="selecionarTodosChkb"><i class="fa fa-check"></i></a></th>
                    <th>id</th>
                    <th>Número Cond.</th>
                    <th>Nome</th>
                    <th>Tipo</th>
                    <th>SubTipo</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><input type="checkbox" class="chkb"></td>
                    <td>1</td>
                    <td>1</td>
                    <td>Condicionante 1</td>
                    <td>Condicionante</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" class="chkb"></td>
                    <td>2</td>
                    <td>2</td>
                    <td>Condicionante 2</td>
                    <td>Programa</td>
                    <td>SubPrograma</td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" class="chkb"></td>
                    <td>3</td>
                    <td>3</td>
                    <td>Condicionante 3</td>
                    <td>Programa</td>
                    <td>SubPrograma</td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" class="chkb"></td>
                    <td>4</td>
                    <td>4</td>
                    <td>Condicionante 4</td>
                    <td>Programa</td>
                    <td>SubPrograma</td>
                  </tr>
                </tbody>
              </table>
            </div>

          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Inserir</button>
      </div>
    </div>
  </div>
</div>