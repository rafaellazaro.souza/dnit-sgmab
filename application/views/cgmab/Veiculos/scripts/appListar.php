<script>
var vmVeiculos = new Vue({
    el: '#gestaoVeiculos',
    data: {
        listaVeiculos: 0,
        listaMarcas: 0,
        listaModelos: 0,
        listaContratos: [],
        dadosFiltroVeiculos: {},
        mostrarAddVeiculos: false,
        bloqueados: {
            marca: true,
            modelo: true
        },
        erros: 0,
    },
    mounted() {
        this.getVeiculos();
        this.getContratos();
    },
    methods: {
        async getVeiculos() {
            $("#listadeVeiculos").DataTable().destroy();
            const params = {
                table: 'veiculos',
                joinLeft: [{
                    table1: 'veiculos',
                    table2: 'modelos',
                    on: 'CodigoModelo'
                },
                {
                    table1: 'modelos',
                    table2: 'marcas',
                    on: 'CodigoMarca'
                },
                {
                    table1: 'veiculos',
                    table2: 'contratos',
                    on: 'CodigoContrato'
                }],
                
                
                where: {
                    CodigoContratada: <?=  $_SESSION['Logado']['CodigoContratada']?>
                }
            }
            this.listaVeiculos = await vmGlobal.getFromAPI(params, 'cgmab');
            vmGlobal.montaDatatable('#listadeVeiculos', true);

        },
        async getContratos(){
            let params ={
                table: 'contratos',
                where: {
                    CnpjEmpresa: <?= isset($_SESSION['Logado']['CnpjContratada']) ? $_SESSION['Logado']['CnpjContratada'] : 0; ?>,
                }
            }
            this.listaContratos = await vmGlobal.getFromAPI(params);
        },  
        async getMarcas() {
            const params = {
                table: 'marcas',
                where: {
                    TipoVeiculo: this.dadosFiltroVeiculos.Tipo
                }
            }
            this.listaMarcas = await vmGlobal.getFromAPI(params, 'cgmab');
            this.bloqueio('marca');
        },
        async getModelos() {
            const params = {
                table: 'modelos',
                where: {
                    CodigoMarca: this.dadosFiltroVeiculos.CodigoMarca
                }
            }
            this.listaModelos = await vmGlobal.getFromAPI(params, 'cgmab');
            this.bloqueio('modelo');
        },
        bloqueio: function(campo) {
            if (campo === 'marca') {
                this.bloqueados.marca = false;
            } else {
                this.bloqueados.modelo = false;
            }

        },
        checkForm: function(e) {
            if (this.dadosFiltroVeiculos.CodigoContrato && this.dadosFiltroVeiculos.Tipo && this.dadosFiltroVeiculos.CodigoMarca && this.dadosFiltroVeiculos.CodigoModelo && this.dadosFiltroVeiculos.Ano 
            && this.dadosFiltroVeiculos.Placa && this.dadosFiltroVeiculos.Chassi) {
                this.salvarVeiculo();
                return true;
            }

            this.erros = [];

            if (!this.dadosFiltroVeiculos.CodigoContrato) {
                this.erros.push('Contrato é obrigatório!');
            }
            if (!this.dadosFiltroVeiculos.Tipo) {
                this.erros.push('Tipo é obrigatório!');
            }
            if (!this.dadosFiltroVeiculos.CodigoMarca) {
                this.erros.push('Marca é obrigatório!');
            }
            if (!this.dadosFiltroVeiculos.CodigoModelo) {
                this.erros.push('Modelo é obrigatório!');
            }
            if (!this.dadosFiltroVeiculos.Ano) {
                this.erros.push('Ano é obrigatório!');
            }
            if (!this.dadosFiltroVeiculos.Placa) {
                this.erros.push('Placa é obrigatório!');
            }
            // if (!this.dadosFiltroVeiculos.Foto) {
            //     this.erros.push('Foto do CLRV é obrigatório!');
            // }

            e.preventDefault();
        },
        async salvarVeiculo() {
            var file = null;
            if (this.$refs.Foto != null && this.$refs.Foto != undefined) {
                this.dadosFiltroVeiculos.NomeFoto = this.$refs.Foto.files[0].name;
                this.dadosFiltroVeiculos.FilesRules = {
                    upload_path: 'webroot/uploads/veiculos',
                    allowed_types: 'jpg|jpeg|png|gif|pdf',
                };
                file = {
                    name: 'Foto',
                    value: this.$refs.Foto.files[0]
                }
            }
            //remove dados desenecessarios para o insert
            delete(this.dadosFiltroVeiculos.Tipo);
            delete(this.dadosFiltroVeiculos.CodigoMarca);

            this.dadosFiltroVeiculos.CodigoContratada = <?=  $_SESSION['Logado']['CodigoContratada']?>;
            const params = {
                table: 'veiculos',
                data: this.dadosFiltroVeiculos
            }
            await vmGlobal.insertFromAPI(params, file, 'cgmab');
            this.$refs.Foto = null;
            this.mostrarAddVeiculos = false;
            this.getVeiculos();
            this.dadosFiltroVeiculos = {}
            

        },
        async deletar(codigo, index) {
            params = {
                table: 'veiculos',
                where: {
                    CodigoVeiculo: codigo
                }
            }
            await vmGlobal.deleteFromAPI(params);
            this.listaVeiculos.splice(index, 1);
        }

    }
});
</script>