<div class="body_scroll" id="gestaoVeiculos">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Gestão de Veículos</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url();?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item">Veículos</li>
                    <li class="breadcrumb-item active">Listar</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                        class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <button @click="mostrarAddVeiculos = true" class="btn btn-secondary"><i class="fa fa-plus"></i>
                        Adicionar Veículo</button>
                    <!-- formulario para adicionar/Editar veiculos  -->
                    <div class="modal-w98-h50 sombra-2 p-5 w-50" v-show="mostrarAddVeiculos">
                        <div class="col-12">
                            <button @click="mostrarAddVeiculos = false;dadosFiltroVeiculos = {}; erros = 0" class="btn btn-secondary btn-r"><i class="fa fa-times"></i></button>
                        </div>
                        <h4>Dados do Veículo</h4>

                        <div class="row mt-3">
                            <div class="alert alert-warning p-3 text-left w-100" v-show="erros.length">
                                <b>Por Favor Preencha o(s) campo(s) abaixo:</b>
                                <ul>
                                    <li v-for="i in erros" v-text="i"></li>
                                </ul>

                            </div>


                            <div class="input-group mb-3 col-12 mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Contrato</span>
                                </div>
                                <select v-model="dadosFiltroVeiculos.CodigoContrato" class="form-control show-tick ms select2">
                                    <option value="">Selecione</option>
                                    <option :value="i.CodigoContrato" v-for="i in listaContratos" v-text="i.Numero+ '/'+ i.NomeEmpresa"></option>
                                   
                                </select>
                            </div>
                            <div class="input-group mb-3 col-12 col-sm-6  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Tipo</span>
                                </div>
                                <select v-model="dadosFiltroVeiculos.Tipo" @change="getMarcas()"
                                    class="form-control show-tick ms select2">
                                    <option value="">Selecione</option>
                                    <option value="1">Carros e Utilitarios</option>
                                    <option value="2">Caminhões</option>
                                    <option value="3">Barcos</option>
                                </select>
                            </div>
                            <div class="input-group mb-3 col-12 col-sm-6  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Marca</span>
                                </div>
                                <select v-model="dadosFiltroVeiculos.CodigoMarca" @change="getModelos()"
                                    class="form-control show-tick ms select2" v-bind:readonly="bloqueados.marca">
                                    <option value="">Selecione</option>
                                    <option v-for="i in listaMarcas" :value="i.CodigoMarca" v-text="i.NomeMarca">
                                    </option>
                                </select>
                            </div>
                            <div class="input-group mb-3 col-12 col-sm-6  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Modelo</span>
                                </div>
                                <select v-model="dadosFiltroVeiculos.CodigoModelo" v-bind:readonly="bloqueados.modelo"
                                    class="form-control show-tick ms select2">
                                    <option value="">Selecione</option>
                                    <option v-for="i in listaModelos" :value="i.CodigoModelo" v-text="i.NomeModelo">
                                    </option>
                                </select>
                            </div>
                            <div class="input-group mb-3 col-12 col-sm-6  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Numero</span>
                                </div>
                                <input type="number" v-model="dadosFiltroVeiculos.Numero" class="form-control">
                            </div>
                            <div class="input-group mb-3 col-12 col-sm-6  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Ano</span>
                                </div>
                                <input type="number" v-model="dadosFiltroVeiculos.Ano" class="form-control">
                            </div>
                            <div class="input-group mb-3 col-12 col-sm-6  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Placa</span>
                                </div>
                                <input type="text" v-model="dadosFiltroVeiculos.Placa" class="form-control">
                            </div>
                            <div class="input-group mb-3 col-12 col-sm-6  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Chassi</span>
                                </div>
                                <input type="text" v-model="dadosFiltroVeiculos.Chassi" class="form-control">
                            </div>
                            <div class="input-group mb-3 col-12 col-sm-6  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Código SICRO</span>
                                </div>
                                <input type="number" v-model="dadosFiltroVeiculos.CodigoSICRO" class="form-control">
                            </div>
                            <div class="input-group mb-3 col-12 mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Foto do Veículo</span>
                                </div>
                                <input type="file" ref="Foto" name="Foto" required class="form-control">
                            </div>
                            <div class="input-group mb-3 col-12  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Observações</span>
                                </div>
                                <textarea v-model="dadosFiltroVeiculos.Observacoes" rows="5"
                                    class="form-control"></textarea>
                            </div>
                            <div class="col-12 text-right">
                                <button @click="mostrarAddVeiculos = false;dadosFiltroVeiculos = {}; erros = 0" class="btn btn-outline-secondary">Cancelar</button>
                                <button @click="checkForm" class="btn btn-secondary">Salvar</button>
                            </div>

                        </div>


                    </div>
                    <!-- formulario para adicionar/Editar veiculos  -->


                    <div class="body mt-4">
                        <table class="table table-striped" id="listadeVeiculos">
                            <thead>
                                <th>Foto</th>
                                <th>Contrato</th>
                                <th>Modelo</th>
                                <th>Marca</th>
                                <th>Placa</th>
                                <th>Tipo</th>
                                <th>Ano</th>
                                <th>Cod. SICRO</th>
                                <th>Ação</th>
                            </thead>
                            <tbody>
                                <tr v-for="(i, index) in listaVeiculos">
                                    <td><img :src="i.Foto" class="img-thumbnail"></td>
                                    <td v-text="i.Numero"></td>
                                    <td v-text="i.NomeModelo"></td>
                                    <td v-text="i.NomeMarca"></td>
                                    <td v-text="i.Placa"></td>
                                    <td>{{i.TipoVeiculo === '1' || i.TipoVeiculo === 1 ? 'Carros/ Utilitários' : '-'}}</td>
                                    <td v-text="i.Ano"></td>
                                    <td v-text="i.CodigoSICRO"></td>
                                    <td><button @click="deletar(i.CodigoVeiculo, index)" class="btn btn-outline-danger btn-sm"><i class="fa fa-times"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('cgmab/Veiculos/scripts/appListar')?>