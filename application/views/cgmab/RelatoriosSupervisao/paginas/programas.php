<div class="row pr-4 pl-4">
    <div class="col-md-12">
        <h4>
            Avaliação dos Resultados do Programas PAC
        </h4>
    </div>
    <div class="col-md-12">
        <button v-if="!isEmptyProgramasPac" class="btn btn-sm btn-success float-right" type="button" @click="salvarProgramasPac()">Salvar</button>
        <div class="alert alert-secondary text-dark" role="alert" v-else>
            Não há programas do PAC associados a este contrato.
        </div>
    </div>
</div>
<form action="" class="row pr-4 pl-4">
    <div class="col-md-6" v-for="(prog,index) in programasPac">
        <div class="form-group">
            <div class="form-line">
                <label for="">{{prog.NomeTema}} – {{prog.NomeSubtipo}}:</label>
                <textarea rows="4" class="form-control editorPAC" :id="'txtProgramasPac'+ index" :name="'txtProgramasPac'+ index"></textarea>
            </div>
        </div>
    </div>
</form>