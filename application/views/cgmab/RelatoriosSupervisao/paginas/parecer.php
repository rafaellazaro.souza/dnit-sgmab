<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }
</style>
<div class="row" v-if="isFiscal && !isConcluido && isEmValidacao">
    <div class="col-md-10">
        <div class="input-group mb-3" v-if="showInputPrazoCorrecao">
            <div class="input-group-prepend">
                <span id="basic-addon1" class="input-group-text">Data do Registro</span>
            </div>
            <input type="date" class="form-control  not-hide" v-model="newParecer.DataCorrecao">
        </div>
        <div class="form-group">
            <div class="form-line">
                <label for="">Parecer Técnico</label>
                <textarea rows="4" class="form-control no-resize" id="textParecer"></textarea>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <br>
        <div class="btn-group-vertical">
            <button class="btn btn-sm btn-success not-hide" type="button" @click="salvarParecer(4)">Aprovado</button>
            <button class="btn btn-sm btn-danger mt-2 not-hide" @click="salvarParecer(2)" type="button">Rejeitado</button>
            <button class="btn btn-sm btn-rejeitado mt-2 not-hide" type="button" @click="salvarParecer(3)">Correções</button>
        </div>
    </div>
</div>
<h3>Histórico de Validações</h3>
<div class="row">
    <div class="col-md-12">
        <table class="table table-sm table-bordered text-center no-footer compact" id="tblParecer">
            <thead>
                <tr>
                    <th>Fiscal</th>
                    <th>Parecer</th>
                    <th>Data do Parecer</th>
                    <th>Data de Correção</th>
                    <th>-</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="par in parecer">
                    <td>{{par.NomeUsuario}}</td>
                    <td><span class="badge" :class="styleParecer(par)">{{par.Situacao}}</span></td>
                    <td>{{formatarData(par.DataParecer)}}</td>
                    <td>{{formatarData(par.DataCorrecao)}}</td>
                    <td>
                        <button class="btn btn-sm btn-outline-secondary" @click="loadParecer(par)"><i class="zmdi zmdi-eye"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row border border-secondary p-2 mt-2" v-if="showParecer">
    <div class="col-md-12">
        <button class="float-right btn btn-sm btn-primary" @click="showParecer=false">x</button>
        <div class="form-group mt-2">
            <p class="p-0 m-0"><strong>Fiscal:</strong> {{selectedParecer.NomeUsuario}}</p>
            <p class="p-0 m-0"><strong>Data do Parecer:</strong> {{selectedParecer.DataParecer}}</p>
            <p class="p-0 m-0"><strong>Data de Correção Anterior:</strong> {{selectedParecer.AntigaDataCorrecao}}</p>
            <p class="p-0 m-0"><strong>Data de Correção:</strong> {{selectedParecer.DataCorrecao}}</p>
            <p class="pb-0 pt-0 mt-0 mb-0"><strong>Status:</strong> <span class="badge" :class="classParecer">{{selectedParecer.Situacao}}</span></p>
            <label for="">Parecer Técnico:</label>
            <textarea class="form-control" rows="4" id="txtParecerTecnicoView"></textarea>
        </div>
    </div>
</div>