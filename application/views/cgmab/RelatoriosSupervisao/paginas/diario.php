<div class="row" id="app-diario">
    <div class="col-md-12">
        <table class="table table-sm table-bordered text-center" id="tblRelatorioDiarios" style="width: 100%;">
            <thead>
                <tr class="bg-primary text-white">
                    <td>D.A.</td>
                    <td>UF</td>
                    <td>Local</td>
                    <td>Data</td>
                    <td>Ocorrência</td>
                    <td>-</td>
                </tr>
            </thead>
            <tbody>
                <tr v-for="reg in diarios">
                    <td>{{reg.CodigoDiario}}</td>
                    <td>{{reg.UF}}</td>
                    <td>{{reg.BR}}{{reg.FER}}{{reg.AQV}}</td>
                    <td>{{formatarData(reg.DataCadastro)}}</td>
                    <td>{{reg.Ocorrencias}}</td>
                    <td>
                        <a href="javascript:void(0);" @click="getDiario(reg.CodigoDiario)" class="btn btn-outline-info btn-sm"><i class="fa fa-edit"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>