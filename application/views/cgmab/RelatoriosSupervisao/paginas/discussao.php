<div class="row pr-4 pl-4">
    <div class="col-md-12">
        <h4>
            Discussões e Conclusões
        </h4>
    </div>
    <div class="col-md-12">
        <button class="btn btn-sm btn-success float-right" type="button" @click="salvarDiscussoesConclusoes();">Salvar</button>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <div class="form-line">
                <label for="">Análises dos Resultados e Discussões:</label>
                <textarea rows="4" class="form-control no-resize editor" id="textDiscussoes"></textarea>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <div class="form-line">
                <label for="">Conclusões:</label>
                <textarea rows="4" class="form-control no-resize editor" id="textConclusoes"></textarea>
            </div>
        </div>
    </div>
</div>