    <div class="row pr-4 pl-4">
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-line">
                    <label for="">Introdução:</label>
                    <textarea rows="4" class="form-control no-resize editor" id="textIntroducao"></textarea>
                </div>
            </div>
            <button class="btn btn-sm btn-success float-right" type="button" @click="salvarIntroducao()">Salvar</button>
        </div>
    </div>
    <div class="row pr-4 pl-4">
        <div class="col-md-12">
            <h4>Anexos</h4>
        </div>
        <div class="col-md-6">
            <input type="file" multiple="multiple" class="dropify" ref="relatorioAnexos">
            <button class="btn btn-sm btn-primary" type="button" @click="importarDocumentos()">Importar</button>
        </div>
        <div class="col-md-6">
            <table class="table table-sm table-hover table-bordered" style="width: 100%;" id="tblRelatorioAnexos">
                <thead>
                    <tr class="bg-secondary text-white">
                        <th>Arquivo</th>
                        <th>Data</th>
                        <th>-</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="arq in arquivos">
                        <td><a :href="arq.Arquivo" target="_blank">{{arq.Nome}}</a></td>
                        <td>{{formatarData(arq.DataCriacao)}}</td>
                        <td>
                            <button type="button" class="btn btn-outline-danger btn-sm btn-to-disabled" :disabled="disabled" @click="excluirArquivo(arq)"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>