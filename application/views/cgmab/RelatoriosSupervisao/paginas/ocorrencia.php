<div class="row" id="app-ocorrencias">
    <div class="col-md-12">
        <table class="table table-sm table-bordered text-center" id="tblRelatorioOcorrencias">
            <thead>
                <tr class="bg-primary text-white">
                    <th>ID</th>
                    <th>Tipo de Ocorrência</th>
                    <th>UF</th>
                    <th>Local</th>
                    <th>Data Registro</th>
                    <th>Data Última Atualização</th>
                    <th>Status</th>
                    <th>-</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="reg in ocorrencias">
                    <td>{{reg.CodigoOcorrencia}}</td>
                    <td>{{reg.TipoOcorrencia}}</td>
                    <td>{{reg.UF}}</td>
                    <td>{{reg.Localizacao}}</td>
                    <td>{{formatarData(reg.DataOcorrencia)}}</td>
                    <td>{{formatarData(reg.DataAtualizacaoOcorrencia)}}</td>
                    <td>{{reg.Status}}</td>
                    <td>
                        <a href="javascript:void(0);" class="btn btn-outline-info btn-sm" @click="getOcorrencia(reg.CodigoOcorrencia)"><i class="fa fa-edit"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>