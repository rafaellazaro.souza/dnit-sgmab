<style>
    .input-group-text {
        background-color: #afafaf;
        color: #f0f0f0;
    }

    .form-control {
        background-color: #dedede;
    }

    .h-150 {
        height: 100px !important;
    }

    .fs-13 {
        font-size: 13px;
    }

    .bg-1 {
        background-color: #fff
    }

    .table td,
    .table th {
        border-top: 1px solid #ffffff;
    }

    .form-add-servicos {
        position: fixed;
        top: 20%;
        margin: auto;
        z-index: 999;
        padding: 20px;
        background-color: #fff;
        height: 300px;
        overflow: auto;
        width: 60%;
    }

    .recursos {
        position: fixed;
        top: 72px;
        margin: auto;
        z-index: 999;
        padding: 20px;
        background-color: #fff;
        height: 91%;
        overflow: auto;
        width: 98.5%;
    }

    .form-gestao-condicionantes {
        position: fixed;
        top: 64px;
        z-index: 999;
        padding: 20px;
        background-color: #fff;
        height: 91%;
        overflow: auto;
        width: 98%;
        left: 1%;
    }

    .theme-dark .form-gestao-condicionantes,
    .theme-dark .desc-programas,
    .theme-dark .nova-meta,
    .theme-dark .listaBuscaCPF,
    .theme-dark .form-add-servicos,
    .theme-dark .nova-meta {
        background-color: #292929 !important;
    }

    .right-link {
        position: absolute;
        right: 7px;
        top: 18%;
    }

    .s-all {
        position: absolute;
        top: 0px;
        right: 16px;

    }

    .lista-licencas {
        position: absolute;
        top: 31px;
        z-index: 1;
        background-color: #fff;
    }

    .list-group-item.active {
        background-color: #97a6b7;
        border-color: #97a6b7;
    }



    .listaBuscaCPF {
        position: fixed;
        top: 20%;
        z-index: 1;
        width: 98%;
        left: 1%;
        background: #fff;
    }

    .desc-programas {
        position: fixed;
        width: 98%;
        top: 5%;
        background-color: #fff;
        z-index: 1;
        left: 1%;
        height: 90%;
        overflow-y: auto;
    }

    div.desc-programas>button {
        right: 3%;
        position: absolute;
    }

    .row>div>button {
        position: relative;
    }

    .cp-form>.input-group {
        margin-top: 21px;
    }

    .nova-meta {
        position: fixed;
        z-index: 2;
        padding: 25px;
        background: #fff;
        width: 85%;
        top: 20%;
        right: 10%;
    }

    .marcador {
        background-color: #0c7ce6 !important;
        color: white;
    }

    .cke_chrome {
        width: 100% !important;
    }
</style>
<div class="body_scroll" id="diario-content-app">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2> Relatório de Supervisão</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="breadcrumb-item active">
                        Relatório de Supervisão
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="relatorioSup">
        <div class="card">
            <div class="body">
                <template v-if="!isFiscal">
                    <p class="pb-0 pt-0 mt-0 mb-0"><strong>Contrato:</strong> {{dadosContrato.Numero}}</p>
                    <p class="pb-0 pt-0 mt-0 mb-0"><strong>Relatório:</strong> Supervisão Ambiental</p>
                    <p class="pb-0 pt-0 mt-0 mb-0"><strong>Mês de Início:</strong> {{formataDataContrato(dadosContrato.DataInicioContrato)}}</p>
                    <p class="pb-0 pt-0 mt-0 mb-0"><strong>Mês Fim:</strong> {{formataDataContrato(dadosContrato.DataTerminoPrevista)}}</p>
                    <button class="btn btn-primary btn-sm float-right mt-2" @click="openModal()"> Novo Relatório </button>
                </template>
                <h4>Lista de Relatório de Supervisão</h4>
                <table class="tabla table-hoover table-sm table-bordered text-center" style="width: 100%;" id="tblRelatorios">
                    <thead>
                        <tr class="bg-primary text-white">
                            <td v-show="isFiscal">Contrato</td>
                            <th>Relatório</th>
                            <th>Mês</th>
                            <th>Status</th>
                            <th>Data do Status</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="rel in relatorios">
                            <td v-show="isFiscal">{{rel.Numero}}</td>
                            <td>{{rel.CodigoRelatorio}}</td>
                            <td>{{rel.Mes}}/{{rel.Ano}}</td>
                            <td>{{getStatus(rel.CodigoSituacao)}}</td>
                            <td>{{formatarData(rel.DataCriacao)}}</td>
                            <td>
                                <a href="javascript:void(0);" @click="openRelatorio(rel.CodigoRelatorio);" class="btn btn-outline-info btn-sm"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" class="modal fade" aria-modal="true" id="modalNovoRelatorio">
            <div role="document" class="modal-dialog modal-xl">
                <form action="" @submit.prevent="createRelatorio()">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Novo Relatório</h4>
                            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body mb-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span id="basic-addon1" class="input-group-text">Ano</span>
                                        </div>
                                        <select required="required" class="form-control show-tick ms select2" v-model="novoRelatorio.Ano" required>
                                            <option v-for="ano in anos" :value="ano">{{ano}}</option>
                                        </select>
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span id="basic-addon1" class="input-group-text">Mês:</span>
                                        </div>
                                        <select required="required" class="form-control show-tick ms select2" v-model="novoRelatorio.Mes" required>
                                            <option v-for="(mes,i) in meses" :value="i + 1">{{mes}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-sm btn-success" type="submit">Novo Relatório</button>
                            <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" aria-label="Close">Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('webroot/arquivos/plugins/moment-business/moment-business-days.min.js') ?>"></script>
<script>
    const relatorio = new Vue({
        el: '#relatorioSup',
        data() {
            return {
                relatorios: [],
                dadosContrato: {
                    CodigoContrato: '',
                    Numero: '',
                    DataInicioContrato: '',
                    DataTerminoPrevista: ''
                },
                novoRelatorio: {
                    CodigoContrato: '',
                    CodigoUsuario: '',
                    CodigoSituacao: 1,
                    Ano: '',
                    Mes: '',
                    PrazoEnvio: ''
                },
                usuario: {
                    CodigoUsuario: '',
                    NomeUsuario: '',
                    Perfil: ''
                },
                status: [
                    'Aprovado',
                    'Correções',
                    'Rejeitado'
                ],
                meses: [
                    'Janeiro',
                    'Fevereiro',
                    'Março',
                    'Abril',
                    'Maio',
                    'Junho',
                    'Julho',
                    'Agosto',
                    'Setembro',
                    'Outubro',
                    'Novembro',
                    'Dezembro'
                ],
                anos: []
            }
        },
        computed: {
            isFiscal() {
                return this.usuario.Perfil.includes('Fiscal');
            }
        },
        watch: {
            'dadosContrato.CodigoContrato'(val) {
                this.getRelatorios();
                this.novoRelatorio.CodigoContrato = val;
            },
            'dadosContrato.DataInicioContrato'(val) {
                if (this.dadosContrato.DataTerminoPrevista.length == 0) return;
                if (val.length == 0) return;

                var anoInicio = val.split('-')[0];
                var anoFim = this.dadosContrato.DataTerminoPrevista.split('-')[0];

                for (var i = anoInicio; i <= anoFim; i++) {
                    this.anos.push(i);
                }
            },
            'usuario.CodigoUsuario'(val) {
                this.novoRelatorio.CodigoUsuario = val;
            }
        },
        async created() {
            await this.getRelatorios();
        },
        methods: {
            getStatus(codigo) {
                var status = [
                    'Em Andamento',
                    'Em Validação',
                    'Não Realizado',
                    'Cancelado',
                    'Concluído',
                    'Rejeitado',
                    'Parcialmente Aprovado'
                ];

                return status[codigo - 1];
            },
            openModal() {
                $('#modalNovoRelatorio', this.$el).modal('show');
            },
            openRelatorio(codigo) {
                var url = base_url + 'RelatoriosSupervisao/relatorio/' + vmGlobal.codificarDecodificarParametroUrl(codigo, 'encode')
                window.open(url, '_blank');
            },
            randomDate(start, end) {
                return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
            },
            formatDate(date) {
                return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
            },
            async getRelatorios() {
                await axios.post(base_url + 'RelatoriosSupervisao/getRelatorios')
                    .then(async (response) => {
                        this.usuario = response.data.usuario;

                        if (response.data.contrato.length != 0) {
                            this.dadosContrato = response.data.contrato;
                        }
                        var relatorios = response.data.relatorios;

                        if (relatorios.length != 0) {
                            await vmGlobal.detroyDataTable('#tblRelatorios');
                            this.relatorios = relatorios;
                            await this.$nextTick(() => {
                                vmGlobal.montaDatatable('#tblRelatorios');
                            });
                        }
                    });
            },
            formatarData(data) {
                if (data.length == 0) return '';
                var date = data.split(' ');
                date[0] = date[0].split('-').reverse().join('/');
                return date[0];
            },
            formataDataContrato(data) {
                if (data.length == 0) return '';
                var date = data.split('-');

                return this.meses[date[1] - 1] + ' de ' + date[0];
            },
            async createRelatorio() {
                this.novoRelatorio.PrazoEnvio = this.calculaPrazoEnvio();

                var CodgioRelatorio = await vmGlobal.insertFromApiAndReturnId(true, {
                    table: 'relatorioSupervisao',
                    data: this.novoRelatorio
                });

                if (CodgioRelatorio.length == 0) return;

                window.location.href = base_url + 'RelatoriosSupervisao/relatorio/' + vmGlobal.codificarDecodificarParametroUrl(CodgioRelatorio, 'encode');
            },
            calculaPrazoEnvio() {
                var date = moment(this.novoRelatorio.Ano + '-' + this.novoRelatorio.Mes + '-07', 'YYYY-MM-DD').add(1, 'months').format('YYYY-MM-DD');
                if (!moment(date, 'YYYY-MM-DD').isBusinessDay()) {
                    date = moment(date, 'YYYY-MM-DD').nextBusinessDay().format('YYYY-MM-DD');
                }

                return date;
            }
        },
        mounted() {
            vmGlobal.montaDatatable('#tblRelatorios');
        },
    })
</script>