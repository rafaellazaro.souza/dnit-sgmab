<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }

    .input-group-text {
        background-color: #afafaf;
        color: #f0f0f0;
    }

    .form-control {
        background-color: #dedede;
    }

    .h-150 {
        height: 100px !important;
    }

    .fs-13 {
        font-size: 13px;
    }

    .bg-1 {
        background-color: #fff
    }

    .table td,
    .table th {
        border-top: 1px solid #ffffff;
    }

    .form-add-servicos {
        position: fixed;
        top: 20%;
        margin: auto;
        z-index: 999;
        padding: 20px;
        background-color: #fff;
        height: 300px;
        overflow: auto;
        width: 60%;
    }

    .recursos {
        position: fixed;
        top: 72px;
        margin: auto;
        z-index: 999;
        padding: 20px;
        background-color: #fff;
        height: 91%;
        overflow: auto;
        width: 98.5%;
    }

    .form-gestao-condicionantes {
        position: fixed;
        top: 64px;
        z-index: 999;
        padding: 20px;
        background-color: #fff;
        height: 91%;
        overflow: auto;
        width: 98%;
        left: 1%;
    }

    .theme-dark .form-gestao-condicionantes,
    .theme-dark .desc-programas,
    .theme-dark .nova-meta,
    .theme-dark .listaBuscaCPF,
    .theme-dark .form-add-servicos,
    .theme-dark .nova-meta {
        background-color: #292929 !important;
    }

    .right-link {
        position: absolute;
        right: 7px;
        top: 18%;
    }

    .s-all {
        position: absolute;
        top: 0px;
        right: 16px;

    }

    .lista-licencas {
        position: absolute;
        top: 31px;
        z-index: 1;
        background-color: #fff;
    }

    .list-group-item.active {
        background-color: #97a6b7;
        border-color: #97a6b7;
    }



    .listaBuscaCPF {
        position: fixed;
        top: 20%;
        z-index: 1;
        width: 98%;
        left: 1%;
        background: #fff;
    }

    .desc-programas {
        position: fixed;
        width: 98%;
        top: 5%;
        background-color: #fff;
        z-index: 1;
        left: 1%;
        height: 90%;
        overflow-y: auto;
    }

    div.desc-programas>button {
        right: 3%;
        position: absolute;
    }

    .row>div>button {
        position: relative;
    }

    .cp-form>.input-group {
        margin-top: 21px;
    }

    .nova-meta {
        position: fixed;
        z-index: 2;
        padding: 25px;
        background: #fff;
        width: 85%;
        top: 20%;
        right: 10%;
    }

    .marcador {
        background-color: #0c7ce6 !important;
        color: white;
    }

    .cke_chrome {
        width: 100% !important;
    }
</style>

<div class="body_scroll" id="diario-content-app">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2> Relatório de Supervisão</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('RelatoriosSupervisao') ?>">Relatório de Supervisão</a>
                    </li>
                    <li class="breadcrumb-item">
                        Preencher Relatório
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="relatorio-supervisao">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-md-9 pl-4">
                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Contrato:</strong> {{contrato.Numero}}</p>
                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Relatório:</strong> Supervisão Ambiental</p>
                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Relatório: </strong> {{CodigoRelatorio}}</p>
                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Período:</strong> {{peridoRefencia}} </p>
                    </div>
                    <div class="col-md-3 pr-4">
                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Prazo para Envio:</strong> {{formatarData(relatorio.PrazoEnvio)}} </p>
                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Data de Envio:</strong> {{dataEnvio}}</p>
                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Hora de Envio</strong> {{horaEnvio}}</p>
                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Colaborador:</strong> {{relatorio.usuarioEnvio}}</p>
                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Status:</strong> <span class="badge" :class="classStatus">{{getStatus(relatorio.CodigoSituacao)}}</span></p>
                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Data de Correção:</strong> {{dataCorrecao}}</p>
                        <button type="button" class="btn btn-sm btn-primary" @click="enviarParaValidacao()">Enviar para validação</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" id="intro-tab" data-toggle="tab" href="#intro" role="tab" aria-controls="intro" aria-selected="true">Introdução</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="diario-tab" data-toggle="tab" href="#diario" role="tab" aria-controls="diario" aria-selected="true">Diario Ambiental</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="ocorrencia-tab" data-toggle="tab" href="#ocorrencia" role="tab" aria-controls="ocorrencia" aria-selected="true">Ocorrências</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pac-tab" data-toggle="tab" href="#pac" role="tab" aria-controls="pac" aria-selected="true">Programas do PAC</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="conclusao-tab" data-toggle="tab" href="#conclusao" role="tab" aria-controls="conclusao" aria-selected="true">Discussões e Conclusões</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="parecer-tab" data-toggle="tab" href="#parecer" role="tab" aria-controls="parecer" aria-selected="true">Parecer Técnico</a>
                            </li>
                        </ul>
                        <div class="body">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade active show" id="intro" role="tabpanel" aria-labelledby="intro">
                                    <?php $this->load->view('cgmab/RelatoriosSupervisao/paginas/introducao') ?>
                                </div>
                                <div class="tab-pane fade" id="diario" role="tabpanel" aria-labelledby="diario">
                                    <?php $this->load->view('cgmab/RelatoriosSupervisao/paginas/diario') ?>
                                </div>
                                <div class="tab-pane fade" id="ocorrencia" role="tabpanel" aria-labelledby="ocorrencia">
                                    <?php $this->load->view('cgmab/RelatoriosSupervisao/paginas/ocorrencia') ?>
                                </div>
                                <div class="tab-pane fade" id="pac" role="tabpanel" aria-labelledby="pac">
                                    <?php $this->load->view('cgmab/RelatoriosSupervisao/paginas/programas') ?>
                                </div>
                                <div class="tab-pane fade" id="conclusao" role="tabpanel" aria-labelledby="conclusao">
                                    <?php $this->load->view('cgmab/RelatoriosSupervisao/paginas/discussao') ?>
                                </div>
                                <div class="tab-pane fade" id="parecer" role="tabpanel" aria-labelledby="parecer">
                                    <?php $this->load->view('cgmab/RelatoriosSupervisao/paginas/parecer') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('webroot/arquivos/plugins/moment-business/moment-business.min.js') ?>"></script>
<script src="<?= base_url('webroot/arquivos/js/app/relatorio-supervisao/relatorio.js') ?>"></script>