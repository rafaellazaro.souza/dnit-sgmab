<div class="p-4" id="parecerFiscal">

    <h4 class="w-100 mb-4 border p-3">Parecer do Fiscal</h4>
    <!-- <button @click="salvarValidacao" v-if="mostrarEnvio" class="btn btn-success btn-sm btn-r mr-5"><i class="fas fa-check-double mr-2"></i> Finalizar Considerações</button>
    <div class="accordion" id="accordionExample">
        <div class="card border" v-for="(i, index) in listaSolicitacaoParecer">
            <div class="card-header" :id="'collapse'+i.CodigoParecerFiscal">
                <h4 class="mb-0">
                    <button @click="iniciarEditores('textarea'+index, index)" class="btn btn-link" type="button" data-toggle="collapse" :data-target="'#collapseOne'+i.CodigoParecerFiscal" aria-expanded="true" aria-controls="collapseOne">
                        {{i.NomeServico}}
                    </button>
                    <button class="btn btn-warning btn-r btn-sm" v-show="i.StatusFiscal=== 0 || i.StatusFiscal === '0'"><i class="far fa-calendar-times mr-2"></i> Avaliação Pendente</button>

                </h4>
            </div>

            <div :id="'collapseOne'+i.CodigoParecerFiscal" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    <h5>Descrição do parecer</h5>
                    <textarea :name="'parecer'+index" :id="'textarea'+index" class="form-control">
                    {{i.ParecerTecnicoFiscal ? i.ParecerTecnicoFiscal : '<ol><li><strong>Dados do Contrato: [Status]</strong><br/>​​​​​​​&nbsp;- [Considera&ccedil;&otilde;es do fiscal sobre dados do contrato]<br/>​​​​​​​&nbsp;-[Considera&ccedil;&otilde;es do fiscal sobre dados do contrato]<br/>​​​​​​​	&nbsp;- [Considera&ccedil;&otilde;es do fiscal sobre dados do contrato]</li><li><strong>Escopo de Servi&ccedil;os: [Status]</strong><br/>​​​​​​​&nbsp;- [Considera&ccedil;&otilde;es do fiscal sobre Escopo de Servi&ccedil;os]<br/>​​​​​​​&nbsp;- [Considera&ccedil;&otilde;es do fiscal sobre Escopo de Servi&ccedil;os]</li><li><strong>Caracteriza&ccedil;&atilde;o do Empreendimento: [Status]</strong><br/>​​​​​​​&nbsp;- [Considera&ccedil;&otilde;es do fiscal sobre Caracteriza&ccedil;&atilde;o do Empreendimento:]</li><li><strong>Recursos Humanos: [Status]</strong><br/>​​​​​​​&nbsp;- [Considera&ccedil;&otilde;es do fiscal sobre Recursos Humanos]</li><li><strong>InfraEstrutura: [Status]</strong><br/>​​​​​​​&nbsp;- [Considera&ccedil;&otilde;es do fiscal sobre InfraEstrutura]<br/>​​​​​​​&nbsp;- [Considera&ccedil;&otilde;es do fiscal sobre InfraEstrutura]</li></ol>'}}


                    </textarea>

                    <div class="row mt-4">
                        <div class="col-12 text-right p-0">
                            <div class="input-group mb-3 col-12 p-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Situação do Parecer</span>
                                </div>
                                <select v-model="dadosParecer.StatusFiscal" class="form-control show-tick ms select2">
                                    <option selected>Selecione</option>
                                    <option :value="0">Incompleto</option>
                                    <option :value="1">Completo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4 border-top">
                        <div class="col-12 text-right">
                            <button @click="preencher(i.CodigoParecerFiscal, 'textarea'+index, index)" class="btn btn-primary btn-sm mr-2"><i class="far fa-save mr-2"></i> Salvar Rascunho</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="row" v-if="habilitarParecer">
        <div class="col-md-10">
            <div class="input-group mb-3" v-if="showCorrecao">
                <div class="input-group-prepend">
                    <span id="basic-addon1" class="input-group-text">Prazo de Correção</span>
                </div>
                <input type="date" class="form-control  not-hide" :min="toDayDate" v-model="parecer.PrazoCorrecao" required>
            </div>
            <div class="form-group">
                <div class="form-line">
                    <label for="">Parecer Técnico</label>
                    <textarea rows="4" class="form-control no-resize" id="textParecer"></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <br>
            <div class="btn-group-vertical">
                <button class="btn btn-sm btn-success not-hide" @click="salvarParecer(4)" type="button">Aprovado</button>
                <button class="btn btn-sm btn-rejeitado mt-2 not-hide" type="button" @click="salvarParecer(5)">Parcialmente Aprovado</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm table-striped table-hover compact" style="width: 100%;" id="tblParecerFiscal">
                <thead>
                    <tr>
                        <th>Situação</th>
                        <th>Envio para Validação</th>
                        <th>Data Parecer</th>
                        <th>Prazo de Correção</th>
                        <th>Prazo de Correção Anterior</th>
                        <th>-</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="par in listaParecer">
                        <td> <span class="badge " :class="getClasse(par.StatusFiscal)"> {{getStatus(par.StatusFiscal)}} </span></td>
                        <td>{{formataData(par.DataEnvio)}}</td>
                        <td>{{formataData(par.DataParecer)}}</td>
                        <td>{{formataData(par.PrazoCorrecao)}}</td>
                        <td>{{formataData(par.AntigaDataCorrecao)}}</td>
                        <td>
                            <button class="btn btn-sm btn-outline-secondary" @click="loadParecer(par)"><i class="zmdi zmdi-eye"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row mt-2" v-if="showParecer">
        <div class="col-md-12 border border-secondary">
            <button class="float-right btn btn-sm btn-primary" @click="showParecer=false">x</button>
            <div class="form-group mt-2">
                <p class="p-0 m-0"><strong>Situação:</strong> <span class="badge " :class="getClasse(selectedParecer.StatusFiscal)">{{getStatus(selectedParecer.StatusFiscal)}}</span></p>
                <p class="p-0 m-0"><strong>Fiscal:</strong> {{selectedParecer.NomeUsuario}}</p>
                <p class="p-0 m-0"><strong>Data do Parecer:</strong> {{selectedParecer.DataParecer}}</p>
                <p class="p-0 m-0"><strong>Data do Envio:</strong> {{selectedParecer.DataEnvio}}</p>
                <p class="p-0 m-0"><strong>Prazo de Correção:</strong> {{selectedParecer.PrazoCorrecao}}</p>
                <p class="p-0 m-0"><strong>Prazo de Correção Anterior:</strong> {{selectedParecer.AntigaDataCorrecao}}</p>
                <label for="">Parecer Técnico:</label>
                <textarea class="form-control" rows="4" id="txtParecerTecnicoView"></textarea>
            </div>
        </div>
    </div>
</div>