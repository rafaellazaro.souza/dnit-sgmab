<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }
</style>
<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Relatórios de Atividades Mensais</h2>
                <ul class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item">Relatorio de Atividades</li>
                    <li class="breadcrumb-item">Atividades do Mes</li>
                    <li class="breadcrumb-item active">Parecer Fiscal</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="parecer-atividades">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="input-group mb-3" v-if="showInputPrazoCorrecao">
                                    <div class="input-group-prepend">
                                        <span id="basic-addon1" class="input-group-text">Data do Registro</span>
                                    </div>
                                    <input type="date" class="form-control  not-hide" v-model="newParecer.PrazoCorrecao">
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="">Atividades</label>
                                        <textarea rows="4" class="form-control no-resize" id="textParecerAtividades"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="">Relatório</label>
                                        <textarea rows="4" class="form-control no-resize" id="textParecerRelatorio"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <br>
                                <div class="btn-group-vertical">
                                    <button class="btn btn-sm btn-secondary mb-2 not-hide" type="button" @click="salvarParecer(0)">Salvar Rascunho</button>
                                    <button class="btn btn-sm btn-success not-hide" type="button" @click="salvarParecer(3)">Aprovado</button>
                                    <button class="btn btn-sm btn-danger mt-2 not-hide" @click="salvarParecer(4)" type="button">Rejeitado</button>
                                    <button class="btn btn-sm btn-rejeitado mt-2 not-hide" type="button" @click="salvarParecer(2)">Parcialmente Aprovado</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var parametrosEscopo = location.search.slice(1);
    const codigoRelatorio = vmGlobal.codificarDecodificarParametroUrl(parametrosEscopo.split('=')[1], 'decode');

    var parecerFiscal = new Vue({
        el: '#parecer-atividades',
        data() {
            return {
                showInputPrazoCorrecao: false,
                newParecer: {
                    CodigoParecerFiscal: '',
                    CodigoFiscal: '',
                    CodigoRelatorioMensalPrograma: '',
                    StatusFiscal: '',
                    ParecerRelatorio: '',
                    ParecerAtividades: '',
                    DataEnvio: '',
                    DataParecer: null,
                    AntigaDataCorrecao: null,
                    PrazoCorrecao: null
                },
                relatorio: {
                    CodigoRelatorioMensalPrograma: '',
                    CodigoEscopoPrograma: '',
                    MesReferencia: '',
                    AnoReferencia: '',
                    Situacao: '',
                    DataInicio: '',
                    DataEnvioFiscal: '',
                    DataAprovado: '',
                    DataReprovado: '',
                    DataCorrecao: '',
                    PrazoEnvio: '',
                    CodigoUsuarioEnvio: ''
                },
                usuario: {
                    CodigoUsuario: ''
                }
            }
        },
        watch: {
            'relatorio.DataEnvioFiscal'(val) {
                this.newParecer.DataEnvio = val;
            },
            'relatorio.DataCorrecao'(val) {
                this.newParecer.AntigaDataCorrecao = val;
            },
            'relatorio.CodigoRelatorioMensalPrograma'(val) {
                this.newParecer.CodigoRelatorioMensalPrograma = val;
            },
            'usuario.CodigoUsuario'(val) {
                this.newParecer.CodigoFiscal = val;
            },
        },
        methods: {
            async iniciaEditores() {
                await this.$nextTick(() => {
                    CKEDITOR.replace('textParecerAtividades', {
                        height: '400px',
                    });

                    CKEDITOR.replace('textParecerRelatorio', {
                        height: '400px',
                    });
                })
            },
            async salvarParecer(status) {
                this.newParecer.StatusFiscal = status;
                if (status == 4 || status == 3) {
                    this.newParecer.PrazoCorrecao = null;
                    this.showInputPrazoCorrecao = false;
                } else if (status == 2 && this.newParecer.PrazoCorrecao == null) {
                    this.showInputPrazoCorrecao = true;
                    return
                }

                var parecer = Object.assign({}, this.newParecer);
                parecer.ParecerRelatorio = String(btoa(CKEDITOR.instances.textParecerRelatorio.getData()))
                parecer.ParecerAtividades = String(btoa(CKEDITOR.instances.textParecerAtividades.getData()))
                delete parecer.CodigoParecerFiscal;

                Swal.fire({
                    title: 'Salvar parecer?',
                    text: "Esta ação não poderá ser desfeita.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Salvar',
                    cancelButtonText: 'Cancelar'
                }).then(async (result) => {
                    if (result.value) {
                        if (this.newParecer.CodigoParecerFiscal.length == 0) {
                            await vmGlobal.insertFromAPI({
                                table: 'parecerAtividade',
                                data: parecer
                            });
                        } else {
                            parecer.DataParecer = moment().format('YYYY-MM-DD');
                            await vmGlobal.updateFromAPI({
                                table: 'parecerAtividade',
                                data: parecer,
                                where: {
                                    CodigoParecerFiscal: this.newParecer.CodigoParecerFiscal
                                }
                            });

                        }

                        if (status != 0) {
                            await this.updateRelatorio(status);
                            setTimeout(() => {
                                window.close();
                            }, 3000);
                        } else {
                            this.getParecer();
                        }
                    }
                })

            },
            async getParecer() {
                var parecer = await vmGlobal.getFromAPI({
                    table: 'parecerAtividade',
                    where: {
                        CodigoRelatorioMensalPrograma: codigoRelatorio,
                        StatusFiscal: 0
                    }
                });

                if (parecer.length != 0) {
                    this.newParecer = parecer[0];

                    this.$nextTick(() => {
                        CKEDITOR.instances.textParecerRelatorio.setData(String(atob(this.newParecer.ParecerRelatorio)));
                        CKEDITOR.instances.textParecerAtividades.setData(String(atob(this.newParecer.ParecerAtividades)));
                    });

                    return;
                }

                this.newParecer = this._parecer;
            },
            async getRelatorio() {
                var relatorio = await vmGlobal.getFromAPI({
                    table: 'tblRelatorioMensalProgramas',
                    where: {
                        CodigoRelatorioMensalPrograma: codigoRelatorio,
                    }
                });

                this.relatorio = relatorio[0];
            },
            async cloneParecer() {
                if (!this.hasOwnProperty('_parecer')) {
                    this._parecer = await Object.assign({}, this.newParecer);
                }
            },
            async updateRelatorio(status) {
                await vmGlobal.updateFromAPI({
                    table: 'tblRelatorioMensalProgramas',
                    data: {
                        DataCorrecao: this.newParecer.PrazoCorrecao,
                        Situacao: status
                    },
                    where: {
                        CodigoRelatorioMensalPrograma: this.newParecer.CodigoRelatorioMensalPrograma
                    }
                });
            }
        },
        async mounted() {
            await this.iniciaEditores();
            await vmGlobal.dadosUsuario();
            this.usuario = vmGlobal.dadosDoUsuario;
            await this.getRelatorio();
            await this.cloneParecer();
            await this.getParecer();
        },
    })
</script>