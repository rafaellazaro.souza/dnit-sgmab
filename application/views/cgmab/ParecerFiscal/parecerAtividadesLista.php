<style>
    .badge-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }

    .badge-warning {
        color: #212529;
        background-color: #ffc107;
    }
</style>
<div class="body_scroll" id="diario-content-app">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Relatórios de Atividades Mensais</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('HistoricoValidacao') ?>">Histórico de Validação</a>
                    </li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>
    <div class="container-fluid table-responsive" id="app-historico">
        <table id="tblRelatorioAtividades" class="table table-hoover table-striped table-sm display compact" style="width: 100;">
            <thead>
                <tr>
                    <th>Contratos</th>
                    <th>Período</th>
                    <th>Tema</th>
                    <th>Subtipo</th>
                    <th>Situação</th>
                    <th>Prazo de Envio</th>
                    <th>Data de Envio</th>
                    <th>Data de Correção</th>
                    <td>-</td>
                </tr>
            </thead>
            <tbody>
                <tr v-for="rel in relatorios">
                    <td>{{rel.Numero}}</td>
                    <td>{{rel.Periodo}}</td>
                    <td>{{rel.NomeTema}}</td>
                    <td>{{rel.NomeSubtipo}}</td>
                    <td><span class="badge text-white " :class="getClass(rel.Situacao)" v-if="rel.Situacao">{{getSituacao(rel.Situacao)}}<span></td>
                    <td>{{formataData(rel.PrazoEnvio)}}</td>
                    <td>{{formataData(rel.DataEnvioFiscal)}}</td>
                    <td>{{formataData(rel.DataCorrecao)}}</td>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-outline-info btn-sm" @click="openRelatorio(rel)"><i class="fa fa-edit"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script>
    const relatorioAtividades = new Vue({
        el: '#app-historico',
        data() {
            return {
                relatorios: []
            }
        },
        methods: {
            async getRelatorios() {
                await axios.post(base_url + 'ParecerFiscal/getRelatoriosDeAtividades')
                    .then((response) => {
                        this.relatorios = response.data.relatorios;
                        this.refreshTable('#tblRelatorioAtividades');
                    })
            },
            constroiTable(id = "") {
                $('table' + id, this.$el).DataTable({
                    language: translateDataTable,
                    lengthChange: false,
                    destroy: true
                });
            },
            async refreshTable(id = "") {
                $('table' + id, this.$el).DataTable().destroy();
                await this.$nextTick(() => {
                    this.constroiTable();
                });
            },
            formataData(data) {
                if (data) {
                    return vmGlobal.frontEndDateFormat(data);
                }
                return '';
            },
            async openRelatorio(relatorio) {
                var param = 'kls65fd89e56ajk4saw8ioo77erw1as6q=' + vmGlobal.codificarDecodificarParametroUrl(relatorio.CodigoEscopoPrograma, 'encode');
                param += '&kls65fd89e56ajk4saw8ioo77erw1as6q=' + vmGlobal.codificarDecodificarParametroUrl(relatorio.CodigoContrato, 'encode');
                param += '&kls65fd89e56ajk4saw8ioo77erw1as6q=' + vmGlobal.codificarDecodificarParametroUrl(relatorio.CodigoRelatorioMensalPrograma, 'encode');
                window.location.href = base_url + 'Atividades/listaAtividadesMes?' + param;
            },
            getClass(situacao) {
                if (situacao == 3) {
                    return 'bg-success';
                } else if (situacao == 0 || situacao == 4) {
                    return 'bg-danger';
                } else if (situacao == 2) {
                    return 'badge-rejeitado';
                }

                return 'badge-primary';
            },
            getSituacao(situacao) {
                situacao = parseInt(situacao);
                if (situacao == 3) {
                    return 'Aprovado';
                } else if (situacao == 0) {
                    return 'Não Validado';
                } else if (situacao == 4) {
                    return 'Rejeitado';
                } else if (situacao == 2) {
                    return 'Parcialmente Aprovado';
                } else if (situacao = 1) {
                    return 'Enviado para Validação';
                }
                return '';
            }
            //     getDiferencaEmDias(ocorrencia) {
            //         if (ocorrencia.TipoOcorrencia == 'RNC') {
            //             var data = ocorrencia.PrazoSolucao.split('-');
            //             var dt1 = new Date(data[1] + '/' + data[2] + '/' + data[0]);
            //             var today = new Date();

            //             if (dt1.getTime() < today.getTime()) {
            //                 const diffTime = Math.abs(dt1 - today);
            //                 const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

            //                 return diffDays;
            //             }
            //         }

            //         return ''
            //     },
        },
        mounted() {
            this.constroiTable('#tblRelatorioAtividades');
            this.getRelatorios();
        },
    });
</script>