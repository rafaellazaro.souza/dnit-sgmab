<script>
    vmParecerFiscal = new Vue({
        el: '#parecerFiscal',
        data: {
            listaSolicitacaoParecer: {},
            listaEscoposEnviados: {},
            dadosParecer: {},
            CodigoParecerFiscal: 0,
            mostrarEnvio: false,
            ParecerFiscal: '',
            parecer: {
                CodigoFiscal: '',
                CodigoContrato: codigoContratoDecode,
                StatusFiscal: '',
                Parecer: '',
                DataEnvio: '',
                AntigaDataCorrecao: null,
                PrazoCorrecao: ''
            },
            selectedParecer: {
                StatusFiscal: '',
                Parecer: '',
                DataEnvio: '',
                DataParecer: '',
                AntigaDataCorrecao: '',
                PrazoCorrecao: '',
                NomeUsuario: ''
            },
            showParecer: false,
            showCorrecao: false,
            listaParecer: [],
            usuario: {
                NomeUsuario: '',
                CodigoUsuario: '',
                Perfil: ''
            },
            contrato: {
                Status: '',
                DataEnvioValidacao: '',
                PrazoCorrecao: ''
            },

        },
        watch: {
            ParecerFiscal(val) {
                this.parecer.Parecer = String(btoa(val));
            },
            'usuario.CodigoUsuario'(val) {
                this.parecer.CodigoFiscal = val;
            },
            'usuario.Perfil'(val) {
                if (val.includes('Fiscal') && this.habilitarParecer) {
                    this.iniciarEditores();
                }
            },
            'contrato.DataEnvioValidacao'(val) {
                if (val) {
                    this.parecer.DataEnvio = moment(val).format('YYYY-MM-DD');
                }
            },
            'contrato.PrazoCorrecao'(val) {
                if (val) {
                    this.parecer.AntigaDataCorrecao = moment(val).format('YYYY-MM-DD');
                }
            },

        },
        computed: {
            toDayDate() {
                return moment().format('YYYY-MM-DD');
            },
            isFiscal() {
                return this.usuario.Perfil.includes('Fiscal');
            },
            isNotAprovado() {
                return this.contrato.Status != 4;
            },
            isAtrazado() {
                if (this.contrato.PrazoCorrecao) {
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var prazoCorrecao = moment(this.contrato.PrazoCorrecao).toDate();
                    prazoCorrecao.setHours(0, 0, 0, 0);

                    return prazoCorrecao.getTime() < today.getTime();
                }

                return false;
            },
            habilitarParecer() {
                if (this.isFiscal && this.contrato.Status == 2) {
                    return true;
                    this.iniciarEditores();
                } else if (this.isFiscal && this.contrato.Status == 5 && this.isAtrazado) {
                    this.iniciarEditores();
                    return true;
                }
                return false;
            }
        },
        methods: {
            getClasse(val) {
                return (val == 4) ? 'badge-success' : 'btn-rejeitado';
            },
            getStatus(val) {
                return (val == 4) ? 'Aprovado' : 'Parcialmente Aprovado';
            },
            async getSolicitacaoParecer() {
                await axios.post(base_url + 'Contratos/listarServicosParecerPendente', $.param({
                        Contrato: codigoContratoDecode
                    }))
                    .then((resp) => {
                        this.listaSolicitacaoParecer = resp.data;
                    })
                this.listaEscoposEnviados = this.listaSolicitacaoParecer.map(x => x.CodigoEscopo).filter(function(elem, index, self) {
                    return index === self.indexOf(elem);
                });
                await this.validarPreenchimentoFiscal();
            },
            async iniciarEditores(textarea) {
                // CKEDITOR.replace(textarea);
                this.$nextTick(() => {
                    CKEDITOR.replace('textParecer');

                    CKEDITOR.instances['textParecer'].on("change", (evt) => {
                        this.ParecerFiscal = evt.editor.getData();
                    });
                });
            },
            async preencher(codigoParecer, textarea, index) {
                this.CodigoParecerFiscal = codigoParecer;
                this.dadosParecer.ParecerTecnicoFiscal = CKEDITOR.instances[textarea].getData();
                let params = {
                    table: 'envioFiscal',
                    data: this.dadosParecer,
                    where: {
                        CodigoParecerFiscal: this.CodigoParecerFiscal
                    }
                }
                await vmGlobal.updateFromAPI(params, null, 'cgmab');
                await this.getSolicitacaoParecer();
                await this.validarPreenchimentoFiscal();
                //limpatextare
                CKEDITOR.instances[textarea].setData(vmParecerFiscal.listaSolicitacaoParecer[index].ParecerTecnicoFiscal);
            },
            async validarPreenchimentoFiscal() {
                let params = {
                    table: 'envioFiscal',
                    where: {
                        StatusFiscal: 1
                    },
                    whereIn: {
                        CodigoEscopo: this.listaEscoposEnviados
                    }
                }
                let verificacao = await vmGlobal.getFromAPI(params, 'cgmab');
                if (verificacao.length === this.listaSolicitacaoParecer.length) {
                    this.mostrarEnvio = true;
                }

            },
            async salvarValidacao() {
                params = {
                    table: 'contratos',
                    data: {
                        Status: 4
                    },
                    where: {
                        CodigoContrato: this.listaSolicitacaoParecer[0].CodigoContrato
                    }
                }
                await vmGlobal.updateFromAPI(params, null, 'cgmab');
                window.location.href = base_url + 'Home/aprovacoesPendentes  '
            },
            salvarParecer(statusFiscal) {
                this.parecer.StatusFiscal = statusFiscal;

                if (statusFiscal != 5) {
                    this.parecer.PrazoCorrecao = null;
                    this.showCorrecao = false;
                } else if (statusFiscal == 5 && this.parecer.PrazoCorrecao.length == 0) {
                    this.showCorrecao = true;
                    return;
                }

                Swal.fire({
                    title: 'Salvar parecer?',
                    text: "Está ação não pode ser alterada.",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Salvar'
                }).then(async (result) => {
                    if (result.value) {
                        await vmGlobal.insertFromAPI({
                            table: 'envioFiscal',
                            data: this.parecer
                        });

                        await vmGlobal.updateFromAPI({
                            table: 'contratos',
                            data: {
                                Status: statusFiscal,
                                PrazoCorrecao: this.parecer.PrazoCorrecao
                            },
                            where: {
                                CodigoContrato: codigoContratoDecode
                            }
                        });

                        if (statusFiscal == 4) {
                            window.location.reload();
                            return;
                        }

                        window.location.href = base_url + 'home/aprovacoesPendentes';
                    }
                })
            },
            loadParecer(val) {
                this.showParecer = true;

                this.selectedParecer.StatusFiscal = val.StatusFiscal;
                this.selectedParecer.NomeUsuario = val.NomeUsuario;
                this.selectedParecer.DataParecer = (val.DataParecer) ? moment(val.DataParecer).format('DD/MM/YYYY') : '';
                this.selectedParecer.DataEnvio = (val.DataEnvio) ? moment(val.DataEnvio).format('DD/MM/YYYY') : '';
                this.selectedParecer.AntigaDataCorrecao = (val.AntigaDataCorrecao) ? moment(val.AntigaDataCorrecao).format('DD/MM/YYYY') : '';
                this.selectedParecer.PrazoCorrecao = (val.PrazoCorrecao) ? moment(val.PrazoCorrecao).format('DD/MM/YYYY') : '';

                if (CKEDITOR.instances['txtParecerTecnicoView']) CKEDITOR.instances['txtParecerTecnicoView'].destroy();
                this.$nextTick(() => {
                    CKEDITOR.replace('txtParecerTecnicoView', {
                        height: '400px',
                        readOnly: true
                    });
                });

                this.$nextTick(() => {
                    CKEDITOR.instances['txtParecerTecnicoView'].setData(String(atob(val.Parecer)));
                });
            },
            async getParecer() {
                await axios.post(base_url + 'ParecerFiscal/getParecerConfigAdmContrato', $.param({
                        CodigoContrato: codigoContratoDecode
                    }))
                    .then((response) => {
                        this.listaParecer = response.data.parecer;
                        this.usuario = response.data.usuario;
                        this.contrato = response.data.contrato;

                        this.refreshTable('#tblParecerFiscal');
                    });
            },
            constroiTable(id = "", pageLength = 10) {
                $('table' + id, this.$el).DataTable({
                    language: translateDataTable,
                    lengthChange: false,
                    destroy: true,
                    pageLength: pageLength
                });
            },
            async refreshTable(id = "") {
                $('table' + id, this.$el).DataTable().destroy();
                await this.$nextTick(() => {
                    this.constroiTable();
                });
            },
            formataData(val) {
                if (val) {
                    return moment(val).format('DD/MM/YYYY');
                }

                return '';
            },
            contratoAtrazado(val) {
                if (val) {
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var prazoCorrecao = moment(val).toDate();
                    prazoCorrecao.setHours(0, 0, 0, 0);

                    return prazoCorrecao.getTime() < today.getTime();
                }

                return false;
            },
        },
        mounted() {
            this.getParecer();
        },
    });
</script>