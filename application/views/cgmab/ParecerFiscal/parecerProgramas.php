<style>
    .badge-correcao {
        background-color: #e8846d;
        color: #fff;
    }
</style>
<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Contratos</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url('home'); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item">Aprovação Configurações Técnicas dos Programas</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid" id="validacaoprogramas">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card">
                    <div class="body table-responsive">
                        <table class="table table-sm table-hover table-stripped comact" style="width: 100%;" id="tblValidacaoProgramas">
                            <thead>
                                <tr>
                                    <th>Contrato</th>
                                    <th>Fiscal</th>
                                    <th>Tema</th>
                                    <th>Subtipo</th>
                                    <th>Data do Envio</th>
                                    <th>Data da Validação</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="prog in programas">
                                    <td>{{prog.Numero}}</td>
                                    <td>{{prog.Fiscal}}</td>
                                    <td>{{prog.NomeTema}}</td>
                                    <td>{{prog.NomeSubtipo}}</td>
                                    <td>{{formatarData(prog.DataEnvio)}}</td>
                                    <td>{{formatarData(prog.DataParecer)}}</td>
                                    <td><span class="badge " :class="getClassStatus(prog.Status)">{{getStatus(prog.Status)}}</span></td>
                                    <td>
                                        <button class="btn btn-outbtn btn-sm btn-outline-secondary" @click="openPrograma(prog)">
                                            <i class="far fa-folder-open"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const validacao = new Vue({
        el: '#validacaoprogramas',
        data() {
            return {
                programas: []
            }
        },
        methods: {
            async getProgramas() {
                await axios.post(base_url + 'ParecerFiscal/getListaProgramasContrato')
                    .then(async (response) => {
                        this.programas = response.data.programas;
                        await this.refreshTable('#tblValidacaoProgramas');
                    });
            },
            constroiTable(id = "", pageLength = 10) {
                $('table' + id, this.$el).DataTable({
                    language: translateDataTable,
                    lengthChange: false,
                    destroy: true,
                    pageLength: pageLength
                });
            },
            async refreshTable(id = "") {
                $('table' + id, this.$el).DataTable().destroy();
                await this.$nextTick(() => {
                    this.constroiTable();
                });
            },
            getStatus(val) {
                if (val == 1) {
                    return 'Pendente de Envio';
                } else if (val == 2) {
                    return 'Pendente de Validação';
                } else if (val == 4) {
                    return 'Aprovado';
                } else if (val == 5) {
                    return 'Parcialmente Aprovado';
                }
                return '';
            },
            getClassStatus(val) {
                if (val == 1) {
                    return 'badge-warning';
                } else if (val == 2) {
                    return 'badge-danger';
                } else if (val == 4) {
                    return 'badge-success';
                } else if (val == 5) {
                    return 'badge-correcao';
                }
                return '';
            },
            formatarData(val) {
                if (val) {
                    return moment(val).format('DD/MM/YYYY');
                }

                return '';
            },
            openPrograma(val) {
                var url = '';
                var paramas = '';
                if (val.CodigoSubtipo == 4) {
                    params = '99cfb275a87d383fa2912331e3117e48=';
                    params += vmGlobal.codificarDecodificarParametroUrl(val.CodigoEscopoPrograma, 'encode');
                    params += '&99cfb275a87d383fa2912331e3117e48=';
                    params += vmGlobal.codificarDecodificarParametroUrl(val.CodigoContrato, 'encode');

                    url = base_url + 'ConfiguracaoProgramas/passagemFauna/?' + params;

                } else if (val.CodigoSubtipo == 3) {
                    params = '99cfb275a87d383fa2912331e3117e48=';
                    params += vmGlobal.codificarDecodificarParametroUrl(val.CodigoEscopoPrograma, 'encode');
                    url = base_url + 'ConfiguracaoProgramas/recursosFaunaModulos/?' + params;

                } else if (val.CodigoSubtipo == 5) {
                    params = '99cfb275a87d383fa2912331e3117e48=';
                    params += vmGlobal.codificarDecodificarParametroUrl(val.CodigoEscopoPrograma, 'encode');
                    url = base_url + 'ConfiguracaoProgramas/recursosHidricos/?' + params;
                } else if (val.CodigoSubtipo == 2) {
                    params = '99cfb275a87d383fa2912331e3117e48=';
                    params += vmGlobal.codificarDecodificarParametroUrl(val.CodigoEscopoPrograma, 'encode');
                    params += '&99cfb275a87d383fa2912331e3117e48=';
                    params += vmGlobal.codificarDecodificarParametroUrl(val.CodigoContrato, 'encode');

                    url = base_url + 'ConfiguracaoSupressaoVegetacao/supressaoVegetacao/?' + params;
                }

                window.location.href = url
            }
        },
        mounted() {
            this.getProgramas();
        },
    })
</script>