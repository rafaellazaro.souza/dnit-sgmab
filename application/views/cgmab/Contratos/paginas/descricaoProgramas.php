<div id="descricaoProgramas" class="desc-programas sombra-2 p-5" v-show="mostrarFormulario">
    <button @click="mostrarFormulario = false" class="btn"><i class="fa fa-times"></i></button>
    <div class="row  cp-form">
        <h4 class="  w-100">Descrição dos Programas, Planos Ambientais, Projetos e Condicionantes</h4>

        <div class="input-group col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text">Descrição</span>
            </div>
            <textarea name="editor1" id="Descricao" v-model="dadosDescricao.Descricao" class="form-control" :value="dadosDescricao.Descricao"></textarea>
        </div>
        <div class="input-group col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text">Justificativa</span>
            </div>
            <textarea name="editor2" id="Justificativa" v-model="dadosDescricao.Justificativa" class="form-control"></textarea>
        </div>
        <div class="input-group col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text">Objetivos</span>
            </div>
            <textarea name="editor3" id="Objetivo" v-model="dadosDescricao.Objetivo" class="form-control"></textarea>
        </div>
        <div class="input-group col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text">Metodologia </span>
            </div>
            <textarea name="editor4" id="Metodologia" v-model="dadosDescricao.Metodologia" class="form-control"></textarea>
        </div>
        <div class="input-group col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text">Publico Alvo </span>
            </div>
            <textarea name="editor5" id="PublicoAlvo" v-model="dadosDescricao.PublicoAlvo" class="form-control"></textarea>
        </div>
        <div class="input-group col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text">Atividades Previstas </span>
            </div>
            <textarea name="editor6" id="AtividadesPrevistas" v-model="dadosDescricao.AtividadesPrevistas" class="form-control"></textarea>
        </div>
        <div class="input-group col-12">
            <div class="input-group-prepend">
                <span class="input-group-text">Responsável Pelo Programa</span>
            </div>
            <select v-model="dadosDescricao.CodigoRecursosHumanos" class="form-control show-tick ms select2">
                <option value="">Selecione o Responsável</option>
                <option :value="i.CodigoRecursosHumanos" v-for="i in listaResponsaveis" v-text="i.NomePessoa"></option>
            </select>
        </div>

        <div class="alert alert-warning p-3 w-100 mt-4 mb-4" v-if="erros.length">
            <h4>Erro ao salvar. Por favor Preencha os campos abaixo:</h4>
            <ul>
                <li v-for="i in erros">{{i}}</li>
            </ul>

        </div>

        <div class="col-12 text-right mt-4">
            <button @click="mostrarFormulario = false; erros = {}" class="btn btn-outline-secondary">Cancelar</button>
            <button v-if="vmDadosContrato.habilitarEdicao" @click="checkForm" class="btn btn-secondary" v-text="update ? 'Atualizar' : 'Salvar'"></button>
        </div>
    </div>
    <hr class="divider">
    <div class="row mt-4 p-5" v-show="mostrartabelas">
        <div class="col-12 col-sm-4 border-left border-top border-bottom p-4">

            <!-- formulario para adicionar meta  -->
            <div class="nova-meta sombra-2" v-show="mostrarAddMeta">
                <div class="input-group col-12">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Meta</span>
                    </div>
                    <input v-model="dadosMeta.Meta" class="form-control" required />
                    <button @click="mostrarAddMeta = false" class="btn btn-outline-secondary">Cancelar</button>
                    <button v-if="vmDadosContrato.habilitarEdicao" @click="checkFormMeta()" class="btn btn-secondary">Salvar</button>
                </div>
            </div>
            <!-- fim do formulario para adicionar meta  -->



            <button v-if="vmDadosContrato.habilitarEdicao" @click="mostrarAddMeta = !mostrarAddMeta" class="btn btn-default mr-4"><i class="fa fa-plus"></i> Nova Meta</button>
            <h4>Metas</h4>
            <div class="h-450r">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Meta</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="i in listaMetas">
                            <td v-text="i.CodigoMetaProgramas"></td>
                            <td v-text="i.Meta"></td>
                            <td>
                                <button v-if="vmDadosContrato.habilitarEdicao" type="button" @click="deletar('metas', i.CodigoMetaProgramas)" class="btn btn-outline-danger"><i class="fa fa-times"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-12 col-sm-8 border p-4">
            <button v-if="vmDadosContrato.habilitarEdicao" @click="mostrarAddIndicador = true" class="btn btn-default mr-4"><i class="fa fa-plus"></i> Novo Indicador</button>

            <!-- formulario para adicionar indicadores  -->
            <div class="nova-meta sombra-2" v-show="mostrarAddIndicador">
                <div class="input-group col-12">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Indicador</span>
                    </div>
                    <input v-model="dadosIndicador.Indicador" class="form-control" required />

                </div>
                <div class="input-group col-12 mb-2 mt-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Descrição</span>
                    </div>
                    <textarea v-model="dadosIndicador.Descricao" class="form-control" required></textarea>

                </div>
                <div class="input-group col-12">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Fórmula Cálculo</span>
                    </div>
                    <textarea v-model="dadosIndicador.FormulaCalculo" class="form-control" required></textarea>

                </div>
                <div class="input-group col-12 mt-3">
                    <button @click="mostrarAddIndicador = false" class="btn btn-outline-secondary">Cancelar</button>
                    <button @click="checkFormIndicador()" class="btn btn-secondary">Salvar</button>
                </div>
            </div>
            <!-- fim do formulario para adicionar indicadores  -->

            <h4>Indicadores</h4>
            <div class="h-450r">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Indicador</th>
                            <th>Descrição</th>
                            <th>Forma de Cálculo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="i in listaIndicadores">
                            <td v-text="i.CodigoIndicadoresProgramas"></td>
                            <td v-text="i.Indicador"></td>
                            <td v-text="i.Descricao"></td>
                            <td v-text="i.FormulaCalculo"></td>
                            <td><button v-if="vmDadosContrato.habilitarEdicao" type="button" @click="deletar('indicadores', i.CodigoIndicadoresProgramas)" class="btn btn-outline-danger"><i class="fa fa-times"></i></button></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>


</div>