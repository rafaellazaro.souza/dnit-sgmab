<div class="form-gestao-condicionantes sombra-2" v-show="mostrarAddCondicionantes">
    <h4 class="w-100 mb-4">Licenças/ Autorizações/ Condicionantes</h4>
    <hr class="divider">
    <!-- filtros para condicionates s -->
    <div class="row">
        <div class="input-group mb-3 col-12 col-sm-6 col-md-3  mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Licença</span>
            </div>
            <input class="form-control" v-model="numeroDigitado" placeholder="Digite o número da licença">
            <button @click="getLicencas()" v-if="mostrarBtnBusca" class="btn btn-outline-secondary">Buscar</button>
            <!-- listagem de licenças  -->
            <div class="col-12 sombra-2 mt-3 p-3 lista-licencas h-450r" v-show="mostrarlistaLicencas">
                <div class="list-group">
                    <button type="button" class="list-group-item list-group-item-action active">Selecione uma opção</button>
                    <button v-if="listaLicencas.length == 0" type="button" class="list-group-item list-group-item-action">Nenhuma Licença Encontrada!</button>
                    <button @click="setTiposCondicionantes(i.CodigoLicenca), numeroDigitado = i.NumeroLicenca+' - '+i.NomeOrgaoExpeditor+ ' - '+i.Sigla" type="button" class="list-group-item list-group-item-action" v-for="i in listaLicencas">{{i.NumeroLicenca}} - {{i.NomeOrgaoExpeditor}} - {{i.Sigla}}</button>
                </div>

                <div class="col-12 text-right">
                    <button @click="mostrarlistaLicencas = false" class="btn btn-sm"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- listagem de licenças  -->
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-2  mb-4" v-show="mostrarTipoCondicionante">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Tipo</span>
            </div>
            <select @change="setTipoFiltro($event, 'Tipo')" class="form-control show-tick ms select2">
                <option value="">Selecione uma opção</option>
                <option :value="i.CodigoTipo" v-for="i in listaTiposCondicionantes" v-text="i.NomeTipo"></option>
            </select>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-2  mb-4" v-show="mostrarTema">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Tema</span>
            </div>
            <select @change="getSubTipos($event)" class="form-control show-tick ms select2">
                <option value="">Selecione uma opção</option>
                <option :value="i.CodigoTema" v-for="i in listaTemas" v-text="i.NomeTema"></option>
            </select>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-2  mb-4" v-show="mostrarNomePadrao">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Nome Padrão</span>
            </div>
            <select @change="setTipoFiltro($event, 'Subtipo')" class="form-control show-tick ms select2">
                <option value="">Selecione uma opção</option>
                <option :value="i.CodigoSubtipo" v-for="i in listaSubtipos" v-text="i.NomeSubtipo"></option>
            </select>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-2  mb-4">
            <button @click="cancelarCondicionantes('reset')" class="btn btn-outline-secondary">Reset</button>
        </div>
    </div>

    <!-- listas de condicionates e condicionantes selecionadas  -->
    <div class="row mt-4">
        <div class="col-12 col-sm-6 border-right h-450r p-3">
            <h5> Lista de condicionantes</h5>
            <!-- <button type="button" class="btn btn-sm btn-outline-secondary s-all" title="Adicionar todos"><i class="fa fa-check"></i></button> -->
            <ul class="list-group">
                <li class="list-group-item" v-for="(i, index) in listaCondicionantes">
                <span class="font-weight-bold" v-text="i.NumeroCondicional  +' - ' + i.Titulo"></span>

<p class="visible" v-text="i.Sigla+ '-'+ i.NumeroLicenca"></p>
<p class="visible" v-text="i.NomeTipo +' - '+i.NomeTema+ ' - '+ i.NomeSubtipo"></p>
<p class="visible" v-text="i.Descricao"></p>
                    <p class="w-100 text-right right-link">
                        <button @click="setCondicionanteSelecionada(i, index)" type="button" class="btn btn-sm btn-outline-secondary"><i class="fa fa-check"></i></button>

                    </p>
                </li>

            </ul>
        </div>
        <div class="col-12 col-sm-6 h-450r p-3">
            <h5>Condicionantes selecionadas</h5>
            <ul class="list-group">
                <li class="list-group-item" v-for="(i, index) in listaCondicionantesSelecionadas">
                    <span class="font-weight-bold" v-text="i.NumeroCondicional  +' - ' + i.Titulo"></span>

                    <p class="visible" v-text="i.Sigla+ '-'+ i.NumeroLicenca"></p>
                    <p class="visible" v-text="i.NomeTipo +' - '+i.NomeTema+ ' - '+ i.NomeSubtipo"></p>
                    <p class="visible" v-text="i.Descricao"></p>
                    <p class="w-100 text-right right-link">
                        <template v-if="deletarCondicionantes = false">
                            <button @click="listaCondicionantesSelecionadas.splice(index, 1)" title="Remover" type="button" class="btn btn-sm btn-outline-danger"><i class="fa fa-times"></i></button>
                        </template>
                        <template v-else>
                        <button @click="deletarCondicionantesServicos(i.CodigoEscopoPrograma, index)" title="Deletar" type="button" class="btn btn-sm btn-outline-danger"><i class="fa fa-times"></i></button>
                        </template>

                    </p>
                </li>

            </ul>
        </div>
    </div>



    <div class="col-12 text-right border-top mt-2 pt-2 ">
        <button @click="cancelarCondicionantes()" class="btn btn-outline-secondary">Cancelar</button>
        <button @click="salvarCondicionantesEscopoServicos()" class="btn btn-secondary">Salvar</button>
    </div>
</div>
