<div class="container-fluid" id="escopoServicos" v-if="mostrarTelaEscopoServicos">

    <div class="row mt-5">

        <div class="col-12 card">

            <div class="col-12 text-left">
                <h4 class="w-100 mb-4 border p-3">Escopo de Serviços</h4>
            </div>



            <div class="col-12 text-right">
                <template v-if="false">
                    <button @click="confirmarEnvioFiscal" :class="'btn btn-success btn-sm <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'invisivel' : '' ?>'" :disabled="!desativarEnvioFiscal"><i class="fas fa-clipboard-check"></i> Solicitar Aprovação
                    </button>
                </template>
                <button @click="mostrarAddEscopo = true, iniciarServicos()" v-if="vmDadosContrato.habilitarEdicao && listaEscopoServicos.length != 3" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Adicionar Serviço</button>

            </div>

            <!-- 
            --------------------------------------------------------------------------------
            Adicionar serviços 
            --------------------------------------------------------------------------------

         -->
            <div class="modal-w98-h50 sombra-2 p-5 " v-show="mostrarAddEscopo">

                <div class="row">
                    <div class="col-8">
                        <h4 class="w-100 mb-4">Escopo de Serviço</h4>
                    </div>
                    <div class="col-4 text-right">
                        <button @click="mostrarAddEscopo = false" class="btn"><i class="fa fa-times pull-right"></i></button>
                    </div>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Atividade Ambiental</span>
                    </div>
                    <select v-model="dadosEscopo.CodigoServico" class="form-control show-tick ms select2  ">
                        <option value="">Selecione uma opção</option>
                        <option :value="i.CodigoServico" v-for="i in listaServicos" v-text="i.NomeServico"></option>
                    </select>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Detalhe do Escopo</span>
                    </div>

                    <textarea v-model="dadosEscopo.DetalheEscopo" class="form-control"></textarea>

                </div>

                <div class="col-12 text-right border-top mt-2 pt-2 ">
                    <button @click="mostrarAddEscopo = false" class="btn btn-outline-secondary">Cancelar</button>
                    <button @click="salvarEscopoServicos()" class="btn btn-secondary">Salvar</button>
                </div>


            </div>

            <table class="table table-striped table-bordered mt-3">
                <thead>
                    <tr>
                        <td>Serviço</td>
                        <td>Observações</td>
                        <td><i class="fa fa-plus"></i></td>
                        <td><i class="fa fa-list"></i></td>
                        <td><i class="fa fa-pen"></i></td>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="i in listaEscopoServicos">
                        <td v-text="i.NomeServico"></td>
                        <td v-text="i.DetalheEscopo"></td>
                        <td> <button @click="setCodigoEscopo(i.CodigoEscopo)" v-if="vmDadosContrato.habilitarEdicao" class="btn btn-outline-secondary btn-sm" title="Condicionantes/ Licenças"><i class="fa fa-list"></i> Adicionar Condicionantes</button></td>
                        <td> <button @click="listarCondicionantes(i.CodigoEscopo, i.NomeServico, i.CodigoServico)" id="btn-programas" class="btn btn-outline-secondary btn-sm" title="Condicionantes/ Licenças"><i class="fa fa-gears"></i>

                                <template v-if="i.CodigoServico === 3 || i.CodigoServico === '3'">Ver Programas</template>
                                <template v-else>Ver Condicionantes</template>
                            </button>
                        </td>
                        <td>
                            <button v-if="vmDadosContrato.habilitarEdicao" @click="mostrarEdicaoServico = true; modelEdicao.CodigoEscopo = i.CodigoEscopo; modelEdicao.DetalheEscopo = i.DetalheEscopo" class="btn btn-sm"><i class="fa fa-pen"></i></button>

                            <button v-if="vmDadosContrato.habilitarEdicao" @click="deletar(i.CodigoEscopo)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- *************************************************
            Modal editar servico
            ************************************************* -->
            <div class="modal-w98-h50 col-6 sombra-2 p-5" v-show="mostrarEdicaoServico">
                <div class="row">
                    <div class="col-8">
                        <h4>Editar Descrição de Programa</h4>
                    </div>
                    <div class="col-4 text-right">
                        <button @click="mostrarEdicaoServico = false" class="btn"><i class="fa fa-times"></i></button>
                    </div>

                   
                    <div class="col-12">
                        <textarea class="form-control" rows="24" v-model="modelEdicao.DetalheEscopo"></textarea>
                    </div>
                    <div class="col-12 text-right mt-4  ">
                        <button @click="editarDescricao()" class="btn btn-success"><i class="fa fa-save"></i> Salvar alterações</button>
                    </div>
                </div>
            </div>
            <!-- *************************************************
            Modal editar servico
            ************************************************* -->

        </div>
    </div>

    <?php $this->load->view('cgmab/Contratos/paginas/modalGestaoCondicionates'); ?>
</div>

<?php $this->load->view('cgmab/Contratos/paginas/listagemCondicionantesEscopo'); ?>