<div class="modal fade bd-example-modal-xl" id="dadosBasicos" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content p-5">


            <div class="row" id="configurar">

                <h3 class="w-100 mb-4">Dados Básicos</h3>

                <div class="input-group mb-3 col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Edital</span>
                    </div>
                    <input type="text" class="form-control" disabled v-model="dadosContrato.Edital">
                </div>
                <div class="input-group mb-3 col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Lote</span>
                    </div>
                    <input type="text" disabled v-model="dadosContrato.Lote" class="form-control">
                </div>
                <div class="input-group mb-3 col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Número SEI</span>
                    </div>
                    <input type="text" disabled v-model="dadosContrato.NumeroSei" class="form-control">
                </div>
                <div class="input-group mb-3 col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Link Processo SEI</span>
                    </div>
                    <input type="text" class="form-control" v-model="dadosContrato.LinkProcessoSEI">
                </div>
                <div class="input-group mb-3 col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Número SEI Edital</span>
                    </div>
                    <input type="text" class="form-control" v-model="dadosContrato.NumeroSeiEdital">
                </div>
                <div class="input-group mb-3 col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Numero SEI Proposta Aprovada pela Empresa</span>
                    </div>
                    <input type="text" class="form-control" v-model="dadosContrato.NumeroSeiPropostaAprovadaEmpresa">
                </div>
                <div class="input-group mb-3 col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Numero SEI Apolice Caução Garantia</span>
                    </div>
                    <input type="text" class="form-control" v-model="dadosContrato.NumeroSeiApoliceCaucaoGarantia">
                </div>
                <!-- <div class="input-group mb-3 col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Tipo de Contrato</span>
                    </div>
                    <select class="form-control show-tick ms select2" v-model="dadosContrato.TipoContrato">
                        <option selected>Selecione o Tipo</option>
                        <option :value="'Convênio'">Convênio</option>
                        <option :value="'TED'">TED</option>
                        <option :value="'Contrato'">Contrato</option>
                    </select>
                </div> -->
                <div class="input-group mb-3 col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Numero SEI do Contrato Assinado</span>
                    </div>
                    <input type="text" class="form-control" v-model="dadosContrato.NumeroSeiContratoAssinado">
                </div>
                <div class="input-group mb-3 col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Numero SEI da Apolice Caução</span>
                    </div>
                    <input type="text" class="form-control" v-model="dadosContrato.NumeroSeiApoliceCaucao">
                </div>



                <template v-if="dadosContrato.CodigoEmpreendimento">
                    <h3 class="w-100 mb-4 mt-4">Dados do Empreendimento</h3>
                    <div class="col-12 text-right">
                        <button @click="CodigoEmpreendimentoBkp = dadosContrato.CodigoEmpreendimento; dadosContrato.CodigoEmpreendimento = ''; mostrarEdicaoEmpreendimento = true" class="btn btn-sm"><i class="fa fa-pen"></i></button>
                    </div>
                    <ul class="list-group w-100">
                        <li class="list-group-item">UF: {{dadosContrato.UF}}</li>
                        <li class="list-group-item">Empreendimento: {{dadosContrato.Sigla}}</li>
                    </ul>

                </template>
                <template v-else>
                    <h3 class="w-100 mb-4 mt-4">Selecione o Empreendimento</h3>
                    <div class="col-12 text-right">
                        <button @click="dadosContrato.CodigoEmpreendimento = CodigoEmpreendimentoBkp ;CodigoEmpreendimentoBkp = ''; mostrarEdicaoEmpreendimento = false" class="btn btn-sm"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="input-group mb-3 col-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">UF</span>
                        </div>
                        <select @change="filtrarEmpreendimentos($event)" class="form-control show-tick ms select2">
                            <option selected>Selecione um estado</option>
                            <option :value="'AC'">Acre</option>
                            <option :value="'AL'">Alagoas</option>
                            <option :value="'AP'">Amapá</option>
                            <option :value="'AM'">Amazonas</option>
                            <option :value="'BA'">Bahia</option>
                            <option :value="'CE'">Ceará</option>
                            <option :value="'DF'">Distrito Federal</option>
                            <option :value="'ES'">Espírito Santo</option>
                            <option :value="'GO'">Goiás</option>
                            <option :value="'MA'">Maranhão</option>
                            <option :value="'MT'">Mato Grosso</option>
                            <option :value="'MS'">Mato Grosso do Sul</option>
                            <option :value="'MG'">Minas Gerais</option>
                            <option :value="'PA'">Pará</option>
                            <option :value="'PB'">Paraíba</option>
                            <option :value="'PR'">Paraná</option>
                            <option :value="'PE'">Pernambuco</option>
                            <option :value="'PI'">Piauí</option>
                            <option :value="'RJ'">Rio de Janeiro</option>
                            <option :value="'RN'">Rio Grande do Norte</option>
                            <option :value="'RS'">Rio Grande do Sul</option>
                            <option :value="'RO'">Rondônia</option>
                            <option :value="'RR'">Roraima</option>
                            <option :value="'SC'">Santa Catarina</option>
                            <option :value="'SP'">São Paulo</option>
                            <option :value="'SE'">Sergipe</option>
                            <option :value="'TO'">Tocantins</option>
                            <option :value="'EX'">Estrangeiro</option>
                        </select>
                    </div>
                    <div class="input-group mb-3 col-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Empreendimento</span>
                        </div>
                        <select v-model="dadosContrato.CodigoEmpreendimento" class="form-control show-tick ms select2">
                            <option selected>Selecio um Empreendimento</option>
                            <option :value="i.CodigoEmpreendimento" v-for="i in listaEmpreendimentos">{{i.Sigla}}</option>
                        </select>
                    </div>

                </template>


                <h3 class="w-100 mb-4 mt-4">Dados dos Fiscais</h3>
                <div class="col-md-12 mb-2">
                    <button class="btn btn-sm btn-primary float-md-right" type="button" @click="novoFiscal()" v-if="!hasAllFiscais">Novo Fiscal</button>
                    <form action="" class="row" @submit.prevent="salvarFiscal()" v-if="showFormFiscal" id="formFiscal">
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">CPF</span>
                                </div>
                                <input required type="text" class="form-control" maxlength="11" :class="isValidCpf" v-model="fiscalSelected.CPF">
                            </div>
                            <span>*Informe somente os números</span>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon2">Telefone</span>
                                </div>
                                <input type="text" class="form-control is-disabled" v-model="fiscalSelected.Telefone">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon2">Atribuição</span>
                                </div>
                                <select class="form-control show-tick ms select2" v-model="fiscalSelected.Tipo" :disabled="disableTipoFiscal" required>
                                    <option value=""></option>
                                    <option v-for="tipo in tiposFiscal" :value="tipo">{{tipo}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 mt-2">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon3">Nome Fiscal</span>
                                </div>
                                <input type="text" required class="form-control is-disabled" v-model="fiscalSelected.NomeUsuario">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon3">E-mail</span>
                                </div>
                                <input type="email" required class="form-control is-disabled" v-model="fiscalSelected.Email">
                            </div>
                            <button class="btn btn-sm btn-danger float-right" type="button" @click="showFormFiscal = false">Cancelar</button>
                            <button class="btn btn-sm btn-success float-right" type="submit">Salvar</button>
                        </div>
                    </form>
                </div>

                <div class="input-group mb-3 col-12 h-450r">
                    <table class="table table-striped">
                        <thead class="bg-1">
                            <th>Atribuição</th>
                            <th>CPF</th>
                            <th class="w-50">Fiscal</th>
                            <th>E-mail</th>
                            <th>Telefone</th>
                            <th v-show="!isValidado">-</th>
                        </thead>
                        <tbody>
                            <tr v-for="i in listaFiscais">
                                <td>{{i.Tipo}}</td>
                                <td>{{i.CPF}}</td>
                                <td>{{i.NomeUsuario}}</td>
                                <td>{{i.Email}}</td>
                                <td>{{i.Telefone}}</td>
                                <td v-show="!isValidado"><button class="btn btn-outline-secondary btn-sm" @click="selectFiscal(i)"><i class="fa fa-edit"></i></button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h3 class="w-100 mb-4 mt-4">Dados de Aditivos</h3>
                <div class="input-group mb-3 col-12 h-450r">
                    <table class="table table-striped">
                        <thead class="bg-1">
                            <th>NumeroAdtivo</th>
                            <th class="w-25">Objeto</th>
                            <th>Data Reinício</th>
                            <th>Valor</th>
                            <th>Número SEI</th>
                        </thead>
                        <tbody>
                            <tr v-for="i in listaAditivos">
                                <td>{{i.NumeroAdtivo}}</td>
                                <td>{{i.Objeto}}</td>
                                <td>{{i.DataReinicio}}</td>
                                <td>{{formatReal(i.ValorAditivo)}}</td>
                                <td>
                                    <input type="text" :value="i.NumeroSeiAditivo" class="form-control" @change="atualizar(i.CodigoAditivo, $event, 'aditivos')" placeholder="Digite aqui o número SEI">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h3 class="w-100 mb-4 mt-4">Dados de Apostilamento</h3>
                <div class="input-group mb-3 col-12 h-450r">
                    <table class="table table-striped">
                        <thead class="bg-1">
                            <th>Numero</th>
                            <th>Valor</th>
                            <th>Numero SEI</th>
                        </thead>
                        <tbody>
                            <tr v-for="i in listaReajustes">
                                <td>{{i.NumeroReajuste}}</td>
                                <td>{{formatReal(i.ValorReajuste)}}</td>
                                <td>
                                    <input type="text" :value="i.NumeroSeiApostilamento" class="form-control" @change="atualizar(i.CodigoApostilamento, $event, 'apostilamentos')" placeholder="Digite aqui o número SEI">
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="alert alert-warning p-3 w-100" v-if="mostrarContratadas === false || mostrarContratadas === 'false'">
                    <p>Existem Construtoras para associar a este contrato?</p>
                    <button class="btn btn-outline-secondary">Não</button>
                    <button @click="mostrarContratadas = true" class="btn btn-secondary">Sim</button>
                </div>
                <div class="mt-2 row p-4" v-if="mostrarContratadas || listaConstrutorasAssociadas.length > 0">
                    <h3 class="w-100 mb-4">
                        Lista de Contratos Associados a Supervisão Ambiental
                        <br>
                        <small>Contratos Construtoras</small>
                    </h3>

                    <div class="input-group mb-3 col-12 col-sm-6 col-md-8 col-xl-10">
                        <div class="input-group-prepend">
                            <span id="basic-addon1" class="input-group-text">Digite o numero do contrato</span>
                        </div>
                        <input v-model="vmInserirContratos.numeroDigitado" type="text" class="form-control">
                        <button class="btn btn-outline-secondary m-0" @click="getContratos('ContratosAssociados')">Buscar</button>
                    </div>

                    <div class="modal-w98-h50 sombra-2 p-5" v-show="mostrarListaContratos">
                        <button class="btn btn-outline-secondary btn-r" @click="mostrarListaContratos = false"><i class="fa fa-times"></i></button>

                        <div class="alert alert-info p-3" v-if="listaContratos.length === 0 || listaContratos.length === '0'">
                            <p>Nenhum contrato encontrado com o número digitado</p>
                        </div>

                        <div class="list-group mt-2" v-else>
                            <h4>Selecione o Contrato</h4>
                            <button type="button" class="list-group-item list-group-item-action" v-for="i in listaContratos" @click="salvarContrato(i)">
                                <h6> {{i.NU_CON_FORMATADO}} - {{i.NO_EMPRESA}} - {{i.SG_UND_GESTORA}}</h6>
                                <p>{{i.DS_OBJETO}}</p>

                            </button>
                        </div>
                    </div>

                    <div class="col-12 mt-3">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Número Contrato</th>
                                    <th>Empresa</th>
                                    <th>CNPJ</th>
                                    <th>Objeto</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="i in listaConstrutorasAssociadas">
                                    <td v-text="i.NumeroContrato"></td>
                                    <td v-text="i.NomeEmpresa"></td>
                                    <td v-text="i.CnpjEmpresa"></td>
                                    <td v-text="i.ObjetoContrato"></td>
                                    <td><button @click="deletarContrutora(i.CodigoContrutorasContratos)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>

                </div>


                <div class="w-100 text-right mt-5 border-top">
                    <button class="btn btn-outline-secondary mt-2" data-dismiss="modal" aria-label="Close">Cancelar</button>


                    <template v-if="parseInt(dadosContrato.Status) == 3">
                        <button class="btn btn-outline-success ml-2 mt-2" @click="dadosContrato.Status = 1 ;atualizarContrato()">Salvar e enviar para Contratada</button>
                    </template>

                    <template v-else>
                        <button class="btn btn-outline-success ml-2 mt-2" @click="atualizarContrato()">Salvar Alterações</button>
                    </template>

                </div>
            </div>


        </div>
    </div>
</div>