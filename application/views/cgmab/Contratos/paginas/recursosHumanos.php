<div class="container-fluid" id="recursosHumanos">
    <div class="row p-4 mt-4">
        <div class="col-12">
            <h4 class="w-100 mb-4 border p-3">Recursos Humanos:</h4>
        </div>

        <div class="col-8">
            <div class="btn-group" role="group" aria-label="Grupo de botões com dropdown aninhado">
                <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-plus"></i> {{NomeServico ? NomeServico : 'Selecione o Serviço para Gerenciar Profissionais'}}
                    </button>
                    <div class="dropdown-menu" style="left: -112px;" aria-labelledby="btnGroupDrop1">
                        <a @click="codigoServico = i.CodigoServico; codigoEscopoServico= i.CodigoEscopo; NomeServico = i.NomeServico " class="dropdown-item" href="javascript:;" v-for="i in listaServicos" v-text="i.NomeServico"></a>
                    </div>
                </div>
            </div>

            <div v-if="vmDadosContrato.habilitarEdicao" class="input-group mt-2">
                <div class="input-group-prepend" v-show="codigoServico != ''">
                     <span id="basic-addon1" class="input-group-text">Digite o CPF</span>
                </div>
                <input class="form-control" v-model="cpfDigitado" v-show="codigoServico != ''" placeholder="Apenas Numeros" />
                <button @click="getRecursosHumanos()" class="btn btn-secondary" v-show="codigoServico != ''">Selecionar Profissional</button>
                <button @click="novoRecurso()" class="btn btn-outline-secondary ml-2 " v-show="codigoServico != ''"><i class="fa fa-plus"></i> Gerenciar Meus Profissionais</button>
            </div>

        </div>
        <div class=" mb-3 col-12 col-sm-4">

            <!-- ===================================== 
                    listar resultados da pesquisa por cpf
                ===================================== -->
            <div class="mt-4 sombra-2 modal-w98-h50 p-4" v-show="mostraResultadoPesquisa">
                <div class="list-group">
                    <button type="button" class="list-group-item list-group-item-action">
                        Selecione um Profissional
                    </button>
                    <!-- lista com profissional filtrado por cpf  -->
                    <button @click="codigoServico != 3 ? salvarRecursoHumanoEscopo(i.CodigoRecursosHumanos) : getCondicionantes(i.CodigoRecursosHumanos) " v-for="i in listaRecursosHumanos" type="button" class="list-group-item list-group-item-action">
                        {{i.NomePessoa}}
                        <!--<span class="badge badge-warning navbar-badge p-2" v-show="mostrarCondicionantes" v-text="dadosEscopoRecursos.length">0</span>-->
                    </button>
                    <!-- lista com profissional filtrado por cpf  -->

                    <button @click="novoRecurso()" type="button" v-if="listaRecursosHumanos.length == 0" class="p-2 btn btn-outline-secondary">
                        <h4>Nenhum Profissional encontrado com o CPF Digitado!</h4>
                        Você pode adicionar um novo profissional clicando aqui!
                    </button>

                </div>
                <div class="mt-4 p-2" v-if="mostrarCondicionantes">
                    <h5 v-if="listaCondicionantes.length > 0">Selecione os Programas</h5>
                    <table class="table table-striped" v-if="listaCondicionantes.length > 0">
                        <tr v-for="(i, index) in listaCondicionantes">
                            <td v-text="i.CodigoCondicionante +' - '+ i.Titulo"></td>
                            <td v-text="i.NomeTipo"></td>
                            <td v-text="i.CodigoTipo === 1 || i.CodigoTipo === '1' ? '-' : i.NomeTema"></td>
                            <td v-text="i.CodigoTipo === 1 || i.CodigoTipo === '1' ? '-' : i.NomeSubtipo"></td>
                            <td><button @click="associarProfissionalCondicionante(i.CodigoCondicionante, index, i)" class="btn btn-default btn-sm"><i class="fa fa-check"></i></button></td>
                        </tr>
                    </table>

                    <div class="mt-3" v-if="listaCondicionantesSelecionadas.length > 0">
                        <hr class="divider">
                        <h5>Programas Selecionados</h5>
                        <table class="table table-striped">
                            <tr v-for="(i, index) in listaCondicionantesSelecionadas">
                                <td v-text="i.CodigoCondicionante +' - '+ i.Titulo"></td>
                                <td v-text="i.NomeTipo"></td>
                                <td v-text="i.CodigoTipo === 1 || i.CodigoTipo === '1' ? '-' : i.NomeTema"></td>
                                <td v-text="i.CodigoTipo === 1 || i.CodigoTipo === '1' ? '-' : i.NomeSubtipo"></td>
                                <td><button @click="removerAssociacaoCondicionates(index, i)" class="btn btn-outline-danger btn-sm"><i class="fa fa-minus"></i></button></td>
                            </tr>
                        </table>

                    </div>

                </div>

                <button @click="salvarPessoaExecucao()" v-if="mostrarCondicionantes" class="btn btn-default r0 mt-5 mr-5">Salvar</button>
                <button @click="resetarListaPessoasCondicionantes()" class="btn btn-default r0 mt-5"><i class="fa fa-times"></i></button>
            </div>
            <!-- ===================================== 
                    fim da listagem de resultados da pesquisa por cpf
                ===================================== -->

        </div>


        <div class="col-12 mt-4">
            <table class="table table-striped " id="listaDeProfissionaisNoEscopo">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Cargo</th>
                        <th>Serviço Associado/Programa</th>
                        <th></th>
                    </tr>

                </thead>
                <tbody>
                    <tr v-for="i in listaRecursosHumanosServico">
                        <td v-text="i.NomePessoa"></td>
                        <td v-text="i.CPF"></td>
                        <td v-text="i.Profissao"></td>
                        <td v-if="i.NomeSubtipo" v-text="i.NomeServico+'/ '+ i.NomeSubtipo"></td>
                        <td v-else v-text="i.NomeServico"></td>
                        <td>
                            <button @click="getAnexosRecurso(i.CodigoRecursosHumanos)" class="btn btn-info btn-sm" title="Ver Documentos"><i class="fas fa-list-ul"></i></button>
                            <button v-if="vmDadosContrato.habilitarEdicao" @click="deletarRecurso(i.CodigoRecurso)" class="btn btn-outline-danger btn-sm"><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <!-- **********************************
        Modal documentos
        ********************************** -->
        <div class="modal-w98-h50 sombra-2 p-5" v-show="mostrarDocumentos">
            <div class="row">
                <div class="col-8">
                    <h4>Documentos Cadastrados do Usuário</h4>
                </div>
                <div class="col-4 text-right">
                    <button @click="mostrarDocumentos = false" class="btn"><i class="fa fa-times"></i></button>
                </div>
                <div class="col-12">
                    <table class="table table-striped ">
                        <thead>
                            <th>Descrição</th>
                            <th><i class="fa fa-download"></i></th>
                        </thead>
                        <tbody>
                            <tr v-for="i in listaAnexosRecurso">
                                <td v-text="i.Descricao"></td>
                                <td><a :href="i.Caminho" target="_blank"><i class="fa fa-download"></i></a></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- **********************************
        Modal documentos
        ********************************** -->
    </div>

</div>