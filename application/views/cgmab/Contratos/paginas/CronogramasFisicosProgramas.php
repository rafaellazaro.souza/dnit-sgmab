<div id="cronogramasFisicos" class="desc-programas sombra-2 p-5" v-if="mostrarCronograma">
    <button v-if="mostrarAtividades === false" @click="mostrarCronograma = false" class="btn"><i class="fa fa-times"></i></button>
    <button v-else @click="mostrarBtnAddCronograma = true; mostrarAtividades = false; dadosCronograma={}; divideColuna = 'col-12 mt-4 p-3'" class="btn">Fechar Atividades</button>
    <div class="row">
        <h4 class="  w-100">Cronogramas Físicos dos Programas</h4>
        <button @click="mostrarAddCronograma = true;" v-if="listaCronogramas.length === 0" class="btn btn-primary" v-if="mostrarBtnAddCronograma"><i class="fa fa-plus"></i> Adicionar Cronograma</button>

    </div>
    <div class="row mt-2">

        <!-- Formulario para adicionar cronogramas  -->

        <div class="nova-meta sombra-2 row" v-show="mostrarAddCronograma">
            <div class="alert alert-warning p-3 w-100 mb-4" v-if="errosFormCronograma.length">
                <h4>Erro ao salvar. Por favor Preencha os campos abaixo:</h4>
                <ul>
                    <li v-for="i in errosFormCronograma">{{i}}</li>
                </ul>

            </div>
            <div class="input-group col-12 col-sm-4">
                <div class="input-group-prepend">
                    <span class="input-group-text">Duração do Programa (meses)</span>
                </div>
                <input type="number" v-model="dadosCronograma.DuracaoPrograma" class="form-control">
            </div>
            <div class="input-group col-12 col-sm-4">
                <div class="input-group-prepend">
                    <span class="input-group-text">Previsão de Início</span>
                </div>
                <input type="date" v-model="dadosCronograma.PrevisaoInicio" class="form-control">
            </div>
            <div class="input-group col-12 col-sm-4">
                <div class="input-group-prepend">
                    <span class="input-group-text">Periodicidade <br> relatório técnico</span>
                </div>
                <select v-model="dadosCronograma.Periodicidade" class="form-control">
                    <option value="Mensal">Mensal</option>
                    <option value="Bimestral">Bimestral</option>
                    <option value="Trimestral">Trimestral</option>
                    <option value="Quadrimestral">Quadrimestral</option>
                    <option value="Semestral">Semestral</option>
                    <option value="Anual">Anual</option>
                </select>
            </div>
            <div class="input-group col-12 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Observações</span>
                </div>
                <textarea v-model="dadosCronograma.Observacoes" class="form-control" aria-label="With textarea"></textarea>
            </div>
            <div class="input-group col-12 mt-3">
                <button @click="mostrarAddCronograma = false; dadosCronograma={}; errosFormCronograma = false" class="btn btn-outline-secondary">Cancelar</button>
                <button @click="checkForm(false)" class="btn btn-secondary">Salvar</button>
            </div>
        </div>

        <!-- Formulario para adicionar cronogramas  -->


        <!-- listagem de cronogramas adicionados  -->
        <div :class="divideColuna">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Cod.</th>
                        <th>Previsão Início</th>
                        <th>Duração (Meses)</th>
                        <th>Periodiciade dos Relatórios técnico</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(i, index) in listaCronogramas" :class="marcador === index ? 'marcador' : ''">
                        <td v-text="i.CodigoCronogramaFisicoProgramas"></td>
                        <td v-text="vmGlobal.frontEndDateFormat(i.PrevisaoInicio)"></td>
                        <td v-text="i.DuracaoPrograma"></td>
                        <td v-text="i.Periodicidade"></td>
                        <td><button @click="getDadosCronograma(i); divideColuna= 'col-12 col-sm-6 col-md-4 col-xs-3  mt-4 p-3 max-h400'; mostrarAtividades = true; marcador = index" class="btn btn-sm btn-secondary">Gerenciar Atividades</button></td>
                    </tr>
                </tbody>
            </table>

        </div>
        <!-- listagem de cronogramas adicionados  -->


        <!-- atividades de um cronograma  -->
        <div class="col-12 col-sm-6 col-md-8 col-xs-9 pl-5" v-show="mostrarAtividades">
            <div class="row border-bottom" v-show="mostrarAtualizarCronograma">
                <h4 class="w-100">Adicionar Atividade</h4>
                <div class="input-group col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Duração do Programa (meses)</span>
                    </div>
                    <input type="number" v-model="dadosCronograma.DuracaoPrograma" class="form-control">
                </div>
                <div class="input-group col-12 col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Previsão de Início</span>
                    </div>
                    <input type="date" v-model="dadosCronograma.PrevisaoInicio" class="form-control">
                </div>
                <div class="input-group col-12 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Observações</span>
                    </div>
                    <textarea v-model="dadosCronograma.Observacoes" class="form-control" aria-label="With textarea"></textarea>
                </div>
                <div class="input-group col-12 mt-3">
                    <button @click="checkForm(true)" class="btn btn-secondary">Atualizar</button>
                </div>
            </div>
            <button v-if="vmDadosContrato.habilitarEdicao" @click="mostrarAddAtividade = true" class="btn btn-primary btn-r"><i class="fa fa-plus"></i> Adicionar Atividade</button>

            <!-- formulario para adicionar atividades no cronograma  -->
            <div class="nova-meta sombra-2 row p-5" v-show="mostrarAddAtividade">

                <div class="alert alert-warning p-3 w-100 mb-4" v-if="errosFormAtividade.length">
                    <h4>Erro ao salvar. Por favor Preencha os campos abaixo:</h4>
                    <ul>
                        <li v-for="i in errosFormAtividade">{{i}}</li>
                    </ul>

                </div>
                <div class="input-group col-12 col-sm-6 pt-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Atividade</span>
                    </div>
                    <input type="text" v-model="dadosAtividade.Atividade" class="form-control">
                </div>
                <div class="input-group col-12 col-sm-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Mês da Atividade</span>
                    </div>
                    <input type="number" min="1" max="12" v-model="dadosAtividade.MesAtividade" class="form-control">
                </div>
                <div class="input-group col-12 col-sm-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Ano da Atividade</span>
                    </div>
                    <input type="number" v-model="dadosAtividade.AnoAtividade" class="form-control">
                </div>
                <div class="input-group col-12 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Descrição</span>
                    </div>
                    <textarea v-model="dadosAtividade.Descricao" class="form-control" aria-label="With textarea"></textarea>
                </div>
                <div class="col-12 mt-4">

                    <input type="radio" v-model="dadosAtividade.PossuiRelatorio" name="relatorio" :value="1">
                    <label for="relatorio">Atividade Tipo Relatório</label>

                    <input type="radio" v-model="dadosAtividade.PossuiRelatorio" name="relatorio" :value="2" checked>
                    <label for="relatorio">Atividade Tipo Ação</label>

                </div>
                <div class="input-group col-12 mt-3">
                    <button @click="mostrarAddAtividade = false; dadosAtividade = {}; errosFormAtividade = false " class="btn btn-outline-secondary">Cancelar</button>
                    <button @click="checkFormAtividade(true)" v-if="this.dadosAtividade.CodigoAtividadeCronogramaFisico" class="btn btn-secondary">Atualizar</button>
                    <button v-if="vmDadosContrato.habilitarEdicao" @click="checkFormAtividade(false)" v-else class="btn btn-secondary">Salvar</button>
                </div>
            </div>
            <!-- formulario para adicionar atividades no cronograma  -->


            <h4 class="mt-5">Atividades Cadastradas</h4>

            <div class="max-h400">
                <table class="table table-striped mt-4">
                    <thead>
                        <tr>
                            <th>Mês da Atividade</th>
                            <th>Atividade</th>
                            <th>Descrição</th>
                            <th>Tipo Atividade</th>
                            <th>Status</th>
                            <th class="w-25"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(i, index) in listaAtividades">
                            <td v-text="i.MesAtividade+'/'+ i.AnoAtividade"></td>
                            <td v-text="i.Atividade"></td>
                            <td v-text="i.Descricao"></td>
                            <td v-if="i.PossuiRelatorio === '2' || i.PossuiRelatorio === '2'" v-text="'Ação'"></td>
                            <td v-else v-text="'Relatório'"></td>
                            <td v-text="i.Status"></td>
                            <td>
                                <button v-if="vmDadosContrato.habilitarEdicao" @click="getDadosAtividade(i)" class="btn btn-outline-secondary btn-sm"><i class="fa fa-pen"></i></button>
                                <button v-if="vmDadosContrato.habilitarEdicao" @click="deletar('atividadesDoCronograma', i.CodigoAtividadeCronogramaFisico);" class="btn btn-outline-danger btn-sm"><i class="fa fa-times"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>


        </div>
        <!-- atividades de um cronograma  -->

    </div>

</div>