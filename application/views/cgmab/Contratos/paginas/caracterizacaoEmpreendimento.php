<?php $this->load->view('cgmab/Contratos/css/cssCaracterizacao') ?>
<div class="container-fluid" id="uploadShapeFile">
    <div class="row ">
        <h4 class="w-100 mb-4 border p-3">Caracterização do Empreendimento <br />
            <small>Cadastro de OACs, OAEs, AUAs</small>
        </h4>
        <div class="col-12">
            <div class="mt-3 mb-5" v-if="vmDadosContrato.habilitarEdicao">
                <div class="header ">
                    <h2><strong>Anexar</strong> Arquivo</h2>
                </div>
                <div class="body no-shadow border">
                    <p>Apenas arquivos no formato XLSX</p>
                    <input type="file" id="file" ref="file" class="dropify" data-allowed-file-extensions="xlsx">
                    <a href="<?= base_url('webroot/uploads/modelos-excel/modelo-oacs-auas.xlsx') ?>" class="btn btn-outline-secondary">Baixar Modelo</a>
                    <button @click="importar()" class="btn btn-outline-secondary btn-r">Enviar Arquivo</button>
                </div>
            </div>
        </div>

        <!--======================================================
            modal importação de áreas de apoio 
        ====================================================== -->
        <div class="modalImportacao col-10 sombra-2 p-4 h-450r" v-if="mostrarListaExcel">
            <h6>Importação de dados</h6>
            <div class="alert alert-info">
                <p>Após verificar os dados clique no botão salvar</p>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Estrutura</th>
                        <th>Tipo de Estrutura</th>
                        <th>Nome</th>
                        <th>Altura</th>
                        <th>Largura</th>
                        <th>Diâmetro</th>
                        <th>UF</th>
                        <th>BR</th>
                        <th>Tipo Trecho</th>
                        <th>KM Inicial</th>
                        <th>KmFinal</th>
                        <th>Estaca</th>
                        <th>Latidude</th>
                        <th>Longitude</th>
                        <th>Observacao</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="i in listaExel">
                        <td v-text="i.Estrutura"></td>
                        <td v-text="i.TipoEstrutura"></td>
                        <td v-text="i.Nome"></td>
                        <td v-text="i.Altura"></td>
                        <td v-text="i.Largura"></td>
                        <td v-text="i.Diametro"></td>
                        <td v-text="i.UF"></td>
                        <td v-text="i.BR"></td>
                        <td v-text="i.TipoTrecho"></td>
                        <td v-text="i.KmInicial"></td>
                        <td v-text="i.KmFinal"></td>
                        <td v-text="i.NumeroEstaca"></td>
                        <td v-text="i.Latitude"></td>
                        <td v-text="i.Longitude"></td>
                        <td v-text="i.Observacao"></td>
                    </tr>
                </tbody>
            </table>

            <button @click="window.location.reload()" class="btn btn-outline-primary btn-r mr-5 pr-5">Cancelar</button>
            <button v-if="vmDadosContrato.habilitarEdicao" @click="salvarEstruturas()" class="btn btn-primary btn-r">Salvar</button>

        </div>

        <!--======================================================
modal importação de áreas de apoio 
====================================================== -->

        <div class="col-12">
            <table class="table table-striped" id="ListaAreasApoios">
                <thead>
                    <tr>
                        <th>Estrutura</th>
                        <th>Tipo Estrutura</th>
                        <th>Nome</th>
                        <th>UF</th>
                        <th>BR</th>
                        <th>Tipo Trecho</th>
                        <th>KM Inicial</th>
                        <th>KmFinal</th>
                        <th>Estaca</th>
                        <th>Obs</th>
                        <th>Dimensões</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(i, index) in listaAreasApoio">
                        <td v-text="i.Estrutura"></td>
                        <td v-text="i.TipoEstrutura"></td>
                        <td v-text="i.Nome"></td>
                        <td v-text="i.UF"></td>
                        <td v-text="i.BR"></td>
                        <td v-text="i.TipoTrecho"></td>
                        <td v-text="i.KmInicial"></td>
                        <td v-text="i.KmFinal"></td>
                        <td v-text="i.NumeroEstaca"></td>
                        <td v-text="i.Observacao"></td>
                        <td>

                            <template v-if="i.Diametro">Diametro: {{i.Diametro}}</template>
                            <template v-else>Altura: {{i.Altura}} | Largura: {{i.Largura}}</template>
                        </td>
                        <td>
                            <button @click="getLicenciamentoAreas(i.CodigoAreaApoio)" class="btn btn-outline-info btn-sm"><i class="fa fa-list"></i> Licenciamento
                                Ambiental</button>
                            <button v-if="vmDadosContrato.habilitarEdicao" @click="deletar(i.CodigoAreaApoio, i.index)" class="btn btn-outline-danger btn-sm"><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>

            <!-- ============================================
     modal licenciamento ambiental 
================================================= -->

            <div class="modal-w98-h50 sombra-2 p-4" v-if="mostrarModalLicenciamento">
                <button @click="mostrarModalLicenciamento = false" class="btn btn-secondary btn-r"><i class="fa fa-times"></i></button>
                <h4>Licenciamento de OAE e Áreas de Apoio</h4>
                <div class="row m-2 p-4 border">
                    <div class="input-group mb-3 col-12 col-sm-6 col-md-4">
                        <div class="input-group-prepend">
                            <span id="basic-addon1" class="input-group-text">
                                Contratada
                            </span>
                        </div>
                        <select v-model="codigoContratada" class="form-control show-tick ms select2">
                            <option value="">Selecione uma contratada</option>
                            <option v-for="i in listaContratadas" :value="i.CodigoContrutorasContratos" v-text="i.NomeEmpresa"></option>
                        </select>
                    </div>
                    <div class="input-group mb-3 col-12 col-sm-6 col-md-8">
                        <div class="input-group-prepend">
                            <span id="basic-addon1" class="input-group-text">
                                Licença Ambiental
                            </span>
                        </div>
                        <input class="form-control" placeholder="digite o número ou ano da licença" v-model="numeroDigitadoLicenca" />
                        <button v-if="vmDadosContrato.habilitarEdicao" @click="getLicencas" class="btn btn-outline-success mr-3 m-0">Buscar</button>
                        <!-- ======================================== 
                         Modal licenças ambientais
                        ======================================== -->
                        <div class="modal-abs border p-4" v-show="mostrarLicencas">
                            <div class="list-group">
                                <h6>Selecione uma Licença</h6>
                                <button @click="salvarLicenciamentoAmbiental(i.CodigoLicenca)" type="button" class="list-group-item list-group-item-action" v-for="i in listaLicencasBusca">{{i.NumeroLicenca}} - {{i.Sigla}} - {{i.NomeOrgaoExpeditor}}</button>

                            </div>
                        </div>
                    </div>
                    <!-- ======================================== 
                         Modal licenças ambientais
                        ======================================== -->


                </div>

                <table class="table table-striped mt-4">
                    <thead>
                        <th>Item</th>
                        <th>Executor da Obra</th>
                        <th>Contrato</th>
                        <th>Objeto</th>
                        <th>Licenças/ Autorizações</th>
                        <th>Expeditor</th>
                        <th>Publicação</th>
                        <th>Validade</th>
                        <th>Status</th>
                        <th>situação Atual</th>
                    </thead>
                    <tbody>
                        <tr v-for="i in listaAreasLicenciadas">
                            <td v-text="i.NomeEstrutura"></td>
                            <td v-text="i.NomeEmpresa"></td>
                            <td v-text="i.NumeroContrato"></td>
                            <td v-text="i.NomeTipoEstrutura"></td>
                            <td v-text="i.NumeroLicenca"></td>
                            <td v-text="i.NomeOrgaoExpeditor"></td>
                            <td v-text="vmGlobal.frontEndDateFormat(i.DataEmissao)"></td>
                            <td v-text="vmGlobal.frontEndDateFormat(i.DataVencimento)"></td>
                            <td v-text="i.SituacaoAtual"></td>
                            <td>-</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>