<div class="modal-w98-h50 sombra-2 p-5"  v-show="mostrarCadastro">
    <div class="col-12 text-right">
        <button @click="mostrarCadastro = false" class="btn"><i class="fa fa-times"></i></button>
    </div>
    <div class="row">
        <h3 class="w-100 mb-4">Cadastro de Convênio ou TED</h3>

        <div class="input-group mb-3 col-12">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">UF</span>
            </div>
            <select @change="vmConfigurarContrato.filtrarEmpreendimentos($event)" class="form-control show-tick ms select2">
                <option selected>Selecione um estado</option>
                <option :value="'AC'">Acre</option>
                <option :value="'AL'">Alagoas</option>
                <option :value="'AP'">Amapá</option>
                <option :value="'AM'">Amazonas</option>
                <option :value="'BA'">Bahia</option>
                <option :value="'CE'">Ceará</option>
                <option :value="'DF'">Distrito Federal</option>
                <option :value="'ES'">Espírito Santo</option>
                <option :value="'GO'">Goiás</option>
                <option :value="'MA'">Maranhão</option>
                <option :value="'MT'">Mato Grosso</option>
                <option :value="'MS'">Mato Grosso do Sul</option>
                <option :value="'MG'">Minas Gerais</option>
                <option :value="'PA'">Pará</option>
                <option :value="'PB'">Paraíba</option>
                <option :value="'PR'">Paraná</option>
                <option :value="'PE'">Pernambuco</option>
                <option :value="'PI'">Piauí</option>
                <option :value="'RJ'">Rio de Janeiro</option>
                <option :value="'RN'">Rio Grande do Norte</option>
                <option :value="'RS'">Rio Grande do Sul</option>
                <option :value="'RO'">Rondônia</option>
                <option :value="'RR'">Roraima</option>
                <option :value="'SC'">Santa Catarina</option>
                <option :value="'SP'">São Paulo</option>
                <option :value="'SE'">Sergipe</option>
                <option :value="'TO'">Tocantins</option>
                <option :value="'EX'">Estrangeiro</option>
            </select>
        </div>
        <div class="input-group mb-3 col-12">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Empreendimento</span>
            </div>
            <select v-model="modelContrato.CodigoEmpreendimento" class="form-control show-tick ms select2">
                <option selected>Selecio um Empreendimento</option>
                <option :value="i.CodigoEmpreendimento" v-for="i in listaEmpreendimentos">{{i.Sigla}}</option>
            </select>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Número Contrato</span>
            </div>
            <input type="text" class="form-control" v-model="modelContrato.Numero">
        </div>
        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Tipo Contrato</span>
            </div>
            <select class="form-control show-tick ms select2" v-model="modelContrato.TipoContrato">
                <option selected>Selecione o Tipo</option>
                <option :value="'Convênio'">Convênio</option>
                <option :value="'TED'">TED</option>
            </select>
        </div>
        <div class="input-group mb-3 col-12 col-sm-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Lote</span>
            </div>
            <input type="text" class="form-control" v-model="modelContrato.Lote">
        </div>
        <div class="input-group mb-3 col-12 col-sm-9">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Objeto Contrato</span>
            </div>
            <textarea class="form-control" v-model="modelContrato.ObjetoContrato"></textarea>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Modalidade Contratação</span>
            </div>
            <input type="text" class="form-control" v-model="modelContrato.ModalidadeContratacao">
        </div>
        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Nome Empresa</span>
            </div>
            <input type="text" class="form-control" v-model="modelContrato.NomeEmpresa">
        </div>
        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">CNPJ Empresa</span>
            </div>
            <input type="text" class="form-control" v-model="modelContrato.CnpjEmpresa">
        </div>
        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Tipo Licitacao</span>
            </div>
            <input type="text" class="form-control" v-model="modelContrato.TipoLicitacao">
        </div>

        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Data Assinatura</span>
            </div>
            <input type="date" class="form-control" v-model="modelContrato.DataAssinatura">
        </div>
        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Data Início Contrato</span>
            </div>
            <input type="date" class="form-control" v-model="modelContrato.DataInicioContrato">
        </div>
        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Data Término Prevista</span>
            </div>
            <input type="date" class="form-control" v-model="modelContrato.DataTerminoPrevista">
        </div>
        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Edital</span>
            </div>
            <input type="text" class="form-control" v-model="modelContrato.Edital">
        </div>
        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Data Publicação</span>
            </div>
            <input type="date" class="form-control" v-model="modelContrato.DataPublicacao">
        </div>

        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Municipio</span>
            </div>
            <input type="text" class="form-control" v-model="modelContrato.Municipio">
        </div>

        <div class="input-group mb-3 col-12 col-sm-6">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Valor Inicial Aditivos e Reajustes</span>
            </div>
            <input type="number" class="form-control" v-model="modelContrato.ValorInicialAditReajustes">
        </div>

        <div class="col-12 text-right">
            <button @click="salvarContrato(null, 1, true)" class="btn btn-success">Salvar</button>
        </div>
    </div>
</div>