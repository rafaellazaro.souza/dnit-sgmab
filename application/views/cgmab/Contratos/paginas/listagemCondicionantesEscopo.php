<div class="container-fluid" id="condicionanteServico" v-show="mostrarListaCondicionantes">
    <div class="row p-4">
        <div class="col-8 border p-3">
            <h4 class="w-100  ">{{vmEscopoServicos.titulo}} - Lista de Condicionantes:</h4>
        </div>
        <div class="col-4 text-right">
            <button @click="voltar()" class="btn btn-sm btn-outline-secondary"><i class="fas fa-angle-left"></i> Voltar para Serviços</button>
        </div>

        <div class="col-12 mt-4">
            <table class="table table-striped " id="listaCondicionantesEscopo">
                <thead>
                    <tr>
                        <th></th>
                        <th>Condicionante</th>
                        <th>Tipo</th>
                        <th>Tema</th>
                        <th>SubTipo</th>
                        <th>Licença</th>
                        <th><i class="fa fa-times"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="i in listaCondicionantes">
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Ações
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="javascript:;" v-if="codigoServico === 3" @click="iniciarDescricao(i.CodigoEscopoPrograma)">Descrição dos Programas e Planos Ambientais</a>
                                    <a class="dropdown-item" href="javascript:;" v-if="codigoServico === 3" @click="iniciarCronograma(i.CodigoEscopoPrograma)">Cronograma Físico</a>
                                    <!-- <a class="dropdown-item <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'invisivel' : '' ?>" href="javascript:;" v-if="codigoServico === 3" @click="iniciaExecucao(i)">Relatório de Acompanhamento de Execução do Programa</a> -->
                                    <!-- <a class="dropdown-item <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'invisivel' : '' ?>" :href="'<?= base_url('ConfiguracaoProgramas/recursosHidricos/?99cfb275a87d383fa2912331e3117e48=') ?>'+ vmGlobal.codificarDecodificarParametroUrl(i.CodigoEscopoPrograma, 'encode')" v-if="i.CodigoTema === 3 || i.CodigoSubtipo === '5' "><i class="fas fa-cog"></i> Configuração</a>                                
                                    <a class="dropdown-item <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'invisivel' : '' ?>" :href="'<?= base_url('ConfiguracaoProgramas/recursosFaunaModulos/?99cfb275a87d383fa2912331e3117e48=') ?>'+ vmGlobal.codificarDecodificarParametroUrl(i.CodigoEscopoPrograma, 'encode')" v-if="i.CodigoTema === 1 || i.CodigoSubtipo === '3' "><i class="fas fa-cog"></i> Configuração</a> -->
                                </div>
                            </div>
                        </td>
                        <td v-text="i.Descricao"></td>
                        <td v-text="i.NomeTipo"></td>
                        <td v-text="i.CodigoTipo === 1 || i.CodigoTipo === '1' ? '-' : i.NomeTema"></td>
                        <td v-text="i.CodigoTipo === 1 || i.CodigoTipo === '1' ? '-' : i.NomeSubtipo"></td>
                        <td >

                        
                        <p class="visible" v-text="i.Sigla+ '-'+ i.NumeroLicenca"></p>
                        <p class="visible" v-text="i.NomeTipo +' - '+i.NomeTema+ ' - '+ i.NomeSubtipo"></p>
                        </td>
                        <td><button @click="deletarCondicinante(i.CodigoEscopoPrograma)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button></td>

                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>

<!-- Descrição dos Croronogramas Fisicos -->
<?php $this->load->view('cgmab/Contratos/paginas/DescricaoProgramas') ?>
<!-- Croronogramas Fisicos -->
<?php $this->load->view('cgmab/Contratos/paginas/CronogramasFisicosProgramas') ?>
<!-- situação de execucao de programas -->
<?php $this->load->view('cgmab/ExecucaoProgramas/index'); ?>