<div class="container-fluid" id="infraestrutura">
    <div class="row p-4 mt-4">
        <div class="col-12">
            <h4 class="w-100 mb-4 border p-3">Infraestrutura</h4>
        </div>

        <div class="col-12 pt-3 pb-2">


            <div class="input-group mt-2 mb-2">
                <div class="btn-group" role="group" aria-label="Grupo de botões com dropdown aninhado">
                    <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle m-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-plus"></i> {{NomeServico ? NomeServico : 'Para Filtrar Selecione o Serviço'}}
                        </button>
                        <div class="dropdown-menu" style="left: -112px;" aria-labelledby="btnGroupDrop1">
                            <a @click="codigoServico = i.CodigoServico; codigoEscopoServico= i.CodigoEscopo; NomeServico = i.NomeServico; getInfraestrutura()" class="dropdown-item" href="javascript:;" v-for="i in listaServicos" v-text="i.NomeServico"></a>
                        </div>
                    </div>
                </div>



                <!-- <select @change="getItensFiltro()" v-model="dadosFiltro.tipoItem" v-show="codigoServico != ''" class="form-control show-tick ms select2  ">
                    <option value="">Selecione o tipo de ítem</option>
                    <option value="1">Veículo</option>
                    <option value="2">Equipamento</option>
                    </option>
                </select> -->

                <!-- ============================ 
                modal listagem de itens 
                ============================ -->
                <div class="modal-w98-h50 sombra-2 p-4" v-if="mostrarItensFiltro">
                    <button @click="mostrarItensFiltro = false; resetarListaPessoasCondicionantes()" class="btn btn-secondary btn-r"><i class="fa fa-times"></i></button>
                    <h4>Selecione o Ítem</h4>

                    <table class="table table-striped" id="tableItensInfra">
                        <template v-if="parseInt(dadosFiltro.tipoItem) == 1">
                            <thead>
                                <th class="w-5">Foto</th>
                                <th>Modelo</th>
                                <th>Marca</th>
                                <th>Placa</th>
                                <th>Tipo</th>
                                <th>Ano</th>
                                <th>Cod. SICRO</th>
                                <th>Selecionar</th>
                            </thead>
                            <tbody>
                                <tr v-for="(i, index) in listaVeiculosEquipamentos">
                                    <td><img :src="i.Foto" class="img-thumbnail"></td>
                                    <td v-text="i.NomeModelo"></td>
                                    <td v-text="i.NomeMarca"></td>
                                    <td v-text="i.Placa"></td>
                                    <td>{{i.TipoVeiculo === '1' || i.TipoVeiculo === 1 ? 'Carros/ Utilitários' : '-'}}
                                    </td>
                                    <td v-text="i.Ano"></td>
                                    <td v-text="i.CodigoSICRO"></td>
                                    <td>
                                        <!-- verifica se é execução de programas - para salvar na condicionante -->
                                        <template v-if="parseInt(codigoServico) == 3">
                                            <button v-if="mostrarResetarListaItens" @click="listaVeiculosEquipamentos = [i]; mostrarCondicionantes = true; CodigoItemSelecionado = i.CodigoVeiculo; getCondicionantes(); mostrarResetarListaItens = false" class="btn btn-outline-success btn-sm"><i class="fa fa-check"></i></button>

                                            <button v-else @click="getItensFiltro(1); mostrarResetarListaItens = true; mostrarCondicionantes = false; CodigoItemSelecionado = 0; " class="btn btn-outline-danger btn-sm"><i class="fa fa-times"></i></button>
                                        </template>
                                        <!-- se for gestão ambiental ou supervisão ambiental - deve salvar no serviço -->
                                        <template v-else>
                                            <button @click="mostrarResetarListaItens = false; salvarItemExecucao(i.CodigoVeiculo)" class="btn btn-outline-success btn-sm"><i class="fa fa-check"></i> Salvar</button>
                                        </template>

                                    </td>
                                </tr>
                            </tbody>
                        </template>

                        <template v-else>
                            <thead>
                                <th class="w-5">Foto</th>
                                <th>Equipamento</th>
                                <th>Descrição/Modelo</th>
                                <th>Num. Série</th>
                                <th>Especificação</th>
                                <th>Obs</th>
                                <th>Selecionar</th>
                            </thead>
                            <tbody>
                                <tr v-for="(i, index) in listaVeiculosEquipamentos">
                                    <td><img :src="i.Foto" class="img-thumbnail"></td>
                                    <td v-text="i.NomeEquipamento"></td>
                                    <td v-text="atob(i.DescricaoModelo)"></td>
                                    <td v-text="i.NumeroSerie"></td>
                                    <td v-text="atob(i.EspecificacaoTecnica)"></td>
                                    <td v-text="atob(i.Observacoes)"></td>
                                    <td>




                                        <!-- verifica se é execução de programas - para salvar na condicionante -->
                                        <template v-if="parseInt(codigoServico) == 3">
                                            <button v-if="mostrarResetarListaItens" @click="listaVeiculosEquipamentos = [i]; mostrarCondicionantes = true; CodigoItemSelecionado = i.CodigoEquipamento; getCondicionantes(); mostrarResetarListaItens = false" class="btn btn-outline-success btn-sm"><i class="fa fa-check"></i></button>

                                            <button v-else @click="getItensFiltro(); mostrarResetarListaItens = true; mostrarCondicionantes = false; CodigoItemSelecionado = 0; " class="btn btn-outline-danger btn-sm"><i class="fa fa-times"></i></button>
                                        </template>
                                        <!-- se for gestão ambiental ou supervisão ambiental - deve salvar no serviço -->
                                        <template v-else>
                                            <button @click="mostrarResetarListaItens = false; salvarItemExecucao(i.CodigoEquipamento)" class="btn btn-outline-success btn-sm"><i class="fa fa-check"></i> Salvar</button>
                                        </template>
                                    </td>
                                </tr>
                            </tbody>
                        </template>
                    </table>



                    <div class="row mt-4 p-2" v-if="mostrarCondicionantes">
                        <div class="col-12 col-sm-6 border-right">
                            <h4>Selecione os Programas</h4>
                            <hr class="divider">
                            <table class="table table-striped" v-if="listaCondicionantes.length > 0">
                                <tr v-for="(i, index) in listaCondicionantes">
                                    <td v-text="i.CodigoCondicionante +' - '+ i.Titulo"></td>
                                    <td v-text="i.NomeTipo"></td>
                                    <td v-text="i.CodigoTipo === 1 || i.CodigoTipo === '1' ? '-' : i.NomeTema"></td>
                                    <td v-text="i.CodigoTipo === 1 || i.CodigoTipo === '1' ? '-' : i.NomeSubtipo"></td>
                                    <td><button @click="associarItemCondicionante(i.CodigoCondicionante, i.CodigoEscopoPrograma, index, i)" class="btn btn-default btn-sm"><i class="fa fa-check"></i></button></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-12 col-sm-6">
                            <h4>Programas Selecionados</h4>
                            <hr class="divider">
                            <table class="table table-striped">
                                <tr v-for="(i, index) in listaCondicionantesSelecionadas">
                                    <td v-text="i.CodigoCondicionante +' - '+ i.Titulo"></td>
                                    <td v-text="i.NomeTipo"></td>
                                    <td v-text="i.CodigoTipo === 1 || i.CodigoTipo === '1' ? '-' : i.NomeTema"></td>
                                    <td v-text="i.CodigoTipo === 1 || i.CodigoTipo === '1' ? '-' : i.NomeSubtipo"></td>
                                    <td><button @click="removerAssociacaoCondicionates(index, i)" class="btn btn-outline-danger btn-sm"><i class="fa fa-minus"></i></button></td>
                                </tr>
                            </table>
                        </div>




                        <div class="mt-3" v-if="listaCondicionantesSelecionadas.length > 0">

                            <button @click="salvarItemExecucao();mostraResultadoPesquisa = false;" v-if="mostrarCondicionantes" class="btn btn-default r0 mt-2 mr-5 btn-r">Salvar</button>

                        </div>

                    </div>
                </div>

                <!-- ============================ 
                modal listagem de itens 
                ============================ -->
            </div>

        </div>



        <div class="row">
            <div class="col-12">
                <h4>Veículos</h4>
                <table class="table table-striped " id="listaDeProfissionaisNoEscopo">
                    <thead>
                        <th class="w-5">Foto</th>
                        <th>Modelo</th>
                        <th>Marca</th>
                        <th>Placa</th>
                        <th>Tipo</th>
                        <th>Ano</th>
                        <th>Cod. SICRO</th>
                        <th>Programa</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr v-for="(i, index) in listaVeiculos">
                            <td><img :src="i.Foto" class="img-thumbnail"></td>
                            <td v-text="i.NomeModelo"></td>
                            <td v-text="i.NomeMarca"></td>
                            <td v-text="i.Placa"></td>
                            <td>{{i.TipoVeiculo === '1' || i.TipoVeiculo === 1 ? 'Carros/ Utilitários' : '-'}}

                            <td v-text="i.Ano"></td>
                            <td v-text="i.CodigoSICRO"></td>
                            <td>
                                <template v-if="i.Titulo">{{i.Titulo+' - '+i.NomeTema+'/ '+i.NomeSubtipo}}</template>
                                <template v-else>-</template>
                            </td>
                            <td><i v-if="vmDadosContrato.habilitarEdicao" @click="deletarRecurso(i.CodigoRecurso)" class="fa fa-times"></i></td>

                        </tr>
                    </tbody>
                </table>
                <div class="p-1 ml-2 border-top text-right mt-4 w-100 " v-show="codigoServico != ''">
                    <button v-if="vmDadosContrato.habilitarEdicao" @click="getItensFiltro(1)" class="btn btn-outline-secondary">Adicionar Veículos</button>
                </div>
            </div>
            <div class="col-12 mt-5 border-top">
                <h4>Equipamentos</h4>
                <table class="table table-striped " id="listaDeProfissionaisNoEscopo">
                    <thead>
                        <th class="w-5">Foto</th>
                        <th>Equipamento</th>
                        <th>Descrição/Modelo</th>
                        <th>Num. Série</th>
                        <th>Especificação</th>
                        <th>Obs</th>
                        <th>Programa</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr v-for="(i, index) in listaEquipamentos">
                            <td><img :src="i.Foto" class="img-thumbnail"></td>
                            <td v-text="i.NomeEquipamento"></td>
                            <td v-text="atob(i.DescricaoModelo)"></td>
                            <td v-text="i.NumeroSerie"></td>
                            <td v-text="atob(i.EspecificacaoTecnica)"></td>
                            <td v-text="atob(i.Observacoes)"></td>
                            <td>
                                <template v-if="parseInt(i.CodigoTipo) == 2">{{i.NomeTema+'/ '+i.NomeSubtipo}}</template>
                                <template v-else>{{i.Titulo}}</template>
                            </td>
                            <td><i v-if="vmDadosContrato.habilitarEdicao" @click="deletarRecurso(i.CodigoRecurso)" class="fa fa-times"></i></td>
                        </tr>
                    </tbody>
                </table>
                <div class="p-1 ml-2 border-top text-right mt-4 w-100 " v-show="codigoServico != ''">
                    <button v-if="vmDadosContrato.habilitarEdicao" @click="getItensFiltro(2)" class="btn btn-outline-secondary">Adicionar Equipamentos</button>
                </div>
            </div>
        </div>





    </div>

</div>