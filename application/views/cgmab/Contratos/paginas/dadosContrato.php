<div class="container-fluid" id="dadosContrato">
    <div class="row p-4">

        <h4 class="w-100 mb-4 ml-2 border p-3">Informações Básicas do Contrato</h4>

        <!-- <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-3" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Número</span>
            </div>
            <div class="form-control">{{i.Numero}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 " v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Empresa</span>
            </div>
            <div class="form-control fs-13">{{i.NomeEmpresa}}</div>
        </div> -->
        <div class="input-group mb-3 col-12" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">CNPJ</span>
            </div>
            <div class="form-control">{{i.CnpjEmpresa}}</div>
        </div>
        <div class="input-group mb-3 col-12" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Objeto do Contrato</span>
            </div>
            <div class="form-control h-150 fs-13">{{i.ObjetoContrato}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-3" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Data Assinatura</span>
            </div>
            <div class="form-control">{{vmGlobal.frontEndDateFormat(i.DataAssinatura)}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-3" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Data Início Contrato</span>
            </div>
            <div class="form-control">{{vmGlobal.frontEndDateFormat(i.DataInicioContrato)}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-3" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Data Publicação</span>
            </div>
            <div class="form-control">{{vmGlobal.frontEndDateFormat(i.DataPublicacao)}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-3" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Data de Término Prevista</span>
            </div>
            <div class="form-control">{{vmGlobal.frontEndDateFormat(i.DataTerminoPrevista)}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-4" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Tipo Contrato</span>
            </div>
            <div class="form-control">{{i.TipoContrato}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-4" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Tipo Licitação</span>
            </div>
            <div class="form-control">{{i.TipoLicitacao}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-4" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Edital</span>
            </div>
            <div class="form-control">{{i.Edital}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-3" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Lote</span>
            </div>
            <div class="form-control">{{i.Lote}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-3" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Modal</span>
            </div>
            <div class="form-control">{{i.Modal}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-3" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Municipio</span>
            </div>
            <div class="form-control">{{i.Municipio}}</div>
        </div>
        <div class="input-group mb-3 col-12 col-sm-6 col-md-4 col-xl-3" v-for="i in listaContrato">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Valor Inicial</span>
            </div>
            <div class="form-control">{{formatReal(i.ValorInicialAditReajustes)}}</div>
            <!-- <div class="form-control" >{{i.ValorInicialAditReajustes}}</div> -->
        </div>
    </div>

    <div class="row mt-4  p-4 border-top">

        <div class="col-12 col-sm-12 col-md-8 border-right p-3">
            <h4 class="w-100 mb-4 ml-2">Aditivos</h4>
            <div class="h-450r w-100">
                <table class="table table-striped">
                    <thead>
                        <th>NumeroAdtivo</th>
                        <th class="w-25">Objeto</th>
                        <!-- <th>Data Reinício</th> -->
                        <th>Valor</th>
                        <th>Número SEI</th>
                    </thead>
                    <tbody>
                        <tr v-for="i in listaAditivos">
                            <td>{{i.NumeroAdtivo}}</td>
                            <td>{{i.Objeto}}</td>
                            <!-- <td>{{i.DataReinicio}}</td> -->
                            <td>{{formatReal(i.ValorAditivo)}}</td>
                            <td>{{i.NumeroSeiAditivo}}</td>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4 p-3">
            <h4 class="w-100 mb-4 ml-2">Apostilamentos (Reajustes)</h4>
            <div class="h-450r w-100">
                <table class="table table-striped">
                    <thead>
                        <th>Número Reajuste</th>
                        <th>Numero SEI</th>
                        <th>Valor (R$)</th>
                    </thead>
                    <tbody>
                        <tr v-for="i in listaApostilamentos">
                            <td>{{i.NumeroReajuste}}</td>
                            <td>{{i.NumeroSeiApostilamento}}</td>
                            <td>{{formatReal(i.ValorReajuste)}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>



    </div>
</div>