<script>
    var vmListagem = new Vue({
        el: "#Listagem",
        data: {
            listaDados: [],
            cnpj: '',
            mostrarCondicionantes: false,
            mostrarDadosPrograma: false,
            selecionarContratada: false,
            mostrarAcao: false,
            mostrarMenuSupervisao: false,
            dadosEscopoPrograma: 0,
            listaServicosContrato: [],
            listaCondicionantes: [],
            listaCadastroContratadas: [],
            dadosDescricao: {},
            contratoSelecionado: 0,
            servicoSelecionado: 0,
            codigoServicoSelecionado: 0,
            cnpjContratada: <?= isset($_SESSION['Logado']['CnpjContratada']) ? $_SESSION['Logado']['CnpjContratada'] : 0; ?>,
            codigoContratada: 0
        },
        watch: {
            contratoSelecionado: function(val) {
                this.contratoSelecionado = val;
                this.getServicosContrato(this.contratoSelecionado.CodigoContrato);
            },
            servicoSelecionado: function(val) {
                if (this.codigoServicoSelecionado == 3) {
                    this.getCondicionantesServico(val);
                    this.mostrarMenuSupervisao = false;
                }
                if (this.codigoServicoSelecionado == 2) {
                    this.mostrarMenuSupervisao = true;
                    this.mostrarCondicionantes = false;
                }
            },
            cnpjContratada: function(val) {
                this.getContratos();
                this.selecionarContratada = false;
                this.gerarCodigoContratada();
                var servico = this.listaServicosContrato.filter((el) => {
                    return el.NomeServico.trim().includes('Supervisão Ambiental') && el.CodigoEscopo == val;
                });

                if (servico.length !== 0) {
                    window.location.href = base_url + 'DiarioAmbiental/diarioAmbientalLista/' + vmGlobal.codificarDecodificarParametroUrl(val, 'encode');
                    return;
                }

                this.getCondicionantesServico(val);
            },
            dadosEscopoPrograma: async function(val) {
                await setTimeout(() => {
                    CKEDITOR.replace('dadosPrograma1');
                    CKEDITOR.replace('dadosPrograma2');
                    CKEDITOR.replace('dadosPrograma3');
                    CKEDITOR.replace('dadosPrograma4');
                    CKEDITOR.replace('dadosPrograma5');
                    CKEDITOR.replace('dadosPrograma6');
                }, 3000);

                await this.getDadosDescricao();
                await setTimeout(() => {
                    CKEDITOR.instances['DescricaoPrograma'].setData(atob(this.dadosDescricao.Descricao));
                    CKEDITOR.instances['Justificativa'].setData(atob(this.dadosDescricao.Justificativa));
                    CKEDITOR.instances['Objetivo'].setData(atob(this.dadosDescricao.Objetivo));
                    CKEDITOR.instances['Metodologia'].setData(atob(this.dadosDescricao.Metodologia));
                    CKEDITOR.instances['PublicoAlvo'].setData(atob(this.dadosDescricao.PublicoAlvo));
                    CKEDITOR.instances['AtividadesPrevistas'].setData(atob(this.dadosDescricao.AtividadesPrevistas));
                }, 5000);

            }
        },
        async mounted() { //
            await this.getContratos();

        },
        methods: {
            async getContratos() {
                // $(".page-loader-wrapper").css('display', 'block')
                $('#tblListagem').DataTable().destroy();


                if (this.cnpjContratada == 0) { // verifica se foi passado cnpj da empresa
                    this.selecionarContratada = true;
                    await this.getContratadas();
                }


                var params = {
                    table: 'contratos',
                    where: {
                        CnpjEmpresa: this.cnpjContratada, //this.cnpj   --buscar da sessao cpnj do cadastro de pessoas,
                    },
                    whereIn: {
                        Status: [1, 2, 4, 5],
                    }
                }


                this.listaDados = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#tblListagem', true);
            },
            async getContratadas() {
                let params = {
                    table: 'contratada'
                }
                this.listaCadastroContratadas = await vmGlobal.getFromAPI(params);
            },
            async gerarCodigoContratada() {
                await axios.post(base_url + 'Login/gerarCodigoContratada', $.param({
                    codigo: this.codigoContratada
                }))
            },
            async getServicosContrato(codigoContrato) {
                await axios.get(base_url + 'EscopoServicos/getEscopoServicoContrato?CodigoContrato=' + codigoContrato)
                    .then((resp) => {
                        this.listaServicosContrato = resp.data
                    })
                    .catch((error) => {
                        console.log(error)
                    });
            },
            async getCondicionantesServico(codigoEscopo) {

                await axios.post(base_url + 'EscopoServicos/getCondiconantesEscopo', $.param({
                        CodigoEscopo: codigoEscopo
                    }))
                    .then((response) => {
                        this.listaCondicionantes = response.data;
                    })
                    .catch((error) => {
                        console.log(error);
                    })
                    .finally(() => {
                        this.mostrarCondicionantes = true;
                    })
            },
            iniciarRelatorioAgua() {
                let url = base_url + '/Relatorios/monitoramentoAgua';
                window.location.href = url + '?99cfb275a87d383fa2912331e3117e48=' + vmGlobal.codificarDecodificarParametroUrl(this.dadosEscopoPrograma.CodigoEscopoPrograma, 'encode');
            },
            iniciarRelatorioActividades(codigoEscopo, numContrato) {
                let url = base_url + 'atividades/listaAtividadesMes';
                window.location.href = url + '?codigoEscopo=' + vmGlobal.codificarDecodificarParametroUrl(codigoEscopo, 'encode') + '&numeroContrato=' + vmGlobal.codificarDecodificarParametroUrl(numContrato, 'encode');
            },
            iniciarRelatoriosMensais(programa, contrato, numeroContrato) {
                this.gerarSessaoContrato();
                var url = base_url + 'Relatorios/listarRelatoriosMensais?Rt546wqQwsSC1CvdYWqsSfRFrSC48DSwF=' + vmGlobal.codificarDecodificarParametroUrl(programa, 'encode') + '&Rt546wqQwsSC1CvdYWqsSfRFrSC48DSwF=' + vmGlobal.codificarDecodificarParametroUrl(contrato, 'encode') + '&Rt546wqQwsSC1CvdYWqsSfRFrSC48DSwF=' + vmGlobal.codificarDecodificarParametroUrl(numeroContrato, 'encode');
                window.open(url, '_blank');
            },
            async gerarSessaoContrato(codigoContrato) {
                await axios.post(base_url + 'Contratos/gerarSessaoContrato', $.param({
                    contrato: this.contratoSelecionado.CodigoContrato
                }))

            },
            async getDadosDescricao() {
                var params = {
                    table: 'descricaoPrograma',
                    join: {
                        table: 'recursosHumanos',
                        on: 'CodigoRecursosHumanos'
                    },
                    where: {
                        CodigoEscopoPrograma: this.dadosEscopoPrograma.CodigoEscopoPrograma
                    }
                }
                var dadosBusca = await vmGlobal.getFromAPI(params, 'cgmab');
                if (dadosBusca.length >= 1) {
                    this.dadosDescricao = dadosBusca[0];
                }
            },
            contratoAtrazado(val) {
                if (val) {
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var prazoCorrecao = moment(val).toDate();
                    prazoCorrecao.setHours(0, 0, 0, 0);

                    return prazoCorrecao.getTime() < today.getTime();
                }

                return false;
            },
            formataData(val) {
                if (val) {
                    return moment(val).format('DD/MM/YYYY');
                }
                return '';
            }
        },
        computed: {
            getStatus() {
                if (this.dadosEscopoPrograma == 0) {
                    return '';
                }
                var status = parseInt(this.dadosEscopoPrograma.StatusPrograma);

                if (status == 1) {
                    return 'Pendente de Envio';
                } else if (status == 2) {
                    return 'Pendente de Validação';
                } else if (status == 4) {
                    return 'Aprovado';
                } else if (status == 5) {
                    return 'Parcialmente Aprovado';
                }
                return '';
            },
            getClassStatus() {
                if (this.dadosEscopoPrograma == 0) {
                    return '';
                }

                var status = parseInt(this.dadosEscopoPrograma.StatusPrograma);

                if (status == 1) {
                    return 'badge-warning';
                } else if (status == 2) {
                    return 'badge-primary';
                } else if (status == 4) {
                    return 'badge-success';
                } else if (status == 5) {
                    return 'badge-correcao';
                }
                return '';
            },
            viewRelatorio() {
                if (this.dadosEscopoPrograma == 0) {
                    return false;
                }

                if (![2, 3, 4, 5].includes(parseInt(this.dadosEscopoPrograma.CodigoSubtipo))) {
                    return true;
                } else if ([2, 3, 4, 5].includes(parseInt(this.dadosEscopoPrograma.CodigoSubtipo)) && this.dadosEscopoPrograma.StatusPrograma == 4) {
                    return true;
                }

                return false;
            }
        }
    });
</script>