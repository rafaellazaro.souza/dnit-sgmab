<script>
    var vmInfraestrutura = new Vue({
        el: '#infraestrutura',
        data: {
            listaInfraestruturaServico: 0,
            listaInfraestrutura: [],
            listaServicos: [],
            listaCondicionantes: 0,
            listaVeiculosEquipamentos: 0,
            listaCondicionantesSelecionadas: [],
            listaVeiculos: 0,
            listaEquipamentos: 0,
            codigoEscopoServico: '',
            codigoServico: '',
            CodigoItemSelecionado: 0,
            cpfDigitado: '',
            Profissional: 0,
            NomeServico: '',
            mostraResultadoPesquisa: false,
            mostrarItensFiltro: false,
            mostrarCondicionantes: false,
            mostrarResetarListaItens: true,
            dadosEscopoRecursos: [],
            dadosFiltro: {},
        },
        watch: {
            codigoServico: function(val) {
                this.getInfraestruturaServico();
            }
        },

        methods: {
            async getInfraestrutura() {
                await axios.post(base_url + 'EscopoServicos/getRecursosServicos', $.param({
                        codigoEscopo: this.codigoEscopoServico,
                        consultar: 'veiculos'
                    }))
                    .then((resp) => {
                        this.listaVeiculos = resp.data;
                    }).finally(() => {
                        this.getInfraestuturaEquipamentos();
                    });
            },
            async getInfraestuturaEquipamentos() {
                await axios.post(base_url + 'EscopoServicos/getRecursosServicos', $.param({
                        codigoEscopo: this.codigoEscopoServico,
                        consultar: 'equipamentos'
                    }))
                    .then((resp) => {
                        this.listaEquipamentos = resp.data;
                    })

            },
            selecionarCondicionantes() {

            },
            async salvarInfraestruturaEscopo(codigoRecurso) {
                var params = {
                    table: 'escopoDeRecursos',
                    data: {
                        CodigoEscopo: this.codigoEscopo,
                        CodigoInfraestrutura: codigoRecurso
                    }
                }

                await vmGlobal.insertFromAPI(params, null, 'cgmab');
                this.mostraResultadoPesquisa = false;
                this.resetarListaPessoasCondicionantes();
                this.getInfraestruturaServico();

            },
            async getInfraestruturaServico() {
                var params = {
                    CodigoContrato: vmEscopoServicos.CodigoContrato
                }
                var params2 = {
                    CodigoServico: this.codigoServico,
                    CodigoContrato: vmEscopoServicos.CodigoContrato
                };
                var controller = 'EscopoServicos/getRecurosHumanosServico';

                if (this.codigoServico) {
                    this.listaInfraestruturaServico = await vmGlobal.getFromController(controller, params2);
                } else {
                    this.listaInfraestruturaServico = await vmGlobal.getFromController(controller, params);
                    //this.listaInfraestruturaServico = await vmGlobal.getFromAPI(params, 'cgmab');
                }


                vmGlobal.montaDatatable('listaDeProfissionaisNoEscopo', true);
            },
            async getItensFiltro(tipoItem) {
                this.dadosFiltro.tipoItem = tipoItem;
                if (tipoItem === '1' || tipoItem === 1) {
                    var params = {
                        table: 'veiculos',
                        joinLeft: [{
                                table1: 'veiculos',
                                table2: 'modelos',
                                on: 'CodigoModelo'
                            },
                            {
                                table1: 'modelos',
                                table2: 'marcas',
                                on: 'CodigoMarca'
                            }
                        ],
                        where: {
                            CodigoContratada: <?= $_SESSION['Logado']['CodigoContratada']; ?>,
                            CodigoContrato: vmDadosContrato.listaContrato[0].CodigoContrato
                        }
                    }
                } else {
                    var params = {
                        table: 'equipamentos',
                        where: {
                            CodigoContratada: <?= $_SESSION['Logado']['CodigoContratada']; ?>,
                            CodigoContrato: vmDadosContrato.listaContrato[0].CodigoContrato
                        }
                    }
                }


                this.listaVeiculosEquipamentos = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#tableItensInfra', false)
                this.mostrarItensFiltro = true;

            },
            async getCondicionantes() {

                this.listaCondicionantes = vmCondicionantes.getCondicionantes(this.codigoEscopoServico);
                this.mostrarCondicionantes = true;
            },
            associarItemCondicionante(condicionante, escopoPrograma, index, itens) {
                this.listaCondicionantes.splice(index, 1);
                //cria lista de associaçção de profissional x condicionate
                var novosDados = this.dadosEscopoRecursos;
                if (this.dadosFiltro.tipoItem === '1' || this.dadosFiltro.tipoItem === 1) {
                    var params = {
                        CodigoEscopoPrograma: escopoPrograma,
                        CodigoVeiculo: this.CodigoItemSelecionado,
                        CodigoCondicionante: condicionante
                    }
                } else {
                    var params = {
                        CodigoEscopoPrograma: escopoPrograma,
                        CodigoVeiculo: this.CodigoItemSelecionado,
                        CodigoCondicionante: condicionante
                    }
                }


                novosDados.push(params)
                this.dadosEscopoRecursos = novosDados;

                //cria lista com condicionantes selecionadas
                novosDados = this.listaCondicionantesSelecionadas;
                novosDados.push(itens);
                this.listaCondicionantesSelecionadas = novosDados;
            },
            removerAssociacaoCondicionates(index, item) {
                this.listaCondicionantes.push(item);
                this.dadosEscopoRecursos.splice(index, 1);
                this.listaCondicionantesSelecionadas.splice(index, 1);
            },
            async salvarItemExecucao(CodigoItem = null) {


                this.listaCondicionantesSelecionadas.forEach(async function(i, id) {
                    if (parseInt(vmInfraestrutura.dadosFiltro.tipoItem) == 1) {
                        i.CodigoVeiculo = vmInfraestrutura.CodigoItemSelecionado;

                    } else {
                        i.CodigoEquipamento = vmInfraestrutura.CodigoItemSelecionado;

                    }
                });


                if (parseInt(this.codigoServico) == 3) { //verifica se o serviço é execução de programas

                
                    var formData = new FormData();
                    formData.append('Dados', JSON.stringify(this.listaCondicionantesSelecionadas));

                    await axios.post(base_url + 'EscopoServicos/salvarPessoaExecucao', formData)
                        .then((resp) => {
                            if (resp.data.status === false) {
                                Swal.fire({
                                    position: 'center',
                                    type: 'error',
                                    title: 'Erro!',
                                    text: "Erro ao tentar inserir os dados. " + e,
                                    showConfirmButton: true,

                                });
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    type: 'success',
                                    title: 'Salvo!',
                                    text: 'Escopo de serviço Adicionado Com Sucesso.',
                                    showConfirmButton: true,
                                });
                            }
                        })
                        .finally(() => {
                            this.mostraResultadoPesquisa = false;
                            this.resetarListaPessoasCondicionantes();
                            this.getInfraestruturaServico();
                            this.getInfraestrutura();
                        });
                } else {
                    var params = {};
                    if (parseInt(vmInfraestrutura.dadosFiltro.tipoItem) == 1) {
                        params = {
                            table: 'escopoDeRecursos',
                            data: {
                                CodigoEscopo: this.codigoEscopoServico,
                                CodigoVeiculo: CodigoItem
                            }
                        }
                        await vmGlobal.insertFromAPI(params);
                    } else {
                        params = {
                            table: 'escopoDeRecursos',
                            data: {
                                CodigoEscopo: this.codigoEscopoServico,
                                CodigoEquipamento: CodigoItem
                            }
                        }
                        await vmGlobal.insertFromAPI(params);

                    }
                    vmInfraestrutura.mostraResultadoPesquisa = false;
                    vmInfraestrutura.resetarListaPessoasCondicionantes();
                    vmInfraestrutura.getInfraestruturaServico();
                    vmInfraestrutura.getInfraestrutura();
                }

            },
            resetarListaPessoasCondicionantes() {
                this.mostraResultadoPesquisa = false;
                this.mostrarItensFiltro = false;
                this.mostrarResetarListaItens = true;
                this.mostrarCondicionantes = false;
                this.listaCondicionantesSelecionadas = [];
                this.listaInfraestrutura = [];
                this.dadosEscopoRecursos = [];
                this.listaVeiculosEquipamentos = {};
            },
            novoRecurso() {

                window.location.href = base_url + 'Infraestrutura/listar';
            },
            async deletarRecurso(codigo) {
                await vmGlobal.deleteFromAPI({
                    table: 'escopoDeRecursos',
                    where: {
                        CodigoRecurso: codigo
                    }
                })
                this.getInfraestrutura();
                this.getInfraestuturaEquipamentos()
            }
        }
    });
</script>