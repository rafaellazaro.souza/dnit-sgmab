<script>
    var vmContratosLista = new Vue({
        el: '#ListagemCotratos',
        data: {
            listaContratos: {},
            modelContrato: {
                Status: 3,
            },
            listaEmpreendimentos: [],
            mostrarCadastro: false,
            mostrarEdicaoEmpreendimento: false,
            CodigoEmpreendimentoBkp: ''
        },
        async mounted() {
            await this.getContratos();
        },
        methods: {
            async getContratos() {
                $('#tblListaContratos').DataTable().destroy();
                var params = {
                    table: 'contratos',
                    join: {
                        table: 'empreendimentos',
                        on: 'CodigoEmpreendimento'
                    }
                }
                this.listaContratos = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#tblListaContratos', false);
            },
            async salvarContrato(i, status, tipo, dados) {
                await vmInserirContratos.salvarContrato(i, status, tipo, this.modelContrato)
                await this.getContratos();
                this.mostrarCadastro = false;
            },
            async deletarContrato(CodigoContrato) {
                await vmGlobal.deleteFromAPI({
                    table: 'contratos',
                    where: {
                        CodigoContrato: CodigoContrato
                    }
                });
                this.getContratos();
            }

        }
    });

    var vmInserirContratos = new Vue({
        el: '#addContratos',
        data: {
            numeroDigitado: '',
            dadosContrato: {},
            resultadoBusca: [],
            exibir: '',
            listaAditivos: {},
            listaReajustes: {},
            listaFiscais: {}
        },
        watch: {
            dadosContrato: function(val) {
                vmContratosLista.getContratos();
            }
        },
        methods: {
            setExibir(tela) {
                this.exibir = tela;
            },

            async getContratos(tipo = null) {
                //buscar contratos pelo numero
                await axios.get(base_url + 'Contratos/getContratos?numero=' + this.numeroDigitado + '&TipoConsulta=' + tipo)
                    .then((resp) => {
                        // handle success
                        this.resultadoBusca = resp.data;
                    })
                    .catch((e) => {
                        // handle error
                        console.log(e);
                    })
                    .finally(() => {
                        // always executed
                    });

            },
            async salvarContrato(i = null, status = null, tipo = null, dados = null) {
                if (tipo) {
                    var params = {
                        table: 'contratos',
                        data: dados
                    }
                    await vmGlobal.insertFromAPI(params);
                    var dadosContratada = {
                        NomeContratadas: dados.NomeEmpresa,
                        CnpjContratada: dados.CnpjEmpresa,
                        Status: 1
                    }
                    vmInserirContratos.SalvarContratada(dadosContratada, 1);
                } else {
                    var params = {
                        SkContrato: i.SK_CONTRATO,
                        Numero: i.NU_CON_FORMATADO,
                        NomeEmpresa: i.NO_EMPRESA,
                        CnpjEmpresa: i.NU_CNPJ_CPF,
                        Modal: i.DS_MODA,
                        municipio: i.NO_MUNICIPIO,
                        Edital: i.NU_EDITAL,
                        Lote: i.NU_LOTE_LICITACAO,
                        TipoContrato: 'Contrato',
                        DataAssinatura: i.DT_ASSINATURA,
                        DataTerminoPrevista: i.DT_TER_PRV,
                        NumeroSei: i.NU_PROCESSO,
                        TipoLicitacao: i.DT_PUBLICACAO,
                        DataPublicacao: i.DT_PUBLICACAO,
                        DataInicioContrato: i.DT_INICIO,
                        ObjetoContrato: i.DS_OBJETO,
                        ValorInicialAditReajustes: i.Valor_Inicial_Adit_Reajustes,
                        Status: status,
                    }
                    await axios.post(base_url + 'Contratos/salvarContrato', $.param(params))
                        .then(async (response) => {

                            if (response.data.status == false)
                                throw new Error(response.data.result);

                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Salvo!',
                                text: 'Dados inseridos com sucesso.',
                                showConfirmButton: true,
                            });

                            //buscar e inserir aditivos do contrato
                            this.getAditivos(params.SkContrato, response.data.result);
                            //buscar e inserir reajustes do contrato
                            this.getReajustes(params.SkContrato, response.data.result);

                            if (tipo) return;

                            await this.salvarFiscais(params.SkContrato, response.data.result);

                        })
                        .catch((e) => {
                            console.log(e)
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: 'Erro!',
                                html: 'Erro ao tentar inserir os dados. <br/> Causa: <br/>- Duplicação de dados<br/>-Bloqueio do sistema',

                                showConfirmButton: true,
                            });
                        })
                        .finally(() => {
                            vmContratosLista.getContratos();
                            this.SalvarContratada(i);
                        });
                }



            },
            async SalvarContratada(dados, tipo = null) {
                var verificar = false;
                var params = {}
                if (parseInt(tipo) == 1) {
                    verificar = await this.verificaCadastroEmpresa(dados.CnpjContratada);
                } else {
                    verificar = await this.verificaCadastroEmpresa(dados.NU_CNPJ_CPF);
                }

                //verifica se empresa ja foi cadastrada
                if (verificar === false) {
                    if (parseInt(tipo) == 1) {
                        params = {
                            table: 'contratada',
                            data: {
                                NomeContratada: dados.NomeContratadas,
                                CnpjContratada: dados.CnpjContratada,
                                Status: dados.Status
                            }
                        }

                    } else {
                        params = {
                            table: 'contratada',
                            data: {
                                SK_Contratada: dados.SK_EMPRESA,
                                NomeContratada: dados.NO_EMPRESA,
                                CnpjContratada: dados.NU_CNPJ_CPF
                            }
                        }

                    }
                    debugger
                    await vmGlobal.insertFromAPI(params, null, 'cgmab', true);
                }
            },
            async verificaCadastroEmpresa(cnpj) {
                let params = {
                    table: 'contratada',
                    where: {
                        CnpjContratada: cnpj
                    }
                }
                var existeContratada = await vmGlobal.getFromAPI(params, 'cgmab');
                if (existeContratada.length === 0) {
                    return false;
                } else {
                    return true;
                }
            },
            async getAditivos(codigo, CodigoContrato) {
                //buscar aditivos no sindnit pelo contrato
                var dados = [];
                await axios.get(base_url + 'Contratos/getContratosAditivos?skContrato=' + codigo + '')
                    .then((resp) => {
                        this.listaAditivos = resp.data;

                        this.listaAditivos.forEach(function(i) {
                            var obj = {
                                CodigoContrato: CodigoContrato,
                                NumeroAdtivo: i.NU_ADITIVO,
                                Objeto: i.DS_OBJETO,
                                DataReinicio: i.DT_REINICIO,
                                ValorAditivo: i.VLR_ADITIVO
                            }
                            dados.push(obj);
                        });

                        var formData = new FormData();
                        formData.append('Dados', JSON.stringify(dados));

                        axios.post(base_url + 'Contratos/getContratosAditivosSalvar', formData)
                            .then((resp) => {
                                console.log(resp.data)
                            })

                            .catch((e) => {
                                // handle error
                                console.log(e);
                            })
                            .finally(() => {
                                // always executed
                            });
                    })
            },
            async getReajustes(codigo, CodigoContrato) {
                //buscar aditivos no sindnit pelo contrato
                var dados = [];
                await axios.get(base_url + 'Contratos/getContratosReajustes?skContrato=' + codigo + '')
                    .then((resp) => {
                        this.listaReajustes = resp.data;

                        this.listaReajustes.forEach(function(i) {
                            var obj = {
                                CodigoContrato: CodigoContrato,
                                NumeroReajuste: parseInt(i.Numero_do_Reajuste),
                                ValorReajuste: i.Valor_do_Reajuste
                            }
                            dados.push(obj);
                        });

                        var formData = new FormData();
                        formData.append('Dados', JSON.stringify(dados));

                        axios.post(base_url + 'Contratos/getContratosReajustesSalvar', formData)
                            .then((resp) => {
                                console.log(resp.data)
                            })

                            .catch((e) => {
                                // handle error
                                console.log(e);
                            })
                            .finally(() => {
                                // always executed
                            });
                    })
            },
            async salvarFiscais(skContrato, codigoContrato) {
                await axios.post(base_url + 'Contratos/salvarFiscais', $.param({
                        CodigoContrato: skContrato
                    }))
                    .then(async (response) => {
                        if (response.data.result == false) {
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: 'Erro!',
                                text: "Não foi possível associar os fiscais do contrato.",
                                showConfirmButton: true,
                            });
                        }

                        await this.getFiscais(codigoContrato);
                    })
                    .catch((e) => {
                        // handle error
                        console.log(e);
                    })
            },
            async getFiscais(codigoContrato) {
                await axios.post(base_url + 'Contratos/getFiscaisContrato', $.param({
                        CodigoContrato: codigoContrato
                    }))
                    .then(response => {
                        this.listaFiscais = response.data.fiscais;
                    })
            }

        }
    });


    //vm modal configuração dados contrato

    var vmConfigurarContrato = new Vue({
        el: '#configurar',
        data: {
            dadosContrato: {},
            idContrato: '',
            listaEmpreendimentos: {},
            listaAditivos: {},
            listaReajustes: {},
            listaContratos: {},
            listaConstrutorasAssociadas: {},
            listaFiscais: [],
            mostrarContratadas: false,
            mostrarListaContratos: false,
            fiscalSelected: {
                CodigoContrato: '',
                CodigoUsuario: '',
                NomeUsuario: '',
                Email: '',
                Telefone: '',
                CPF: '',
                Tipo: '',
                CodigoPerfil: 0
            },
            tiposFiscal: [
                'Fiscal Técnico',
                'Fiscal Substituto',
                'Fiscal Adminstrativo'
            ],
            showFormFiscal: false,
            disableTipoFiscal: true,
        },
        watch: {
            async 'fiscalSelected.CPF'(val) {
                this.fiscalSelected.CodigoPerfil == 0;
                if (val.length == 11 && vmGlobal.validarCPF(val)) {
                    var fiscal = await vmGlobal.getFromAPI({
                        table: 'usuariosSistema',
                        columns: ['CPF', 'Email', 'NomeUsuario', 'Telefone', 'CodigoUsuario', 'CodigoPerfil'],
                        where: {
                            CPF: val
                        }
                    });

                    if (fiscal.length != 0) {
                        this.fiscalSelected.CPF = fiscal[0].CPF;
                        this.fiscalSelected.NomeUsuario = fiscal[0].NomeUsuario;
                        this.fiscalSelected.Telefone = fiscal[0].Telefone;
                        this.fiscalSelected.Email = fiscal[0].Email;
                        this.fiscalSelected.CodigoUsuario = fiscal[0].CodigoUsuario;
                        this.fiscalSelected.CodigoPerfil = fiscal[0].CodigoPerfil;

                        if (this.fiscalSelected.CodigoPerfil != 3) {
                            Swal.fire({
                                position: 'center',
                                type: 'warning',
                                title: 'Atenção!',
                                text: 'Usuário não pode ser associado, pois possui perfil diferente de fiscal.',
                                showConfirmButton: true,
                            });

                            this.showFormFiscal = false;
                        }
                    } else {
                        this.$nextTick(() => {
                            $('.is-disabled', this.$el).attr('disabled', false);
                        });

                        for (field in this.fiscalSelected) {
                            if (field == 'CPF') return;
                            if (field == 'CodigoPerfil') return;

                            this.fiscalSelected[field] = '';
                        }
                    }
                    this.disableTipoFiscal = false;
                }
            }
        },
        computed: {
            hasAllFiscais() {
                return this.listaFiscais.length === 3;
            },
            keyFiscal() {
                switch (this.fiscalSelected.Tipo) {
                    case 'Fiscal Técnico':
                        return 'FiscalTecnico'
                        break;
                    case 'Fiscal Substituto':
                        return 'FiscalSubstituto'
                        break;
                    case 'Fiscal Administrativo':
                        return 'FiscalAdministrativo';
                        break
                }

                return '';
            },
            isValidado() {
                return this.dadosContrato.Status == 4;
            },
            isNewFiscal() {
                var fiscal = this.listaFiscais.filter((fiscal) => {
                    return fiscal.CodigoUsuario == this.fiscalSelected.CodigoUsuario;
                });

                return fiscal.length === 0;
            },
            isValidCpf() {
                if (this.fiscalSelected.CPF.length == 11 && !vmGlobal.validarCPF(this.fiscalSelected.CPF)) {
                    return 'is-invalid';
                }

                return '';
            }
        },
        methods: {
            setDadosContrato(param) {

                var dados = Object.assign({}, param);
                this.idContrato = dados.CodigoContrato;
                delete dados.CodigoContrato;
                this.dadosContrato = dados;
                //buscar aditivos
                this.getAditivos(this.idContrato, dados.CodigoContrato);
                //buscar apostilamentos
                this.getApostilamentos(this.idContrato);
                //buscar Construtoras associadas
                this.listarContratosConstrutoras();
                //buscar fiscais
                this.getFiscais(this.idContrato);
                this.showFormFiscal = false;
            },
            async getAditivos(contrato) {
                var params = {
                    table: 'aditivos',
                    where: {
                        CodigoContrato: contrato
                    }
                }
                this.listaAditivos = await vmGlobal.getFromAPI(params, 'cgmab');
                //conta quantos estão preenchidos e calcula restante
                var AditivosPreenchidos = this.listaAditivos.filter(function(obj) {
                    return obj.NumeroSeiAditivo;
                });
                this.totalAditivos = this.listaAditivos.length - AditivosPreenchidos.length;

            },
            async getApostilamentos(contrato) {
                var params = {
                    table: 'apostilamentos',
                    where: {
                        CodigoContrato: contrato,

                    }
                }

                this.listaReajustes = await vmGlobal.getFromAPI(params, 'cgmab');
                //conta quantos apostilamentos estão preenchidos e calcula restante
                var reajustesPreenchidos = this.listaReajustes.filter(function(obj) {
                    return obj.NumeroSeiApostilamento;
                });
                this.totalReajustes = this.listaReajustes.length - reajustesPreenchidos.length;

            },
            async getContratos(tipo) {
                await vmInserirContratos.getContratos(tipo);
                this.listaContratos = vmInserirContratos.resultadoBusca;
                this.mostrarListaContratos = true;
            },
            async salvarContrato(dados) {
                var params = {
                    table: 'contratosConstrutoras',
                    data: {
                        CodigoContrato: this.idContrato,
                        SK_CONTRATO: dados.SK_CONTRATO,
                        NumeroContrato: dados.NU_CON_FORMATADO,
                        NomeEmpresa: dados.NO_EMPRESA,
                        ObjetoContrato: dados.DS_OBJETO,
                        CnpjEmpresa: dados.NU_CNPJ_CPF
                    }
                }
                await vmGlobal.insertFromAPI(params, null, 'cgmab');
                this.mostrarListaContratos = false;
                this.listarContratosConstrutoras();
            },
            async atualizarContrato() {

                //await this.validarPreenchimento();
                delete this.dadosContrato.Titulo
                delete this.dadosContrato.CodigoEmpreendimento
                delete this.dadosContrato.deleteTitulo
                delete this.dadosContrato.UF
                delete this.dadosContrato.Sigla
                delete this.dadosContrato.CodigoUF
                delete this.dadosContrato.Rodovia
                delete this.dadosContrato.Natureza
                delete this.dadosContrato.Extensao
                delete this.dadosContrato.Estagio
                delete this.dadosContrato.CodigoMT
                delete this.dadosContrato.CodigoDirex
                delete this.dadosContrato.TipoEmpreendimento
                delete this.dadosContrato.Modal
                delete this.dadosContrato.TipoItem
                delete this.dadosContrato.Status


                var params = {
                    table: 'contratos',
                    data: this.dadosContrato,
                    where: {
                        CodigoContrato: this.idContrato
                    }
                }
                await vmGlobal.updateFromAPI(params, true, 'cgmab');
                vmContratosLista.getContratos();
                this.dadosContrato = [];
                $("#dadosBasicos").modal('hide');
            },
            async listarContratosConstrutoras() {
                var params = {
                    table: 'contratosConstrutoras',
                    where: {
                        CodigoContrato: this.idContrato
                    }
                }
                this.listaConstrutorasAssociadas = await vmGlobal.getFromAPI(params, 'cgmab');
                if (this.listaConstrutorasAssociadas.length > 0) {
                    this.mostrarContratadas = true;
                }


            },
            async validarPreenchimento() {
                var erro = 1;
                //verifica se dados basicos foram preenchidos
                if (this.dadosContrato.NumeroSei &&
                    this.dadosContrato.LinkProcessoSEI &&
                    this.dadosContrato.NumeroSeiEdital &&
                    this.dadosContrato.NumeroSeiPropostaAprovadaEmpresa &&
                    this.dadosContrato.NumeroSeiApoliceCaucaoGarantia &&
                    this.dadosContrato.TipoContrato &&
                    this.dadosContrato.NumeroSeiContratoAssinado &&
                    this.dadosContrato.NumeroSeiApoliceCaucao &&
                    this.dadosContrato.CodigoEmpreendimento
                ) {
                    erro = 0
                }
                //Verifica de dados de Aditivos e apostilamentos foram preenchidos
                // if (this.totalAditivos === 0 && this.totalReajustes === 0) {
                //     erro = 0;
                // } else {
                //     erro += 1
                // }
                if (erro === 0) {
                    this.dadosContrato.Status = 1;
                } else {
                    this.dadosContrato.Status = 3;
                }
            },
            async atualizar(CodigoItem, event, tabela) {
                var params = {}

                if (tabela === 'apostilamentos') {
                    var params = {
                        table: tabela,
                        data: {
                            NumeroSeiApostilamento: event.target.value
                        },
                        where: {
                            CodigoApostilamento: CodigoItem
                        }
                    }
                } else {
                    params = {
                        table: tabela,
                        data: {
                            NumeroSeiAditivo: event.target.value
                        },
                        where: {
                            CodigoAditivo: CodigoItem
                        }
                    }
                }

                await vmGlobal.updateFromAPI(params, null, 'cgmab');

                this.getApostilamentos(this.idContrato);
                this.getAditivos(this.idContrato);
            },
            async filtrarEmpreendimentos(event) {
                params = {
                    table: 'empreendimentos',
                    where: {
                        UF: event.target.value
                    }
                }
                this.listaEmpreendimentos = await vmGlobal.getFromAPI(params);
                vmContratosLista.listaEmpreendimentos = this.listaEmpreendimentos; //preenche lista em contratos
            },
            formatReal(numero) {
                var numero = numero.split('.');
                numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                return numero.join(',');
            },
            async deletarContrutora(CodigoContrutorasContratos) {
                let params = {
                    table: 'contratosConstrutoras',
                    where: {
                        CodigoContrutorasContratos: CodigoContrutorasContratos
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                this.listarContratosConstrutoras();
            },
            async getFiscais(codigoContrato) {
                this.listaFiscais = [];
                await axios.post(base_url + 'Contratos/getFiscaisContrato', $.param({
                        CodigoContrato: codigoContrato
                    }))
                    .then(response => {
                        this.listaFiscais = response.data.fiscais;
                    })
            },
            selectFiscal(i) {
                this.$nextTick(() => {
                    $('.is-disabled', this.$el).attr('disabled', false);
                });
                this.showFormFiscal = true;
                this.tiposFiscal = [
                    'Fiscal Técnico',
                    'Fiscal Substituto',
                    'Fiscal Administrativo'
                ];

                this.disableTipoFiscal = true;
                this.showFormFiscal = true;
                this.fiscalSelected = Object.assign({}, i);
            },
            async salvarFiscal() {
                if (!vmGlobal.validarCPF(this.fiscalSelected.CPF)) return;
                if (this.fiscalSelected.Email.length == 0) return;
                if (this.fiscalSelected.NomeUsuario.length == 0) return;
                if (![0, 3].includes(parseInt(this.fiscalSelected.CodigoPerfil))) return;

                var data = {
                    NomeUsuario: this.fiscalSelected.NomeUsuario,
                    Email: this.fiscalSelected.Email,
                    Telefone: this.fiscalSelected.Telefone,
                    CPF: this.fiscalSelected.CPF
                };

                if (this.fiscalSelected.CodigoUsuario.length == 0) {
                    await axios.post(base_url + 'Contratos/salvarFiscal', $.param(data))
                        .then(async (response) => {
                            if (response.data.status) {

                                await this.asssociaFiscalContrato(response.data.result);

                                this.showFormFiscal = false;
                                this.getThisContrato();
                            }
                        });

                    return;
                } else if (this.isNewFiscal) {
                    await this.asssociaFiscalContrato(this.fiscalSelected.CodigoUsuario);
                    this.getThisContrato();
                    this.showFormFiscal = false;
                    return;
                }

                await vmGlobal.updateFromAPI({
                    table: 'usuariosSistema',
                    where: {
                        CodigoUsuario: this.fiscalSelected.CodigoUsuario
                    },
                    data: data
                });
                this.showFormFiscal = false;
                this.getThisContrato();
            },
            novoFiscal() {
                for (field in this.fiscalSelected) {
                    this.fiscalSelected[field] = '';
                }

                this.disableTipoFiscal = false;

                var tipos = this.listaFiscais.map((el) => {
                    return el.Tipo;
                });

                this.tiposFiscal = this.tiposFiscal.filter(el => !tipos.includes(el));

                this.showFormFiscal = true;

                this.$nextTick(() => {
                    $('.is-disabled', this.$el).attr('disabled', true);
                });

            },
            async asssociaFiscalContrato(val) {
                var fiscal = {};
                fiscal[this.keyFiscal] = val
                await vmGlobal.updateFromAPI({
                    table: 'contratos',
                    data: fiscal,
                    where: {
                        CodigoContrato: this.idContrato
                    }
                });
            },
            async getThisContrato() {
                var params = {
                    table: 'contratos',
                    where: {
                        CodigoContrato: this.idContrato
                    }
                }
                var contrato = await vmGlobal.getFromAPI(params, 'cgmab');
                this.setDadosContrato(contrato[0]);
            }
        }
    });
</script>