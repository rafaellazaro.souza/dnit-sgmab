<script>
    var vmCondicionantes = new Vue({
        el: '#condicionanteServico',
        data: {
            mostrarListaCondicionantes: false,
            mostrarRecursos: false,
            listaCondicionantes: [],
            codigoServico: 0,
            codigoEscopo: 0
        },
        methods: {
            async getCondicionantes(codigoEscopo, codigo = null) {
                this.codigoEscopo = codigoEscopo
                await axios.post(base_url + 'EscopoServicos/getCondiconantesEscopo', $.param({
                        CodigoEscopo: codigoEscopo
                    }))
                    .then((response) => {
                        this.listaCondicionantes = response.data;
                        // vmEscopoServicos.listaCondicionantesSelecionadas = response.data;
                        vmRecursosHumanos.listaCondicionantes = response.data;
                        vmInfraestrutura.listaCondicionantes = response.data;

                    })
                    .finally(() => {
                        this.codigoServico = parseInt(codigo);
                    })
            },
            voltar() {
                this.mostrarListaCondicionantes = false;
                vmEscopoServicos.mostrarTelaEscopoServicos = true
            },
            async iniciarDescricao(codigoEscopo) {
                vmDescricaoCronogramaFisicoProgramas.CodigoEscopoPrograma = codigoEscopo;
                vmDescricaoCronogramaFisicoProgramas.mostrarFormulario = true;
                await vmDescricaoCronogramaFisicoProgramas.getResponsaveis();
                await vmDescricaoCronogramaFisicoProgramas.getDadosDescricao();


            },
            async iniciarCronograma(codigoEscopo) {
                vmCronogramaFisicoProgramas.codigoEscopoPrograma = codigoEscopo;
                await vmCronogramaFisicoProgramas.getCronogramas();
                vmCronogramaFisicoProgramas.mostrarCronograma = true


            },
            iniciaExecucao(dadosDaCondicionante) {
                this.mostrarListaCondicionantes = false
                vmSituacaoExecucao.mostrarExecucao = true;
                vmSituacaoExecucao.DadosCondicionante = dadosDaCondicionante;
                vmControle.mostrarDadosContrato = false;
                vmControle.mostrarCaracterizacao = false;
            },
            async deletarCondicinante(codigo) {
                const params = {
                    table: 'escopoProgramas',
                    where: {
                        CodigoEscopoPrograma: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                this.getCondicionantes(this.codigoEscopo);
            }
        }
    });
</script>