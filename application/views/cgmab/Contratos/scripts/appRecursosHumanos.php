<script>
    var vmRecursosHumanos = new Vue({
        el: '#recursosHumanos',
        data: {
            listaRecursosHumanosServico: 0,
            listaRecursosHumanos: [],
            listaAnexosRecurso: [],
            listaServicos: [],
            listaCondicionantes: [],
            listaCondicionantesSelecionadas: [],
            codigoEscopoServico: '',
            codigoServico: '',
            cpfDigitado: '',
            Profissional: 0,
            NomeServico: '',
            mostraResultadoPesquisa: false,
            mostrarCondicionantes: false,
            mostrarDocumentos: false,
            dadosEscopoRecursos: [],
        },
        watch: {
            codigoServico: function(val) {
                this.getRecursosHumanosServico();
            }
        },
        methods: {
            async getRecursosHumanos() {
                var params = {
                    table: 'recursosHumanos',
                    where: {
                        CPF: this.cpfDigitado,
                        CodigoContratada: vmDadosContrato.codigoContratada
                    }
                }
                this.listaRecursosHumanos = await vmGlobal.getFromAPI(params, 'cgmab');
                this.mostraResultadoPesquisa = true;
                this.cpfDigitado = '';
            },
            async getAnexosRecurso(codigo){
                this.mostrarDocumentos = true;
                let params = {
                    table: 'anexos',
                    where: {
                        CodigoRecursosHumanos: codigo
                    }
                }
                this.listaAnexosRecurso = await vmGlobal.getFromAPI(params);
            },
            async salvarRecursoHumanoEscopo(codigoRecurso) {
                var params = {
                    table: 'escopoDeRecursos',
                    data: {
                        CodigoEscopo: this.codigoEscopoServico,
                        CodigoRecursosHumanos: codigoRecurso
                    }
                }

                await vmGlobal.insertFromAPI(params, null, 'cgmab');
                this.mostraResultadoPesquisa = false;
                this.resetarListaPessoasCondicionantes();
                this.getRecursosHumanosServico();

            },
            async getRecursosHumanosServico() {
                var params = {
                    CodigoContrato: vmEscopoServicos.CodigoContrato
                }
                var params2 = {
                    CodigoServico: this.codigoServico,
                    CodigoContrato: vmEscopoServicos.CodigoContrato
                };
                var controller = 'EscopoServicos/getRecurosHumanosServico';

                if (this.codigoServico) {
                    this.listaRecursosHumanosServico = await vmGlobal.getFromController(controller, params2);
                } else {
                    this.listaRecursosHumanosServico = await vmGlobal.getFromController(controller, params);
                }


                vmGlobal.montaDatatable('listaDeProfissionaisNoEscopo', true);
            },
            async getCondicionantes(profissional) {
                this.Profissional = profissional;
                if (this.listaCondicionantes.length === 0)
                    this.listaCondicionantes = vmCondicionantes.getCondicionantes(this.codigoEscopoServico);
                this.mostrarCondicionantes = true;
            },
            associarProfissionalCondicionante(condicionante, index, itens) {
                // this.listaCondicionantes.splice(index, 1);
                //cria lista de associaçção de profissional x condicionate
                var novosDados = this.dadosEscopoRecursos;
                var params = {
                    CodigoRecursosHumanos: this.Profissional,
                    CodigoCondicionante: condicionante
                }

                novosDados.push(params)
                this.dadosEscopoRecursos = novosDados;

                //cria lista com condicionantes selecionadas
                novosDados = this.listaCondicionantesSelecionadas;
                novosDados.push(itens);
                this.listaCondicionantesSelecionadas = novosDados;
            },
            removerAssociacaoCondicionates(index, item) {
                // this.listaCondicionantes.push(item);
                this.dadosEscopoRecursos.splice(index, 1);
                this.listaCondicionantesSelecionadas.splice(index, 1);
            },
            async salvarPessoaExecucao() {
                this.listaCondicionantesSelecionadas.forEach(async function(i, index) {
                   i.CodigoRecursosHumanos = vmRecursosHumanos.Profissional
                });

                var formData = new FormData();
                formData.append('Dados', JSON.stringify(this.listaCondicionantesSelecionadas));

                await axios.post(base_url + 'EscopoServicos/salvarPessoaExecucao?', formData)
                .then((resp)=>{
                    if (resp.data.status === false > 0) {
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Erro!',
                        text: "Erro ao tentar inserir os dados. " + e,
                        showConfirmButton: true,

                    });
                } else {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: 'Salvo!',
                        text: 'Recurso Adicionado Com Sucesso.',
                        showConfirmButton: true,
                    });
                }
                });

              

                this.mostraResultadoPesquisa = false;
                this.resetarListaPessoasCondicionantes()
                this.getRecursosHumanosServico();

            },
            resetarListaPessoasCondicionantes() {
                this.mostraResultadoPesquisa = false;
                this.mostrarCondicionantes = false;
                this.listaCondicionantesSelecionadas = [];
                this.listaRecursosHumanos = [];
                this.dadosEscopoRecursos = []
            },
            novoRecurso() {
                var base_url = '<?php echo base_url('RecursosHumanos') ?>';
                window.location.href = base_url;
            },
            async deletarRecurso(codigo) {
                params = {
                    table: 'escopoDeRecursos',
                    where: {
                        CodigoEscopoRecruso: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
            }
        }
    });
</script>