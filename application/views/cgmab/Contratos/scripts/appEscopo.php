<script>
    var vmEscopoServicos = new Vue({
        el: '#escopoServicos',
        data: {
            CodigoContrato: codigoContratoDecode,
            CodigoEscopo: '',
            empreendimento: '',
            mostrarTelaEscopoServicos: true,
            mostrarBtnBusca: true,
            mostrarAddEscopo: false,
            mostrarAddCondicionantes: false,
            mostrarBtnSalvarEscopo: true,
            mostrarTipoCondicionante: false,
            mostrarlistaLicencas: false,
            mostrarTema: false,
            mostrarEdicaoServico: false,
            mostrarNomePadrao: false,
            licensaSelecionada: '',
            listaEscopos: {},
            listaLicencas: {},
            listaServicos: {},
            listaEscopoServicos: 0,
            listaCondicionantes: {},
            listaCondicionantesSelecionadas: [],
            listaTiposCondicionantes: {},
            listaTemas: {},
            listaSubtipos: {},
            listaValidacaoPreenchimento1: {},
            listaValidacaoPreenchimento2: {},
            listaValidacaoPreenchimento3: {},
            totalMinimoSolicitarFiscal: parseInt(1),
            totalServicosAdicionados: 0,
            statusContrato: 0,
            mostrarBtnEnvioFiscal: false,
            desativarEnvioFiscal: false,
            dadosEscopo: {},
            numeroDigitado: '',
            deletarCondicionantes: false,
            licencaSelecionada: '',
            titulo: '',
            paramsCondicionantes: {
                table: 'ListagemCondicionantes',
                where: {
                    StatusCondicionante: 1,
                    CodigoLicenca: ''
                }
            },
            modelEdicao: {}

        },

        methods: {
            async getLicencas() {
                this.listaLicencas = await vmGlobal.getFromController('LicenciamentoAmbiental/getLicencas?NumeroLicenca=' + this.numeroDigitado);
                this.mostrarlistaLicencas = true;
            },
            async getTiposCondicionantes() {
                var params = {
                    table: 'tipos',
                    where: {
                        Status: 1
                    }
                }
                this.listaTiposCondicionantes = await vmGlobal.getFromAPI(params);
                this.mostrarTipoCondicionante = true;
            },
            async getCondicionantes() {
                var join = {
                    table: 'condicionantes',
                   
                    join:{
                        table: 'licencas',
                        on: 'codigoLicenca'
                    },
                  
                    joinLeft:[{
                        table1:'licencas',
                        table2: 'tiposLicencas',
                        on: 'CodigoLicencaTipo'
                    },
                    {
                        table1: 'condicionantes',
                        table2: 'subtipos',
                        on: 'CodigoSubtipo'
                    },
                    {
                        table1: 'subtipos',
                        table2: 'temas',
                        on: 'CodigoTema'
                    },
                    {
                        table1: 'condicionantes',
                        table2: 'tipos',
                        on: 'CodigoTipo'
                    },
                ]
                    // debug: 'safely_debug'
                }

                this.paramsCondicionantes = Object.assign({}, this.paramsCondicionantes, join);
                this.listaCondicionantes = await vmGlobal.getFromAPI(this.paramsCondicionantes, 'cgmab');
            },
            iniciarServicos() {
                this.getServicos();
            },
            async getEscoposServicos() {
            
                    await axios.get(base_url + 'EscopoServicos/getEscopoServicoContrato?CodigoContrato=' + this.CodigoContrato)
                        .then((response) => {
                            this.listaEscopoServicos = response.data;
                            vmRecursosHumanos.listaServicos = response.data;
                            vmInfraestrutura.listaServicos = response.data;
                        })
                        .finally(() => {
                            vmGlobal.montaDatatable('#listaEscopoServicos', true);
                            this.listaEscopos = this.listaEscopoServicos.map(x => x.CodigoEscopo).filter(function(elem, index, self) {
                                return index === self.indexOf(elem);
                            });
                        })
                
                //validar preenchimento
                this.validarPreenchimento();
            },
            async getServicos() {
                var params = {
                    table: 'servicos',
                    where: {
                        Status: 1
                    }
                }

                this.listaServicos = await vmGlobal.getFromAPI(params);
            },
            async getTemas() {
                var params = {
                    table: 'temas',
                    where: {
                        Status: 1
                    }
                }

                this.listaTemas = await vmGlobal.getFromAPI(params);
            },
            async getSubTipos(event) {
                var params = {
                    table: 'subtipos',
                    where: {
                        CodigoTema: event.target.value,
                        Status: 1
                    }
                }

                this.listaSubtipos = await vmGlobal.getFromAPI(params);
                this.mostrarNomePadrao = true;
            },
            setCodigoEscopo(CodigoEscopo) {
                this.CodigoEscopo = CodigoEscopo;
                this.mostrarAddCondicionantes = true;
                vmCondicionantes.getCondicionantes(this.CodigoEscopo);
            },
            setTiposCondicionantes(licenca) {
                this.licencaSelecionada = licenca;
                this.paramsCondicionantes.where.CodigoLicenca = licenca;
                this.mostrarlistaLicencas = false;
                this.getCondicionantes();
                this.getTiposCondicionantes();

            },
            async setTipoFiltro(event, campo) {
                if (campo === 'Tipo') {
                    this.paramsCondicionantes.where.CodigoTipo = event.target.value;
                    if (this.paramsCondicionantes.where.CodigoTipo != 1) {
                        this.mostrarTema = true;
                        this.getTemas()
                    } else {
                        this.mostrarTema = false;
                        this.listaTemas = {}
                    }

                } else {
                    this.paramsCondicionantes.where.CodigoSubTipo = event.target.value;
                }

                await this.getCondicionantes(this.licencaSelecionada);
            },
            setCondicionanteSelecionada(dados, codigoArray) {
                
            //    var numeroCondicional, codigo, titulo, descricao
                this.listaCondicionantes.splice(codigoArray, 1)
                this.listaCondicionantesSelecionadas.push({
                    CodigoEscopo: this.CodigoEscopo,
                    NumeroCondicional: dados.NumeroCondicional,
                    CodigoCondicionante: dados.CodigoCondicionante,
                    Titulo: dados.Titulo,
                    Descricao: dados.Descricao,
                    Sigla: dados.Sigla,
                    NumeroLicenca: dados.NumeroLicenca,
                    NomeSubtipo: dados.NomeSubtipo,
                    NomeTema: dados.NomeTema,
                    NomeTipo: dados.NomeTipo,
                });
                //this.mostrarBtnBusca = false;
            },
            async salvarEscopoServicos() {
                this.dadosEscopo.CodigoContrato = codigoContratoDecode;

                //Verifica se escopo ja existe no contrato
                var existeEscopoContrato = await this.verificaEscopoExistente();
                if (existeEscopoContrato === true) {
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Erro!',
                        text: "Erro ao tentar inserir. Escopo Já foi Cadastrado. ",
                        showConfirmButton: true,
                    });
                } else {

                    await axios.post(base_url + 'EscopoServicos/salvarEscopoServicoContrato', $.param(this.dadosEscopo))
                        .then((response) => {
                            if (response.data.status == false)
                                throw new Error(response.data.result);

                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Salvo!',
                                text: 'Escopo de serviço Adicionado Com Sucesso. Selecione quais condicionantes compoem o serviço!',
                                showConfirmButton: true,
                            });

                            //preenche o codigo do escpopo adicionado
                            this.CodigoEscopo = response.data.result;
                        })
                        .catch((e) => {
                            console.log(e)
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: 'Erro!',
                                text: "Erro ao tentar inserir os dados. " + e,
                                showConfirmButton: true,

                            });
                        });
                    await this.getCondicionantes();
                    this.listaEscopoServicos = [];
                    this.getEscoposServicos();
                    // this.mostrarAddCondicionantes = true;

                    this.mostrarAddEscopo = false;
                    this.dadosEscopo = {};
                }
            },
            async verificaEscopoExistente() {
                let params = {
                    table: 'escopoDeServicos',
                    where: {
                        CodigoContrato: codigoContratoDecode,
                        CodigoServico: this.dadosEscopo.CodigoServico
                    }
                }
                var busca = await vmGlobal.getFromAPI(params, 'cgmab');
                console.log(busca);
                if (busca.length > 0) {
                    return true;
                } else {
                    return false;
                }

            },
            async validarPreenchimento() {
                //verifica quantos serviços foram adicionados pela contratada
                this.totalServicosAdicionados = parseInt(this.listaEscopoServicos.length);
                //verificar o preenchimento dos ítens de cada serviço
                if (this.totalServicosAdicionados > 0) {
                    this.listaEscopoServicos.forEach(async function(item, i) {
                        var CodigoDoEscopo = item.CodigoEscopo;
                        var params = {
                            table: 'vw_verificarPreenchimentoCaracterizacao',
                            where: {
                                CodigoContrato: codigoContratoDecode,
                                CodigoEscopo: CodigoDoEscopo
                            }
                        }

                        let data = await vmGlobal.getFromAPI(params, 'cgmab');
                        if (i === 0) {
                            vmEscopoServicos.listaValidacaoPreenchimento1 = data[0];
                            await vmEscopoServicos.validarTotais(vmEscopoServicos.listaValidacaoPreenchimento1);
                        } else if (i === 1) {
                            vmEscopoServicos.listaValidacaoPreenchimento2 = data[0];
                            await vmEscopoServicos.validarTotais(vmEscopoServicos.listaValidacaoPreenchimento2);
                        } else {
                            vmEscopoServicos.listaValidacaoPreenchimento3 = data[0];
                            await vmEscopoServicos.validarTotais(vmEscopoServicos.listaValidacaoPreenchimento3);
                        }

                    });
                    //valida

                }

            },
            async validarTotais(lista) {
                if (lista.totalServico >= this.totalMinimoSolicitarFiscal &&
                    lista.totalCondicionantes >= this.totalMinimoSolicitarFiscal &&
                    lista.totalRecursoHumanos >= this.totalMinimoSolicitarFiscal &&
                    lista.totalVeiculo >= this.totalMinimoSolicitarFiscal &&
                    lista.totalEquipamento >= this.totalMinimoSolicitarFiscal &&
                    lista.TotalEstruturas >= this.totalMinimoSolicitarFiscal) {
                    this.desativarEnvioFiscal = true; //libera
                    this.statusContrato = lista.Status;
                    vmDadosContrato.listaContrato.Status = lista.Status;


                } else {
                    this.desativarEnvioFiscal = false //bloqueia
                }
            },
            async salvarCondicionantesEscopoServicos() {
                var erro = 0;
                //Grava condicionantes do serviço
                
                this.listaCondicionantesSelecionadas.forEach(async function(i, id) {
                    await axios.post(base_url + 'EscopoServicos/salvarCondicionantes?CodigoEscopo=' + i.CodigoEscopo + '&CodigoCondicionante=' + i.CodigoCondicionante + '')
                        .then((response) => {
                            //verifica erros
                            if (response.data.status == false)
                                erro += 1;
                        })
                })

                if (erro > 0) {
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Erro!',
                        text: "Erro ao tentar inserir os dados. " + e,
                        showConfirmButton: true,

                    });
                } else {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: 'Salvo!',
                        text: 'Escopo de serviço Adicionado Com Sucesso.',
                        showConfirmButton: true,
                    });
                }

                //reseta tela
                this.cancelarCondicionantes();

            },
            cancelarCondicionantes(botao = null) {
                this.mostrarTema = false;
                this.mostrarNomePadrao = false;
                this.listaCondicionantes = {};
                this.numeroDigitado = '';
                this.mostrarTipoCondicionante = false;
                this.listaTiposCondicionantes = {};
                this.listaLicencas = {};
                this.listaCondicionantesSelecionadas = [];

                if (botao === 'reset') {
                    this.mostrarlistaLicencas = false;
                    this.mostrarBtnBusca = true;

                } else {
                    this.mostrarAddCondicionantes = false;
                }


            },
            listarCondicionantes(servico, titulo, codigo) {
                vmCondicionantes.mostrarListaCondicionantes = true;
                this.mostrarTelaEscopoServicos = false;
                this.titulo = titulo;
                vmCondicionantes.getCondicionantes(servico, codigo);
            },


            confirmarEnvioFiscal() {
                Swal.fire({
                    title: 'Deseja enviar dados para validação do fiscal?',
                    text: "Ao efetuar este procedimento, os dados não poderão ser editados!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim Enviar!'
                }).then((result) => {
                    if (result.value === true) {
                        this.enviarFiscal();
                        Swal.fire(
                            'Dados Enviados!',
                            'Aguarde o retorno do fical',
                            'success'
                        )
                    }
                });
            },
            async enviarFiscal() {
                this.listaEscopos.forEach(async function(i) {
                    const params = {
                        table: 'envioFiscal',
                        data: {
                            CodigoEscopo: i,
                            Mes: new Date().getMonth() + 1,
                            Ano: new Date().getFullYear(),
                            DataEnvioFiscal: new Date(),
                            ConfiguracaoInicial: 1
                        }
                    }
                    await vmGlobal.insertFromAPI(params, null, 'cgmab');

                    //Altera Status do contrato para bloqueado
                    await vmDadosContrato.bloquearContrato();


                });




            },
            async deletarCondicionantesServicos(escopo, index) {
                this.listaCondicionantesSelecionadas.splice(index, 1);
                let params = {
                    table: 'escopoProgramas',
                    where: {
                        CodigoEscopoPrograma: escopo
                    }
                }

                await vmGlobal.deleteFromAPI(params);

            },
            async editarDescricao(){
                $(".page-loader-wrapper").css('display', 'block');
                const params = {
                    table: 'escopoDeServicos',
                    data:{
                        DetalheEscopo:  this.modelEdicao.DetalheEscopo
                    },
                    where:{
                        CodigoEscopo: this.modelEdicao.CodigoEscopo
                    }
                }
                await vmGlobal.updateFromAPI(params, null);
                await this.getEscoposServicos();
                this.mostrarEdicaoServico = false;
                this.modelEdicao = {}
                $(".page-loader-wrapper").css('display', 'none');

            },

            async deletar(codigo){
                $(".page-loader-wrapper").css('display', 'block');
                const params = {
                    table: 'escopoDeServicos',
                    where:{
                        CodigoEscopo:codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                await this.getEscoposServicos();
                $(".page-loader-wrapper").css('display', 'none');
            }
        }


    });
</script>