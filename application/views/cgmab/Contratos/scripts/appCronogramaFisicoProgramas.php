<script>
    vmCronogramaFisicoProgramas = new Vue({
        el: '#cronogramasFisicos',
        data: {
            mostrarCronograma: false,
            mostrarAddCronograma: false,
            mostrarAddAtividade: false,
            mostrarAtividades: false,
            mostrarAtualizarCronograma: false,
            mostrarBtnAddCronograma: true,
            dadosCronograma: {},
            dadosAtividade: {},
            codigoCronograma: 0,
            updateAtividade: parseInt(0),
            codigoEscopoPrograma: 0,
            errosFormCronograma: 0,
            errosFormAtividade: 0,
            errosFormAtividades: 0,
            listaCronogramas: 0,
            listaAtividades: 0,
            divideColuna: 'col-12 w-100 mt-4 p-3 border',
            marcador: ''
        },
        methods: {
            async getCronogramas() {
                var params = {
                    table: 'conogramasDosProgramas',
                    where: {
                        CodigoEscopoPrograma: this.codigoEscopoPrograma
                    }
                }
                this.listaCronogramas = await vmGlobal.getFromAPI(params, 'cgmab');
            },
            async getAtividades() {
                var params = {
                    table: 'atividadesDoCronograma',
                    where: {
                        CodigoCronogramaFisicoProgramas: this.dadosCronograma.CodigoCronogramaFisicoProgramas
                    },
                    orderBy: ['DataMesAtividade']
                }
                this.listaAtividades = await vmGlobal.getFromAPI(params, 'cgmab');
            },
            getDadosCronograma(i) {
                this.dadosCronograma = i;
                this.mostrarBtnAddCronograma = false;
                this.getAtividades();
            },
            getDadosAtividade(i) {
                this.dadosAtividade = i;
                this.mostrarAddAtividade = true;
            },
            async checkForm(update = false) {
                if (this.dadosCronograma.DuracaoPrograma && this.dadosCronograma.PrevisaoInicio) {
                    if (update === false) {
                        await this.salvarCronograma();
                    } else {
                        this.atualizarCronograma()
                    }
                    return true;
                }

                this.errosFormCronograma = [];

                if (!this.dadosCronograma.DuracaoPrograma) {
                    this.errosFormCronograma.push('O campo Duração é Obrigatório.');
                }
                if (!this.dadosCronograma.PrevisaoInicio) {
                    this.errosFormCronograma.push('O campo  Previsão de Início é Obrigatório.');
                }


            },
            async checkFormAtividade(update = false) {

                //var dataAtividade = this.dadosAtividade.MesAtividade + '-'+'01'+'-'+this.dadosAtividade.AnoAtividade;

                var dataAtividade = new Date(parseInt(this.dadosAtividade.AnoAtividade), parseInt(this.dadosAtividade.MesAtividade) - 1, 1);
                dataAtividade.setHours(0, 0, 0, 0);

                var dataCronograma = this.dadosCronograma.PrevisaoInicio.split('-');

                dataCronograma = new Date(dataCronograma[0], dataCronograma[1] - 1, 1);
                dataCronograma.setHours(0, 0, 0, 0);

                if (this.dadosAtividade.Atividade &&
                    this.dadosAtividade.MesAtividade &&
                    this.dadosAtividade.PossuiRelatorio &&
                    this.dadosAtividade.AnoAtividade &&
                    dataAtividade.getTime() >= dataCronograma.getTime()) {
                    // debugger
                    if (update === false) {
                        await this.salvarAtividade();
                    } else {
                        await this.atualizarAtividade()
                    }
                    return true;
                }

                this.errosFormAtividade = [];

                if (!this.dadosAtividade.Atividade) {
                    this.errosFormAtividade.push('O campo Atividade é Obrigatório.');
                }
                if (!this.dadosAtividade.MesAtividade) {
                    this.errosFormAtividade.push('O campo  Mes da Atividade de Início é Obrigatório.');
                }
                if (!this.dadosAtividade.PossuiRelatorio) {
                    this.errosFormAtividade.push('O campo  Possui Relatório é Obrigatório.');
                }
                if (!this.dadosAtividade.AnoAtividade) {
                    this.errosFormAtividade.push('O campo Ano da Atividade é Obrigatório.');
                }
                if (dataAtividade.getTime() < dataCronograma.getTime()) {
                    this.errosFormAtividade.push('É necessário que a data da atividade seja maior que a data da previsão de início do relatório.');
                }

            },
            async salvarCronograma() {
                this.dadosCronograma.CodigoEscopoPrograma = this.codigoEscopoPrograma
                var params = {
                    table: 'conogramasDosProgramas',
                    data: this.dadosCronograma
                }
                await vmGlobal.insertFromAPI(params, 'cgmab')
                this.getCronogramas();
                this.dadosCronograma = {}
                this.mostrarAddCronograma = false;
            },
            async atualizarCronograma() {
                this.codigoCronograma = this.dadosCronograma.CodigoCronogramaFisicoProgramas;
                delete this.dadosCronograma.CodigoCronogramaFisicoProgramas;
                var params = {
                    table: 'conogramasDosProgramas',
                    data: this.dadosCronograma,
                    where: {
                        CodigoCronogramaFisicoProgramas: this.codigoCronograma
                    }
                }
                await vmGlobal.updateFromAPI(params, null, 'cgmab');
                this.getCronogramas();
            },
            async salvarAtividade() {
                this.dadosAtividade.CodigoCronogramaFisicoProgramas = this.dadosCronograma.CodigoCronogramaFisicoProgramas;
                this.dadosAtividade.DataMesAtividade = this.dadosAtividade.AnoAtividade + '/' + this.dadosAtividade.MesAtividade + '/1';

                var params = {
                    table: 'atividadesDoCronograma',
                    data: this.dadosAtividade
                }
                await vmGlobal.insertFromAPI(params, 'cgmab')
                this.getAtividades();
                this.dadosAtividade = {}
                this.mostrarAddAtividade = false;
            },
            async atualizarAtividade() {
                this.codigoCronograma = this.dadosAtividade.CodigoAtividadeCronogramaFisico;
                this.dadosAtividade.DataMesAtividade = this.dadosAtividade.AnoAtividade + '/' + this.dadosAtividade.MesAtividade + '/1';
                delete this.dadosAtividade.CodigoAtividadeCronogramaFisico;
                var params = {
                    table: 'atividadesDoCronograma',
                    data: this.dadosAtividade,
                    where: {
                        CodigoAtividadeCronogramaFisico: this.codigoCronograma
                    }
                }
                await vmGlobal.updateFromAPI(params, null, 'cgmab');
                this.getAtividades();
                this.mostrarAddAtividade = false;
            },
            async deletar(tabela, codigo, atualizar) {
                if (tabela === 'atividadesDoCronograma') {
                    var params = {
                        table: tabela,
                        where: {
                            CodigoAtividadeCronogramaFisico: codigo
                        }
                    }
                } else {
                    // params = {
                    //     table: tabela,
                    //     where: {
                    //         CodigoIndicadoresProgramas: codigo
                    //     }
                    // }
                }

                await vmGlobal.deleteFromAPI(params);
                tabela === 'atividadesDoCronograma' ? this.getAtividades() : '';
            }


        }
    });
</script>