<script>
var vmCaracterizacao = new Vue({
    el: '#uploadShapeFile',
    data: {
        file: '',
        listaExel: {},
        listaAreasApoio: 0,
        listaAreasLicenciadas: 0,
        listaContratadas: 0,
        listaLicencas: 0,
        listaLicencasBusca: 0,
        mostrarListaExcel: false,
        mostrarModalLicenciamento: false,
        mostrarLicencas: false,
        numeroDigitadoLicenca: '',
        codigoContratada: 0,
        codigoAreaApoio: 0,
    },
    methods: {
        async getAreasApoio() {
            $('#ListaAreasApoios').DataTable().destroy();
            params = {
                table: 'areasApoio',
               
                 where: {
                    CodigoContrato: codigoContratoDecode ,
                }
            }
           
                this.listaAreasApoio = await vmGlobal.getFromAPI(params, 'cgmab');
           
            vmGlobal.montaDatatable('#ListaAreasApoios', true)
        },
        async getLicenciamentoAreas(item) {
            const params = {
                table: 'construtorasLicencas',
                joinLeft: [{
                        table1: 'construtorasLicencas',
                        table2: 'areasApoio',
                        on: 'CodigoAreaApoio'
                    },
                    {
                        table1: 'construtorasLicencas',
                        table2: 'contratosConstrutoras',
                        on: 'CodigoContrutorasContratos'
                    },
                   
                    {
                        table1: 'construtorasLicencas',
                        table2: 'licencas',
                        on: 'CodigoLicenca'
                    },
                    {
                        table1: 'licencas',
                        table2: 'ListagemOrgaoExpeditor',
                        on: 'CodigoOrgaoExpeditor'
                    },
                ],
                where: {
                    CodigoAreaApoio: item
                }
            }
            this.listaAreasLicenciadas = await vmGlobal.getFromAPI(params, 'cgmab');
            vmListas.getContratadasContrato(codigoContratoDecode);
            this.mostrarModalLicenciamento = true;
            this.codigoAreaApoio = item;
        },
        async getLicencas() {
                var params = {
                    table: 'licencas',
                    joinMult: [{
                        table: 'orgaosExpeditores',
                        on: 'CodigoOrgaoExpeditor'
                    },
                    {
                        table: 'tiposLicencas',
                        on: 'CodigoLicencaTipo'
                    },
                
                ],
                    like: {
                        NumeroLicenca: this.numeroDigitadoLicenca
                    }
                }
                this.listaLicencasBusca = await vmGlobal.getFromAPI(params, 'cgmab');
                this.mostrarLicencas = true;
            },
        async importar() {

            //criar sessao com dados da condicionante
            //this.armazenarDadosImportados();

            //upload do arquivo excel
            this.file = this.$refs.file.files[0];

            let formData = new FormData();
            formData.append('file', this.file);

            await axios.post('<?= base_url('API/uploadXLS') ?>', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                .then((response) => {
                    this.listaExel = response.data
                })
                .catch((error) => {
                    console.log(error);
                })
                .finally(() => {
                    this.mostrarListaExcel = true;
                    $(".dropify-wrapper").addClass('invisible');
                });

        },
        async salvarEstruturas() {

            await vmGlobal.getFromController('AreasApoio/salvar?CodigoContrato='+codigoContratoDecode);
            Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Salvo!',
                text: 'Os dados foram salvos com sucesso.',
                showConfirmButton: true,
            });

            this.mostrarListaExcel = false;
            this.listaExel = {}
            this.getAreasApoio();

        },
        async salvarLicenciamentoAmbiental(numeroLicenca){
            params = {
                table: 'construtorasLicencas',
                data: {
                    CodigoAreaApoio: this.codigoAreaApoio,
                    CodigoContrutorasContratos: this.codigoContratada,
                    CodigoLicenca: numeroLicenca
                }
            }
            await vmGlobal.insertFromAPI(params, null, 'cgmab');
            this.getLicenciamentoAreas(this.codigoAreaApoio);
            this.mostrarLicencas = false;
        },
        async deletar(codigo, index) {
            params = {
                table: 'areasApoio',
                where: {
                    CodigoAreaApoio: codigo
                }
            }
            await vmGlobal.deleteFromAPI(params);
            this.listaAreasApoio.splice(index, 1);
        }
    }
});



var vmListas = new Vue({
    methods: {
        async getContratadasContrato(contrato) {
            const params = {
                table: 'contratosConstrutoras',
                where: {
                    CodigoContrato: contrato
                }
            }
            vmCaracterizacao.listaContratadas = await vmGlobal.getFromAPI(params, 'cgmab');
        },
        async getLicencas(Numero) {
            const params = {
                table: 'licensasAmbientais',
                where: {
                    Numero: Numero
                }
            }
            vmCaracterizacao.listaLicencas = await vmGlobal.getFromAPI(params, 'cgmab');
        },
    }

});
</script>