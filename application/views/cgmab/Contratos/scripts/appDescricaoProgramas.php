<script>
    var vmDescricaoCronogramaFisicoProgramas = new Vue({
        el: '#descricaoProgramas',
        data: {
            mostrarFormulario: false,
            mostrartabelas: false,
            mostrarAddMeta: false,
            mostrarAddIndicador: false,
            listaResponsaveis: [],
            listaMetas: [],
            listaIndicadores: [],
            CodigoEscopoPrograma: 0,
            dadosDescricao: {},
            dadosIndicador: {},
            dadosMeta: {},
            update: false,
            erros: []

        },
        methods: {
            async getResponsaveis() {
                this.dadosDescricao.CodigoEscopoPrograma = this.CodigoEscopoPrograma;
                // buscar todo mundo que foi associado no serviço execução de programas 
                this.listaResponsaveis = await vmGlobal.getFromController('EscopoServicos/getPessoasEmExecucaoProgramas?CodigoEscopoPrograma=' + this.CodigoEscopoPrograma);
            },
            async getDadosDescricao() {
                $('.page-loader-wrapper').css('diplay', 'block');

                // buscar descrição do programa/ plano
                var params = {
                    table: 'descricaoPrograma',
                    where: {
                        CodigoEscopoPrograma: this.CodigoEscopoPrograma
                    }
                }
                var dadosBusca = await vmGlobal.getFromAPI(params, 'cgmab');
                if (dadosBusca.length >= 1) {
                    this.dadosDescricao = dadosBusca[0];

                    CKEDITOR.instances['Descricao'].setData(atob(this.dadosDescricao.Descricao));
                    CKEDITOR.instances['Justificativa'].setData(atob(this.dadosDescricao.Justificativa));
                    CKEDITOR.instances['Objetivo'].setData(atob(this.dadosDescricao.Objetivo));
                    CKEDITOR.instances['Metodologia'].setData(atob(this.dadosDescricao.Metodologia));
                    CKEDITOR.instances['PublicoAlvo'].setData(atob(this.dadosDescricao.PublicoAlvo));
                    CKEDITOR.instances['AtividadesPrevistas'].setData(atob(this.dadosDescricao.AtividadesPrevistas));

                    this.mostrartabelas = true;
                    this.update = true;
                    //trazMetas
                    this.getMetas();
                    this.getIndicador();

                } else {
                    CKEDITOR.instances['Descricao'].setData('');
                    CKEDITOR.instances['Justificativa'].setData('');
                    CKEDITOR.instances['Objetivo'].setData('');
                    CKEDITOR.instances['Metodologia'].setData('');
                    CKEDITOR.instances['PublicoAlvo'].setData('');
                    CKEDITOR.instances['AtividadesPrevistas'].setData('');

                    this.dadosDescricao = {}
                    this.mostrartabelas = false;
                    this.update = false;
                }

                $('.page-loader-wrapper').css('diplay', 'none');


            },
            async checkForm(e) {
            
                // this.dadosDescricao.Justificativa && this.dadosDescricao.Descricao && this.dadosDescricao.Objetivo && this.dadosDescricao.PublicoAlvo && this.dadosDescricao.Metodologia && this.dadosDescricao.AtividadesPrevistas && 
                if (this.dadosDescricao.CodigoRecursosHumanos) {
                    await this.savarDescricao();
                    return true;
                }

                this.erros = [];
               
                // if (!this.dadosDescricao.Justificativa) {
                //     this.erros.push('O campo Justificativa é Obrigatório.');
                // }
                // if (!this.dadosDescricao.Descricao) {
                //     this.erros.push('O campo  Descrição é Obrigatório.');
                // }
                // if (!this.dadosDescricao.Objetivo) {
                //     this.erros.push('O campo  Objetivo é Obrigatório.');
                // }
                // if (!this.dadosDescricao.PublicoAlvo) {
                //     this.erros.push('O campo  Publico Alvo é Obrigatório.');
                // }
                // if (!this.dadosDescricao.Metodologia) {
                //     this.erros.push('O campo  Metodologia é Obrigatório.');
                // }
                // if (!this.dadosDescricao.AtividadesPrevistas) {
                //     this.erros.push('O campo  Atividades Previstas é Obrigatório.');
                // }
                if (!this.dadosDescricao.CodigoRecursosHumanos) {
                    this.erros.push('O campo  Responsável é Obrigatório.');
                }

                e.preventDefault();
            },
            async checkFormMeta(e) {
                if (this.dadosMeta.Meta) {
                    await this.salvarMeta();
                    return true;
                }

                this.erros = [];

                if (!this.dadosMeta.Meta) {
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Erro!',
                        text: "Preencha o campo Meta ",
                        showConfirmButton: true,
                    });
                }


                e.preventDefault();
            },
            checkFormIndicador(e) {
                if (this.dadosIndicador.Indicador && this.dadosIndicador.Descricao && this.dadosIndicador.FormulaCalculo) {
                    this.salvarIndicador();
                    return true;
                }

                this.erros = [];

                if (!this.dadosIndicador.Indicador || !this.dadosIndicador.Descricao || !this.dadosIndicador.FormulaCalculo) {
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Erro!',
                        text: "Todos os Campos São Obrigatórios ",
                        showConfirmButton: true,
                    });
                }


                e.preventDefault();
            },
            async savarDescricao() { //insere e atualiza dados

                var params = {
                    table: 'descricaoPrograma',
                    data: this.dadosDescricao
                }
                if (this.update === false) {
                    this.dadosDescricao.CodigoEscopoPrograma = this.CodigoEscopoPrograma;
                    await vmGlobal.insertFromAPI(params, null, 'cgmab');
                    await this.getDadosDescricao();
                    this.mostrartabelas = true;
                    this.listaIndicadores = {};
                    this.listaMetas = {};
                } else {
                    var codigo = this.dadosDescricao.CodigoDescricaoPrograma;
                    //remove o codigo para update
                    delete this.dadosDescricao.CodigoDescricaoPrograma;
                    var params = {
                        table: 'descricaoPrograma',
                        data: this.dadosDescricao,
                        where: {
                            CodigoDescricaoPrograma: codigo
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null, 'cgmab');
                    await this.getDadosDescricao();
                    this.erros = {}
                    this.mostrarFormulario = {}
                
                }


            },
            async getMetas() {
                var params = {
                    table: 'metas',
                    where: {
                        CodigoDescricaoPrograma: this.dadosDescricao.CodigoDescricaoPrograma
                    }
                }
                this.listaMetas = await vmGlobal.getFromAPI(params, 'cgmab');
            },
            async getIndicador() {
                var params = {
                    table: 'indicadores',
                    where: {
                        CodigoDescricaoPrograma: this.dadosDescricao.CodigoDescricaoPrograma
                    }
                }
                this.listaIndicadores = await vmGlobal.getFromAPI(params, 'cgmab');
            },
            async salvarMeta() {
                this.dadosMeta.CodigoDescricaoPrograma = this.dadosDescricao.CodigoDescricaoPrograma
                var params = {
                    table: 'metas',
                    data: this.dadosMeta,
                }
                await vmGlobal.insertFromAPI(params, null, 'cgmab');
                await this.getMetas();
                this.mostrarAddMeta = false;
                this.dadosMeta.Meta = '';
            },
            async salvarIndicador() {
                this.dadosIndicador.CodigoDescricaoPrograma = this.dadosDescricao.CodigoDescricaoPrograma;
                var params = {
                    table: 'indicadores',
                    data: this.dadosIndicador
                }
                await vmGlobal.insertFromAPI(params, null, 'cgmab');
                this.getIndicador();
                this.dadosIndicador = {};
                this.mostrarAddIndicador = false;

            },

            async deletar(tabela, codigo) {
                if (tabela === 'metas') {
                    var params = {
                        table: tabela,
                        where: {
                            CodigoMetaProgramas: codigo
                        }
                    }
                } else {
                    params = {
                        table: tabela,
                        where: {
                            CodigoIndicadoresProgramas: codigo
                        }
                    }
                }

                await vmGlobal.deleteFromAPI(params);
                tabela === 'metas' ? this.getMetas() : this.getIndicador();
            }
        }
    });




    $(document).ready(function() {

        CKEDITOR.replace('editor1');
        CKEDITOR.replace('editor2');
        CKEDITOR.replace('editor3');
        CKEDITOR.replace('editor4');
        CKEDITOR.replace('editor5');
        CKEDITOR.replace('editor6');

        CKEDITOR.instances['Descricao'].on("change", function() {
            vmDescricaoCronogramaFisicoProgramas.dadosDescricao.Descricao = btoa(CKEDITOR.instances['Descricao'].getData());
        });
        CKEDITOR.instances['Justificativa'].on("change", function() {
            vmDescricaoCronogramaFisicoProgramas.dadosDescricao.Justificativa = btoa(CKEDITOR.instances['Justificativa'].getData());
        });
        CKEDITOR.instances['Objetivo'].on("blur", function() {
            vmDescricaoCronogramaFisicoProgramas.dadosDescricao.Objetivo = btoa(CKEDITOR.instances['Objetivo'].getData());
        });
        CKEDITOR.instances['Metodologia'].on("change", function() {
            vmDescricaoCronogramaFisicoProgramas.dadosDescricao.Metodologia = btoa(CKEDITOR.instances['Metodologia'].getData());
        });
        CKEDITOR.instances['PublicoAlvo'].on("change", function() {
            vmDescricaoCronogramaFisicoProgramas.dadosDescricao.PublicoAlvo = btoa(CKEDITOR.instances['PublicoAlvo'].getData());
        });
        CKEDITOR.instances['AtividadesPrevistas'].on("change", function() {
            vmDescricaoCronogramaFisicoProgramas.dadosDescricao.AtividadesPrevistas = btoa(CKEDITOR.instances['AtividadesPrevistas'].getData());
        });
    });
</script>