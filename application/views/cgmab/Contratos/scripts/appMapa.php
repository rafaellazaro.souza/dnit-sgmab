
<script>
var vmMapaContratada = new Vue({
    el: '#mapaContratada',
    data:{
        mymap: null
    },
    mounted(){
        this.gerarMapa();
    },
    methods:{
        gerarMapa() {
                const style = 'reduced.night';
                this.mymap = L.map('mapid').setView(['-10.6007767', '-63.6037797'], 4.5);
                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 18,
                    // attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                    //     '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
                    id: 'mapbox.streets',
                    fadeAnimation: true,
                }).addTo(this.mymap);
            },

    }
});
</script>