<script>
    const codigoContratoDecode = vmGlobal.codificarDecodificarParametroUrl('<?php echo $CodigoContrato ?>', 'decode');
    const perfilUsuario = '<?= $this->session->PerfilLogin ?>';

    var base_url = '<?php echo base_url() ?>';
    $('#preloader').hide();

    var vmDadosContrato = new Vue({
        el: '#dadosContrato',
        data: {
            listaContrato: {},
            habilitarParecer: false,
            listaAditivos: {},
            listaApostilamentos: {},
            dadosVerificarEnvio: {
                Mes: 2,
                Ano: 2020,
                CodigoContrato: codigoContratoDecode,
            },
            codigoContratada: '<?= $_SESSION['Logado']['CodigoContratada']; ?>',
            VerificaEnvioFiscal: {},

        },
        async mounted() {
            await this.getContrato();
            // await this.getVerificaConfiguracaoInicialContrato();
            await this.getAdtivos();
            await this.getApostilamentos();
            await vmEscopoServicos.validarPreenchimento();
        },
        computed: {
            habilitarEdicao() {
                if (perfilUsuario.includes('Fiscal')) {
                    return true;
                } else if (
                    Array.isArray(this.listaContrato) &&
                    this.listaContrato[0].length != 0 &&
                    parseInt(this.listaContrato[0].Status) != 4
                ) {
                    return true;
                }

                return false;
            },
            isNotDeferido() {
                if (!perfilUsuario.includes('Fiscal') &&
                    Array.isArray(this.listaContrato) &&
                    this.listaContrato[0].length != 0 &&
                    parseInt(this.listaContrato[0].Status) != 4
                ) {
                    return true;
                }

                return false;
            },
        },
        methods: {
            async getContrato() {
                let params = {
                    table: 'contratos',
                    where: {
                        CodigoContrato: codigoContratoDecode,
                    }
                }
                this.listaContrato = await vmGlobal.getFromAPI(params, 'cgmab');
                vmCabecalhoContrato.listaContrato = this.listaContrato;
            },
            async getVerificaConfiguracaoInicialContrato() {

                this.VerificaEnvioFiscal = await vmGlobal.getFromController('Contratos/getVerificaConfiguracaoInicialContrato', this.dadosVerificarEnvio);

            },
            async getAdtivos() {
                var params = {
                    table: 'aditivos',
                    where: {
                        CodigoContrato: codigoContratoDecode,
                    }
                }

                this.listaAditivos = await vmGlobal.getFromAPI(params, 'cgmab')

            },
            async getApostilamentos() {
                var params = {
                    table: 'apostilamentos',
                    where: {
                        CodigoContrato: codigoContratoDecode,
                    }
                }

                this.listaApostilamentos = await vmGlobal.getFromAPI(params, 'cgmab')

            },
            async bloquearContrato() {
                var params = {
                    table: 'contratos',
                    data: {
                        Status: 2,
                        DataEnvioValidacao: moment().format('YYYY-MM-DD HH:mm:ss')
                    },
                    where: {
                        CodigoContrato: codigoContratoDecode,
                    }
                }

                await vmGlobal.updateFromAPI(params, null, 'cgmab');
                window.location.href = base_url + 'Contratos/listar'

            },
            formatReal(numero) {
                var numero = numero.split('.');
                numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                return numero.join(',');
            }
        }

    });



    var vmControle = new Vue({
        el: '#myTab',
        data: {
            refazer: true,
            mostrarDadosContrato: true,
            mostrarCaracterizacao: true,
            mostrarTabs: true,
        },
        methods: {
            async iniciar() {
                await vmEscopoServicos.getEscoposServicos();
                this.refazer = false;
            },
            async iniciaRecursosHumanos() {
                await vmRecursosHumanos.getRecursosHumanosServico();
            },
            async iniciarCaracterizacao() {
                await vmCaracterizacao.getAreasApoio();
            },
            async iniciarParecer() {
                // await vmParecerFiscal.getSolicitacaoParecer();
                // await vmParecerFiscal.iniciarEditores();
            },
        }
    });

    var vmCabecalhoContrato = new Vue({
        el: '#cabecalhoContrato',
        data: {
            mostrarMesAnoSelecionado: false,
            mostrarDados: false,
            mostrarBtnEnvioFiscal: false,
            listaContrato: {},
            mesSelecionado: new Date().getMonth() + 1,
            anoSelecionado: new Date().getFullYear(),
            mesesAno: [],
            mostrarTelaVertical: false
        },
        watch: {
            listaContrato: function(val) {
                if (val[0].Status === '4' || val[0].Status === 4) {
                    this.mostrarMesAnoSelecionado = true;
                }
            }
        },
        methods: {
            confirmarEnvioFiscal() {
                vmEscopoServicos.confirmarEnvioFiscal();
            },
            async inicializacaoFiscal() {
                this.mostrarTelaVertical = !this.mostrarTelaVertical;

                if (this.mostrarTelaVertical === false) {
                    vmControle.mostrarTabs = true;
                    $("#home").addClass("tab-pane fade show active");
                    $("#profile").addClass("tab-pane fade");
                    $("#caracterizacao").addClass("tab-pane fade");
                    $("#rh").addClass("tab-pane fade");
                    $("#infraestrutura").addClass("tab-pane fade");
                    $("#parecer").addClass("tab-pane fade");
                } else {
                    vmControle.mostrarTabs = false;
                    $("page-loader-wrapper").addClass('visible');
                    await vmControle.iniciar();
                    await vmControle.iniciaRecursosHumanos();
                    await vmControle.iniciarCaracterizacao();
                    await vmControle.iniciarParecer();
                    $("page-loader-wrapper").removeClass('visible');

                    $("#home").removeClass("tab-pane fade show active");
                    $("#profile").removeClass("tab-pane fade show active");
                    $("#caracterizacao").removeClass("tab-pane fade show active");
                    $("#rh").removeClass("tab-pane fade show active");
                    $("#infraestrutura").removeClass("tab-pane fade show active");
                    $("#parecer").removeClass("tab-pane fade show active");
                }

            },

        }
    });
</script>