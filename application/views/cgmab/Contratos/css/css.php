<style>
    .lista-contratos {
        width: 350px;
        position: absolute;
        right: 14px;
        z-index: 5;
        background-color: #FFF;
    }

    .lista-contratos>ul {
        height: 200px;
        overflow: auto;
    }

    .bg-1 {
        background-color: #e9ecef
    }

    .modal-backdrop.show {
        opacity: 0.9;
        background-color: #d9d9d9;
    }

    .listaConstrutoras {
        position: fixed;
        width: 50%;
        top: 20%;
        left: 25%;
        height: 40%;
        background-color: #fff;
        overflow-y: auto;
    }
</style>