<style>
.no-shadow {
    box-shadow: 0px 0px 0px transparent !important;
}

.modalImportacao {
    position: absolute;
    background-color: #fff
}
.theme-dark .modalImportacao{
    background-color: #3d3d3d !important;
}
.modal-abs {
    width: 90%;
    height: 251px;
    overflow-x: auto;
    position: absolute;
    top: 42px;
    background-color: #ededed;
}
</style>