<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }

    .input-group-text {
        background-color: #afafaf;
        color: #f0f0f0;
    }

    .form-control {
        background-color: #dedede;
    }

    .h-150 {
        height: 100px !important;
    }

    .fs-13 {
        font-size: 13px;
    }

    .bg-1 {
        background-color: #fff
    }

    .table td,
    .table th {
        border-top: 1px solid #ffffff;
    }

    .form-add-servicos {
        position: fixed;
        top: 20%;
        margin: auto;
        z-index: 999;
        padding: 20px;
        background-color: #fff;
        height: 300px;
        overflow: auto;
        width: 60%;
    }

    .recursos {
        position: fixed;
        top: 72px;
        margin: auto;
        z-index: 999;
        padding: 20px;
        background-color: #fff;
        height: 91%;
        overflow: auto;
        width: 98.5%;
    }

    .form-gestao-condicionantes {
        position: fixed;
        top: 64px;
        z-index: 999;
        padding: 20px;
        background-color: #fff;
        height: 91%;
        overflow: auto;
        width: 98%;
        left: 1%;
    }

    .theme-dark .form-gestao-condicionantes,
    .theme-dark .desc-programas,
    .theme-dark .nova-meta,
    .theme-dark .listaBuscaCPF,
    .theme-dark .form-add-servicos,
    .theme-dark .nova-meta {
        background-color: #292929 !important;
    }

    .right-link {
        position: absolute;
        right: 7px;
        top: 18%;
    }

    .s-all {
        position: absolute;
        top: 0px;
        right: 16px;

    }

    .lista-licencas {
        position: absolute;
        top: 31px;
        z-index: 1;
        background-color: #fff;
    }

    .list-group-item.active {
        background-color: #97a6b7;
        border-color: #97a6b7;
    }



    .listaBuscaCPF {
        position: fixed;
        top: 20%;
        z-index: 1;
        width: 98%;
        left: 1%;
        background: #fff;
    }

    .desc-programas {
        position: fixed;
        width: 98%;
        top: 5%;
        background-color: #fff;
        z-index: 1;
        left: 1%;
        height: 90%;
        overflow-y: auto;
    }

    div.desc-programas>button {
        right: 3%;
        position: absolute;
    }

    .row>div>button {
        position: relative;
    }

    .cp-form>.input-group {
        margin-top: 21px;
    }

    .nova-meta {
        position: fixed;
        z-index: 2;
        padding: 25px;
        background: #fff;
        width: 85%;
        top: 20%;
        right: 10%;
    }

    .marcador {
        background-color: #0c7ce6 !important;
        color: white;
    }

    .cke_chrome {
        width: 100% !important;
    }
</style>