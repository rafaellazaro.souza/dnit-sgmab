<?php $this->load->view('cgmab/Contratos/css/cssLista');
?>
<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }
</style>
<div class="body_scroll" id="Listagem">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Meus Contratos</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item">Contratos</li>
                    <li class="breadcrumb-item active">Meus Contratos</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <!-- <h2><strong>Basic</strong> Examples </h2> -->

                    </div>
                    <div class="body table-responsive">
                        <table class="table table-striped compact" id="tblListagem">
                            <thead>
                                <tr>

                                    <th>Código</th>
                                    <th>Numero</th>
                                    <th>Empresa</th>
                                    <th>CNPJ</th>
                                    <th>Município</th>
                                    <th>Edital</th>
                                    <th>Lote</th>
                                    <th>Status inicial</th>
                                    <th>Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="i in listaDados">

                                    <td>{{i.CodigoContrato}}</td>
                                    <td>{{i.Numero}}</td>
                                    <td>{{i.NomeEmpresa}}</td>
                                    <td>{{i.CnpjEmpresa}}</td>
                                    <td>{{i.Municipio}}</td>
                                    <td>{{i.Edital}}</td>
                                    <td>{{i.Lote}}</td>
                                    <td>
                                        <template v-if="i.Status === '1' || i.Status === 1">
                                            <button class="btn btn-danger w-100">Pendente Contratada</button>
                                        </template>
                                        <template v-if="i.Status === '2' || i.Status === 2">
                                            <button class="btn btn-warning w-100">Pendente Fiscal DNIT</button>
                                        </template>
                                        <template v-if="i.Status == parseInt(4)">
                                            <button class="btn btn-success w-100 pt-2 pb-2">Aprovado Fiscal</button>
                                        </template>
                                        <template v-if="i.Status === '5' || i.Status === 5">
                                            <button v-if="!contratoAtrazado(i.PrazoCorrecao)" class="btn btn-rejeitado w-100 pt-2 pb-2">Parcialmente Aprovado</button>
                                            <button v-else class="btn btn-danger w-100 pt-2 pb-2 text-white">Prazo de Correção Expirado - Contate o Fiscal</button>
                                            <span>Prazo: {{formataData(i.PrazoCorrecao)}}</span>
                                        </template>
                                    </td>
                                    <td>
                                        <template v-if="(i.Status === '1' || i.Status === 1) || (i.Status === '5' || i.Status === 5)">
                                            <a v-if="!contratoAtrazado(i.PrazoCorrecao)" :href="base_url+'contratos/dados/' + vmGlobal.codificarDecodificarParametroUrl(i.CodigoContrato, 'encode')" class="btn btn-sm btn-outline-secondary w-100"><i class="far fa-gears"></i> Complementar cadastro</a>
                                        </template>

                                        <template v-if="i.Status === '4' || i.Status === 4">
                                            <button @click="contratoSelecionado = i" data-toggle="modal" data-target="#modal-contrato-selecionado" class="btn btn-sm btn-outline-secondary w-100 pt-2 pb-2"><i class="far fa-gears"></i>Selecionar</button>
                                        </template>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <!-- modal menus do contrato  -->
                        <div class="modal fade modal-menu-contrado" tabindex="-1" role="dialog" id="modal-contrato-selecionado" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-xl" role="document">

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <!-- <h4>0000508/2011 - CONSÓRCIO AMBIENTAL BR 230 /422/PA - STE PROGAIA E ASTEC</h4> -->
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body mb-5 mt-5">
                                        <div class="row">
                                            <div class="col-12 col-sm-4" id="servico">
                                                <h5>1) Selecione uma Opção</h5>
                                                <!-- Menu para execução de programas  -->
                                                <div class="list-group">

                                                    <a :href="base_url+'contratos/dados/' + vmGlobal.codificarDecodificarParametroUrl(contratoSelecionado.CodigoContrato, 'encode')" class="list-group-item list-group-item-action">Configuração Administrativa do Contrato</a>
                                                    <a @click="servicoSelecionado = i.CodigoEscopo; codigoServicoSelecionado = parseInt(i.CodigoServico)" v-for="i in listaServicosContrato" href="javascript:;" class="list-group-item list-group-item-action" v-text="i.NomeServico"></a>

                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-4 border-right border-left">

                                                <div class="list-group" v-show="mostrarCondicionantes">
                                                    <h5>2) Selecione um Programa</h5>
                                                    <a v-for="i in listaCondicionantes" @click="dadosEscopoPrograma = i; mostrarAcao = true" href="javascript:;" class="list-group-item list-group-item-action">
                                                        {{i.Titulo}} <br />
                                                        {{i.NomeTipo}} - {{i.NomeSubtipo}} <br />
                                                        Prazo: {{i.Prazo}}<br />
                                                        Recorrência: {{i.Recorrencia}}<br />
                                                    </a>

                                                </div>
                                                <!-- Menu para supervisão ambiental  -->
                                                <div class="list-group" v-show="mostrarMenuSupervisao">
                                                    <h5>2) Selecione um Módulo</h5>
                                                    <a :href="base_url + 'DiarioAmbiental/diarioAmbientalLista/'+vmGlobal.codificarDecodificarParametroUrl(servicoSelecionado, 'encode')" class="list-group-item">Diário Ambiental</a>
                                                    <a href="<?= base_url('HistoricoValidacao'); ?>" class="list-group-item">Controle de Ocorrências</a>
                                                    <a href="#" class="list-group-item">Programas PAC</a>
                                                    <a href="<?= base_url('RelatoriosSupervisao'); ?>" class="list-group-item">Relatórios de Supervisão</a>
                                                </div>

                                            </div>
                                            <div class="col-12 col-sm-4" v-if="mostrarAcao">
                                                <h5>3) Selecione uma ação</h5>
                                                <div class="list-group">
                                                    <a href="javascript:;" @click="mostrarDadosPrograma = true" class="list-group-item list-group-item-action">Dados do Programa</a>
                                                    <!-- ***************************************************
                                                    modal dados do programa 
                                                    *************************************************** -->
                                                    <div class="modal-w98-h50 sombra-2 p-5 br-10" v-show="mostrarDadosPrograma">

                                                        <div class="col-12 text-right">
                                                            <button class="btn btn-sm" @click="mostrarDadosPrograma = false"><i class="fa fa-times"></i></button>
                                                        </div>
                                                        <div class="row  cp-form">
                                                            <h4 class="w-100 mb-4">Descrição dos Programas, Planos Ambientais, Projetos e Condicionantes</h4>

                                                            <div class="input-group col-12 col-sm-6">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">Descrição</span>
                                                                </div>
                                                                <textarea name="dadosPrograma1" id="DescricaoPrograma" class="form-control"></textarea>
                                                            </div>
                                                            <div class="input-group col-12 col-sm-6">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">Justificativa</span>
                                                                </div>
                                                                <textarea name="dadosPrograma2" id="Justificativa" class="form-control"></textarea>
                                                            </div>
                                                            <div class="input-group col-12 col-sm-6 mt-3 mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">Objetivos</span>
                                                                </div>
                                                                <textarea name="dadosPrograma3" id="Objetivo" class="form-control"></textarea>
                                                            </div>
                                                            <div class="input-group col-12 col-sm-6 mt-3 mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">Metodologia </span>
                                                                </div>
                                                                <textarea name="dadosPrograma4" id="Metodologia" class="form-control"></textarea>
                                                            </div>
                                                            <div class="input-group col-12 col-sm-6">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">Publico Alvo </span>
                                                                </div>
                                                                <textarea name="dadosPrograma5" id="PublicoAlvo" class="form-control"></textarea>
                                                            </div>
                                                            <div class="input-group col-12 col-sm-6">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">Atividades Previstas </span>
                                                                </div>
                                                                <textarea name="dadosPrograma6" id="AtividadesPrevistas" class="form-control"></textarea>
                                                            </div>

                                                            <div class="input-group col-12 mt-4">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">Responsável Pelo Programa</span>
                                                                </div>
                                                                <div class="form-control" v-text="dadosDescricao.NomePessoa"></div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- ***************************************************
                                                    modal dados do programa 
                                                    *************************************************** -->
                                                    <!-- configurações tecnicas  -->
                                                    <!-- Configuração fauna  -->
                                                    <a target="_blank" :href="'<?= base_url('ConfiguracaoProgramas/recursosFaunaModulos/?99cfb275a87d383fa2912331e3117e48=') ?>'+ vmGlobal.codificarDecodificarParametroUrl(dadosEscopoPrograma.CodigoEscopoPrograma, 'encode')" class="list-group-item list-group-item-action  <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'invisivel' : '' ?>" v-if="dadosEscopoPrograma.CodigoSubtipo == 3">
                                                        Configurações Técnicas do Programa<br />
                                                        <span class="badge" :class="getClassStatus">{{getStatus}}</span>
                                                    </a>
                                                    <!-- recursos hidricos  -->

                                                    <a target="_blank" :href="'<?= base_url('ConfiguracaoProgramas/recursosHidricos/?99cfb275a87d383fa2912331e3117e48=') ?>'+ vmGlobal.codificarDecodificarParametroUrl(dadosEscopoPrograma.CodigoEscopoPrograma, 'encode')" class="list-group-item list-group-item-action  <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'invisivel' : '' ?>" v-if="dadosEscopoPrograma.CodigoTema === 3 || dadosEscopoPrograma.CodigoTema === '3'">
                                                        Configurações Técnicas do Programa<br />
                                                        <span class="badge" :class="getClassStatus">{{getStatus}}</span>
                                                    </a>
                                                    <!-- Passagem fauna  -->
                                                    <a target="_blank" :href="'<?= base_url('ConfiguracaoProgramas/passagemFauna/?99cfb275a87d383fa2912331e3117e48=') ?>'+ vmGlobal.codificarDecodificarParametroUrl(dadosEscopoPrograma.CodigoEscopoPrograma, 'encode')+'&99cfb275a87d383fa2912331e3117e48=' +vmGlobal.codificarDecodificarParametroUrl(contratoSelecionado.CodigoContrato, 'encode')" class="list-group-item list-group-item-action  <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'invisivel' : '' ?>" v-if="dadosEscopoPrograma.CodigoSubtipo == 4">
                                                        Configurações Técnicas do Programa<br />
                                                        <span class="badge" :class="getClassStatus">{{getStatus}}</span>
                                                    </a>
                                                    <!-- Supressão de Vegetação  -->
                                                    <a target="_blank" :href="'<?= base_url('ConfiguracaoSupressaoVegetacao/supressaoVegetacao/?CodigoEscopoPrograma=') ?>'+ vmGlobal.codificarDecodificarParametroUrl(dadosEscopoPrograma.CodigoEscopoPrograma, 'encode')+'&CodigoContrato=' +vmGlobal.codificarDecodificarParametroUrl(contratoSelecionado.CodigoContrato, 'encode')" class="list-group-item list-group-item-action  <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'invisivel' : '' ?>" v-if="dadosEscopoPrograma.CodigoSubtipo == 2">
                                                        Configurações Técnicas do Programa<br />
                                                        <span class="badge" :class="getClassStatus">{{getStatus}}</span>
                                                    </a>
                                                    <!-- configurações tecnicas  -->
                                                    <!-- <a :href="base_url+'atividades/listaAtividadesMes/?99cfb275a87d383fa2912331e3117e48='+vmGlobal.codificarDecodificarParametroUrl(dadosEscopoPrograma.CodigoEscopoPrograma, 'encode')" class="list-group-item list-group-item-action">Relatório de Atividades</a> -->

                                                    <a href="javascript:void(0);" v-if="viewRelatorio" @click="iniciarRelatoriosMensais(dadosEscopoPrograma.CodigoEscopoPrograma, contratoSelecionado.Numero)" class="list-group-item list-group-item-action">Relatórios de Atividades</a>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal menus do contrato  -->

                        <!-- modal caso usuario administrador selecionar a contratada para preencher a proprieade this.cnpjContratada -->
                        <div class="modal-w98-h50 sombra-2 p-5" v-show="selecionarContratada">
                            <div class="row">
                                <div class="col-12 text-center mt-5 alert-warning">
                                    <h4>OPA!</h4>
                                    <h4><small>Antes de continuar, Selecione uma Contratada</small></h4>
                                </div>
                                <div class="col-12 col-sm-12 h450-r mt-5">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <td>codigo</td>
                                                <td>CNPJ</td>
                                                <td>Razão Social</td>
                                                <td><i class="fa fa-check"></i></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="i in listaCadastroContratadas">
                                                <td v-text="i.CodigoContratada"></td>
                                                <td v-text="i.CnpjContratada"></td>
                                                <td v-text="i.NomeContratada"></td>
                                                <td>
                                                    <buton @click="cnpjContratada = i.CnpjContratada; codigoContratada = i.CodigoContratada" class="btn btn-sm"><i class="fa fa-check"></i> Selecionar</buton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <!-- modal caso usuario administrador selecionar a contratada para preencher a proprieade this.cnpjContratada -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<?php $this->load->view('cgmab/Contratos/scripts/appLista') ?>