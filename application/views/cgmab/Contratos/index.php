<?php $this->load->view('cgmab/Contratos/css/css') ?>
<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Contratos</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url('home'); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item">Contratos</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">

                    </div>
                    <div class="body">
                        <div class="row p-2" id="addContratos">
                            <div class="col-12 col-sm-6 mb-4">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            Buscar Contrato no SIAC - Digite o Número ou Data
                                        </div>
                                    </div>
                                    <input class="form-control" v-model="numeroDigitado">
                                    <button @click="setExibir('resultados-busca-contratos'), getContratos()" v-if="numeroDigitado.length >= 4" class="btn btn-outline-secondary mr-2">Buscar</button>
                                </div>
                                <div class="lista-contratos sombra-1" v-show.parent="exibir === 'resultados-busca-contratos'">
                                    <ul class="list-group mb-4 ">

                                        <li class="list-group-item bg-1 ">Selecione o contrato  <i @click="exibir = '-'" class="fa fa-times"></i></li>
                                        <button @click="exibir = '-', 
                                salvarContrato(i ,3)" v-for="i in resultadoBusca" type="button" class="list-group-item list-group-item-action">{{i.NU_CON_FORMATADO}} </button>
                                    </ul>
                                </div>

                            </div>

                        </div>

                        <div class="row p-4" id="ListagemCotratos">
                            <button @click="mostrarCadastro = true" class="btn btn-success mt-3 mb-4">Adicionar Convênio ou TED</button>
                            <!-- *******************************************
                            Formulario para adicionar um contrato manualmente  
                            ******************************************* -->
                            <?php $this->load->view('cgmab/Contratos/paginas/adicionarContrato') ?>
                            <!-- Formulario para adicionar um contrato manualmente  -->
                            <table class="table table-striped" id="tblListaContratos">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Numero</th>
                                        <th>Empresa</th>
                                        <th>CNPJ</th>
                                        <th>Modal</th>
                                        <th>Município</th>
                                        <th>Edital</th>
                                        <th>Lote</th>
                                        <th>Env. Contratada</th>
                                        <th>Conf. Zero</th>
                                        <th>Detalhes</th>
                                        <th>Deletar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="i in listaContratos">
                                        <td>{{i.CodigoContrato}}</td>
                                        <td>{{i.Numero}}</td>
                                        <td>{{i.NomeEmpresa}}</td>
                                        <td>{{i.CnpjEmpresa}}</td>
                                        <td>{{i.Modal}}</td>
                                        <td>{{i.Municipio}}</td>
                                        <td>{{i.Edital}}</td>
                                        <td>{{i.Lote}}</td>
                                        <!-- se dados basicos foi preenchido  -->

                                        <td v-if="i.Status === 3 || i.Status === '3'"><button class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button></td>

                                        <td v-else><button class="btn btn-success btn-sm"><i class="fa fa-check"></i></button>
                                        </td>
                                        <td>
                                            <template v-if="i.Status === 4 || i.Status === '4'">
                                                <button class="btn btn-success btn-sm"><i class="fa fa-check"></i></button>
                                            </template>
                                            <template v-else>
                                                <button class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                            </template>

                                        </td>
                                        <td>
                                            <button @click="vmConfigurarContrato.setDadosContrato(i)" data-toggle="modal" data-target="#dadosBasicos" class="btn btn-outline-secondary btn-sm"><i class="fa fa-list"></i> Configurações</button>
                                            
                                        </td>
                                        <!-- se contrato não foi enviado para contratada pode deletar  -->
                                        <td>
                                            <button v-if="i.Status == 3" @click="deletarContrato(i.CodigoContrato)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Deletar</button>

                                            <button v-else @click="alert('Ação Bloqueada! Contrato ja foi enviado para contratada')" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Deletar</button>
                                        
                                        </td>
                                    
                                        <!-- fim se dados basicos foi preenchido  -->


                                    </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php $this->load->view('cgmab/Contratos/paginas/modalDadosBasicos') ?>
    <?php $this->load->view('cgmab/Contratos/scripts/app') ?>