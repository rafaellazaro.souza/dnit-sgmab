<?php $this->load->view('cgmab/Contratos/css/cssDados'); ?>

<div class="body_scroll" id="vmDadosDoContrato">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Serviços</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item">Contratos</li>
                    <li class="breadcrumb-item active">Dados do Contrato</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>


    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row mb-2" id="cabecalhoContrato">
                            <!-- filtro por ano/mes -->
                            <!-- <div class="col-12 col-sm-3">
                                <button @click="inicializacaoFiscal()" class="btn btn-secondary mt-3 <?= $this->session->userdata('PerfilLogin') != 'Fiscal' ? 'invisivel' : '' ?>"><i class="fas fa-desktop"></i></button>
                                <div class="input-group mb-3 mt-3" v-if="mostrarMesAnoSelecionado">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Ano Selecionado</span>
                                    </div>
                                    <div class="form-control lh-40">
                                        <select class="form-control show-tick ms select2" v-model="anoSelecionado">
                                            <?php
                                            for ($i = date('Y'); $i >= 1980; $i--) {
                                                echo '<option  value="' . $i . '">' . $i . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-12 col-sm-3">
                                <div class="input-group mb-3 mt-3" v-if="mostrarMesAnoSelecionado">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Mês Selecionado</span>
                                    </div>
                                    <div class="form-control lh-40">
                                        <select class="form-control show-tick ms select2" v-model="mesSelecionado">
                                            <?php
                                            for ($i = 1; $i <= 12; $i++) {
                                                echo '<option  value="' . $i . '">' . $i . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div> -->
                            <!-- fim filtro por ano/mes -->
                            <div class="col-sm-6 text-right">
                                <div class="row mt-3">
                                    <div class="input-group mb-3 col-12 col-sm-6" v-for="i in listaContrato">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Número</span>
                                        </div>
                                        <div class="form-control lh-40">{{i.Numero}}</div>
                                    </div>
                                    <div class="input-group mb-3 col-12 col-sm-6 " v-for="i in listaContrato">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Empresa</span>
                                        </div>
                                        <div class="form-control fs-13">{{i.NomeEmpresa}}</div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6 d-flex align-items-center flex-row-reverse">
                                <button @click="vmDadosContrato.bloquearContrato()" class="btn btn-sm btn-success" v-if="vmDadosContrato.isNotDeferido">Solicitar Aprovação</button>
                            </div>


                        </div>
                    </div>
                    <div class="body">

                        <ul v-show="mostrarTabs" class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'invisivel' : '' ?>">
                                <a class="nav-link <?= $this->session->userdata('PerfilLogin') != 'Fiscal' ? 'active' : '' ?>" id="mapa-tab" data-toggle="tab" href="#mapa" role="tab" aria-controls="home" aria-selected="true">Mapa</a>
                            </li>
                            <li class="nav-item" v-show="mostrarDadosContrato">
                                <a class="nav-link <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'active' : '' ?>" id="contrato-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dados do Contrato</a>
                            </li>
                            <li class="nav-item">
                                <a @click="iniciar()" v-if="refazer" class="nav-link" id="escopo-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Escopo dos Serviços Contratados</a>
                                <a v-else class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Escopo dos Serviços Contratados</a>
                            </li>
                            <li class="nav-item" v-show="mostrarCaracterizacao">
                                <a @click="iniciarCaracterizacao()" class="nav-link" id="caracterizacao-tab" data-toggle="tab" href="#caracterizacao" role="tab" aria-controls="home" aria-selected="true">Caracterização do Empreendimento</a>
                            </li>
                            <li class="nav-item" v-show="!mostrarDadosContrato">
                                <a class="nav-link" id="quadroAtividades-tab" data-toggle="tab" href="#quadroAtividades" role="tab" aria-controls="home" aria-selected="true">Quadro de Atividades/ Relatório Técnico</a>
                            </li>
                            <li class="nav-item" v-show="!mostrarDadosContrato">
                                <a class="nav-link" id="relatorio-fotografico-tab" data-toggle="tab" href="#relatoriFotografico" role="tab" aria-controls="home" aria-selected="true">Relatório Fotográfico</a>
                            </li>
                            <li class="nav-item">
                                <a @click="iniciaRecursosHumanos();iniciar()" class="nav-link" id="rh-tab" data-toggle="tab" href="#rh" role="tab" aria-controls="home" aria-selected="true">Vinculação de RH</a>
                            </li>
                            <li class="nav-item">
                                <a @click="iniciar();" class="nav-link" id="infraestrutura-tab" data-toggle="tab" href="#infraestrutura" role="tab" aria-controls="home" aria-selected="true">Vinculação de Infraestrutura </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="parecerFiscal-tab" data-toggle="tab" href="#parecer" role="tab" aria-controls="home" aria-selected="true">Parecer do Fiscal</a>
                            </li>

                        </ul>

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade <?= $this->session->userdata('PerfilLogin') != 'Fiscal' ? 'show active' : '' ?>" id="mapa" role="tabpanel" aria-labelledby="mapa">
                                <?php $this->load->view('cgmab/Contratos/paginas/mapa.php') ?>
                            </div>
                            <div class="tab-pane fade <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'show active' : '' ?>" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <?php $this->load->view('cgmab/Contratos/paginas/dadosContrato.php') ?>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <?php $this->load->view('cgmab/Contratos/paginas/escopoServicos.php') ?>
                            </div>
                            <div class="tab-pane fade" id="caracterizacao" role="tabpanel" aria-labelledby="caracterizacao-tab">
                                <?php $this->load->view('cgmab/Contratos/paginas/caracterizacaoEmpreendimento.php') ?>
                            </div>
                            <div class="tab-pane fade" id="quadroAtividades" role="tabpanel" aria-labelledby="caracterizacao-tab">
                                <?php $this->load->view('cgmab/Condicionantes/paginas/quadroAtividades.php') ?>
                            </div>
                            <div class="tab-pane fade" id="relatoriFotografico" role="tabpanel" aria-labelledby="caracterizacao-tab">
                                <?php $this->load->view('cgmab/Condicionantes/paginas/relatorioFotografico.php') ?>
                            </div>
                            <div class="tab-pane fade" id="rh" role="tabpanel" aria-labelledby="profile-tab">
                                <?php $this->load->view('cgmab/Contratos/paginas/recursosHumanos.php') ?>
                            </div>
                            <div class="tab-pane fade" id="infraestrutura" role="tabpanel" aria-labelledby="profile-tab">
                                <?php $this->load->view('cgmab/Contratos/paginas/infraestrutura.php') ?> 
                            </div>
                            <div class="tab-pane fade" id="parecer" role="tabpanel" aria-labelledby="profile-tab">
                                <?php $this->load->view('cgmab/ParecerFiscal/parecerInicial.php') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('cgmab/Contratos/scripts/appDados') ?>
<?php $this->load->view('cgmab/Contratos/scripts/appEscopo') ?>
<?php $this->load->view('cgmab/Contratos/scripts/appCondicionantes') ?>
<?php $this->load->view('cgmab/Contratos/scripts/appRecursosHumanos') ?>
<?php $this->load->view('cgmab/Contratos/scripts/appInfraestrutura') ?>
<?php $this->load->view('cgmab/Contratos/scripts/appCaracterizacao') ?>
<?php $this->load->view('cgmab/Contratos/scripts/appCronogramaFisicoProgramas') ?>

<?php $this->load->view('cgmab/Contratos/scripts/appMapa') ?>
<?php $this->load->view('cgmab/Condicionantes/scripts/appQuadroAtividades') ?>
<?php $this->load->view('cgmab/Condicionantes/scripts/appRelatorioFotografico') ?>
<?php $this->load->view('cgmab/Contratos/scripts/appDescricaoProgramas') ?>
<?php $this->load->view('cgmab/ParecerFiscal/scripts/appParecerFiscal.php') ?>