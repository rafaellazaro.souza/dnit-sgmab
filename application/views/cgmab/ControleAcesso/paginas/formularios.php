<div class="modal-w98-h50 sombra-2 p-5" v-show="mostrarFormulario">
    <button class="btn btn-secondary btn-r" @click="mostrarFormulario = false"><i class="fa fa-times"></i></button>
    <h3 class="pb-5 pt-4">Formulário Gestão de Áreas</h3>

    <div class="row pb-5 mt-5 border-bottom">
        <div class="input-group col-12 col-sm-6 ml-0 mb-3">
            <div class="input-group-prepend">
                <span id="basic-addon1" class="input-group-text">Nome da Área</span>
            </div>
            <input type="text" v-model="dadosArea.NomeArea" class="form-control"/>
        </div>
        <div class="input-group col-12 col-sm-6 ml-0 mb-3">
            <div class="input-group-prepend">
                <span id="basic-addon1" class="input-group-text">Ícone</span>
            </div>
            <input type="text" v-model="dadosArea.Icone" class="form-control"/>
        </div>
        <div class="input-group col-12 col-sm-6 ml-0 mb-3">
            <div class="input-group-prepend">
                <span id="basic-addon1" class="input-group-text">URL</span>
            </div>
            <input type="text" v-model="dadosArea.Url" class="form-control"/>
        </div>
        <!-- <div class="input-group col-12 col-sm-6 ml-0 mb-3">
            <div class="input-group-prepend">
                <span id="basic-addon1" class="input-group-text">Menu Pai</span>
            </div>
            <select v-model="dadosArea.CodigoSubAreaSistema" class="form-control show-tick ms select2">
                <option :value="i.CodigoAreaSistema " v-for="i in listaAreas" v-text="i.NomeArea"></option>
            </select>
        </div> -->
    </div>

    <div class="w-100 mt-3 text-right">
        <button @click="mostrarFormulario = false; dadosArea = {}" class="btn btn-outline-secondary">Cancelar</button>
        <button @click="salvarArea();" class="btn btn-secondary">Salvar</button>
    </div>
</div>