<script>
    var vmControleAcesso = new Vue({
        el: '#areasSistema',
        data: {
            listaAreas: [],
            mostrarFormulario: false,
            dadosArea: {}
        },
        async mounted() {
            await this.getAreas();
        },
        methods: {
            async getAreas() {
                $("#tblAreasSistema").DataTable().destroy();
                let params = {
                    table: 'areasSistema',
                }
                this.listaAreas = await vmGlobal.getFromAPI(params, 'cgmab');
               vmGlobal.montaDatatable("#tblAreasSistema", true);

            },
            async salvarArea() {

                if (this.dadosArea.CodigoAreaSistema) {
                    let params = {
                        table: 'areasSistema',
                        data: this.dadosArea,
                        where: {
                            CodigoAreaSistema: this.dadosArea.CodigoAreaSistema
                        }
                    }
                   delete(this.dadosArea.CodigoAreaSistema);
                    await vmGlobal.updateFromAPI(params, null, 'cgmab');
                } else {
                    let params = {
                        table: 'areasSistema',
                        data: this.dadosArea
                    }
                    await vmGlobal.insertFromAPI(params, null, 'cgmab');
                }

                this.mostrarFormulario = false;
                this.dadosArea = {};
                this.getAreas();

            },
            editar(codigo, dados, status) {
                this.dadosArea.CodigoAreaSistema = codigo;
                this.dadosArea = dados;
                if(status === 'inativo'){
                    this.dadosArea.Status = '0';
                    this.salvarArea();
                }else if(status === 'ativar'){
                    this.dadosArea.Status = 1;
                    this.salvarArea(); 
                }
            }

        }
    });
</script>