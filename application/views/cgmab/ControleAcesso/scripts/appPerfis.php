<script>
    var vmControleAcesso = new Vue({
        el: '#perfis',
        data: {
            listaPerfis: [],
            dadosPerfil: {},
            listaPerfisAreas: [],
            listaPermissoesCadastradas: [],
            listaCodigosPerfisAreas: [],
            listaAreas: [],
            CodigoPerfilSelecionado: 0,
            permissaoSelecionada: {},
            mostrarTablePerfis: true,
            mostrarBloqueioAreas: true,
            mostrarBloqueioPermissoes: true,
            mostrarNomePerfilSelecionado: false,
            mostrarformAdd: false,
            NomePerfilSelecionado: '',
            nomePerfil: '',
            cssNomePerfil: 'bloqueio text-center',
            cssAreas: 'bloqueio text-center',

        },
        watch: {
            CodigoPerfilSelecionado: function(val) {
                if (val === 0) {
                    this.cssNomePerfil = 'bloqueio text-center animated bounceOut';
                    setTimeout(() => {
                        this.cssAreas = 'bloqueio text-center animated bounceIn';
                        this.mostrarBloqueioAreas = true;
                        this.mostrarNomePerfilSelecionado = false;
                    }, 1000);
                } else {
                    this.cssAreas = 'bloqueio text-center animated bounceOut';
                    setTimeout(() => {
                        this.cssNomePerfil = 'bloqueio text-center animated bounceIn';
                        this.mostrarBloqueioAreas = false;
                        this.mostrarNomePerfilSelecionado = true;
                    }, 1000);
                }

            },
            listaPerfisAreas: function(val) {
                $('#listaAreasSelecionadas').removeClass('animated shake');
                setTimeout(() => {
                    $('#listaAreasSelecionadas').addClass('animated shake');
                }, 500);

            }
        },
        async mounted() {
            await this.getPerfis();
            await this.getAreas();
            await this.listarPermissoes();
        },
        methods: {
            async listarPermissoes() {
                $('#permissoesCadastradas').DataTable().destroy();
                let params = {
                    table: 'controleAcesso',
                    joinLeft: [{
                            table1: 'controleAcesso',
                            table2: 'areasSistema',
                            on: 'CodigoAreaSistema'
                        },
                        {
                            table1: 'controleAcesso',
                            table2: 'perfisSistema',
                            on: 'CodigoPerfil'
                        }
                    ]
                }
                this.listaPermissoesCadastradas = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#permissoesCadastradas', true);
            },
            async getPerfis() {
                $("#tblPerfis").DataTable().destroy();
                let params = {
                    table: 'perfisSistema',
                    where: {
                        StatusPerfil: 1
                    }
                }
                this.listaPerfis = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable("#tblPerfis", true);

            },
            async getAreas() {
                $("#tblAreasSistema").DataTable().destroy();
                let params = {
                    table: 'areasSistema'
                }
                this.listaAreas = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable("#tblAreasSistema", true);

            },
            async setAreas(codigoArea, nomeArea, indice, permissao) {
                // remove elemento da lista de areas pra não haver duplicidade
                this.listaAreas.splice(indice, 1);
                //cria lista para mostrar
                this.listaPerfisAreas.push({
                    nomeArea: nomeArea,
                    Permissao: permissao
                });
                //cria lista para o insert
                this.listaCodigosPerfisAreas.push({
                    CodigoPerfil: this.CodigoPerfilSelecionado,
                    CodigoAreaSistema: codigoArea,
                    Permissao: permissao
                });

            },
            editar(codigo, dados, status) {
                this.dadosPerfil.CodigoPerfil = codigo;
                this.dadosPerfil = dados;
                if (status === 'inativo') {
                    this.dadosPerfil.StatusPerfil = '0';
                    this.salvarArea();
                } else if (status === 'ativar') {
                    this.dadosPerfil.StatusPerfil = 1;
                    this.salvarArea();
                }
            },
            async salvarPermissoes() {
                let formData = new FormData();
                formData.append('Dados', JSON.stringify(this.listaCodigosPerfisAreas));
                await axios.post(base_url + 'ControleAcesso/salvarPermissoes', formData)
                    .then((resp) => {
                        if (resp.data.status === true) {
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Salvo!',
                                text: 'Dados inseridos com sucesso.',
                                showConfirmButton: true,
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: 'Erro!',
                                text: "Erro ao tentar salvar arquivo.",
                                showConfirmButton: true,

                            });
                        }
                    })
                    .finally(() => {
                        this.listaCodigosPerfisAreas = {};
                        this.listaPerfisAreas = {};
                        this.getAreas();
                        this.listarPermissoes();
                    })

            },
            async salvarNovoPefil() {
                let params = {
                    table: 'perfisSistema',
                    data: {
                        NomePerfil: this.nomePerfil
                    }
                }
                await vmGlobal.insertFromAPI(params, null, 'cgmab');
                this.mostrarformAdd = false;
                this.mostrarTablePerfis = true;
                this.getPerfis();
            },
            async deletarPermissao(codigo) {
                let params = {
                    table: 'controleAcesso',
                    where: {
                        CodigoControleAcessoPerfis: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                this.listarPermissoes();
            },
            async desativarPerfil(CodigoPerfil) {
                await vmGlobal.updateFromAPI({
                    table: 'perfisSistema',
                    data: {
                        StatusPerfil: 3
                    },
                    where: {
                        CodigoPerfil: CodigoPerfil
                    }
                }, null);
                this.getPerfis();
            }

        }
    });
</script>