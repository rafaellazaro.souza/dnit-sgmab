<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Perfis</h2>
                <ul class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Perfis</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid" id="perfis">
        <div class="row clearfix">
            <div class="col-lg-12">
                <!-- <button class="btn btn-success mb-3" @click="mostrarFormulario = true"><i class="fa fa-plus"></i> Adicionar Perfis</button> -->
                <div class="card">
                    <div class="body">
                        <div class="row">
                            <div class="col-12 col-md-6 col-xl-4 ">
                                <h5>Selecionar Perfil</h5>
                                <button @click="mostrarformAdd = true; mostrarTablePerfis = false" class="btn btn-sm mb-3"  v-show="mostrarformAdd === false"><i class="fa fa-plus"></i> Adicionar Perfil</button>
                                <div :class="cssNomePerfil" v-show="mostrarNomePerfilSelecionado">
                                    <i class="fas fa-check" data-wow-duration="1s" data-wow-offset="2" data-wow-iteration="2"></i>
                                    <h5 class="mt-4" v-text="NomePerfilSelecionado "> </h5>
                                    <button @click="CodigoPerfilSelecionado = 0" class="btn btn-sm btn-transparent"> <i class="fa fa-times"></i> Alterar</button>
                                </div>
                                <div class="h450-r">
                                    <div class=" row mt-5" v-show="mostrarformAdd">
                                        <div class="alert alert-info w-100 mb-4">
                                            <p class="ml-3"><i class="fas fa-exclamation-triangle"></i> Para adicionar um novo perfil basta digitar um nome abaixo!</p>
                                        </div>
                                        <div class="input-group col-8">
                                            <div class="input-group-prepend">
                                                <span id="basic-addon1" class="input-group-text">
                                                    Nome
                                                </span>
                                            </div>
                                            <input type="text" v-model="nomePerfil" class="form-control">
                                        </div>
                                        <div class="col-4 pt-2">
                                            <input class="chk" type="checkbox" value="1"> Interno
                                        </div>
                                        <div class="col-12 mt-5 border-top text-right">
                                            <button @click="mostrarTablePerfis = true; mostrarformAdd = false;" class="btn btn-outline-secondary">Cancelar</button>
                                            <button @click="salvarNovoPefil()" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                                        </div>
                                    </div>

                                    <table class="table table-striped" v-show="mostrarTablePerfis">
                                        <thead>
                                            <tr>
                                                <th>Selecionar</th>
                                                <th>Nome</th>
                                                <th>Tipo</th>
                                                <th><i class="fa fa-times"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="i in listaPerfis">
                                                <td>
                                                    <button @click="CodigoPerfilSelecionado = i.CodigoPerfil; NomePerfilSelecionado = i.NomePerfil"><i class="far fa-check-square"></i></button>
                                                </td>
                                                <td v-text="i.NomePerfil"></td>
                                                <td v-text="i.DNIT === 1 || i.DNIT === '1' ? 'Interno DNIT' : 'Externo Contratada'"></td>
                                                <td>
                                                    <button @click="desativarPerfil(i.CodigoPerfil)" title="desativar Perfils" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>


                                </div>

                            </div>

                            <div class="col-12 col-md-6 col-xl-4">
                                <h5>Selecionar Áreas</h5>
                                <div class="h450-r">
                                    <div :class="cssAreas" v-show="mostrarBloqueioAreas">
                                        <i class="fas fa-lock wow bounce" data-wow-duration="1s" data-wow-offset="2" data-wow-iteration="2"></i>
                                        <h5 class="mt-4">Selecione o perfil</h5>
                                    </div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Selecionar</th>
                                                <th>Nome</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(i, index) in listaAreas">
                                                <td>


                                                    <div class="dropdown">
                                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="far fa-check-square"></i>
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                            <button @click="setAreas(i.CodigoAreaSistema, i.NomeArea,index, 1)" class="dropdown-item" type="button">Leitura</button>
                                                            <button @click="setAreas(i.CodigoAreaSistema, i.NomeArea,index, 2)" class="dropdown-item" type="button">Escrita </button>
                                                            <button @click="setAreas(i.CodigoAreaSistema, i.NomeArea,index, 3)" class="dropdown-item" type="button">Excluir</button>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td v-text="i.NomeArea"></td>


                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-12 col-md-12 col-xl-4">
                                <h5>Áreas Selecionadas</h5>
                                <div class="bloqueio text-center" v-show="listaPerfisAreas.length === 0">
                                    <i class="fas fa-lock wow bounce" data-wow-duration="1s" data-wow-offset="2" data-wow-iteration="2"></i>
                                    <h5 class="mt-4">Selecione uma Área</h5>
                                </div>
                                <div class="h450-r p-4" id="listaAreasSelecionadas">
                                    <table class="table table-striped mt-3">
                                        <thead>
                                            <tr>
                                                <th>Perfil</th>
                                                <th>Area</th>
                                                <th>Permissão</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(i, index) in listaPerfisAreas">
                                                <td>{{NomePerfilSelecionado}}</td>
                                                <td>{{i.nomeArea}}</td>
                                                <td>
                                                    <template v-if="i.Permissao === 1 || i.Permissao === '1'">Leitura</template>
                                                    <template v-if="i.Permissao === 2 || i.Permissao === '2'">Escrita</template>
                                                    <template v-if="i.Permissao === 3 || i.Permissao === '3'">Exclusão</template>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                            <div class="w-100 text-right">
                                <button @click="window.location.reload()" class="btn btn-outline-secondary mt-4 mr-2"><i class="fa fa-save"></i> Cancelar</button>
                                <button v-show="listaPerfisAreas.length > 0" @click="salvarPermissoes()" class="btn btn-success mt-4 mr-2"><i class="fa fa-save"></i> Salvar</button>
                            </div>


                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="row clearfix p-3 mt-5">
            <div class="card">
                <div class="body">
                    <h4>Permissões Cadastradas</h4>
                    <table class="table table-striped" id="permissoesCadastradas">
                        <thead>
                            <tr>
                                <th>Area</th>
                                <th>URL</th>
                                <th>Perfil</th>
                                <th>Permissão</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in listaPermissoesCadastradas">
                                <td v-text="i.NomeArea"></td>
                                <td v-text="i.Url"></td>
                                <td v-text="i.NomePerfil"></td>
                                <td>
                                    <template v-if="i.Permissao === 1 || i.Permissao === '1'">Leitura</template>
                                    <template v-if="i.Permissao === 2 || i.Permissao === '2'">Escrita</template>
                                    <template v-if="i.Permissao === 3 || i.Permissao === '3'">Exclusão</template>
                                </td>
                                <td>
                                    <!-- <button class="btn btn-info btn-sm"><i class="fa fa-pen"></i></button> -->
                                    <button @click="deletarPermissao(i.CodigoControleAcessoPerfis)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>



</div>


<?php $this->load->view('cgmab/ControleAcesso/scripts/appAreas'); ?>
<?php $this->load->view('cgmab/ControleAcesso/scripts/appPerfis'); ?>