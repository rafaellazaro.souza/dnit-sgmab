<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Áreas Sistema</h2>
                <ul class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Áreas</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid" id="areasSistema">
        <div class="row clearfix">
            <div class="col-lg-12">
                <button class="btn btn-success mb-3" @click="mostrarFormulario = true"><i class="fa fa-plus"></i> Adicionar Área</button>
                <div class="card">
                    <div class="body">
                        <table class="table table-striped" id="tblAreasSistema">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Icone</th>
                                    <th>Nome</th>
                                    <th>Status</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="i in listaAreas">
                                    <td v-text="i.CodigoAreaSistema"></td>
                                    <td><i :class="i.Icone"></i></td>
                                    <td v-text="i.NomeArea"></td>
                                    <td>
                                        <template v-if="i.Status === '1'">
                                            <button class="btn btn-success btn-sm">Ativo</button>
                                        </template>
                                        <template v-else>
                                            <button class="btn btn-danger btn-sm">Inativo</button>
                                            <button @click="editar(i.CodigoAreaSistema, i, 'ativar');"  class="btn btn-info btn-sm"><i class="fa fa-check"></i></button>
                                        </template>

                                    </td>
                                    <td>
                                        <button @click="editar(i.CodigoAreaSistema, i, 'ativo'); mostrarFormulario = true" class="btn btn-info btn-sm"><i class="fa fa-pen"></i></button>
                                        <button @click="editar(i.CodigoAreaSistema, i, 'inativo'); " class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('cgmab/ControleAcesso/paginas/formularios'); ?>
    </div>



</div>


<?php $this->load->view('cgmab/ControleAcesso/scripts/appAreas'); ?>