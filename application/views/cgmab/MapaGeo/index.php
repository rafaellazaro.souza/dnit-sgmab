<?php $this->load->view('cgmab/css/comum.php') ?>
<?php $this->load->view('../../webroot/arquivos/plugins/leatleft/css/leatleft.php') ?>
<?php $this->load->view('../../webroot/arquivos/plugins/leatleft/js/leatleft.php') ?>
<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Mapa Geo</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Mapa Geo</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>
    <style>
        .botoesMapa {
            margin-left: -25px !important;
            width: 100%;
            z-index: 1;
        }

        .botoesMapa button {
            width: 120px !important;
            height: 100px;
        }

        .botoesMapa img {
            width: 35px !important;
        }

        .map-height {
            width: 100%;
            height: 1000px;

        }

        .layerMonitoramento {
            width: 10px;
            height: 10px;
            border-radius: 5px;
            background-color: red;
            z-index: 1000;
        }

        .layerPontosColeta {
            width: 10px;
            height: 10px;
            border-radius: 5px;
            background-color: blue;
            z-index: 1000;
        }

        .layerArmadilha {
            width: 10px;
            height: 10px;
            border-radius: 5px;
            background-color: yellow;
            z-index: 1000;
        }

        .layerPassagem {
            width: 10px;
            height: 10px;
            border-radius: 5px;
            background-color: aqua;
            z-index: 1000;
        }

        .layerLicencas {
            width: 10px;
            height: 10px;
            border-radius: 5px;
            background-color: green;
            z-index: 1000;
        }
    </style>
    <div class="container-fluid" id="maps">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="row">
                            <div class="form-group col-md-2">
                                <label>UF</label>
                                <select v-model="uf" class="form-control show-tick ms select2">
                                    <option value=""></option>
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espirito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">Paraíba</option>
                                    <option value="PR">Paraná</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="TO">Tocantins</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Contrato</label>
                                <select v-model="codigoContrato" class="form-control show-tick ms select2">
                                    <option value=""></option>

                                    <option v-for="i in filtrosContrato" :value="i.CodigoContrato">{{i.Numero}}</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Mês</label>
                                <select v-model="mes" class="form-control show-tick ms select2">
                                    <option value=""></option>
                                    <option v-for="i in filtrosMes" :value="i.MesReferencia">{{i.MesReferencia}}/{{i.AnoReferencia}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="p-0">
                                <div id="mapid" class="map-height">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 botoesMapa">
                            <button @click="getMonitoramentoFauna()" :class="btnMonitoramentoFauna ? 'btn-success' : ''" class="btn  "><img src="<?= base_url('webroot/arquivos/images/coelho.png') ?>" alt=""><br>
                                <div class="layerMonitoramento"></div> <small>Atropelamento ({{totalAtropelamentos}})</small>
                            </button>
                            <button @click="getRecursosHidricos()" :class="btnRecurosHidricos ? 'btn-success' : ''" class="btn"><img src="<?= base_url('webroot/arquivos/images/microscopio.png') ?>" alt=""><br>
                                <div class="layerPontosColeta"></div><small>Pontos de coleta ({{totalPontosHidr}})</small>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('cgmab/MapaGeo/script.php');
?>