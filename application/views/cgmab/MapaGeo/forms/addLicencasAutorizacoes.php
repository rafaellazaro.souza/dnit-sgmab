<div class="content mb-5" v-show="secaoAddLicencas">
    <div class="card" :class="expandAddLicencas">

        <div class="card-body p-4">

            <h4>Adicionar Licença</h4>
            <!-- form add ou editar licencas --->
            <form id="form-add-licenca" class="p-4">
                <div class="row">

                    <div class="alert alert-warning p-4 w-100 mb-4" v-show="errosInsertLicensa.length > 0">
                        <h4>Atenção! Preencha os campos abaixo:</h4>
                        <ul>
                            <li v-for="i in errosInsertLicensa" v-text="i"></li>
                        </ul>
                    </div>
                   

                    <div class="form-group col-12 col-sm-4 col-md-6 col-xl-2">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Modal
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" @change="removeDados($event)" v-model="novaLicenca.Modal">
                                <option :value="'Aquaviario'">Aquaviário</option>
                                <option :value="'Ferroviario'">Ferroviário</option>
                                <option :value="'Rodoviario'">Rodoviário</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-md-6 col-xl-2">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    UF

                                </div>
                            </div>
                            <select class="form-control show-tick ms select2l" @change="getRodovia($event)" v-model="novaLicenca.CodigoUF">
                                <option v-for="i in listaUf" :value="i.CodigoUF">{{i.nome}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-xl-2" v-show="novaLicenca.Modal != 'Aquaviario' && novaLicenca.Modal != 'Ferroviario'">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    BR
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" :disabled="vmGlobal.attr.disabled" v-model="novaLicenca.CodigoVia">
                                <option v-for="i in listaRodovia" :value="i.CodigoRodovia">{{i.Rodovia}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-12 col-sm-6 col-md-6 col-xl-2" v-if="novaLicenca.Modal != 'Aquaviário'">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Km Inicial

                                </div>
                            </div>
                            <input type="number" class="form-control" v-model="novaLicenca.KmInicial">
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6  col-xl-2" v-if="novaLicenca.Modal != 'Aquaviário'">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Km Final

                                </div>
                            </div>
                            <input type="number" class="form-control" v-model="novaLicenca.KmFinal" @blur="calcularExtensao()">
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Número da Licença
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.NumeroLicenca">
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Tipo de Licença

                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.CodigoLicencaTipo">
                                <option v-for="i in listaTiposContratos" :value="i.CodigoLicencaTipo">{{i.Sigla}}</option>

                            </select>
                        </div>

                    </div>



                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Início do SubTrecho
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.InicioDoSubTrecho">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Fim do SubTrecho
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.FimDoSubTrecho">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Empreendimento
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.CodigoEmpreendimento">
                                <option :value="i.CodigoEmpreendimento" v-for="i in listaEmpreendimentos">{{i.Sigla}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Órgão Expeditor

                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.CodigoOrgaoExpeditor">
                                <option v-for="i in listaOrgaosExpeditor" :value="i.CodigoOrgaoExpeditor">{{i.NomeOrgaoExpeditor}}</option>

                            </select>
                        </div>

                    </div>

                    <div class="form-group col-12 col-md-4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Município
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.Municipio">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Processo Administrativo Interveniente
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.ProcessoAdministrativoInterveniente">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Ação
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.Acao">
                                <option :value="'Em andamento'">Em andamento</option>
                                <option :value="'Em Trâmite'">Em Trâmite</option>
                                <option :value="'Solicitação de LI'">Solicitação de LI</option>
                                <option :value="'Solicitação de Renovação'">Solicitação de Renovação</option>
                                <option :value="'Solicitação de Retificação'">Solicitação de Retificação</option>

                            </select>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Situação Atual
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.SituacaoAtual">
                                <option :value="'Ativa'">Ativa</option>
                                <option :value="'Inativa'">Inativa</option>
                                <option :value="'Renovada'">Renovada</option>
                                <option :value="'Retificada'">Retificada</option>
                                <option :value="'RetificaaoRenovacao'">Retificação de Renovação</option>

                            </select>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Status
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.Status">
                                <option :value="'Vigente'">Vigente</option>
                                <option :value="'Expirada'">Expirada</option>
                                <option :value="'Sem Efeito'">Sem Efeito</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Prazo Validade (anos)
                                </div>
                            </div>
                            <input type="number" class="form-control" v-model="novaLicenca.PrazoValidade">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Processo SEI
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.ProcessoSei">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Link para Processo SEI
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.LinkProcessoSei">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Número do Processo SEI
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.NumeroProcessoSei">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Link Número Processo SEI
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.LinkNumeroSei">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Data de Emissão
                                </div>
                            </div>
                            <input type="text" class="form-control" @blur="calcularVencimento()" maxlength="10" onkeypress="formatar(this, '00/00/0000')" v-model="novaLicenca.DataEmissao">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Data de Vencimento
                                </div>
                            </div>
                            <input type="text" class="form-control" maxlength="10" onkeypress="formatar(this, '00/00/0000')" v-model="novaLicenca.DataVencimento">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Objeto
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.Objeto">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Observações
                                </div>
                            </div>
                            <textarea class="form-control" v-model="novaLicenca.Observacoes"></textarea>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Coordenadas (Colar aqui)
                                </div>
                            </div>
                            <textarea class="form-control" v-model="novaLicenca.Coordenada"></textarea>
                        </div>
                    </div>
                    <!-- <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Georreferenciamento da Licença
                                </div>
                            </div>
                            <input type="file" class="form-control" v-model="novaLicenca.Coordenada">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Documento da Licença
                                </div>
                            </div>
                            <input type="file" class="form-control" v-model="novaLicenca.NomeArquivoShapeFile">
                        </div>
                    </div> -->

                    <div class="col-12 text-right">
                        <button type="button" @click="salvarLicenca()" type="button" class="btn btn-success">Salvar nova licença</button>
                    </div>

                </div>
            </form>
            <!-- form add ou editar licencas --->




        </div>

        <!-- /.card-footer-->
    </div>


</div>