<script>
    var base_url = '<?php echo base_url() ?>';

    mapageo = new Vue({
        el: "#maps",
        data: {
            mymap: null,
            // layers
            layerMonitoramentoFauna: null,
            layerRecurosHidricos: null,
            layerRecurosHidricos: null,
            layerLicencas: null,
            // fim layers
            btnMonitoramentoFauna: false,
            btnRecurosHidricos: false,
            filtrosMes: [],
            filtrosContrato: [],
            codigoContrato: '',
            uf: '',
            mes: '',
            id1: 1,
            id2: 1,
            totalAtropelamentos: 0,
            totalPontosHidr: 0
        },
        mounted() {
            this.getFiltros();
            this.gerarMapa()
            this.layerMonitoramentoFauna = L.geoJSON().addTo(this.mymap)
            this.layerRecurosHidricos = L.geoJSON().addTo(this.mymap)
        },
        watch: {
            codigoContrato() {
                this.getAtropelamento();
                this.getHidricos()
            },
            mes() {
                this.getAtropelamento();
                this.getHidricos()
            },
            uf() {
                this.getAtropelamento();
                this.getHidricos()
            }
        },
        methods: {
            getMonitoramentoFauna() {
                var valor = this.id1++;
                if (valor & 1) {
                    this.getAtropelamento()
                    this.btnMonitoramentoFauna = true
                } else {
                    this.layerMonitoramentoFauna.clearLayers();
                    this.btnMonitoramentoFauna = false
                }
            },
            getRecursosHidricos() {
                var valor = this.id2++;
                if (valor & 1) {
                    this.getHidricos()
                    this.btnRecurosHidricos = true
                } else {
                    this.layerRecurosHidricos.clearLayers();
                    this.btnRecurosHidricos = false
                }
            },
            getAtropelamento() {
                var fd = new FormData();
                fd.append('mes', this.mes)
                fd.append('contrato', this.codigoContrato)
                fd.append('uf', this.uf)
                axios.post(base_url + 'MapaGeo/getMonitoramentoFauna', fd)
                    .then((resp) => {
                        this.totalAtropelamentos = resp.data.length
                        this.layerMonitoramentoFauna.clearLayers();
                        for (var i = 0; i < resp.data.length; i++) {
                            var cord = resp.data[i]
                            let arr = JSON.stringify({
                                "type": "Polygon",
                                "coordinates": [
                                    [
                                        [cord.Long, cord.Lat]
                                    ]
                                ]
                            });
                            let conct = '<div style="width: 500px"><p><b>Atropelamento de animais</b><br>';
                            conct += '<img style="width: 300px" src="' + cord.CaminhoFoto + '"><br>'
                            conct += '<br>Grupo: <b>' + cord.Grupo + "</b><br>"
                            conct += '<br>Classe: <b>' + cord.Classe + "</b>"
                            conct += '<br>Ordem: <b>' + cord.Ordem + "</b>"
                            conct += '<br>Família: <b>' + cord.Familia + "</b>"
                            conct += '<br>Especie: <b>' + cord.Especie + "</b>"
                            conct += '<br>Nome Comum: <b>' + cord.NomeComum + "</b>"
                            conct += '<br>Obs: <b>' + cord.Observacoes + "</b>"
                            conct += '<br>Data foto: <b>' + cord.DataFoto + "</b>"
                            conct += '<br>Contrato: <b>' + cord.Contrato + "</b>"
                            conct += '<br>UF: <b>' + cord.UF + "</b>"
                            conct += '<br>Rodovia: <b>' + cord.Rodovia + "</b>"
                            conct += '<br>Nome Serviço: <b>' + cord.NomeServico + "</b>"
                            conct += '<br>Mês/Ano de referência: <b>' + cord.MesReferencia + " / " + cord.AnoReferencia + "</b>"
                            conct += '</p></div>'
                            this.aplicarLayer(arr, conct, 'red', this.layerMonitoramentoFauna);
                        }
                    })


            },
            getFiltros() {

                axios.get(base_url + 'MapaGeo/getFiltros')
                    .then((resp) => {
                        console.log(resp)
                        this.filtrosMes = resp.data.meses;
                        this.filtrosContrato = resp.data.contratos;
                    })
            },


            async getHidricos() {

                var fd = new FormData();
                fd.append('mes', this.mes)
                fd.append('contrato', this.codigoContrato)
                fd.append('uf', this.uf)

                axios.post(base_url + 'MapaGeo/getRecursosHidricos', fd)
                    .then((resp) => {
                        this.totalPontosHidr = resp.data.length

                        this.layerRecurosHidricos.clearLayers();
                        var cord = [];
                        let arr = {};
                        //for (var i = 0; i < resp.data.length; i++) {
                        resp.data.forEach(async (i, index) => {
                            var cord = i
                            let arr = JSON.stringify({
                                "type": "Polygon",
                                "coordinates": [
                                    [
                                        [cord.Lng, cord.Lat]
                                    ]
                                ]
                            });
                            let conct = '<div style="width: 500px"><p><b>Recuros hídricos</b><br>';
                            conct += '<img style="width: 300px" src="' + cord.Caminho + '"><br>'
                            conct += '<br>Tipo ponto amostra: <b>' + cord.TipoPontoAmostra + "</b><br>"
                            conct += '<br>Nome do ponto de coleta: <b>' + cord.NomePontoColeta + "</b>"
                            conct += '<br>Bacia: <b>' + cord.Bacia + "</b>"
                            conct += '<br>Classe 1: <b>' + cord.Classe1 + "</b>"
                            conct += '<br>Classe 2: <b>' + cord.Classe2 + "</b>"
                            conct += '<br>Classe 3: <b>' + cord.Classe3 + "</b>"
                            conct += '<br>Elevação: <b>' + cord.Elevacao + "</b>"
                            conct += '<br>Estaca: <b>' + cord.Estaca + "</b>"
                            conct += '<br>Km Inicial: <b>' + cord.KmInicial + "</b>"
                            conct += '<br>Km Final: <b>' + cord.KmFinal + "</b>"
                            conct += '<br>Massa Água: <b>' + cord.MassaAgua + "</b>"
                            conct += '<br>Tipo Massa Água: <b>' + cord.TipoMassaAgua + "</b>"
                            conct += '<br>Tipo Ambiente: <b>' + cord.TipoAmbiente + "</b>"
                            conct += '<br>Mês/Ano de referência: <b>' + cord.MesReferencia + " / " + cord.AnoReferencia + "</b>"
                            conct += '<br>Município: <b>' + cord.Municipio + "</b>"
                            conct += '<br>Contrato: <b>' + cord.Contrato + "</b>"
                            conct += '<br>UF: <b>' + cord.UF + "</b>"
                            conct += '<br>Rodovia: <b>' + cord.Rodovia + "</b>"
                            conct += '<br>Obs: <b>' + cord.Observacoes + "</b>"
                            conct += '</p></div>';

                            //buscar Medições dos parametros
                            let params = {CodigoConfiguracaoRecursosHidricosFotos: cord.CodigoConfiguracaoRecursosHidricosFotos}
                            var parametros = '<br><br><b>Medições</b><br>';
                            var dados = [];
                             await axios.post('MapaGeo/buscarMedicoesParametros', $.param(params)).then((resp)=>{
                                dados = resp.data
                             });
                           
                            for (var i = 0; i < dados.length; i++) {
                                var dado = dados[i]
                                parametros += '<br><b>' + dado.nomeP + ":</b>" + dado.Valor;

                            }
                            conct += parametros;
                            //buscar Medições dos parametros
                            this.aplicarLayer(arr, conct, 'blue', this.layerRecurosHidricos);
                        });



                    })

            },
            async buscarMedicoesParametros(CodigoConfiguracaoRecursosHidricosFotos) {
                let params = {
                    table: 'relatorioMedicao',
                    where: {
                        CodigoConfiguracaoRecursosHidricosFotos: CodigoConfiguracaoRecursosHidricosFotos
                    }
                }
                const dados = await vmGlobal.getFromAPI(params);

                let conct = '<div style="width: 500px;margin-top:30px"><p><b>Medições</b><br>';
                for (var i = 0; i < dados.length; i++) {
                    var dado = dados[i]
                    let conct = '<div style="width: 500px;margin-top:30px"><p><b>Medições</b><br>';
                    conct += '<br>' + dado.NomeParametro + ":</b>" + dado.Valor + "<br>";

                }
                conct += '</p></div>';
                return conct;
            },
            getLicencas() {
                axios.get(base_url + 'MapaGeo/getLicencas')
                    .then((resp) => {
                        this.layerLicencas = L.geoJSON().addTo(this.mymap);
                        this.layerLicencas.clearLayers();
                        for (var i = 0; i < resp.data.length; i++) {
                            var cord = resp.data[i]
                            this.aplicarLayer(cord.coordenada, cord.Concatenado, 'green', this.layerLicencas);
                        }
                    })
            },

            gerarMapa() {
                const style = 'reduced.night';
                this.mymap = L.map('mapid').setView(['-10.6007767', '-63.6037797'], 4.5);
                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 18,
                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
                    id: 'mapbox.streets',
                    fadeAnimation: true,
                }).addTo(this.mymap);
            },

            aplicarLayer(coord, objectid, color, layer) {
                var array = {
                    "type": "FeatureCollection",
                    "features": [{
                        "type": "Feature",
                        "geometry": JSON.parse(coord),
                        "properties": {
                            "OBJECTID": objectid,

                        }
                    }]
                }
                L.geoJson(array, {
                    style: function(feature) {
                        return {
                            stroke: true,
                            color: color,
                            opacity: 1,
                            fillColor: color,
                            weight: 8,
                            fillOpacity: 1
                        };
                    },
                    onEachFeature: function(feature, l) {
                        l.bindPopup(feature.properties.OBJECTID);

                        l.on('click', function(e) {
                            l.setStyle({
                                weight: 12,
                                outline: 'red'
                            });
                        });

                        l.on("popupclose", function(e) {
                            l.setStyle({
                                weight: 5,
                            });
                        });
                    }
                }).addTo(layer)
            },


        },

    });
</script>