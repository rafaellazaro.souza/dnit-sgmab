<style>
    /*map*/
    .map-height {
        width: 100%;
        height: 1000px;
        z-index: 1
    }

    .lock {
        width: 100%;
        height: 100%;
        background: rgba(255, 255, 255, 0.94);
        position: absolute;
        top: 0;
        z-index: 999;
        padding-top: 11%;
    }

    .lock>img {
        width: 70px;
        position: fixed;
    }

    #spinner {
        position: fixed;
        z-index: 999;
        height: 5em;
        width: 5em;
        overflow: show;
        margin: auto;
        top: 50%;
        left: 50%;
        display: block;
    }

    .top-filter {
        position: relative !important;

    }

    .menu-layer-mapa {
        width: 74px;
        position: fixed;
        right: -1px;
        bottom: 0;
        z-index: 4;
        background-color: rgba(255, 255, 255, 0.97);
        padding: 7px 0px 0px 0px;
        box-shadow: -1px 1px 11px rgba(0, 0, 0, 0.24);
        border-radius: 7px;
    }

    .row.menu-layer-mapa>div>button>i {
        font-size: 17px;
    }

    .row.menu-layer-mapa>div>div {
        position: absolute;
        width: 165px;
        left: -171px;
        background-color: #5d80a5;
        top: 0;
        padding: 5px 0 5px 12px;
        color: #ffffff;
        border-left: 2px solid #2d3c49
    }
    .nradius{
        border-radius: 0px !important;
    }
</style>