<?php $this->load->view('cgmab/OrgaoLicenciamentos/css/css'); ?>
<div class="container-fluid">
    <section class="content-header">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tipos de Licença</h1>
            </div>
            <div class="col-sm-6 text-right">
                <a href="<?php echo site_url('cgmab/home') ?>"><i class="fa fa-times"></i></a>
            </div>
            <div class="col-sm-6 text-right">

            </div>

        </div>
    </section>

    <section class="content mb-5">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-list mr-3"></i> Lista de Tipos de Licença</h3>

                <div class="card-tools">

                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <!-- botão expandir -->
                    <button type="button" class="btn btn-tool" v-if="vmGlobal.expandSection === ''" @click="vmGlobal.expandirCards('listaLicencas') ">
                        <i class="fas fa-expand"></i></button>
                    <button type="button" class="btn btn-tool" v-else @click="vmGlobal.expandirCards('') ">
                        <i class="fas fa-expand"></i></button>
                    <!-- botão expandir -->

                </div>
            </div>
            <div class="card-body">
                <div class="row" id="ListagemOrgaodeLicenciamento">
                    <table class="table table-striped" id="tblListaOrgaodeLicenciamento">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in listaOrgaodeLicenciamento">
                                <td>{{i.NomeOrgao}}</td>
                                <td v-text="i.Status === '1' ? 'Ativo' : 'Inativo'"></td>
                            </tr>
                        </tbody>

                    </table>
                </div>




            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>

</div>

<?php $this->load->view('cgmab/OrgaoLicenciamentos/scripts/app'); ?>