    <script>
    vmListaOrgaodeLicenciamento = new Vue({
        el: '#ListagemOrgaodeLicenciamento',
        data: {
            listaOrgaodeLicenciamento: []
        },
        mounted(){
            this.getOrgaodeLicenciamento();
        },
        methods: {
            async getOrgaodeLicenciamento(){
                var params = {
                    table: 'OrgaodeLicenciamento',
                    where: {
                        Status: 1
                    }
                }
                this.listaOrgaodeLicenciamento = await vmGlobal.getFromAPI(params, 'cgmab');
            }
        }
    });
</script>
