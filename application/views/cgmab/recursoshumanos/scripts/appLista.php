<script>
    vmListarRecursos = new Vue({
        el: '#recursosLista',
        data: {
            listaRecursos: []
        },
        mounted() {
            this.getRecursosHumanos();

        },
        methods: {
            async getRecursosHumanos() {
                $('#recursosLista').DataTable().destroy();
                var params = {
                    table: 'recursosHumanos',
                    join: {
                        table: 'contratada',
                        on: 'CodigoContratada'
                    },
                    join: {
                        table: 'contratos',
                        on: 'CodigoContrato'
                    },
                    where: {
                        CodigoContratada: <?= $_SESSION['Logado']['CodigoContratada'] ?>
                    }
                }
                this.listaRecursos = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#recursosLista', true)
            },
            getProfissional(codigoProfissional) {
                vmFormularios.getProfissional(codigoProfissional);
            },


            async deletarPessoa(codigo, caminho, index) {
                let params = {
                    table: 'recursosHumanos',
                    where: {
                        CodigoRecursosHumanos: codigo
                    }
                }
                let deletar = await vmGlobal.deleteFromAPI(params);
                if (deletar) {
                    await vmGlobal.deleteFile(caminho); //deletar arquivo da pasta
                    // await axios.post(base_url+'RecursosHumanos/deleteFile', $.param({path: caminho}));
                    this.listaRecursos.splice(index, 1);
                }
            }
        }
    });
</script>