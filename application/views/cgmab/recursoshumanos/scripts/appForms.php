<script>
    var vmFormularios = new Vue({
        el: '#dadosProfissioal',
        data: {
            mostrarFormulario: false,
            mostrarBloqueioAnexos: null,
            dadosProfissional: {},
            listaContratos: [],
            listaDocumentos: [],
            erros: [],
            qtdDocumentos: 0,
            idInserido: 0,
        },
        methods: {
            async getContratos() {
                let params = {
                    table: 'contratos',
                    where: {
                        CnpjEmpresa: <?= isset($_SESSION['Logado']['CnpjContratada']) ? $_SESSION['Logado']['CnpjContratada'] : 0; ?>,
                    }
                }
                this.listaContratos = await vmGlobal.getFromAPI(params);
            },
            checkForm: function(e) {
                if (this.dadosProfissional.NomePessoa && this.dadosProfissional.CPF && this.dadosProfissional.Email && this.dadosProfissional.Profissao && vmGlobal.validarCPF(this.dadosProfissional.CPF) === true) {
                    this.salvarProfissional();
                }

                this.erros = [];

                if (!this.dadosProfissional.NomePessoa) {
                    this.erros.push('Nome é obrigatório!');
                }
                if (!this.dadosProfissional.CPF) {
                    this.erros.push('CPF é obrigatório!');
                }
                if (!this.dadosProfissional.Email) {
                    this.erros.push('E-mail é obrigatório!');
                }
                if (!this.dadosProfissional.Profissao) {
                    this.erros.push('Profissao é obrigatório!');
                }
                if (vmGlobal.validarCPF(this.dadosProfissional.CPF) === false) {
                    this.erros.push('O CPF Digitado é Inválido!');
                }

                e.preventDefault();
            },

            async salvarProfissional() {
                let liberaAnexos = false;
                var file = null;
                if (this.$refs.Foto != null && this.$refs.Foto != undefined) {
                    this.dadosProfissional.FilesRules = {
                        upload_path: 'webroot/uploads/recursosHumanos',
                        allowed_types: 'jpg|jpeg|png|gif',
                    };
                    file = {
                        name: 'Foto',
                        value: this.$refs.Foto.files[0]
                    }
                }
                // verifica se salva ou atualiza 
                if (this.dadosProfissional.CodigoRecursosHumanos) {
                    var params = {
                        table: 'recursosHumanos',
                        data: {
                            NomePessoa: this.dadosProfissional.NomePessoa,
                            CPF: this.dadosProfissional.CPF,
                            Email: this.dadosProfissional.Email,
                            Profissao: this.dadosProfissional.Profissao,
                            FilesRules: this.dadosProfissional.FilesRules
                        },
                        where: {
                            CodigoRecursosHumanos: this.dadosProfissional.CodigoRecursosHumanos
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null, 'cgmab');
                } else {
                    this.dadosProfissional.CodigoContratada = <?= $_SESSION['Logado']['CodigoContratada'] ?>;
                    var params = {
                        table: 'recursosHumanos',
                        data: this.dadosProfissional
                    }
                    //insertFromApiAndReturnId(showMessage = true, params, file = null, base = null, msg = null) 
                   this.idInserido =  await vmGlobal.insertFromApiAndReturnId(false , params, file);
                    this.mostrarBloqueioAnexos =  false;
                }
                
                vmListarRecursos.getRecursosHumanos();
            },

            async getProfissional(codigoProfissional) {
                var params = {
                    table: 'recursosHumanos',
                    where: {
                        CodigoRecursosHumanos: codigoProfissional
                    }
                }
                var dados = await vmGlobal.getFromAPI(params, 'cgmab');

                this.dadosProfissional.CodigoRecursosHumanos = dados[0].CodigoRecursosHumanos;
                this.dadosProfissional.NomePessoa = dados[0].NomePessoa;
                this.dadosProfissional.CPF = dados[0].CPF;
                this.dadosProfissional.Email = dados[0].Email;
                this.dadosProfissional.Profissao = dados[0].Profissao;

                this.mostrarFormulario = true;
            },
            async consultarCPF() {
                let params = {
                    table: 'recursosHumanos',
                    where: {
                        CPF: this.dadosProfissional.CPF
                    }
                }
                let dados = await vmGlobal.getFromAPI(params, 'cgmab');
                console.log(dados);
                if (dados.length > 0) {
                    alert('CPF Já Cadastrado');
                    $('#btn-salvar-profissional').addClass('invisible');
                } else {
                    $('#btn-salvar-profissional').removeClass('invisible');
                }

            },
            async consultarEmail() {
                let params = {
                    table: 'recursosHumanos',
                    where: {
                        Email: this.dadosProfissional.Email
                    }
                }
                let dados = await vmGlobal.getFromAPI(params, 'cgmab');
                console.log(dados);
                if (dados.length > 0) {
                    alert('E-mail Já Cadastrado');
                    $('#btn-salvar-profissional').addClass('invisible');
                } else {
                    $('#btn-salvar-profissional').removeClass('invisible');
                }

            },
            async addDocumentos() {
                var myfiles = document.getElementById("documentosMaterial");
                var files = myfiles.files;
                var data = new FormData();

                for (i = 0; i < files.length; i++) {
                    data.append(i, files[i]);
                    console.log('files[i]', files[i])
                }

                data.append('CodigoRecursosHumanos', parseInt(this.idInserido));

          
                $.ajax({
                    url: '<?= base_url('RecursosHumanos/insertAnexos') ?>',
                    method: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Salvo!',
                            text: 'Fotos salvas com sucesso.',
                            showConfirmButton: true,
                        });
                        document.getElementById('documentosMaterial').value = null;
                    },
                    error: function(error) {
                        console.log(error);
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao tentar salvar arquivo. " + e,
                            showConfirmButton: true,

                        });
                    },
                    complete: function() {
                        vmFormularios.getAnexos();
                        $('.dropify-clear').click();
                    }
                });
            },
            async getAnexos(){
                let params = {
                    table: 'anexos',
                    where: {
                        CodigoRecursosHumanos: this.idInserido
                    }
                }
                this.listaDocumentos = await vmGlobal.getFromAPI(params);
            },
            async deletarArquivo(codigo, caminho) {
                await vmGlobal.deleteFromAPI({
                    table: 'anexos',
                    where: {
                        CodigoAnexo: codigo
                    }
                });
                await vmGlobal.deleteFile(caminho);
                await this.getAnexos();
            },

        }
    });
    $(document).ready(function() {
        "use strict";
        $('.dropify').dropify({
            messages: {
                default: "Solte ou clique para anexar arquivo."
            }
        });
    });
</script>