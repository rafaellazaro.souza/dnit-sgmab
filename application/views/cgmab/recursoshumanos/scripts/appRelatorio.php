<script>
    var vmAppRelatorio = new Vue({
        el: '#relatorio',
        data() {
            return {
                listaRecursos: []
            }
        },
        async mounted() {
            await this.getRelatorioRecursos();
        },
        methods: {

            async getRelatorioRecursos() {
               
                $('#recursosEncontrados').DataTable().destroy();
                this.listaRecursos = await vmGlobal.getFromController('RecursosHumanos/getRelatorioRecursos');
                vmGlobal.montaDatatable('#recursosEncontrados', true)
              
            }
        }
    })
</script>