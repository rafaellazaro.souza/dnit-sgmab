<?php $this->load->view('cgmab/recursoshumanos/css.php'); ?>
<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Gestão de Recursos Humanos</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="zmdi zmdi-home"></i> Início</a></li>
                    <li class="breadcrumb-item">Contratada</li>
                    <li class="breadcrumb-item active">Recursos Humanos</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header" id="dadosProfissioal">
                        <!-- <h2><strong>Basic</strong> Examples </h2> -->
                        <button class="btn" @click="mostrarFormulario = true; getContratos(); vmFormularios.mostrarBloqueioAnexos = true; vmFormularios.idInserido = 0">Adicionar Recursos Humanos</button>
                        <!-- ===================================================
                            Formulario Adicionar/Editar Dados do Profissional 
                         ===================================================-->
                        <?php $this->load->view('cgmab/recursoshumanos/paginas/adicionarNovo.php') ?>
                        <!-- ===================================================
                            FIM do Formulario Adicionar/Editar Dados do Profissional 
                         ===================================================-->
                    </div>
                    <div class="body">
                        <table class="table table-striped" id="recursosLista">
                            <thead>
                                <tr>
                                    <th>Foto</th>
                                    <th>Contrato</th>
                                    <th>Nome</th>
                                    <th>C.P.F</th>
                                    <th>Profissão</th>
                                    <th>Empresa</th>
                                    <th>CNPJ</th>
                                    <th>Ações</th>
                                </tr>

                            </thead>
                            <tbody>
                                <tr v-for="(i, index) in listaRecursos">

                                    <td>
                                        <img v-if="i.Foto" :src="i.Foto" class="img-thumbnail w-25" alt="fotoFuncionario">
                                        <img v-else src="https://via.placeholder.com/80" class="img-thumbnail w-25" alt="fotoFuncionario">

                                    </td>
                                    <td v-text="i.Numero"></td>
                                    <td v-text="i.NomePessoa"></td>
                                    <td v-text="i.CPF"></td>
                                    <td v-text="i.Profissao"></td>
                                    <td v-text="i.NomeContratada"></td>
                                    <td v-text="i.CnpjContratada"></td>
                                    <td>
                                        <button @click="getProfissional(i.CodigoRecursosHumanos); vmFormularios.getContratos(); vmFormularios.mostrarBloqueioAnexos = false; vmFormularios.idInserido = i.CodigoRecursosHumanos; vmFormularios.getAnexos()" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></button>
                                        <button @click="deletarPessoa(i.CodigoRecursosHumanos, i.Foto, index);" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<?php $this->load->view('cgmab/recursoshumanos/scripts/appLista.php') ?>
<?php $this->load->view('cgmab/recursoshumanos/scripts/appForms.php') ?>