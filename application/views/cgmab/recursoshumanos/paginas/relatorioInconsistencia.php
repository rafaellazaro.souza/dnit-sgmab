<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Relatório de Inconsistência de Alocação de Recursos</h2>
                <ul class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Recursos</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid" id="relatorio">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <!-- colocar filtros aqui  -->
                    </div>
                    <div class="body">
                        <a href="<?= base_url('RecursosHumanos/exportarRelatorioRecurso')?>" href="_blank"  class="btn"><i class="fas fa-file-excel"></i> Exportar para Excel</a>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="recursosEncontrados">
                                <thead>
                                    <th>Nome recurso</th>
                                    <th>CPF</th>
                                    <th>Contrato</th>
                                    <th>Término</th>
                                    <th>Serviço</th>
                                    <th>Condicionante</th>
                                    <th>Tema</th>
                                    <th>SubTema</th>
                                </thead>
                                <tbody>
                                    <tr v-for="i in listaRecursos">
                                        <td v-text="i.NomePessoa"></td>
                                        <td v-text="i.CPF"></td>
                                        <td v-text="i.Numero"></td>
                                        <td v-text="vmGlobal.frontEndDateFormat(i.DataTerminoPrevista)"></td>
                                        <td v-text="i.NomeServico"></td>
                                        <td v-text="i.Titulo"></td>
                                        <td v-text="i.NomeTema"></td>
                                        <td v-text="i.NomeSubtipo"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('cgmab/recursoshumanos/scripts/appRelatorio') ?>

