

<table  id="relatorio">
                                <thead>
                                    <th>Nome recurso</th>
                                    <th>CPF</th>
                                    <th>Contrato</th>
                                    <th>Término</th>
                                    <th>Serviço</th>
                                    <th>Condicionante</th>
                                    <th>Tema</th>
                                    <th>SubTema</th>
                                </thead>
                                <tbody>
                                    <?php foreach($listaRecursos as  $i){?>
                                    <tr>
                                        <td><?= $i['NomePessoa']?></td>
                                        <td><?= $i['CPF']?></td>
                                        <td><?= $i['Numero']?></td>
                                        <td><?= $i['DataTerminoPrevista']?></td>
                                        <td><?= $i['NomeServico']?></td>
                                        <td><?= $i['Titulo']?></td>
                                        <td><?= $i['NomeTema']?></td>
                                        <td><?= $i['NomeSubtipo']?></td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>

<?php 


$arquivo = 'Relatorio-Inconsitência-'.date('dd-mm-Y').'.xls';
// Configurações header para forçar o download  
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$arquivo.'"');
header('Cache-Control: max-age=0');
// Se for o IE9, isso talvez seja necessário
header('Cache-Control: max-age=1');
?>

