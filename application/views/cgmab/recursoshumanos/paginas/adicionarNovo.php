<div class="modal-w98-h50 sombra-2 p-5 br-10" v-if="mostrarFormulario">


    <div class="row">
        <div class="col-12 col-sm-6 pr-5 border-right">
            <div class="alert alert-warning p-3 text-left" v-show="erros.length">
                <b>Por Favor Preencha o(s) campo(s) abaixo:</b>
                <ul>
                    <li v-for="i in erros" v-text="i"></li>
                </ul>

            </div>
            <h5 class="w-100 text-left mb-4">Adicionar Profissional</h5>
            <form>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Contrato *</span>
                    </div>
                    <select v-model="dadosProfissional.CodigoContrato" class="form-control show-tick ms select2">
                        <option value="Selecione o contrato">Selecione um contrato</option>
                        <option :value="i.CodigoContrato" v-for="i in listaContratos" v-text="i.Numero+ '/'+ i.NomeEmpresa"></option>
                    </select>
                </div>
                <div class="input-group mb-3 mb-4">
                    <div class="input-group-prepend">
                        <span id="basic-addon1" class="input-group-text">Foto</span>
                    </div>
                    <input type="file" ref="Foto" name="Foto" required class="form-control">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Nome *</span>
                    </div>
                    <input type="text" v-model="dadosProfissional.NomePessoa" class="form-control" placeholder="Digite o Nome" require>

                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">CPF *</span>
                    </div>
                    <input type="number" v-model="dadosProfissional.CPF" @blur="consultarCPF" class="form-control" placeholder="Digite o CPF - apenas números" require>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">E-mail *</span>
                    </div>
                    <input type="email" v-model="dadosProfissional.Email" @blur="consultarEmail" class="form-control" placeholder="Digite o E-mail" require>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Profissão *</span>
                    </div>
                    <input type="text" v-model="dadosProfissional.Profissao" class="form-control" placeholder="Digite a Profissão" require>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Carteira Profissional *</span>
                    </div>
                    <input type="text" v-model="dadosProfissional.CarteiraProfissional" class="form-control" placeholder="EX: CREA-2541-DF" require>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Observações *</span>
                    </div>
                    <textarea v-model="dadosProfissional.Observacoes" class="form-control" rows="10"></textarea>
                </div>
                <div class="col-12 text-right">
                    <button @click="mostrarFormulario = false; dadosProfissional = {}; erros = []" class="btn btn-outline-secondary">Cancelar</button>
                    <button type="button" @click="checkForm(1)" id="btn-salvar-profissional" class="btn btn-secondary ml-2" v-if="idInserido == 0">Salvar</button>
                </div>

            </form>
        </div>
        <div class="col-12 col-sm-6 pl-5">
            <div :class="[mostrarBloqueioAnexos ? 'bloqueio text-center wow bounceIn' : 'bloqueio text-center wow bounceOut']" v-show="mostrarBloqueioAnexos"><i data-wow-duration="1s" data-wow-offset="2" data-wow-iteration="2" class="fas fa-lock wow bounce" style="visibility: visible; animation-duration: 1s; animation-iteration-count: 2; animation-name: bounce;"></i>
                <h5 class="mt-4">Adicione um usuário <br /><small> para liberar o upload de documentos</small></h5>
            </div>
            <div class="alert alert-success p-4 wow bounceIn" v-if="idInserido != 0">
                <p>Usuário foi cadastrado com sucesso! Por favor adicione os documentos exigidos no edital!</p>
            </div>
            <h5 class="w-100 text-left mb-4">Anexar Documentos</h5>
            <div class="body no-shadow border">
                <form enctype="multipart/form-data" method="POST">
                    <input type="file" id="documentosMaterial" ref="documentos" @change="qtdDocumentos" name="documentos[]" class="dropify" multiple>
                </form>
            </div>
            <button class="btn btn-outline-secondary mt-2 float-right" @click="addDocumentos">Importar</button>

            <div class="h450-r">
                <table class="table table-striped mt-2">
                    <tbody>
                        <tr v-for="i in listaDocumentos">
                            <td v-text="i.Descricao"></td>
                            <td><a :href="i.Caminho"><i class="fa fa-download"></i></a></td>
                            <td><button @click="deletarArquivo(i.CodigoAnexo, i.Caminho)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div>