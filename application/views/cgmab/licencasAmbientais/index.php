<!-- css mapa  -->
<?php $this->load->view('../../webroot/arquivos/plugins/leatleft/css/leatleft.php') ?>
<!-- js mapa  -->
<?php $this->load->view('../../webroot/arquivos//plugins/leatleft/js/leatleft.php') ?>
<!-- css local -->
<?php $this->load->view('cgmab/licencasAmbientais/css.php') ?>
<?php $this->load->view('cgmab/css/comum.php') ?>


<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Gestão de Licenças Ambientais</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Gestão de Licenças Ambientais</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid" id="licenciamentoAmbiental">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                    </div>

                    <div class="col-12 row">
                        <div class="col-3 col-sm-3 col-md-2 p-0">
                            <button @click="homeMap()" class="btn btn-outline-secondary btn-sm w-100 p-3 text-left nradius" :class="showMapa ? 'active' : ''"><i class="fa fa-map"></i> Mapa</button>
                        </div>
                        <div class="col-4 col-sm-3 col-md-2 p-0 mr-1 ml-2">
                            <button @click="tabelaLicencas()" class="btn btn-outline-secondary btn-sm w-100 p-3 text-left nradius" :class="showTabela ? 'active' : ''"><i class="fa fa-list"></i> Tabelas</button>
                        </div>
                        <div class="col-5 col-sm-3 col-md-2 p-0" v-if="secaoCondicionantes != true">
                            <button @click="
								showTabela= '',
							 	showMapa = '',
							  	secaoAddLicencas = true,
							   	secaoMapa = false,
								secaoLicencas= false,
							 	iniciarDadosAdd(0)" id="addLicenca" class="btn btn-outline-secondary btn-sm w-100 p-3 text-left" :class="secaoAddLicencas ? 'active' : ''"><i class="fa fa-plus nradius"></i> Adicionar Licenças</button>
                        </div>
                    </div>
                    <div class="row">


                        <!-- MAPA -->
                        <div class=" mb-5 mt-2 p-0" :class="vmGlobal.expandSection === 'mapa' ? 'card full-card' : secaoDadosDaLicenca ? 'col-8' : 'col-12'" v-show="secaoMapa">
                            <!-- Default box :class="['card',{ full-card : expandMapa != '' }]"-->
                            <div class="p-0" :class="expandMapa ">

                                <div id="mapid" class="map-height"></div>

                                <!-- /.card-footer-->
                            </div>
                            <!-- /.card -->

                        </div>
                        <!-- FIM MAPA -->

                        <!-- dados da licença  -->
                        <div class="mb-5 mt-2" :class="secaoDadosDaLicenca ? 'col-4' : ''" v-if="secaoDadosDaLicenca">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fa fa-th-list mr-3"></i> Dados da Licença</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" @click="secaoDadosDaLicenca = !secaoDadosDaLicenca">
                                            <i class="fas fa-times"></i></button>
                                        <!-- botão expandir -->
                                    </div>
                                </div>
                                <div class="card-body p-4">
                                    <ul class="list-group">
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Número / Autorização:</span> {{dadosDaLicensa[0].NumeroLicenca}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">{{dadosDaLicensa[0].TipoVia}}:</span> {{dadosDaLicensa[0].VIA}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">UF:</span> {{dadosDaLicensa[0].UF}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Modal:</span> {{dadosDaLicensa[0].Modal}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Situação:</span> {{dadosDaLicensa[0].SituacaoAtual ? dadosDaLicensa[0].SituacaoAtual : 'Não Informado'}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Status:</span> {{dadosDaLicensa[0].Status}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Km Inicial:</span> {{dadosDaLicensa[0].KmInicial}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Km Inicial:</span> {{dadosDaLicensa[0].KmFinal}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Extensão:</span> {{dadosDaLicensa[0].Extensao}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Início do Sub Trecho:</span> {{dadosDaLicensa[0].InicioDoSubTrecho}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Fim do Sub Trecho:</span> {{dadosDaLicensa[0].FimDoSubTrecho}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Licenciamento/ Autorizacao:</span> {{dadosDaLicensa[0].LicenciamentoAutorizacao}}</li>
                                        <li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Link Documento da Licença:</span> <a class="btn btn-sm btn-outline-secondary" target="_blank" :href="(dadosDaLicensa[0].UsarLincencaAnexo == '1')? dadosDaLicensa[0].NomeLicencaAnexo : dadosDaLicensa[0].NomeArquivoLicencas">Exibir</a></li>

                                    </ul>
                                </div>


                            </div>
                        </div>
                        <!-- fim dados da licença  -->
                    </div>
                    <!-- Listagem 1-->
                    <div class="mb-5" :class="vmGlobal.expandSection === 'licencas' ? 'card full-card' : ''" v-show="secaoLicencas">
                        <table id="cadastroLicencasVertical" class="table table-striped pt-5">
                            <thead>
                                <tr>
                                    <th class="w-10" data-sort="false">
                                        <div class="row mr-5 ml-4">
                                            Ações
                                        </div>
                                    </th>
                                    <th>Validada</th>
                                    <th>Órgão Expeditor</th>
                                    <th>BR</th>
                                    <th>UF</th>
                                    <th>Tipo de Licença</th>
                                    <th>Número</th>
                                    <th>Status</th>
                                    <th>Situação Atual</th>
                                    <th>Validade</th>
                                    <th>Processo SEI</th>
                                    <th>Num. SEI</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="i in viewLicencas">

                                    <td class="w-10">
                                        <!--                                        <button @click="CodigoLicencaGeo = i.CodigoLicencaGeo" type="button" class="btn btn-outline-secondary btn-sm" title="Ver no mapa"><i class="fa fa-map-marker-alt"></i>-->
                                        <!--                                        </button>-->
                                        <!--                                        <button @click="gerarFormulario('editar-licencas'), vmGlobal.expandirCards('')" type="button" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target=".bd-example-modal-xl" title="Mais Detalhes"><i class="fas fa-chevron-right"></i></button>-->
                                        <a :href="'<?= base_url('condicionantes/index?codigoLicenca=') ?>'+i.CodigoLicenca+'&NumeroLicenca='+i.NumeroLicenca" class="btn btn-outline-secondary btn-sm" title="Ver Condicionantes"><i class="fas fa-th-list"></i> Gestão de Condicionantes</a>
                                        <a v-if="parseInt(vmGlobal.dadosDoUsuario.CodigoPerfil) == 1 || parseInt(vmGlobal.dadosDoUsuario.CodigoPerfil) == 5" @click="iniciarDadosAdd(i.CodigoLicenca)" class="btn btn-outline-secondary btn-sm" title="Editar Licença"><i class="fa fa-pencil-square-o"></i> Editar Licença</a>
                                    </td>
                                    <td>
                                        <template v-if="i.Validada === '1' || i.Validada === 1">
                                            <button v-if="vmGlobal.dadosDoUsuario.CodigoPerfil === '1'" @click="validarLicenca(i.CodigoLicenca, 0)" class="btn btn-sm btn-outline-danger"><i class="fa fa-times"></i></button>
                                            <span class="p-1 mr-2 btn-success"></span>
                                            Sim
                                            <!--<button v-if="i.StatusCondicionante === '1' || i.StatusCondicionante === 1" class="btn btn-sm btn-outline-danger invisivel"><i class="fa fa-times"></i> Reprovar</button>-->
                                        </template>

                                        <template v-else>
                                            <button v-if="vmGlobal.dadosDoUsuario.CodigoPerfil === '1'" @click="validarLicenca(i.CodigoLicenca, 1)" class="btn btn-sm btn-outline-success"><i class="fa fa-check"></i></button>
                                            <span class="p-1 mr-2 btn-danger"></span>
                                            Não
                                            <!--<button v-if="i.StatusCondicionante === '0' && vmGlobal.dadosDoUsuario.CodigoPerfil === '1'" class="btn btn-sm btn-outline-success "><i class="fa fa-check"></i> Aprovar</button>-->
                                        </template>
                                    </td>
                                    <td>{{i.NomeOrgaoExpeditor}}</td>
                                    <td>{{i.VIA}}</td>
                                    <td>{{i.UF}}</td>
                                    <td>{{i.Sigla }}</td>
                                    <td>{{i.NumeroLicenca}}</td>
                                    <td>{{i.Status}}</td>
                                    <td>{{i.SituacaoAtual}}</td>
                                    <td>{{formatData(i.DataVencimento, 1)}}</td>
                                    <td>{{i.ProcessoSei}}</td>
                                    <td>{{i.NumeroProcessoSei}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- Fim da Listagem -->

                    <!-- Listagem 2-->
                    <div class="mb-5" :class="vmGlobal.expandSection === 'condicionantes' ? 'card full-card' : ''" v-show="secaoCondicionantes">

                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title">
                                    <i class="fa fa-list mr-3"></i>
                                    Condicionantes Cadastradas para Licença: LI0000/2014BR153 : Condicionantes
                                </h5>

                                <div class="card-tools">
                                    <button @click="gerarFormulario('adicionar-condicionantes')" type="button" class="btn btn-tool" data-toggle="modal" data-target=".cadastro-condicionantes">
                                        <i class="fas fa-upload"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>

                                    <!-- botão expandir -->
                                    <button type="button" class="btn btn-tool" v-if="vmGlobal.expandSection === ''" @click="vmGlobal.expandirCards('condicionantes') ">
                                        <i class="fas fa-expand"></i></button>
                                    <button type="button" class="btn btn-tool" v-else @click="vmGlobal.expandirCards('') ">
                                        <i class="fas fa-expand"></i></button>
                                    <!-- botão expandir -->

                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped pt-5" id="cadastroCondicionantes">
                                    <thead>
                                        <tr>
                                            <th class="w-10" data-sort="false">
                                                <div class="row mr-5">Ações</div>
                                            </th>
                                            <th>ID</th>
                                            <th>Numero Condicional</th>
                                            <th>Nome</th>
                                            <th>Tipo</th>
                                            <th>Sub Tipo</th>
                                            <th>Tema</th>
                                            <th>Nome DNIT</th>
                                            <th>Periodicidade</th>
                                            <th>Prazo</th>
                                            <th>Responsável</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i = 1; $i < 10; $i++) { ?>
                                            <tr>
                                                <td>
                                                    <button type="button" class="btn btn-outline-secondary btn-sm" @click="expandCond= ''" data-toggle="modal" data-target=".bd-example-modal-xl"><i class="far fa-edit"></i></button>
                                                    <button type="button" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></button>

                                                </td>
                                                <td><?= $i ?></td>
                                                <td><?= $i ?></td>
                                                <td>Sub <?= $i ?>.1</td>
                                                <td>Programa</td>
                                                <td>SubPrograma</td>
                                                <td>Fauna</td>
                                                <td>Padrão <?= $i ?></td>
                                                <td>Periódico <?= $i ?></td>
                                                <td>Antes da LO</td>
                                                <td>Nome <?= $i ?> Responsável</td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                        <div class="card" :class="expandCond">
                            <div class="card-header">
                                <h3 class="card-title"><i class="fa fa-list mr-3"></i> Lista de Recursos Humanos Autorizados (Quando Necessário)</h3>

                                <div class="card-tools">

                                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>

                                    <!-- botão expandir -->
                                    <button type="button" class="btn btn-tool" v-if="expandListRec === ''" @click="expandListRec= 'card full-card' ">
                                        <i class="fas fa-expand"></i></button>
                                    <button type="button" class="btn btn-tool" v-else @click="expandListRec= '' ">
                                        <i class="fas fa-expand"></i></button>
                                    <!-- botão expandir -->

                                </div>
                            </div>
                            <div class="card-body">

                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>C.P.F</th>
                                            <th>CTF</th>
                                            <th>Profissão</th>
                                            <th>Registro Conselho de Classe</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1248</td>
                                            <td>01454656</td>
                                            <td>213546</td>
                                            <td>Arquólogo</td>
                                            <td>44565DF</td>
                                            <td>
                                                <button class="btn btn-outline-primary btn-sm"><i class="fa fa-times"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>

                    </div>
                    <!-- Fim da Listagem 2 -->
                    <!-- formulario para dicionar Licenças e Autorizações-->
                    <?php $this->load->view('cgmab/licencasAmbientais/forms/addLicencasAutorizacoes.php'); ?>
                    <!-- fim formulario para dicionar Licenças e Autorizações -->

                    <!-- Formulário para cadastrar condicionantes-->
                    <?php $this->load->view('cgmab/licencasAmbientais/forms/addCondicionantes.php'); ?>
                    <!-- fim Formulário para cadastrarcondicionantes-->

                    <!-- Formulário para Editar cadastro Licenças-->
                    <!-- fim Formulário para Editar cadastro Licenças-->

                    <!-- layers mapa  -->
                    <div class="row menu-layer-mapa" v-show="secaoAddLicencas === false">
                        <div class="col-12 mb-2">
                            <button @click="setGet('SituacaoGeo=Regular'), getStatus = '', uf = ''" class="btn btn-outline-secondary btn-sm" :class="showFiltro ? 'active' : ''"><i @click="showFiltro = !showFiltro" class="fas fa-filter"></i></button>
                        </div>
                        <div class="col-12 mb-2" v-show="showFiltro">
                            <button @click="setGet('SituacaoGeo=Regular&SituacaoAtual=Ativa')" @mouseover="exibirLegenda('la')" @mouseout="exibirLegenda('')" class="btn btn-sm btn-primary"><i class="fas fa-sticky-note"></i></button>
                            <div v-show="showLegenda === 'la'">Licenças Ativas</div>
                        </div>
                        <div class="col-12 mb-2" v-show="showFiltro">
                            <button @click="setGet('SituacaoGeo=Regular&SituacaoAtual=Inativa')" @mouseover="exibirLegenda('li')" @mouseout="exibirLegenda('')" class="btn btn-sm btn-primary"><i class="far fa-sticky-note"></i></button>
                            <div v-show="showLegenda === 'li'">Licenças Inativas</div>
                        </div>
                        <div class="col-12 mb-2" v-show="showFiltro">
                            <button @click="setGet('SituacaoGeo=Regular&SituacaoAtual=Renovadas')" @mouseover="exibirLegenda('lrn')" @mouseout="exibirLegenda('')" class="btn btn-sm btn-primary"><i class="fab fa-researchgate"></i></button>
                            <div v-show="showLegenda === 'lrn'">Licenças Renovadas</div>
                        </div>
                        <div class="col-12 mb-2" v-show="showFiltro">
                            <button @click="setGet('SituacaoGeo=Regular&SituacaoAtual=Retificadas')" @mouseover="exibirLegenda('lrt')" @mouseout="exibirLegenda('')" class="btn btn-sm btn-primary"><i class="far fa-plus-square"></i></button>
                            <div v-show="showLegenda === 'lrt'">Licenças Retificadas</div>
                        </div>
                        <div class="col-12 mb-2" v-show="showFiltro">
                            <button id="btnGroupDrop1" type="button" class="btn btn-danger btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="far fa-question-circle"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="background-color: rgba(255, 255, 255, 0.81);top: 37px !important;left: -54px!important;">
                                <button @click="setGetStatus('Status=Vigente')" class="btn btn-sm btn-success w-100 mb-2"><i class="far fa-calendar-check"></i> Vigente</button>
                                <button @click="setGetStatus('Status=Expirada')" class="btn btn-sm btn-danger w-100 mb-2"><i class="far fa-calendar-minus"></i> Expirada</button>
                                <button @click="setGetStatus('Status=Sem Efeito')" class="btn btn-sm btn-secondary w-100"><i class="far fa-calendar-times"></i> Sem Efeito</button>
                            </div>

                        </div>
                        <div class="col-12 mb-2" v-show="showFiltro">
                            <button id="btnGroupDrop1" type="button" class="btn btn-warning btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="fas fa-map-marker-alt"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="background-color: rgba(255, 255, 255, 0.81);heigh: 400px; overflow: auto">

                                <button @click="setUf('UF=' + i.nome)" class="btn btn-sm btn-outline-dark w-25 mb-2" v-for="i in listaUf">{{i.nome}}</button>

                            </div>

                        </div>

                    </div>

                    <!-- layers mapa  -->
                </div>
            </div>
        </div>
    </div>
</div>
</div>







<?php $this->load->view('cgmab/licencasAmbientais/script.php'); ?>