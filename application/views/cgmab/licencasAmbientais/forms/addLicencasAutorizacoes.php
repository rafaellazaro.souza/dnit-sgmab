<div class="content mb-5" v-show="secaoAddLicencas">
    <div class="card" :class="expandAddLicencas">

        <div class="card-body p-4">

            <h4>{{idLicenca == 0 ? 'Adicionar Licença' : 'Atualizar licença'}} </h4>
            <!-- form add ou editar licencas --->
            <form id="form-add-licenca" class="p-4">
                <div class="row">

                    <div class="alert alert-warning p-4 w-100 mb-4" v-show="errosInsertLicensa.length > 0">
                        <h4>Atenção! Preencha os campos abaixo:</h4>
                        <ul>
                            <li v-for="i in errosInsertLicensa" v-text="i"></li>
                        </ul>
                    </div>

                    <div class="form-group col-12 col-sm-4 col-md-6 col-xl-2">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Modulo
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" @change="removeDados($event)" v-model="novaLicenca.Modal">
                                <option :value="'Aquaviario'">Aquaviário</option>
                                <option :value="'Ferroviario'">Ferroviário</option>
                                <option :value="'Rodoviario'">Rodoviário</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-md-6 col-xl-2">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    UF
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2l" @change="getRodovia($event)" v-model="novaLicenca.CodigoUF">
                                <option v-for="i in listaUf" :value="i.CodigoUF" v-bind="novaLicenca.CodigoUF">{{i.nome}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-xl-2" v-show="novaLicenca.Modal != 'Aquaviario' && novaLicenca.Modal != 'Ferroviario'">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    BR
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.CodigoVia">
                                <option v-for="i in listaRodovia" :value="i.CodigoRodovia">{{i.Rodovia}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-12 col-sm-6 col-md-6 col-xl-2" v-if="novaLicenca.Modal != 'Aquaviário'">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Km Inicial

                                </div>
                            </div>
                            <input type="number" class="form-control" v-model="novaLicenca.KmInicial">
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6  col-xl-2" v-if="novaLicenca.Modal != 'Aquaviário'">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Km Final

                                </div>
                            </div>
                            <input type="number" class="form-control" v-model="novaLicenca.KmFinal" @blur="calcularExtensao()">
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Número da Licença
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.NumeroLicenca">
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Tipo de Licença
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.CodigoLicencaTipo">
                                <option v-for="i in listaTiposContratos" :value="i.CodigoLicencaTipo">{{i.Sigla}}</option>
                            </select>
                        </div>

                    </div>



                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Início do SubTrecho
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.InicioDoSubTrecho">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Fim do SubTrecho
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.FimDoSubTrecho">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Empreendimento
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.CodigoEmpreendimento">
                                <option :value="i.CodigoEmpreendimento" v-for="i in listaEmpreendimentos">{{i.Sigla}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Órgão Expeditor

                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.CodigoOrgaoExpeditor">
                                <option v-for="i in listaOrgaosExpeditor" :value="i.CodigoOrgaoExpeditor">{{i.NomeOrgaoExpeditor}}</option>

                            </select>
                        </div>

                    </div>

                    <div class="form-group col-12 col-md-4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Município
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.Municipio">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Processo Administrativo Interveniente
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.ProcessoAdministrativoInterveniente">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Ação
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.Acao">
                                <option :value="'Em andamento'">Em andamento</option>
                                <option :value="'Em Trâmite'">Em Trâmite</option>
                                <option :value="'Solicitação de Renovação'">Solicitação de Renovação</option>
                                <option :value="'Solicitação de Retificação'">Solicitação de Retificação</option>

                            </select>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Situação Atual
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.SituacaoAtual">
                                <option :value="'Ativa'">Ativa</option>
                                <option :value="'Inativa'">Inativa</option>
                                <option :value="'Renovada'">Renovada</option>
                                <option :value="'Retificada'">Retificada</option>
                                <option :value="'RetificaaoRenovacao'">Retificação de Renovação</option>

                            </select>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Status
                                </div>
                            </div>
                            <select class="form-control show-tick ms select2" v-model="novaLicenca.Status">
                                <option :value="'Vigente'">Vigente</option>
                                <option :value="'Expirada'">Expirada</option>
                                <option :value="'Sem Efeito'">Sem Efeito</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Prazo Validade (anos)
                                </div>
                            </div>
                            <input type="number" class="form-control" v-model="novaLicenca.PrazoValidade">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Processo SEI
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.ProcessoSei">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Link para Processo SEI
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.LinkProcessoSei">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Número do Processo SEI
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.NumeroProcessoSei">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Link Número Processo SEI
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.LinkNumeroSei">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Data de Emissão
                                </div>
                            </div>
                            <input type="text" class="form-control" @blur="calcularVencimento()" maxlength="10" onkeypress="formatar(this, '00/00/0000')" v-model="novaLicenca.DataEmissao">
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Data de Vencimento
                                </div>
                            </div>
                            <input type="text" class="form-control" maxlength="10" onkeypress="formatar(this, '00/00/0000')" v-model="novaLicenca.DataVencimento" disabled>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Objeto
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.Objeto">
                        </div>
                    </div>
                    <div v-show="novaLicenca.CodigoLicencaTipo == 6" class="form-group col-12 col-md-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Volume m³
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.Volume">
                        </div>
                    </div>
                    <div v-show="novaLicenca.CodigoLicencaTipo == 6" class="form-group col-12 col-md-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Área em APP (ha)
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.AreaEmApp">
                        </div>
                    </div>
                    <div v-show="novaLicenca.CodigoLicencaTipo == 6" class="form-group col-12 col-md-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Área fora APP (ha)
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.AreaForaApp">
                        </div>
                    </div>
                    <div v-show="novaLicenca.CodigoLicencaTipo == 6" class="form-group col-12 col-md-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Área Total (ha)
                                </div>
                            </div>
                            <input type="text" class="form-control" v-model="novaLicenca.AreaTotal">
                        </div>
                    </div>
                    <div v-show="(novaLicenca.SituacaoGeo == 'Pendência' || !novaLicenca.Coordenada) && novaLicenca.Coordenada == null" class="input-group col-12 mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Georreferenciamento da Licença (Shapefile)</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" ref="Shapefile" name="Shapefile" class="custom-file-input" id="Shapefile" accept=".zip">
                            <label class="custom-file-label font-italic" id="labelShapefile" for="Shapefile" style="background-color: #f5f5f5">Selecione um arquivo ZIP</label>
                        </div>
                    </div>
                    <!--                    <div class="form-group col-12 col-md-6">-->
                    <!--                        <div class="input-group">-->
                    <!--                            <div class="input-group-prepend">-->
                    <!--                                <div class="input-group-text">-->
                    <!--                                    Documento da Licença-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                            <input type="file" class="form-control" v-model="novaLicenca.NomeArquivoShapeFile">-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <div class="form-group col-12 ">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Situação do Cadastro
                                </div>
                            </div>
                            <template v-if="parseInt(vmGlobal.dadosDoUsuario.CodigoPerfil) == 1 || parseInt(vmGlobal.dadosDoUsuario.CodigoPerfil) == 5">
                                <select class="form-control show-tick ms select2" v-model="novaLicenca.Validada">
                                    <option value="">Selecione</option>
                                    <option value="1">Validado</option>
                                    <option value="0">Não Validado</option>
                                </select>
                            </template>

                            <template v-else>
                                <div v-if="parseInt(novaLicenca.Validada) == 0" class="form-control">Pendende Validação Fiscal</div>
                                <div v-else class="form-control">Validada</div>
                                <input type="hidden" v-model="novaLicenca.Validada = 0">
                            </template>

                        </div>
                    </div>
                    <div class="form-group col-12 col-md-12">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    Observações
                                </div>
                            </div>
                            <textarea class="form-control" v-model="novaLicenca.Observacoes"></textarea>
                        </div>
                    </div>
                    <div class="card col-6" style="min-height: auto;" v-if="idLicenca != 0">
                        <div class="header">
                            <h2><strong></strong> Arquivo de Licença <i class="fas fa-archive"></i></h2>
                        </div>
                        <div class="body border border-secondary">
                            <div class="form-group">
                                <input type="file" class="dropify" ref="licencaDoc">
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        Link para Sharepoint
                                    </div>
                                </div> <input type="text" class="form-control" v-model="arquivoLicenca.NomeArquivoLicencas">
                            </div>

                            <div class="form-group mt-2">
                                <label for="">Qual arquivo utilizar?</label><br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" v-model="arquivoLicenca.UsarLincencaAnexo" id="inlineRadio1" value="0">
                                    <label class="form-check-label" for="inlineRadio1">Usar Link Sharepoint</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" v-model="arquivoLicenca.UsarLincencaAnexo" id="inlineRadio2" value="1">
                                    <label class="form-check-label" for="inlineRadio2">Usar Arquivo Anexo</label>
                                </div>
                            </div>

                            <button class="btn btn-primary" @click="salvarArquivoLicenca()" type="button">Salvar</button>
                        </div>
                    </div>

                    <div class="col-12 text-right">
                        <button v-if="idLicenca != 0" @click="refTabelas()" class="btn btn-outline-info float-right" type="button">Finalizar</button>
                        <button type="button" @click="salvarLicenca()" type="button" class="btn btn-outline-success float-right"><span class="spinnerShapefile"></span> {{ idLicenca == 0 ? 'Salvar nova licença' : 'Atualizar licença'}}</button>
                        <button v-if="novaLicenca.Coordenada != null" @click="deletarShapefile()" class="btn btn-outline-danger float-right" type="button">Excluir Shapefile</button>

                    </div>

                </div>
            </form>
            <!-- form add ou editar licencas --->
            <div class="mt-3 mb-3">
                <div class="no-shadow body">
                    <div class="border border-secondary" id="mapidLicencas" style="height: 600px; width: 100%"></div>
                </div>
            </div>


        </div>

        <!-- /.card-footer-->
    </div>


</div>