<script>
	var base_url = '<?php echo base_url() ?>';

	if (location.search.slice(1)) {
		var renderizaNovaLicenca = vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[0].split('=')[1], 'decode');
	}

	licenciamentoAmbiental = new Vue({
		el: "#licenciamentoAmbiental",
		data: {
			get: 'SituacaoGeo=Regular',
			getStatus: '',
			errosInsertLicensa: [],
			showTabela: false,
			showMapa: true,
			showFiltro: false,
			showLegenda: '',
			showImportar: false,
			tabela: [],
			linhasUploadLicencas: false,
			dadosExcel: '',
			mymap: null,
			layer: null,
			uf: '',
			Status: '',
			SituacaoAtual: '',
			CodigoLicencaGeo: '',
			tituloModal: '',
			expandMapa: '',
			expandCadLicencas: '',
			expandAddLicencas: '',
			expandCond: '',
			expandListRec: '',
			secaoMapa: true,
			secaoLicencas: false,
			secaoDadosDaLicenca: false,
			secaoCondicionantes: false,
			secaoAddLicencas: false,
			novaLicenca: {},
			dadosDaLicensa: [],
			listaTiposContratos: [],
			listaOrgaosExpeditor: [],
			listaRodovia: [],
			listaUf: [],
			listaEmpreendimentos: [],

			coordenadasShapefile: {},
			idLicenca: 0,
			mymapNew: null,
			layerNew: null,
			idCodigoLicenca: 0,
			viewLicencas: [],
			arquivoLicenca: {
				UsarLincencaAnexo: 0,
				NomeArquivoLicencas: '',
				NomeLicencaAnexo: ''
			}
		},
		async mounted() {
			await this.gerarMapa();
			await this.getLicencas();
			await this.getUf();
			await this.rinderizaLicenca();
			await vmGlobal.dadosUsuario();
			await this.getAllLicencas();
			if (renderizaNovaLicenca) {
				await this.gerarMapaLicencas();
			}
		},
		computed: {},
		watch: {
			get: async function(val) {
				$('#cadastroLicencasVertical').DataTable().destroy();
				this.layer.clearLayers();
				await this.getAllLicencas();
			},
			getStatus: async function(val) {
				$('#cadastroLicencasVertical').DataTable().destroy();
				this.layer.clearLayers();
				await this.getAllLicencas();
			},
			uf: async function(val) {
				$('#cadastroLicencasVertical').DataTable().destroy();
				this.layer.clearLayers();
				await this.getAllLicencas();
			},
			secaoDadosDaLicenca: function(val) {
				this.secaoDadosDaLicenca = val;
				// this.getLi
			},
			'novaLicenca.NomeArquivoLicencas'(val) {
				if (val) this.arquivoLicenca.NomeArquivoLicencas = val;
			},
			'novaLicenca.NomeLicencaAnexo'(val) {
				if (val) this.arquivoLicenca.NomeLicencaAnexo = val;
			},
			'novaLicenca.UsarLincencaAnexo'(val) {
				if (val) this.arquivoLicenca.UsarLincencaAnexo = val;
			},

		},
		methods: {
			setGet(val) {
				this.get = val;
			},
			setGetStatus(val) {
				this.getStatus = val;
			},
			setUf(val) {
				this.uf = val;
			},
			setsecaoDadosDaLicenca(val) {
				this.secaoDadosDaLicenca = val;
			},


			gerarMapa() {
				const style = 'reduced.night';
				this.mymap = L.map('mapid').setView(['-10.6007767', '-63.6037797'], 4.5);
				L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
					maxZoom: 18,
					// attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
					//     '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
					id: 'mapbox.streets',
					fadeAnimation: true,
				}).addTo(this.mymap);
			},

			gerarMapaLicencas() {
				if (this.mymapNew) this.mymapNew.remove();
				setTimeout(() => {
					const style = 'reduced.night';
					this.mymapNew = L.map('mapidLicencas').setView(['-10.6007767', '-63.6037797'], 4.5);
					L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
						maxZoom: 18,
						// attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
						//     '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
						id: 'mapbox.streets',
						fadeAnimation: true,
					}).addTo(this.mymapNew);
					this.layerNew = L.geoJSON().addTo(this.mymapNew);
				}, 1000);
			},

			aplicarLayer(coord, objectid, color, layer) {
				var array = {
					"type": "FeatureCollection",
					"features": [{
						"type": "Feature",
						"geometry": JSON.parse(coord),
						"properties": {
							"OBJECTID": objectid,

						}
					}]
				}
				L.geoJson(array, {
					style: function(feature) {
						return {
							stroke: true,
							color: color,
							weight: 5
						};
					},
					onEachFeature: function(feature, l) {
						l.bindPopup(feature.properties.OBJECTID);

						l.on('click', function(e) {
							l.setStyle({
								weight: 12,
								outline: 'red'
							});
						});

						l.on("popupclose", function(e) {
							l.setStyle({
								weight: 5,
							});
						});


					}
				}).addTo(layer)
			},

			async homeMap() {
				this.showTabela = '';
				this.showMapa = true;
				this.secaoMapa = true;
				this.secaoAddLicencas = false;
				this.secaoLicencas = false
			},

			tabelaLicencas() {
				this.secaoDadosDaLicenca = false;
				this.showMapa = '';
				this.showTabela = true;
				this.secaoMapa = false;
				this.secaoAddLicencas = false;
				this.secaoLicencas = true
				if (this.layerNew) {
					this.layerNew.clearLayers();
				}
			},
			async iniciarDadosAdd(idLicenca) {

				this.showTabela = '',
					this.showMapa = '',
					this.secaoAddLicencas = true,
					this.secaoMapa = false,
					this.secaoLicencas = false,
					this.novaLicenca = {},
					this.idLicenca = idLicenca;
				this.idCodigoLicenca = idLicenca;

				await this.gerarMapaLicencas();
				await this.getTiposContratos();
				await this.getUf();
				await this.getOrgaosExpeditores();

				if (this.novaLicenca.Coordenada) {
					this.layerNew.clearLayers();
				}
				$("#Shapefile").val(null);
				$("#labelShapefile").html('Selecione um arquivo ZIP');

				//Editar
				if (idLicenca > 0) {
					var params = {
						table: 'viewLicencas',
						where: {
							CodigoLicenca: idLicenca
						}
					}
					var dadosLicenca = await vmGlobal.getFromAPI(params, 'cgmab');
					this.novaLicenca = dadosLicenca[0];
					this.novaLicenca.DataVencimento = await vmGlobal.frontEndDateFormat(String(this.novaLicenca.DataVencimento));
					this.novaLicenca.DataEmissao = await vmGlobal.frontEndDateFormat(String(this.novaLicenca.DataEmissao));

					setTimeout(() => {
						let data = this.novaLicenca.Coordenada;
						let coordenadaShapefile = JSON.parse(data);
						let concatenado = 0;
						let corLayer = '#000';
						if (coordenadaShapefile != null) {
							if (Array.isArray(coordenadaShapefile)) {
								for (let index = 0; index < coordenadaShapefile.length; index++) {
									let cd = coordenadaShapefile[index].coordenada

									this.aplicarLayer(cd, concatenado, corLayer, this.layerNew);
								}
							} else {
								let cd = coordenadaShapefile.Coordenada;
								this.aplicarLayer(cd, concatenado, corLayer, this.layerNew);
							}
						}
					}, 900);
					await this.setUF(this.novaLicenca.CodigoUF);

				}
			},

			async getLicencas() {
				await axios.get(base_url + 'LicenciamentoAmbiental/getLicencas?' + this.get + '&' + this.getStatus + '&' + this.uf)
					.then((resp) => {
						this.tabela = resp.data;
					})
					.catch((e) => {
						console.log(e)
					})
					.finally(() => {
						this.layer = L.geoJSON().addTo(this.mymap);
						this.layer.clearLayers();
						var corLayer = '';
						for (i = 0; i < this.tabela.length; i++) {
							if (this.tabela[i].Status === 'Expirada')
								corLayer = '#e90000';
							if (this.tabela[i].Status === 'Vigente')
								corLayer = '#00b21e';
							if (this.tabela[i].Status === 'Sem Efeito')
								corLayer = '#7a7a7a';

							var concatenado = '<div class="text-left" style="color: #888"><h6>INFORMAÇÕES DA LICENÇA</h6> <br>' +
								'<b>Numero da Licença</b>: ' + this.tabela[i].NumeroLicenca + '<br>' +
								'<b>Órgão Expeditor</b>: ' + this.tabela[i].NomeOrgaoExpeditor + '<br>' +
								this.tabela[i].TipoVia + ' - ' + this.tabela[i].VIA + ' - ' + this.tabela[i].UF + '<br>' +
								'<b>Tipo de Licença</b>: ' + this.tabela[i].Sigla + ' (' + this.tabela[i].TituloLicenca + ') ' + '<br>' +
								'<b>Status</b>: ' + this.tabela[i].Status + '<br>' +

								'<button class="mt-4 btn btn-sm btn-outline-primary" onclick="abrirInformacoesLicens(' + this.tabela[i].CodigoLicenca + ')">Ver Detalhes</button>';

							let data = this.tabela[i].Coordenada;
							let coordenadaShapefile = JSON.parse(data);

							if (coordenadaShapefile != null) {
								if (Array.isArray(coordenadaShapefile)) {
									for (let index = 0; index < coordenadaShapefile.length; index++) {
										let cd = coordenadaShapefile[index].coordenada

										this.aplicarLayer(cd, concatenado, corLayer, this.layer);
									}
								} else {
									let cd = this.tabela[i].Coordenada;
									this.aplicarLayer(cd, concatenado, corLayer, this.layer);
								}
							}
						}

						// $(".lock").addClass('invisible');
						// vmGlobal.montaDatatable('#cadastroCondicionantes');
						// vmGlobal.montaDatatable('#cadastroLicencasVertical');
					})
			},

			async getAllLicencas() {
				var params = {
					table: 'viewLicencas',
				}
				this.viewLicencas = await vmGlobal.getFromAPI(params, 'cgmab');

				$(".lock").addClass('invisible');
				vmGlobal.montaDatatable('#cadastroCondicionantes');
				vmGlobal.montaDatatable('#cadastroLicencasVertical');
			},

			async getInformacaoLicensa(IdLicensa) {
				var params = {
					table: 'viewLicencas',
					where: {
						CodigoLicenca: IdLicensa
					}
				}
				this.dadosDaLicensa = await vmGlobal.getFromAPI(params, 'cgmab');
				this.setsecaoDadosDaLicenca(true);
			},
			gerarFormulario(param) {

				if (param === 'adicionar-licencas') {
					this.tituloModal = 'Adicionar Licença';
					$("#form-add-licenca").removeClass('invisivel');
					$("#form-add-nova-licenca").addClass('invisivel');
				} else if (param === 'editar-licencas') {
					this.tituloModal = "Editar Licença";
					$("#form-add-licenca").removeClass('invisivel');
				} else if (param === 'adicionar-nova-licencas') {
					this.tituloModal = "Cadastro de Licença/ Autorização";
					$("#form-add-licenca").addClass('invisivel');
					$("#form-add-orgao").addClass('invisivel');
					$("#form-add-nova-licenca").removeClass('invisivel');

				} else if (param === 'adicionar-orgao') {
					this.tituloModal = "Cadastro de Órgão ";
					$("#form-add-nova-licenca").addClass('invisivel');
					$("#form-add-orgao").removeClass('invisivel');
				}

			},
			validarCadastroLicensa(e) {
				if (
					this.novaLicenca.Modal &&
					this.novaLicenca.NumeroLicenca &&
					this.novaLicenca.DataEmissao &&
					this.novaLicenca.DataVencimento &&
					// this.novaLicenca.NumeroProcessoSei &&
					this.novaLicenca.CodigoLicencaTipo &&
					this.novaLicenca.CodigoOrgaoExpeditor
				) {
					return true;
				}

				this.errosInsertLicensa = [];
				$('.spinnerShapefile').removeClass('spinner-border spinner-border-sm');

				if (!this.novaLicenca.Modal) {
					this.errosInsertLicensa.push('Modulo é obrigatório.');
				}
				if (!this.novaLicenca.NumeroLicenca) {
					this.errosInsertLicensa.push('Número da licença é obrigatória.');
				}
				if (!this.novaLicenca.DataEmissao) {
					this.errosInsertLicensa.push('Data de Emissão é obrigatória.');
				}
				if (!this.novaLicenca.DataVencimento) {
					this.errosInsertLicensa.push('Data de Vencimento é obrigatória.');
				}
				// if (!this.novaLicenca.NumeroProcessoSei) {
				// 	this.errosInsertLicensa.push('Número do processo SEI é obrigatória.');
				// }
				if (!this.novaLicenca.CodigoLicencaTipo) {
					this.errosInsertLicensa.push('Tipo de Licença é obrigatória.');
				}
				if (!this.novaLicenca.CodigoOrgaoExpeditor) {
					this.errosInsertLicensa.push('Órgão Expeditor é obrigatória.');
				}
				e.preventDefault();
			},

			async salvarLicenca() {

				//converter datas pt > eng
				this.novaLicenca.DataEmissao = await vmGlobal.backEndDateFormat(String(this.novaLicenca.DataEmissao));
				this.novaLicenca.DataVencimento = await vmGlobal.backEndDateFormat(String(this.novaLicenca.DataVencimento));
				const codigoUF = this.novaLicenca.CodigoUF;
				delete this.novaLicenca.CodigoUF;

				//verifica se denit inseriu nova licença
				if (vmGlobal.dadosUsuario.CodigoPerfil === 1 || vmGlobal.dadosUsuario.CodigoPerfil === '1') {
					this.novaLicenca.Validada = 1
				} else {
					this.novaLicenca.Validada = 0
				}

				//Processa arquivo Shapefile
				$('.spinnerShapefile').addClass('spinner-border spinner-border-sm');
				var myfiles = document.getElementById("Shapefile");
				var files = myfiles.files;
				var data = new FormData();
				var file = this.$refs.Shapefile.files[0];
				data.append('Coordenada', file);

				//processaZipShapefile
				this.layerNew.clearLayers();
				await this.processaShapefile(files, data, file);

				// valida campos obrigatorios
				let validar = this.validarCadastroLicensa();
				if (validar) {
					if (this.idLicenca == 0) {
						await this.registraLicenca();
					} else {
						await this.atualizaLicenca();
					}
				}

				this.novaLicenca.DataVencimento = await vmGlobal.frontEndDateFormat(String(this.novaLicenca.DataVencimento));
				this.novaLicenca.DataEmissao = await vmGlobal.frontEndDateFormat(String(this.novaLicenca.DataEmissao));
				$('.spinnerShapefile').removeClass('spinner-border spinner-border-sm');
				$("#ShapeFile").val(null);
				$("#labelShapeFile").html('Selecione um arquivo ZIP');
				this.novaLicenca.CodigoUF = codigoUF;

				//atualiza o map geral das licenças apos atualização
				this.layer.clearLayers();
				await this.getLicencas();

			},

			async processaShapefile(files, data, file) {
				if (file != undefined) {
					await $.ajax({
						url: '<?= base_url('LicenciamentoAmbiental/postShapefile') ?>',

						method: 'POST',
						data: data,
						contentType: false,
						processData: false,
						success: function(data) {
							licenciamentoAmbiental.coordenadasShapefile = JSON.parse(data);
							const numCoordenadas = licenciamentoAmbiental.coordenadasShapefile.length
							const concatenado = 0;
							const corLayer = '#000';
							for (let index = 0; index < numCoordenadas; index++) {
								var cd = licenciamentoAmbiental.coordenadasShapefile[index].coordenada
								licenciamentoAmbiental.aplicarLayer(cd, concatenado, corLayer, licenciamentoAmbiental.layerNew);
							}
						},
						error: function(error) {
							console.log('error', error);
							Swal.fire({
								position: 'center',
								type: 'error',
								title: 'Erro!',
								text: "Erro ao tentar salvar arquivo. " + e,
								showConfirmButton: true,

							});
						},
						complete: function() {
							$('.spinnerShapefile').addClass('spinner-border spinner-border-sm');
						}
					})

					this.novaLicenca.Coordenada = JSON.stringify(this.coordenadasShapefile);
					this.novaLicenca.SituacaoGeo = 'Regular';
				}
			},

			async registraLicenca() {
				if (this.novaLicenca.Coordenada == undefined) {
					console.log('aqui');
					this.novaLicenca.SituacaoGeo = 'Pendência';
				}

				//grava
				await axios.post(base_url + 'LicenciamentoAmbiental/postNovaLicenca', $.param(this.novaLicenca))
					.then((response) => {
						if (response.data.status === true) {
							//ID do registro
							this.idLicenca = response.data.result
							Swal.fire({
								position: 'center',
								type: 'success',
								title: 'Salvo!',
								text: 'Dados inseridos com sucesso.',
								showConfirmButton: true,
							});
						} else {
							Swal.fire({
								position: 'center',
								type: 'error',
								title: 'Erro!',
								text: "Erro ao tentar salvar arquivo. ",
								showConfirmButton: true,

							});
						}
					})
					.catch((error) => {
						console.log(error);
					})
					.finally(() => {

					});
			},

			refTabelas() {
				this.novaLicenca = {};
				this.secaoAddLicencas = false;
				this.secaoLicencas = true;
			},

			async atualizaLicenca() {
				delete this.novaLicenca.CodigoLicenca;
				delete this.novaLicenca.NomeOrgaoExpeditor;
				delete this.novaLicenca.TipoVia;
				delete this.novaLicenca.VIA;
				delete this.novaLicenca.UF;
				delete this.novaLicenca.DataEmissaoF;
				delete this.novaLicenca.DataVencimentoF;
				delete this.novaLicenca.DiasVencimento;
				delete this.novaLicenca.TituloLicenca;
				delete this.novaLicenca.Sigla;


				var params = {
					table: 'licensasAmbientais',
					data: this.novaLicenca,
					where: {
						CodigoLicenca: this.idLicenca,
					}
				}

				await vmGlobal.updateFromAPI(params, 'cgmab');
				// await this.getLicencas();
				await this.iniciarDadosAdd(this.idLicenca);

			},

			async deletarShapefile() {

				let params = {
					table: 'licensasAmbientais',
					data: {
						Coordenada: null,
						SituacaoGeo: 'Pendência'
					},
					where: {
						CodigoLicenca: this.idLicenca
					}
				}
				await vmGlobal.updateFromAPI(params, null);
				await this.iniciarDadosAdd(this.idLicenca);
				this.layer.clearLayers();
				this.layerNew.clearLayers();
				await this.getLicencas();
			},

			async getTiposContratos() {
				var params = {
					table: 'tiposLicencas',
					where: {
						Status: 1
					}
				}
				this.listaTiposContratos = await vmGlobal.getFromAPI(params, 'cgmab');
			},
			async getOrgaosExpeditores() {
				var params = {
					table: 'orgaosExpeditores',
					where: {
						Status: 1
					}
				}
				this.listaOrgaosExpeditor = await vmGlobal.getFromAPI(params, 'cgmab');
			},
			async getUf() {
				var params = {
					table: 'BaseUF'
				}
				this.listaUf = await vmGlobal.getFromAPI(params, 'cgmab');
			},

			async getRodovia(event) {
				this.setUF(event.target.value);
			},

			async setUF(idUF) {
				var params = {
					table: 'BaseRodovia',
					where: {
						CodigoUF: idUF
					}
				}
				vmGlobal.attr.disabled = false;
				licenciamentoAmbiental.listaRodovia = await vmGlobal.getFromAPI(params, 'cgmab');
				this.getEmpreendimentos();
			},
			async getEmpreendimentos() {
				var params = {
					table: 'empreendimentos',
					where: {
						CodigoUF: this.novaLicenca.CodigoUF,
						Status: 1
					}
				}
				licenciamentoAmbiental.listaEmpreendimentos = await vmGlobal.getFromAPI(params, 'cgmab');
			},
			async importarLicencas() {

				//criar sessao com dados da condicionante
				//this.armazenarDadosCondicionante();

				//upload do arquivo excel
				this.file = this.$refs.file.files[0];

				let formData = new FormData();
				formData.append('file', this.file);

				await axios.post('<?php echo base_url('licenciamentoambiental/uploadCsv') ?>', formData, {
						headers: {
							'Content-Type': 'multipart/form-data'
						}
					})
					.then((response) => {
						this.dadosExcel = response.data
					})
					.catch((error) => {
						console.log(error);
					})
					.finally(() => {
						// this.upload = false;
						this.linhasUploadLicencas = true;
					});

			},
			calcularExtensao() {
				this.novaLicenca.Extensao = parseInt(this.novaLicenca.KmFinal) - parseInt(this.novaLicenca.KmInicial);
			},
			async calcularVencimento() {
				var data = this.novaLicenca.DataEmissao.split('/');
				var somaAnos = parseInt(data[2]) + parseInt(this.novaLicenca.PrazoValidade);
				this.novaLicenca.DataVencimento = data[0] + '/' + data[1] + '/' + somaAnos;
				// this.novaLicenca.DataVencimento = moment(new Date(String(this.novaLicenca.DataEmissao))).add(new Date(String(this.novaLicenca.PrazoValidade, "year")));
			},
			/*
			 * Função responsável por converter datas ENG > PTBR:
			 * DD/MM/YYYY = 1
			 * MM/YYYY = 2
			 *
			 */
			formatData(param, tipo) {

				if (tipo === 1) {
					return moment(String(param)).format('DD/MM/YYYY');
				} else {
					return moment(String(param)).format('MM/YYYY');
				}

			},
			exibirLegenda(legenda) {
				this.showLegenda = legenda
			},
			removeDados(event) {
				if (event.target.value === 'Aquaviário') {
					this.novaLicenca.CodigoUF = '';
					this.novaLicenca.CodigoRodovia = '';
					this.novaLicenca.KmInicial = '';
					this.novaLicenca.KmFinal = '';
				}

			},

			rinderizaLicenca() {
				if (renderizaNovaLicenca) {
					// setTimeout(() => {
					document.getElementById("addLicenca").click();
					// }, 900);
				}
			},

			async validarLicenca(idLicencia, status) {
				var params = {
					table: 'licensasAmbientais',
					data: {
						Validada: status,
					},
					where: {
						CodigoLicenca: idLicencia,
					}

				}

				await vmGlobal.updateFromAPI(params, 'cgmab');



				this.getAllLicencas();
			},

			async salvarArquivoLicenca() {
				var arquivoAnterior = this.arquivoLicenca.NomeLicencaAnexo;
				var licenca = Object.assign({}, this.arquivoLicenca);
				var file = null;

				if (this.$refs.licencaDoc.files.length != 0) {
					var file = {
						name: 'NomeLicencaAnexo',
						value: this.$refs.licencaDoc.files[0]
					}

					licenca['FilesRules'] = {
						upload_path: 'webroot/uploads/anexos_licencas',
						allowed_types: 'pdf',
					};

					licenca.NomeLicencaAnexo = base_url + 'webroot/uploads/anexos_licencas/' + this.$refs.licencaDoc.files[0].name;
				}

				var params = {
					table: 'licensasAmbientais',
					data: licenca,
					where: {
						CodigoLicenca: this.novaLicenca.CodigoLicenca
					}
				};

				await vmGlobal.updateFromAPI(params, file);

				if (arquivoAnterior.length != 0) {
					await vmGlobal.deleteFile(arquivoAnterior);
				}

				this.iniciarDadosAdd(this.novaLicenca.CodigoLicenca);
			}
		},

	});

	function abrirInformacoesLicens(IdLicensa) {
		licenciamentoAmbiental.getInformacaoLicensa(IdLicensa);
	}

	function formatar(src, mask) {
		var i = src.value.length;
		var saida = mask.substring(0, 1);
		var text = mask.substring(i);
		if (text.substring(0, 1) != saida) {
			src.value += text.substring(0, 1);
		}
	}

	$(document).on('change', '.custom-file-input', function(event) {
		$(this).next('.custom-file-label').html(event.target.files[0].name);
	})
</script>