<div class="container-fluid">
    <section class="content-header">

        <div class="row mt-4 mb-4">
            <div class="col-sm-6">
                <h1>Configurações</h1>
            </div>
            <div class="col-sm-6 text-right">
                <a href="<?php echo site_url('cgmab/home') ?>" class="ml-3"><i class="fa fa-times"></i></a>
            </div>

        </div>

    </section>


    <div class="row">
        <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    Informações Contratuais

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body h-450r">

                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href=" <?php echo site_url('cgmab/contratos/vercadastro/') ?>">Identificação do Contrato</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>

                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href=" <?php echo site_url('cgmab/infraestrutura/index/') ?>"> Infraestrutura</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php echo site_url('cgmab/recursoshumanos/') ?>">Recursos Humanos</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/recursoshumanos/') 
                                        ?> javascript:;">Escopo dos Serviços Contratados *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-success fa fa-check mr-3"></i>
                        </div>
                    </div>
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/recursoshumanos/') 
                                        ?> javascript:;">Acompanhamento dos Serviços Contratados *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-success fa fa-check mr-3"></i>
                        </div>
                    </div>
                   
                   
                   

                </div>
                <!-- /.card-body -->
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>

        <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    Caracterização do Empreendimento

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body h-450r">

                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/recursoshumanos/') 
                                        ?> javascript:;">Identificação das Construtoras e Supervisoras de Obras *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/recursoshumanos/') 
                                        ?> javascript:;">Caracterização dos Segmentos *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                  


                </div>
                <!-- /.card-body -->
               
            </div>
            <!-- /.card -->

        </section>

        <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    Atividades de Pré-Instalação

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body h-450r">

                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php echo site_url('cgmab/programasplanosambientais/') ?>">Descrição dos Programas e Planos Ambientais</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </section>

        <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    Cronograma de Gestão Ambiental
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body h-450r">
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php echo site_url('cgmab/cronogramas/fisicoprogramas') ?>">Cronograma Físico dos Programas</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/recursoshumanos/') 
                                        ?> javascript:;">Cronograma Financeiro *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </section>

        <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">
              <!-- Default box -->
              <div class="card">
                <div class="card-header">
                    Caracterização Geral
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body h-450r">
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/cronogramas/fisicoprogramas') ?>">Estrutura Viária *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/recursoshumanos/') 
                                        ?> javascript:;">Condições de Trafegabilidade *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </section>

        <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">
              <!-- Default box -->
              <div class="card">
                <div class="card-header">
                   Análise da Paisagem
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body h-450r">
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/cronogramas/fisicoprogramas') ?>">Características dos Recursos Hídricos *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/recursoshumanos/') 
                                        ?> javascript:;">Caracterização da Geometria *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/recursoshumanos/') 
                                        ?> javascript:;">Caracterização da Geologia *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/recursoshumanos/') 
                                        ?> javascript:;">Caracterização do Uso de Ocupação do Solo *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </section>

        <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">
              <!-- Default box -->
              <div class="card">
                <div class="card-header">
                   Relatório Fotográfico
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body h-450r">
                    <div class="row border mb-3 p-2 btn-conf">
                        <div class="col-10">
                            <a href="<?php //echo site_url('cgmab/cronogramas/fisicoprogramas') ?>">Passivo Ambiental *</a>
                        </div>
                        <div class="col-2">
                            <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                        </div>
                    </div>
                    
                    

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </section>
    </div>
</div>