<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Configurações</h1>
            </div>

        </div>
    </div>
</section>


<div class="card">
    <!-- TABS-->
    <div class="card-header p-0">
        <!--        <h3 class="card-title p-3">Tabs</h3>-->
        <ul class="nav nav-pills ml-auto p-2">
            <li class="nav-item"><a class="nav-link active" href="#tab_inf_cont" data-toggle="tab">Informações Contratuais</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_carac_emp" data-toggle="tab">Caracterização do Empreendimento</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_pre_inst" data-toggle="tab">Atividades de Pré-instalação</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_cronograma_gest" data-toggle="tab">Cronograma de Gestão Ambiental</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_carac_geral" data-toggle="tab">Caracterização Geral</a></li>    
            <li class="nav-item"><a class="nav-link" href="#tab_analise_paisagem" data-toggle="tab">Análize da Paisagem</a></li>    
            <li class="nav-item"><a class="nav-link" href="#tab_rel_foto_inicio" data-toggle="tab">Relatório Fotográfico Antes do Início da Obra</a></li>

        </ul>
    </div><!-- /.card-header -->
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_inf_cont">
                <h4>Informações Contratuais</h4>
                <table class="table table-striped">
                    
                    <tbody>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Identificação do Contrato</td>

                            <td><span class="badge bg-danger">Pendência</span></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Escopo dos Serviços Contratados</td>

                            <td><span class="badge bg-success">Validado</span></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Acompanhamento de Contratos de Convênios</td>

                            <td><span class="badge bg-success">Validado</span></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Infraestrutura</td>

                            <td><span class="badge bg-success">Validado</span></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Recursos Humanos</td>

                            <td><span class="badge bg-danger">Pendência</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_carac_emp">
                <table class="table table-striped">
                    
                    <tbody>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Identificação das Construtoras e Supervisoras de Obras</td>

                            <td><span class="badge bg-danger">Pendência</span></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Caracterização dos Segmentos</td>

                            <td><span class="badge bg-success">Validado</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_pre_inst">
                <table class="table table-striped">
                    
                    <tbody>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Descricao dos Programas e Planos Ambientais</td>

                            <td><span class="badge bg-success">Pendência</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.tab-pane -->
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_cronograma_gest">
                 <table class="table table-striped">
                    
                    <tbody>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Cronograma Físico de Programas</td>

                            <td><span class="badge bg-danger">Pendência</span></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Cronograma Financeiro</td>

                            <td><span class="badge bg-danger">Validado</span></td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_carac_geral">
                <table class="table table-striped">
                    
                    <tbody>
                        
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Estrutura Viária</td>

                            <td><span class="badge bg-danger">Validado</span></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Condições de Trafegabilidade</td>

                            <td><span class="badge bg-danger">Validado</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="tab-pane" id="tab_analise_paisagem">
                <table class="table table-striped">
                    
                    <tbody>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Característica dos recursos Hídricos</td>

                            <td><span class="badge bg-danger">Pendência</span></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Caracterização da Geomofologia</td>

                            <td><span class="badge bg-danger">Pendência</span></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Caracterização da Geologia</td>

                            <td><span class="badge bg-danger">Pendência</span></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Caracterização do Uso e Ocupação do Solo</td>

                            <td><span class="badge bg-danger">Pendência</span></td>
                        </tr>
                       
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab_rel_foto_inicio">
                <table class="table table-striped">
                    
                    <tbody>
                        <tr>
                            <td><i class="fa fa-check mr-3"></i></td>
                            <td>Passivo Ambiental</td>

                            <td><span class="badge bg-danger">Pendência</span></td>
                        </tr>
                       
                    </tbody>
                </table>
            </div>
            <!-- /.tab-content -->
        </div><!-- /.card-body -->
    </div>


