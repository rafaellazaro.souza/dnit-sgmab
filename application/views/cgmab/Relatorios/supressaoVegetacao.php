<?php $this->load->view('cgmab/Relatorios/css/cssRelatoriosFauna.php') ?>

<div class="body_scroll">
	<div class="block-header">
		<div class="row">
			<div class="col-lg-7 col-md-6 col-sm-12">
				<h2>Relatório</h2>
				<ul class="breadcrumb mt-2">
					<li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a>
					</li>
					<li class="breadcrumb-item">Relatórios</li>
					<li class="breadcrumb-item active">Supressão de Vegetação</li>
				</ul>
				<button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div id="informacoesSupressaoVegetacao">
					<div class="header">
						<div class="row mb-2">
							<div class="col-12 col-sm-3">
								<div class="input-group mb-3 mt-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">Ano</span>
									</div>
									<div class="form-control form-header lh-40">{{anoAtividade}}</div>
								</div>
							</div>
							<div class="col-12 col-sm-3">
								<div class="input-group mb-3 mt-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">Mês</span>
									</div>
									<div class="form-control form-header lh-40">{{mesAtividade}}</div>
								</div>
							</div>
							<div class="col-sm-6 text-right">
								<div class="row mt-3">
									<div class="input-group mb-3 col-12 col-sm-6">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">Contrato</span>
										</div>
										<div class="form-control form-header lh-40">{{numeroContrato}}</div>
									</div>
									<div class="input-group mb-3 col-12 col-sm-6 ">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">Empresa</span>
										</div>
										<div class="form-control form-header lh-40">{{session.Logado.NomeContratada}} </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="body">
						<div class="row mb-4" style="display: none;">
							<div class="col-12">
								<h4 class="w-100 mb-4 border p-3">Relatório de Supressão de Vegetação</h4>
								<div class="text-right ">
									<button class="btn btn-success mt-2">Enviar para Validação</button>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">Prazo para envio:</li>
									<li class="list-group-item">Data de Envio:</li>
									<li class="list-group-item">Hora de Envio:</li>
									<li class="list-group-item">Colaborador:</li>
								</ul>
							</div>
						</div>
						<ul class="nav nav-tabs border-bottom" id="tabsSupressaoVegetacao" role="tablist">
							<li class="nav-item">
								<a class="nav-link border active" id="home-tab" data-toggle="tab" href="#introducaoSupressaoVegetacao" role="tab" aria-controls="introducaoSupressaoVegetacao" aria-selected="true">Introdução</a>
							</li>
							<li class="nav-item">
								<a @click="iniciarInsercaoDados" class="nav-link border" id="profile-tab" data-toggle="tab" href="#insercao-dados" role="tab" aria-controls="insercao-dados" aria-selected="false">Inserção de Dados de Supressão</a>
							</li>
							<li class="nav-item">
								<a class="nav-link border" id="controle-tab" data-toggle="tab" href="#controle-pilhas" role="tab" aria-controls="analise" aria-selected="false">Atualização do controle de Pilhas</a>
							</li>
							<li class="nav-item">
								<a @click="" class="nav-link border" id="analise-tab" data-toggle="tab" href="#analise-resultados" role="tab" aria-controls="analise-resultados" aria-selected="false">Análise de Resultados</a>
							</li>
							<li class="nav-item">
								<a @click="iniciarDiscussoesConclusoes()" class="nav-link border" id="contact-tab" data-toggle="tab" href="#discussoes-conclusao" role="tab" aria-controls="discussoes-conclusao" aria-selected="false">Discussões e Conclusões</a>
							</li>
							<li class="nav-item">
								<a class="nav-link border" id="recursos-tab" data-toggle="tab" href="#recursos-utilizados" role="tab" aria-controls="recursos-utilizados" aria-selected="false">Recursos Utilizados</a>
							</li>
						</ul>
						<div class="tab-content p-4" id="myTabContent">
							<div class="tab-pane fade show active" id="introducaoSupressaoVegetacao" role="tabpanel" aria-labelledby="home-tab">
								<?php $this->load->view('cgmab/Relatorios/paginas/relatorioSupressaoVegetacao/introducao'); ?>
							</div>
							<div class="tab-pane fade " id="insercao-dados" role="tabpanel" aria-labelledby="profile-tab">
								<?php $this->load->view('cgmab/Relatorios/paginas/relatorioSupressaoVegetacao/insercaoDados'); ?>
							</div>
							<div class="tab-pane fade" id="controle-pilhas" role="tabpanel" aria-labelledby="controle-tab">
								<?php $this->load->view('cgmab/Relatorios/paginas/relatorioSupressaoVegetacao/atualizacaoControlePilhas'); ?>
							</div>
							<div class="tab-pane fade" id="analise-resultados" role="tabpanel" aria-labelledby="analise-tab">
								<?php $this->load->view('cgmab/Relatorios/paginas/relatorioSupressaoVegetacao/analiseResultados'); ?>
							</div>
							<div class="tab-pane fade " id="discussoes-conclusao" role="tabpanel" aria-labelledby="contact-tab">
								<?php $this->load->view('cgmab/Relatorios/paginas/relatorioSupressaoVegetacao/discussoesConclusoes'); ?>
							</div>
							<div class="tab-pane fade " id="recursos-utilizados" role="tabpanel" aria-labelledby="recursos-tab">
								<?php $this->load->view('cgmab/Relatorios/paginas/relatorioSupressaoVegetacao/recursosUtilizados'); ?>
							</div>
						</div>
						<div id="relatorioMensal"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('cgmab/Relatorios/scripts/appSupressaoVegetacao'); ?>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioSupressaoVegetacao/appIntroducao'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioSupressaoVegetacao/appinsercaoDados'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioSupressaoVegetacao/appDiscussoesConclusoes'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioSupressaoVegetacao/appAnaliseResultado'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioSupressaoVegetacao/recursosUtilizados'); ?>

<?php $this->load->view('cgmab/Relatorios/template/card-fotos-template'); ?>

<script src="<?= base_url('webroot/arquivos/js/app/supresssao-vegetacao/card-fotos-component.js') ?>"></script>
<script src="<?= base_url('webroot/arquivos/js/app/supresssao-vegetacao/controle-pilhas.js') ?>"></script>