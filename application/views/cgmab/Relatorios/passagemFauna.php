<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Passagem de Fauna</h2>
                <ul class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item">Relatorios</li>
                    <li class="breadcrumb-item active">Passagem de Fauna</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">

                    </div>
                    <div class="body">
                        <ul class="nav nav-tabs" id="navegacao" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="introducao-tab" data-toggle="tab" href="#introducao" role="tab" aria-controls="home" aria-selected="true">Introdução</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a @click="iniciarInsercao()" class="nav-link" id="insercao-tab" data-toggle="tab" href="#insercao" role="tab" aria-controls="profile" aria-selected="false">Inserção de Dados</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a @click="iniciarProfissionais()" class="nav-link" id="profissionais-tab" data-toggle="tab" href="#profissionais" role="tab" aria-controls="contact" aria-selected="false">Recursos utilizados</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a @click="iniciarAnaliseGrupo()" class="nav-link" id="analise-tab" data-toggle="tab" href="#analise" role="tab" aria-controls="contact" aria-selected="false">Análise por Grupo</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="discussoes-tab" data-toggle="tab" href="#discussoes" role="tab" aria-controls="contact" aria-selected="false">Discussões e Conclusões</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="correlação-tab" data-toggle="tab" href="#correlação" role="tab" aria-controls="contact" aria-selected="false">Correlação de Programa de Atropelamento</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active p-3" id="introducao" role="tabpanel" aria-labelledby="introducao">
                                <?php $this->load->view('cgmab/Relatorios/paginas/relatorioPassagemFauna/introducao') ?>
                            </div>
                            <div class="tab-pane fade p-3" id="insercao" role="tabpanel" aria-labelledby="insercao">
                                <?php $this->load->view('cgmab/Relatorios/paginas/relatorioPassagemFauna/insercao') ?>
                            </div>
                            <div class="tab-pane fade p-3" id="profissionais" role="tabpanel" aria-labelledby="profissionais">
                                <?php $this->load->view('cgmab/Relatorios/paginas/relatorioPassagemFauna/profissionais') ?>
                            </div>
                            <div class="tab-pane fade p-3" id="analise" role="tabpanel" aria-labelledby="analise">
                                <?php $this->load->view('cgmab/Relatorios/paginas/relatorioPassagemFauna/analiseGrupo') ?>
                            </div>
                            <div class="tab-pane fade p-3" id="discussoes" role="tabpanel" aria-labelledby="discussoes">
                                <?php $this->load->view('cgmab/Relatorios/paginas/relatorioPassagemFauna/dicussoesConclusoes') ?>
                            </div>
                            <div class="tab-pane fade p-3" id="correlação" role="tabpanel" aria-labelledby="correlação">
                                <?php $this->load->view('cgmab/Relatorios/paginas/relatorioPassagemFauna/correlacao') ?>
                            </div>
                        </div>
                        <div id="relatorioMensal"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var parametrosEscopo = location.search.slice(1);
    let codigoEscopo = parametrosEscopo.split('=')[1].split('&')[0];
    let contrato = parametrosEscopo.split('=')[3];
    let atividade = parametrosEscopo.split('&')[2].split('=')[1];
    let mes = parametrosEscopo.split('&')[4].split('=')[1];
    let ano = parametrosEscopo.split('&')[3].split('=')[1];

    // await vmRelatorioAtividade.closeButtons()

    const vmRelatorioAtividade = new Vue({
        el: '#relatorioMensal',
        data() {
            return {
                PerfilUsuario: '',
                Situacao: '',
            }
        },
        methods: {
            async getUsuario() {
                await vmGlobal.dadosUsuario();
                this.PerfilUsuario = vmGlobal.dadosDoUsuario.NomePerfil;
            },
            async getRelatorio() {
                var status = await vmGlobal.getFromAPI({
                    table: 'tblRelatorioMensalProgramas',
                    column: 'Situacao',
                    where: {
                        MesReferencia: vmGlobal.codificarDecodificarParametroUrl(mes, 'decode'),
                        AnoReferencia: vmGlobal.codificarDecodificarParametroUrl(ano, 'decode'),
                        CodigoEscopoPrograma: vmGlobal.codificarDecodificarParametroUrl(codigoEscopo, 'decode'),
                    }
                });
                if (status.length != 0) {
                    this.Situacao = status[0].Situacao;
                }
            },
            async closeButtons() {
                if (this.Situacao == 3 && this.PerfilUsuario) {
                    var botoes = document.querySelectorAll('.btn-toBlock');
                    var inputs = document.querySelectorAll('.input-toBlock');

                    botoes.forEach((el) => {
                        $(el).attr('disabled', true).css({
                            display: 'none'
                        });
                    });

                    inputs.forEach((el) => {
                        $(el).attr('disabled', true);
                    });

                    $.each(CKEDITOR.instances, (el) => {
                        this.$nextTick(() => {
                            var text = CKEDITOR.instances[el].getData();
                            CKEDITOR.instances[el].destroy();
                            CKEDITOR.replace(el, {
                                height: '400px',
                                readOnly: true
                            });
                            CKEDITOR.instances[el].setData(text);
                        });
                    })
                }
            }
        },
        async mounted() {
            await this.getUsuario();
            await this.getRelatorio();
            await this.closeButtons();
        },
    })
</script>



<?php $this->load->view('cgmab/Relatorios/scripts/relatorioPassagemFauna/introducao') ?>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioPassagemFauna/insercao') ?>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioPassagemFauna/profissionais') ?>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioPassagemFauna/analiseGrupo') ?>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioPassagemFauna/dicussoesConclusoes') ?>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioPassagemFauna/correlacao') ?>

<?php $this->load->view('cgmab/Relatorios/scripts/appPassagemFauna') ?>