<!-- css mapa  -->
<?php $this->load->view('../../webroot/arquivos/plugins/leatleft/css/leatleft.php') ?>
<!-- js mapa  -->
<?php $this->load->view('../../webroot/arquivos//plugins/leatleft/js/leatleft.php') ?>
<style>

</style>
<div id="hotSpotsAtropelamento" class="row file_manager">
	<div class="card col-6 col-md-12">
		<h5>Importar ShapeFile</h5>

		<div class="mt-3 mb-3">
			<div class="no-shadow body">
				<div class="border border-secondary" id="mapid" style="height: 600px; width: 100%"></div>
			</div>

			<div>
				<button v-if="dadosAtropelamentoObsercacoes.GraficoHotSpots" @click="deletarShapefile()" id="excluirShapeFile" class="btn btn-outline-secondary float-right btn-toBlock" type="button">Excluir</button>
				<button v-if="coordenadasShapeFile.length && !dadosAtropelamentoObsercacoes.GraficoHotSpots" @click="deletarCoordenadasShapeFile()" class="btn btn-outline-secondary float-right btn-toBlock" type="button">Excluir</button>
				<button v-if="coordenadasShapeFile.length && !dadosAtropelamentoObsercacoes.GraficoHotSpots" @click="salvarShapefile()" class="btn btn-outline-secondary float-right btn-toBlock" type="button"><span class="spinnerShapefile"></span> Salvar</button>

				<div class="input-group" v-show="!coordenadasShapeFile.length">
					<div class="custom-file">
						<input type="file" ref="ShapeFile" name="ShapeFile" class="custom-file-input input-toBlock" accept=".zip" id="ShapeFile">
						<label class="custom-file-label" id="labelShapeFile" for="ShapeFile">Selecione um arquivo ZIP</label>
					</div>
					<div class="input-group-append">
						<button @click="addShapefile()" class="btn btn-outline-secondary btn-toBlock" type="button"><span class="spinnerShapefile"></span> Importar</button>
					</div>
				</div>
			</div>
			<div class="mt-5">
				<textarea name="hotSpots" id="txtHotSpots" class="form-control"></textarea>
				<button type="button" @click="salvarAnaliseHotSpots()" class="btn btn-outline-secondary mt-2 float-right btn-toBlock">Salvar</button>
			</div>

		</div>
	</div>
</div>