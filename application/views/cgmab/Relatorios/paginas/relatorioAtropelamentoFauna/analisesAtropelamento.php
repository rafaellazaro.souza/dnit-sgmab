<div class='row' id="analiseAtropelamento">
	<div class="accordion w-100 mt-3" id="accordion">
		<div class="w-100 border" style="min-height: auto;">
			<div class="card-header" id="headingOne">
				<h5 class="mb-0">
					<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
						Período Seco
					</button>
				</h5>
			</div>

			<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
				<div class="card-body">
					<analise-atropelamento-comp :name-foto="'GraficoPeriodoSeco'" :name-analise="'PeriodoSeco'"></analise-atropelamento-comp>
				</div>
			</div>
		</div>
		<div class="w-100 border" style="min-height: auto;">
			<div class="card-header" id="headingTwo">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
						Período Chuvosos
					</button>
				</h5>
			</div>
			<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
				<div class="card-body">
					<analise-atropelamento-comp :name-foto="'GraficoChuvoso'" :name-analise="'PeriodoChuvoso'"></analise-atropelamento-comp>
				</div>
			</div>
		</div>
		<div class="w-100 border" style="min-height: auto;">
			<div class="card-header" id="headingThree">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
						Comparação entre períodos
					</button>
				</h5>
			</div>
			<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
				<div class="card-body">
					<analise-atropelamento-comp :name-foto="'GraficoComparacaoPeriodos'" :name-analise="'ComparacaoPeriodos'"></analise-atropelamento-comp>
				</div>
			</div>
		</div>
		<div class="w-100 border" style="min-height: auto;">
			<div class="card-header" id="headingFour">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
						Detectabilidade e Taxa de Remoção de Carcaças
					</button>
				</h5>
			</div>
			<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
				<div class="card-body">
					<analise-atropelamento-comp :name-foto="'GraficoDetectabilidadeRemocaoCarcacas'" :name-analise="'DetectabilidadeRemocaoCarcacas'"></analise-atropelamento-comp>
				</div>
			</div>
		</div>
	</div>


</div>
