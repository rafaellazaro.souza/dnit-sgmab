<template id="conclusoesAtropelamentoComp">
	<div>
		<textarea :name="names" :id="names" v-model="dadosDiscussoesConclusoes[names]" class="form-control"></textarea>
		<button type="button" @click="salvaDiscussoesConclusoes(names)" class="btn btn-outline-secondary mt-2 float-right btn-toBlock">Salvar</button>
	</div>
</template>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAtropelamentoFauna/components/appComDiscussoesConclusoes'); ?>