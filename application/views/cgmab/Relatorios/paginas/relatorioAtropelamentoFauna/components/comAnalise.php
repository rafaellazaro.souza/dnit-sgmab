<style>
	.button-grafico {
		text-align: center;
	}
</style>

<template id='analiseAtropelamentoComp'>
	<div>
		<div class="row file_manager">
			<div class="col-6 col-md-5" style="min-height: auto;">
				<div class="file">
					<div class="hover">
						<button @click="deletarFoto(informacaoRelatorioAnalises[nameFoto], nameFoto)" type="button" class="btn-toBlock btn btn-icon btn-icon-mini btn-round btn-danger">
							<i class="zmdi zmdi-delete"></i>
						</button>
					</div>
					<div>
						<div v-if="informacaoRelatorioAnalises && informacaoRelatorioAnalises[nameFoto]">
							<img :src="informacaoRelatorioAnalises[nameFoto]" alt="img" class="img-fluid" style="height: 300px!important; width: 100%;">
						</div>
					</div>
				</div>
				<br>
				<div class="button-grafico" v-if="informacaoRelatorioAnalises && informacaoRelatorioAnalises[nameFoto]">
					<a :href="informacaoRelatorioAnalises[nameFoto]" target="_blank" class="btn btn-outline-secondary mt-0 float-center" role="button" aria-pressed="true">Visualizar Gráfico</a>
				</div>
				<div class="input-group mt-4" v-if="!informacaoRelatorioAnalises || !informacaoRelatorioAnalises[nameFoto]">
					<div class="custom-file">
						<input type="file" ref="Foto" name="Foto" class="custom-file-input input-toBlock" :id="nameFoto" :aria-describedby="'inputGroupFileAdd'+nameFoto">
						<label class="custom-file-label" :for="nameFoto">Selecionar Gráfico</label>
					</div>
					<div class="input-group-append">
						<button @click="verificaFotoRelatorioGF(nameFoto)" class="btn btn-outline-secondary btn-toBlock" type="button" :id="nameFoto">Salvar</button>
					</div>
				</div>
			</div>
			<div class="image col-12 col-md-7">
				<div v-if="informacaoRelatorioAnalises">
					<textarea :nameAnalise="nameAnalise" v-model="informacaoRelatorioAnalises[nameAnalise]" :id="nameAnalise" class="form-control"></textarea>
				</div>
				<div v-else>
					<textarea :nameAnalise="nameAnalise" :id="nameAnalise" class="form-control"></textarea>
				</div>
				<br>
				<button type="button" @click="salvarAnaliseTextoGF(nameAnalise)" class="btn btn-outline-secondary mt-2 float-right btn-toBlock">Salvar</button>
			</div>
		</div>
	</div>
</template>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAtropelamentoFauna/components/appComAnalise'); ?>
