<style>
	.table-responsive {
		height: 500px;
		overflow: auto;
		width: 100%;
	}
</style>

<div class="body_scroll">
	<div id="atropelamentoInsercaoDados" class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<h5>Pontos de Atropelamento</h5>
					<div style="min-height: 100px;" class="no-shadow body">
						<div class="btn-group" role="group" aria-label="Grupo de botões com dropdown aninhado">
							<div class="btn-group" role="group">
								<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-filter"></i> Nome de Grupo
								</button>
								<div class="dropdown-menu" style="left: -112px;" aria-labelledby="btnGroupDrop1">
									<a class="dropdown-item" href="javascript:;" @click="filtroSelecionado = i" v-for="i in listaAtropelamentoFaunaFiltro" v-text="i"></a>
									<!-- <a class="dropdown-item" href="javascript:;" @click="">Com Fotos</a> -->
								</div>
							</div>
						</div>
						<a download="Relatorio_atropelamento_fauna.xlsx" href="http://localhost:8080/sgmab/webroot/uploads/modelos-excel/RelatorioAtropelamentoFauna.xlsx" class="float-right btn btn-info"><i class="fas fa-download"></i> Baixar modelo .xlsx</a>
						<button data-toggle="modal" data-target="#defaultModal" class="float-right btn btn-success mb-3 btn-toBlock"><i class="fas fa-upload"></i> Importar dados dos pontos de coleta .xlsx</button>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
								<thead class="thead-dark">
									<tr>
										<th rowspan="2">Foto</th>
										<th rowspan="2">ID Codigo </br>Foto</th>
										<th colspan="5">Taxonomia</th>
										<th rowspan="2">Nome Comum</th>
										<th colspan="2">Detectabilidade de Carcaças</th>
										<th colspan="4">UTM (Data Sirgas 2000)</th>
										<th rowspan="2">Observações</th>
										<th rowspan="2">Ações</th>
									</tr>
									<tr>
										<th>Grupo</th>
										<th>Classe</th>
										<th>Ordem</th>
										<th>Família</th>
										<th>Espécie</th>
										<th>Tipo de Registro</th>
										<th>Tipo de Monitoramento</th>
										<th>Zona</th>
										<th>CN</th>
										<th>CE</th>
										<th>Elevação(m)</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="i in listaAtropelamentoFauna">
										<td>
											<i v-if="i.CaminhoFoto != null" class="fa fa-check-circle" aria-hidden="true"></i>
											<i v-if="i.CaminhoFoto == null" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
										</td>
										<td>{{i.CodigoAtropelamentoAnimais}}</td>
										<td v-text="i.Grupo"></td>
										<td v-text="i.Classe"></td>
										<td v-text="i.Ordem"></td>
										<td v-text="i.Familia"></td>
										<td v-text="i.Especie"></td>
										<td v-text="i.NomeComum"></td>
										<td v-text="i.TipoRegistro"></td>
										<td v-text="i.TipoMonitoramento"></td>
										<td v-text="i.Zona"></td>
										<td v-text="i.CN"></td>
										<td v-text="i.CE"></td>
										<td v-text="i.Elevacao"></td>
										<td v-text="i.Observacoes"></td>
										<td v-show="i.CaminhoFoto == null"><button @click="deletarRegistros(i.CodigoAtropelamentoAnimais)" class="btn btn-outline-danger btn-sm btn-toBlock"><i class="fa fa-times"></i></button></td>
										<td v-show="i.CaminhoFoto != null"><button disabled @click="deletarRegistros(i.CodigoAtropelamentoAnimais)" class="btn btn-outline-danger btn-sm btn-toBlock"><i class="fa fa-times"></i></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="card" style="min-height: auto;">
					<div class="header">
						<h2><strong>{{totalFotos}}</strong> Pontos de Atropelamento <i class="fas fa-images"></i></h2>
					</div>
					<div class="body no-shadow border">
						<form enctype="multipart/form-data" method="POST">
							<input type="file" id="imagemMaterial" @change="qtdFotos" name="fotos[]" class="dropify input-toBlock" multiple>
						</form>
					</div>
					<button class="btn btn-outline-secondary mt-2 float-right btn-toBlock" @click="verifcaFotos">IMPORTAR</button>
				</div>
				<div class="row file_manager">
					<div v-for="(i, index) in listaAtropelamentoFauna" v-if="i.CaminhoFoto != null" :key="index" class="col-lg-4 col-md-4 col-sm-12">
						<div class="card " :class="[i.Lat == '' || i.Lat == null ? 'borderRed' : 'borderGreen']">
							<div class="file">
								<div class="hover">
									<button @click="deletarFotos(i.CodigoAtropelamentoAnimais, i.CaminhoFoto)" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger btn-toBlock">
										<i class="zmdi zmdi-delete"></i>
									</button>
								</div>
								<div class="image">
									<img :src="i.CaminhoFoto" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
								</div>
								<div class="file-name d-flex justify-content-between">
									<label for="email_address">ID</label>
									<span class="date text-info"><b>{{i.CodigoAtropelamentoAnimais}}</b></span>
								</div>
								<div class="file-name">
									<small>Nome<span class="date text-info"><b>{{i.NomeFoto | singleName}}</b></span></small>
									<small>Nome da Foto<span class="date text-info"><b>{{i.CodigoAtropelamentoAnimais+'_'+i.NomeFoto | singleName}}</b></span></small>
									<small v-if="i.Lat != ''">Latitude<span class="date text-info">{{i.Lat}}</span></small>
									<small v-else>Latitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
									<small v-if="i.Long != ''">Longitude<span class="date text-info">{{i.Long}}</span></small>
									<small v-else>Longitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
									<small v-if="i.DataFoto != '1900-01-01 00:00:00.000'">Data da fotografia <span class="date text-info">{{i.DataFoto}}</span></small>
									<small v-else>Data da fotografia<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
								</div>
								<div class="file-name">
									<label cla>UTM (Data Sirgas 2000)</label><br>
									<div class="row">
										<div class=" col-md-3">
											<label for="email_address">Zona</label>
											<div class="form-group">
												<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
												<input v-else class="form-control" disabled :placeholder="i.Zona">
											</div>
										</div>
										<div class=" col-md-3">
											<label for="email_address">CN</label>
											<div class="form-group">
												<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
												<input v-else class="form-control" disabled :placeholder="i.CN">
											</div>
										</div>
										<div class=" col-md-3">
											<label for="email_address">CE</label>
											<div class="form-group">
												<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
												<input v-else class="form-control" disabled :placeholder="i.CE">
											</div>
										</div>
										<div class=" col-md-3">
											<label for="email_address">Elevação(m)</label>
											<div class="form-group">
												<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
												<input v-else class="form-control" disabled :placeholder="i.Elevacao">
											</div>
										</div>
									</div>
								</div>
								<div class="file-name">
									<small>Grupo<span class="date text-info"><b>{{i.Grupo}}</b></span></small>
									<small>Classe<span class="date text-info"><b>{{i.Classe}}</b></span></small>
									<small>Ordem<span class="date text-info"><b>{{i.Ordem}}</b></span></small>
									<small>Família<span class="date text-info"><b>{{i.Familia}}</b></span></small>
									<small>Espécie<span class="date text-info"><b>{{i.Especie}}</b></span></small>
									<small>Nome Comum<span class="date text-info"><b>{{i.NomeComum}}</b></span></small>
								</div>
								<div class="file-name">
									<label cla>Detectabilidade de Carcaças</label><br>
									<small>Tipo de Registro<span class="date text-info"><b>{{i.TipoRegistro}}</b></span></small>
									<small>Tipo de Monitoramento<span class="date text-info"><b>{{i.TipoMonitoramento}}</b></span></small>
									<small>Observações<span class="date text-info"><b>{{i.Observacoes}}</b></span></small>
								</div>
								<div class="file-name">
									<label>Ficha de Atropelamento modelo do IBAMA</label><br>
									<div class="custom-file" v-if="!i.CaminhoFicha">
										<input type="file" :ref="index+'fichaAtropelamento'" :name="index+'fichaAtropelamento'" :id="index+'fichaAtropelamento'" accept="application/pdf" class="custom-file-input" v-model="i.modelDadosFotosInformacao.FichaAtropelameno">
										<label class="custom-file-label" for="fichaAtropelamento">Seleccionar archivo PDF</label>
									</div>
									<div v-else>
										<a :href="i.CaminhoFicha" target="_blank" class="btn btn-secondary btn-lg btn-block" role="button" aria-pressed="true">Ficha de Atropelamento</a>
									</div>
								</div>
								<div class="file-name">
									<label for="email_address">Fotografia Observações</label>

									<div class="form-group">
										<textarea v-if="i.ObservacaoFoto != null && i.CaminhoFicha !=null" rows="4" disabled class="form-control" :placeholder="i.ObservacaoFoto"></textarea>
										<textarea v-else-if="i.ObservacaoFoto == null && i.CaminhoFicha != null" disabled rows="4" class="form-control" placeholder="Foto salvo sem observações"></textarea>
										<!-- <textarea v-else-if="i.ObservacaoFoto != null && i.CaminhoFicha == null" rows="4" v-text="i.ObservacaoFoto" v-bind:value="i.ObservacaoFoto" v-on:input="i.ObservacaoFoto = i.modelDadosFotosInformacao.ObservacaoFoto" class="form-control"></textarea> -->
										<textarea v-else-if="i.Lat != ''" rows="4" v-model="i.modelDadosFotosInformacao.ObservacaoFoto" class="form-control"></textarea>
										<textarea v-else rows="4" disabled class="form-control" placeholder="A foto não está de acordo com o manual"></textarea>
									</div>
								</div>
								<div v-bind:id="'error-'+index">

								</div>
								<div class="file-name">
									<button v-if="i.Lat != '' && i.CodigoConfiguracaoMonitoramentoFaunaFotos == null && i.CaminhoFicha == null" @click="verificaInformacoesAtropelamento(index, i.CodigoAtropelamentoAnimais)" class="btn btn-success btn-toBlock">Salvar</button>
									<button @click="deletarFotos(i.CodigoAtropelamentoAnimais, i.CaminhoFoto, i.CaminhoFicha)" class="btn btn-danger btn-toBlock">Excluir</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- modal importar pontos de coleta  -->

		<div class="modal fade bd-example-modal-xl" id="defaultModal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="title" id="defaultModalLabel">Importa dados dos Pontos de Coleta
							<br /><small>Utilizar o modelo excel disponibilizado</small>
						</h4>
					</div>
					<div class="modal-body">
						<template v-show="mostrarListaExcel === false">
							<input type="file" id="file" ref="file" name="" class="dropify btn-toBlock" multiple>
						</template>
						<div class="w-100 h450-r">
							<table class="table table-striped" v-show="mostrarListaExcel" id="listaPontosExcel">
								<thead>
									<tr>
										<th colspan="5">Taxonomia</th>
										<th rowspan="2">Nome Comum</th>
										<th colspan="2">Detectabilidade de Carcaças</th>
										<th colspan="4">UTM (Data Sirgas 2000)</th>
										<th rowspan="2">Observações</th>
									</tr>
									<tr>
										<th>Grupo</th>
										<th>Classe</th>
										<th>Ordem</th>
										<th>Família</th>
										<th>Espécie</th>
										<th>Tipo de Registro</th>
										<th>Tipo de Monitoramento</th>
										<th>Zona</th>
										<th>CN</th>
										<th>CE</th>
										<th>Elevação</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="i in listaExcel">
										<td v-text="i.Grupo"></td>
										<td v-text="i.Classe"></td>
										<td v-text="i.Ordem"></td>
										<td v-text="i.Familia"></td>
										<td v-text="i.Especie"></td>
										<td v-text="i.NomeComum"></td>
										<td v-text="i.TipoRegistro"></td>
										<td v-text="i.TipoMonitoramento"></td>
										<td v-text="i.Zona"></td>
										<td v-text="i.CN"></td>
										<td v-text="i.CE"></td>
										<td v-text="i.Elevacao"></td>
										<td v-text="i.Observacoes"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<button @click="importarDadosInsercao()" type="button" class="btn btn-default btn-round waves-effect btn-toBlock" v-show="mostrarListaExcel === false">Importar</button>
						<button @click="salvarDadosInsercaoFauna()" type="button" class="btn btn-default btn-round waves-effect btn-toBlock" v-show="mostrarListaExcel">Confirmar e Salvar</button>
					</div>
				</div>
			</div>
		</div> <!-- modal importar pontos de coleta -->
	</div>
</div>