<div class="row" id="introducaoMonitoramento">
	<div class="col-12">
		<h5>Introdução</h5>
		<textarea name="editorIntroducaoAtropelamento" id="Introducao" v-model="dadosFormulario.Introducao" :value="dadosFormulario.Introducao" class="form-control"></textarea>
		<button type="button" @click="salvarIntroducao()" class="btn btn-outline-secondary mt-2 float-right btn-toBlock">Salvar</button>
	</div>

	<div class="col-12">
		<div class="card-body">
			<div class="header">
				<h5>Documentação Auxiliar</h5>
				<h2><strong>{{totalDocumentos}}</strong> Documentos <i class="fas fa-file-alt"></i></h2>
			</div>
			<div class="body no-shadow border">
				<form enctype="multipart/form-data" method="POST">
					<input type="file" id="documentosMaterial" ref="documentos" @change="qtdDocumentos" name="documentos[]" class="dropify input-toBlock" multiple>
				</form>
			</div>
			<button class="btn btn-outline-secondary mt-2 float-right btn-toBlock" @click="addDocumentos">Importar</button>
		</div>
	</div>

	<div class="col-12">
		<table class="table table-striped" id="listadeDocumentos">
			<thead>
				<tr>
					<th>#</th>
					<th>Nome</th>
					<th>Ações</th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="(i, index) in listaFaunaDocumentos">
					<td>{{index + 1}}</td>
					<td><a :href="i.Caminho" download>{{i.Nome}}</a></td>
					<td>
						<button @click="deletar(i.CodigoAnaliseAnexos, i.index, i.Caminho)" class="btn btn-toBlock btn-outline-danger btn-sm <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'invisivel' : '' ?>"><i class="fa fa-times"></i></button>
						<a :href="i.Caminho" download title="Baixar Arquivo" class="btn btn-outline-info btn-sm <?= $this->session->userdata('PerfilLogin') === 'Fiscal' ? 'invisivel' : '' ?>"><i class="fa fa-download" aria-hidden="true"></i></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>