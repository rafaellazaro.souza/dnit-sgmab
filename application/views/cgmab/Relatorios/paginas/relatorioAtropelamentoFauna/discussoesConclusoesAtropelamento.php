<div class="row" id="discussoesConclusoesAtropelamento">
    <div class="col-12 mb-3">
        <h5>Análises dos Resultados e Discussões</h5>
        <conclusoes-atropelamento-comp :names="'ResultadoDiscussoes'"></conclusoes-atropelamento-comp>

    </div>
    <div class="col-12 mb-3">
        <h5>Conclusões</h5>
        <conclusoes-atropelamento-comp :names="'Conclusoes'"></conclusoes-atropelamento-comp>

    </div>
    <div class="col-12 mb-3">
        <h5>Bibliografia</h5>
        <conclusoes-atropelamento-comp :names="'Bibliografia'"></conclusoes-atropelamento-comp>

    </div>
</div>
