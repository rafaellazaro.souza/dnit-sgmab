<div class="row" id="analiseModulo">
    <div class="accordion w-100 mt-3" id="accordionTwo">
        <div class="w-100 border" style="min-height: auto;" v-for="i in listaConfiguracaoModulo" @onload>
            <div class="card-header" id="headingTwo">
                <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" aria-controls="collapseTwo" :data-target="'#collapseTwo'+i.CodigoModulo" v-text="i.NomeModuloAmostragem">
                    </button>
                </h5>
            </div>

            <div :id="'collapseTwo'+i.CodigoModulo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionTwo">
                <div class="card-body">
                    <analisemodulofauna-comp :names="i.NomeModuloAmostragem" :codigomodulo="i.CodigoModulo">
                    </analisemodulofauna-comp>
                </div>
            </div>
        </div>
    </div>
</div>
