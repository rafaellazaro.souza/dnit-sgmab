<style>
	.button-grafico {
		text-align: center;
	}
</style>

<template id="analiseGrupoFaunistico">
    <div>
        <h5>Análise do Resultados Padrão de Abundância e Riqueza</h5>
        <div class="row file_manager">
            <div class="col-6 col-md-5" style="min-height: auto;">

                <div class="file">
                    <div class="hover">
                        <button @click="deletarFoto(informacaoRelatorioGF.CodigoAnaliseGrupoModulo, 'GraficoAbundanciaRiqueza')" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger btn-toBlock">
                            <i class="zmdi zmdi-delete"></i>
                        </button>
                    </div>
                    <div>
                        <div v-if="informacaoRelatorioGF && informacaoRelatorioGF.GraficoAbundanciaRiqueza">
                            <img :src="informacaoRelatorioGF.GraficoAbundanciaRiqueza" alt="img" class="img-fluid" style="height: 300px!important; width: 100%;">
                        </div>
                    </div>
                </div>
				<br>
				<div class="button-grafico" v-if="informacaoRelatorioGF && informacaoRelatorioGF.GraficoAbundanciaRiqueza">
					<a :href="informacaoRelatorioGF.GraficoAbundanciaRiqueza" target="_blank" class="btn btn-outline-secondary mt-0 float-center" role="button" aria-pressed="true">Visualizar Gráfico</a>
				</div>
                <div class="input-group mt-4" v-if="!informacaoRelatorioGF || !informacaoRelatorioGF.GraficoAbundanciaRiqueza">
                    <div class="custom-file">
                        <input @change="fileSelected($event, 'nameFileAnaliseAbundancia')" type="file" ref="Foto" name="Foto" class="custom-file-input input-toBlock" :id="'AnaliseResultadoAbundancia'+codigogrupofaunistico" :aria-describedby="'inputGroupFileAdd'+codigogrupofaunistico">
                        <label class="custom-file-label" :for="'AnaliseResultadoAbundancia'+codigogrupofaunistico">{{nameFileAnaliseAbundancia}}</label>
                    </div>
                    <div class="input-group-append">
                        <button @click="verificaFotoRelatorioGF('GraficoAbundanciaRiqueza', 'Foto')" class="btn btn-outline-secondary btn-toBlock" type="button" :id="codigogrupofaunistico">Salvar</button>
                    </div>

                </div>
            </div>
            <div class="image col-12 col-md-7">
                <div v-if="informacaoRelatorioGF">
                    <textarea :name="names" v-model="informacaoRelatorioGF.AnaliseResultadoAbundanciaRiqueza" :id="'AnaliseResultadoAbundanciaRiqueza'+codigogrupofaunistico" class="form-control"></textarea>
                </div>
                <div v-else>
                    <textarea :name="names" :id="'AnaliseResultadoAbundanciaRiqueza'+codigogrupofaunistico" class="form-control"></textarea>
                </div>
                <br>
                <button type="button" @click="salvarAnaliseTextoGF('AnaliseResultadoAbundanciaRiqueza')" class="btn-toBlock btn btn-outline-secondary mt-2 float-right">Salvar</button>
            </div>
        </div>

        <h5>Análise do Resultados Índice de Diversidade Shannon-Wiener e Equitabilidade de Pielon</h5>
        <div class="row file_manager">
            <div class="col-6 col-md-5" style="min-height: auto;">

                <div class="file">
                    <div class="hover">
                        <button @click="deletarFoto(informacaoRelatorioGF.CodigoAnaliseGrupoModulo, 'GraficoIndiceDiversidade')" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger btn-toBlock">
                            <i class="zmdi zmdi-delete"></i>
                        </button>
                    </div>
                    <div>
                        <div v-if="informacaoRelatorioGF && informacaoRelatorioGF.GraficoIndiceDiversidade">
                            <img :src="informacaoRelatorioGF.GraficoIndiceDiversidade" alt="img" class="img-fluid" style="height: 300px!important; width: 100%;">
                        </div>
                    </div>
                </div>
				<br>
				<div class="button-grafico" v-if="informacaoRelatorioGF && informacaoRelatorioGF.GraficoIndiceDiversidade">
					<a :href="informacaoRelatorioGF.GraficoIndiceDiversidade" target="_blank" class="btn btn-outline-secondary mt-0 float-center" role="button" aria-pressed="true">Visualizar Gráfico</a>
				</div>
                <div class="input-group mt-4" v-if="!informacaoRelatorioGF || !informacaoRelatorioGF.GraficoIndiceDiversidade">
                    <div class="custom-file">
                        <input @change="fileSelected($event, 'nameFileAnaliseIndice')" type="file" ref="Foto2" name="Foto2" class="custom-file-input input-toBlock" :id="'AnaliseResultaldoIndice'+codigogrupofaunistico" :aria-describedby="'inputGroupFileAdd'+codigogrupofaunistico">
                        <label class="custom-file-label" :for="'AnaliseResultaldoIndice'+codigogrupofaunistico">{{nameFileAnaliseIndice}}</label>
                    </div>
                    <div class="input-group-append">
                        <button @click="verificaFotoRelatorioGF('GraficoIndiceDiversidade', 'Foto2')" class="btn btn-outline-secondary btn-toBlock" type="button" :id="codigogrupofaunistico">Salvar</button>
                    </div>
                </div>
            </div>
            <div class="image col-12 col-md-7">
                <div v-if="informacaoRelatorioGF">
                    <textarea :name="names" :id="'AnaliseResultaldoIndiceDiversidade'+codigogrupofaunistico" class="form-control"></textarea>
                </div>
                <div v-else>
					<textarea :name="names" :id="'AnaliseResultaldoIndiceDiversidade'+codigogrupofaunistico" class="form-control"></textarea>
				</div>
                <br>
                <button type="button" @click="salvarAnaliseTextoGF('AnaliseResultaldoIndiceDiversidade')" class="btn btn-outline-secondary mt-2 float-right btn-toBlock">Salvar</button>
            </div>
        </div>
    </div>
</template>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioMonitoramentoFauna/components/appComAnaliseGrupo'); ?>
