<template id="analiseModuloFauna">
    <div>
        <h5>Análise do Resultados Padrão de Abundância e Riqueza</h5>
        <div class="row file_manager">
            <div class="col-6 col-md-5" style="min-height: auto;">
                <div class="file">
                    <div class="hover">
                        <button @click="deletarFoto(informacaoRelatorioModuloGF.CodigoAnaliseGrupoModulo, 'GraficoAbundanciaRiqueza')" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger btn-toBlock">
                            <i class="zmdi zmdi-delete"></i>
                        </button>
                    </div>
                    <div>
                        <div v-if="informacaoRelatorioModuloGF && informacaoRelatorioModuloGF.GraficoAbundanciaRiqueza">
                            <img :src="informacaoRelatorioModuloGF.GraficoAbundanciaRiqueza" alt="img" class="img-fluid" style="height: 300px!important; width: 100%;">
                        </div>
                    </div>
                </div>
				<br>
				<div class="button-grafico" v-if="informacaoRelatorioModuloGF && informacaoRelatorioModuloGF.GraficoAbundanciaRiqueza">
					<a :href="informacaoRelatorioModuloGF.GraficoAbundanciaRiqueza" target="_blank" class="btn btn-outline-secondary mt-0 float-center" role="button" aria-pressed="true">Visualizar Gráfico</a>
				</div>
                <div class="input-group mt-4" v-if="!informacaoRelatorioModuloGF || !informacaoRelatorioModuloGF.GraficoAbundanciaRiqueza">
                    <div class="custom-file">
                        <input @change="fileSelected($event, 'nameFileAnaliseAbundanciaModulo')" type="file" ref="Foto" name="Foto" class="custom-file-input input-toBlock" :id="'AnaliseResultadoAbundancia'+codigomodulo" :aria-describedby="'inputGroupFileAdd'+codigomodulo">
                        <label class="custom-file-label" :for="'AnaliseResultadoAbundancia'+codigomodulo">{{nameFileAnaliseAbundanciaModulo}}</label>
                    </div>
                    <div class="input-group-append">
                        <button @click="verificaFotoRelatorioModuloGF('GraficoAbundanciaRiqueza', 'Foto')" class="btn btn-outline-secondary btn-toBlock" type="button" :id="codigomodulo">Salvar</button>
                    </div>

                </div>
            </div>
            <div class="image col-12 col-md-7">
                <div v-if="informacaoRelatorioModuloGF">
                    <textarea :name="names" v-model="informacaoRelatorioModuloGF.AnaliseResultadoAbundanciaRiqueza" :id="'AnaliseResultadoAbundanciaRiquezaModulo'+codigomodulo" class="form-control"></textarea>
                </div>
                <div v-else>
                    <textarea :name="names" :id="'AnaliseResultadoAbundanciaRiquezaModulo'+codigomodulo" class="form-control"></textarea>
                </div>
                <br>
                <button type="button" @click="salvarAnaliseTextoGF('AnaliseResultadoAbundanciaRiqueza')" class="btn btn-outline-secondary mt-2 float-right btn-toBlock">Salvar</button>
            </div>
        </div>

        <h5>Análise do Resultados Índice de Diversidade Shannon-Wiener e Equitabilidade de Pielon</h5>
        <div class="row file_manager">
            <div class="col-6 col-md-5" style="min-height: auto;">
                <div class="file">
                    <div class="hover">
                        <button @click="deletarFoto(informacaoRelatorioModuloGF.CodigoAnaliseGrupoModulo, 'GraficoIndiceDiversidade')" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger btn-toBlock">
                            <i class="zmdi zmdi-delete"></i>
                        </button>
                    </div>
                    <div>
                        <div v-if="informacaoRelatorioModuloGF && informacaoRelatorioModuloGF.GraficoIndiceDiversidade">
                            <img :src="informacaoRelatorioModuloGF.GraficoIndiceDiversidade" alt="img" class="img-fluid" style="height: 300px!important; width: 100%;">
                        </div>
                    </div>
                </div>
				<br>
				<div class="button-grafico" v-if="informacaoRelatorioModuloGF && informacaoRelatorioModuloGF.GraficoIndiceDiversidade">
					<a :href="informacaoRelatorioModuloGF.GraficoIndiceDiversidade" target="_blank" class="btn btn-outline-secondary mt-0 float-center" role="button" aria-pressed="true">Visualizar Gráfico</a>
				</div>
                <div class="input-group mt-4" v-if="!informacaoRelatorioModuloGF || !informacaoRelatorioModuloGF.GraficoIndiceDiversidade">
                    <div class="custom-file">
                        <input @change="fileSelected($event, 'nameFileAnaliseIndiceModulo')" type="file" ref="Foto2" name="Foto2" class="custom-file-input input-toBlock" :id="'AnaliseResultaldoIndice'+codigomodulo" :aria-describedby="'inputGroupFileAdd'+codigomodulo">
                        <label class="custom-file-label" :for="'AnaliseResultaldoIndice'+codigomodulo">{{nameFileAnaliseIndiceModulo}}</label>
                    </div>
                    <div class="input-group-append">
                        <button @click="verificaFotoRelatorioModuloGF('GraficoIndiceDiversidade', 'Foto2')" class="btn btn-outline-secondary btn-toBlock" type="button" :id="codigomodulo">Salvar</button>
                    </div>
                </div>
            </div>
            <div class="image col-12 col-md-7">
                <div v-if="informacaoRelatorioModuloGF">
                    <textarea :name="names" v-model="informacaoRelatorioModuloGF.AnaliseResultaldoIndiceDiversidade" :id="'AnaliseResultaldoIndiceDiversidadeModulo'+codigomodulo" class="form-control"></textarea>
                </div>
                <div v-else>
                    <textarea :name="names" :id="'AnaliseResultaldoIndiceDiversidadeModulo'+codigomodulo" class="form-control"></textarea>
                </div>
                <br>
                <button type="button" @click="salvarAnaliseTextoGF('AnaliseResultaldoIndiceDiversidade')" class="btn btn-outline-secondary mt-2 float-right btn-toBlock">Salvar</button>
            </div>
        </div>

        <h5>Análise do Resultados Dendograma de Similaridade</h5>
        <div class="row file_manager">
            <div class="col-6 col-md-5" style="min-height: auto;">

                <div class="file">
                    <div class="hover">
                        <button @click="deletarFoto(informacaoRelatorioModuloGF.CodigoAnaliseGrupoModulo, 'GraficoDendograma')" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger btn-toBlock">
                            <i class="zmdi zmdi-delete"></i>
                        </button>
                    </div>
                    <div>
                        <div v-if="informacaoRelatorioModuloGF && informacaoRelatorioModuloGF.GraficoDendograma">
                            <img :src="informacaoRelatorioModuloGF.GraficoDendograma" alt="img" class="img-fluid" style="height: 300px!important; width: 100%;">
                        </div>
                    </div>
                </div>
				<br>
				<div class="button-grafico" v-if="informacaoRelatorioModuloGF && informacaoRelatorioModuloGF.GraficoDendograma">
					<a :href="informacaoRelatorioModuloGF.GraficoDendograma" target="_blank" class="btn btn-outline-secondary mt-0 float-center" role="button" aria-pressed="true">Visualizar Gráfico</a>
				</div>
                <div class="input-group mt-4" v-if="!informacaoRelatorioModuloGF || !informacaoRelatorioModuloGF.GraficoDendograma">
                    <div class="custom-file">
                        <input @change="fileSelected($event, 'nameFileAnaliseDendogramaModulo')" type="file" ref="Foto3" name="Foto3" class="custom-file-input input-toBlock" :id="'AnaliseResultadoDendograma'+codigomodulo" :aria-describedby="'inputGroupFileAdd'+codigomodulo">
                        <label class="custom-file-label" :for="'AnaliseResultadoDendograma'+codigomodulo">{{nameFileAnaliseDendogramaModulo}}</label>
                    </div>
                    <div class="input-group-append">
                        <button @click="verificaFotoRelatorioModuloGF('GraficoDendograma', 'Foto3')" class="btn btn-outline-secondary btn-toBlock" type="button" :id="codigomodulo">Salvar</button>
                    </div>
                </div>
            </div>
            <div class="image col-12 col-md-7">
                <div v-if="informacaoRelatorioModuloGF">
                    <textarea :name="names" v-model="informacaoRelatorioModuloGF.AnaliseResultadoDendograma" :id="'AnaliseResultadoDendogramaModulo'+codigomodulo" class="form-control"></textarea>
                </div>
                <div v-else>
                    <textarea :name="names" :id="'AnaliseResultadoDendogramaModulo'+codigomodulo" class="form-control"></textarea>
                </div>
                <br>
                <button type="button" @click="salvarAnaliseTextoGF('AnaliseResultadoDendograma')" class="btn btn-outline-secondary mt-2 float-right btn-toBlock">Salvar</button>
            </div>
        </div>
    </div>
</template>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioMonitoramentoFauna/components/appComAnaliseModulo'); ?>
