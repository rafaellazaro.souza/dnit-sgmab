<div class="row" id="analiseGrupo">
    <div class="accordion w-100 mt-3" id="accordion">
        <div class="w-100 border" style="min-height: auto;" v-for="i in listaGrupoFaunistico" @onload>
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" aria-controls="collapseOne" :data-target="'#collapse'+i.CodigoGrupoFaunistico" v-text="i.NomeGrupoFaunistico">
                    </button>
                </h5>
            </div>

            <div :id="'collapse'+i.CodigoGrupoFaunistico" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <analisegrupofaunistico-comp :names="i.NomeGrupoFaunistico" :codigogrupofaunistico="i.CodigoGrupoFaunistico">
                    </analisegrupofaunistico-comp>
                </div>
            </div>
        </div>
    </div>
</div>
