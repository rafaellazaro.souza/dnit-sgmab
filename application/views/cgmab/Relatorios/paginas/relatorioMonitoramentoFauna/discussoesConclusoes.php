<div class="row" id="discussoesConclusoesFauna">
    <div class="col-12 mb-3">
        <h5>Análises dos Resultados e Discussões</h5>
        <discussoesconclusoes-comp :names="'AnaliseResultadosDiscussoes'"></discussoesconclusoes-comp>
    </div>
    <div class="col-12 mb-3">
        <h5>Conclusões</h5>
        <discussoesconclusoes-comp :names="'AnaliseResultadosConclusoes'"></discussoesconclusoes-comp>
    </div>
    <div class="col-12 mb-3">
        <h5>Bibliografia</h5>
        <discussoesconclusoes-comp :names="'AnaliseResultadosBibliografia'"></discussoesconclusoes-comp>
    </div>
</div>
