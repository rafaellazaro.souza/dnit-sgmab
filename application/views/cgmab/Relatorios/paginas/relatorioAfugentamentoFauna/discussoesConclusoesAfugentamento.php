<style>
	.button-grafico {
		text-align: center;
	}
</style>

<div class="row" id="discussoesConclusoesAfugentamento">
	<div class="col-12 mb-3">
		<h5>Análises dos Resultados e Discussões</h5>
		<div class="row file_manager">
			<div class="col-6 col-md-5">
				<div class="file">
					<div class="hover">
						<button @click="deletarFoto(informacaoRelatorioAnalises.UploadGrafico)" type="button" class="btn-toBlock btn btn-icon btn-icon-mini btn-round btn-danger">
							<i class="zmdi zmdi-delete"></i>
						</button>
					</div>
					<div>
						<div v-if="informacaoRelatorioAnalises && informacaoRelatorioAnalises.UploadGrafico">
							<img :src="informacaoRelatorioAnalises.UploadGrafico" alt="img" class="img-fluid" style="height: 300px!important; width: 100%;">
						</div>
					</div>
				</div>
				<br>
				<div class="button-grafico" v-if="informacaoRelatorioAnalises && informacaoRelatorioAnalises.UploadGrafico">
					<a :href="informacaoRelatorioAnalises.UploadGrafico" target="_blank" class="btn btn-outline-secondary mt-0 float-center" role="button" aria-pressed="true">Visualizar Gráfico</a>
				</div>
				<div class="input-group mt-4" v-if="!informacaoRelatorioAnalises || !informacaoRelatorioAnalises.UploadGrafico">
					<div class="custom-file">
						<input type="file" ref="Foto" name="Foto" class="custom-file-input btn-toBlock" id="analiseFoto" aria-describedby="analiseFoto">
						<label class="custom-file-label" for="analiseFoto">Selecionar Gráfico</label>
					</div>
					<div class="input-group-append">
						<button @click="verificaFotoRelatorio" class="btn btn-outline-secondary btn-toBlock" type="button" id="analiseFoto">Salvar</button>
					</div>
				</div>
			</div>

			<div class="image col-12 col-md-7">
				<conclusoes-afugentamento-comp :names="'AnaliseResultados'"></conclusoes-afugentamento-comp>
			</div>
		</div>
	</div>
	<div class="col-12 mb-3">
		<h5>Conclusões</h5>
		<conclusoes-afugentamento-comp :names="'Conclusoes'"></conclusoes-afugentamento-comp>

	</div>
	<div class="col-12 mb-3">
		<h5>Bibliografia</h5>
		<conclusoes-afugentamento-comp :names="'Bibliografia'"></conclusoes-afugentamento-comp>

	</div>
</div>
