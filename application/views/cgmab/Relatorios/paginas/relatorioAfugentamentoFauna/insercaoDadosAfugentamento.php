<style>
	.table-responsive {
		height: 500px;
		overflow: auto;
		width: 100%;
	}
</style>

<div class="body_scroll">
	<div id="afugentamentoInsercaoDados" class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card">
					<h5>Pontos de Afugentamento</h5>
					<div style="min-height: 100px;" class="no-shadow body">
						<div class="btn-group" role="group" aria-label="Grupo de botões com dropdown aninhado">
							<div class="btn-group" role="group">
								<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-filter"></i> Nome de Grupo
								</button>
								<div class="dropdown-menu" style="left: -112px;" aria-labelledby="btnGroupDrop1">
									<a class="dropdown-item" href="javascript:;" @click="filtroSelecionado = i" v-for="i in listaAfugentamentoFaunaFiltro" v-text="i"></a>
									<!-- <a class="dropdown-item" href="javascript:;" @click="">Com Fotos</a> -->
								</div>
							</div>
						</div>
						<a download="Relatorio_afugentamento_fauna.xlsx" href="http://localhost:8080/sgmab/webroot/uploads/modelos-excel/RelatorioAfugentamentoFauna.xlsx" class="float-right btn btn-info"><i class="fas fa-download"></i> Baixar modelo .xlsx</a>
						<button data-toggle="modal" data-target="#defaultModal" class="float-right btn btn-success mb-3 btn-toBlock"><i class="fas fa-upload"></i> Importar dados dos pontos de coleta .xlsx</button>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
								<thead class="thead-dark">
									<tr>
										<th rowspan="2">Foto</th>
										<th rowspan="2">ID Codigo </br>Foto</th>
										<th colspan="5">Taxonomia</th>
										<th rowspan="2">Nome Comum</th>
										<th colspan="2">Grau de Ameaça</th>
										<th colspan="4">UTM (Data Sirgas 2000)</th>
										<th rowspan="2">Registro</th>
										<th rowspan="2">Tipo de Registro</th>
										<th rowspan="2">Destinação</th>
										<th rowspan="2">Observações</th>
										<th rowspan="2">Ações</th>
									</tr>
									<tr>
										<th>Grupo</th>
										<th>Classe</th>
										<th>Ordem</th>
										<th>Família</th>
										<th>Espécie</th>
										<th>IUCN (2018.1)</th>
										<th>ICMBio (2016)</th>
										<th>Zona</th>
										<th>CN</th>
										<th>CE</th>
										<th>Elevação(m)</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="i in listaAfugentamentoFauna">
										<td>
											<i v-if="i.CaminhoFoto != null" class="fa fa-check-circle" aria-hidden="true"></i>
											<i v-if="i.CaminhoFoto == null" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
										</td>
										<td>{{i.CodigoPontoAfugentamentoPontos}}</td>
										<td v-text="i.GrupoFaunistico"></td>
										<td v-text="i.Classe"></td>
										<td v-text="i.Ordem"></td>
										<td v-text="i.Familia"></td>
										<td v-text="i.Especie"></td>
										<td v-text="i.NomeComum"></td>
										<td v-text="i.IUCN"></td>
										<td v-text="i.ICMBIO"></td>
										<td v-text="i.Zona"></td>
										<td v-text="i.CN"></td>
										<td v-text="i.CE"></td>
										<td v-text="i.Elevacao"></td>
										<td v-text="i.Registro"></td>
										<td v-text="i.TipoRegistro"></td>
										<td v-text="i.Destinacao"></td>
										<td v-text="i.Observacoes"></td>
										<td v-show="i.CaminhoFoto == null"><button @click="deletarRegistros(i.CodigoPontoAfugentamentoPontos)" class="btn btn-outline-danger btn-sm btn-toBlock"><i class="fa fa-times"></i></button></td>
										<td v-show="i.CaminhoFoto != null"><button disabled @click="deletarRegistros(i.CodigoPontoAfugentamentoPontos)" class="btn btn-outline-danger btn-sm btn-toBlock"><i class="fa fa-times"></i></button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="card" style="min-height: auto;">
					<div class="header">
						<h2><strong>{{totalFotos}}</strong> Pontos de Afugentamento <i class="fas fa-images"></i></h2>
					</div>
					<div class="body no-shadow border">
						<form enctype="multipart/form-data" method="POST">
							<input type="file" id="imagemMaterial" @change="qtdFotos" name="fotos[]" class="dropify input-toBlock " multiple>
						</form>
					</div>
					<button class="btn btn-outline-secondary mt-2 float-right btn-toBlock" @click="verifcaFotos">IMPORTAR</button>
				</div>
				<div class="row file_manager">
					<div v-for="(i, index) in listaAfugentamentoFauna" v-if="i.CaminhoFoto != null" :key="index" class="col-lg-4 col-md-4 col-sm-12">
						<insercao-dados-afugentamento-comp :i="i" :index="index"></insercao-dados-afugentamento-comp>
					</div>
				</div>
			</div>
		</div>

		<!-- modal importar pontos de coleta  -->

		<div class="modal fade bd-example-modal-xl" id="defaultModal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="title" id="defaultModalLabel">Importa dados dos Pontos de Coleta
							<br /><small>Utilizar o modelo excel disponibilizado</small>
						</h4>
					</div>
					<div class="modal-body">
						<template v-show="mostrarListaExcel === false">
							<input type="file" id="file" ref="file" name="" class="dropify" multiple>
						</template>
						<div class="w-100 h450-r">
							<table class="table table-striped" v-show="mostrarListaExcel" id="listaPontosExcel">
								<thead>
									<tr>
										<th colspan="5">Taxonomia</th>
										<th rowspan="2">Nome Comum</th>
										<th colspan="2">Grau de Ameaça</th>
										<th colspan="4">UTM (Data Sirgas 2000)</th>
										<th rowspan="2">Registro</th>
										<th rowspan="2">Tipo de Registro</th>
										<th rowspan="2">Destinação</th>
										<th rowspan="2">Observações</th>
									</tr>
									<tr>
										<th>Grupo</th>
										<th>Classe</th>
										<th>Ordem</th>
										<th>Família</th>
										<th>Espécie</th>
										<th>IUCN (2018.1)</th>
										<th>ICMBio (2016)</th>
										<th>Zona</th>
										<th>CN</th>
										<th>CE</th>
										<th>Elevação(m)</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="i in listaExcel">
										<td v-text="i.Grupo"></td>
										<td v-text="i.Classe"></td>
										<td v-text="i.Ordem"></td>
										<td v-text="i.Familia"></td>
										<td v-text="i.Especie"></td>
										<td v-text="i.NomeComum"></td>
										<td v-text="i.IUCN"></td>
										<td v-text="i.ICMBio"></td>
										<td v-text="i.Zona"></td>
										<td v-text="i.CN"></td>
										<td v-text="i.CE"></td>
										<td v-text="i.Elevacao"></td>
										<td v-text="i.Registro"></td>
										<td v-text="i.TipoRegistro"></td>
										<td v-text="i.Destinacao"></td>
										<td v-text="i.Observacoes"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<button @click="importarDadosInsercao()" type="button" class="btn btn-default btn-round waves-effect btn-toBlock" v-show="mostrarListaExcel === false">Importar</button>
						<button @click="salvarDadosInsercaoFauna()" type="button" class="btn btn-default btn-round waves-effect btn-toBlock" v-show="mostrarListaExcel">Confirmar e Salvar</button>
					</div>
				</div>
			</div>
		</div> <!-- modal importar pontos de coleta -->
	</div>
</div>