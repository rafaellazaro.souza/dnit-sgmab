<template id="conclusoesAfugentamentoComp">
	<div>
		<textarea v-if="dadosDiscussoesConclusoes[names] != null" :name="names" :id="names" v-model="atob(dadosDiscussoesConclusoes[names])" class="form-control"></textarea>
		<textarea v-else :name="names" :id="names" v-model="dadosDiscussoesConclusoes[names]" class="form-control"></textarea>
		<button type="button" @click="salvaDiscussoesConclusoes(names)" class="btn btn-outline-secondary mt-2 float-right btn-toBlock">Salvar</button>
	</div>
</template>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAfugentamentoFauna/components/appComDiscussoesConclusoes'); ?>
