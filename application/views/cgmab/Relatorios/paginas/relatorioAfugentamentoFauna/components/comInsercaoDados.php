<template id="insercaoDadosAfugentamentoComp">
	<div>
		<div class="card " :class="[i.Lat == '' || i.Lat == null ? 'borderRed' : 'borderGreen']">
			<div class="file">
				<div class="hover">
					<button @click="deletarFotos(i.CodigoPontoAfugentamentoPontos, i.CaminhoFoto)" type="button" class="btn-toBlock btn btn-icon btn-icon-mini btn-round btn-danger">
						<i class="zmdi zmdi-delete"></i>
					</button>
				</div>
				<div class="image">
					<img :src="i.CaminhoFoto" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
				</div>
				<div class="file-name d-flex justify-content-between">
					<label for="email_address">ID</label>
					<span class="date text-info"><b>{{i.CodigoPontoAfugentamentoPontos}}</b></span>
				</div>
				<div class="file-name">
					<small>Nome<span class="date text-info"><b>{{i.NomeFoto | singleName}}</b></span></small>
					<small>Nome da Foto<span class="date text-info"><b>{{i.CodigoPontoAfugentamentoPontos+'_'+i.NomeFoto | singleName}}</b></span></small>
					<small v-if="i.Lat != ''">Latitude<span class="date text-info">{{i.Lat}}</span></small>
					<small v-else>Latitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
					<small v-if="i.Long != ''">Longitude<span class="date text-info">{{i.Long}}</span></small>
					<small v-else>Longitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
					<small v-if="i.DataFoto != '1900-01-01 00:00:00.000'">Data da fotografia <span class="date text-info">{{i.DataFoto}}</span></small>
					<small v-else>Data da fotografia<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
				</div>
				<div class="file-name">
					<label cla>UTM (Data Sirgas 2000)</label><br>
					<div class="row">
						<div class=" col-md-3">
							<label for="email_address">Zona</label>
							<div class="form-group">
								<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
								<input v-else class="form-control" disabled :placeholder="i.Zona">
							</div>
						</div>
						<div class=" col-md-3">
							<label for="email_address">CN</label>
							<div class="form-group">
								<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
								<input v-else class="form-control" disabled :placeholder="i.CN">
							</div>
						</div>
						<div class=" col-md-3">
							<label for="email_address">CE</label>
							<div class="form-group">
								<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
								<input v-else class="form-control" disabled :placeholder="i.CE">
							</div>
						</div>
						<div class=" col-md-3">
							<label for="email_address">Elevação(m)</label>
							<div class="form-group">
								<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
								<input v-else class="form-control" disabled :placeholder="i.Elevacao">
							</div>
						</div>
					</div>
				</div>
				<div class="file-name">
					<small>Grupo<span class="date text-info"><b>{{i.GrupoFaunistico}}</b></span></small>
					<small>Classe<span class="date text-info"><b>{{i.Classe}}</b></span></small>
					<small>Ordem<span class="date text-info"><b>{{i.Ordem}}</b></span></small>
					<small>Família<span class="date text-info"><b>{{i.Familia}}</b></span></small>
					<small>Espécie<span class="date text-info"><b>{{i.Especie}}</b></span></small>
					<small>Nome Comum<span class="date text-info"><b>{{i.NomeComum}}</b></span></small>
				</div>
				<div class="file-name">
					<label>Grau de Ameaça</label><br>
					<small>IUCN (2018.1)<span class="date text-info"><b>{{i.IUCN}}</b></span></small>
					<small>ICMBIO (2016)<span class="date text-info"><b>{{i.ICMBIO}}</b></span></small>
				</div>
				<div class="file-name">
					<small>Registro<span class="date text-info"><b>{{i.Registro}}</b></span></small>
					<small>Tipo de Registro<span class="date text-info"><b>{{i.TipoRegistro}}</b></span></small>
					<small>Destinação<span class="date text-info"><b>{{i.Destinacao}}</b></span></small>
				</div>
				<div class="file-name">
					<small>Observações<span class="date text-info"><b>{{i.Observacoes}}</b></span></small>
				</div>
				<div class="file-name">
					<label for="email_address">Fotografia Observações</label>
					<div class="form-group">
						<textarea v-if="i.Lat == '' && i.Long == ''" rows="4" disabled class="form-control" placeholder="A foto não está de acordo com o manual"></textarea>
						<textarea v-else-if="i.ObservacaoFoto != null" rows="4" disabled :placeholder="i.ObservacaoFoto" class="form-control"></textarea>
						<textarea v-else rows="4" v-model="ObservacaoFoto" class="form-control"></textarea>
					</div>
				</div>
				<div class="file-name">
					<button v-if="i.Lat != ''" @click="salvarObservacoes(i.CodigoPontoAfugentamentoPontos)" class="btn btn-success btn-toBlock">Salvar</button>
					<button @click="deletarFotos(i.CodigoPontoAfugentamentoPontos, i.CaminhoFoto)" class="btn btn-danger btn-toBlock">Excluir</button>
				</div>
			</div>
		</div>
	</div>
</template>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAfugentamentoFauna/components/appComIsercaoDados'); ?>