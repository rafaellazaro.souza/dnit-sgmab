<div class="container-fluid" id="insercaoMedicoes" v-show="mostrarMedicoes">

    <div class="row file_manager">
        <div v-for="i in listaPontosColeta" class="col-lg-4 col-md-4 col-sm-12">
            <div class="card border">
                <div class="file">
                    <div class="image">
                        <img :src="i.Caminho" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
                    </div>
                    <div class="file-name">
                        <small>Nome do Ponto de Coleta<span class="date text-info"><b>{{i.NomePontoColeta}}</b></span></small>
                        <small>Nome da Foto<span class="date text-info"><b>{{i.NomeFoto}}</b></span></small>
                        <small>Tipo de Ponto de Amostra<span class="date text-info"><b>{{i.TipoPontoAmostra}}</b></span></small>
                        <small v-if="i.Lat != ''">Latitude<span class="date text-info">{{i.Lat}}</span></small>
                        <small v-else>Latitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                        <small v-if="i.Lng != ''">Longitude<span class="date text-info">{{i.Lng}}</span></small>
                        <small v-else>Longitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                        <small v-if="i.DataFoto != '1900-01-01 00:00:00.000'">Data da fotografia <span class="date text-info">{{i.DataFotoF}}</span></small>
                        <small v-else>Data da fotografia<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                    </div>
                    <div class="file-name">
                        <label cla>UTM (Data Sirgas 2000)</label><br>
                        <div class="row">
                            <div class=" col-md-3">
                                <label for="email_address">Zona</label>
                                <div class="form-group">
                                    <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">

                                </div>
                            </div>
                            <div class=" col-md-3">
                                <label for="email_address">CN</label>
                                <div class="form-group  ">
                                    <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">

                                </div>
                            </div>
                            <div class=" col-md-3">
                                <label for="email_address">CE</label>
                                <div class="form-group  ">
                                    <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">

                                </div>
                            </div>
                            <div class=" col-md-3">
                                <label for="email_address  ">Elevação (m)</label>
                                <div class="form-group ">
                                    <input v-if="i.Lat == '' && i.DataFoto == '1900-01-01 00:00:00.000'" disabled class="form-control" placeholder="">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="file-name text-right">
                        <button class="btn btn-outline-secondary" @click="iniciarMedicoes(i.CodigoConfiguracaoRecursosHidricosFotos, vmApp.DadosAtividade, 'observacao', i)">Observações Técnicas</button>
                        <button class="btn btn-outline-secondary" @click="iniciarMedicoes(i.CodigoConfiguracaoRecursosHidricosFotos, vmApp.DadosAtividade, 'medicao', i)">Medições</button>
                        <button class="btn btn-outline-secondary" @click="iniciarMedicoes(i.CodigoConfiguracaoRecursosHidricosFotos, vmApp.DadosAtividade, 'anexo')">Anexos</button>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>




<?php $this->load->view('cgmab/Relatorios/paginas/relatorioAgua/insercaoObservacoes'); ?>
<?php $this->load->view('cgmab/Relatorios/paginas/relatorioAgua/insercaoMedicoesParametros'); ?>
<?php $this->load->view('cgmab/Relatorios/paginas/relatorioAgua/insercaoDocumentos'); ?>
<?php //$this->load->view('cgmab/Relatorios/paginas/relatorioAgua/insercaoFotos'); ?>
