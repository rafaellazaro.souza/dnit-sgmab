<div class="row" id="profissionais">

    <div class="col-12 col-sm-6 border-right">
        <h4>Selecione uma opção abaixo </h4>

        <button @click="getRecursosServico('profisionais')" class="btn">Profissionais</button>
        <button @click="getRecursosServico('veiculos')" class="btn">Veículos</button>
        <button @click="getRecursosServico('equipamentos')" class="btn">Equipamentos</button>


        <!-- lista de equipamentos  -->
        <table class="table table-striped mt-3" v-if="opcao == 'equipamentos'">
            <thead>
                <tr>
                    <th><i class="fa fa-photo"></i></th>
                    <th>Equipamento</th>
                    <th>NS</th>
                    <th></th>
                </tr>

            </thead>
            <tbody>
                <tr v-for="i in listaRecursosServico">
                    <td class="w-25"><img :src="i.Foto" alt=""></td>
                    <td v-text="i.NomeEquipamento"></td>
                    <td v-text="i.NumeroSerie"></td>
                    <td>
                        <button @click="salvarProfissionalRelatorio(i.CodigoRecurso, 'equipamentos')" class="btn btn-outline-success btn-sm"><i class="fa fa-check"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>

        <!-- lista de veiculos  -->
        <table class="table table-striped mt-3" v-if="opcao == 'veiculos'">
            <thead>
                <tr>
                    <th><i class="fa fa-photo"></i></th>
                    <th>Marca/modelo</th>
                    <th>Placa</th>
                    <th>Ano</th>
                    <th></th>
                </tr>

            </thead>
            <tbody>
                <tr v-for="i in listaRecursosServico">
                    <td class="w-25"><img :src="i.Foto" alt=""></td>
                    <td v-text="i.NomeModelo"></td>
                    <td v-text="i.Placa"></td>
                    <td v-text="i.Ano"></td>
                    <td>
                        <button @click="salvarProfissionalRelatorio(i.CodigoRecurso, 'veiculos')" class="btn btn-outline-success btn-sm"><i class="fa fa-check"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- lista de profissionais  -->
        <table class="table table-striped mt-3" v-if="opcao == 'profisionais'">
            <thead>
                <tr>
                    <th><i class="fa fa-photo"></i></th>
                    <th>Nome</th>
                    <th></th>
                </tr>

            </thead>
            <tbody>
                <tr v-for="i in listaRecursosServico">

                    <td class="w-25"><img :src="i.Foto" alt=""></td>
                    <td v-text="i.NomePessoa"></td>
                    <td v-text="i.CPF"></td>
                    <td>
                        <button @click="salvarProfissionalRelatorio(i.CodigoRecurso, 'profisionais')" class="btn btn-outline-success btn-sm"><i class="fa fa-check"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>



    <!-- *************************** recursos da atividade ************************************ -->
    <div class="col-12 col-sm-6">

        <h4>Recursos utilizados</h4>
        <!-- lista de equipamentos  -->
        <table class="table table-striped mt-3" v-if="opcao == 'equipamentos'">
            <thead>
                <tr>
                    <th><i class="fa fa-photo"></i></th>
                    <th>Equipamento</th>
                    <th>NS</th>
                    <th></th>
                </tr>

            </thead>
            <tbody>
                <tr v-for="i in listaRecursosPrograma">
                    <td class="w-25"><img :src="i.Foto" alt=""></td>
                    <td v-text="i.NomeEquipamento"></td>
                    <td v-text="i.NumeroSerie"></td>
                    <td>
                        <button @click="deletarRecurso(i.CodigoAtividadeRecurso)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>

        <!-- lista de veiculos  -->
        <table class="table table-striped mt-3" v-if="opcao == 'veiculos'">
            <thead>
                <tr>
                    <th><i class="fa fa-photo"></i></th>
                    <th>Marca/modelo</th>
                    <th>Placa</th>
                    <th>Ano</th>
                    <th></th>
                </tr>

            </thead>
            <tbody>
                <tr v-for="i in listaRecursosPrograma">
                    <td class="w-25"><img :src="i.Foto" alt=""></td>
                    <td v-text="i.NomeModelo"></td>
                    <td v-text="i.Placa"></td>
                    <td v-text="i.Ano"></td>
                    <td>
                        <button @click="deletarRecurso(i.CodigoAtividadeRecurso)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- lista de profissionais  -->
        <table class="table table-striped mt-3" v-if="opcao == 'profisionais'">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th></th>
                </tr>

            </thead>
            <tbody>
                <tr v-for="i in listaRecursosPrograma">
                    <td v-text="i.NomePessoa"></td>
                    <td v-text="i.CPF"></td>
                    <td>
                        <button @click="deletarRecurso(i.CodigoAtividadeRecurso)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>