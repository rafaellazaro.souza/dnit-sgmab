<div class="row" id="observacoesCampanha">
<button class="btn btn-success btn-r mr-4" @click="savarDados()"><i class="fa fa-save"></i> Salvar</button>
<h4 class="w-100">Observações da Campanha </h4>
    <div class="col-12 col-sm-6">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    Data de início da campanha:
                </div>
            </div> 
            <input type="text" v-model="inicio" class="form-control">
        </div>
    </div>
    <div class="col-12 col-sm-6">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    Data de Fim da campanha:
                </div>
            </div> 
            <input @blur="savarDados('datas')" type="text" v-model="fim" class="form-control">
        </div>
    </div>
    <div class="col-12 mb-5 mt-5">
        <h5>Observações Gerais</h5>
        <textarea name="editor1" id="textAreaObsCamp1"></textarea>

    </div>
    <div class="col-12 mb-5">
        <h5>Informações Sobre a Área de Influência</h5>
        <textarea name="editor2" id="textAreaObsCamp2"></textarea>

    </div>
    <div class="col-12 mb-5">
        <h5>Informações sobre Local e Pontos de Coleta</h5>
        <textarea name="editor3" id="textAreaObsCamp3"></textarea>

    </div>
    <div class="col-12 mb-5">
        <h5>Observações sobre Coleta e Preservação das Amostras</h5>
        <textarea name="editor4" id="textAreaObsCamp4"></textarea>

    </div>
    <div class="col-12 mb-5">
        <h5>Observações Sobre Acondicionamento Transporte de Amostras</h5>
        <textarea name="editor5" id="textAreaObsCamp5"></textarea>

    </div>
</div>