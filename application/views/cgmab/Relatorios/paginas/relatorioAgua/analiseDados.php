<style>
    .btn-pt-analise {
        height: 196px;
        letter-spacing: 3px;
        text-align: center;
        background-color: #0c7ce6 !important;
        box-shadow: -23px 3px 46px -32px #020202;
    }

    .btn-pt-analise>h5 {
        line-height: 136px;
        font-size: 15px;
    }

    .btn-pt-analise>h5>i {
        font-size: 21px;
    }
</style>
<div class="container-fluid" id="analiseDados">
    <div class="row" v-show="mostrarSelecaoPonto1">
        <div class="col-12">
            <div v-for="i in listaPontosDoPrograma" @click="iniciarMedicoes(i);" class="btn col-2 btn-pt-analise">
                <h5 class="mt-3"><i class="fa fa-map-marker"></i> &nbsp; {{i.NomePontoColeta}}</h5>
            </div>
        </div>
    </div>

    <div class="row pt-5" v-show="mostrarDadosPontoAnaliseDados">
        <div class="col-12 text-right">
            <button @click="mostrarSelecaoPonto1 = true; mostrarDadosPontoAnaliseDados = false" class="btn btn-secondary">Selecionar outro Pontos</button>
        </div>
        <h5>Dados do Ponto {{pontoSelecionado.CodigoConfiguracaoRecursosHidricosPontoColeta}} - {{pontoSelecionado.NomePontoColeta}}</h5>
        <div class="col-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th colspan="9" class="border-right">Natureza da Amostra</th>
                        <th colspan="11">Dados Localiazação</th>
                    </tr>
                    <tr>
                        <th>Classe 1</th>
                        <th>Classe 2</th>
                        <th>Classe 3</th>
                        <th>Classe 4</th>
                        <th>Origem Massa D'agua</th>
                        <th>Tipo Massa D'agua</th>
                        <th>Classe Hídrirca</th>
                        <th>Tipo Amostra</th>
                        <th>Tipo Ambiente</th>
                        <th class="border-left">UF</th>
                        <th>Município</th>
                        <th>Bacia</th>
                        <th>KMI</th>
                        <th>KMF</th>
                        <th>Estaca</th>
                        <th class="border-left">Obs</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td v-text="pontoSelecionado.Classe1"></td>
                        <td v-text="pontoSelecionado.Classe2"></td>
                        <td v-text="pontoSelecionado.Classe3"></td>
                        <td v-text="pontoSelecionado.Classe4"></td>
                        <td v-text="pontoSelecionado.MassaAgua"></td>
                        <td v-text="pontoSelecionado.TipoMassaAgua"></td>
                        <td v-text="pontoSelecionado.ClasseHidrica"></td>
                        <td v-text="pontoSelecionado.TipoAmostra"></td>
                        <td v-text="pontoSelecionado.TipoAmbiente">Lótico</td>
                        <td v-text="pontoSelecionado.UF"></td>
                        <td v-text="pontoSelecionado.Municipio"></td>
                        <td v-text="pontoSelecionado.Bacia"></td>
                        <td v-text="pontoSelecionado.KmInicial"></td>
                        <td v-text="pontoSelecionado.KmFinal"></td>
                        <td v-text="pontoSelecionado.Estaca"></td>
                        <td v-text="pontoSelecionado.Observacoes"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row mt-5 border-top pt-5" v-show="mostrarDadosPontoAnaliseDados">
        <h5 class="w-100 mb-4">Resultado Parâmetros Analizados</h5>
        <div class="col-12 col-sm-6">
            <h6>Jusante</h6>
            <div class="h450-r">
                <table class="table table-striped">
                    <thead>

                        <tr>
                            <th>Parâmetro</th>
                            <th>Unidade Medida</th>
                            <th>Limite</th>
                            <th>Campanha Anterior/Atual</th>
                            <th>Status</th>
                            <th>Variação</th>
                            <th>Metodolgia de Análise</th>
                            <th>Lista Equipamentos Técnicos</th>
                            <th>Observações Específicas</th>
                            <th>Gráfico</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="i in listaMedicoesJusante">
                            <td v-text="i.NomeParametro"></td>
                            <td v-text="i.UnidadeMedida"></td>
                            <td v-text="i.ValorLimite ? i.Limite+' '+i.ValorLimite : '-'"></td>
                            <td v-text="'0 / '+ i.Valor"></td>
                            <td>
                                <template v-if="i.Limite != 'Fórmula'">
                                    <template v-if="i.Limite === 'Maior Que'">
                                        <template v-if="parseInt(i.Valor) > parseInt(i.ValorLimite)"> <span style="color:green;color: green;border: 1px solid green;padding: 5px;">Aceitavel</span></template>
                                        <template v-else><span style="color:green;color: red;padding: 5px;">Não Aceitavel</span></template>
                                    </template>
                                    <template v-if="i.Limite === 'Menor Que'">
                                        <template v-if="parseInt(i.Valor) < parseInt(i.ValorLimite)"> <span style="color:green;color: green;border: 1px solid green;padding: 5px;">Aceitavel</span></template>
                                        <template v-else><span style="color:green;color: red;padding: 5px;">Não Aceitavel</span></template>
                                    </template>
                                </template>
                                <template v-else>
                                    <span v-show="!mostrarSelectStatus" v-text="i.Status"></span>
                                    <i @click="mostrarSelectStatus = true" class="fa fa-pen" v-show="!mostrarSelectStatus"></i>
                                    <select @change="atualizarStatusMedicao(i.CodigoRecursosHidricosRelatorioMedicao, $event, 'Jusante')" class="form-control" v-show="mostrarSelectStatus">
                                        <option value="Ótimo">Ótima</option>
                                        <option value="Boa">Boa</option>
                                        <option value="Razoável">Razoável</option>
                                        <option value="Ruim">Ruim</option>
                                        <option value="Péssima">Péssima</option>
                                    </select>


                                </template>
                            </td>
                            <td>
                                <i class="fa fa-arrow-up"></i>
                            </td>
                            <td v-text="i.MetodologiaAnalise"></td>
                            <td><i @click="getEquipamentosUtilizados(i.CodigoRecursosHidricosParametros)" class="fa fa-list"></i></td>
                            <td><a href="javascript:;" :title="i.ObservacoesEspecificas ? i.ObservacoesEspecificas : 'Nenhuma Observação'"><i class="fa fa-eye"></i></a></td>
                            <td>
                                <button @click="dadosGraficoParametros(i.CodigoRecursosHidricosParametros, i.NomeParametro)" class="btn btn-sm btn-outline-secondary"><i class="fas fa-chart-pie"></i></button>
                            </td>
                        </tr>


                    </tbody>
                </table>

                <div class="modal-w98-h50 col-6 sombra-2 p-5" v-show="mostrarEquipamentos">
                    <div class="col-12 text-right"><button @click="mostrarEquipamentos = false" class="btn btn-r"><i class="fa fa-times"></i></button></div>
                    
                    <h4>Equipamentos Utilizados</h4>
                    <table class="table table-striped table-bordered">
                        <tbody>
                            <tr v-for="i in listaEquipamentosUtilzados">
                                <td class="w-25">
                                    <img :src="i.Foto" alt="" class="w-100">
                                </td>
                                <td v-text="i.NomeEquipamento"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="col-12 col-sm-6 border-left">
            <h6>Montante</h6>
            <div class="h450-r">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Parâmetro</th>
                            <th>Unidade Medida</th>
                            <th>Limite</th>
                            <th>Campanha Anterior/Atual</th>
                            <th>Status</th>
                            <th>Variação</th>
                            <th>Metodolgia de Análise</th>
                            <th>Lista Equipamentos Técnicos</th>
                            <th>Observações Específicas</th>
                            <th>Gráfico</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="i in listaMedicoesMontante">
                            <td v-text="i.NomeParametro"></td>
                            <td v-text="i.UnidadeMedida"></td>
                            <td v-text="i.Limite+' '+ i.ValorLimite"></td>
                            <td v-text="'0 / '+ i.Valor"></td>
                            <td>
                                <template v-if="i.Limite != 'Fórmula'">
                                    <template v-if="i.Limite === 'Maior Que'">
                                        <template v-if="parseInt(i.Valor) > parseInt(i.ValorLimite)"> <span style="color:green;color: green;border: 1px solid green;padding: 5px;">Aceitavel</span></template>
                                        <template v-else><span style="color:green;color: red;padding: 5px;">Não Aceitavel</span></template>
                                    </template>
                                    <template v-if="i.Limite === 'Menor Que'">
                                        <template v-if="parseInt(i.Valor) < parseInt(i.ValorLimite)"> <span style="color:green;color: green;border: 1px solid green;padding: 5px;">Aceitavel</span></template>
                                        <template v-else><span style="color:green;color: red;padding: 5px;">Não Aceitavel</span></template>
                                    </template>
                                </template>
                                <template v-else>
                                    <span v-show="!mostrarSelectStatus" v-text="i.Status"></span>
                                    <i @click="mostrarSelectStatus = true" class="fa fa-pen" v-show="!mostrarSelectStatus"></i>
                                    <select @change="atualizarStatusMedicao(i.CodigoRecursosHidricosRelatorioMedicao, $event, 'Jusante')" class="form-control" v-show="mostrarSelectStatus">
                                        <option value="Ótimo">Ótima</option>
                                        <option value="Boa">Boa</option>
                                        <option value="Razoável">Razoável</option>
                                        <option value="Ruim">Ruim</option>
                                        <option value="Péssima">Péssima</option>
                                    </select>


                                </template>
                            </td>
                            <td>
                                <i class="fa fa-arrow-up"></i>
                            </td>
                            <td v-text="i.MetodologiaAnalise"></td>
                            <td><i  @click="getEquipamentosUtilizados(i.CodigoRecursosHidricosParametros)"  class="fa fa-list"></i></td>
                            <td><a href="javascript:;" :title="i.ObservacoesEspecificas ? i.ObservacoesEspecificas : 'Nenhuma Observação'"><i class="fa fa-eye"></i></a></td>
                            <td>
                                <button @click="dadosGraficoParametros(i.CodigoRecursosHidricosParametros, i.NomeParametro)" class="btn btn-sm btn-outline-secondary"><i class="fas fa-chart-pie"></i></button>
                            </td>
                        </tr>


                    </tbody>
                </table>
            </div>
        </div>


        <!-- Grafico meses medição parametro  -->
        <div class="modal-w98-h50 sombra-2 p-5 col-8" v-show="mostrarGrafico">
            <div class="col-12 text-right">
                <button @click="mostrarGrafico = false" class="btn btn-secondary"><i class="fa fa-times"></i></button>
            </div>
            <h5>{{nomeParametro}}</h5>
            <div id="chart_div"></div>
        </div>
    </div>
    <div class="row mt-5 pt-4 border-top" v-show="mostrarDadosPontoAnaliseDados">
        <h5>Análise de Resultados </h5>
        <div class="col-12 mt-4">
            <textarea id="txtAnaliseResultados" name="editorAnaliseDados1"></textarea>
        </div>
        <div class="col-12 text-right">
            <button @click="salvarAnaliseDados(true)" class="btn btn-success">Salvar Análise de Resultados</button>
        </div>
    </div>
</div>