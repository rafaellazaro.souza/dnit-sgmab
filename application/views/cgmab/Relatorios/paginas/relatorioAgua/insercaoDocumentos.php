<div class="row" id="anexoDocumentos" v-show="mostrarAnexos">
    <h4>Anexo de Documentos</h4>
    <div class="col-12 text-right">
        <buton @click="mostrarAddAnexo = true" class="btn btn-info btn-sm mb-2"><i class="fa fa-plus"></i> Adicionar Documento</buton>
    </div>
    <div class="col-12 h450-r">
        <table class="table table-striped">
            <thead>
                <th>Data Envio</th>
                <th>Documento</th>
                <th>Anexo</th>
                <th>Açoes</th>
            </thead>
            <tbody>
                <tr v-for="i in listaAnexos">
                    <td v-text="vmGlobal.frontEndDateFormat(i.Data)"></td>
                    <td v-text="i.Descricao"></td>
                    <td><a :href="i.Caminho" target="_blank" class="btn btn-sm"><i class="fa fa-download"></i></a></td>
                    <td>
                        <button @click="deletarAnexo(i.CodigoAnexo, i.Caminho)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>
    <div class="col-12 text-right">
        <button @click="mostrarAnexos = false; vmInsercao.mostrarMedicoes = true" class="btn btn-sm">Voltar</button>
    </div>

    <div class="modal-w98-h50 sombra-2 col-6 p-5" v-show="mostrarAddAnexo">
        <h4>Adicionar Anexos</h4>
        <div class="alert alert-info mb-3">
            <p>Digite o nome do documento e selecione o arquivo a ser enviado, em seguida presione enviar!</p>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Nome/ Descrição</span>
            </div>
            <input v-model="arquivo" type="text" class="form-control">
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Arquivo</span>
            </div>
            <input type="file" id="file" ref="file" @change="carregarArquivo()" class="form-control">
        </div>
        <div class="col-12 text-right border-top mt-5 pt-3">
            <button @click="mostrarAddAnexo = false" class="btn btn-sm">Cancelar</button>
            <button @click="enviarArquivo()" class="btn btn-success btn-sm">Enviar</button>
        </div>
    </div>
</div>