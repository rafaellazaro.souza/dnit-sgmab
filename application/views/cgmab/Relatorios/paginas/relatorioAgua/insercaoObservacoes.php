<div class="row" id="observacoes-tecnicas" v-show="mostrarObs">

    <ul class="w-100 mb-4">
        <li>Nome do Ponto de Coleta<span class="date text-info"><b>{{i.NomePontoColeta}}</b></span></li>
        <li>Nome da Foto<span class="date text-info"><b>{{i.NomeFoto}}</b></span></li>
        <li>Tipo de Ponto de Amostra<span class="date text-info"><b>{{i.TipoPontoAmostra}}</b></span></li>
        <li v-if="i.Lat != ''">Latitude<span class="date text-info">{{i.Lat}}</span></li>
        <li v-else>Latitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></li>
        <li v-if="i.Lng != ''">Longitude<span class="date text-info">{{i.Lng}}</span></li>
        <li v-else>Longitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></li>
        <li v-if="i.DataFoto != '1900-01-01 00:00:00.000'">Data da fotografia <span class="date text-info">{{i.DataFotoF}}</span></li>
        <li v-else>Data da fotografia<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
    </ul>


    <h4>Observações Técnicas</h4>
    <div class="col-12">
        <textarea name="editorObs" id="obsTec"></textarea>
    </div>
    <div class="col-12 text-right">
        <button @click="mostrarObs = false; vmInsercao.mostrarMedicoes = true" class="btn btn-secondary mt-4"><i class="fa fa-times"></i> Cancelar</button>
        <button @click="salvarObservacao()" class="btn btn-success mt-4"><i class="fa fa-save"></i> Salvar Observações Técnicas</button>
    </div>
</div>