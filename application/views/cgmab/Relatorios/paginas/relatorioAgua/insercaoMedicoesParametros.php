<div class="row" id="medicoes" v-show="mostrarMedicoes">


<ul class="w-100 mb-4">
        <li>Nome do Ponto de Coleta<span class="date text-info"><b>{{i.NomePontoColeta}}</b></span></li>
        <li>Nome da Foto<span class="date text-info"><b>{{i.NomeFoto}}</b></span></li>
        <li>Tipo de Ponto de Amostra<span class="date text-info"><b>{{i.TipoPontoAmostra}}</b></span></li>
        <li v-if="i.Lat != ''">Latitude<span class="date text-info">{{i.Lat}}</span></li>
        <li v-else>Latitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></li>
        <li v-if="i.Lng != ''">Longitude<span class="date text-info">{{i.Lng}}</span></li>
        <li v-else>Longitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></li>
        <li v-if="i.DataFoto != '1900-01-01 00:00:00.000'">Data da fotografia <span class="date text-info">{{i.DataFotoF}}</span></li>
        <li v-else>Data da fotografia<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
    </ul>

    
    <h4>Medição de Parâmetros</h4>
    <button @click="mostrarAddMedicao = true" class="btn btn-success mr-5 btn-r">Adicionar Medição</button>
    <div class="col-12 h450-r">
        <table class="table table-striped">
            <thead>
                <th>Data Cadastro</th>
                <th>Parâmetro</th>
                <th>Limite</th>
                <th>Valor da Medição</th>
                <th>Ações</th>
            </thead>
            <tbody>
                <tr v-for="i in listaMedicoesAdicionadas">
                    <td v-text="vmGlobal.frontEndDateFormat(i.DataCadastro)"></td>
                    <td v-text="i.NomeParametro"></td>
                    <td v-text="i.Limite +': '+ i.ValorLimite"></td>
                    <td>
                        <template v-if="mostrarInputUpdate">
                                    <input @blur="salvarMedicao(i.CodigoRecursosHidricosRelatorioMedicao , true, $event)" type="text" class="form-control">
                        </template>
                        <template v-else>
                            {{i.Valor}}
                        </template>
                    </td>
                    <td>
                        <button @click="mostrarInputUpdate = true;" class="btn btn-info btn-sm"><i class="fa fa-pen"></i></button>
                        <button @click="deletarMedicao(i.CodigoRecursosHidricosRelatorioMedicao)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-12 text-right">
        <button @click="mostrarMedicoes = false; vmInsercao.mostrarMedicoes = true" class="btn btn-secondary">Voltar</button>
    </div>



    <div class="modal-w98-h50 col-6 h450-r sombra-2 p-4" v-show="mostrarAddMedicao">
        <button @click="mostrarListaParametros = true; mostrarInputAdd = false; mostrarAddMedicao = false" class="btn btn-sm btn-r mr-4"><i class="fa fa-times"></i></button>
        <h4>Adicionar Medição {{nomeParametroSelecionado ? nomeParametroSelecionado : ''}}</h4>
        <div class="border p-4" v-show="mostrarListaParametros">
            <div class="alert alert-info">
                <p>Para avançar selecione um parametro!</p>
            </div>

            <table class="table table-striped mt-4">
                <thead>
                    <th>Parâmetro</th>
                    <th>Unindade Medida</th>
                    <th>Limite</th>
                    <th>Selecionar</th>
                </thead>
                <tbody>
                    <tr v-for="i in listaParametros">
                        <td v-text="i.NomeParametro"></td>
                        <td v-text="i.UnidadeMedida"></td>
                        <td v-text="i.Limite +': '+ i.ValorLimite"></td>
                        <td>
                            <button @click="parametroSelecionado = i.CodigoRecursosHidricosParametros; nomeParametroSelecionado= i.NomeParametro;  mostrarListaParametros = false; mostrarInputAdd = true" class="btn btn-info"><i class="fa fa-check"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="border p-4" v-show="mostrarInputAdd">
            <div class="alert alert-info">
                <p>Digite o valor da medição!</p>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Valor</span>
                </div>
                <input type="text" class="form-control" v-model="valorMedicao">
            </div>
            <div class="col-12 text-right">
                <button @click="salvarMedicao();  mostrarListaParametros = true; mostrarInputAdd = false; mostrarAddMedicao = false" class="btn btn-success">Salvar</button>
            </div>
        </div>

    </div>
</div>