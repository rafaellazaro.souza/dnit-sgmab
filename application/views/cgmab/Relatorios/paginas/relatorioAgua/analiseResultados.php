<div id="analiseResultados">
    <div class="row">
        <div class="col 12">
            <h5>Gráfico de Resultados Gerais</h5>
            <div class="row">
                <div class="col-12">
                    <div id="graficoIQAResultados"></div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col 12 col-sm-12">
            <h5>Análise de Resultados</h5>
            <textarea id="txtAnaliseResultadosIQA" name="editorAnaliseResultados1"></textarea>
           <button @click="salvarAnalise(false, 1)" class="btn btn-success"><i class="fa fa-save"></i> Salvar Analise</button>
            <h5>Conclusões</h5>
            <textarea id="txtConclusoesIQA" name="editorAnaliseResultados2"></textarea>
            <button @click="salvarAnalise(false, 2)" class="btn btn-success"><i class="fa fa-save"></i> Salvar Analise</button>
            <h5>Bibliografia</h5>
            <textarea id="txtBibliografiaIQA" name="editorAnaliseResultados3"></textarea>
            <button @click="salvarAnalise(false, 3)" class="btn btn-success"><i class="fa fa-save"></i> Salvar Analise</button>
        </div>
    </div>
</div>