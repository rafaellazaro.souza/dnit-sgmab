<div id="vmIntroducao" class="row mt-5">

    <div class="col-12 col-sm-4 border-right pr-3 ">
        <div class="card" style="min-height: auto;">
            <div class="header">
                <h5>Documentação Auxiliar (ART e Outros)</h5>
                <h2><strong>{{totalDocumentos}}</strong> Documentos <i class="fas fa-file-alt"></i></h2>
            </div>
            <div class="body no-shadow border">
                <form enctype="multipart/form-data" method="POST">
                    <input type="file" id="documentosMaterial" ref="documentos" @change="qtdDocumentos" name="documentos[]" class="dropify input-toBlock" multiple>
                </form>
            </div>
            <button class="btn btn-outline-secondary mt-2 float-right btn-toBlock" @click="addDocumentos">Importar</button>
        </div>
        <div class="h450-r">
            <table class="table table-striped mt-2">
                <tbody>
                    <tr v-for="i in listaDocumentos">
                        <td v-text="i.Nome"></td>
                        <td><a :href="i.Caminho"><i class="fa fa-download"></i></a></td>
                        <td><button @click="deletarArquivo(i.CodigoPassagemDocumento, i.Caminho)" class="btn btn-danger btn-sm btn-toBlock"><i class="fa fa-times"></i></button></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
    <div class="col-12 col-sm-8 pl-3">
        <h4>Introdução</h4>
        <textarea name="editorIntro" id="txtIntro"></textarea>
        <div class="w-100 text-right">
            <button v-if="introducao.length > 0" @click="salvarDados(introducao[0].CodigoIntroducaoAnalise)" class="btn btn-success mt-4 btn-toBlock">Atualizar Introdução</button>
            <button v-else @click="salvarDados()" class="btn btn-success mt-4 btn-toBlock">Salvar Introdução</button>
        </div>
    </div>
</div>