<div class="row" id="vmInsercao">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card" style="min-height: auto;">
                <h5>Animais Capturados</h5>
                <div style="min-height: 100px;" class="no-shadow body">
                    <!-- <div class="btn-group" role="group" aria-label="Grupo de botões com dropdown aninhado">
                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-filter"></i> Nome de Grupo
                            </button>
                            <div class="dropdown-menu" style="left: -112px;" aria-labelledby="btnGroupDrop1">
                                <a class="dropdown-item" href="javascript:;" @click="filtroSelecionado = i" v-for="i in listaAnimaisFiltro" v-text="i"></a>
                               
                            </div>
                        </div>
                    </div>  -->
                    <a download="Relatorio_monitoramento_fauna.xlsx" href="<?= base_url('webroot/uploads/modelos-excel/Relatorio_passagem_fauna.xlsx') ?>" class="float-right btn btn-info"><i class="fas fa-download"></i> Baixar modelo .xlsx</a>

                    <button data-toggle="modal" data-target="#defaultModal" class="float-right btn btn-success mb-3 btn-toBlock"><i class="fas fa-upload"></i> Importar dados</button>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered ">
                            <thead>
                                <tr>
                                    <!-- <th rowspan="2"><i class="fa fa-photo"></i></th> -->
                                    <th rowspan="2">Codigo Animal </th>
                                    <th rowspan="2">Tipo de Identificação</th>
                                    <th colspan="5">Taxonomia</th>
                                    <th rowspan="2">Nome Comum</th>
                                    <th rowspan="2">Observações</th>
                                    <th rowspan="2">Ações</th>
                                </tr>
                                <tr>
                                    <th>Grupo</th>
                                    <th>Classe</th>
                                    <th>Ordem</th>
                                    <th>Família</th>
                                    <th>Espécie</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(i, index) in listaAnimais">
                                    <!-- <td>
                                        <i v-if="i.Caminho != null" class="fa fa-check-circle" aria-hidden="true"></i>
                                        <i v-if="i.Caminho == null" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    </td> -->
                                    <td>{{i.CodigoAnimalCapturado}}</td>
                                    <td v-text="i.TipoRegistro"></td>
                                    <td v-text="i.GrupoFaunistico"></td>
                                    <td v-text="i.Classe"></td>
                                    <td v-text="i.Ordem"></td>
                                    <td v-text="i.Familia"></td>
                                    <td v-text="i.Especie"></td>
                                    <td v-text="i.NomeComum"></td>

                                    <td>
                                        <a href="javascript:;" :title="i.Observacao ? i.Observacao : 'Nenhuma Observação'"><i class="fa fa-eye"></i></a>
                                    </td>

                                    <td><button @click="deletarAnimal(i.CodigoAnimalCapturado, index)" onclick="return confirm('Tem Cerreza que deseja deletar este registro?')" class="btn btn-outline-danger btn-sm btn-toBlock"><i class="fa fa-times"></i></button></td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="alert alert-info">
                <p>Antes de efetuar o upload renomear foto para o exemplo: CodigoAnimal_nomeAnimal.jpg.</p>
            </div>
            <div class="card" style="min-height: auto;">
                <div class="header">
                    <h2><strong>{{totalFotos}}</strong> Animais <i class="fas fa-images"></i></h2>
                </div>
                <div class="body no-shadow border">
                    <form enctype="multipart/form-data" method="POST">
                        <input type="file" id="imagemMaterial" @change="qtdFotos" name="fotos[]" class="dropify input-toBlock " multiple>
                    </form>
                </div>
                <button class="btn btn-outline-secondary mt-2 float-right btn-toBlock" @click="addFotos">IMPORTAR</button>
            </div>
            <!-- <div class="row mb-4 mt-4" v-if="pendencias">
                <div class="col-12">
                    <div class="alert alert-info">
                        <h4><b>Atenção: </b>Ao inserir novas fotos,é preciso definir qual é o animal e a armadilha de cada foto!</h4>
                    </div>
                </div>
            </div> -->
            <div class="row file_manager">
                <div v-for="(i, index) in listaFotosAnimais" v-if="i.CaminhoFotoAnimaisCapturados != null" :key="index" class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card " :class="[i.Lat == '' || i.Lat == null ? 'borderRed' : 'borderGreen']">
                        <div class="file">
                            <div class="hover">
                                <button @click="deletarFotos(i.CodigoFotoAnimalCapturado, i.CaminhoFotoAnimaisCapturados)" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger btn-toBlock">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                            </div>
                            <div class="image">
                                <img :src="i.CaminhoFotoAnimaisCapturados" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
                            </div>
                            <div class="file-name d-flex justify-content-between">
                                <div class="alert alert-warning" v-show="!i.CodigoAnimalCapturado">
                                    <p>Por favor atribua um animal e uma armadilha a esta foto!</p>
                                </div>
                            </div>

                            <div class="file-name" v-show="!pendencias">

                                <small>Codigo Foto:<span class="date text-info"><b>{{i.CodigoFotoAnimalCapturado}}</b></span></small>
                                <small>Estrutura :<span class="date text-info"><b>{{i.Estrutura}}</b></span></small>
                                <small>Tipo Estrutura :<span class="date text-info"><b>{{i.TipoEstrutura}}</b></span></small>
                                <small>UF :<span class="date text-info"><b>{{i.UF}}</b></span></small>
                                <small>BR :<span class="date text-info"><b>{{i.BR}}</b></span></small>
                                <small>Dimensões :<span class="date text-info"><b>
                                            <template v-if="i.Diametro"> Diametro: {{i.Diametro}}</template>
                                            <template v-else> Altura: {{i.Altura}} | Largura: {{i.Largura}}</template>
                                        </b></span></small>
                            </div>
                            <div class="file-name" v-show="!pendencias">

                                <small>Nome Foto<span class="date text-info"><b>{{i.NomeFoto}}</b></span></small>
                                <small v-if="i.Lat != ''">Latitude<span class="date text-info">{{i.Lat}}</span></small>
                                <small v-else>Latitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                <small v-if="i.Lng != ''">Longitude<span class="date text-info">{{i.Lng}}</span></small>
                                <small v-else>Longitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                                <small v-if="i.DataFoto != '1900-01-01 00:00:00.000'">Data da fotografia <span class="date text-info">{{i.DataFoto}}</span></small>
                                <small v-else>Data da fotografia<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
                            </div>
                            <div class="file-name">
                                <div class="input-group mb-3" v-if="i.StatusFoto != 0">
                                    <div class="input-group-prepend">
                                        <span id="basic-addon1" class="input-group-text">
                                            Animal
                                        </span>
                                    </div>


                                    <div class="form-control">
                                        {{i.CodigoAnimalCapturado+' - '+i.NomeComum}}
                                    </div>



                                </div>
                                <div class="input-group mb-1">
                                    <div class="input-group-prepend">
                                        <span id="basic-addon1" class="input-group-text">
                                            Passagens
                                        </span>
                                    </div>



                                    <select @change="getArmadilhas($event)" class="form-control">
                                        <option value="">Selecione uma Passagem</option>
                                        <option :value="i.CodigoAreaApoio" v-for="i in listaPassagens" v-text="i.Estrutura+' - '+i.TipoEstrutura+' -  '+i.Nome+' - UF: '+i.UF+' - BR: '+i.BR+'- Dimensões: '+i.Altura+'x'+i.Largura+'x'+i.Diametro"></option>
                                    </select>

                                </div>
                                <div class="input-group mb-1">
                                    <div class="input-group-prepend">
                                        <span id="basic-addon1" class="input-group-text">
                                            Armadilha
                                        </span>
                                    </div>

                                    <select v-if="!i.CodigoConfiguracaoPassagemFaunaArmadilha  || listaArmadilhas.length > 0" class="form-control" @change="updateArmadilha(i.CodigoAnimalCapturado, $event, i.CodigoFotoAnimalCapturado)">
                                        <option selected>Selecione uma armadilha abaixo</option>
                                        <option v-for="i in listaArmadilhas" :value="i.CodigoConfiguracaoPassagemFaunaArmadilha" v-text="i.NomeEquipamento"></option>
                                    </select>

                                    <select name="" disabled v-else>
                                        <option value="">{{i.NomeEquipamento + 'NS: '+i.NumeroSerie}}</option>
                                    </select>

                                </div>
                            </div>
                            <div class="file-name">
                                <label cla>UTM (Data Sirgas 2000)</label><br>
                                <div class="row">
                                    <div class=" col-md-3">
                                        <label for="email_address">Zona</label>
                                        <div class="form-group">
                                            <template v-if="i.ZonaFoto">
                                                <div class="form-control">{{i.ZonaFoto}}</div>
                                            </template>
                                            <input v-else class="form-control" @blur="setDadosUpdate('ZonaFoto', $event)" placeholder="">
                                        </div>
                                    </div>
                                    <div class=" col-md-3">
                                        <label for="email_address">CN</label>
                                        <div class="form-group  ">
                                            <template v-if="i.CNFoto">
                                                <div class="form-control">{{i.ElevacaoFoto}}</div>
                                            </template>
                                            <input v-else class="form-control" @blur="setDadosUpdate('CNFoto', $event)" placeholder="">
                                        </div>
                                    </div>
                                    <div class=" col-md-3">
                                        <label for="email_address">CE</label>
                                        <div class="form-group  ">
                                            <template v-if="i.CEFoto">
                                                <div class="form-control">{{i.CEFoto}}</div>
                                            </template>
                                            <input v-else class="form-control" @blur="setDadosUpdate('CEFoto', $event)" placeholder="">
                                        </div>
                                    </div>
                                    <div class=" col-md-3">
                                        <label for="email_address  ">Elevação (m)</label>
                                        <div class="form-group ">
                                            <template v-if="i.ElevacaoFoto">
                                                <div class="form-control">{{i.ElevacaoFoto}}</div>
                                            </template>
                                            <input v-else class="form-control" @blur="setDadosUpdate('ElevacaoFoto', $event, true, i.CodigoFotoAnimalCapturado)" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="file-name" v-show="!pendencias">
                                <small>Grupo<span class="date text-info"><b>{{i.NomePontoMonitoramento}}</b></span></small>
                                <small>Classe<span class="date text-info"><b>{{i.Classe}}</b></span></small>
                                <small>Ordem<span class="date text-info"><b>{{i.Ordem}}</b></span></small>
                                <small>Família<span class="date text-info"><b>{{i.Familia}}</b></span></small>
                                <small>Especie<span class="date text-info"><b>{{i.Especie}}</b></span></small>
                                <small>NomeComum<span class="date text-info"><b>{{i.NomeComum}}</b></span></small>
                            </div>
                            <div class="file-name">
                                <label for="email_address">Observações</label>
                                <div class="form-group">
                                    <template v-if="i.ObservacoesFoto">
                                        <div class="form-control pb-4">{{i.ObservacoesFoto}}</div>
                                    </template>
                                    <textarea v-else rows="4" @blur="setDadosUpdate('ObservacoesFoto', $event, true, i.CodigoFotoAnimalCapturado)" class="form-control"></textarea>
                                </div>
                            </div>
                            <div v-bind:id="'error-'+index">

                            </div>
                            <div class="file-name">

                                <button @click="deletarFotos(i.CodigoFotoAnimalCapturado, i.CaminhoFotoAnimaisCapturados)" class="btn btn-danger btn-toBlock">Excluir</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal importar pontos de coleta  -->

    <div class="modal fade bd-example-modal-xl" id="defaultModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Importa Animais Capturados
                        <br /><small>Utilizar o modelo excel disponibilizado</small>
                    </h4>
                </div>
                <div class="modal-body">
                    <template v-show="mostrarListaExcel === false">
                        <input type="file" id="file" ref="file" name="" class="dropify" multiple>
                    </template>
                    <div class="w-100 h450-r">
                        <table class="table table-striped" v-show="mostrarListaExcel" id="listaPontosExcel">
                            <thead>
                                <tr>
                                    <th>Tipo de Identificação</th>
                                    <th>Nome Comum</th>
                                    <th>Grupo</th>
                                    <th>Classe</th>
                                    <th>Ordem</th>
                                    <th>Família</th>
                                    <th>Espécie</th>
                                    <th>Observações</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="i in listaExcel">
                                    <td v-text="i.TipoRegistro"></td>
                                    <td v-text="i.NomeComum"></td>
                                    <td v-text="i.GrupoFaunistico"></td>
                                    <td v-text="i.Classe"></td>
                                    <td v-text="i.Ordem"></td>
                                    <td v-text="i.Familia"></td>
                                    <td v-text="i.Especie"></td>
                                    <td v-text="i.Observacoes"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button @click="importarDadosInsercao()" type="button" class="btn btn-default btn-round waves-effect" v-show="mostrarListaExcel === false">Importar</button>
                    <button @click="salvarAnimaisImportados()" type="button" class="btn btn-default btn-round waves-effect" v-show="mostrarListaExcel">Confirmar e Salvar</button>
                </div>
            </div>
        </div>
    </div> <!-- modal importar pontos de coleta -->
</div>