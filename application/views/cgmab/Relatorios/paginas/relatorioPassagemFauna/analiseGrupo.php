<div class="row" id="analiseGrupo">
    <div class="row">
        <div :class="[codigoGrupoFaunistico ? 'col-12 mb-2' : 'col-12 mb-2 sombra-2 p-5']">
            <div class="dropdown mb-4">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Selecione o Gupo Faunístico
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a @click="codigoGrupoFaunistico = i.CodigoGrupoFaunistico; nomeGrupoSelecionado = i.NomeGrupoFaunistico" v-for="i in listaGruposFaunisticos" class="dropdown-item" href="#" v-text="i.NomeGrupoFaunistico">
                    </a>

                </div>
            </div>
            <h5>{{nomeGrupoSelecionado}}</h5>
        </div>


        <div class="col-12 border-right mt- 2" v-show="mostrarGrafico">

            <div id="graficoRiquezaAbundancia" style="width: 1200px; height: 400px;"></div>
        </div>
        <div class="col-12">
            <h4>FRI por Passagem de Fauna</h4>
            <div class="h450-r">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID Passagem</th>
                            <th>Estrutura</th>
                            <th>TipoEstrutura</th>
                            <th>Dimensões</th>
                            <th>Total de Registros</th>
                            <th>Percentual %</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="i in listaAnimaisCapurados">
                            <td v-text="i.CodigoAreaApoio"></td>
                            <td v-text="i.Estrutura +'-'+i.Nome"></td>
                            <td v-text="i.TipoEstrutura"></td>
                            <td>
                                <template v-if="i.Diametro">Diametro: {{i.Diametro}}</template>
                                <template v-else>Altura: {{i.Diametro}} | Largura: {{i.Largura}}</template>
                            </td>
                            <td v-text="i.TotalRegistrosGrupoFaunistico"></td>
                            <td v-text="((i.TotalRegistrosGrupoFaunistico / i.TotalRegistros) * 100).toFixed()+'%'"></td>
                        </tr>
                        <tr class="border-bottom">
                            <td colspan="4" class="text-right"><b>Total Geral</b></td>
                            <td colspan="2">{{listaAnimaisCapurados.length > 0 ? listaAnimaisCapurados[0].TotalRegistros : ''}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-12 border-top">
            <h4>Analise de Resultados</h4>
            <textarea name="analiseResultadosFauna" id="txtAnaliseResultadosFauna"></textarea>

        </div>
        <div class="col-12 text-right mt-3">
            <button @click="salvarTexto()" class="btn btn-success btn-toBlock">Salvar</button>
        </div>
    </div>


</div>