<div class="row" id="discucoes">
    <div class="col-12">
        <h4>Análises dos Resultados e Discussões</h4>
        <textarea name="analiseResultadosDiscucoes" id="txTanaliseResultadosDiscucoes"></textarea>
        <div class="col-12 text-right">
            <button v-if="analiseResultadosDiscucoes.length == 0" @click="salvarTexto('analiseResultadosDiscucoes')" class="btn btn-success btn-toBlock">Salvar</button>
            <button v-else @click="salvarTexto('analiseResultadosDiscucoes', analiseResultadosDiscucoes[0].CodigoIntroducaoAnalise)" class="btn btn-success btn-toBlock">Atualizar</button>
        </div>
        <h4>Conclusões</h4>
        <textarea name="analiseResultadosConclusoes" id="txTanaliseResultadosConclusoes"></textarea>
        <div class="col-12 text-right">
            <button v-if="analiseResultadosConclusoes.length == 0" @click="salvarTexto('Conclusao')" class="btn btn-success btn-toBlock">Salvar</button>
            <button v-else @click="salvarTexto('Conclusao', analiseResultadosConclusoes[0].CodigoIntroducaoAnalise)" class="btn btn-success btn-toBlock">Atualizar</button>
        </div>
        <h4>Bibliografia</h4>
        <textarea name="analiseResultadosBibliografia" id="txTanaliseResultadosBibliografia"></textarea>
        <div class="col-12 text-right">
            <button v-if="analiseResultadosBibliografia.length == 0" @click="salvarTexto('Bibliografia')" class="btn btn-success btn-toBlock">Salvar</button>
            <button v-else @click="salvarTexto('Bibliografia', analiseResultadosBibliografia[0].CodigoIntroducaoAnalise)" class="btn btn-success btn-toBlock">Atualizar</button>
        </div>
    </div>
</div>