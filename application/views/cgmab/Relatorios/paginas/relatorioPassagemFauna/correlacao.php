<div class="row" id="correlacao">
    <div class="col-12">
        <h4>Análises dos Resultados e Discussões</h4>
        <textarea name="analiseResultadosCorrelacao" id="txTanaliseResultadosCorrelacao"></textarea>
        <div class="col-12 text-right">

            <button v-if="analiseResultadosCorrelacao.length == 0" @click="salvarTexto('AnaliseResultadoProgramaAtropelamento')" class="btn btn-success btn-toBlock">Salvar</button>

            <button v-else @click="salvarTexto('AnaliseResultadoProgramaAtropelamento', analiseResultadosCorrelacao[0].CodigoIntroducaoAnalise)" class="btn btn-success btn-toBlock">Atualizar</button>

        </div>
        <h4>Conclusões</h4>
        <textarea name="analiseResultadosCorrelacaoConclusao" id="txTanaliseResultadosCorrelacaoConclusao"></textarea>
        <div class="col-12 text-right">

            <button v-if="analiseResultadosCorrelacaoConclusao.length == 0" @click="salvarTexto('ConclusaoProgramaAtropelamento')" class="btn btn-success btn-toBlock">Salvar</button>

            <button v-else @click="salvarTexto('ConclusaoProgramaAtropelamento', analiseResultadosCorrelacaoConclusao[0].CodigoIntroducaoAnalise)" class="btn btn-success btn-toBlock">Atualizar</button>

        </div>

    </div>
</div>