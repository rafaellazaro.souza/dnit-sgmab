<div class="row" id="discussoesConclusoesSupreVege">
	<div class="col-12 mb-3">
		<h5>Conclusões</h5>
		<conclusoes-supressao-comp :names="'Conclusao'"></conclusoes-supressao-comp>

	</div>
	<div class="col-12 mb-3">
		<h5>Bibliografia</h5>
		<conclusoes-supressao-comp :names="'Bibliografia'"></conclusoes-supressao-comp>

	</div>
</div>

