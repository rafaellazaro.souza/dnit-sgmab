<!-- css mapa  -->
<?php $this->load->view('../../webroot/arquivos/plugins/leatleft/css/leatleft.php') ?>
<!-- js mapa  -->
<?php $this->load->view('../../webroot/arquivos//plugins/leatleft/js/leatleft.php') ?>


<div id="insercaoDadosSupreVege" class="row file_manager">
	<h5>Importar ShapeFile</h5>

	<div class="row col-md-12 col-sm-12">
		<div class="form-group col-12 col-sm-6 col-md-6">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">
						Número ASV
					</div>
				</div>
				<select class="form-control show-tick ms select2" v-model="codigoFloraSupressao">
					<option v-for="i in listaConfiguracaoASV" :value="i.CodigoFloraSupressao">{{i.NumeroLicenca}}</option>
				</select>
			</div>

		</div>
		<div class="form-group col-12 col-sm-6 col-md-6">
			<div v-if="novasCoordenadasShapeFile.length == null && !codigoFloraSupressao != true">
				<div class="input-group">
					<div class="custom-file">
						<input type="file" ref="ShapeFile" name="ShapeFile" class="custom-file-input input-toBlock" accept=".zip" id="ShapeFile">
						<label class="custom-file-label" id="labelShapeFile" for="ShapeFile">Selecione um arquivo ZIP</label>
					</div>
					<div class="input-group-append">
						<button @click="addShapefile()" class="btn btn-outline-secondary btn-toBlock" type="button"><span class="spinnerShapefile"></span> Importar</button>
					</div>
				</div>
			</div>
			<button v-if="novasCoordenadasShapeFile.length && !dadosAtropelamentoObsercacoes.GraficoHotSpots" @click="deletarCoordenadasShapeFile()" class="btn btn-outline-secondary float-right btn-toBlock" type="button">Excluir</button>
			<button v-if="novasCoordenadasShapeFile.length && !dadosAtropelamentoObsercacoes.GraficoHotSpots" @click="salvarShapefile()" class="btn btn-outline-secondary float-right btn-toBlock" type="button"><span class="spinnerShapefile"></span> Salvar</button>
		</div>
	</div>

	<div class="no-shadow body col-12">
		<div class="border border-secondary" id="mapid" style="height: 600px; width: 100%"></div>
	</div>

</div>