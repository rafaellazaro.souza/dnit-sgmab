<div class="row" id="app-controlePilhas">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary btn-sm float-right btn-toBlock" @click="novaPilha()"><i class="fa fa-plus"></i> Nova Pilha</button>
                <table class="table table-sm table-hover table-striped" style="width: 100%;" id="tblPilhas">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Número da Pilha / Fustes</th>
                            <th>Número ASV</th>
                            <th>Tipo de Pilha</th>
                            <th>Data de Criação</th>
                            <th>Zona</th>
                            <th>CN</th>
                            <th>CE</th>
                            <th>Elevacão</th>
                            <th>Observacões</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="p in listaDePilhas">
                            <td>{{p.CodigoPilha}}</td>
                            <td>{{p.NumeroPilha}}</td>
                            <td>{{getAsvPilha(p.CodigoFloraSupressao)}}</td>
                            <td>{{(p.TipoPilha == 1)? 'Comum' : 'Ameaçada'}}</td>
                            <td>{{vmGlobal.frontEndDateFormat(p.DataCriacao)}}</td>
                            <td>{{p.Zona}}</td>
                            <td>{{p.CN}}</td>
                            <td>{{p.CE}}</td>
                            <td>{{p.Elevacao}}</td>
                            <td>{{p.Observacao}}</td>
                            <td>
                                <button class="btn btn-outline-secondary btn-sm" @click="updatePilha(p)"><i class="fa fa-edit"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <ul class="nav nav-tabs border-bottom" id="tabsSupressaoVegetacao" role="tablist">
            <li class="nav-item" v-if="showFormNovaPilha">
                <a class="nav-link border active" id="cadastroPilhas-tab" data-toggle="tab" href="#cadastro-pilhas" role="tab" aria-controls="introducaoSupressaoVegetacao" aria-selected="true">Cadastro de Pilhas</a>
            </li>
            <template v-if="showSelectedPilha">
                <li class="nav-item">
                    <a class="nav-link border" id="controleFoosts-tab" data-toggle="tab" href="#controles-foosts" role="tab" aria-controls="insercao-dados" aria-selected="false">Controle de Pllhas e Fustes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link border" id="destinacaoPilha-tab" data-toggle="tab" href="#destinacao-pilhas" role="tab" aria-controls="analise" aria-selected="false">Destinação</a>
                </li>
            </template>
        </ul>
        <div class="tab-content p-4" id="myTabContent">
            <button v-if="showFormNovaPilha || showSelectedPilha" class="btn btn-sm btn-outline-default float-right" @click="closeAll()">Fechar</button>
            <div v-if="showFormNovaPilha" class="tab-pane fade show active" id="cadastro-pilhas" role="tabpanel" aria-labelledby="cadastroPilhas-tab">
                <form action="" @submit.prevent="salvarPilha()" id="formPilha">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Número da Pilha/Fuste</span>
                        </div>
                        <input class="form-control input-toBlock" v-model="pilha.NumeroPilha" />
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Número da ASV</span>
                        </div>
                        <select name="" id="" class="form-control input-toBlock show-tick ms select2" v-model="pilha.CodigoFloraSupressao">
                            <option value="">-- Selecione --</option>
                            <option v-for="i in listaLicencasAmbientais" :value="i.CodigoFloraSupressao">{{i.NumeroLicenca}} - {{i.NomeOrgaoExpeditor}}</option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Tipo de Pilha</span>
                        </div>
                        <select name="" id="" class="form-control input-toBlock show-tick ms select2" v-model="pilha.TipoPilha">
                            <option value="">-- Selecione --</option>
                            <option value="1">Comum</option>
                            <option value="2">Espécie Ameaçadas</option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Data de Criação</span>
                        </div>
                        <input type="date" class="form-control input-toBlock" :min="dataMinima" :max="dataMaxima" v-model="pilha.DataCriacao" />
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Latitude</span>
                        </div>
                        <input type="text" class="form-control input-toBlock" v-model="pilha.Latitude">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Longitude</span>
                        </div>
                        <input type="text" class="form-control input-toBlock" v-model="pilha.Logitude">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Zona</span>
                        </div>
                        <input type="text" class="form-control input-toBlock" v-model="pilha.Zona">
                        <div class="input-group-prepend">
                            <span class="input-group-text">CN</span>
                        </div>
                        <input type="text" class="form-control input-toBlock" v-model="pilha.CN">
                        <div class="input-group-prepend">
                            <span class="input-group-text">CE</span>
                        </div>
                        <input type="text" class="form-control input-toBlock" v-model="pilha.CE">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Elevação (m)</span>
                        </div>
                        <input type="text" class="form-control input-toBlock" v-model="pilha.Elevacao">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Observações</span>
                        </div>
                        <textarea class="form-control input-toBlock" v-model="pilha.Observacao"></textarea>
                    </div>
                    <button type="submit" class="btn btn-sm btn-success btn-toBlock">Salvar</button>
                    <button type="button" class="btn btn-sm btn-danger" @click="showFormNovaPilha = false; showSelectedPilha = false;">Cancelar</button>
                </form>
            </div>
            <template v-if="showSelectedPilha">
                <div class="tab-pane fade" id="controles-foosts" role="tabpanel" aria-labelledby="controleFoosts-tab">
                    <h3>Controle de Pilhas e Fustes</h3>
                    <button class="btn btn-primary btn-sm float-right btn-toBlock" @click="novoControle()"><i class="fa fa-plus"></i> Novo Controle</button>
                    <table class="table table-sm table-hover table-striped mb-5" style="width: 100%;" id="tblControle">
                        <thead>
                            <tr>
                                <th>Mês</th>
                                <th>Data do Registro</th>
                                <th>ASV</th>
                                <th>Nº</th>
                                <th>Tipo</th>
                                <th>Espécies</th>
                                <th>Nº de Indivíduos</th>
                                <th>Volume (m³)</th>
                                <th>Observações</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="p in listaControle">
                                <td>{{mesPilha}}</td>
                                <td>{{formatarData(p.DataCriacao)}}</td>
                                <td>{{getAsvPilha(pilha.CodigoFloraSupressao)}}</td>
                                <td>{{pilha.NumeroPilha}}</td>
                                <td>{{(pilha.TipoPilha == 1)? 'Comum' : 'Ameaçada'}}</td>
                                <td>{{p.Especies}}</td>
                                <td>{{p.NumeroDeIndividuos}}</td>
                                <td>{{p.Volume}}</td>
                                <td>{{p.Observacaoes}}</td>
                                <td>
                                    <button class="btn btn-sm btn-outline-secondary" @click="editControlePilha(p)"><i class="fa fa-edit"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <form action="" @submit.prevent="salvarControle()" v-if="novoControlePilha" class="mt-5" id="formControle">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Mês</span>
                            </div>
                            <input type="number" class="form-control input-toBlock" disabled :value="mesPilha" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">ASV</span>
                            </div>
                            <input type="text" class="form-control input-toBlock" disabled :value="getAsvPilha(pilha.CodigoFloraSupressao)" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Tipo de Pilha</span>
                            </div>
                            <input type="text" class="form-control input-toBlock" disabled :value="(pilha.TipoPilha == 1)? 'Comum' : 'Ameaçada'" />
                        </div>
                        <div class="input-group mb-3" v-if="controle.DataCriacao">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Data do Registro</span>
                            </div>
                            <input type="date" class="form-control input-toBlock" disabled v-model="controle.DataCriacao" />
                        </div>
                        <div class=" input-group mb-3" v-show="isEspeciesAmeacada">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Espécies</span>
                            </div>
                            <input type="text" class="form-control input-toBlock" v-model="controle.Especies" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nº de Indivíduo</span>
                            </div>
                            <input type="number" class="form-control input-toBlock" v-model="controle.NumeroDeIndividuos" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Volume (m³)</span>
                            </div>
                            <input type="text" class="form-control input-toBlock" v-model="controle.Volume" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Observação</span>
                            </div>
                            <textarea v-model="controle.Observacaoes" class="form-control input-toBlock"></textarea>
                        </div>
                        <div class="form-control">
                            <label for="">Fotos</label>
                            <input type="file" class="dropify input-toBlock" ref="controleFotos" multiple>
                        </div>
                        <button type="submit" class="btn btn-sm btn-success btn-toBlock">Salvar</button>
                        <button type="button" class="btn btn-sm btn-danger" @click="novoControlePilha = false; controleFotos = [] ">Cancelar</button>
                    </form>
                    <div class="col-12 container-fluid mt-2">
                        <div class="row file_manager">
                            <template>
                                <card-fotos v-for="(foto,i) in controleFotos" :disabled="isControleMesAnterior" key="'controleFoto'+i" :foto="foto" @excluir-foto="getFotos('controle')"></card-fotos>
                            </template>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade table-responsive" id="destinacao-pilhas" role="tabpanel" aria-labelledby="destinacaoPilha-tab">
                    <h3>Destinação</h3>
                    <button class="btn btn-primary btn-sm float-right btn-toBlock" @click="novaDestinacao()"><i class="fa fa-plus"></i> Nova Destinação</button>
                    <table class="table table-sm table-hover table-striped mb-5" style="width: 100%;" id="tblDestinacao">
                        <thead>
                            <tr>
                                <th>Mês</th>
                                <th>Data do Registro</th>
                                <th>ASV</th>
                                <th>Nº Pilha</th>
                                <th>Tipo</th>
                                <th>Espécies</th>
                                <th>Nº de Indivíduos</th>
                                <th>Volume (m³)</th>
                                <th>Data de Envio</th>
                                <th>Uso da Madeira</th>
                                <th>Destinatario</th>
                                <th>Observações</th>
                                <th>-</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="p in listaDestinacao">
                                <td>{{mesPilha}}</td>
                                <td>{{formatarData(p.DataCriacao)}}</td>
                                <td>{{getAsvPilha(pilha.CodigoFloraSupressao)}}</td>
                                <td>{{pilha.NumeroPilha}}</td>
                                <td>{{(pilha.TipoPilha == 1)? 'Comum' : 'Ameaçada'}}</td>
                                <td>{{p.Especies}}</td>
                                <td>{{p.NumeroDeIndividuos}}</td>
                                <td>{{p.Volume}}</td>
                                <td>{{formatarData(p.DataEnvio)}}</td>
                                <td>{{p.UsoDaMadeira}}</td>
                                <td>{{p.Destinatario}}</td>
                                <td>{{p.Observacaoes}}</td>
                                <td>
                                    <button class="btn btn-sm btn-outline-secondary" @click="editDestinacao(p)"><i class="fa fa-edit"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <form action="" @submit.prevent="salvarDestinacao()" v-if="novaDesitnacaoPilha" class="mt-5" id="formDestinacao">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Mês</span>
                            </div>
                            <input type="number" class="form-control input-toBlock" disabled :value="mesPilha" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">ASV</span>
                            </div>
                            <input type="text" class="form-control input-toBlock" disabled :value="getAsvPilha(pilha.CodigoFloraSupressao)" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Tipo de Pilha</span>
                            </div>
                            <input type="text" class="form-control input-toBlock" disabled :value="(pilha.TipoPilha == 1)? 'Comum' : 'Ameaçada'" />
                        </div>
                        <div class="input-group mb-3" v-if="destinacao.DataCriacao">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Data do Registro</span>
                            </div>
                            <input type="date" class="form-control input-toBlock" disabled v-model="destinacao.DataCriacao" />
                        </div>
                        <div class="input-group mb-3" v-show="isEspeciesAmeacada">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Espécies</span>
                            </div>
                            <input type="text" class="form-control input-toBlock" v-model="destinacao.Especies" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nº de Indivíduo</span>
                            </div>
                            <input type="number" class="form-control input-toBlock" v-model="destinacao.NumeroIndividuos" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Volume (vm³)</span>
                            </div>
                            <input type="text" class="form-control input-toBlock" v-model="destinacao.Volume" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Data de Envio</span>
                            </div>
                            <input type="date" class="form-control input-toBlock" v-model="destinacao.DataEnvio" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Uso da Madeira</span>
                            </div>
                            <select name="" id="" class="form-control input-toBlock show-tick ms select2" v-model="destinacao.UsoDaMadeira">
                                <option value="">-- Selecione --</option>
                                <option v-for="(uso,key) in usoMadeira" :value="key+1">{{uso}}</option>
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Destinatário</span>
                            </div>
                            <input type="text" class="form-control input-toBlock" v-model="destinacao.Destinatario" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Observação</span>
                            </div>
                            <textarea class="form-control input-toBlock" v-model="destinacao.Observacao"></textarea>
                        </div>
                        <div class="form-row">
                            <div class="form-control input-toBlock col-6 pr-1">
                                <label for="">Fotos</label>
                                <input type="file" class="dropify input-toBlock" ref="destinacaoFotos" multiple>
                            </div>
                            <div class="form-control input-toBlock col-6 pl-1">
                                <label for="">Arquivos</label>
                                <input type="file" class="dropify input-toBlock" ref="destinacaoDocs" multiple>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-success btn-toBlock">Salvar</button>
                        <button type="button" class="btn btn-sm btn-danger" @click="cancelarDestinacao()">Cancelar</button>
                    </form>
                    <template v-if="novaDesitnacaoPilha">
                        <table class="table table-sm table-hoover" id="tblAnexos">
                            <thead>
                                <tr>
                                    <th>Arquivo</th>
                                    <th>Data</th>
                                    <th>-</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="docs in destinacaoDocs">
                                    <td><a :href="docs.Arquivo" target="_blank">{{docs.Nome}}</a></td>
                                    <td>{{formatarData(docs.DataCriacao)}}</td>
                                    <td><button type="button" class="btn btn-sm btn-danger btn-toBlock" @click="excluirArquivo(docs)"><i class="fa fa-close"></i></button> </td>
                                </tr>
                            </tbody>
                        </table>
                    </template>
                    <div class="container-fluid mt-2">
                        <div class="row file_manager">
                            <template>
                                <card-fotos v-for="(foto,i) in destinacaoFotos" key="'destinacaoFoto'+i" :disabled="isDestinacaoMesAnterior" :foto="foto" @excluir-foto="getFotos('destinacao')"></card-fotos>
                            </template>
                        </div>
                    </div>
                </div>
            </template>
        </div>
    </div>
</div>