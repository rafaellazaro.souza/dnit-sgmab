<?php $this->load->view('cgmab/Relatorios/css/cssRelatoriosFauna.php') ?>
<style>
	.table-responsive {
		height: 500px;
		overflow: auto;
		width: 100%;
	}
</style>

<div class="body_scroll">
	<div class="block-header">
		<div class="row">
			<div class="col-lg-7 col-md-6 col-sm-12">
				<h2>Relatório</h2>
				<ul class="breadcrumb mt-2">
					<li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a>
					</li>
					<li class="breadcrumb-item">Relatórios</li>
					<li class="breadcrumb-item active">Supressão de Vegetação</li>
				</ul>
				<button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div id="supressaoVegetacaoControleSemanal">
					<div class="header">
						<div class="row mb-2">
							<div class="col-12 col-sm-3">
								<div class="input-group mb-3 mt-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">Ano</span>
									</div>
									<div class="form-control form-header lh-40">{{anoAtividade}}</div>
								</div>
							</div>
							<div class="col-12 col-sm-3">
								<div class="input-group mb-3 mt-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">Mês</span>
									</div>
									<div class="form-control form-header lh-40">{{mesAtividade}}</div>
								</div>
							</div>
							<div class="col-sm-6 text-right">
								<div class="row mt-3">
									<div class="input-group mb-3 col-12 col-sm-6">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">Contrato</span>
										</div>
										<div class="form-control form-header lh-40">{{numeroContrato}}</div>
									</div>
									<div class="input-group mb-3 col-12 col-sm-6 ">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">Empresa</span>
										</div>
										<div class="form-control form-header lh-40">{{session.Logado.NomeContratada}}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="body">
							<div class="row mb-4">
								<div class="col-12">
									<h4 class="w-100 mb-4 border p-3">Controle Semanal de Supressão</h4>
									<!-- <div class="text-right ">
										<button class="btn btn-success mt-2">Enviar para Validação</button>
									</div> -->
									<!-- <ul class="list-group list-group-flush" style="display: none;">
										<li class="list-group-item">Prazo para envio:</li>
										<li class="list-group-item">Data de Envio:</li>
										<li class="list-group-item">Hora de Envio:</li>
										<li class="list-group-item">Colaborador:</li>
									</ul> -->
								</div>
							</div>
							<div class="body_scroll">
								<div id="monitoramentoInsercaoDados" class="container-fluid">
									<div class="row clearfix">
										<div class="col-lg-12">
											<div>
												<div style="min-height: 100px;" class="no-shadow body">
													<div class="btn-group" role="group" aria-label="Grupo de botões com dropdown aninhado">
														<div class="btn-group" role="group">
															<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="fa fa-filter"></i> Nome de Grupo
															</button>
															<div class="dropdown-menu" style="left: -112px;" aria-labelledby="btnGroupDrop1">
																<a class="dropdown-item" href="javascript:;" @click="filtroSelecionado = i" v-for="i in listaControleSemanalFiltro" v-text="i"></a>
																<!-- <a class="dropdown-item" href="javascript:;" @click="">Com Fotos</a> -->
															</div>
														</div>
													</div>
													<a download="Relatorio_Controle_Semanal.xlsx" href="http://localhost:8080/sgmab/webroot/uploads/modelos-excel/RelatorioControleSemanalSupressao.xlsx" class="float-right btn btn-info"><i class="fas fa-download"></i>
														Baixar modelo .xlsx</a>
													<button data-toggle="modal" data-target="#defaultModal" class="float-right btn btn-success mb-3 btn-toBlock"><i class="fas fa-upload"></i> Importar dados .xlsx
													</button>
													<div class="table-responsive">
														<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
															<thead class="thead-dark">
																<tr>
																	<th>Foto</th>
																	<th>ID Codigo Foto</th>
																	<th>Semana</th>
																	<th>Data Inicial</th>
																	<th>Data Final</th>
																	<th>Cobertura Vegetal</th>
																	<th>Tipologia</th>
																	<th>Fitofisionomia</th>
																	<th>Estágio Sucessional</th>
																	<th>Área em APP (ha)</th>
																	<th>Área fora APP (ha)</th>
																	<th>Área total (ha)</th>
																	<th>Observações</th>
																	<th>Ações</th>
																</tr>
															</thead>
															<tbody>
																<tr v-for="i in listaControleSemanal">
																	<td>
																		<i v-if="i.Caminho != null" class="fa fa-check-circle" aria-hidden="true"></i>
																		<i v-if="i.Caminho == null" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
																	</td>
																	<td>{{i.CodigoControleSemanal}}</td>
																	<td v-text="i.Semana"></td>
																	<td v-text="i.DataInicial"></td>
																	<td v-text="i.DataFinal"></td>
																	<td v-text="i.CoberturaVegetal"></td>
																	<td v-text="i.Tipologia"></td>
																	<td v-text="i.Fitofisionomia"></td>
																	<td v-text="i.EstagioSucessional"></td>
																	<td v-text="i.AreaEmApp"></td>
																	<td v-text="i.AreaForaApp"></td>
																	<td v-text="i.AreaTotal"></td>
																	<td v-text="i.Observacoes"></td>
																	<td v-show="i.RegistroFoto == 0">
																		<button @click="deletarRegistros(i.CodigoControleSemanal, i.RegistroFoto)" class="btn btn-outline-danger btn-sm btn-toBlock"><i class="fa fa-times"></i></button>
																	</td>
																	<td v-show="i.RegistroFoto != 0">
																		<button disabled @click="deletarRegistros(i.CodigoControleSemanal)" class="btn btn-outline-danger btn-sm btn-toBlock"><i class="fa fa-times"></i></button>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="mt-2 mb-5">
												<div class="header">
													<h2><strong>{{totalFotos}}</strong> Pontos de Supressão <i class="fas fa-images"></i></h2>
												</div>
												<div class="body no-shadow border">
													<form enctype="multipart/form-data" method="POST">
														<input type="file" id="imagemMaterial" @change="qtdFotos" name="fotos[]" class="dropify  input-toBlock" multiple>
													</form>
												</div>
												<button class="btn btn-outline-secondary mt-2 float-right btn-toBlock" @click="verifcaFotos">IMPORTAR
												</button>
											</div>
											<div class="row file_manager mt-2">
												<div v-for="(i, index) in listaFotos" v-if="i.CaminhoFoto != null" :key="index" class="mt-4 col-lg-4 col-md-4 col-sm-12">
													<div :class="[i.Lat == '' || i.Lat == null ? 'borderRed' : 'borderGreen']">
														<div class="file">
															<div class="hover">
																<button @click="deletarFotos(i.CodigoControleSemanalFotos, i.CaminhoFoto)" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger btn-toBlock">
																	<i class="zmdi zmdi-delete"></i>
																</button>
															</div>
															<div class="image">
																<img :src="i.CaminhoFoto" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
															</div>
															<div class="file-name d-flex justify-content-between">
																<label for="email_address">ID</label>
																<span class="date text-info"><b>{{i.CodigoControleSemanal}}</b></span>
															</div>
															<div class="file-name">
																<small>Nome<span class="date text-info"><b>{{i.CodigoControleSemanal}}_{{i.NomeFoto | singleName}}</b></span></small>
																<small>Nome da Foto<span class="date text-info"><b>{{i.NomeFoto | singleName}}</b></span></small>
																<small v-if="i.Lat != ''">Latitude<span class="date text-info">{{i.Lat}}</span></small>
																<small v-else>Latitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
																<small v-if="i.Long != ''">Longitude<span class="date text-info">{{i.Long}}</span></small>
																<small v-else>Longitude<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
																<small v-if="i.DataFoto != '1900-01-01 00:00:00.000'">Data
																	da fotografia <span class="date text-info">{{i.DataFoto}}</span></small>
																<small v-else>Data da fotografia<span class="date text-info"><i class="text-danger fas fa-exclamation-triangle"></i></span></small>
															</div>
															<div class="file-name">
																<label cla>UTM (Data Sirgas 2000)</label><br>
																<div class="row">
																	<div class=" col-md-3">
																		<label for="email_address">Zona</label>
																		<div class="form-group">
																			<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
																			<input v-else-if="i.Zona != null" disabled :placeholder="i.Zona" class="form-control">
																			<input v-else v-model="i.modelDadosFotosInformacao.Zona" class="form-control">
																		</div>
																	</div>
																	<div class=" col-md-3">
																		<label for="email_address">CN</label>
																		<div class="form-group">
																			<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
																			<input v-else-if="i.CN != null" disabled :placeholder="i.CN" class="form-control">
																			<input v-else v-model="i.modelDadosFotosInformacao.CN" class="form-control">
																		</div>
																	</div>
																	<div class=" col-md-3">
																		<label for="email_address">CE</label>
																		<div class="form-group">
																			<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
																			<input v-else-if="i.CE != null" disabled :placeholder="i.CE" class="form-control">
																			<input v-else v-model="i.modelDadosFotosInformacao.CE" class="form-control">
																		</div>
																	</div>
																	<div class=" col-md-3">
																		<label for="email_address">Elevação(m)</label>
																		<div class="form-group">
																			<input v-if="i.Long == '' && i.Lat == ''" disabled class="form-control">
																			<input v-else-if="i.Elevacao != null" disabled :placeholder="i.Elevacao" class="form-control">
																			<input v-else v-model="i.modelDadosFotosInformacao.Elevacao" class="form-control">
																		</div>
																	</div>
																</div>
															</div>
															<div class="file-name">
																<label for="email_address">Fotografia
																	Observações</label>
																<div class="form-group">
																	<textarea v-if="i.Observacoes != null" rows="4" disabled class="form-control" :placeholder="i.Observacoes"></textarea>
																	<textarea v-else-if="i.Observacoes == null && i.Elevacao != null" disabled rows="4" class="form-control" placeholder="Foto salvo sem observações"></textarea>
																	<textarea v-else-if="i.Lat != ''" rows="4" v-model="i.modelDadosFotosInformacao.Observacoes" class="form-control"></textarea>
																	<textarea v-else rows="4" disabled class="form-control" placeholder="A foto não está de acordo com o manual"></textarea>
																</div>
															</div>
															<div v-bind:id="'error-'+index">

															</div>
															<div class="file-name">
																<button v-if="i.Lat != '' && i.CodigoControleSemanal != null" @click="verificaInformacoes(index, i.CodigoControleSemanalFotos)" class="btn btn-success btn-toBlock">Salvar
																</button>
																<button @click="deletarFotos(i.CodigoControleSemanalFotos, i.CaminhoFoto)" class="btn btn-danger btn-toBlock">Excluir
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<!-- modal importar pontos de coleta  -->

									<div class="modal fade bd-example-modal-xl" id="defaultModal" tabindex="-1" role="dialog">
										<div class="modal-dialog modal-xl" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="title" id="defaultModalLabel">Importa dados dos Pontos de
														Coleta
														<br /><small>Utilizar o modelo excel disponibilizado</small>
													</h4>
												</div>
												<div class="modal-body">
													<template v-show="mostrarListaExcel === false">
														<input type="file" id="file" ref="file" name="" class="dropify" multiple>
													</template>
													<div class="w-100 h450-r">
														<table class="table table-striped" v-show="mostrarListaExcel" id="listaPontosExcel">
															<thead>
																<tr>
																	<th>Semana</th>
																	<th>Data Inicial</th>
																	<th>Data Final</th>
																	<th>Cobertura Vegetal</th>
																	<th>Tipologia</th>
																	<th>Fitofisionomia</th>
																	<th>Estágio Sucessional</th>
																	<th>Área em APP (ha)</th>
																	<th>Área fora APP (ha)</th>
																	<th>Área total (ha)</th>
																	<th>Observações</th>
																</tr>
															</thead>
															<tbody>
																<tr v-for="i in listaExcel">
																	<td v-text="i.Semana"></td>
																	<td v-text="i.DataInicial"></td>
																	<td v-text="i.DataFinal"></td>
																	<td v-text="i.CoberturaVegetal"></td>
																	<td v-text="i.Tipologia"></td>
																	<td v-text="i.Fitofisionomia"></td>
																	<td v-text="i.EstagioSucessional"></td>
																	<td v-text="i.AreaEmApp"></td>
																	<td v-text="i.AreaForaApp"></td>
																	<td v-text="i.AreaTotal"></td>
																	<td v-text="i.Observacoes"></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
												<div class="modal-footer">
													<button @click="importarDadosControleSemanal()" type="button" class="btn btn-default btn-round waves-effect" v-show="mostrarListaExcel === false">Importar
													</button>
													<button @click="salvarDadosControleSemanal()" type="button" class="btn btn-default btn-round waves-effect" v-show="mostrarListaExcel">Confirmar e Salvar
													</button>
												</div>
											</div>
										</div>
									</div> <!-- modal importar pontos de coleta -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="relatorioMensal"></div>
		</div>
	</div>
</div>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioSupressaoVegetacao/appControleSemanal'); ?>