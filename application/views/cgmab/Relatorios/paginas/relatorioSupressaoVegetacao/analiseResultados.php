<div class="row" id="app-analiseResultados">
    <div class="accordion w-100 mt-3" id="accordion">
        <div class="w-100 border" style="min-height: auto;" v-for="(i,key) in resultados" @onload>
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" aria-controls="collapseOne" :data-target="'#collapse'+key" v-text="i.titulo">
                    </button>
                </h5>
            </div>

            <div :id="'collapse'+key" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <h5>{{i.titulo}}</h5>
                    <div class="row file_manager">
                        <div class="col-6 col-md-5" style="min-height: auto;">
                            <div class="file" v-if="i.Arquivo">
                                <div class="hover">
                                    <button @click="deletarFoto(i, key)" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger btn-toBlock">
                                        <i class="zmdi zmdi-delete"></i>
                                    </button>
                                </div>
                                <div>
                                    <div>
                                        <img :src="i.Arquivo" alt="img" class="img-fluid" style="height: 300px!important; width: 100%;">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="button-grafico text-center" v-if="i.Arquivo">
                                <a :href="i.Arquivo" target="_blank" class="btn btn-outline-secondary mt-0 float-center" role="button" aria-pressed="true">Visualizar Gráfico</a>
                            </div>
                            <div class="input-group mt-4" v-if="!(i.Arquivo)">
                                <div class="custom-file">
                                    <input type="file" :ref="'Foto'+key" class="custom-file-input input-toBlock" @change="getFileName($event, key)">
                                    <label class="custom-file-label">Selecione</label>
                                </div>
                                <div class="input-group-append">
                                    <button @click="salvarArquivo(i,key)" class="btn btn-outline-secondary btn-toBlock" type="button">Salvar</button>
                                </div>
                            </div>
                        </div>
                        <div class="image col-12 col-md-7">
                            <textarea :id="'editorResultados'+key" class="form-control"></textarea>
                            <br>
                            <button type="button" @click="salvarTexto(i, key)" class="btn-toBlock btn btn-outline-secondary mt-2 float-right">Salvar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
