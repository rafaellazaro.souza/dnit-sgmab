<template id="template-card-fotos">
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card" style="min-height: auto;">
            <div class="file">
                <div class="hover">
                    <button v-if="!disabled" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger btn-toBlock" @click="excluirFoto()">
                        <i class="zmdi zmdi-delete"></i>
                    </button>
                </div>
                <div class="image">
                    <img :src="foto.Arquivo" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
                </div>
                <div class="file-name">
                    <small>Latitude<span class="date text-info">{{foto.Lat}}</span></small>
                    <small>Longitude<span class="date text-info">{{foto.Long}}</span></small>
                    <small>Data da fotografia <span class="date text-info">{{formatDataFoto(foto.DataFoto)}}</span></small>
                </div>
                <form action="" @submit.prevent="salvarFoto()">
                    <div class="file-name">
                        <div class="row">
                            <div class=" col-md-12 pr-0 pl-0">
                                <label for="email_address">Observação</label>
                                <div class="form-group ">
                                    <textarea :disabled="disabled" class="form-control input-toBlock" v-model="foto.Observacao"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="file-name">
                        <button v-if="!disabled" class="btn btn-success btn-toBlock" type="submit">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</template>