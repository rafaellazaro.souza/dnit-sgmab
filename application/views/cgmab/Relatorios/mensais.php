<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }
</style>
<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Relatórios de Atividades Mensais</h2>
                <ul class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item">Contrato</li>
                    <li class="breadcrumb-item active">Relatórios</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid" id="relatorio-mensal">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <div class="col-12 text-right">
                            <button @click="setMes()" class="btn btn-success btn-sm">Novo Relatório</button>
                        </div>
                    </div>
                    <div class="body">
                        <div class="row mb-3 pb-3 mt-3 ml-1">
                            <h5 class="w-100 ml-3">Filtrar Relatórios</h5>
                            <div class="form-group col-12 col-md-4">
                                <label>Mês</label>
                                <select v-model="filtro.MesReferencia" @change="getMeses()" class="form-control show-tick ms select2">
                                    <option :value="01">01 - Janeiro</option>
                                    <option :value="02">02 - Fevereiro</option>
                                    <option :value="03">03 - Março</option>
                                    <option :value="04">04 - Abril</option>
                                    <option :value="05">05 - Maio</option>
                                    <option :value="06">06 - Junho</option>
                                    <option :value="07">07 - Julho</option>
                                    <option :value="08">08 - Agosto</option>
                                    <option :value="09">09 - Setembro</option>
                                    <option :value="10">10 - Outubro</option>
                                    <option :value="11">11 - Novembro</option>
                                    <option :value="12">12 - Dezembro</option>
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-4">
                                <label>Ano</label>
                                <select v-model="filtro.AnoReferencia" @change="getMeses()" class="form-control show-tick ms select2">
                                    <option v-for="i in listaAnos" :value="i.ano" v-text="i.ano"></option>
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-4">
                                <label>Situação</label>
                                <select v-model="filtro.Situacao" @change="getMeses()" class="form-control show-tick ms select2">
                                    <option value="todos">Todas</option>
                                    <option value="0">Pendente Validacao</option>
                                    <option value="1">Pendente Correção</option>
                                    <option value="3">Validado DNIT</option>
                                </select>
                            </div>
                        </div>
                        <h5 class="w-100 ml-3 mb-4">Relatórios de Atividades Cadastrados</h5>
                        <div class="col-12 text-right mb-4">
                            <button @click="mostrarCronogramaAtividades = true" class="btn btn-info btn-sm">Ver cronograma do programa</button>
                            <div class="modal-w98-h50 sombra-2 p-4 text-left" v-show="mostrarCronogramaAtividades">
                                <button @click="mostrarCronogramaAtividades = false" class="btn btn-sm btn-r"><i class="fa fa-times"></i></button>
                                <div class="row mt-5">
                                    <div class="col-12 col-sm-4">
                                        <h4 class="  w-100">Cronogramas Físicos dos Programas</h4>
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Cod.</th>
                                                    <th>Previsão Início</th>
                                                    <th>Duração (Meses)</th>
                                                    <th>Periodiciade dos Relatórios técnico</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td v-text="listaAtividadesCronograma[0].CodigoCronogramaFisicoProgramas"></td>
                                                    <td v-text="vmGlobal.frontEndDateFormat(listaAtividadesCronograma[0].PrevisaoInicio)"></td>
                                                    <td v-text="listaAtividadesCronograma[0].DuracaoPrograma"></td>
                                                    <td v-text="listaAtividadesCronograma[0].Periodicidade"></td>

                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-12 col-sm-8 border-left">
                                        <h4 class="  w-100">Atividades Cadastradas</h4>
                                        <table class="table table-striped mt-4">
                                            <thead>
                                                <tr>
                                                    <th>Mês da Atividade</th>
                                                    <th>Atividade</th>
                                                    <th>Descrição</th>
                                                    <th>Atividade Tipo Relatório</th>
                                                    <th>Status</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="i in listaAtividadesCronograma">
                                                    <td v-text="i.MesAtividade+'/'+ i.AnoAtividade"></td>
                                                    <td v-text="i.Atividade"></td>
                                                    <td v-text="i.Descricao"></td>
                                                    <td v-if="i.PossuiRelatorio === '2' || i.PossuiRelatorio === '2'" v-text="'Não'"></td>
                                                    <td v-else v-text="'Sim'"></td>
                                                    <td v-text="i.Status"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>



                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Contrato</th>
                                    <th>Mês/Ano</th>
                                    <th>Situação</th>
                                    <th>Atividades</th>
                                    <th>Prazo para Envio</th>
                                    <th>Data de Envio</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="i in listaMeses">
                                    <td>

                                        {{codigoContrato}}</td>
                                    <td v-text="i.MesReferencia+'/'+i.AnoReferencia">05/2020</td>
                                    <td>
                                        <template v-if="i.Situacao == 0">
                                            <button class="btn btn-sm btn-danger p-2"><i class="fa fa-calendar-times"></i> Não Validado</button>
                                        </template>
                                        <template v-if="i.Situacao == 1">
                                            <button class="btn btn-sm btn-primary p-2"><i class="fa fa-calendar-times"></i> Em Validação</button>
                                        </template>
                                        <template v-if="i.Situacao == 3">
                                            <button class="btn btn-sm btn-success p-2"><i class="fa fa-calendar-check"></i> Aprovado</button>
                                        </template>
                                        <template v-if="i.Situacao == 2">
                                            <button class="btn btn-sm btn-rejeitado p-2"><i class="fa fa-calendar-minus"></i> Parcialmente Aprovado</button>
                                        </template>
                                        <template v-if="i.Situacao == 4">
                                            <button class="btn btn-sm btn-danger p-2"><i class="fa fa-calendar-minus"></i> Rejeitado</button>
                                        </template>
                                    </td>
                                    <td>
                                        {{i.AtividadesRelatorio}}
                                        <button @click="abrirModalAtividades(i.CodigoRelatorioMensalPrograma)" v-if="!i.AtividadesRelatorio" class="btn btn-sm"><i class="fa fa-plus"></i> Adicionar Atividades</button>
                                    </td>
                                    <td>
                                        {{i.PrazoEnvio}}
                                    </td>
                                    <td>
                                        {{i.DataEnvioFiscal}}
                                    </td>
                                    <td>
                                        <a v-if="habilitarEdicao(i)" :href="base_url+'Atividades/listaAtividadesMes?kls65fd89e56ajk4saw8ioo77erw1as6q='+vmGlobal.codificarDecodificarParametroUrl(escopoPrograma, 'encode')+'&kls65fd89e56ajk4saw8ioo77erw1as6q='+vmGlobal.codificarDecodificarParametroUrl(codigoContrato, 'encode')+'&kls65fd89e56ajk4saw8ioo77erw1as6q='+vmGlobal.codificarDecodificarParametroUrl(i.CodigoRelatorioMensalPrograma,'encode')" class="btn btn-sm btn-success" title="Executar Atividades"><i class="fa fa-check"></i></a>

                                        <button v-if="habilitarEnvioFiscal(i) && i.Situacao != 3" class="btn btn-sm btn-info" @click="enviarParaValidacao(i)" title="Enviar para aprovação do fiscal"><i class="fas fa-share"></i></button>

                                        <button v-if="i.Situacao == 0" @click="deletarRelatório(i.CodigoRelatorioMensalPrograma)" class="btn btn-sm btn-danger" title="Deletar"><i class="fa fa-times"></i></button>
                                    </td>
                                </tr>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <div>



        </div>

        <!-- Modal -->
        <div class="modal  fade" id="setAtividades" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Selecionar Atividades para relatório do mês {{MesSelecionado+'/'+ new Date().getFullYear()}}</h5>
                        <button @click="listaAtividadesSelecionadas = []" type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="row mt-3">
                            <div class="col-6 border-right h450-r">
                                <p>Selecione as atividades que deseja atribuir ao relatório:</p>
                                <table class="table table-striped">
                                    <tbody>
                                        <tr v-for="(i, index) in listaAtividades">
                                            <td><a href="javascript:;" :title="i.Descricao">{{i.MesAtividade+'/'+i.AnoAtividade +' - ' +i.Atividade}}</a></td>
                                            <td v-text="i.PossuiRelatorio == 2 ? 'TipoAção' : 'Tipo Relatório' "></td>
                                            <td class="text-right">
                                                <button @click="selecionarAtividades(i.CodigoAtividadeCronogramaFisico, i.MesAtividade, i.AnoAtividade, i.Atividade, i.PossuiRelatorio, index, 'inserir')" class="btn btn-sm"><i class="fa fa-check"></i></button>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="col-6 h450-r">
                                <p>Atividades Selecionadas:</p>
                                <table class="table table-striped">
                                    <tbody>
                                        <tr v-for="(i, index) in listaAtividadesSelecionadas">
                                            <td><a href="javascript:;" :title="i.Descricao">{{i.MesAtividade+'/'+i.AnoAtividade +' - ' +i.Atividade}}</a></td>
                                            <td v-text="i.PossuiRelatorio == 2 ? 'TipoAção' : 'Tipo Relatório' "></td>
                                            <td class="text-right"><button @click="selecionarAtividades(i.CodigoAtividadeCronogramaFisico, i.MesAtividade, i.AnoAtividade, i.Atividade, i.PossuiRelatorio, index, 'remover')" class="btn btn-sm"><i class="fa fa-times"></i></button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button @click="listaAtividadesSelecionadas = []; getMeses()" type="button" id="fecharModal" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button @click="salvarAtividades()" type="button" class="btn btn-primary">Salvar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('webroot/arquivos/plugins/moment-business/moment-business-days.min.js') ?>"></script>
<?php $this->load->view('cgmab/Relatorios/scripts/appRelatorioMensal'); ?>