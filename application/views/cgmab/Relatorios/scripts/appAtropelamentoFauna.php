<script>
	const codigoEscopo = location.search.slice(1).split('&')[0].split('=')[1];
	const atividade = location.search.slice(1).split('&')[2].split('=')[1];

	var session = eval('(<?php echo json_encode($_SESSION) ?>)');
	var vmAppAtropelamentoFauna = new Vue({
		el: "#informacoesAtropelamentoFauna",
		data() {
			return {
				codigoEscopo: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[0].split('=')[1], 'decode'),
				numeroContrato: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[1].split('=')[1], 'decode'),
				anoAtividade: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[3].split('=')[1], 'decode'),
				mesAtividade: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[4].split('=')[1], 'decode'),
			}
		},
	})

	//await vmRelatorioAtividade.closeButtons();
	const vmRelatorioAtividade = new Vue({
		el: '#relatorioMensal',
		data() {
			return {
				PerfilUsuario: '',
				Situacao: '',
			}
		},
		methods: {
			async getUsuario() {
				await vmGlobal.dadosUsuario();
				this.PerfilUsuario = vmGlobal.dadosDoUsuario.NomePerfil;
			},
			async getRelatorio() {
				var status = await vmGlobal.getFromAPI({
					table: 'tblRelatorioMensalProgramas',
					column: 'Situacao',
					where: {
						MesReferencia: vmAppAtropelamentoFauna.mesAtividade,
						AnoReferencia: vmAppAtropelamentoFauna.anoAtividade,
						CodigoEscopoPrograma: vmAppAtropelamentoFauna.codigoEscopo,
					}
				});
				if (status.length != 0) {
					this.Situacao = status[0].Situacao;
				}
			},
			async closeButtons() {
				if (this.Situacao == 3 && this.PerfilUsuario) {
					var botoes = document.querySelectorAll('.btn-toBlock');
					var inputs = document.querySelectorAll('.input-toBlock');

					botoes.forEach((el) => {
						$(el).attr('disabled', true).css({
							display: 'none'
						});
					});

					inputs.forEach((el) => {
						$(el).attr('disabled', true);
					});

					$.each(CKEDITOR.instances, (el) => {
						this.$nextTick(() => {
							var text = CKEDITOR.instances[el].getData();
							CKEDITOR.instances[el].destroy();
							CKEDITOR.replace(el, {
								height: '400px',
								readOnly: true
							});
							CKEDITOR.instances[el].setData(text);
						});
					})
				}
			}
		},
		async mounted() {
			await this.getUsuario();
			await this.getRelatorio();
			await this.closeButtons();
		},
	})


	var vmAbasAtropelamento = new Vue({
		el: "#tabsAtropelamentoFauna",
		data: {

		},
		methods: {
			iniciarInsercaoDados() {
				vmAtropelamentoInsercaoDados.getListaAtropelamentoFauna();
			},
			iniciarHotSpots() {
				vmHotSpotsAtropelamento.getDadosAropelamentoObservacoes();
				vmHotSpotsAtropelamento.gerarCkeditor();
				vmHotSpotsAtropelamento.gerarMapa();
			},
			iniciarDiscussoesConclusoes() {
				// vmDiscussoesConclusoesAtropelamento.iniciarTextArea();
			},
		}

	});
</script>