<script>
    var parametrosEscopo = location.search.slice(1);
    var codigoEscopoPrograma = vmGlobal.codificarDecodificarParametroUrl(parametrosEscopo.split('&')[0].split('=')[1], 'decode');
    var numeroContrato = vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[1].split('=')[1], 'decode');

    var appRelatorioMensal = new Vue({
        el: '#relatorio-mensal',
        data() {
            return {
                mostrarModalAddRelatorio: true,
                mostrarCronogramaAtividades: false,
                listaMeses: [],
                listaAtividadesCronograma: [{
                    CodigoCronogramaFisicoProgramas: "",
                    PrevisaoInicio: '',
                    DuracaoPrograma: '',
                    Periodicidade: ''
                }],
                listaAtividades: [],
                listaAtividadesSelecionadas: [],
                listaAtividadesInsert: [],
                escopoPrograma: codigoEscopoPrograma,
                codigoContrato: numeroContrato,
                codigoRelatorioMensal: 0,
                MesSelecionado: 0,
                filtro: {
                    CodigoEscopoPrograma: codigoEscopoPrograma,
                    AnoReferencia: new Date().getFullYear(),
                },
                listaAnos: [],
                usuario: {
                    NomeUsuario: '',
                    NomePerfil: '',
                    CodigoUsuario: ''
                }
            }
        },
        async mounted() {
            await this.getMeses();
            this.gerarAnos();
            await this.getAtividadesCronograma();
            await this.getUsuario();
        },
        computed: {
            isFiscal() {
                return this.usuario.NomePerfil.includes('Fiscal');
            },
        },
        methods: {
            habilitarEdicao(i) {
                if (this.isFiscal) {
                    return true;
                } else if (!(i.Situacao == 1)) {
                    return true;
                }

                return false;
            },
            habilitarEnvioFiscal(i) {
                return ![4, 1].includes(parseInt(i.Situacao)) && !this.isFiscal;
            },
            async getMeses() {
                this.listaMeses = []
                let params = {
                    table: 'tblRelatorioMensalProgramas',
                    where: this.filtro
                }
                var meses = await vmGlobal.getFromAPI(params);

                meses.forEach(async function(item) {
                    await appRelatorioMensal.listaMeses.push({
                        CodigoRelatorioMensalPrograma: item.CodigoRelatorioMensalPrograma,
                        CodigoEscopoPrograma: item.CodigoEscopoPrograma,
                        MesReferencia: item.MesReferencia,
                        AnoReferencia: item.AnoReferencia,
                        Situacao: item.Situacao,
                        DataInicio: item.DataInicio,
                        DataEnvioFiscal: (item.DataEnvioFiscal) ? moment(item.DataEnvioFiscal).format('DD/MM/YYYY') : '',
                        DataAprovado: item.DataAprovado,
                        DataReprovado: item.DataReprovado,
                        PrazoEnvio: (item.PrazoEnvio) ? moment(item.PrazoEnvio).format('DD/MM/YYYY') : '',
                        AtividadesRelatorio: await appRelatorioMensal.getAtividadesRelatorio(item.CodigoRelatorioMensalPrograma)
                    });
                });

            },
            async getAtividadesCronograma() {
                let params = {
                    table: 'conogramasDosProgramas',
                    join: {
                        table: 'atividadesDoCronograma',
                        on: 'CodigoCronogramaFisicoProgramas'
                    },
                    where: {
                        CodigoEscopoPrograma: this.escopoPrograma
                    },
                    orderBy: ['DataMesAtividade']
                }
                this.listaAtividadesCronograma = await vmGlobal.getFromAPI(params);
            },
            async getAtividades() {
                this.listaAtividadesSelecionadas = [];
                await axios.post(base_url + 'Atividades/getAtividadesEscopoMes', $.param({
                        codigoEscopoPrograma: this.escopoPrograma
                    }))
                    .then((resp) => {
                        this.listaAtividades = resp.data;
                    });
            },
            async getAtividadesRelatorio(codigoRelatorio) {
                var atividades = await vmGlobal.getFromAPI({
                    table: 'tblRelatorioMensalAtividades',
                    join: {
                        table: 'atividadesDoCronograma',
                        on: 'CodigoAtividadeCronogramaFisico'
                    },
                    where: {
                        CodigoRelatorioMensalPrograma: codigoRelatorio
                    }

                })
                var resultado = '';
                atividades.forEach(function(item) {
                    resultado += item.Atividade + ' | '
                });

                return resultado
            },
            async setMes() {
                var {
                    value: formValues
                } = await Swal.fire({
                    title: 'Qual Mês de Referência do relatório?',
                    html: '<p>selecione o mês para continuar!<p>',
                    icon: 'question',
                    input: 'range',
                    inputAttributes: {
                        min: 1,
                        max: 12,
                        step: 1
                    },
                    inputValue: new Date().getMonth()
                });

                if (formValues) {
                    this.MesSelecionado = formValues;
                    await this.salvarRelatoioMes();
                }
            },
            async selecionarAtividades(CodigoAtividade, mes, ano, atividade, tipo, remover, acao) {
                var verifica = await this.verificarAtividadeNoRelatorio(CodigoAtividade);
                if (verifica == false) {

                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Erro!',
                        html: "Não é possível salvar atividades duplicadas no relatório",
                        showConfirmButton: true,

                    });
                } else {
                    if (acao == 'inserir') {
                        this.listaAtividadesSelecionadas.push({
                            MesAtividade: mes,
                            AnoAtividade: ano,
                            Atividade: atividade,
                            PossuiRelatorio: tipo,
                            CodigoRelatorioMensalPrograma: this.codigoRelatorioMensal,
                            CodigoAtividadeCronogramaFisico: CodigoAtividade,
                            Situacao: 'Em andamento',
                            DataCadastro: new Date()
                        });
                        this.listaAtividadesInsert.push({
                            CodigoRelatorioMensalPrograma: this.codigoRelatorioMensal,
                            CodigoAtividadeCronogramaFisico: CodigoAtividade,
                            Situacao: 'Não Validado',
                            DataCadastro: new Date()
                        });
                        this.listaAtividades.splice(remover, 1);
                    } else {
                        this.listaAtividades.push({
                            MesAtividade: mes,
                            AnoAtividade: ano,
                            Atividade: atividade,
                            PossuiRelatorio: tipo,
                            CodigoRelatorioMensalPrograma: this.codigoRelatorioMensal,
                            CodigoAtividadeCronogramaFisico: CodigoAtividade,
                            Situacao: 'Em andamento',
                            DataCadastro: new Date()
                        });
                        this.listaAtividadesSelecionadas.splice(remover, 1);
                        this.listaAtividadesInsert.splice(remover, 1);
                    }
                }

            },
            async salvarRelatoioMes() {
                var ndata = new Date();
                let data = ndata.getFullYear() + '-' + (ndata.getMonth() + 1) + '-' + ndata.getDate();
                var prazoEnvio = ndata.getFullYear() + '-' + this.MesSelecionado;
                prazoEnvio = moment(prazoEnvio, 'YYYY-MM').endOf('month').businessAdd(7).format('YYYY-MM-DD');

                await axios.post(base_url + 'Relatorios/salvarRelatorioMensal', $.param({
                    CodigoEscopoPrograma: this.escopoPrograma,
                    MesReferencia: this.MesSelecionado,
                    AnoReferencia: new Date().getFullYear(),
                    Situacao: 0,
                    DataInicio: data,
                    PrazoEnvio: prazoEnvio
                })).then((resp) => {
                    this.codigoRelatorioMensal = resp.data
                });

                this.abrirModalAtividades();
            },
            async abrirModalAtividades(CodigoRelatorio = null) {
                if (CodigoRelatorio) {
                    this.codigoRelatorioMensal = CodigoRelatorio;
                }

                this.listaAtividadesSelecionadas = [];
                $('#setAtividades').modal();
                if (this.listaAtividades.length == 0) {
                    await this.getAtividades();
                }
            },
            async salvarAtividades() {
                var formData = new FormData();
                formData.append('Dados', JSON.stringify(this.listaAtividadesInsert));
                await axios.post(base_url + 'Atividades/salvarAtividadesRelatorioMes', formData)
                    .then((resp) => {
                        if (resp.data.status) {
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Salvo!',
                                text: 'Dados inseridos com sucesso.',
                                showConfirmButton: true,
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: 'Erro!',
                                html: "Erro ao tentar salvar arquivo. Causas: <br/> Dados Duplicados<br/> Bloqueio Sistema",
                                showConfirmButton: true,

                            });
                        }
                    });
                window.location.reload();
                // $('#fecharModal').click();
                // this.listaMeses = [];
                // await this.getMeses();
            },
            async verificarAtividadeNoRelatorio(codigoAtividade) {
                const params = {
                    table: 'tblRelatorioMensalAtividades',
                    where: {
                        CodigoAtividadeCronogramaFisico: codigoAtividade
                    }
                }
                var dados = await vmGlobal.getFromAPI(params);
                if (dados.length > 0) {
                    return false
                } else {
                    return true
                }
            },
            async deletarRelatório(codigoRelatorio) {
                await vmGlobal.deleteFromAPI({
                    table: 'tblRelatorioMensalProgramas',
                    where: {
                        CodigoRelatorioMensalPrograma: codigoRelatorio
                    }
                });
                this.listaMeses = [];
                this.getMeses();
            },
            gerarAnos() {
                for (var i = (this.filtro.AnoReferencia + 30); i >= 1980; i--) {
                    this.listaAnos.push({
                        ano: i
                    });
                }
            },
            async enviarParaValidacao(cronograma) {
                if (cronograma.AtividadesRelatorio.length == 0) return;
                await vmGlobal.updateFromAPI({
                    table: 'tblRelatorioMensalProgramas',
                    data: {
                        DataEnvioFiscal: moment().format('YYYY-MM-DD HH:mm:ss'),
                        Situacao: 1,
                        CodigoUsuarioEnvio: vmGlobal.dadosDoUsuario.CodigoUsuario
                    },
                    where: {
                        CodigoRelatorioMensalPrograma: cronograma.CodigoRelatorioMensalPrograma
                    }
                });

                await this.getMeses();
            },
            async getUsuario() {
                await vmGlobal.dadosUsuario();
                this.usuario.NomeUsuario = vmGlobal.dadosDoUsuario.NomeUsuario;
                this.usuario.NomePerfil = vmGlobal.dadosDoUsuario.NomePerfil;
                this.usuario.CodigoUsuario = vmGlobal.dadosDoUsuario.CodigoUsuario;
            }
        }
    });
</script>