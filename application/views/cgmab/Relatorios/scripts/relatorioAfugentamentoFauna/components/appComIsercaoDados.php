<script>
Vue.component("insercao-dados-afugentamento-comp", {
	template: "#insercaoDadosAfugentamentoComp",
	props: {
		i: Object,
		index: Number,
	},
	data() {
		return {
			ObservacaoFoto: ''
		}
	},
	async mounted() {
		
	},
	filters: {
		singleName(valor) {
			return valor.replace(/.\w+$/, "");
		}
	},

	methods: {
		deletarFotos(codigo, caminho) {
			let params = {
				table: 'afugentamentoPontos',
				data: {
					NomeFoto: null,
					CaminhoFoto: null,
					Lat: null,
					Long: null,
					ObservacaoFoto: null,
					DataFoto: null,
				},
				where: {
					CodigoPontoAfugentamentoPontos: codigo
				}
			}

			//Exclui as fotografias
			Swal.fire({
				title: 'Você tem certeza?',
				text: "Você não poderá reverter isso!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim, excluir!'
			}).then(async (result) => {
				if (result.value) {
					await vmGlobal.updateFromAPI(params, null, 'cgmab');
					vmGlobal.deleteFile(caminho);
					vmAfugentamentoInsercaoDados.getListaAfugentamentoFauna();
				}
			})
		},

		async salvarObservacoes(codigo) {
			if(!this.ObservacaoFoto){
				this.ObservacaoFoto = "Foto salvo sem observações"
			} 
			
			//Salva observacoes dos cards
			let params = {
                    table: 'afugentamentoPontos',
                    data: {
						ObservacaoFoto: this.ObservacaoFoto
					},
                    where: {
                        CodigoPontoAfugentamentoPontos: codigo
                    }
                }
                await vmGlobal.updateFromAPI(params, null, 'cgmab');
				vmAfugentamentoInsercaoDados.getListaAfugentamentoFauna();
		},
	},
})
</script>
