<script>
Vue.component("conclusoes-afugentamento-comp", {
	template: "#conclusoesAfugentamentoComp",
	props: {
		names: String,
	},
	data() {
		return {
			codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),
			update: false,
			dadosDiscussoesConclusoes: {},
		}
	},
	async mounted() {
		await this.getDadosDiscussoesConclusoes();
		this.gerarCkeditor();
		
	},
	watch: {

	},
	methods: {
		gerarCkeditor() {
			CKEDITOR.replace(this.names, {
				height: '400px'
			});
		},

		async getDadosDiscussoesConclusoes() {
			//Traz os registros armazenados no banco de dados
			var params = {
				table: 'afugentamentoInformacoes',
				where: {
					CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
				}
			};
			var dadosBusca = await vmGlobal.getFromAPI(params, 'cgmab');
			this.dadosDiscussoesConclusoes = dadosBusca[0];
			if (dadosBusca.length > 0) {
				this.update = true
			} else {
				this.update = false
			}
		},

		async salvaDiscussoesConclusoes(tipoObservacao) {
			//Popular a informacao dos quadros de texto
			await this.getDadosDiscussoesConclusoes();
			let descricaoTexto = btoa(CKEDITOR.instances[tipoObservacao].getData());
			if (this.update === false) {
				var params = {
					table: 'afugentamentoInformacoes',
					data: {
						[tipoObservacao]: descricaoTexto,
						CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
					},
				}
				await vmGlobal.insertFromAPI(params, null, 'cgmab');
				await this.getDadosDiscussoesConclusoes();
			} else {
				let CodigoObservacoes = parseInt(this.dadosDiscussoesConclusoes.CodigoDiscucaoIntroConclusoes);
				var params = {
					table: 'afugentamentoInformacoes',
					data: {
						[tipoObservacao]: descricaoTexto
					},
					where: {
						CodigoDiscucaoIntroConclusoes: CodigoObservacoes
					}
				}
				await vmGlobal.updateFromAPI(params, null, 'cgmab');
				await this.getDadosDiscussoesConclusoes();
			}
		},
	},
})
</script>
