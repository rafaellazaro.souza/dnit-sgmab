<?php $this->load->view('cgmab/Relatorios/paginas/relatorioAfugentamentoFauna/components/comDiscussoesConclusoes') ?>

<script>
	var vmDiscussoesConclusoesAfugentamento = new Vue({
		el: '#discussoesConclusoesAfugentamento',
		data() {
			return {
				codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1)
					.split('&')[2].split('=')[1], 'decode'),
				fotoGrafico: {},
				informacaoRelatorioAnalises: {}
			}
		},
		async mounted() {
			// this.getInformacaoRelatorioAnalises();
			await vmRelatorioAtividade.closeButtons();
		},
		methods: {
			async getInformacaoRelatorioAnalises() {
				//Traz os registros armazenados no banco de dados		
				let params = {
					table: 'afugentamentoInformacoes',
					where: {
						CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico,
						Ativo: 1
					}
				}
				let informacoesAfugentamentoAnalise = await vmGlobal.getFromAPI(params, null);
				this.informacaoRelatorioAnalises = informacoesAfugentamentoAnalise[0]
			},

			async verificaFotoRelatorio() {

				//Verifica o formato da fotografia e prepara o arquivo 'file' para popular no banco de dados
				let file = null;
				if (this.$refs.Foto != null && this.$refs.Foto != undefined) {
					this.fotoGrafico.CodigoAtividadeCronogramaFisico = this.codigoAtividadeCronogramaFisico;
					this.fotoGrafico.DataCadastro = new Date();
					this.fotoGrafico.FilesRules = {
						upload_path: 'webroot/uploads/relatorio-afugentamento-fauna/fotos-analises',
						allowed_types: 'jpg|jpeg|png|gif',
					};
					file = {
						name: "UploadGrafico",
						value: this.$refs.Foto.files[0]
					}
				}
				await this.salvarFotoRelatorioGF(file);
			},

			async salvarFotoRelatorioGF(file) {
				//Salva a fotografia do registro no banco de dados
				if (this.informacaoRelatorioAnalises) {
					let params = {
						table: 'afugentamentoInformacoes',
						data: this.fotoGrafico,
						where: {
							CodigoDiscucaoIntroConclusoes: this.informacaoRelatorioAnalises.CodigoDiscucaoIntroConclusoes
						}
					}
					await vmGlobal.updateFromAPI(params, file, 'cgmab');
					await this.getInformacaoRelatorioAnalises();
				} else {
					let params = {
						table: 'afugentamentoInformacoes',
						data: this.fotoGrafico,
					}
					await vmGlobal.insertFromAPI(params, file, 'cgmab');
					await this.getInformacaoRelatorioAnalises();
				}
			},

			async deletarFoto(caminhoFoto) {
				//Apaga o registro da fotografia do banco de dados e o arquivo no projeto       
				let params = {
					table: 'afugentamentoInformacoes',
					data: {
						UploadGrafico: null,
					},
					where: {
						CodigoRelatorioAnalises: this.informacaoRelatorioAnalises.CodigoRelatorioAnalises,
					}
				}
				await vmGlobal.updateFromAPI(params, null);
				await vmGlobal.deleteFile(caminhoFoto);
				await vmDiscussoesConclusoesAfugentamento.getInformacaoRelatorioAnalises();
			},
		}
	});

	$(document).on('change', '.custom-file-input', function(event) {
		$(this).next('.custom-file-label').html(event.target.files[0].name);
	});
</script>
