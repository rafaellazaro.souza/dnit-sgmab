<?php $this->load->view('cgmab/Relatorios/paginas/relatorioAfugentamentoFauna/components/comInsercaoDados') ?>

<script>
	var vmAfugentamentoInsercaoDados = new Vue({
		el: "#afugentamentoInsercaoDados",
		data() {
			return {
				// codigoEscopoPrograma: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[0].split('=')[1], 'decode'),
				codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),

				filtroSelecionado: "",
				listaAfugentamentoFauna: {},
				listaAfugentamentoFaunaFiltro: 0,
				listaExcel: {},
				totalFotos: 0,
				mostrarListaExcel: false,

			}
		},
		watch: {
			filtroSelecionado: function(val) {
				this.getListaAfugentamentoFauna();
			}
		},

		async mounted() {
			await this.getListaAfugentamentoFauna();
			await vmRelatorioAtividade.closeButtons();
		},

		filters: {

		},

		methods: {
			async getListaAfugentamentoFauna() {
				let params, where;
				if (this.filtroSelecionado === 'Sem-foto') {
					this.listaAfugentamentoFauna = await vmGlobal.getFromController('Relatorios/getListaAfugentamentoFaunaSemFoto?codigo=' + this.codigoAtividadeCronogramaFisico)
				} else {
					if (this.filtroSelecionado != "") {
						where = {
							GrupoFaunistico: this.filtroSelecionado
						}
					} else {
						where = {
							CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
						}
					}

					params = {
						table: 'afugentamentoPontos',
						where: where
					}

					//lista pontos
					this.listaAfugentamentoFauna = await vmGlobal.getFromAPI(params, 'cgmab');
				}

				// lista para filtro dos pontos
				if (this.listaAfugentamentoFaunaFiltro === 0) {
					this.listaAfugentamentoFauna.push({
						GrupoFaunistico: 'Sem-foto'
					});
					this.listaAfugentamentoFaunaFiltro = this.listaAfugentamentoFauna.map(x => x
						.GrupoFaunistico).filter(function(elem, index, self) {
						return index === self.indexOf(elem);
					});
				}
			},

			async importarDadosInsercao() {
				//upload do arquivo excel
				this.file = this.$refs.file.files[0];

				let formData = new FormData();
				formData.append('file', this.file);

				await axios.post('<?= base_url('API/uploadXLS') ?>',
						formData, {
							headers: {
								'Content-Type': 'multipart/form-data'
							}
						})
					.then((response) => {
						this.listaExcel = response.data
					})
					.catch((error) => {
						console.log(error);
					})
					.finally(() => {
						this.mostrarListaExcel = true;
						$(".dropify-wrapper").addClass('invisivel');
					});
			},

			async salvarDadosInsercaoFauna() {
				this.listaExcel.CodigoAtividadeCronogramaFisico = this.codigoAtividadeCronogramaFisico;
				var formData = new FormData();
				formData.append('Dados', JSON.stringify(this.listaExcel));
				formData.append('CodigoAtividadeCronogramaFisico', this.codigoAtividadeCronogramaFisico);
				await axios.post(base_url + 'Relatorios/salvarAfugentamentoFaunaDadosInsercao', formData)
					.then((resp) => {
						console.log(resp.data);
						if (resp.data.status === true) {
							Swal.fire({
								position: 'center',
								type: 'success',
								title: 'Salvo!',
								text: 'Dados inseridos com sucesso.',
								showConfirmButton: true,
							});
						} else {
							Swal.fire({
								position: 'center',
								type: 'error',
								title: 'Erro!',
								text: "Erro ao tentar salvar arquivo. " + e,
								showConfirmButton: true,
							});
						}
					});
				this.mostrarListaExcel = false;
				this.getListaAfugentamentoFauna();
				$('.dropify-clear').click();
				$('.dropify-wrapper').removeClass('invisivel');
				$('.ls-toggle-menu').removeClass('modal-open');
				$('.theme-blue').removeClass('modal-open');
				$('.modal').hide();
				$('.modal-backdrop').remove();
			},

			deletarRegistros(codigo) {
				//Deletar Registros
				Swal.fire({
					title: 'Você tem certeza?',
					text: "Você não poderá reverter isso!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim, excluir!'
				}).then((result) => {
					if (result.value) {
						let fd = new FormData();
						fd.append('Codigo', codigo)
						var txt;

						axios.post('<?= base_url('Relatorios/deletarRegistroAfugentamentoFauna') ?>', fd)
							.then(response => {
								vmAfugentamentoInsercaoDados.getListaAfugentamentoFauna();
								Swal.fire(
									'Excluído!',
									'Sua fotografia foi excluída com sucesso.',
									'success'
								)
							}).catch(function(error) {
								console.log(error)
							}).finally(() => {})
					}
				})
			},

			qtdFotos() {
				//veridica a quantidade das fotografias para ser importado
				var myfiles = document.getElementById("imagemMaterial");
				this.totalFotos = myfiles.files.length;
			},

			async verifcaFotos() {
				//Adiciona as fotografias
				var myfiles = document.getElementById("imagemMaterial");
				var filesFotos = myfiles.files;
				var dataFoto = new FormData();

				//Listo os IDs do Registros do banco de dados
				let arrayIDs = [];
				this.listaAfugentamentoFauna.forEach((valor, index) => arrayIDs.push(valor.CodigoPontoAfugentamentoPontos));

				let strError = "";
				let numArray = 0;

				for (i = 0; i < filesFotos.length; i++) {
					//Separa o nome da foto 
					let nameFile = filesFotos[i].name.split("_");

					//Verifica se o ID existe no banco de dados
					let fotoID = filesFotos[i].name.split("_")[0];
					const findID = arrayIDs.includes(fotoID);

					//Verifica se a foto existe no banco de dados
					let valorFotografia = this.listaAfugentamentoFauna.filter(valor => valor.CodigoPontoAfugentamentoPontos == fotoID);
					let caminhoFotografia = '';

					if (valorFotografia.length > 0) {
						caminhoFotografia = valorFotografia[0].CaminhoFoto
					} else {
						caminhoFotografia = null
					}

					//Seleciona as fotos com erros
					if (nameFile.length != 2 || !findID || caminhoFotografia != null) {
						strError += filesFotos[i].name + "<br>";
					} else {
						dataFoto.append(numArray, filesFotos[i]);
						numArray += 1;
					}
				}
				if (strError != "") {
					await Swal.fire({
						type: 'error',
						title: 'Nome da Imagem está fora dos padrões',
						text: 'Por favor Corrigir!',
						html: 'Por favor Corrigir! </br>' + '</br>' +
							strError,
					})
				}
				await vmAfugentamentoInsercaoDados.addFotos(dataFoto);
			},

			addFotos(dataFoto) {
				$.ajax({
					url: '<?= base_url('Relatorios/getGpsFotosAfugentamentoFauna') ?>',
					method: 'POST',
					data: dataFoto,
					contentType: false,
					processData: false,
					success: function(data) {
						Swal.fire({
							position: 'center',
							type: 'success',
							title: 'Salvo!',
							text: 'Fotos salvas com sucesso.',
							timer: 1500,
							showConfirmButton: true,
						});
						document.getElementById('imagemMaterial').value = null;
					},
					error: function(error) {
						console.log(error);
						Swal.fire({
							position: 'center',
							type: 'error',
							title: 'Erro!',
							text: "Erro ao tentar salvar arquivo. " + e,
							showConfirmButton: true,
						});
					},
					complete: function() {
						vmAfugentamentoInsercaoDados.getListaAfugentamentoFauna();
						vmAfugentamentoInsercaoDados.filtroSelecionado = '';
						vmAfugentamentoInsercaoDados.totalFotos = 0
						$('.dropify-clear').click();
					}
				});
			},
		},
	});

	$(function() {
		"use strict";
		$('.dropify').dropify({
			messages: {
				default: "Solte ou clique para anexar arquivo."
			}
		});
	});
</script>