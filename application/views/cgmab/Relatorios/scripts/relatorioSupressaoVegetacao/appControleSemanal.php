<script>
	var session = eval('(<?php echo json_encode($_SESSION) ?>)');

	var vmSupressaoVegetacaoControleSemanal = new Vue({
		el: "#supressaoVegetacaoControleSemanal",
		data() {
			return {
				codigoEscopo: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[0].split('=')[1], 'decode'),
				numeroContrato: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[1].split('=')[1], 'decode'),
				mesAtividade: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[3].split('=')[1], 'decode'),
				anoAtividade: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[4].split('=')[1], 'decode'),
				codigoSupressaoArea: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[5].split('=')[1], 'decode'),
				filtroSelecionado: "",
				listaControleSemanal: {},
				listaControleSemanalFiltro: 0,
				listaExcel: {},
				file: [],
				totalFotos: 0,
				mostrarListaExcel: false,
				errorsAddCondicionante: [],
				listaFotos: {},
			}
		},

		watch: {
			filtroSelecionado: function(val) {
				this.getListaControleSemanalSupressao();
			}
		},

		async mounted() {
			await this.getListaControleSemanalSupressao();
			await this.getListaFotos();
			await vmRelatorioAtividade.closeButtons();
		},

		filters: {
			singleName(valor) {
				// return valor;
				//Para funcionar o Filtro precisa excluir as fotos do banco de dados e cadastrar as fotografias de forma correta.
				//As fotos já salvas no banco estão com erros e por isso tem conflito
				return valor.replace(/.\w+$/, "");
			}
		},

		methods: {
			async getListaControleSemanalSupressao() {
				let params, where;
				if (this.filtroSelecionado === 'Sem-foto') {
					params = {
						table: 'floraControleSemanalSupressaoArea',
						where: {
							CodigoSupressaoArea: this.codigoSupressaoArea,
							RegistroFoto: 0
						}
					}
					this.listaControleSemanal = await vmGlobal.getFromAPI(params, 'cgmab');
				} else {
					if (this.filtroSelecionado != "") {
						where = {
							Tipologia: this.filtroSelecionado
						}
					} else {
						where = {
							CodigoSupressaoArea: this.codigoSupressaoArea
						}
					}

					params = {
						table: 'floraControleSemanalSupressaoArea',
						where: where
					}

					//lista pontos
					this.listaControleSemanal = await vmGlobal.getFromAPI(params, 'cgmab');

				}

				// lista para filtro dos pontos
				if (this.listaControleSemanalFiltro === 0) {
					this.listaControleSemanal.push({
						Tipologia: 'Sem-foto'
					});

					this.listaControleSemanalFiltro = this.listaControleSemanal.map(x => x
						.Tipologia).filter(function(elem, index, self) {
						return index === self.indexOf(elem);
					});
				}
			},

			async getListaFotos() {
				let params = {
					table: 'floraControleSemanalSupressaoArea',
					joinLeft: [{
						table1: 'floraControleSemanalSupressaoArea',
						table2: 'floraControleSemanalSupressaoAreaFotos',
						on: 'CodigoControleSemanal'
					}],
					where: {
						CodigoSupressaoArea: this.codigoSupressaoArea
					}
				}
				this.listaFotos = await vmGlobal.getFromAPI(params, 'cgmab');

				for (let i = 0; i < this.listaFotos.length; i++) {
					this.listaFotos[i].modelDadosFotosInformacao = []
					this.errorsAddCondicionante[i] = []
				}
				await this.getListaControleSemanalSupressao();
			},

			async salvarDadosControleSemanal() {
				this.listaExcel.CodigoSupressaoArea = this.codigoSupressaoArea;
				var formData = new FormData();
				formData.append('Dados', JSON.stringify(this.listaExcel));
				formData.append('CodigoSupressaoArea', this.codigoSupressaoArea);
				await axios.post(base_url + 'RelatorioSupressaoVegetacao/salvarDadosControleSemanal', formData)
					.then((resp) => {
						console.log(resp.data);
						if (resp.data.status === true) {
							Swal.fire({
								position: 'center',
								type: 'success',
								title: 'Salvo!',
								text: 'Dados inseridos com sucesso.',
								showConfirmButton: true,
							});
						} else {
							Swal.fire({
								position: 'center',
								type: 'error',
								title: 'Erro!',
								text: "Erro ao tentar salvar arquivo. " + e,
								showConfirmButton: true,
							});
						}
					});
				this.mostrarListaExcel = false;
				this.getListaControleSemanalSupressao();
				this.getListaFotos();
				$('.dropify-clear').click();
				$('.dropify-wrapper').removeClass('invisivel');
				$('.ls-toggle-menu').removeClass('modal-open');
				$('.theme-blue').removeClass('modal-open');
				$('.modal').hide();
				$('.modal-backdrop').remove();
			},

			deletarRegistros(codigo, RegistroFoto) {
				//Deletar Registros
				Swal.fire({
					title: 'Você tem certeza?',
					text: "Você não poderá reverter isso!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim, excluir!'
				}).then((result) => {
					if (RegistroFoto == 0) {
						let params = {
							table: 'floraControleSemanalSupressaoArea',
							where: {
								CodigoControleSemanal: codigo
							}
						}
						vmGlobal.deleteFromAPI(params, null);
						Swal.fire(
							'Excluído!',
							'Seu registro foi excluída com sucesso.',
							'success'
						);
						vmSupressaoVegetacaoControleSemanal.getListaControleSemanalSupressao();
						vmSupressaoVegetacaoControleSemanal.getListaFotos();
					} else {
						Swal.fire({
							position: 'center',
							type: 'error',
							title: 'Erro!',
							text: "Excluir a fotografia",
							showConfirmButton: true,
						})
					}
				})
			},

			deletarFotos(codigo, caminho) {

				let fd = new FormData();
				fd.append('Codigo', codigo)

				//Exclui as fotografias
				Swal.fire({
					title: 'Você tem certeza?',
					text: "Você não poderá reverter isso!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim, excluir!'
				}).then(async (result) => {
					if (result.value) {
						axios.post('<?= base_url('RelatorioSupressaoVegetacao/deletarFotos') ?>', fd)
							.then(response => {
								this.getFotos()
							}).catch(function(error) {
								console.log(error)
							}).finally(() => {
								vmGlobal.deleteFile(caminho);
								vmGlobal.deleteFile(caminho);
								vmSupressaoVegetacaoControleSemanal.getListaControleSemanalSupressao();
								vmSupressaoVegetacaoControleSemanal.getListaFotos();
							})
					}
				})
			},

			qtdFotos() {
				var myfiles = document.getElementById("imagemMaterial");
				this.totalFotos = myfiles.files.length;
			},

			verificaInformacoes(i, codigo) {
				$("#error-" + i).html('').stop()
				var salvarInformacoes = true;
				var html = ''
				if (!this.listaFotos[i].modelDadosFotosInformacao['Zona'] ||
					!this.listaFotos[i].modelDadosFotosInformacao['CN'] ||
					!this.listaFotos[i].modelDadosFotosInformacao['CE'] ||
					!this.listaFotos[i].modelDadosFotosInformacao['Elevacao']
				) {
					html += '<div class="alert alert-warning p-3 w-100 " >' +
						'<h5>Erro ao salvar. Por favor Preencha os campos abaixo:</h5>'
					'<ul>'

					if (this.listaFotos[i].modelDadosFotosInformacao['Zona'] == undefined) {
						salvarInformacoes = false;
						html += '<li>A Zona é obrigatório.</li>';
					}
					if (this.listaFotos[i].modelDadosFotosInformacao['CN'] ==
						undefined) {
						salvarInformacoes = false;
						html += '<li>O CN é obrigatório.</li>';
					}
					if (this.listaFotos[i].modelDadosFotosInformacao['CE'] ==
						undefined) {
						salvarInformacoes = false;
						html += '<li>O CE é obrigatório.</li>';
					}
					if (this.listaFotos[i].modelDadosFotosInformacao['Elevacao'] ==
						undefined) {
						salvarInformacoes = false;
						html += '<li>A Elevação é obrigatório.</li>';
					}

					if (html != '') {
						html += '</ul>' +
							'</div>'
					}
				}
				$("#error-" + i).html(html)

				this.salvarInformacoesFotos(i, codigo, salvarInformacoes)
			},

			async salvarInformacoesFotos(i, codigo, salvarInformacoes) {
				var params = {
					table: 'floraControleSemanalSupressaoAreaFotos',
					data: {
						Zona: this.listaFotos[i].modelDadosFotosInformacao['Zona'],
						CN: this.listaFotos[i].modelDadosFotosInformacao['CN'],
						CE: this.listaFotos[i].modelDadosFotosInformacao['CE'],
						Elevacao: this.listaFotos[i].modelDadosFotosInformacao['Elevacao'],
						Observacoes: this.listaFotos[i].modelDadosFotosInformacao['Observacoes'],
					},
					where: {
						CodigoControleSemanalFotos: codigo
					}
				}

				if (this.listaFotos[i].modelDadosFotosInformacao['Observacoes'] == null) {
					if (salvarInformacoes == true)
						Swal.fire({
							title: 'Deseja salvar sem observações?',
							text: "Você não poderá reverter isso!",
							icon: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#1e7e34',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Sim, salvar!'
						}).then(async (result) => {
							if (result.value) {
								if (salvarInformacoes == true) {
									await vmGlobal.updateFromAPI(params, null, 'cgmab')
									vmSupressaoVegetacaoControleSemanal.getListaControleSemanalSupressao();
									vmSupressaoVegetacaoControleSemanal.getListaFotos();
								}
							}
						});
				} else {
					await vmGlobal.updateFromAPI(params, true, 'cgmab')
					vmSupressaoVegetacaoControleSemanal.getListaControleSemanalSupressao();
				}
			},

			async verifcaFotos() {
				//Adiciona as fotografias
				var myfiles = document.getElementById("imagemMaterial");
				var filesFotos = myfiles.files;
				var dataFoto = new FormData();

				//Listo os IDs do Registros do banco de dados
				let arrayIDs = [];
				this.listaControleSemanal.forEach((valor, index) => arrayIDs.push(valor.CodigoControleSemanal));

				let strError = "";
				let numArray = 0;

				for (i = 0; i < filesFotos.length; i++) {
					//Separa o nome da foto
					let nameFile = filesFotos[i].name.split("_");

					//Verifica se o ID existe no banco de dados
					let fotoID = filesFotos[i].name.split("_")[0];
					const findID = arrayIDs.includes(fotoID);

					//Seleciona as fotos com erros
					if (nameFile.length != 2 || !findID) {
						strError += filesFotos[i].name + "<br>";
					} else {
						// dataFoto.append(numArray, filesFotos[i]);
						dataFoto.append(numArray, filesFotos[i]);
						numArray += 1;
					}
				}
				if (strError != "") {
					await Swal.fire({
						type: 'error',
						title: 'Nome da Imagem está fora dos padrões',
						text: 'Por favor Corrigir!',
						html: 'Por favor Corrigir! </br>' + '</br>' +
							strError,
					})
				}
				await vmSupressaoVegetacaoControleSemanal.addFotos(dataFoto);
			},

			addFotos(dataFoto) {
				$.ajax({
					url: '<?= base_url('RelatorioSupressaoVegetacao/getGpsFotos') ?>',
					method: 'POST',
					data: dataFoto,
					contentType: false,
					processData: false,
					success: function(data) {
						Swal.fire({
							position: 'center',
							type: 'success',
							title: 'Salvo!',
							text: 'Fotos salvas com sucesso.',
							showConfirmButton: true,
						});
						document.getElementById('imagemMaterial').value = null;
					},
					error: function(error) {
						console.log(error);
						Swal.fire({
							position: 'center',
							type: 'error',
							title: 'Erro!',
							text: "Erro ao tentar salvar arquivo. " + e,
							showConfirmButton: true,

						});
					},
					complete: function() {
						vmSupressaoVegetacaoControleSemanal.getListaControleSemanalSupressao();
						vmSupressaoVegetacaoControleSemanal.getListaFotos();
						vmSupressaoVegetacaoControleSemanal.filtroSelecionado = '';
						vmSupressaoVegetacaoControleSemanal.totalFotos = 0
						$('.dropify-clear').click();
					}
				});
			},

			async importarDadosControleSemanal() {
				//upload do arquivo excel
				this.file = this.$refs.file.files[0];

				let formData = new FormData();
				formData.append('file', this.file);

				await axios.post('<?= base_url('API/uploadXLS') ?>',
						formData, {
							headers: {
								'Content-Type': 'multipart/form-data'
							}
						})
					.then((response) => {
						this.listaExcel = response.data
					})
					.catch((error) => {
						console.log(error);
					})
					.finally(() => {
						this.mostrarListaExcel = true;
						$(".dropify-wrapper").addClass('invisivel');
					});
			},

			// criarSessaoCodigoPrograma() {
			//     axios.post(base_url + 'ConfiguracaoProgramas/criarSessaoCodigoPrograma', $.param({
			//         CodigoModulo: this.CodigoModulo
			//     }));
			// }
		}
	});


	const vmRelatorioAtividade = new Vue({
		el: '#relatorioMensal',
		data() {
			return {
				PerfilUsuario: '',
				Situacao: '',
			}
		},
		computed: {
			disableAprovado() {
				if (this.Situacao == 3) {
					return 'disabled="true" style="display:none;"';
				}
				return '';
			}
		},
		methods: {
			async getUsuario() {
				await vmGlobal.dadosUsuario();
				this.PerfilUsuario = vmGlobal.dadosDoUsuario.NomePerfil;
			},
			async getRelatorio() {
				var status = await vmGlobal.getFromAPI({
					table: 'tblRelatorioMensalProgramas',
					column: 'Situacao',
					where: {
						MesReferencia: vmSupressaoVegetacaoControleSemanal.mesAtividade,
						AnoReferencia: vmSupressaoVegetacaoControleSemanal.anoAtividade,
						CodigoEscopoPrograma: vmSupressaoVegetacaoControleSemanal.codigoEscopo,
					}
				});
				if (status.length != 0) {
					this.Situacao = status[0].Situacao;
				}
			},
			async closeButtons() {
				if (this.Situacao == 3 && this.PerfilUsuario) {
					var botoes = document.querySelectorAll('.btn-toBlock');
					var inputs = document.querySelectorAll('.input-toBlock');

					botoes.forEach((el) => {
						$(el).attr('disabled', true).css({
							display: 'none'
						});
					});

					inputs.forEach((el) => {
						$(el).attr('disabled', true);
					});

					$.each(CKEDITOR.instances, (el) => {
						this.$nextTick(() => {
							var text = CKEDITOR.instances[el].getData();
							CKEDITOR.instances[el].destroy();
							CKEDITOR.replace(el, {
								height: '400px',
								readOnly: true
							});
							CKEDITOR.instances[el].setData(text);
						});
					})
				}
			}
		},
		async mounted() {
			await this.getUsuario();
			await this.getRelatorio();
			await this.closeButtons();
		},
	})

	$(function() {
		"use strict";
		$('.dropify').dropify({
			messages: {
				default: "Solte ou clique para anexar arquivo."
			}
		});
	});
</script>