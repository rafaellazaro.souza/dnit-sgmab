<script>
    const analiseGrup = new Vue({
        el: "#app-analiseResultados",
        data() {
            return {
                analise: {
                    CodigoSuprecaoVegetacaoAnaliseResultados: '',
                    CodigoAtividadeCronogramaFisico: '',
                    ArquivoAvanco: '',
                    AvancoSupressaoVegetacao: '',
                    ArquivoControlePilhasComuns: '',
                    ControlePilhasComuns: '',
                    ArquivoControlePilhasAmeacadas: '',
                    ControlePilhasAmeacadas: '',
                    ArquivoDestinacaoEspeciesAmeacadas: '',
                    DestinacaoEspeciesAmeacadas: '',
                    ArquivoDestinacaoEspeciesNaoAmeacadas: '',
                    DestinacaoEspeciesNaoAmeacadas: ''
                },
                resultados: [{
                    titulo: 'Avanço Supressão Vegetação',
                    Arquivo: '',
                    keyArquivo: 'ArquivoAvanco',
                    keyTexto: 'AvancoSupressaoVegetacao'
                }, {
                    titulo: 'Controle de Pilhas Comuns',
                    Arquivo: '',
                    keyArquivo: 'ArquivoControlePilhasComuns',
                    keyTexto: 'ControlePilhasComuns',
                }, {
                    titulo: 'Controle de Pilhas Espécies Ameaçadas',
                    Arquivo: '',
                    keyArquivo: 'ArquivoControlePilhasAmeacadas',
                    keyTexto: 'ControlePilhasAmeacadas'
                }, {
                    titulo: 'Destinação Espécies Não Ameaçadas',
                    Arquivo: '',
                    keyArquivo: 'ArquivoDestinacaoEspeciesNaoAmeacadas',
                    keyTexto: 'DestinacaoEspeciesNaoAmeacadas'
                }, {
                    titulo: 'Destinação Espécies Ameaçadas',
                    Arquivo: '',
                    keyArquivo: 'ArquivoDestinacaoEspeciesAmeacadas',
                    keyTexto: 'DestinacaoEspeciesAmeacadas'
                }]
            }
        },
        methods: {
            async iniciaEditores() {
                var contEditores = this.resultados.length;
                for (var i = 0; i < contEditores; i++) {
                    if (CKEDITOR.instances['editorResultados' + i]) CKEDITOR.instances['editorResultados' + i].destroy();
                    CKEDITOR.replace('editorResultados' + i, {
                        height: '400px'
                    });
                    // this.$nextTick(() => {})
                }
            },
            async salvarTexto(analise, key) {
                if (this.analise.CodigoSuprecaoVegetacaoAnaliseResultados.length == 0) {
                    await vmGlobal.insertFromAPI({
                        table: 'suprecaoAnliseResultados',
                        data: {
                            CodigoAtividadeCronogramaFisico: atividade,
                            [analise.keyTexto]: btoa(CKEDITOR.instances['editorResultados' + key].getData())
                        }
                    })

                    await this.getAnaliseRusultado();
                    return;
                }
                await vmGlobal.updateFromAPI({
                    table: 'suprecaoAnliseResultados',
                    data: {
                        [analise.keyTexto]: btoa(CKEDITOR.instances['editorResultados' + key].getData())
                    },
                    where: {
                        CodigoSuprecaoVegetacaoAnaliseResultados: this.analise.CodigoSuprecaoVegetacaoAnaliseResultados,
                    }
                });
                await this.getAnaliseRusultado();
                return;
            },
            async deletarFoto(analise, key) {
                await vmGlobal.updateFromAPI({
                    table: 'suprecaoAnliseResultados',
                    data: {
                        [analise.keyArquivo]: null
                    },
                    where: {
                        CodigoSuprecaoVegetacaoAnaliseResultados: this.analise.CodigoSuprecaoVegetacaoAnaliseResultados
                    }
                });

                await vmGlobal.deleteFile(analise.Arquivo);
                await this.getAnaliseRusultado();
            },
            async salvarArquivo(analise, key) {
                var file = this.$refs['Foto' + key][0].files;

                if (file.length == 0) return;

                var data = {
                    CodigoAtividadeCronogramaFisico: atividade,
                    FilesRules: {
                        upload_path: 'webroot/uploads/relatorio_supressao_vegetacao',
                        allowed_types: 'jpg|jpeg|png|gif',
                    }
                }

                file = {
                    name: analise.keyArquivo,
                    value: file[0]
                }

                if (this.analise.CodigoSuprecaoVegetacaoAnaliseResultados.length == 0) {

                    await vmGlobal.insertFromAPI({
                        table: 'suprecaoAnliseResultados',
                        data: data
                    }, file);
                    await this.getAnaliseRusultado();
                    return;
                }

                await vmGlobal.updateFromAPI({
                    table: 'suprecaoAnliseResultados',
                    data: data,
                    where: {
                        CodigoSuprecaoVegetacaoAnaliseResultados: this.analise.CodigoSuprecaoVegetacaoAnaliseResultados
                    }
                }, file);
                await this.getAnaliseRusultado();
            },
            async getAnaliseRusultado() {
                var resultados = await vmGlobal.getFromAPI({
                    table: 'suprecaoAnliseResultados',
                    where: {
                        CodigoAtividadeCronogramaFisico: atividade,
                    }
                });

                if (resultados.length != 0) {
                    this.analise = resultados[0];
                    this.carregaDados();
                }
            },
            carregaDados() {
                for (key in this.analise) {
                    this.resultados = this.resultados.map((el, index) => {
                        if (el.keyTexto == key && this.analise[key]) {
                            CKEDITOR.instances['editorResultados' + index].setData(atob(this.analise[key]));
                        }

                        if (key == el.keyArquivo) {
                            el.Arquivo = this.analise[key];
                        }

                        return el;
                    });
                }
            },
            getFileName(event) {
                var fileName = event.target.files[0].name;
                $(event.target).next().text(fileName);
            }
        },
        async mounted() {
            await this.iniciaEditores();
            await this.getAnaliseRusultado();
            await vmRelatorioAtividade.closeButtons();
        },
    })
</script>
