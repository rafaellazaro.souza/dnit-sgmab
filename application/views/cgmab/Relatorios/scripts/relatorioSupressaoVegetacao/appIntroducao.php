<script>
	var vmIntroducaoSupressaoVegetacao = new Vue({
		el: '#introducaoSupressaoVegetacao',
		data() {
			return {
				codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),
				dadosFormulario: {},
				update: false,
				totalDocumentos: 0,
				listaFloraDocumentos: 0,
			}
		},
		async mounted() {
			await this.getListaDocumentos();
			await this.getDadosDescricao();
			await vmRelatorioAtividade.closeButtons();
		},
		methods: {
			async getDadosDescricao() {
				var params = {
					table: 'floraSupressaoTextos',
					where: {
						CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
					}
				};
				var dadosBusca = await vmGlobal.getFromAPI(params, 'cgmab');
				if (dadosBusca.length >= 1) {
					this.dadosFormulario = dadosBusca[0];
					// CKEDITOR.instances['Observacoes'].setData(this.dadosFormulario.Observacoes);
					if (this.dadosFormulario.Introducao != null) {
						this.dadosFormulario.Introducao = atob(this.dadosFormulario.Introducao);
					}
					this.update = true
				} else {
					// CKEDITOR.instances['Observacoes'].setData('');
					this.update = false
				}
			},

			async salvarIntroducao() {
				var data = new FormData();
				data.append('Introducao', this.dadosFormulario.Introducao);

				if (this.update === false) {
					var params = {
						table: 'floraSupressaoTextos',
						data: {
							Introducao: btoa(CKEDITOR.instances.Introducao.getData()),
							CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
						},
					}
					await vmGlobal.insertFromAPI(params, null, 'cgmab');
					await this.getDadosDescricao();
				} else {
					var CodigoObservacoes = parseInt(this.dadosFormulario.CodigoSupressaoTexto);
					var params = {
						table: 'floraSupressaoTextos',
						data: {
							Introducao: btoa(CKEDITOR.instances.Introducao.getData())
						},
						where: {
							CodigoSupressaoTexto: CodigoObservacoes
						}
					}
					await vmGlobal.updateFromAPI(params, null, 'cgmab');
					await this.getDadosDescricao();
				}
			},

			async getListaDocumentos() {
				$("#listadeDocumentos").DataTable().destroy();
				const params = {
					table: 'floraSupressaoAnexos',
					where: {
						Ativo: 1
					}
				}
				this.listaFloraDocumentos = await vmGlobal.getFromAPI(params, 'cgmab');
				vmGlobal.montaDatatable('#listadeDocumentos', true);
			},

			qtdDocumentos() {
				var myfiles = document.getElementById("documentosMaterial");
				this.totalDocumentos = myfiles.files.length;
			},

			async addDocumentos() {
				var myfiles = document.getElementById("documentosMaterial");
				var files = myfiles.files;
				var data = new FormData();

				for (i = 0; i < files.length; i++) {
					data.append(i, files[i]);
				}

				data.append('codigoAtividade', parseInt(this.codigoAtividadeCronogramaFisico));

				$.ajax({
					url: '<?= base_url('RelatorioSupressaoVegetacao/postDocumentos') ?>',
					method: 'POST',
					data: data,
					contentType: false,
					processData: false,
					success: function(data) {
						Swal.fire({
							position: 'center',
							type: 'success',
							title: 'Salvo!',
							text: 'Fotos salvas com sucesso.',
							showConfirmButton: true,
						});
						document.getElementById('documentosMaterial').value = null;
					},
					error: function(error) {
						console.log(error);
						Swal.fire({
							position: 'center',
							type: 'error',
							title: 'Erro!',
							text: "Erro ao tentar salvar arquivo. " + e,
							showConfirmButton: true,

						});
					},
					complete: function() {
						vmIntroducaoSupressaoVegetacao.getListaDocumentos();
						vmIntroducaoSupressaoVegetacao.totalDocumentos = 0
						$('.dropify-clear').click();
					}
				});
			},

			async deletar(codigo, index, caminho) {
				params = {
					table: 'floraSupressaoAnexos',
					where: {
						CodigoSupressaoAnexos: codigo
					}
				}
				await vmGlobal.deleteFromAPI(params);
				await vmGlobal.deleteFile(caminho);
				await this.getListaDocumentos();
			},
		},
	})

	$(function() {
		"use strict";
		$('.dropify').dropify({
			messages: {
				default: "Solte ou clique para anexar arquivo."
			}
		});

		$(document).ready(function() {
			CKEDITOR.replace('editorIntroducaoSupressaoVegetacao', {
				height: '400px'
			});

			CKEDITOR.instances['Introducao'].on("blur", function() {
				vmIntroducaoSupressaoVegetacao.dadosFormulario.Introducao = CKEDITOR.instances[
					'Introducao'].getData();
			});
		});
	});
</script>