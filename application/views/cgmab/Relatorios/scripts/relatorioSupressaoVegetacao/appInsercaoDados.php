<script>
	var vmInsercaoDadosSupreVege = new Vue({
		el: '#insercaoDadosSupreVege',
		data() {
			return {
				codigoEscopoPrograma: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[0].split('=')[1], 'decode'),
				numeroContrato: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[1].split('=')[1], 'decode'),
				codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),
				mesAtividade: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[3].split('=')[1], 'decode'),
				anoAtividade: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[4].split('=')[1], 'decode'),


				// mesAtividade: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[3].split('=')[1],'decode'),
				// anoAtividade: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[4].split('=')[1],'decode'),

				update: false,
				dadosAtropelamentoObsercacoes: {},
				fotoGrafico: {},

				configuracaoASVCoordenadasShapeFile: {},
				relatorioASVCoordenadasShapeFile: {},
				codigoFloraSupressao: null,
				novasCoordenadasShapeFile: {},
				mymap: null,
				layer: null,
				novaLayer: null,
				listaConfiguracaoASV: {},
				listaRelatorioASV: {},
			}
		},
		watch: {
			codigoFloraSupressao(val) {
				this.$nextTick(async () => {
					await vmRelatorioAtividade.closeButtons();
				});
			}
		},
		async mounted() {
			console.log('CodigoEscopoPrograma', this.codigoEscopoPrograma);
			console.log('this.codigoAtividadeCronogramaFisico', this.codigoAtividadeCronogramaFisico);
			console.log('numeroContrato', this.numeroContrato);
			console.log('mesAtividade', this.mesAtividade);
			console.log('anoAtividade', this.anoAtividade);

			// await this.getDadosConfiguracaoSuprevessaoVegetacaoASV();
			// await this.setDadosMapa()
		},

		methods: {
			gerarMapa() {
				const style = 'reduced.night';
				this.mymap = L.map('mapid').setView(['-16.7646342', '-61.3179296'], 5.34);
				L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
					maxZoom: 15,
					// attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
					//     '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
					id: 'mapbox.streets',
					fadeAnimation: true,
				}).addTo(this.mymap);
				this.layer = L.geoJSON().addTo(this.mymap);
			},

			aplicarLayer(coord, objectid, color, layer) {
				var array = {
					"type": "FeatureCollection",
					"features": [{
						"type": "Feature",
						"geometry": JSON.parse(coord),
						"properties": {
							"OBJECTID": objectid,

						}
					}]
				}
				L.geoJson(array, {
					style: function(feature) {
						return {
							stroke: true,
							color: color,
							weight: 1
						};
					},
					onEachFeature: function(feature, l) {
						l.bindPopup(feature.properties.OBJECTID);

						l.on('click', function(e) {
							l.setStyle({
								weight: 5,
								outline: 'red'
							});
						});

						l.on("popupclose", function(e) {
							l.setStyle({
								weight: 1,
							});
						});
					}
				}).addTo(layer);
			},

			async getDadosConfiguracaoSuprevessaoVegetacaoASV() {
				let params = {
					table: 'floraSupressaoConfig',
					joinLeft: [{
						table1: 'floraSupressaoConfig',
						table2: 'licensasAmbientais',
						on: 'CodigoLicenca'
					}],
					where: {
						CodigoEscopoPrograma: this.codigoEscopoPrograma
					}
				}

				//lista de Configração
				this.listaConfiguracaoASV = await vmGlobal.getFromAPI(params, 'cgmab');
			},

			async getDadosRelatorioSuprevessaoVegetacaoASV() {
				let params = {
					table: 'floraSupressaoConfig ',
					joinLeft: [{
						table1: 'floraSupressaoConfig',
						table2: 'floraSupressaoAreas',
						on: 'CodigoFloraSupressao'
					}],
					where: {
						CodigoEscopoPrograma: this.codigoEscopoPrograma
					}
				}

				//lista de Relatorio
				this.listaRelatorioASV = await vmGlobal.getFromAPI(params, 'cgmab');
			},


			async setDadosMapaConfiguracaoASV() {
				// this.layer.clearLayers();
				await this.getDadosConfiguracaoSuprevessaoVegetacaoASV();
				const registrosASV = this.listaConfiguracaoASV

				if (registrosASV.length > 0) {
					for (const registro of registrosASV) {
						var data = registro.Coordenada;
						this.configuracaoASVCoordenadasShapeFile = JSON.parse(data);
						const concatenado = '<div class="text-left" style="color: #888"><h6>Área de ASV</h6> <br>' +
							'<b>ASV</b>: ' + registro.NumeroLicenca + '<br>';
						const corLayer = '#000';
						for (let index = 0; index < this.configuracaoASVCoordenadasShapeFile.length; index++) {
							var cd = this.configuracaoASVCoordenadasShapeFile[index].coordenada
							vmInsercaoDadosSupreVege.aplicarLayer(cd, concatenado, corLayer, vmInsercaoDadosSupreVege.layer);
						}
					}
				}
			},


			async setDadosMapaRelatorioASV() {
				// this.layer.clearLayers();
				await this.getDadosRelatorioSuprevessaoVegetacaoASV();
				const registrosRelatorioASV = this.listaRelatorioASV

				var btnDisabled = vmRelatorioAtividade.disableAprovado;

				if (this.listaConfiguracaoASV.length > 0) {
					for (const registro of registrosRelatorioASV) {
						var data = registro.CoordenadasArea;
						this.relatorioASVCoordenadasShapeFile = JSON.parse(data);
						const concatenado = '<div class="text-left" style="color: #888"><h6>Cadastro Mensal de Área Suprimida</h6> <br>' +
							'<b>Data de Cadastro</b>: ' + registro.DataCadastro + '<br>' +
							// '<button class="mt-4 btn btn-sm btn-outline-primary" onclick="novoRelatorioASV(' + registro.CodigoSupressaoArea + ')">Controle Semanal de Supressão</button>';

							'<button class="mt-4 btn btn-sm btn-outline-primary" onclick="abrirRelatorioASV(' + registro.CodigoSupressaoArea + ')">Inserir Dados</button>' +
							'<button class="mt-4 btn btn-sm btn-danger" ' + btnDisabled + ' onclick="deletarAreaSuprimida(' + registro.CodigoSupressaoArea + ')">Deletar</button>';


						const corLayer = '#007';
						for (let index = 0; index < this.relatorioASVCoordenadasShapeFile.length; index++) {
							var cd = this.relatorioASVCoordenadasShapeFile[index].coordenada
							vmInsercaoDadosSupreVege.aplicarLayer(cd, concatenado, corLayer, vmInsercaoDadosSupreVege.layer);
						}
					}
				}
			},

			async deletarAreaSuprimida(codigo) {
				Swal.fire({
					title: 'Opa?',
					text: "Deseja deletar este e todos os dados vinculados?",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim, agora!'
				}).then(async (result) => {
					if (result.value) {
						const params = {
							table: 'floraSupressaoAreas',
							where: {
								CodigoSupressaoArea: codigo
							}
						}
						await vmGlobal.deleteFromAPI(params);
						window.location.reload();
					}
				})


			},

			async getDadosInsercaoDados() {
				// //Traz os registros armazenados no banco de dados
				// var params = {
				// 	table: 'atropelamentoAnalises',
				// 	where: {
				// 		codigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
				// 	}
				// };
				// var dadosBusca = await vmGlobal.getFromAPI(params, 'cgmab');
				//  this.dadosAtropelamentoObsercacoes = dadosBusca[0]
				// // if (dadosBusca.length >= 1) {
				// // 	CKEDITOR.instances['txtHotSpots'].setData(this.dadosAtropelamentoObsercacoes.HotSpots);
				// // 	this.update = true
				// // } else {
				// // 	this.update = false
				// // }

				// // if (this.dadosAtropelamentoObsercacoes.GraficoHotSpots != null) {

				// // }

				// this.getDadosMapa();
			},


			async verificaShapefile() {
				//Verifica o formato da fotografia e prepara o arquivo 'file' para popular no banco de dados
				// let file = null;
				//
				// if (this.$refs.Foto != null && this.$refs.Foto != undefined) {
				// 	this.fotoGrafico.CodigoAtividadeCronogramaFisico = this.codigoAtividadeCronogramaFisico;
				// 	this.fotoGrafico.DataCadastro = new Date();
				// 	this.fotoGrafico.FilesRules = {
				// 		upload_path: 'webroot/uploads/relatorio-atropelamento-fauna/hot-spots',
				// 		allowed_types: 'zip',
				// 	};
				// 	file = {
				// 		name: 'GraficoHotSpots',
				// 		value: this.$refs.Foto.files[0]
				// 	}
				// }
				//
				// await this.addShapefile(file);
			},

			async addShapefile() {
				$('.spinnerShapefile').addClass('spinner-border spinner-border-sm');
				var myfiles = document.getElementById("ShapeFile");
				var files = myfiles.files;
				var data = new FormData();
				var file = this.$refs.ShapeFile.files[0];


				// data.append('CodigoFloraSupressao', parseInt(this.listaConfiguracaoASV.CodigoFloraSupressao));
				data.append('Shapefile', file);

				$.ajax({
					url: '<?= base_url('RelatorioSupressaoVegetacao/postShapefile') ?>',
					method: 'POST',
					data: data,
					contentType: false,
					processData: false,
					success: function(data) {
						vmInsercaoDadosSupreVege.novasCoordenadasShapeFile = JSON.parse(data);
						const numCoordenadas = vmInsercaoDadosSupreVege.novasCoordenadasShapeFile.length
						const concatenado = 0;
						const corLayer = '#007';
						vmInsercaoDadosSupreVege.novaLayer = vmInsercaoDadosSupreVege.layer;
						for (let index = 0; index < numCoordenadas; index++) {
							var cd = vmInsercaoDadosSupreVege.novasCoordenadasShapeFile[index].coordenada
							vmInsercaoDadosSupreVege.aplicarLayer(cd, concatenado, corLayer, vmInsercaoDadosSupreVege.novaLayer);
						}
					},
					error: function(error) {
						console.log(error);
						Swal.fire({
							position: 'center',
							type: 'error',
							title: 'Erro!',
							text: "Erro ao tentar salvar arquivo. " + e,
							showConfirmButton: true,

						});
					},
					complete: function() {

					}
				})
			},

			async salvarShapefile() {
				$('.spinnerShapefile').addClass('spinner-border spinner-border-sm');
				let params = {
					table: 'floraSupressaoAreas',
					data: {
						CodigoFloraSupressao: this.codigoFloraSupressao,
						CoordenadasArea: JSON.stringify(this.novasCoordenadasShapeFile),
						DataCadastro: new Date(),
					}
				}
				await vmGlobal.insertFromAPI(params, null, 'cgmab');

				this.codigoFloraSupressao = null;
				this.novasCoordenadasShapeFile = {};
				window.location.reload();
			},

			deletarCoordenadasShapeFile() {
				this.setDadosMapaConfiguracaoASV();
				this.setDadosMapaRelatorioASV();

				this.novasCoordenadasShapeFile = {};
				this.novaLayer.clearLayers();
				this.Layer.clearLayers();


				// Limpa a entrada do arquivo personalizado para o input file
				$('.spinnerShapefile').removeClass('spinner-border spinner-border-sm');
				$("#ShapeFile").val(null);
				$("#labelShapeFile").html('Selecione um arquivo ZIP');
			},

			async deletarShapefile() {

				//Apaga o registro da fotografia do banco de dados e o arquivo no projeto
				let params = {
					table: 'atropelamentoAnalises',
					data: {
						GraficoHotSpots: null,
					},
					where: {
						CodigoRelatorioAnalises: this.dadosAtropelamentoObsercacoes.CodigoRelatorioAnalises,
					}
				}
				await vmGlobal.updateFromAPI(params, null);

				// Limpa a entrada do arquivo personalizado para o input file
				$('.spinnerShapefile').removeClass('spinner-border spinner-border-sm');
				$("#ShapeFile").val(null);
				$("#labelShapeFile").html('Selecione um arquivo ZIP');

				this.novasCoordenadasShapeFile = {};
				this.layer.clearLayers();
				await this.getDadosInsercaoDados();

			},

			cadastrarRelatorioASV(codigoSupressaoArea) {
				var url = base_url + 'RelatorioSupressaoVegetacao/controleSemanal?codigoEscopoPrograma=' + vmGlobal.codificarDecodificarParametroUrl(this.codigoEscopoPrograma, 'encode') + '&numeroContrato=' + vmGlobal.codificarDecodificarParametroUrl(this.numeroContrato, 'encode') + '&codigoAtividadeCronogramaFisico=' + vmGlobal.codificarDecodificarParametroUrl(this.codigoAtividadeCronogramaFisico, 'encode') + '&ano=' + vmGlobal.codificarDecodificarParametroUrl(this.anoAtividade, 'encode') + '&mes=' + vmGlobal.codificarDecodificarParametroUrl(this.mesAtividade, 'encode') + '&codigoSupressaoArea=' + vmGlobal.codificarDecodificarParametroUrl(codigoSupressaoArea, 'encode');
				window.open(url, '_blank');
			}


		},
	});

	function abrirRelatorioASV(codigoSupressaoArea) {
		vmInsercaoDadosSupreVege.cadastrarRelatorioASV(codigoSupressaoArea)
	}

	function deletarAreaSuprimida(codigo) {
		vmInsercaoDadosSupreVege.deletarAreaSuprimida(codigo);
	}
</script>