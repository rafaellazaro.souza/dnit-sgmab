<script>
    vmAppProfissionais = new Vue({
        el: '#profissionais',
        data() {
            return {
                mostrarProfissionaisServico: false,
                listaRecursosPrograma: [],
                listaRecursosServico: [],
                contrato: '<?= $this->session->userdata('codigoContratoSelecionado'); ?>',
                opcao: ''
            }
        },
        watch: {
            opcao: function(val) {
                this.getRecursosRelatorio();
            }
        },
        methods: {
            async getRecursosServico(opcao) {
                this.opcao = opcao;
                if (opcao == 'profisionais') {
                    this.listaRecursosServico = await vmGlobal.getFromController(
                        'RecursosHumanos/getRecursosHumanosContrato', {
                            contrato: '<?= $this->session->userdata('codigoContratoSelecionado'); ?>',
                            escopo: vmGlobal.codificarDecodificarParametroUrl(codigoEscopo, 'decode')
                        }
                    );
                }

                if (opcao == 'veiculos') {
                    this.listaRecursosServico = await vmGlobal.getFromController(
                        'Veiculos/getRecursosVeiculosContrato', {
                            contrato: '<?= $this->session->userdata('codigoContratoSelecionado'); ?>',
                            escopo: vmGlobal.codificarDecodificarParametroUrl(codigoEscopo, 'decode')
                        }
                    );
                }
                if (opcao == 'equipamentos') {
                    this.listaRecursosServico = await vmGlobal.getFromController(
                        'Equipamentos/getRecursosEquipamentosContrato', {
                            contrato: '<?= $this->session->userdata('codigoContratoSelecionado'); ?>',
                            escopo: vmGlobal.codificarDecodificarParametroUrl(codigoEscopo, 'decode')
                        }
                    );
                }

                await this.$nextTick(async () => {
                    await vmRelatorioAtividade.closeButtons();
                })
                // this.listaRecursosHumanosServico = await vmGlobal.getFromAPI(params);
                // var controller = 'EscopoServicos/getRecurosHumanosServico';
                // this.listaRecursosHumanosServico = await vmGlobal.getFromController(controller, params);
            },
            async getRecursosRelatorio() {
                if (this.opcao == 'profisionais') {
                    this.listaRecursosPrograma = await vmGlobal.getFromController(
                        'Atividades/getRecursosAtividadesContrato', {
                            atividade: atividade,
                            tipo: this.opcao
                        }
                    );
                }

                if (this.opcao == 'veiculos') {
                    this.listaRecursosPrograma = await vmGlobal.getFromController(
                        'Atividades/getRecursosAtividadesContrato', {
                            atividade: atividade,
                            tipo: this.opcao
                        }
                    );
                }
                if (this.opcao == 'equipamentos') {
                    this.listaRecursosPrograma = await vmGlobal.getFromController(
                        'Atividades/getRecursosAtividadesContrato', {
                            atividade: atividade,
                            tipo: this.opcao
                        }
                    );
                }
                // let params = {
                //     table: 'recursosAtividade',
                //     where: {
                //         CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode'),
                //         TipoRecurso: this.opcao
                //     }
                // }
                // this.listaRecursosPrograma = await vmGlobal.getFromAPI(params);
                await this.$nextTick(async () => {
                    await vmRelatorioAtividade.closeButtons();
                })
            },
            async salvarProfissionalRelatorio(codigoRecurso, tipo) {
                let params = {
                    table: 'recursosAtividade',
                    data: {
                        CodigoAtividadeCronogramaFisico: atividade,
                        CodigoRecurso: codigoRecurso,
                        TipoRecurso: tipo,
                        DataCadastro: new Date()
                    }
                }
                await vmGlobal.insertFromAPI(params, null);
                this.getRecursosRelatorio();

            },
            async deletarRecurso(codigo) {
                let params = {
                    table: 'recursosAtividade',
                    where: {
                        CodigoAtividadeRecurso: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                this.getRecursosRelatorio();
            }
        }

    });
</script>