<script>
	Vue.component("conclusoes-supressao-comp", {
		template: "#conclusoesSupreVegeComp",
		props: {
			names: String,
		},
		data() {
			return {
				codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),
				update: false,
				dadosDiscussoesConclusoes: {},
			}
		},
		async mounted() {
			this.gerarCkeditor();
			await this.getDadosDiscussoesConclusoes();
			await vmRelatorioAtividade.closeButtons();
		},
		watch: {

		},
		methods: {
			gerarCkeditor() {
				CKEDITOR.replace(this.names, {
					height: '400px'
				});
			},

			async getDadosDiscussoesConclusoes() {
				//Traz os registros armazenados no banco de dados
				var params = {
					table: 'floraSupressaoTextos',
					where: {
						CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
					}
				};
				var dadosBusca = await vmGlobal.getFromAPI(params, 'cgmab');
				this.dadosDiscussoesConclusoes = dadosBusca[0]
				if (dadosBusca.length > 0) {
					if (this.dadosDiscussoesConclusoes.Conclusao) {
						this.dadosDiscussoesConclusoes.Conclusao = atob(this.dadosDiscussoesConclusoes.Conclusao);
					}
					if (this.dadosDiscussoesConclusoes.Bibliografia) {
						this.dadosDiscussoesConclusoes.Bibliografia = atob(this.dadosDiscussoesConclusoes.Bibliografia);
					}
					this.update = true
				} else {
					this.update = false
				}
			},

			async salvaDiscussoesConclusoes(tipoObservacao) {
				//Popular a informacao dos quadros de texto
				await this.getDadosDiscussoesConclusoes();
				let descricaoTexto = btoa(CKEDITOR.instances[tipoObservacao].getData());
				if (this.update === false) {
					var params = {
						table: 'floraSupressaoTextos',
						data: {
							[tipoObservacao]: descricaoTexto,
							CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
						},
					}
					await vmGlobal.insertFromAPI(params, null, 'cgmab');
					await this.getDadosDiscussoesConclusoes();
				} else {
					let CodigoObservacoes = parseInt(this.dadosDiscussoesConclusoes.CodigoSupressaoTexto);
					var params = {
						table: 'floraSupressaoTextos',
						data: {
							[tipoObservacao]: descricaoTexto
						},
						where: {
							CodigoSupressaoTexto: CodigoObservacoes
						}
					}
					await vmGlobal.updateFromAPI(params, null, 'cgmab');
					await this.getDadosDiscussoesConclusoes();
				}
			},
		},
	})
</script>