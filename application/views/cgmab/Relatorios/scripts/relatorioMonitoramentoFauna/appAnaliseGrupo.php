<?php $this->load->view('cgmab/Relatorios/paginas/relatorioMonitoramentoFauna/components/comAnaliseGrupo')?>
<script>
var vmAnaliseGrupo = new Vue({
    el: '#analiseGrupo',
    data() {
        return {
            listaGrupoFaunistico: {},
        }
    },
    async mounted() {
        // await this.getListaGrupoFaunistico();
    },
    methods: {
        async getListaGrupoFaunistico() {
            params = {
                table: 'gruposFaunisticos',
            }
            this.listaGrupoFaunistico = await vmGlobal.getFromAPI(params, 'cgmab');
        },
    },
})
</script>

