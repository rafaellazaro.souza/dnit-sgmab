<?php $this->load->view('cgmab/Relatorios/paginas/relatorioMonitoramentoFauna/components/comAnaliseModulo')?>
<script>
var vmAnaliseModulo = new Vue({
    el: '#analiseModulo',
    data() {
        return {
            codigoEscopoPrograma: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[
                0].split('=')[1], 'decode'),
            listaConfiguracaoModulo: {},
        }
    },
    async mounted() {
        // await this.getlistaConfiguracaoModulos();
    },
    methods: {
        async getlistaConfiguracaoModulos() {
            params = {
                table: 'configFaunaModulos',
                where: {
                    CodigoEscopoPrograma: this.codigoEscopoPrograma
                }
            }
            this.listaConfiguracaoModulo = await vmGlobal.getFromAPI(params, 'cgmab');
        },
    }
})
</script>

