<script>
    Vue.component("analisemodulofauna-comp", {
        template: "#analiseModuloFauna",
        props: {
            names: String,
            codigomodulo: String,
        },
        data() {
            return {
                codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1)
                    .split('&')[2].split('=')[1], 'decode'),
                informacaoRelatorioModuloGF: {
                    CodigoAnaliseGrupoModulo: null
                },
                fotoGraficoModulo: {},

                nameFileAnaliseAbundanciaModulo: 'Selecionar Gráfico',
                nameFileAnaliseIndiceModulo: 'Selecionar Gráfico',
                nameFileAnaliseDendogramaModulo: 'Selecionar Gráfico',
                defineColuna: 'CodigoModulo',
            }
        },
        async mounted() {
            await this.gerarCkeditor();
            await this.getInformacaoRelatorioModuloGF()
        },
        watch: {},
        methods: {
            async gerarCkeditor() {
                await this.$nextTick(() => {
                    //Gera o editor de texto
                    CKEDITOR.replace('AnaliseResultadoAbundanciaRiquezaModulo' + this.codigomodulo, {
                        height: '200px'
                    });
                    CKEDITOR.replace('AnaliseResultaldoIndiceDiversidadeModulo' + this.codigomodulo, {
                        height: '200px'
                    });
                    CKEDITOR.replace('AnaliseResultadoDendogramaModulo' + this.codigomodulo, {
                        height: '200px'
                    });
                });
                await vmRelatorioAtividade.closeButtons();
            },

            async getInformacaoRelatorioModuloGF() {
                //Traz os registros armazenados no banco de dados
                let informacaoRelatorioMonitoramentoModuloGF = await vmGlobal.getFromController(
                    'Relatorios/getInformacaoRelatorioMonitoramentoFauna?codigoAtividade=' + this
                    .codigoAtividadeCronogramaFisico + '&defineColuna=' + this.defineColuna +
                    '&codigoDoRegistro=' + this
                    .codigomodulo);

                if (informacaoRelatorioMonitoramentoModuloGF.length != 0) {
                    this.informacaoRelatorioModuloGF = informacaoRelatorioMonitoramentoModuloGF[0];

                    this.$nextTick(() => {
                    	if(this.informacaoRelatorioModuloGF.AnaliseResultadoAbundanciaRiqueza){
							CKEDITOR.instances['AnaliseResultadoAbundanciaRiquezaModulo' + this.codigomodulo].setData(atob(this.informacaoRelatorioModuloGF.AnaliseResultadoAbundanciaRiqueza));
						}
                    	if(this.informacaoRelatorioModuloGF.AnaliseResultaldoIndiceDiversidade){
							CKEDITOR.instances['AnaliseResultaldoIndiceDiversidadeModulo' + this.codigomodulo].setData(atob(this.informacaoRelatorioModuloGF.AnaliseResultaldoIndiceDiversidade));
						}
                    	if(this.informacaoRelatorioModuloGF.AnaliseResultadoDendograma){
							CKEDITOR.instances['AnaliseResultadoDendogramaModulo' + this.codigomodulo].setData(atob(this.informacaoRelatorioModuloGF.AnaliseResultadoDendograma));
						}
                    })
                }
            },

            async verificaFotoRelatorioModuloGF(tipografico, IdFoto) {
                //Verifica o formato da fotografia e prepara o arquivo 'file' para popular no banco de dados
                let file = null;
                if (this.$refs[IdFoto] != null && this.$refs[IdFoto] != undefined) {
                    this.fotoGraficoModulo.CodigoAtividadeCronogramaFisico = this
                        .codigoAtividadeCronogramaFisico;
                    this.fotoGraficoModulo.CodigoModulo = this.codigomodulo;
                    this.fotoGraficoModulo.DataCadastro = new Date();
                    this.fotoGraficoModulo.FilesRules = {
                        upload_path: 'webroot/uploads/relatorio-monitoramento-fauna/fotos-analise-modulo',
                        allowed_types: 'jpg|jpeg|png|gif',
                    };
                    if (tipografico === 'GraficoAbundanciaRiqueza') {
                        file = {
                            name: tipografico,
                            value: this.$refs.Foto.files[0]
                        }
                        this.nameFileAnaliseAbundanciaModulo = 'Selecionar Gráfico';

                    } else if (tipografico === 'GraficoIndiceDiversidade') {
                        file = {
                            name: tipografico,
                            value: this.$refs.Foto2.files[0]
                        }
                        this.nameFileAnaliseIndiceModulo = 'Selecionar Gráfico';
                    } else if (tipografico === 'GraficoDendograma') {
                        file = {
                            name: tipografico,
                            value: this.$refs.Foto3.files[0]
                        }
                        this.nameFileAnaliseDendogramaModulo = 'Selecionar Gráfico';
                    }
                }

                await this.salvaFotoRelatorioModuloGF(file);
            },

            async salvaFotoRelatorioModuloGF(file) {
                //Salva a fotografia do registro no banco de dados
                if (this.informacaoRelatorioModuloGF) {
                    let params = {
                        table: 'relatorioMonitoramentoFaunaAnaliseGrupoModulo',
                        data: this.fotoGraficoModulo,
                        where: {
                            CodigoAnaliseGrupoModulo: this.informacaoRelatorioModuloGF
                                .CodigoAnaliseGrupoModulo
                        }
                    }
                    await vmGlobal.updateFromAPI(params, file, 'cgmab');
                    await this.getInformacaoRelatorioModuloGF();
                } else {
                    let params = {
                        table: 'relatorioMonitoramentoFaunaAnaliseGrupoModulo',
                        data: this.fotoGraficoModulo,
                    }
                    await vmGlobal.insertFromAPI(params, file, 'cgmab');
                    await this.getInformacaoRelatorioModuloGF();
                }
            },

            fileSelected(event, nameFile) {
                //Preenche o nome da fotografia na caixa para importar
                if (nameFile == 'nameFileAnaliseAbundanciaModulo') {
                    this.nameFileAnaliseAbundanciaModulo = event.target.files[0].name
                } else if (nameFile == 'nameFileAnaliseIndiceModulo') {
                    this.nameFileAnaliseIndiceModulo = event.target.files[0].name
                } else if (nameFile == 'nameFileAnaliseDendogramaModulo') {
                    this.nameFileAnaliseDendogramaModulo = event.target.files[0].name
                }
            },

            async deletarFoto(codigoFoto, caminho) {
                //Apaga o registro da fotografia do banco de dados e o arquivo no projeto
                let params = {
                    table: 'relatorioMonitoramentoFaunaAnaliseGrupoModulo',
                    data: {
                        [caminho]: null,
                    },
                    where: {
                        CodigoAnaliseGrupoModulo: codigoFoto,
                    }
                }
                await vmGlobal.updateFromAPI(params, null);
                await vmGlobal.deleteFile(this.informacaoRelatorioModuloGF[caminho]);
                await this.getInformacaoRelatorioModuloGF();
            },

            async salvarAnaliseTextoGF(tipoanalise) {
                //Popular a informacao dos quadros de texto
                let descricaoTexto = String(btoa(CKEDITOR.instances[tipoanalise + 'Modulo' + this.codigomodulo.toString()].getData()));

                if (this.informacaoRelatorioModuloGF.CodigoAnaliseGrupoModulo && this.informacaoRelatorioModuloGF.CodigoModulo) {
                    var params = {
                        table: 'relatorioMonitoramentoFaunaAnaliseGrupoModulo',
                        data: {
                            [tipoanalise]: descricaoTexto
                        },
                        where: {
                            CodigoAnaliseGrupoModulo: this.informacaoRelatorioModuloGF.CodigoAnaliseGrupoModulo
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null, 'cgmab');
                    await this.getInformacaoRelatorioModuloGF();
                    return;
                }

                var params = {
                    table: 'relatorioMonitoramentoFaunaAnaliseGrupoModulo',
                    data: {
                        [tipoanalise]: descricaoTexto,
						CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico,
						CodigoModulo: this.codigomodulo
                    },
                }
                await vmGlobal.insertFromAPI(params, null, 'cgmab');
                await this.getInformacaoRelatorioModuloGF();
            },

        }
    });
</script>
