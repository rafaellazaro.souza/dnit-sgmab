<script>
    Vue.component("analisegrupofaunistico-comp", {
        template: "#analiseGrupoFaunistico",
        props: {
            names: String,
            codigogrupofaunistico: String,
        },
        data() {
            return {
                codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1)
                    .split('&')[2].split('=')[1], 'decode'),
                informacaoRelatorioGF: {
                    CodigoAnaliseGrupoModulo: null,
                    GraficoIndiceDiversidade: null
                },
                fotoGrafico: {},

                nameFileAnaliseAbundancia: 'Selecionar Gráfico',
                nameFileAnaliseIndice: 'Selecionar Gráfico',
                defineColuna: 'CodigoGrupoFaunistico',
            }
        },
        async mounted() {
            await this.gerar();
			setTimeout(async () => {
				await this.getInformacaoRelatorioGF();
			}, 400)
        },
        watch: {},
        methods: {
            async gerar() {
                await this.$nextTick(() => {
                    //Gera o editor de texto
                    CKEDITOR.replace('AnaliseResultadoAbundanciaRiqueza' + this.codigogrupofaunistico, {
                        height: '200px'
                    });
                    CKEDITOR.replace('AnaliseResultaldoIndiceDiversidade' + this.codigogrupofaunistico, {
                        height: '200px'
                    });
                });
                await vmRelatorioAtividade.closeButtons();
			},

            async getInformacaoRelatorioGF() {
                //Traz os registros armazenados no banco de dados
                let informacaoRelatorioMonitoramentoGF = await vmGlobal.getFromController(
                    'Relatorios/getInformacaoRelatorioMonitoramentoFauna?codigoAtividade=' + this.codigoAtividadeCronogramaFisico + '&defineColuna=' + this.defineColuna +
                    '&codigoDoRegistro=' + this.codigogrupofaunistico);
                if (informacaoRelatorioMonitoramentoGF.length != 0) {
                    this.informacaoRelatorioGF = informacaoRelatorioMonitoramentoGF[0];
                     await this.$nextTick(() => {
                     	if(this.informacaoRelatorioGF.AnaliseResultadoAbundanciaRiqueza){
							CKEDITOR.instances['AnaliseResultadoAbundanciaRiqueza' + this.codigogrupofaunistico].setData(String(atob(this.informacaoRelatorioGF.AnaliseResultadoAbundanciaRiqueza)));
						}

                     	if(this.informacaoRelatorioGF.AnaliseResultaldoIndiceDiversidade) {
							CKEDITOR.instances['AnaliseResultaldoIndiceDiversidade' + this.codigogrupofaunistico].setData(String(atob(this.informacaoRelatorioGF.AnaliseResultaldoIndiceDiversidade)));
						}
                    });
                }
            },

            async verificaFotoRelatorioGF(tipografico, IdFoto) {
                //Verifica o formato da fotografia e prepara o arquivo 'file' para popular no banco de dados
                let file = null;
                if (this.$refs[IdFoto] != null && this.$refs[IdFoto] != undefined) {
                    this.fotoGrafico.CodigoAtividadeCronogramaFisico = this.codigoAtividadeCronogramaFisico;
                    this.fotoGrafico.CodigoGrupoFaunistico = this.codigogrupofaunistico;
                    this.fotoGrafico.DataCadastro = new Date();
                    this.fotoGrafico.FilesRules = {
                        upload_path: 'webroot/uploads/relatorio-monitoramento-fauna/fotos-analise-grupo',
                        allowed_types: 'jpg|jpeg|png|gif',
                    };
                    if (tipografico === 'GraficoAbundanciaRiqueza') {
                        file = {
                            name: tipografico,
                            value: this.$refs.Foto.files[0]
                        }
                        this.nameFileAnaliseAbundancia = 'Selecionar Gráfico';

                    } else if (tipografico == 'GraficoIndiceDiversidade') {
                        file = {
                            name: tipografico,
                            value: this.$refs.Foto2.files[0]
                        }
                        this.nameFileAnaliseIndice = 'Selecionar Gráfico';
                    }
                }
                await this.salvarFotoRelatorioGF(file);
            },

            async salvarFotoRelatorioGF(file) {
                //Salva a fotografia do registro no banco de dados
                if (this.informacaoRelatorioGF.CodigoAnaliseGrupoModulo) {
                    let params = {
                        table: 'relatorioMonitoramentoFaunaAnaliseGrupoModulo',
                        data: this.fotoGrafico,
                        where: {
                            CodigoAnaliseGrupoModulo: this.informacaoRelatorioGF.CodigoAnaliseGrupoModulo
                        }
                    }
                    await vmGlobal.updateFromAPI(params, file, 'cgmab');
                    await this.getInformacaoRelatorioGF();
                } else {
                    let params = {
                        table: 'relatorioMonitoramentoFaunaAnaliseGrupoModulo',
                        data: this.fotoGrafico,
                    }
					await vmGlobal.insertFromAPI(params, file, 'cgmab');
                    await this.getInformacaoRelatorioGF();
                }
            },

            fileSelected(event, nameFile) {
                //Preenche o nome da fotografia na caixa para importar
                if (nameFile == 'nameFileAnaliseAbundancia') {
                    this.nameFileAnaliseAbundancia = event.target.files[0].name
                } else if (nameFile == 'nameFileAnaliseIndice') {
                    this.nameFileAnaliseIndice = event.target.files[0].name
                }
            },

            async deletarFoto(codigoFoto, caminho) {
                //Apaga o registro da fotografia do banco de dados e o arquivo no projeto       
                let params = {
                    table: 'relatorioMonitoramentoFaunaAnaliseGrupoModulo',
                    data: {
                        [caminho]: null,
                    },
                    where: {
                        CodigoAnaliseGrupoModulo: codigoFoto,
                    }
                }
                await vmGlobal.updateFromAPI(params, null);
                await vmGlobal.deleteFile(this.informacaoRelatorioGF[caminho]);
                await this.getInformacaoRelatorioGF();
            },

            async salvarAnaliseTextoGF(tipoanalise) {
                //Popular a informacao dos quadros de texto
                let descricaoTexto = String(btoa(CKEDITOR.instances[tipoanalise + this.codigogrupofaunistico.toString()].getData()));

                if (this.informacaoRelatorioGF.CodigoAnaliseGrupoModulo && this.informacaoRelatorioGF.CodigoGrupoFaunistico) {
                    var params = {
                        table: 'relatorioMonitoramentoFaunaAnaliseGrupoModulo',
                        data: {
                            [tipoanalise]: descricaoTexto
                        },
                        where: {
                            CodigoAnaliseGrupoModulo: this.informacaoRelatorioGF.CodigoAnaliseGrupoModulo
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null, 'cgmab');
                    await this.getInformacaoRelatorioGF();
                    return;
                }

                var params = {
                    table: 'relatorioMonitoramentoFaunaAnaliseGrupoModulo',
                    data: {
                        [tipoanalise]: descricaoTexto,
                        CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico,
                        CodigoGrupoFaunistico: this.codigogrupofaunistico
                    },
                }
                await vmGlobal.insertFromAPI(params, null, 'cgmab');
                await this.getInformacaoRelatorioGF();
            },
        }
    });
</script>
