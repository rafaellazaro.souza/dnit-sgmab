<script>
    Vue.component("discussoesconclusoes-comp", {
        template: "#discussoesConclusoesComp",
        props: {
            names: String,
        },
        data() {
            return {
                codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1)
                    .split('&')[2].split('=')[1], 'decode'),
                update: false,
                dadosDiscussoesConclusoes: {},

            }
        },
        async mounted() {
            await this.gerarCkeditor();
            await this.getDadosDiscussoesConclusoes();
        },
        watch: {

        },
        methods: {
            async gerarCkeditor() {
                this.$nextTick(() => {
                    if (CKEDITOR.instances[this.names]) CKEDITOR.instances[this.names].destroy();
                    CKEDITOR.replace(this.names, {
                        height: '400px'
                    });

                })
                await vmRelatorioAtividade.closeButtons();
            },

            async getDadosDiscussoesConclusoes() {
                //Traz os registros armazenados no banco de dados
                var params = {
                    table: 'relatorioMonitoramentoFaunaObservacao',
                    where: {
                        CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
                    }
                };
                var dadosBusca = await vmGlobal.getFromAPI(params, 'cgmab');
                if (dadosBusca.length != 0) {
                    this.dadosDiscussoesConclusoes = dadosBusca[0];

                    await this.gerarCkeditor();
                    await this.$nextTick(() => {
                    	if(this.dadosDiscussoesConclusoes[this.names].length > 0) {
							CKEDITOR.instances[this.names].setData(atob(this.dadosDiscussoesConclusoes[this.names]));
						}
                    })
                    this.update = true
                } else {
                    // CKEDITOR.instances['Observacoes'].setData('');
                    this.update = false
                }
            },

            async salvaDiscussoesConclusoes(tipoObservacao) {
                //Popular a informacao dos quadros de texto
                let descricaoTexto = String(btoa(CKEDITOR.instances[tipoObservacao].getData()));
                if (this.update === false) {
                    var params = {
                        table: 'relatorioMonitoramentoFaunaObservacao',
                        data: {
                            [tipoObservacao]: descricaoTexto,
                            CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
                        },
                    }
                    await vmGlobal.insertFromAPI(params, null, 'cgmab');
                    await this.getDadosDiscussoesConclusoes();
                } else {
                    var CodigoObservacoes = parseInt(this.dadosDiscussoesConclusoes.CodigoRelatorioObservacoes);
                    var params = {
                        table: 'relatorioMonitoramentoFaunaObservacao',
                        data: {
                            [tipoObservacao]: descricaoTexto
                        },
                        where: {
                            CodigoRelatorioObservacoes: CodigoObservacoes
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null, 'cgmab');
                    await this.getDadosDiscussoesConclusoes();
                }
            },
        },

    })
</script>
