<script>
var vmMonitoramentoInsercaoDados = new Vue({
	el: "#monitoramentoInsercaoDados",
	data() {
		return {
			CodigoEscopoPrograma: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[0].split('=')[1], 'decode'),
			CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),

			filtroSelecionado: "",
			listaMonitoramentoFauna: {},
			listaMonitoramentoFaunaFiltro: 0,
			listaExcel: {},
			file: [],
			totalFotos: 0,
			mostrarListaExcel: false,
			errorsAddCondicionante: [],
			listaModuloFauna: {},
			pontosConfiguracaoFauna: {},
			listaArmadilha: {},
		}

	},

	watch: {
		filtroSelecionado: function(val) {
			this.getListaMonitoramentoFauna();
		}
	},

	async mounted() {
		await this.getListaMonitoramentoFauna();
		// this.criarSessaoCodigoPrograma();
		await this.getModuloFauna();
		await this.getConfiguracaoMonitoramentoFauna();
	},

	filters: {
		singleName(valor){
			return valor;
			// return valor.replace(/.\w+$/,"");
		}
	},

	methods: {
		async getListaMonitoramentoFauna() {
			let params, where;
			if (this.filtroSelecionado === 'Sem-foto') {
				this.listaMonitoramentoFauna = await vmGlobal.getFromController('Relatorios/getListaMonitoramentoFaunaSemFoto?codigo=' + this.CodigoAtividadeCronogramaFisico)
			} else {
				if (this.filtroSelecionado != "") {
					where = {
						Grupo: this.filtroSelecionado
					}
				} else {
					where = {
						CodigoAtividadeCronogramaFisico: this.CodigoAtividadeCronogramaFisico
					}
				}

				params = {
					table: 'relatorioMonitoramentoFaunaAnimais',
					where: where
				}

				//lista pontos
				this.listaMonitoramentoFauna = await vmGlobal.getFromAPI(params, 'cgmab');
			}

			// lista para filtro dos pontos
			if (this.listaMonitoramentoFaunaFiltro === 0) {
				this.listaMonitoramentoFauna.push({
					Grupo: 'Sem-foto'
				});
				this.listaMonitoramentoFaunaFiltro = this.listaMonitoramentoFauna.map(x => x
					.Grupo).filter(function(elem, index, self) {
					return index === self.indexOf(elem);
				});
			}

			for (let i = 0; i < this.listaMonitoramentoFauna.length; i++) {
				this.listaMonitoramentoFauna[i].modelDadosFotosInformacao = []
				this.errorsAddCondicionante[i] = []
			}
		},

		async getModuloFauna() {
			//Lista os Modulos Fauna
			params = {
				table: 'configFaunaModulos',
				where: {
					CodigoEscopoPrograma: this.CodigoEscopoPrograma
				}
			}
			this.listaModuloFauna = await vmGlobal.getFromAPI(params, 'cgmab');
		},

		async getConfiguracaoMonitoramentoFauna() {
			//lista a Configuração da Armadilhas para o Monitoramento de Fauna
			let params = {
				table: 'escopoDeRecursos',
				join: {
					table: 'configFaunaFotos',
					on: 'CodigoRecurso'
				},
				where: {
					CodigoEscopoPrograma: this.CodigoEscopoPrograma
				}
			}
			this.pontosConfiguracaoFauna = await vmGlobal.getFromAPI(params, 'cgmab');
		},

		async salvarDadosInsercaoFauna() {
			this.listaExcel.CodigoAtividadeCronogramaFisico = this.CodigoAtividadeCronogramaFisico;
			var formData = new FormData();
			formData.append('Dados', JSON.stringify(this.listaExcel));
			formData.append('CodigoAtividadeCronogramaFisico', this.CodigoAtividadeCronogramaFisico);
			await axios.post(base_url + 'Relatorios/salvarDadosInsercaoFauna', formData)
				.then((resp) => {
					console.log(resp.data);
					if (resp.data.status === true) {
						Swal.fire({
							position: 'center',
							type: 'success',
							title: 'Salvo!',
							text: 'Dados inseridos com sucesso.',
							showConfirmButton: true,
						});
					} else {
						Swal.fire({
							position: 'center',
							type: 'error',
							title: 'Erro!',
							text: "Erro ao tentar salvar arquivo. " + e,
							showConfirmButton: true,
						});
					}
				});
			this.mostrarListaExcel = false;
			this.getListaMonitoramentoFauna();
			$('.dropify-clear').click();
			$('.dropify-wrapper').removeClass('invisivel');
			$('.ls-toggle-menu').removeClass('modal-open');
			$('.theme-blue').removeClass('modal-open');
			$('.modal').hide();
			$('.modal-backdrop').remove();
		},

		deletarRegistros(codigo) {
			//Deletar Registros
			Swal.fire({
				title: 'Você tem certeza?',
				text: "Você não poderá reverter isso!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim, excluir!'
			}).then((result) => {
				if (result.value) {
					let fd = new FormData();
					fd.append('Codigo', codigo)
					var txt;

					axios.post('<?=base_url('Relatorios/deletarRegistrosFauna')?>', fd)
						.then(response => {
							vmMonitoramentoInsercaoDados.getListaMonitoramentoFauna();
							Swal.fire(
								'Excluído!',
								'Sua fotografia foi excluída com sucesso.',
								'success'
							)
						}).catch(function(error) {
							console.log(error)
						}).finally(() => {})
				}
			})
		},

		deletarFotos(codigo, caminho) {
			let params = {
				table: 'relatorioMonitoramentoFaunaAnimais',
				data: {
					Caminho: null,
					NomePontoMonitoramento: null,
					NomeFoto: null,
					Lat: null,
					Lng: null,
					DataFoto: null,
					CodigoConfiguracaoMonitoramentoFaunaFotos: null,
					CodigoModulo: null,
					ObservacaoFotografia: null,
				},
				where: {
					CodigoRelatorioAnimais: codigo
				}
			}

			//Exclui as fotografias
			Swal.fire({
				title: 'Você tem certeza?',
				text: "Você não poderá reverter isso!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sim, excluir!'
			}).then(async (result) => {
				if (result.value) {
					await vmGlobal.updateFromAPI(params, null, 'cgmab');
					vmGlobal.deleteFile(caminho);
					vmMonitoramentoInsercaoDados.getListaMonitoramentoFauna();
				}
			})
		},

		qtdFotos() {
			var myfiles = document.getElementById("imagemMaterial");
			this.totalFotos = myfiles.files.length;
		},

		verificaInformacoesFotosAnimais(i, codigo) {
			//Salva as informações das fotografias
			$("#error-" + i).html('').stop()
			var salvarinformacoes = true;
			var html = ''
			if (!this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoModulo'] ||
				(!this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoConfiguracaoMonitoramentoFaunaFotos'] && this.listaMonitoramentoFauna[i].TipoAvistamento == 'Armadilha')) {
				html += '<div class="alert alert-warning p-3 w-100 " >' +
					'<h5>Erro ao salvar. Por favor Preencha os campos abaixo:</h5>'
				'<ul>'

				if (this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoModulo'] == undefined) {
					salvarinformacoes = false;
					html += '<li>O Módulo é obrigatório.</li>';
				}
				if (this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoConfiguracaoMonitoramentoFaunaFotos'] ==
					undefined && this.listaMonitoramentoFauna[i].TipoAvistamento == 'Armadilha') {
					salvarinformacoes = false;
					html += '<li>A Armadilha é obrigatório.</li>';
				}
				if (html != '') {
					html += '</ul>' +
						'</div>'
				}
			}
			$("#error-" + i).html(html)

			this.salvarInformacoesFotosAnimais(i, codigo, salvarinformacoes)
		},

		async salvarInformacoesFotosAnimais(i, codigo, salvarinformacoes) {
			var params = {
				table: 'relatorioMonitoramentoFaunaAnimais',
				data: {
					CodigoModulo: this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoModulo'],
					CodigoConfiguracaoMonitoramentoFaunaFotos: this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoConfiguracaoMonitoramentoFaunaFotos'],
					ObservacaoFotografia: this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['ObservacaoFotografia'],
				},
				where: {
					CodigoRelatorioAnimais: codigo
				}
			}

			if (this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['ObservacaoFotografia'] == null) {
				if (salvarinformacoes == true)
					Swal.fire({
						title: 'Deseja salvar sem observações?',
						text: "Você não poderá reverter isso!",
						icon: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#1e7e34',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Sim, salvar!'
					}).then(async (result) => {
						if (result.value) {
							if (salvarinformacoes == true) {
								await vmGlobal.updateFromAPI(params, null, 'cgmab')
								vmMonitoramentoInsercaoDados.getListaMonitoramentoFauna();
							}
						}
					});
			} else {
				await vmGlobal.updateFromAPI(params, true, 'cgmab')
				vmMonitoramentoInsercaoDados.getListaMonitoramentoFauna();
			}
		},

		async verifcaFotos() {
			//Adiciona as fotografias
			var myfiles = document.getElementById("imagemMaterial");
			var filesFotos = myfiles.files;
			var dataFoto = new FormData();

			//Listo os IDs do Registros do banco de dados
			let arrayIDs = [];
			this.listaMonitoramentoFauna.forEach((valor, index) => arrayIDs.push(valor.CodigoRelatorioAnimais));
			
			let strError = "";
			let numArray = 0;

			for (i = 0; i < filesFotos.length; i++) {
				//Separa o nome da foto 
				let nameFile = filesFotos[i].name.split("_");

				//Verifica se o ID existe no banco de dados
				let fotoID = filesFotos[i].name.split("_")[0];
				const findID = arrayIDs.includes(fotoID);

				//Verifica se a foto existe no banco de dados
				let valorFotografia = this.listaMonitoramentoFauna.filter(valor => valor.CodigoRelatorioAnimais == fotoID);
				let caminhoFotografia = '';
				
				if (valorFotografia.length > 0) {
					caminhoFotografia = valorFotografia[0].Caminho;
				} else {
					caminhoFotografia = null;
				}
				//Seleciona as fotos com erros
				if (nameFile.length != 2 || !findID || caminhoFotografia != null) {
					strError += filesFotos[i].name + "<br>";
				} else {
					// dataFoto.append(numArray, filesFotos[i]);
					dataFoto.append(numArray, filesFotos[i]);
					numArray += 1;					
				}	
			}
			if(strError != ""){
				await Swal.fire({
					type: 'error',
					title: 'Nome da Imagem está fora dos padrões',
					text: 'Por favor Corrigir!',
					html: 'Por favor Corrigir! </br>' + '</br>' +
						   strError,
				})
			} 
			await vmMonitoramentoInsercaoDados.addFotos(dataFoto); 
		},

		addFotos(dataFoto) {
			$.ajax({
				url: '<?=base_url('Relatorios/getGpsFotosFauna')?>',
				method: 'POST',
				data: dataFoto,
				contentType: false,
				processData: false,
				success: function(data) {
					Swal.fire({
						position: 'center',
						type: 'success',
						title: 'Salvo!',
						text: 'Fotos salvas com sucesso.',
						showConfirmButton: true,
					});
					document.getElementById('imagemMaterial').value = null;
				},
				error: function(error) {
					console.log(error);
					Swal.fire({
						position: 'center',
						type: 'error',
						title: 'Erro!',
						text: "Erro ao tentar salvar arquivo. " + e,
						showConfirmButton: true,

					});
				},
				complete: function() {
					vmMonitoramentoInsercaoDados.getListaMonitoramentoFauna();
					vmMonitoramentoInsercaoDados.filtroSelecionado = '';
					vmMonitoramentoInsercaoDados.totalFotos = 0
					$('.dropify-clear').click();
				}
			});
		},

		async importarDadosInsercao() {
			//upload do arquivo excel
			this.file = this.$refs.file.files[0];

			let formData = new FormData();
			formData.append('file', this.file);

			await axios.post('<?=base_url('API/uploadXLS')?>',
					formData, {
						headers: {
							'Content-Type': 'multipart/form-data'
						}
					})
				.then((response) => {
					this.listaExcel = response.data
				})
				.catch((error) => {
					console.log(error);
				})
				.finally(() => {
					this.mostrarListaExcel = true;
					$(".dropify-wrapper").addClass('invisivel');
				});
		},

		async carregaArmadilha(i) {
			let params = {
				table: 'configFaunaFotos',
				where: {
					CodigoModulo: this.listaMonitoramentoFauna[i].modelDadosFotosInformacao['CodigoModulo'],
				}
			}
			this.listaArmadilha = await vmGlobal.getFromAPI(params, 'cgmab');
		},

		buscaInformacaoArmadilha(PontosFauna, Codigo, CampoInformacao) {
			//Busca a FitoFisinomia da Configuração do Ponto
			for (let ponto of Object.keys(PontosFauna)) {
				let pontoSelecionado = PontosFauna[ponto]
				if (Codigo === pontoSelecionado.CodigoConfiguracaoMonitoramentoFaunaFotos) {
					return pontoSelecionado[CampoInformacao]
				}
			}
		},

		buscaInformacaoModulo(listaModuloFauna, Codigo, CampoInformacao) {
			//Busca o nome do modulo
			for (let modulo of Object.keys(listaModuloFauna)) {
				let moduloSelecionado = listaModuloFauna[modulo]
				if (Codigo === moduloSelecionado.CodigoModulo) {
					return moduloSelecionado[CampoInformacao]
				}
			}
		},

		// criarSessaoCodigoPrograma() {
		//     axios.post(base_url + 'ConfiguracaoProgramas/criarSessaoCodigoPrograma', $.param({
		//         CodigoModulo: this.CodigoModulo
		//     }));
		// }
	}
})
$(function() {
	"use strict";
	$('.dropify').dropify({
		messages: {
			default: "Solte ou clique para anexar arquivo."
		}
	});
});
</script>
