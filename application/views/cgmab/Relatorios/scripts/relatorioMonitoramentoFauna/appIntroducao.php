<script>
    var vmIntroducaoMonitoramento = new Vue({
        el: '#introducaoMonitoramento',
        data() {
            return {
                CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),
                dadosFormulario: {
                    Introducao: ''
                },
                listaFaunaDocumentos: 0,
                update: false,
                totalDocumentos: 0,
            }
        },
        async mounted() {
            await this.getListaDocumentos();
			setTimeout(async () => {
				await this.getDadosDescricao();
			}, 500)
        },
        methods: {
            async getListaDocumentos() {
                $("#listadeDocumentos").DataTable().destroy();
                const params = {
                    table: 'relatorioMonitoramentoFaunaAnexos',
                    where: {
                        Ativo: 1
                    }
                }
                this.listaFaunaDocumentos = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#listadeDocumentos', true);
            },

            async getDadosDescricao() {
                var params = {
                    table: 'relatorioMonitoramentoFaunaObservacao',
                    where: {
                        CodigoAtividadeCronogramaFisico: this.CodigoAtividadeCronogramaFisico
                    }
                };
                var dadosBusca = await vmGlobal.getFromAPI(params, 'cgmab');
                if (dadosBusca.length != 0) {
                    this.dadosFormulario = dadosBusca[0];
                    await this.$nextTick(() => {
                    	if(this.dadosFormulario.Introducao) {
							CKEDITOR.instances['Introducao'].setData(atob(this.dadosFormulario.Introducao));
						}
                    });
                    this.update = true
                } else {
                    // CKEDITOR.instances['Observacoes'].setData('');
                    this.update = false
                }
            },

            qtdDocumentos() {
                var myfiles = document.getElementById("documentosMaterial");
                this.totalDocumentos = myfiles.files.length;
            },

            async salvarIntroducao() {
                var data = new FormData();
                data.append('Introducao', this.dadosFormulario.Introducao);

                if (this.update === false) {
                    var params = {
                        table: 'relatorioMonitoramentoFaunaObservacao',
                        data: {
                            Introducao: String(btoa(CKEDITOR.instances.Introducao.getData())),
                            CodigoAtividadeCronogramaFisico: this.CodigoAtividadeCronogramaFisico
                        },
                    }
                    await vmGlobal.insertFromAPI(params, null, 'cgmab');
                    await this.getDadosDescricao();
                } else {
                    var CodigoObservacoes = parseInt(this.dadosFormulario.CodigoRelatorioObservacoes);
                    var params = {
                        table: 'relatorioMonitoramentoFaunaObservacao',
                        data: {
                            Introducao: String(btoa(CKEDITOR.instances.Introducao.getData()))
                        },
                        where: {
                            CodigoRelatorioObservacoes: CodigoObservacoes
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null, 'cgmab');
                    await this.getDadosDescricao();
                }
            },

            async addDocumentos() {
                var myfiles = document.getElementById("documentosMaterial");
                var files = myfiles.files;
                var data = new FormData();

                for (i = 0; i < files.length; i++) {
                    data.append(i, files[i]);
                }

                data.append('codigoAtividade', parseInt(this.CodigoAtividadeCronogramaFisico));

                $.ajax({
                    url: '<?= base_url('Relatorios/postMonitoramentoFaunaDocumentos') ?>',
                    method: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Salvo!',
                            text: 'Fotos salvas com sucesso.',
                            showConfirmButton: true,
                        });
                        document.getElementById('documentosMaterial').value = null;
                    },
                    error: function(error) {
                        console.log(error);
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao tentar salvar arquivo. " + e,
                            showConfirmButton: true,

                        });
                    },
                    complete: function() {
                        vmIntroducaoMonitoramento.getListaDocumentos();
                        vmIntroducaoMonitoramento.totalDocumentos = 0
                        $('.dropify-clear').click();
                    }
                });
            },

            async deletar(codigo, index, caminho) {
                params = {
                    table: 'relatorioMonitoramentoFaunaAnexos',
                    where: {
                        CodigoRelatorioAnexo: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                await vmGlobal.deleteFile(caminho);
                await this.getListaDocumentos();
            },
        }
    })

    $(function() {
        "use strict";
        $('.dropify').dropify({
            messages: {
                default: "Solte ou clique para anexar arquivo."
            }
        });

        $(document).ready(function() {
            CKEDITOR.replace('editor1', {
                height: '400px'
            });

            CKEDITOR.instances['Introducao'].on("blur", function() {
                vmIntroducaoMonitoramento.dadosFormulario.Introducao = CKEDITOR.instances['Introducao'].getData();
            });
        });
    });
</script>
