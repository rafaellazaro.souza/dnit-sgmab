<script>
    var parametrosEscopo = location.search.slice(1);
    let codigoEscopo = parametrosEscopo.split('=')[1].split('&')[0];
    let atividade = location.search.slice(1).split('99cfb275a87d383fa2912331e3117e48')[2].split("=")[1].split("&")[0];
    let ano = parametrosEscopo.split('99cfb275a87d383fa2912331e3117e48')[3].split("=")[1].split('&')[0];
    let mes = parametrosEscopo.split('99cfb275a87d383fa2912331e3117e48')[4].split("=")[1].split('&')[0]
    //  console.log(parametrosEscopo.split('99cfb275a87d383fa2912331e3117e48')[3].split("=")[1].split('&')[0]);
    //  console.log(parametrosEscopo.split('99cfb275a87d383fa2912331e3117e48'));
    //cooreções 26/07/2020

    var vmApp = new Vue({
        data: {
            CodigoEscopo: vmGlobal.codificarDecodificarParametroUrl(codigoEscopo, 'decode'),
            DadosAtividade: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode'),
            mesAtividade: vmGlobal.codificarDecodificarParametroUrl(mes, 'decode'),
            anoAtividade: vmGlobal.codificarDecodificarParametroUrl(ano, 'decode'),
        },
    })


    var vmAbasRelatorio = new Vue({
        el: "#tabsRelatorioAgua",
        data: {

        },
        methods: {
            iniciarObservacoesCampanha() {
                vmObservacoesCampanha.iniciarTextArea();
            },
            iniciarAnaliseDados() {
                vmAnaliseDados.iniciarTextArea();
            },
            iniciarAnaliseResultados() {
                vmAnaliseResultados.iniciarTextArea();
            }
        }
    });
</script>