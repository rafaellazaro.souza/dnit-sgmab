<script>
	var session = eval('(<?php echo json_encode($_SESSION) ?>)');

	const atividade = vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode');
	const codigoEscopo = location.search.slice(1).split('&')[0].split('=')[1];

	var vmAppSupressaoVegetacao = new Vue({
		el: "#informacoesSupressaoVegetacao",
		data() {
			return {
				CodigoEscopo: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[0].split('=')[1], 'decode'),
				numeroContrato: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[1].split('=')[1], 'decode'),
				anoAtividade: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[3].split('=')[1], 'decode'),
				mesAtividade: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[4].split('=')[1], 'decode'),
			}
		},
	})

	const vmRelatorioAtividade = new Vue({
		el: '#relatorioMensal',
		data() {
			return {
				PerfilUsuario: '',
				Situacao: '',
			}
		},
		computed: {
			disableAprovado() {
				if (this.Situacao == 3) {
					return 'disabled="true" style="display:none;"';
				}
				return '';
			}
		},
		methods: {
			async getUsuario() {
				await vmGlobal.dadosUsuario();
				this.PerfilUsuario = vmGlobal.dadosDoUsuario.NomePerfil;
			},
			async getRelatorio() {
				var status = await vmGlobal.getFromAPI({
					table: 'tblRelatorioMensalProgramas',
					column: 'Situacao',
					where: {
						MesReferencia: vmAppSupressaoVegetacao.mesAtividade,
						AnoReferencia: vmAppSupressaoVegetacao.anoAtividade,
						CodigoEscopoPrograma: vmAppSupressaoVegetacao.CodigoEscopo,
					}
				});
				if (status.length != 0) {
					this.Situacao = status[0].Situacao;
				}
			},
			async closeButtons() {
				if (this.Situacao == 3 && this.PerfilUsuario) {
					var botoes = document.querySelectorAll('.btn-toBlock');
					var inputs = document.querySelectorAll('.input-toBlock');
					var tables = document.querySelectorAll('table');

					tables.forEach((el) => {
						var trs = $(el).dataTable();
						var rows = trs.$("tr");
						$(rows).each((i, val) => {
							$(val).find('td:eq(-1)').children('.btn-outline-danger').css({
								display: 'none'
							});

							$(val).find('td:eq(-1)').children('.btn-danger').css({
								display: 'none'
							});
						});
					});

					botoes.forEach((el) => {
						$(el).attr('disabled', true).css({
							display: 'none'
						});
					});

					inputs.forEach((el) => {
						$(el).attr('disabled', true);
					});

					$.each(CKEDITOR.instances, (el) => {
						this.$nextTick(() => {
							var text = CKEDITOR.instances[el].getData();
							CKEDITOR.instances[el].destroy();
							CKEDITOR.replace(el, {
								height: '400px',
								readOnly: true
							});
							CKEDITOR.instances[el].setData(text);
						});
					});


				}
			}
		},
		async mounted() {
			await this.getUsuario();
			await this.getRelatorio();
			await this.closeButtons();
		},
	})

	var vmAbasSupressaoVegetacao = new Vue({
		el: "#tabsSupressaoVegetacao",
		data: {

		},
		methods: {
			iniciarIntroducao() {
				// vmIntroducaoSupressaoVegetacao.getDadosDescricao();
				// vmIntroducaoSupressaoVegetacao.getListaDocumentos();
			},
			async iniciarInsercaoDados() {
				setTimeout(async () => {
					await vmInsercaoDadosSupreVege.gerarMapa();
					await vmInsercaoDadosSupreVege.setDadosMapaConfiguracaoASV();
					await vmInsercaoDadosSupreVege.setDadosMapaRelatorioASV();
				}, 1000);
			},
			// iniciarAnaliseGrupo(){
			// 	vmAnaliseGrupo.getListaGrupoFaunistico();
			// },
			// iniciarAnaliseModulo(){
			// 	vmAnaliseModulo.getlistaConfiguracaoModulos();
			// },
			iniciarDiscussoesConclusoes() {
				// vmDiscussoesConclusoesSupreVege.getDadosDiscussoesConclusoes();
			},
		}

	});
</script>