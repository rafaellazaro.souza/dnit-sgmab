<script>
    var vmAppCorrelacao = new Vue({
        el: '#correlacao',
        data() {
            return {
                
                analiseResultadosCorrelacao: [],
                analiseResultadosCorrelacaoConclusao: [],
               
            }
        },
        methods: {
            async getTextos(coluna) {       
                CKEDITOR.instances['txTanaliseResultadosCorrelacao'].setData('');//limpar textarea
                CKEDITOR.instances['txTanaliseResultadosCorrelacaoConclusao'].setData('');//limpar textarea
                
                axios.post(base_url+'Relatorios/getTextos', $.param({
                    coluna: coluna,
                    atividade: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode')
                }))
                .then((resp)=>{
                
                    
                    if(coluna === 'AnaliseResultadoProgramaAtropelamento'){
                        this.analiseResultadosCorrelacao = resp.data;
                        CKEDITOR.instances['txTanaliseResultadosCorrelacao'].setData(atob(this.analiseResultadosCorrelacao[0].AnaliseResultadoProgramaAtropelamento));//preencher textarea
                        
                    }
                    if(coluna === 'ConclusaoProgramaAtropelamento'){
                        this.analiseResultadosCorrelacaoConclusao = resp.data;
                        CKEDITOR.instances['txTanaliseResultadosCorrelacaoConclusao'].setData(atob(this.analiseResultadosCorrelacaoConclusao[0].ConclusaoProgramaAtropelamento));//preencher textarea
                        
                    }
                })
                .catch((e)=>{
                    console.log(e)
                })
               
                
            },
            async salvarTexto(campo, codigo = null) {
                $(".page-loader-wrapper").css('display', 'block');
                var params;

                if (campo == 'AnaliseResultadoProgramaAtropelamento') {
                    if (this.analiseResultadosCorrelacao.length == 0) { //insert
                        params = {
                            table: 'textosPassagemFauna',
                            data: {
                                CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode'),
                                AnaliseResultadoProgramaAtropelamento: btoa(CKEDITOR.instances['txTanaliseResultadosCorrelacao'].getData()),
                                DataCadastro: new Date()
                            }
                        }
                        await vmGlobal.insertFromAPI(params);
                    } else { //update
                        params = {
                            table: 'textosPassagemFauna',
                            data: {
                                AnaliseResultadoProgramaAtropelamento: btoa(CKEDITOR.instances['txTanaliseResultadosCorrelacaoConclusao'].getData()),
                            },
                            where: {
                                CodigoIntroducaoAnalise: codigo
                            }
                        }
                        await vmGlobal.updateFromAPI(params);
                    }

                }
                if (campo == 'ConclusaoProgramaAtropelamento') {
                    if (this.analiseResultadosCorrelacaoConclusao.length == 0) { //insert
                        params = {
                            table: 'textosPassagemFauna',
                            data: {
                                CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode'),
                                ConclusaoProgramaAtropelamento: btoa(CKEDITOR.instances['txTanaliseResultadosCorrelacaoConclusao'].getData()),
                                DataCadastro: new Date()
                            }
                        }
                        await vmGlobal.insertFromAPI(params);
                    } else { //update
                        params = {
                            table: 'textosPassagemFauna',
                            data: {
                                ConclusaoProgramaAtropelamento: btoa(CKEDITOR.instances['txTanaliseResultadosCorrelacaoConclusao'].getData()),
                            },
                            where: {
                                CodigoIntroducaoAnalise: codigo
                            }
                        }
                        await vmGlobal.updateFromAPI(params);
                    }

                }
                await this.getTextos('AnaliseResultadoProgramaAtropelamento');
                await this.getTextos('ConclusaoProgramaAtropelamento');
                $(".page-loader-wrapper").css('display', 'none');
            }
        }
    });
</script>