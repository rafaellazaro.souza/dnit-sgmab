<script>
    var vmAppIntroducao = new Vue({
        el: '#vmIntroducao',
        data() {
            return {
                introducao: [],
                listaDocumentos: [],
                totalDocumentos: 0,
                qtdDocumentos: 0,
                codigoAtividade: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode'),

            }
        },
        methods: {
            async getIntro() { //buscar introducao
                CKEDITOR.instances['txtIntro'].setData('');
                let params = {
                    table: 'introPassagem',
                    where: {
                        CodigoAtividadeCronogramaFisico: this.codigoAtividade
                    }
                }
                this.introducao = await vmGlobal.getFromAPI(params);
                if(this.introducao.length > 0){
                    CKEDITOR.instances['txtIntro'].setData(atob(this.introducao[0].Introducao));
                }else{
                    CKEDITOR.instances['txtIntro'].setData('Digite a introdução');
                }
                
            },
            async getAnexos() { //buscarAnexos
                let params = {
                    table: 'documentosPassagem',
                    where: {
                        CodigoAtividadeCronogramaFisico: this.codigoAtividade
                    }
                }
                this.listaDocumentos = await vmGlobal.getFromAPI(params);
            },
            async salvarDados(codigo = null) { // salva ou atualiza dados
                $(".page-loader-wrapper").css('display', 'block');

            var params;
                if(this.introducao.length > 0){// atualizar
                    params = {
                        table: 'introPassagem',
                        data: {
                            Introducao: btoa(CKEDITOR.instances['txtIntro'].getData())
                        },
                        where: {
                            CodigoIntroducaoAnalise: codigo
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null);
                }else{//inserir
                    params = {
                        table: 'introPassagem',
                        data: {
                            CodigoAtividadeCronogramaFisico: this.codigoAtividade,
                            Introducao: btoa(CKEDITOR.instances['txtIntro'].getData()),
                            DataCadastro: new Date()
                        }
                    }
                    await vmGlobal.insertFromAPI(params);
                }
                await this.getIntro();
                 $(".page-loader-wrapper").css('display', 'none');
            },
            async addDocumentos() {
                var myfiles = document.getElementById("documentosMaterial");
                var files = myfiles.files;
                var data = new FormData();

                for (i = 0; i < files.length; i++) {
                    data.append(i, files[i]);
                    console.log('files[i]', files[i])
                }

                data.append('codigoAtividade', parseInt(this.codigoAtividade));

                console.log('data', data);
                $.ajax({
                    url: '<?= base_url('Relatorios/postPassagemFaunaDocumentos') ?>',
                    method: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Salvo!',
                            text: 'Fotos salvas com sucesso.',
                            showConfirmButton: true,
                        });
                        document.getElementById('documentosMaterial').value = null;
                    },
                    error: function(error) {
                        console.log(error);
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao tentar salvar arquivo. " + e,
                            showConfirmButton: true,

                        });
                    },
                    complete: function() {
                        vmAppIntroducao.getAnexos();
                        vmAppIntroducao.totalDocumentos = 0
                        $('.dropify-clear').click();
                    }
                });
            },
            async deletarArquivo(codigo, caminho){
                await vmGlobal.deleteFromAPI({
                    table: 'documentosPassagem',
                    where: {
                        CodigoPassagemDocumento:  codigo
                    }
                });
                await vmGlobal.deleteFile(caminho);
                await this.getAnexos();
            }
        }
    });

    $(document).ready(function() {
        "use strict";
        $('.dropify').dropify({
            messages: {
                default: "Solte ou clique para anexar arquivo."
            }
        });
    });
</script>