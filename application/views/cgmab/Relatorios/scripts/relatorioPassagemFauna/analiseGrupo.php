<script>
  vmAppAnaliseGrupo = new Vue({
    el: '#analiseGrupo',
    data() {
      return {
        mostrarGrafico: true,
        listaAnimaisCapurados: [],
        listaGruposFaunisticos: [],
        analiseResultadoGrupoFaunistico: [],
        codigoGrupoFaunistico: 0,
        nomeGrupoSelecionado: '',
        dadosGrafico: [
          ['campanhas', 'Abundância', 'Riqueza']
        ],
        totalAbundancia: 0
      }

    },
    watch: {
      codigoGrupoFaunistico: function(val) {
        this.dadosGrafico = [
          ['campanhas', 'Abundância', 'Riqueza']
        ]
        this.getDadosPassagemGrupoFaunistico();
      }
    },
    methods: {
      async getAnaliseResultadoGrupoFaunistico(coluna) {
        CKEDITOR.instances['txtAnaliseResultadosFauna'].setData(''); //limpar textarea
        axios.post(base_url + 'Relatorios/getTextos', $.param({
            coluna: coluna,
            cronograma: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode')
          }))
          .then(async (resp) => {
            this.analiseResultadoGrupoFaunistico = resp.data;
            CKEDITOR.instances['txtAnaliseResultadosFauna'].setData(atob(this.analiseResultadoGrupoFaunistico[0].AnaliseResultadoGrupoFaunistico)); //preencher textarea
          });
      },
      async salvarTexto() {
        var params;
        if (this.analiseResultadoGrupoFaunistico.length == 0) { //insert
          params = {
            table: 'textosPassagemFauna',
            data: {
              CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode'),
              AnaliseResultadoGrupoFaunistico: btoa(CKEDITOR.instances['txtAnaliseResultadosFauna'].getData()),
              DataCadastro: new Date()
            }
          }
        } else { //update
          params = {
            table: 'textosPassagemFauna',
            data: {
              AnaliseResultadoGrupoFaunistico: btoa(CKEDITOR.instances['txtAnaliseResultadosFauna'].getData()),
            },
            where: {
              CodigoIntroducaoAnalise: this.analiseResultadoGrupoFaunistico[0].CodigoIntroducaoAnalise
            }
          }
        }
      },
      async getDadosPassagemGrupoFaunistico() {

        this.listaAnimaisCapurados = await vmGlobal.getFromController('AreasApoio/getDadosPassagemGrupoFaunistico', {
          CodigoGrupoFaunistico: this.codigoGrupoFaunistico,
          CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode')
        });
        if (this.listaAnimaisCapurados.length == 0) {
          this.mostrarGrafico = false;
          Swal.fire({
            position: 'center',
            type: 'warning',
            title: 'OPA!',
            text: "Nenhum Resultado Encontrato para este Grupo Faunístico",
            showConfirmButton: true,

          });
        } else {
          this.mostrarGrafico = true;
          this.totalAbundancia = this.listaAnimaisCapurados[0].TotalRegistros
          await this.dadosGraficoRiquezaAbundancia();
        }
      },
      async getGruposFaunisticos() {
        let params = {
          table: 'gruposFaunisticos',
        }
        this.listaGruposFaunisticos = await vmGlobal.getFromAPI(params);
        await vmRelatorioAtividade.closeButtons();
      },
      async dadosGraficoRiquezaAbundancia() {
        this.dadosGrafico.RiquezaAbundancia = await vmGlobal.getFromController('AreasApoio/dadosRiquezaAbundancia', {
          codigoAtividade: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode'),
          codigoGrupoFaunistico: this.codigoGrupoFaunistico
        });
        if (this.dadosGrafico.RiquezaAbundancia.length > 0) {
          await this.graficoFRI();
        }
        await vmRelatorioAtividade.closeButtons();
      },
      async graficoFRI() {
        await google.charts.load('current', {
          'packages': ['bar']
        });

        // var passagem, percentual;
        // this.listaAnimaisCapurados.forEach(function(i, index){
        //   passagem = (i.TipoEstrutura+'/'+i.Nome+'/'+i.UF+'/'+i.BR).toString();
        //   percentual = i.TotalRegistrosGrupoFaunistico;
        //   vmAppAnaliseGrupo.dadosGrafico.push([passagem, vmAppAnaliseGrupo.totalAbundancia, percentual ]);
        // });

        var data = new google.visualization.arrayToDataTable([
          ['campanhas', 'Abundância', 'Riqueza'],
          ['Anterior', parseInt(this.dadosGrafico.RiquezaAbundancia[0].TotalRegistrosAnterior), parseInt(this.dadosGrafico.RiquezaAbundancia[0].TotalRiquezaAnterior)], //anterior
          ['Atual', parseInt(this.dadosGrafico.RiquezaAbundancia.length), parseInt(this.dadosGrafico.RiquezaAbundancia[0].TotalRiquesaAtual)] // atual
        ]);
        // var data = new google.visualization.arrayToDataTable(this.dadosGrafico);

        var options = {
          width: 1200,
          vAxis: {
            minValue: 0
          },
          chart: {
            title: 'Riqueza e Abundância',
            // subtitle: 'Riqueza'
          },
          bar: {
            groupWidth: "85%"
          },
          orientation: 'horizontal',

        };

        var chart = new google.charts.Bar(document.getElementById('graficoRiquezaAbundancia'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));

      }

    },
  });
</script>
<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<script type="text/javascript">
  google.charts.load('current', {
    'packages': ['bar']
  });
  google.charts.setOnLoadCallback(drawStuff);

  function drawStuff() {

  };
</script>