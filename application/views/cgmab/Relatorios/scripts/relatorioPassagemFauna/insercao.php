<script>
    vmAppInsercao = new Vue({
        el: '#vmInsercao',
        data() {
            return {
                mostrarListaExcel: false,
                listaExcel: [],
                listaImportada: [],
                listaAnimaisFiltro: [],
                listaAnimais: [],
                listaFotosAnimais: [],
                listaArmadilhas: [],
                listaPassagens: [],
                totalFotos: 0,
                pendencias: true,
                model: {
                    ZonaFoto: '',
                    CNFoto: '',
                    CEFoto: '',
                    ElevacaoFoto: ''
                },
            }
        },
        // watch: {
        //     pendencias: async function(val) {
        //         await this.getFotosAnimais();
        //     }
        // },

        methods: {
            async getAnimaisCaputrados() {
                let params = {
                    table: 'animasCapturadosNaPassagem',
                    where: {
                        CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode')
                    },
                }
                this.listaAnimais = await vmGlobal.getFromAPI(params);
            },
            async getFotosAnimaisTemp() {
                let params = {
                    table: 'fotoAnimalCapturadoPassagem',
                    where: {
                        StatusFoto: 0
                    }
                }
                this.listaFotosAnimais = await vmGlobal.getFromAPI(params);
                if (this.listaFotosAnimais.length == 0) {
                    this.pendencias = false;
                    await this.getFotosAnimais();
                }
            },
            async getFotosAnimais() {

                let params = {
                    table: 'animasCapturadosNaPassagem',
                    // join: {
                    //     table: 'fotoAnimalCapturadoPassagem',
                    //     on: 'CodigoAnimalCapturado'
                    // },
                    joinLeft: [{
                            table1: 'animasCapturadosNaPassagem',
                            table2: 'fotoAnimalCapturadoPassagem',
                            on: 'CodigoAnimalCapturado'
                        },
                        {
                            table1: 'animasCapturadosNaPassagem',
                            table2: 'armadilhasDaPassagem',
                            on: 'CodigoConfiguracaoPassagemFaunaArmadilha'
                        },
                        {
                            table1: 'armadilhasDaPassagem',
                            table2: 'escopoDeRecursos',
                            on: 'CodigoRecurso'
                        },
                        {
                            table1: 'escopoDeRecursos',
                            table2: 'equipamentos',
                            on: 'CodigoEquipamento'
                        },
                        {
                            table1: 'armadilhasDaPassagem',
                            table2: 'areasApoio',
                            on: 'CodigoAreaApoio'
                        },


                    ],
                    where: {
                        CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode')
                    }
                }
                if (this.pendencias == false) {
                    this.listaFotosAnimais = await vmGlobal.getFromAPI(params);
                }
                // if (this.listaFotosAnimais.length == 0) {
                //     this.pendencias = true;
                // }
            },
            async getPassagens() {
                let params = {
                    table: 'areasApoio',
                    whereIn: {
                        Estrutura: ['OAC', 'OAE']
                    }
                }
                this.listaPassagens = await vmGlobal.getFromAPI(params);
            },
            async getArmadilhas(event) {
                this.listaArmadilhas = await vmGlobal.getFromAPI({
                    table: 'armadilhasDaPassagem',
                    joinLeft: [{
                            table1: 'armadilhasDaPassagem',
                            table2: 'escopoDeRecursos',
                            on: 'CodigoRecurso'
                        },

                        {
                            table1: 'escopoDeRecursos',
                            table2: 'equipamentos',
                            on: 'CodigoEquipamento'
                        },
                    ],
                    where: {
                        CodigoAreaApoio: event.target.value
                    }
                })

                // axios.post(base_url + 'Equipamentos/getArmadilhasPassagem')
                //     .then((resp) => {
                //         this.listaArmadilhas = resp.data
                //     })
                //     .catch((e) => {
                //         console.log(error);
                //     })
                //     .finally(() => {

                //     });

            },
            async updateFoto(codigo) {
                let params = {
                    table: 'fotoAnimalCapturadoPassagem',
                    data: {
                        StatusFoto: 1
                    },
                    where: {
                        CodigoFotoAnimalCapturado: codigo
                    }
                }
                await vmGlobal.updateFromAPI(params, false, 'cgmab', false);
            },
            async updateArmadilha(codigo, event, codigoFoto) {

                await this.updateFoto(codigoFoto);
                let params = {
                    table: 'animasCapturadosNaPassagem',
                    data: {
                        CodigoConfiguracaoPassagemFaunaArmadilha: event.target.value
                    },
                    where: {
                        CodigoAnimalCapturado: codigo
                    }
                }
                this.listaArmadilhas = [];
                await vmGlobal.updateFromAPI(params, false, 'cgmab', false);
                await this.getFotosAnimaisTemp();

                // if (this.pendencias == false) {
                //     await this.getFotosAnimais();
                // } else {
                //     await this.getFotosAnimaisTemp();
                // }
            },
            qtdFotos() {

            },
            addFotos() {
                var myfiles = document.getElementById("imagemMaterial");
                var files = myfiles.files;
                var data = new FormData();

                for (i = 0; i < files.length; i++) {
                    data.append(i, files[i]);
                }

                $.ajax({
                    url: '<?= base_url('Relatorios/getGpsFotosFaunaAnimais') ?>',
                    method: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Salvo!',
                            text: 'Fotos salvas com sucesso.',
                            showConfirmButton: true,
                        });
                        document.getElementById('imagemMaterial').value = null;
                    },
                    error: function(error) {
                        console.log(error);
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao tentar salvar arquivo. " + error,
                            showConfirmButton: true,

                        });
                    },
                    complete: function() {
                        vmAppInsercao.getFotosAnimaisTemp();
                        $('.dropify-clear').click();
                    }
                });
            },

            async importarDadosInsercao() {
                //upload do arquivo excel
                this.file = this.$refs.file.files[0];

                let formData = new FormData();
                formData.append('file', this.file);

                await axios.post('<?= base_url('API/uploadXLS') ?>',
                        formData, {
                            headers: {
                                'Content-Type': 'multipart/form-data'
                            }
                        })
                    .then((response) => {
                        this.listaExcel = response.data
                    })
                    .catch((error) => {
                        console.log(error);
                    })
                    .finally(() => {
                        this.mostrarListaExcel = true;
                        // $(".dropify-wrapper").addClass('invisivel');
                    });
            },
            async salvarAnimaisImportados() {
                // CodigoAtividadeCronogramaFisico
                var lista = [];
                var grupoFaunistico = 0;
                this.listaExcel.forEach(function(item, index) {
                    var codigoGrupo = item.GrupoFaunistico.split("-")[0].trim();
                    if (codigoGrupo === 'I') {
                        grupoFaunistico = 1
                    }
                    if (codigoGrupo === 'II') {
                        grupoFaunistico = 2
                    }
                    if (codigoGrupo === 'III') {
                        grupoFaunistico = 3
                    }
                    if (codigoGrupo === 'IV') {
                        grupoFaunistico = 4
                    }
                    if (codigoGrupo === 'V') {
                        grupoFaunistico = 5
                    }
                    if (codigoGrupo === 'VI') {
                        grupoFaunistico = 6
                    }
                    if (codigoGrupo === 'VII') {
                        grupoFaunistico = 7
                    }
                    lista.push({
                        CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode'),
                        CodigoGrupoFaunistico: grupoFaunistico,
                        Classe: item.Classe,
                        Especie: item.Especie,
                        Familia: item.Familia,
                        GrupoFaunistico: item.GrupoFaunistico,
                        NomeComum: item.NomeComum,
                        Observacoes: item.Observacoes,
                        Ordem: item.Ordem,
                        TipoRegistro: item.TipoRegistro
                    })
                });

                var dados = new FormData();
                dados.append('Dados', JSON.stringify(lista))

                await axios.post(base_url + 'Relatorios/insertAnimaisPassagem', dados)
                    .then((resp) => {
                        if (resp.data == true) {
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Salvo!',
                                text: 'Dados inseridos com sucesso.',
                                showConfirmButton: true,
                            });
                        }
                    }).catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        this.getAnimaisCaputrados();
                        $("#defaultModal").modal('hide');
                    });

            },
            setDadosUpdate(campo, event, update = false, codigo = null) {

                if (campo == 'Foto') {
                    this.model.ZonaFoto = event.target.value;
                }
                if (campo == 'CNFoto') {
                    this.model.CNFoto = event.target.value;

                }
                if (campo == 'CEFoto') {
                    this.model.CEFoto = event.target.value;

                }
                if (campo == 'ElevacaoFoto') {
                    this.model.ElevacaoFoto = event.target.value;

                }
                if (campo == 'ObservacoesFoto') {
                    this.model.ObservacoesFoto = event.target.value;

                }
                if (campo == 'CodigoRecurso') {
                    this.model.CodigoRecurso = event.target.value;

                }
                if (update == true) {
                    if (this.model.ObservacoesFoto) {
                        this.updateDados(codigo, true);
                    } else {
                        this.updateDados(codigo);
                    }
                }

            },
            async updateDados(codigo, observacao = false) {
                var params;
                if (observacao == false) {
                    params = {
                        table: 'fotoAnimalCapturadoPassagem',
                        data: this.model,
                        where: {
                            CodigoFotoAnimalCapturado: codigo
                        }
                    }
                } else {
                    params = {
                        table: 'fotoAnimalCapturadoPassagem',
                        data: {
                            ObservacoesFoto: this.model.ObservacoesFoto
                        },
                        where: {
                            CodigoFotoAnimalCapturado: codigo
                        }
                    }
                }

                await vmGlobal.updateFromAPI(params, null);
                this.getFotosAnimais();
                this.model = {
                    Zona: '',
                    CN: '',
                    CE: '',
                    Elevacao: ''
                }

            },
            async deletarAnimal(codigo, index) {
                let params = {
                    table: 'animasCapturadosNaPassagem',
                    where: {
                        CodigoAnimalCapturado: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                this.listaAnimais.splice(index, 1);
            },
            deletarFotos(codigo, caminho) {
                novoCaminho = caminho.split('webroot');
                let fd = $.param({
                    Codigo: codigo,
                    Caminho: 'webroot' + novoCaminho[1],
                    fotosAnimais: true
                });
                var txt;
                var r = confirm("Deseja deletar a foto?");
                if (r == true) {
                    axios.post('<?= base_url('ConfiguracaoProgramas/deletarFotosPassagem') ?>', fd)
                        .then(response => {
                            this.getFotosAnimais();
                        }).catch(function(error) {
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: 'Erro!',
                                text: "Erro ao tentar  ardeletar arquivo. " + e,
                                showConfirmButton: true,

                            });
                        }).finally(() => {

                        })
                }

            },
       
        }
    });
</script>