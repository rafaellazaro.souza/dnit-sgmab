<script>
    var vmAppDiscussoes = new Vue({
        el: '#discucoes',
        data() {
            return {
                analiseResultadosDiscucoes: '',
                analiseResultadosConclusoes: '',
                analiseResultadosBibliografia: '',


            }
        },
        methods: {
            async getTextos(coluna) {
                CKEDITOR.instances['txTanaliseResultadosDiscucoes'].setData(''); //limpar textarea
                CKEDITOR.instances['txTanaliseResultadosConclusoes'].setData(''); //limpar textarea
                CKEDITOR.instances['txTanaliseResultadosBibliografia'].setData(''); //limpar textarea
                CKEDITOR.instances['txTanaliseResultadosCorrelacao'].setData(''); //limpar textarea
                CKEDITOR.instances['txTanaliseResultadosCorrelacaoConclusao'].setData(''); //limpar textarea

                axios.post(base_url + 'Relatorios/getTextos', $.param({
                        coluna: coluna,
                        atividade: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode')
                    }))
                    .then((resp) => {
                        if (coluna === 'AnaliseResultadoDiscussao') {
                            this.analiseResultadosDiscucoes = resp.data;
                            CKEDITOR.instances['txTanaliseResultadosDiscucoes'].setData(this.analiseResultadosDiscucoes[0].AnaliseResultadoDiscussao ? atob(this.analiseResultadosDiscucoes[0].AnaliseResultadoDiscussao) : 'Digite aqui'); //preencher textarea

                        }
                        if (coluna === 'Conclusao') {
                            this.analiseResultadosConclusoes = resp.data;
                            CKEDITOR.instances['txTanaliseResultadosConclusoes'].setData(this.analiseResultadosConclusoes[0].Conclusao ? atob(this.analiseResultadosConclusoes[0].Conclusao) : 'Digite aqui'); //preencher textarea

                        }
                        if (coluna === 'Bibliografia') {
                            this.analiseResultadosBibliografia = resp.data;
                            CKEDITOR.instances['txTanaliseResultadosBibliografia'].setData(this.analiseResultadosBibliografia[0].Bibliografia ? atob(this.analiseResultadosBibliografia[0].Bibliografia) : 'Digite aqui');
                        }
                        if (coluna === 'AnaliseResultadoProgramaAtropelamento') {
                            this.analiseResultadosCorrelacao = resp.data;
                        }
                        if (coluna === 'ConclusaoProgramaAtropelamento') {
                            this.analiseResultadosCorrelacaoConclusao = resp.data;
                        }
                    })
                    .catch((e) => {
                        console.log(e)
                    })


            },
            async salvarTexto(campo, codigo = null) {
                $(".page-loader-wrapper").css('display', 'block');
                var params;

                if (campo == 'analiseResultadosDiscucoes') {
                    if (this.analiseResultadosDiscucoes.length == 0) { //insert
                        params = {
                            table: 'textosPassagemFauna',
                            data: {
                                CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode'),
                                AnaliseResultadoDiscussao: btoa(CKEDITOR.instances['txTanaliseResultadosDiscucoes'].getData()),
                                DataCadastro: new Date()
                            }
                        }
                        await vmGlobal.insertFromAPI(params);
                    } else { //update
                        params = {
                            table: 'textosPassagemFauna',
                            data: {
                                AnaliseResultadoDiscussao: btoa(CKEDITOR.instances['txTanaliseResultadosDiscucoes'].getData()),
                            },
                            where: {
                                CodigoIntroducaoAnalise: codigo
                            }
                        }
                        await vmGlobal.updateFromAPI(params);
                    }

                }
                if (campo == 'Conclusao') {
                    if (this.analiseResultadosConclusoes.length == 0) { //insert
                        params = {
                            table: 'textosPassagemFauna',
                            data: {
                                CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode'),
                                Conclusao: btoa(CKEDITOR.instances['txTanaliseResultadosConclusoes'].getData()),
                                DataCadastro: new Date()
                            }
                        }
                        await vmGlobal.insertFromAPI(params);
                    } else { //update
                        params = {
                            table: 'textosPassagemFauna',
                            data: {
                                Conclusao: btoa(CKEDITOR.instances['txTanaliseResultadosConclusoes'].getData()),
                            },
                            where: {
                                CodigoIntroducaoAnalise: codigo
                            }
                        }
                        await vmGlobal.updateFromAPI(params);
                    }

                }
                if (campo == 'Bibliografia') {
                    if (this.analiseResultadosBibliografia.length == 0) { //insert
                        params = {
                            table: 'textosPassagemFauna',
                            data: {
                                CodigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(atividade, 'decode'),
                                Bibliografia: btoa(CKEDITOR.instances['txTanaliseResultadosBibliografia'].getData()),
                                DataCadastro: new Date()
                            }
                        }
                        await vmGlobal.insertFromAPI(params);
                    } else { //update
                        params = {
                            table: 'textosPassagemFauna',
                            data: {
                                Bibliografia: btoa(CKEDITOR.instances['txTanaliseResultadosBibliografia'].getData()),
                            },
                            where: {
                                CodigoIntroducaoAnalise: codigo
                            }
                        }
                        await vmGlobal.updateFromAPI(params);
                    }

                }

                await this.getTextos('AnaliseResultadoDiscussao');
                await this.getTextos('Conclusao');
                await this.getTextos('Bibliografia');
                $(".page-loader-wrapper").css('display', 'none');
            }
        },
        mounted() {
            console.log(atividade)
        },
    });
</script>