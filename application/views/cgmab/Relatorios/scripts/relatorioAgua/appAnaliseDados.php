<script>
    var vmAnaliseDados = new Vue({
        el: '#analiseDados',
        data: {
            modelObservacaoCampanha: {},
            mostrarSelecaoPonto1: true,
            mostrarSelecaoPonto2: false,
            mostrarGrafico: false,
            mostrarEquipamentos: false,
            mostrarDadosPontoAnaliseDados: false,
            mostrarSelectStatus: false,
            listaPontosDoPrograma: [],
            listaMedicoesJusante: [],
            listaMedicoesMontante: [],
            listaEquipamentosUtilzados: [],
            pontoSelecionado: {},
            analiseDados: [],
            dadosGraficoParametro: [],
            nomeParametro: ''
        },
        methods: {
            async getPontos() {
                let params = {
                    table: 'pontosColetaAgua',
                    where: {
                        CodigoEscopoPrograma: vmApp.CodigoEscopo
                    }
                }
                this.listaPontosDoPrograma = await vmGlobal.getFromAPI(params);
            },
            async getPontosTipo() {
                await axios.post(base_url + 'Relatorios/getPontos', $.param({
                        CodigoEscopoPrograma: vmApp.CodigoEscopo
                    }))
                    .then((resp) => {
                        this.listaPontosTipo = resp.data;
                    })
            },
            async getAnaliseDados(ponto, tipoAmostra, mes, ano) {
                await axios.post(base_url + 'RelatorioMonitoramentoAgua/getParametroAnalizado', $.param({
                        CodigoConfiguracaoRecursosHidricosPontoColeta: ponto,
                        TipoPontoAmostra: tipoAmostra,
                        MesAtividade: mes,
                        AnoAtividade: ano
                    }))
                    .then((resp) => {
                        if (tipoAmostra === 'Jusante') {
                            this.listaMedicoesJusante = resp.data;
                        } else {
                            this.listaMedicoesMontante = resp.data;
                        }
                    });


            },
            async getEquipamentosUtilizados(codigo) {
                const params = {
                    table: 'equipamentosUtilizados',
                    joinLeft: [{
                            table1: 'equipamentosUtilizados',
                            table2: 'escopoDeRecursos',
                            on: 'CodigoRecurso'
                        },
                        {
                            table1: 'escopoDeRecursos',
                            table2: 'equipamentos',
                            on: 'CodigoEquipamento'
                        }
                    ],
                    where: {
                        CodigoRecursosHidricosParametros: codigo
                    }
                }
                this.listaEquipamentosUtilzados = await vmGlobal.getFromAPI(params);
                this.mostrarEquipamentos = true;
            },
            async iniciarTextArea() {
                await this.$nextTick(() => {
                    if (CKEDITOR.instances.txtAnaliseResultados) CKEDITOR.instances.txtAnaliseResultados.destroy();
                    CKEDITOR.replace('txtAnaliseResultados', {
                        height: '400px',
                    });
                })
                await this.getPontos();
            },
            async iniciarMedicoes(dados) {
                this.mostrarDadosPontoAnaliseDados = true;
                this.mostrarSelecaoPonto1 = false;
                this.pontoSelecionado = dados
                await this.getAnaliseDados(this.pontoSelecionado.CodigoConfiguracaoRecursosHidricosPontoColeta, 'Jusante', vmApp.anoAtividade, vmApp.mesAtividade);
                await this.getAnaliseDados(this.pontoSelecionado.CodigoConfiguracaoRecursosHidricosPontoColeta, 'Montante', vmApp.anoAtividade, vmApp.mesAtividade);
                this.verificaAnalise();
            },
            async verificaAnalise() {
                let params = {
                    table: 'analiseResutultadosParametrosAgua',
                    where: {
                        CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                        CodigoConfiguracaoRecursosHidricosPontoColeta: this.pontoSelecionado.CodigoConfiguracaoRecursosHidricosPontoColeta
                    }
                }
                this.analiseDados = await vmGlobal.getFromAPI(params);
                if (this.analiseDados.length === 0) {
                    this.salvarAnaliseDados(false);
                } else {
                    CKEDITOR.instances['txtAnaliseResultados'].setData(atob(this.analiseDados[0].AnaliseResultado));
                }
            },
            async salvarAnaliseDados(update) {
                if (update === false) {
                    let params = {
                        table: 'analiseResutultadosParametrosAgua',
                        data: {
                            CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                            CodigoConfiguracaoRecursosHidricosPontoColeta: this.pontoSelecionado.CodigoConfiguracaoRecursosHidricosPontoColeta,
                            AnaliseResultado: btoa('<p>Digite aqui a análise!</p>')
                        }
                    }
                    await vmGlobal.insertFromAPI(params, null, 'cgmab', null);
                } else {
                    let params = {
                        table: 'analiseResutultadosParametrosAgua',
                        data: {
                            AnaliseResultado: btoa(CKEDITOR.instances['txtAnaliseResultados'].getData()),
                            CodigoConfiguracaoRecursosHidricosPontoColeta: this.pontoSelecionado.CodigoConfiguracaoRecursosHidricosPontoColeta,

                        },
                        where: {
                            CodigoAnaliseResultados: this.analiseDados.CodigoAnaliseResultados
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null);
                }

            },
            async dadosGraficoParametros(parametro, nomeParametro) {
                this.nomeParametro = nomeParametro;
                await axios.post(base_url + 'relatorioMonitoramentoAgua/getDadosGraficoParametros', $.param({
                        CodigoConfiguracaoRecursosHidricosPontoColeta: this.pontoSelecionado.CodigoConfiguracaoRecursosHidricosPontoColeta,
                        CodigoRecursosHidricosParametros: parametro,
                        CodigoAtividade: vmApp.DadosAtividade
                    }))
                    .then((resp) => {
                        this.graficoParametros(resp.data);
                    })
                this.mostrarGrafico = true;
            },
            async graficoParametros(dadosGraficoParametro) {
                await google.charts.load('current', {
                    'packages': ['corechart']
                });

                var dadosGrafico = [
                    ['Mês', 'Jusante', 'Montante', 'Limite']
                ];
                dadosGraficoParametro.forEach((item) => {
                    dadosGrafico.push([(item.MesAtividade + '/' + item.AnoAtividade).toString(), parseInt(item.ValorJusante), parseInt(item.ValorMontante), parseInt(item.ValorLimite)]);

                });
                var data = google.visualization.arrayToDataTable(dadosGrafico);

                var options = {
                    width: 900,
                    height: 500,
                    title: 'Grafíco de Medições- Jusante e Montante',
                    vAxis: {
                        title: 'Valor'
                    },
                    hAxis: {
                        title: 'Mês Campanha'
                    },
                    vAxis: {
                        minValue: 0
                    },
                    seriesType: 'line',
                    series: {
                        2: {
                            type: 'line'
                        }
                    }
                };

                var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            },
            async atualizarStatusMedicao(codigoMedicao, event, tipoPonto) {

                let params = {
                    table: 'relatorioMedicao',
                    data: {
                        Status: event.target.value
                    },
                    where: {
                        CodigoRecursosHidricosRelatorioMedicao: codigoMedicao
                    }
                }
                await vmGlobal.updateFromAPI(params, null);
                await this.getAnaliseDados(this.pontoSelecionado.CodigoConfiguracaoRecursosHidricosPontoColeta, tipoPonto, vmApp.mesAtividade, vmApp.anoAtividade);
                this.mostrarSelectStatus = false;


            }
        }

    });
</script>