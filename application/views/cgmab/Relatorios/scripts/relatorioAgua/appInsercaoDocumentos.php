<script>
    vmAnexo = new Vue({
        el: '#anexoDocumentos',
        data() {
            return {
                mostrarAnexos: false,
                mostrarAddAnexo: false,
                listaAnexos: {},
                arquivo: '',
                file: ''
            }
        },
        methods: {
            async getAnexos() {
                let params = {
                    table: 'anexos',
                    where: {
                        CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade
                    }
                }
                this.listaAnexos = await vmGlobal.getFromAPI(params);
                this.mostrarAnexos = true;
            },
            carregarArquivo() {
                this.file = this.$refs.file.files[0];
            },
            async enviarArquivo() {
                let formData = new FormData();
                formData.append('file', this.file);
                let params = {
                    table: 'anexos',
                    data: {
                        CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                        Descricao: this.arquivo,
                        Data: new Date(),
                        Caminho: base_url + '/webroot/uploads/anexos/' + this.file.name
                    }
                }
                await vmGlobal.insertFromAPI(params, null);
                await axios.post(base_url + 'RelatorioMonitoramentoAgua/uploadAnexo', formData); //Upload
                this.mostrarAddAnexo = false;
                this.arquivo = '';
                this.file = '';
                this.getAnexos();
            },
            async deletarAnexo(codigoAnexo, caminho) {
                Swal.fire({
                    title: 'Tem Certeza que deseja deletar este anexo?',
                    text: "Você não poderá reverter esta ação!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim, tenho certeza!'
                }).then(async (result) => {
                    if (result.value) {
                        let params = {
                            table: 'anexos',
                            where: {
                                CodigoAnexo: codigoAnexo
                            }
                        }
                        await vmGlobal.deleteFromAPI(params);
                        await axios.post(base_url + 'RelatorioMonitoramentoAgua/deleteFile', $.param({
                            Caminho: caminho
                        }));
                        Swal.fire(
                            'Deletado!',
                            'Arquivo deletado com sucesso.',
                            'success'
                        );
                        this.getAnexos();
                    }
                })


            }
        }

    });
</script>