<script>
    var vmObservacoesTecnicas = new Vue({
        el: '#observacoes-tecnicas',
        data() {
            return {
                mostrarObs: false,
                listaObservacaoTecnica: {},
                codigoObservacaoTecnica: '',
                codigoAtividade: '',
                codigoPonto: '',
                dadosObservacao: '',
                i: {}
            }
        },
        mounted() {

        },
        methods: {
            async getObservacoesTecnicas(ponto, atividade, i) {
                this.i = i;
                CKEDITOR.instances['obsTec'].setData('');//limpar ckeditor para update
                this.codigoAtividade = atividade;
                this.codigoPonto = ponto;
                let params = {
                    table: 'relatorioObservacaoTecnica',
                    where: {
                        CodigoAtividadeCronogramaFisico: atividade,
                        CodigoConfiguracaoRecursosHidricosFotos: ponto
                    }
                }
                this.listaObservacaoTecnica = await vmGlobal.getFromAPI(params, 'cgmab');
                this.mostrarObs = true;

                if (this.listaObservacaoTecnica.length > 0) {
                    CKEDITOR.instances['obsTec'].setData(atob(this.listaObservacaoTecnica[0].Observacoes));
                }
            },
            async salvarObservacao() {
                this.dadosObservacao = btoa(CKEDITOR.instances['obsTec'].getData());

                let params;
                //se já existir o codigo da observação, atualizar registro
                if (this.listaObservacaoTecnica.length > 0) {
                    params = {
                        table: 'relatorioObservacaoTecnica',
                        data: {
                            CodigoAtividadeCronogramaFisico: this.codigoAtividade,
                            CodigoConfiguracaoRecursosHidricosFotos: this.ponto,
                            Observacoes: this.dadosObservacao,
                            DataCadastro: new Date(),
                            TipoObservacao: 'tecnica'
                        },
                        where: {
                            CodigoRelatorioObservacao: this.listaObservacaoTecnica[0].CodigoRelatorioObservacao
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null);

                } else {
                    console.log('insert');
                    params = {
                        table: 'relatorioObservacaoTecnica',
                        data: {
                            CodigoAtividadeCronogramaFisico: this.codigoAtividade,
                            CodigoConfiguracaoRecursosHidricosFotos: this.codigoPonto,
                            Observacoes: this.dadosObservacao
                        }
                    }
                    await vmGlobal.insertFromAPI(params, null);
                }


            }
        }
    });
</script>