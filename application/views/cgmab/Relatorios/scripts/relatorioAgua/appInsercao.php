<script>
    var vmInsercao = new Vue({
        el: '#insercaoMedicoes',
        data: {
            mostrarMedicoes: true,
            listaPontosColeta: [],
            mostrarPontos: false,
            pontoSelecionado: '',

        },
        mounted() {
            this.getPontos();
        },
        methods: {
            async getDatasRelatorio() {

            },
            async getPontos() {
                await axios.post(base_url + 'Relatorios/getPontos', $.param({
                        CodigoEscopoPrograma: vmApp.CodigoEscopo
                    }))
                    .then((resp) => {
                        this.listaPontosColeta = resp.data;
                    })
            },
            async iniciarMedicoes(CodigoPonto, atividade, modulo, i = null) {
                this.pontoSelecionado = CodigoPonto;
                this.mostrarMedicoes = false;
                if (modulo === 'observacao') {
                    await vmObservacoesTecnicas.getObservacoesTecnicas(this.pontoSelecionado, atividade, dadosCard = i);
                }
                if(modulo === 'medicao'){
                    await vmInsercaoMedicao.getParametrosConfiguracao(CodigoPonto, dadosCard = i);
                }
                if(modulo === 'anexo'){
                    await vmAnexo.getAnexos();
                }


            },
        }

    });

    setTimeout(() => {
                        CKEDITOR.replace('editorObs', {
                            height: '400px',
                        })
                    }, 1000);
</script>