<script>
    var vmObservacoesCampanha = new Vue({
        el: '#observacoesCampanha',
        data: {
            model: {},
            inicio: '',
            fim: '',
            listaObs: [],
            consultaInsert: false,

        },
        mounted() {
            // this.getPontos();
        },
        methods: {
            async getObservacoes() {
                let params = {
                    table: 'obsCampanha',
                    where: {
                        CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade
                    }
                }
                this.listaObs = await vmGlobal.getFromAPI(params);

                if (this.listaObs.length === 0) {
                    await this.criarObsCampanha();
                } else {
                    vmObservacoesCampanha.inicio = vmGlobal.frontEndDateFormat(this.listaObs[0].Inicio);
                    vmObservacoesCampanha.fim = vmGlobal.frontEndDateFormat(this.listaObs[0].Fim);
                    setTimeout(() => {
                        CKEDITOR.instances['textAreaObsCamp1'].setData(atob(this.listaObs[0].ObsGerais));
                        CKEDITOR.instances['textAreaObsCamp2'].setData(atob(this.listaObs[0].AreaInfluencia));
                        CKEDITOR.instances['textAreaObsCamp3'].setData(atob(this.listaObs[0].LocalPontoColeta));
                        CKEDITOR.instances['textAreaObsCamp4'].setData(atob(this.listaObs[0].ObsColetaPrevencaoAmostras));
                        CKEDITOR.instances['textAreaObsCamp5'].setData(atob(this.listaObs[0].AcondicionamentoTransporteAmostras));

                    }, 1500);

                }
            },
            async iniciarTextArea() {
                await this.$nextTick(() => {
                    if (CKEDITOR.instances.textAreaObsCamp1) CKEDITOR.instances.textAreaObsCamp1.destroy();
                    CKEDITOR.replace('textAreaObsCamp1', {
                        height: '400px',
                    });
                    if (CKEDITOR.instances.textAreaObsCamp2) CKEDITOR.instances.textAreaObsCamp2.destroy();
                    CKEDITOR.replace('textAreaObsCamp2', {
                        height: '400px',
                    });
                    if (CKEDITOR.instances.textAreaObsCamp3) CKEDITOR.instances.textAreaObsCamp3.destroy();
                    CKEDITOR.replace('textAreaObsCamp3', {
                        height: '400px',
                    });
                    if (CKEDITOR.instances.textAreaObsCamp4) CKEDITOR.instances.textAreaObsCamp4.destroy();
                    CKEDITOR.replace('textAreaObsCamp4', {
                        height: '400px',
                    });
                    if (CKEDITOR.instances.textAreaObsCamp5) CKEDITOR.instances.textAreaObsCamp5.destroy();
                    CKEDITOR.replace('textAreaObsCamp5', {
                        height: '400px',
                    });
                })
                await this.getObservacoes();
            },
            async savarDados(campo = null) {
                var params;
                if (campo === 'datas') {

                    params = {
                        table: 'obsCampanha',
                        data: {
                            Inicio: vmGlobal.backEndDateFormat(this.inicio),
                            Fim: vmGlobal.backEndDateFormat(this.fim)
                        },
                        where: {
                            CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null, true);
                } else {
                    params = {
                        table: 'obsCampanha',
                        data: {
                            CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                            Inicio: vmGlobal.backEndDateFormat(this.inicio),
                            Fim: vmGlobal.backEndDateFormat(this.fim),
                            ObsGerais: btoa(CKEDITOR.instances['textAreaObsCamp1'].getData()),
                            AreaInfluencia: btoa(CKEDITOR.instances['textAreaObsCamp2'].getData()),
                            LocalPontoColeta: btoa(CKEDITOR.instances['textAreaObsCamp3'].getData()),
                            ObsColetaPrevencaoAmostras: btoa(CKEDITOR.instances['textAreaObsCamp4'].getData()),
                            AcondicionamentoTransporteAmostras: btoa(CKEDITOR.instances['textAreaObsCamp5'].getData()),
                        },
                        where: {
                            CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null);
                }

                this.getObservacoes();
            },
            async criarObsCampanha() {
                let params = {
                    table: 'obsCampanha',
                    data: {
                        CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                        Inicio: new Date(),
                        Fim: new Date(),
                        ObsGerais: '<p><b>Preenchimento Obrigatório!</b></p>',
                        AreaInfluencia: '<p><b>Preenchimento Obrigatório!</b></p>',
                        LocalPontoColeta: '<p><b>Preenchimento Obrigatório!</b></p>',
                        ObsColetaPrevencaoAmostras: '<p><b>Preenchimento Obrigatório!</b></p>',
                        AcondicionamentoTransporteAmostras: '<p><b>Preenchimento Obrigatório!</b></p>',

                    }
                }
                await vmGlobal.insertFromAPI(params, null);
                await this.getObservacoes();
            }


        }

    });
</script>