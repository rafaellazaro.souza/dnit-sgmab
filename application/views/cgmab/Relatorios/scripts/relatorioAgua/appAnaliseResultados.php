<script>
  var vmAnaliseResultados = new Vue({
    el: '#analiseResultados',
    data() {
      return {
        CodigoAnaliseResultadoIQA: 0,
        CodigoConclusao: 0,
        CodigoBiblioGrafia: 0,
        listaResultadosIqa: [],
        listaResultadosIqa2: [
          ['Campanha', 'PT1 - Jusante', 'PT2-Montante'],
          ['01/2020', 60, 55],
          ['02/2020', 55, 50],
          ['03/2020', 48, 49],
          ['04/2020', 44, 47]
        ]
      }
    },
    methods: {
      async getAnaliseResultados() {
        await axios.post(base_url + 'RelatorioMonitoramentoAgua/getAnaliseResultadosPontos', $.param({
            CodigoAtividade: vmApp.DadosAtividade
          }))
          .then((resp) => {
            
            if (resp.data.AnaliseResultadoIQA == null) {
              this.salvarAnalise(true, 1);
            } else {
              this.CodigoAnaliseResultadoIQA = resp.data.AnaliseResultadoIQA.split("-")[1];
              setTimeout(() => {
                CKEDITOR.instances['txtAnaliseResultadosIQA'].setData(atob(resp.data.AnaliseResultadoIQA.split("-")[0]));
              }, 1000);
            }
            if (resp.data.ConclusoesIQA == null) {
              this.salvarAnalise(true, 2);
            } else {
              setTimeout(() => {
                this.CodigoConclusao = resp.data.ConclusoesIQA.split("-")[1];
                CKEDITOR.instances['txtConclusoesIQA'].setData(atob(resp.data.ConclusoesIQA.split("-")[0]));
              }, 1000);
            }
            if (resp.data.BibliografiaIQA == null) {
              this.salvarAnalise(true, 3);
            } else {
              setTimeout(() => {
                this.CodigoBiblioGrafia = resp.data.BibliografiaIQA.split("-")[1];
                CKEDITOR.instances['txtBibliografiaIQA'].setData(atob(resp.data.BibliografiaIQA.split("-")[0]));
              }, 1000);
            }
          })
      },
      async getMedicoesIqaGrafico() {
        await axios.post(base_url + 'RelatorioMonitoramentoAgua/getDadosGraficoIqa', $.param({
            CodigoAtividade: vmApp.DadosAtividade
          }))
          .then((resp) => {
            this.listaResultadosIqa = resp.data
          })
          .catch((e) => {
            console.log(e);
          })
          .finally(() => {
            var campanha = [];
            var resultados = [];
            this.listaResultadosIqa.forEach(function(i) {
              campanha.push(vmGlobal.frontEndDateFormat(i.DataAnterior), vmGlobal.frontEndDateFormat(i.DataMesAtividade));
              resultados.push({
                  name: i.CodigoConfiguracaoRecursosHidricosPontoColeta + ' - ' + i.TipoPontoAmostra,
                  data: [parseInt(i.ValorAnterior), parseInt(i.ValorAtual)]
                },
                // {
                //   name: i.CodigoConfiguracaoRecursosHidricosPontoColeta+' - '+ i.TipoPontoAmostra,
                //   data: parseInt(i.ValorAtual)
                // },
              );
            });
            this.geraGrafico(campanha, resultados);
          });
      },

      geraGrafico(campanha, resultados) {
        Highcharts.chart('graficoIQAResultados', {
          chart: {
            type: 'line'
          },
          title: {
            text: 'Analise de Resultados IQA'
          },
          credits: {
            enabled: false
          },
          subtitle: {
            text: ''
          },
          xAxis: {
            categories: campanha
          },
          yAxis: {
            title: {
              text: 'IQA'
            },
            min: 0,
            max: 100,
            tickInterval: 20
          },


          plotOptions: {
            line: {
              dataLabels: {
                enabled: true
              },
              enableMouseTracking: true
            }
          },
          series: resultados
          // series: [{
          //   name: 'PT150 - Jusante',
          //   data: [10,20]
          // }, {
          //   name: 'PT150 - Montante',
          //   data: [20,15]
          // }]
        });
      },




      // async iniciarMapa() {
      //   await google.charts.load('current', {
      //     'packages': ['corechart']
      //   });



      //   var options = {
      //     width: 1400,
      //     height: 600,
      //     title: 'Analise de Resultados IQA',
      //     vAxis: {
      //       minValue: 0,
      //       ticks: [0, 20, 40, 60, 80, 100]
      //     },
      //     legend: {
      //       position: 'bottom'
      //     }
      //   };

      //   var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

      //   chart.draw(google.visualization.arrayToDataTable(this.listaResultadosIqa), options);
      // },
      async iniciarTextArea() {
        await this.$nextTick(() => {
          if (CKEDITOR.instances.txtAnaliseResultadosIQA) CKEDITOR.instances.txtAnaliseResultadosIQA.destroy();
          CKEDITOR.replace('txtAnaliseResultadosIQA', {
            height: '400px',
          });
          if (CKEDITOR.instances.txtConclusoesIQA) CKEDITOR.instances.txtConclusoesIQA.destroy();
          CKEDITOR.replace('txtConclusoesIQA', {
            height: '400px',
          });
          if (CKEDITOR.instances.txtBibliografiaIQA) CKEDITOR.instances.txtBibliografiaIQA.destroy();
          CKEDITOR.replace('txtBibliografiaIQA', {
            height: '400px',
          });
        })
        await this.getAnaliseResultados();
        await this.getMedicoesIqaGrafico();
        await this.getMedicoesIqaGrafico();
      },
      async salvarAnalise(insert, campo) {
        var params;
        if (insert === true) { //insert
          if (campo === 1) {
            params = {
              table: 'analiseResutultadosParametrosAgua',
              data: {
                CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                AnaliseResultadoIQA: btoa('Digite a Análise aqui!'),
                DataCadastro: new Date()
              }
            }
          } else if (campo === 2) {
            params = {
              table: 'analiseResutultadosParametrosAgua',
              data: {
                CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                ConclusoesIQA: btoa('Digite a Conclusão aqui!'),
                DataCadastro: new Date()
              }
            }
          } else {
            params = {
              table: 'analiseResutultadosParametrosAgua',
              data: {
                CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                BibliografiaIQA: btoa('Digite a Bibliografia aqui!'),
                DataCadastro: new Date()
              }
            }
          }

          await vmGlobal.insertFromAPI(params, null, 'cgmab', false);
        } else { //update
          if (parseInt(campo) == 1) {
            params = {
              table: 'analiseResutultadosParametrosAgua',
              data: {
                CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                AnaliseResultadoIQA: btoa(CKEDITOR.instances['txtAnaliseResultadosIQA'].getData()),
                DataCadastro: new Date()
              },
              where: {
                CodigoAnaliseResultados: this.CodigoAnaliseResultadoIQA
              }
            }
          } else if (parseInt(campo) == 2) {
            params = {
              table: 'analiseResutultadosParametrosAgua',
              data: {
                CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                ConclusoesIQA: btoa(CKEDITOR.instances['txtConclusoesIQA'].getData()),
                DataCadastro: new Date()
              },
              where: {
                CodigoAnaliseResultados: this.CodigoConclusao
              }
            }
          } else {
            params = {
              table: 'analiseResutultadosParametrosAgua',
              data: {
                CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                BibliografiaIQA: btoa(CKEDITOR.instances['txtBibliografiaIQA'].getData()),
                DataCadastro: new Date()
              },
              where: {
                CodigoAnaliseResultados: this.CodigoBiblioGrafia
              }
            }
          }
          await vmGlobal.updateFromAPI(params, null);
        }

        await this.getAnaliseResultados();
      }

    }


  });
</script>