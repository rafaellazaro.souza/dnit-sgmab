<script>
    var vmInsercaoMedicao = new Vue({
        el: '#medicoes',
        data: {
            mostrarMedicoes: false,
            mostrarAddMedicao: false,
            mostrarListaParametros: true,
            mostrarInputAdd: false,
            mostrarInputUpdate: false,
            modelObservacaoTecnica: {},
            listaMedicoesAdicionadas: {},
            listaParametros: {},
            valorMedicao: '',
            pontoSelecionado: '',
            parametroSelecionado: '',
            nomeParametroSelecionado: '',
            i: {}
        },
        mounted() {


        },
        methods: {
            async getParametrosConfiguracao(ponto, i) {
                this.i = i;
                this.pontoSelecionado = ponto;
                this.mostrarMedicoes = true;
                await axios.post(base_url + 'RelatorioMonitoramentoAgua/getParametrosPonto', $.param({
                        CodigoPonto: ponto
                    }))
                    .then((resp) => {
                        this.listaParametros = resp.data;
                    })
                    .finally(() => {
                        this.getMedicoesPonto();
                    });
            },
            async getMedicoesPonto() {
                let params = {
                    table: 'relatorioMedicao',
                    joinLeft: [{
                        table1: 'relatorioMedicao',
                        table2: 'listaParametrosPrograma',
                        on: 'CodigoRecursosHidricosParametros',
                    }],
                    where: {
                        CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                        CodigoConfiguracaoRecursosHidricosFotos: this.pontoSelecionado
                    }
                }
                this.listaMedicoesAdicionadas = await vmGlobal.getFromAPI(params);

            },
            async salvarMedicao(codigo = false ,update = false, event) {
                var params;
                if (update) { //atualizar
                    params = {
                        table: 'relatorioMedicao',
                        data: {
                            Valor: event.target.value
                        },
                        where: {
                            CodigoRecursosHidricosRelatorioMedicao: codigo 
                        }
                    }
                    await vmGlobal.updateFromAPI(params, null);
                    this.mostrarInputUpdate = false;
                } else { //inserir
                    params = {
                        table: 'relatorioMedicao',
                        data: {
                            CodigoAtividadeCronogramaFisico: vmApp.DadosAtividade,
                            CodigoRecursosHidricosParametros: this.parametroSelecionado,
                            CodigoConfiguracaoRecursosHidricosFotos: this.pontoSelecionado,
                            DataCadastro: new Date(),
                            Valor: this.valorMedicao
                        }
                    }
                    await vmGlobal.insertFromAPI(params, null);
                }
                this.getMedicoesPonto();
                this.valorMedicao = '';
            },
            async deletarMedicao(codigo) {
                let params = {
                    table: 'relatorioMedicao',
                    where: {
                        CodigoRecursosHidricosRelatorioMedicao: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                this.getMedicoesPonto();
            }
        }

    });
   
</script>