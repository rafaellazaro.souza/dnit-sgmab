<script>
    var vmNavegacao = new Vue({
        el: '#navegacao',
        data() {
            return {

            }
        },
        async mounted() {
            this.iniciarIntroducao();
            CKEDITOR.replace('analiseResultadosDiscucoes', {
                height: '400px',
            });
            CKEDITOR.replace('analiseResultadosConclusoes', {
                height: '400px',
            });
            CKEDITOR.replace('analiseResultadosBibliografia', {
                height: '400px',
            });
            CKEDITOR.replace('analiseResultadosCorrelacao', {
                height: '400px',
            });
            CKEDITOR.replace('analiseResultadosCorrelacaoConclusao', {
                height: '400px',
            });
            // 
            setTimeout(async () => {
                await vmAppDiscussoes.getTextos('AnaliseResultadoDiscussao');
                await vmAppDiscussoes.getTextos('Conclusao');
                await vmAppDiscussoes.getTextos('Bibliografia');
                await vmAppCorrelacao.getTextos('AnaliseResultadoProgramaAtropelamento');
                await vmAppCorrelacao.getTextos('ConclusaoProgramaAtropelamento');
            }, 5000);

            await vmRelatorioAtividade.closeButtons();
        },
        methods: {
            iniciarIntroducao() {
                CKEDITOR.replace('editorIntro', {
                    height: '400px',
                });
                vmAppIntroducao.getIntro();
                vmAppIntroducao.getAnexos();
            },
            iniciarInsercao() {
                vmAppInsercao.getAnimaisCaputrados();
                vmAppInsercao.getFotosAnimaisTemp();
                vmAppInsercao.getPassagens();
            },
            iniciarAnaliseGrupo() {
                CKEDITOR.replace('analiseResultadosFauna', {
                    height: '400px',
                });
                vmAppAnaliseGrupo.getGruposFaunisticos();
                vmAppAnaliseGrupo.getAnaliseResultadoGrupoFaunistico('AnaliseResultadoGrupoFaunistico');
            },
            iniciarProfissionais() {
                vmAppProfissionais.getRecursosServico('profisionais');
            },
            iniciarDiscucoesConclusoes() {


            },
            iniciarCorrelacao() {


            },


        }
    });
</script>