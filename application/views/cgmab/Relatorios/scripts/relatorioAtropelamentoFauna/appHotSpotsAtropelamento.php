<script>
	var vmHotSpotsAtropelamento = new Vue({
		el: '#hotSpotsAtropelamento',
		data() {
			return {
				codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),
				update: false,
				dadosAtropelamentoObsercacoes: {
					GraficoHotSpots: null,
					HotSpots: ''
				},
				fotoGrafico: {},

				coordenadasShapeFile: {},
				mymap: null,
				layer: null,
			}
		},
		async mounted() {
			// this.getDadosAropelamentoObservacoes();
			// this.gerarCkeditor();
			// this.gerarMapa();
		},

		methods: {
			gerarCkeditor() {
				if (CKEDITOR.instances.txtHotSpots) CKEDITOR.instances.txtHotSpots.destroy();
				CKEDITOR.replace('txtHotSpots', {
					height: '400px'
				});
			},

			gerarMapa() {
				if (this.mymap) this.mymap.remove();
				const style = 'reduced.night';
				this.mymap = L.map('mapid').setView(['-16.7646342', '-61.3179296'], 5.34);
				L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
					maxZoom: 15,
					// attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
					//     '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
					id: 'mapbox.streets',
					fadeAnimation: true,
				}).addTo(this.mymap);
				this.layer = L.geoJSON().addTo(this.mymap);
			},

			aplicarLayer(coord, objectid, color, layer) {
				var array = {
					"type": "FeatureCollection",
					"features": [{
						"type": "Feature",
						"geometry": JSON.parse(coord),
						"properties": {
							"OBJECTID": objectid,

						}
					}]
				}
				L.geoJson(array, {
					style: function(feature) {
						return {
							stroke: true,
							color: color,
							weight: 1
						};
					},
					onEachFeature: function(feature, l) {
						l.bindPopup(feature.properties.OBJECTID);

						l.on('click', function(e) {
							l.setStyle({
								weight: 12,
								outline: 'red'
							});
						});

						l.on("popupclose", function(e) {
							l.setStyle({
								weight: 1,
							});
						});
					}
				}).addTo(layer);
			},

			async getDadosAropelamentoObservacoes() {
				//Traz os registros armazenados no banco de dados
				var params = {
					table: 'atropelamentoAnalises',
					where: {
						CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
					}
				};
				var dadosBusca = await vmGlobal.getFromAPI(params, 'cgmab');
				this.dadosAtropelamentoObsercacoes = (dadosBusca.length != 0) ? dadosBusca[0] : {};
				if (this.dadosAtropelamentoObsercacoes.HotSpots) {
					CKEDITOR.instances['txtHotSpots'].setData(String(atob(this.dadosAtropelamentoObsercacoes.HotSpots)));
					this.update = true;
				} else {
					this.update = false
				}

				if (this.dadosAtropelamentoObsercacoes.GraficoHotSpots != null) {
					this.getDadosMapa();
                    this.update = true;
				}

				await this.$nextTick(async () => {
					await vmRelatorioAtividade.closeButtons();
				})
			},

			getDadosMapa() {
				this.layer.clearLayers();
				var data = this.dadosAtropelamentoObsercacoes.GraficoHotSpots;
				this.coordenadasShapeFile = JSON.parse(data);
				const concatenado = '<div>Dados do ShapeFile</div>';
				const corLayer = '#000';
				for (let index = 0; index < this.coordenadasShapeFile.length; index++) {
					var cd = this.coordenadasShapeFile[index].coordenada
					vmHotSpotsAtropelamento.aplicarLayer(cd, concatenado, corLayer, this.layer);
				}
			},

			async verificaFotoHotSpot() {
				//Verifica o formato da fotografia e prepara o arquivo 'file' para popular no banco de dados
				let file = null;

				if (this.$refs.Foto != null && this.$refs.Foto != undefined) {
					this.fotoGrafico.CodigoAtividadeCronogramaFisico = this.codigoAtividadeCronogramaFisico;
					this.fotoGrafico.DataCadastro = new Date();
					this.fotoGrafico.FilesRules = {
						upload_path: 'webroot/uploads/relatorio-atropelamento-fauna/hot-spots',
						allowed_types: 'zip',
					};
					file = {
						name: 'GraficoHotSpots',
						value: this.$refs.Foto.files[0]
					}
				}

				await this.addShapefile(file);
			},

			async addShapefile() {
				$('.spinnerShapefile').addClass('spinner-border spinner-border-sm');
				var myfiles = document.getElementById("ShapeFile");
				var files = myfiles.files;
				var data = new FormData();
				var file = this.$refs.ShapeFile.files[0];


				data.append('CodigoAtividadeCronogramaFisico', parseInt(this.codigoAtividadeCronogramaFisico));
				data.append('GraficoHotSpots', file);
				this.layer.clearLayers();

				$.ajax({
					url: '<?= base_url('Relatorios/postShapefileAtropelamento') ?>',
					method: 'POST',
					data: data,
					contentType: false,
					processData: false,
					success: function(data) {
						vmHotSpotsAtropelamento.coordenadasShapeFile = JSON.parse(data);
						const numCoordenadas = vmHotSpotsAtropelamento.coordenadasShapeFile.length
						const concatenado = 0;
						const corLayer = '#000';
						for (let index = 0; index < numCoordenadas; index++) {
							var cd = vmHotSpotsAtropelamento.coordenadasShapeFile[index].coordenada
							vmHotSpotsAtropelamento.aplicarLayer(cd, concatenado, corLayer, vmHotSpotsAtropelamento.layer);
						}
					},
					error: function(error) {
						console.log(error);
						Swal.fire({
							position: 'center',
							type: 'error',
							title: 'Erro!',
							text: "Erro ao tentar salvar arquivo. " + e,
							showConfirmButton: true,

						});
					},
					complete: function() {

					}
				})
			},

			async salvarShapefile() {
				$('.spinnerShapefile').addClass('spinner-border spinner-border-sm');

				if (this.update === false) {
					let params = {
						table: 'atropelamentoAnalises',
						data: {
							GraficoHotSpots: JSON.stringify(this.coordenadasShapeFile),
                            CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
						}
					}
					await vmGlobal.insertFromAPI(params, null, 'cgmab');
				} else {
					let params = {
						table: 'atropelamentoAnalises',
						data: {
							GraficoHotSpots: JSON.stringify(this.coordenadasShapeFile)
						},
						where: {
							CodigoRelatorioAnalises: this.dadosAtropelamentoObsercacoes.CodigoRelatorioAnalises,
						}
					}
					await vmGlobal.updateFromAPI(params, null);
				}
				await this.getDadosAropelamentoObservacoes();

			},

			deletarCoordenadasShapeFile() {
				this.coordenadasShapeFile = {};
				this.layer.clearLayers();

				// Limpa a entrada do arquivo personalizado para o input file
				$('.spinnerShapefile').removeClass('spinner-border spinner-border-sm');
				$("#ShapeFile").val(null);
				$("#labelShapeFile").html('Selecione um arquivo ZIP');
			},

			async deletarShapefile() {

				//Apaga o registro da fotografia do banco de dados e o arquivo no projeto
				let params = {
					table: 'atropelamentoAnalises',
					data: {
						GraficoHotSpots: null,
					},
					where: {
						CodigoRelatorioAnalises: this.dadosAtropelamentoObsercacoes.CodigoRelatorioAnalises,
					}
				}
				await vmGlobal.updateFromAPI(params, null);

				// Limpa a entrada do arquivo personalizado para o input file
				$('.spinnerShapefile').removeClass('spinner-border spinner-border-sm');
				$("#ShapeFile").val(null);
				$("#labelShapeFile").html('Selecione um arquivo ZIP');

				this.coordenadasShapeFile = {};
				this.layer.clearLayers();
				await this.getDadosAropelamentoObservacoes();

			},

			async salvarAnaliseHotSpots() {
				//Popular a informacao dos quadros de texto
				let descricaoTexto = String(btoa(CKEDITOR.instances['txtHotSpots'].getData()));
				if (this.update === false) {
					var params = {
						table: 'atropelamentoAnalises',
						data: {
							HotSpots: descricaoTexto,
							CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
						},
					}
					await vmGlobal.insertFromAPI(params, null, 'cgmab');
					await this.getDadosAropelamentoObservacoes();
				} else {
					let codigoRelatorio = parseInt(this.dadosAtropelamentoObsercacoes.CodigoRelatorioAnalises);
					var params = {
						table: 'atropelamentoAnalises',
						data: {
							HotSpots: descricaoTexto
						},
						where: {
							CodigoRelatorioAnalises: codigoRelatorio
						}
					}
					await vmGlobal.updateFromAPI(params, null, 'cgmab');
					await this.getDadosAropelamentoObservacoes();
				}
			},

		},
	})
</script>
