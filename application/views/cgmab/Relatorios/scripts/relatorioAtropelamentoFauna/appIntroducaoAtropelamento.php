<script>
	var vmIntroducaoAtropelamento = new Vue({
		el: '#introducaoAtropelamento',
		data() {
			return {
				codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),
				dadosFormulario: {
					Introducao: ''
				},
				update: false,
				totalDocumentos: 0,
				listaFaunaDocumentos: 0,
			}
		},
		async mounted() {
			await this.iniciaEditores();
			await this.getListaDocumentos();
			await this.getDadosDescricao();
			await vmRelatorioAtividade.closeButtons();
		},
		methods: {
			async iniciaEditores() {
				await this.$nextTick(() => {
					CKEDITOR.replace('Introducao', {
						height: '400px'
					});
				})
			},
			async getDadosDescricao() {
				var params = {
					table: 'atropelamentoObservacoes',
					where: {
						CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
					}
				};
				var dadosBusca = await vmGlobal.getFromAPI(params, 'cgmab');
				if (dadosBusca.length != 0) {
					this.dadosFormulario = dadosBusca[0];
					await this.$nextTick(() => {
						CKEDITOR.instances.Introducao.setData(String(atob(this.dadosFormulario.Introducao)));
					})
					this.update = true
				} else {
					// CKEDITOR.instances['Introducao'].setData('');
					this.update = false
				}
			},

			async salvarIntroducao() {
				var data = new FormData();
				data.append('Introducao', this.dadosFormulario.Introducao);

				if (this.update === false) {
					var params = {
						table: 'atropelamentoObservacoes',
						data: {
							Introducao: String(btoa(CKEDITOR.instances.Introducao.getData())),
							CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
						},
					}
					await vmGlobal.insertFromAPI(params, null, 'cgmab');
					await this.getDadosDescricao();
				} else {
					var CodigoObservacoes = parseInt(this.dadosFormulario.CodigoAtropelamentoFaunaObservacoes);
					var params = {
						table: 'atropelamentoObservacoes',
						data: {
							Introducao: String(btoa(CKEDITOR.instances.Introducao.getData()))
						},
						where: {
							CodigoAtropelamentoFaunaObservacoes: CodigoObservacoes
						}
					}
					await vmGlobal.updateFromAPI(params, null, 'cgmab');
					await this.getDadosDescricao();
				}
			},

			async getListaDocumentos() {
				$("#listadeDocumentos").DataTable().destroy();
				const params = {
					table: 'atropelamentoAnexos',
					where: {
						Ativo: 1
					}
				}
				this.listaFaunaDocumentos = await vmGlobal.getFromAPI(params, 'cgmab');
				vmGlobal.montaDatatable('#listadeDocumentos', true);
			},

			qtdDocumentos() {
				var myfiles = document.getElementById("documentosMaterial");
				this.totalDocumentos = myfiles.files.length;
			},

			async addDocumentos() {
				var myfiles = document.getElementById("documentosMaterial");
				var files = myfiles.files;
				var data = new FormData();

				for (i = 0; i < files.length; i++) {
					data.append(i, files[i]);
				}

				data.append('codigoAtividade', parseInt(this.codigoAtividadeCronogramaFisico));

				$.ajax({
					url: '<?= base_url('Relatorios/postAtropelamentoFaunaDocumentos') ?>',
					method: 'POST',
					data: data,
					contentType: false,
					processData: false,
					success: function(data) {
						Swal.fire({
							position: 'center',
							type: 'success',
							title: 'Salvo!',
							text: 'Fotos salvas com sucesso.',
							showConfirmButton: true,
						});
						document.getElementById('documentosMaterial').value = null;
					},
					error: function(error) {
						console.log(error);
						Swal.fire({
							position: 'center',
							type: 'error',
							title: 'Erro!',
							text: "Erro ao tentar salvar arquivo. " + e,
							showConfirmButton: true,

						});
					},
					complete: function() {
						vmIntroducaoAtropelamento.getListaDocumentos();
						vmIntroducaoAtropelamento.totalDocumentos = 0
						$('.dropify-clear').click();
					}
				});
			},

			async deletar(codigo, index, caminho) {
				params = {
					table: 'atropelamentoAnexos',
					where: {
						CodigoAnaliseAnexos: codigo
					}
				}
				await vmGlobal.deleteFromAPI(params);
				await vmGlobal.deleteFile(caminho);
				await this.getListaDocumentos();
			},
		},
	})

	$(function() {
		"use strict";
		$('.dropify').dropify({
			messages: {
				default: "Solte ou clique para anexar arquivo."
			}
		});
	});
</script>