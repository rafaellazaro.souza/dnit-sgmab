<script>
	Vue.component("conclusoes-atropelamento-comp", {
		template: "#conclusoesAtropelamentoComp",
		props: {
			names: String,
		},
		data() {
			return {
				codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),
				update: false,
				dadosDiscussoesConclusoes: {},
			}
		},
		async mounted() {
			this.gerarCkeditor();
			await this.getDadosDiscussoesConclusoes();
		},
		watch: {

		},
		methods: {
			gerarCkeditor() {
				if (CKEDITOR.instances[this.names]) CKEDITOR.instances[this.names].destroy();
				CKEDITOR.replace(this.names, {
					height: '400px'
				});
			},

			async getDadosDiscussoesConclusoes() {
				//Traz os registros armazenados no banco de dados
				var params = {
					table: 'atropelamentoObservacoes',
					where: {
						CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
					}
				};
				var dadosBusca = await vmGlobal.getFromAPI(params, 'cgmab');
				if (dadosBusca.length != 0) this.dadosDiscussoesConclusoes = dadosBusca[0];
				if (dadosBusca.length != 0) {
					this.update = true;
					var texto = (this.dadosDiscussoesConclusoes[this.names]) ? String(atob(this.dadosDiscussoesConclusoes[this.names])) : '';
					CKEDITOR.instances[this.names].setData(texto);
					return;
				}
				this.update = false;
			},

			async salvaDiscussoesConclusoes(tipoObservacao) {
				//Popular a informacao dos quadros de texto
				let descricaoTexto = String(btoa(CKEDITOR.instances[this.names].getData()));
				await this.getDadosDiscussoesConclusoes();
				var texto = (this.dadosDiscussoesConclusoes[this.names]) ? String(atob(this.dadosDiscussoesConclusoes[this.names])) : '';
				CKEDITOR.instances[this.names].setData(texto);

				if (this.update === false) {
					var params = {
						table: 'atropelamentoObservacoes',
						data: {
							[tipoObservacao]: descricaoTexto,
							CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico
						},
					}
					await vmGlobal.insertFromAPI(params, null, 'cgmab');
					await this.getDadosDiscussoesConclusoes();
				} else {
					let CodigoObservacoes = parseInt(this.dadosDiscussoesConclusoes.CodigoAtropelamentoFaunaObservacoes);
					var params = {
						table: 'atropelamentoObservacoes',
						data: {
							[tipoObservacao]: descricaoTexto
						},
						where: {
							CodigoAtropelamentoFaunaObservacoes: CodigoObservacoes
						}
					}
					await vmGlobal.updateFromAPI(params, null, 'cgmab');
					await this.getDadosDiscussoesConclusoes();
				}
			},
		},
	})
</script>