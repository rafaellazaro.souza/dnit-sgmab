<script>
	Vue.component("analise-atropelamento-comp", {
		template: "#analiseAtropelamentoComp",
		props: {
			nameFoto: String,
			nameAnalise: String,
		},
		data() {
			return {
				codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1)
					.split('&')[2].split('=')[1], 'decode'),
				fotoGrafico: {},
				informacaoRelatorioAnalises: {},
			}
		},
		async mounted() {
			await this.gerar();
			await this.getInformacaoRelatorioAnalises();
		},

		methods: {
			async gerar() {
				if (CKEDITOR.instances[this.nameAnalise]) CKEDITOR.instances[this.nameAnalise].destroy();
				this.$nextTick(() => {
					CKEDITOR.replace(this.nameAnalise, {
						height: '200px'
					});
				})
			},

			async getInformacaoRelatorioAnalises() {
				//Traz os registros armazenados no banco de dados		
				let params = {
					table: 'atropelamentoAnalises',
					where: {
						CodigoAtividadeCronogramaFisico: this.codigoAtividadeCronogramaFisico,
						Ativo: 1
					}
				}
				let informacoesArtropelamentoAnalise = await vmGlobal.getFromAPI(params, null);
				this.informacaoRelatorioAnalises = (informacoesArtropelamentoAnalise.length != 0) ? informacoesArtropelamentoAnalise[0] : {};
				if (this.informacaoRelatorioAnalises.hasOwnProperty(this.nameAnalise)) {
					var texto = (this.informacaoRelatorioAnalises[this.nameAnalise]) ? this.informacaoRelatorioAnalises[this.nameAnalise] : '';
					await this.$nextTick(() => {
						if(this.informacaoRelatorioAnalises[this.nameAnalise]) {
							CKEDITOR.instances[this.nameAnalise].setData(String(atob(this.informacaoRelatorioAnalises[this.nameAnalise])));
						}
					})
				}
			},

			async verificaFotoRelatorioGF(tipografico) {
				//Verifica o formato da fotografia e prepara o arquivo 'file' para popular no banco de dados
				let file = null;
				if (this.$refs.Foto != null && this.$refs.Foto != undefined) {
					this.fotoGrafico.CodigoAtividadeCronogramaFisico = this.codigoAtividadeCronogramaFisico;
					this.fotoGrafico.DataCadastro = new Date();
					this.fotoGrafico.FilesRules = {
						upload_path: 'webroot/uploads/relatorio-atropelamento-fauna/fotos-analises',
						allowed_types: 'jpg|jpeg|png|gif',
					};
					file = {
						name: tipografico,
						value: this.$refs.Foto.files[0]
					}
				}
				await this.salvarFotoRelatorioGF(file);
			},

			async salvarFotoRelatorioGF(file) {
				//Salva a fotografia do registro no banco de dados
				await this.getInformacaoRelatorioAnalises();

				if (this.informacaoRelatorioAnalises) {
					let params = {
						table: 'atropelamentoAnalises',
						data: this.fotoGrafico,
						where: {
							CodigoRelatorioAnalises: this.informacaoRelatorioAnalises.CodigoRelatorioAnalises
						}
					}
					await vmGlobal.updateFromAPI(params, file, 'cgmab');
					await this.getInformacaoRelatorioAnalises();
				} else {
					let params = {
						table: 'atropelamentoAnalises',
						data: this.fotoGrafico,
					}
					await vmGlobal.insertFromAPI(params, file, 'cgmab');
					await this.getInformacaoRelatorioAnalises();
				}
			},

			async deletarFoto(caminhoFoto, foto) {

				//Apaga o registro da fotografia do banco de dados e o arquivo no projeto       
				let params = {
					table: 'atropelamentoAnalises',
					data: {
						[foto]: null,
					},
					where: {
						CodigoRelatorioAnalises: this.informacaoRelatorioAnalises.CodigoRelatorioAnalises,
					}
				}
				await vmGlobal.updateFromAPI(params, null);
				await vmGlobal.deleteFile(caminhoFoto);
				await this.getInformacaoRelatorioAnalises();
			},

			async salvarAnaliseTextoGF(tipoanalise) {
				//Popular a informacao dos quadros de texto
				let descricaoTexto = String(btoa(CKEDITOR.instances[tipoanalise].getData()));

				var params = {
					table: 'atropelamentoAnalises',
					data: {
						[tipoanalise]: descricaoTexto
					},
					where: {
						CodigoRelatorioAnalises: this.informacaoRelatorioAnalises.CodigoRelatorioAnalises,
					}
				}
				await vmGlobal.updateFromAPI(params, null, 'cgmab');
				await this.getInformacaoRelatorioAnalises();
			},
		},

	})
</script>
