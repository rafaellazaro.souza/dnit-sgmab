<script>
	var vmAtropelamentoInsercaoDados = new Vue({
		el: "#atropelamentoInsercaoDados",
		data() {
			return {
				codigoEscopoPrograma: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[0].split('=')[1], 'decode'),
				codigoAtividadeCronogramaFisico: vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[2].split('=')[1], 'decode'),

				filtroSelecionado: "",
				listaAnimas: {},
				listaAtropelamentoFauna: {},
				listaAtropelamentoFaunaFiltro: 0,
				listaExcel: {},
				file: [],
				totalFotos: 0,
				mostrarListaExcel: false,
				errorsAddCondicionante: [],
				pontosConfiguracaoFauna: {},
			}
		},
		watch: {
			filtroSelecionado: function(val) {
				this.getListaAtropelamentoFauna();
			}
		},

		async mounted() {
			await this.getListaAtropelamentoFauna();
			await vmRelatorioAtividade.closeButtons();
		},

		filters: {
			singleName(valor) {
				return valor.replace(/.\w+$/, "");
			}
		},

		methods: {
			async getListaAtropelamentoFauna() {
				let params, where;
				if (this.filtroSelecionado === 'Sem-foto') {
					this.listaAtropelamentoFauna = await vmGlobal.getFromController('Relatorios/getListaAtropelamentoFaunaSemFoto?codigo=' + this.codigoEscopoPrograma)
				} else {
					if (this.filtroSelecionado != "") {
						where = {
							Grupo: this.filtroSelecionado
						}
					} else {
						where = {
							CodigoEscopoPrograma: this.codigoEscopoPrograma
						}
					}

					params = {
						table: 'atropelamentoAnimais',
						where: where
					}

					//lista pontos
					this.listaAtropelamentoFauna = await vmGlobal.getFromAPI(params, 'cgmab');
				}

				// lista para filtro dos pontos
				if (this.listaAtropelamentoFaunaFiltro === 0) {
					this.listaAtropelamentoFauna.push({
						Grupo: 'Sem-foto'
					});
					this.listaAtropelamentoFaunaFiltro = this.listaAtropelamentoFauna.map(x => x
						.Grupo).filter(function(elem, index, self) {
						return index === self.indexOf(elem);
					});
				}

				for (let i = 0; i < this.listaAtropelamentoFauna.length; i++) {
					this.listaAtropelamentoFauna[i].modelDadosFotosInformacao = []
					this.errorsAddCondicionante[i] = []
				}
			},

			async getConfiguracaoMonitoramentoFauna() {
				//lista a Configuração da Armadilhas para o Monitoramento de Fauna
				let params = {
					table: 'escopoDeRecursos',
					join: {
						table: 'configFaunaFotos',
						on: 'CodigoRecurso'
					},
					where: {
						CodigoEscopoPrograma: this.codigoEscopoPrograma
					}
				}
				this.pontosConfiguracaoFauna = await vmGlobal.getFromAPI(params, 'cgmab');
			},

			async salvarDadosInsercaoFauna() {
				this.listaExcel.CodigoEscopoPrograma = this.codigoEscopoPrograma;
				var formData = new FormData();
				formData.append('Dados', JSON.stringify(this.listaExcel));
				formData.append('CodigoEscopoPrograma', this.codigoEscopoPrograma);
				await axios.post(base_url + 'Relatorios/salvarAtropelamentoFaunaDadosInsercao', formData)
					.then((resp) => {
						console.log(resp.data);
						if (resp.data.status === true) {
							Swal.fire({
								position: 'center',
								type: 'success',
								title: 'Salvo!',
								text: 'Dados inseridos com sucesso.',
								showConfirmButton: true,
							});
						} else {
							Swal.fire({
								position: 'center',
								type: 'error',
								title: 'Erro!',
								text: "Erro ao tentar salvar arquivo. " + e,
								showConfirmButton: true,
							});
						}
					});
				this.mostrarListaExcel = false;
				this.getListaAtropelamentoFauna();
				$('.dropify-clear').click();
				$('.dropify-wrapper').removeClass('invisivel');
				$('.ls-toggle-menu').removeClass('modal-open');
				$('.theme-blue').removeClass('modal-open');
				$('.modal').hide();
				$('.modal-backdrop').remove();
			},

			deletarRegistros(codigo) {
				//Deletar Registros
				Swal.fire({
					title: 'Você tem certeza?',
					text: "Você não poderá reverter isso!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim, excluir!'
				}).then((result) => {
					if (result.value) {
						let fd = new FormData();
						fd.append('Codigo', codigo)
						var txt;

						axios.post('<?= base_url('Relatorios/deletarRegistroAtropelamentoFauna') ?>', fd)
							.then(response => {
								vmAtropelamentoInsercaoDados.getListaAtropelamentoFauna();
								Swal.fire(
									'Excluído!',
									'Sua fotografia foi excluída com sucesso.',
									'success'
								)
							}).catch(function(error) {
								console.log(error)
							}).finally(() => {})
					}
				})
			},

			deletarFicha(codigo, caminho) {
				let params = {
					table: 'atropelamentoAnimais',
					data: {
						NomeFichaAtropelamento: null,
						CaminhoFicha: null,
					},
					where: {
						CodigoAtropelamentoAnimais: codigo
					}
				}

				//Exclui a ficha Atropelamento
				Swal.fire({
					title: 'Você tem certeza?',
					text: "Você não poderá reverter isso!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim, excluir!'
				}).then(async (result) => {
					if (result.value) {
						await vmGlobal.updateFromAPI(params, null, 'cgmab');
						vmGlobal.deleteFile(caminho);
						vmAtropelamentoInsercaoDados.getListaAtropelamentoFauna();
					}
				})
			},

			deletarFotos(codigo, caminho, caminhoFicha) {
				let params = {
					table: 'atropelamentoAnimais',
					data: {
						CaminhoFoto: null,
						NomeFoto: null,
						Lat: null,
						Long: null,
						DataFoto: null,
						ObservacaoFoto: null,
						NomeFichaAtropelamento: null,
						CaminhoFicha: null
					},
					where: {
						CodigoAtropelamentoAnimais: codigo
					}
				}

				//Exclui as fotografias
				Swal.fire({
					title: 'Você tem certeza?',
					text: "Você não poderá reverter isso!",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim, excluir!'
				}).then(async (result) => {
					if (result.value) {
						await vmGlobal.updateFromAPI(params, null, 'cgmab');
						vmGlobal.deleteFile(caminho);
						vmGlobal.deleteFile(caminhoFicha);
						vmAtropelamentoInsercaoDados.getListaAtropelamentoFauna();
					}
				})
			},

			qtdFotos() {
				//veridica a quantidade das fotografias para ser importado
				var myfiles = document.getElementById("imagemMaterial");
				this.totalFotos = myfiles.files.length;
			},

			verificaInformacoesAtropelamento(i, codigo) {
				// Verifica se a ficha foi importado 
				$("#error-" + i).html('').stop()
				var salvarinformacoes = true;
				var html = ''
				if (this.listaAtropelamentoFauna[i].modelDadosFotosInformacao['FichaAtropelameno'] == undefined) {
					html += '<div class="alert alert-warning p-3 w-100 " >' +
						'<h5>Erro ao salvar. Por favor Preencha os campos abaixo:</h5>'
					'<ul>'
					salvarinformacoes = false;
					html += '<li>A Ficha de Atropelamento modelo do IBAMA é obrigatório.</li>';
					if (html != '') {
						html += '</ul>' +
							'</div>'
					}
				}
				$("#error-" + i).html(html)

				this.salvarInformacoesAtropelamento(i, codigo);
			},

			salvarInformacoesAtropelamento(i, codigo) {
				//Salva as informções dos cards
				if ($("#" + i + 'fichaAtropelamento').length > 0) {
					var inputFile = document.getElementById(i + 'fichaAtropelamento').files[0];
					var fd = new FormData();
					fd.append('files', inputFile);
					fd.append('CodigoAtropelamentoAnimais', codigo);
					if (this.listaAtropelamentoFauna[i].modelDadosFotosInformacao['ObservacaoFoto'] != undefined) {
						fd.append('ObservacaoFoto', this.listaAtropelamentoFauna[i].modelDadosFotosInformacao['ObservacaoFoto']);
					}
					axios.post(base_url + 'Relatorios/fichaAtropelamento', fd).then((resp => {
						vmAtropelamentoInsercaoDados.getListaAtropelamentoFauna();
					}))
				}
			},

			async verifcaFotos() {
				//Adiciona as fotografias
				var myfiles = document.getElementById("imagemMaterial");
				var filesFotos = myfiles.files;
				var dataFoto = new FormData();

				//Listo os IDs do Registros do banco de dados
				let arrayIDs = [];
				this.listaAtropelamentoFauna.forEach((valor, index) => arrayIDs.push(valor.CodigoAtropelamentoAnimais));

				let strError = "";
				let numArray = 0;

				for (i = 0; i < filesFotos.length; i++) {
					//Separa o nome da foto 
					let nameFile = filesFotos[i].name.split("_");

					//Verifica se o ID existe no banco de dados
					let fotoID = filesFotos[i].name.split("_")[0];
					const findID = arrayIDs.includes(fotoID);

					//Verifica se a foto existe no banco de dados
					let valorFotografia = this.listaAtropelamentoFauna.filter(valor => valor.CodigoAtropelamentoAnimais == fotoID);
					let caminhoFotografia = '';

					if (valorFotografia.length > 0) {
						caminhoFotografia = valorFotografia[0].CaminhoFoto
					} else {
						caminhoFotografia = null
					}

					//Seleciona as fotos com erros
					if (nameFile.length != 2 || !findID || caminhoFotografia != null) {
						strError += filesFotos[i].name + "<br>";
					} else {
						dataFoto.append(numArray, filesFotos[i]);
						numArray += 1;
					}
				}
				if (strError != "") {
					await Swal.fire({
						type: 'error',
						title: 'Nome da Imagem está fora dos padrões',
						text: 'Por favor Corrigir!',
						html: 'Por favor Corrigir! </br>' + '</br>' +
							strError,
					})
				}
				await vmAtropelamentoInsercaoDados.addFotos(dataFoto);
			},

			addFotos(dataFoto) {
				$.ajax({
					url: '<?= base_url('Relatorios/getGpsFotosAtropelamentoFauna') ?>',
					method: 'POST',
					data: dataFoto,
					contentType: false,
					processData: false,
					success: function(data) {
						Swal.fire({
							position: 'center',
							type: 'success',
							title: 'Salvo!',
							text: 'Fotos salvas com sucesso.',
							timer: 1500,
							showConfirmButton: true,
						});
						document.getElementById('imagemMaterial').value = null;
					},
					error: function(error) {
						console.log(error);
						Swal.fire({
							position: 'center',
							type: 'error',
							title: 'Erro!',
							text: "Erro ao tentar salvar arquivo. " + e,
							showConfirmButton: true,
						});
					},
					complete: function() {
						vmAtropelamentoInsercaoDados.getListaAtropelamentoFauna();
						vmAtropelamentoInsercaoDados.filtroSelecionado = '';
						vmAtropelamentoInsercaoDados.totalFotos = 0
						$('.dropify-clear').click();
					}
				});
			},

			async importarDadosInsercao() {
				//upload do arquivo excel
				this.file = this.$refs.file.files[0];

				let formData = new FormData();
				formData.append('file', this.file);

				await axios.post('<?= base_url('API/uploadXLS') ?>',
						formData, {
							headers: {
								'Content-Type': 'multipart/form-data'
							}
						})
					.then((response) => {
						this.listaExcel = response.data
					})
					.catch((error) => {
						console.log(error);
					})
					.finally(() => {
						this.mostrarListaExcel = true;
						$(".dropify-wrapper").addClass('invisivel');
					});
			},

		},
	})

	$(function() {
		"use strict";
		$('.dropify').dropify({
			messages: {
				default: "Solte ou clique para anexar arquivo."
			}
		});
	});

	$(document).on('change', '.custom-file-input', function(event) {
		$(this).next('.custom-file-label').html(event.target.files[0].name);
	})
</script>