<style>
.no-shadow {
    box-shadow: 0px 0px 0px transparent !important;
}

.input-group-text {
        background-color: #afafaf;
        color: #f0f0f0;
    }

.form-header {
    background-color: #dedede;
}


/* Tela Incercao de Dados */
.borderRed {
    border: 1px solid red;
}

.borderGreen {
    border: 1px solid green;
}

.nav-tabs>.nav-item>.nav-link {
    border: 1px solid #dfdfdf !important;
    border-radius: 0px;
}

.nav-tabs>.nav-item>.nav-link.active {
    border: 1px solid #dfdfdf !important;
    border-radius: 0px;
    background-color: #e2e2e2;
    color: #6d6d6d !important;
    font-weight: bold;
}

th {
    text-align:center ;
}

option{
    text-align: right;
}
</style>


