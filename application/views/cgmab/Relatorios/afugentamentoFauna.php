<?php $this->load->view('cgmab/Relatorios/css/cssRelatoriosFauna.php') ?>

<div class="body_scroll">
	<div class="block-header">
		<div class="row">
			<div class="col-lg-7 col-md-6 col-sm-12">
				<h2>Relatório</h2>
				<ul class="breadcrumb mt-2">
					<li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a>
					</li>
					<li class="breadcrumb-item">Relatórios</li>
					<li class="breadcrumb-item active">Afugentamento e Resgate de Fauna</li>
				</ul>
				<button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card" id="informacoesAfugentamentoFauna" style="min-height: auto;">
					<div class="header">
						<div class="row mb-2">
							<div class="col-12 col-sm-3">
								<div class="input-group mb-3 mt-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">Ano</span>
									</div>
									<div class="form-control form-header lh-40">{{anoAtividade}}</div>
								</div>
							</div>
							<div class="col-12 col-sm-3">
								<div class="input-group mb-3 mt-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">Mês</span>
									</div>
									<div class="form-control form-header lh-40">{{mesAtividade}}</div>
								</div>
							</div>
							<div class="col-sm-6 text-right">
								<div class="row mt-3">
									<div class="input-group mb-3 col-12 col-sm-6">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">Contrato</span>
										</div>
										<div class="form-control form-header lh-40">{{numeroContrato}}</div>
									</div>
									<div class="input-group mb-3 col-12 col-sm-6 ">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">Empresa</span>
										</div>
										<div class="form-control form-header lh-40">{{session.Logado.NomeContratada}} </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="body">
						<ul class="nav nav-tabs border-bottom" id="tabsAfugentamentoFauna" role="tablist">
							<li class="nav-item">
								<a class="nav-link border active" id="home-tab" data-toggle="tab" href="#introducao-afugentamento" role="tab" aria-controls="introducao-afugentamento" aria-selected="true">Introdução</a>
							</li>
							<li class="nav-item">
								<a @click="iniciarInsercaoDados()" class="nav-link border" id="profile-tab" data-toggle="tab" href="#insercao-dados" role="tab" aria-controls="insercao-dados" aria-selected="false">Inserção de Dados</a>
							</li>
							<li class="nav-item">
								<a @click="iniciarDiscussoesConclusoes()" class="nav-link border" id="contact-tab" data-toggle="tab" href="#discussoes-conclusao" role="tab" aria-controls="discussoes-conclusao" aria-selected="false">Discussões e Conclusões</a>
							</li>
							<li class="nav-item">
								<a class="nav-link border" id="recursos-tab" data-toggle="tab" href="#recursos-utilizados" role="tab" aria-controls="recursos-utilizados" aria-selected="false">Recursos Utilizados</a>
							</li>
						</ul>
						<div class="tab-content p-4" id="myTabContent">
							<div class="tab-pane fade show active" id="introducao-afugentamento" role="tabpanel" aria-labelledby="home-tab">
								<?php $this->load->view('cgmab/Relatorios/paginas/relatorioAfugentamentoFauna/introducaoAfugentamento'); ?>
							</div>
							<div class="tab-pane fade" id="insercao-dados" role="tabpanel" aria-labelledby="profile-tab">
								<?php $this->load->view('cgmab/Relatorios/paginas/relatorioAfugentamentoFauna/insercaoDadosAfugentamento'); ?>
							</div>
							<div class="tab-pane fade" id="discussoes-conclusao" role="tabpanel" aria-labelledby="contact-tab">
								<?php $this->load->view('cgmab/Relatorios/paginas/relatorioAfugentamentoFauna/discussoesConclusoesAfugentamento'); ?>
							</div>
							<div class="tab-pane fade" id="recursos-utilizados" role="tabpanel" aria-labelledby="recursos-tab">
								<?php $this->load->view('cgmab/Relatorios/paginas/relatorioAfugentamentoFauna/recursosUtilizados'); ?>
							</div>
						</div>
						<div id="relatorioMensal"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('cgmab/Relatorios/scripts/appAfugentamentoFauna'); ?>

<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAfugentamentoFauna/appIntroducaoAfugentamento'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAfugentamentoFauna/appInsercaoDadosAfugentamento'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAfugentamentoFauna/appDiscussoesConclusoesAfugentamento'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAfugentamentoFauna/recursosUtilizados'); ?>