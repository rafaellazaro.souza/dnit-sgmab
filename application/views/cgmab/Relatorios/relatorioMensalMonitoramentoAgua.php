<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2></h2>
                <ul class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item">Relatórios</li>
                    <li class="breadcrumb-item active">Recursos Hídricos</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Relatório de Monitoramento</strong> Qualidade Recursos Hídricos</h2>
                    </div>
                    <div class="body">
                        <ul class="nav nav-tabs border-bottom" id="tabsRelatorioAgua" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link border active" id="home-tab" data-toggle="tab" href="#insersao" role="tab" aria-controls="insersao" aria-selected="true">Medições de Parâmetros dos Ponto</a>
                            </li>
                            <li class="nav-item">
                                <a @click="iniciarObservacoesCampanha()" class="nav-link border" id="profile-tab" data-toggle="tab" href="#observacao" role="tab" aria-controls="observacao" aria-selected="false">Observações Campanha</a>
                            </li>
                            <li class="nav-item">
                                <a @click="iniciarAnaliseDados()" class="nav-link border" id="contact-tab" data-toggle="tab" href="#analise-dados" role="tab" aria-controls="analise-dados" aria-selected="false">Análise de Dados</a>
                            </li>
                            <li class="nav-item">
                                <a @click="iniciarAnaliseResultados()" class="nav-link border" id="contact-tab" data-toggle="tab" href="#analise-resultados" role="tab" aria-controls="analise-resultados" aria-selected="false">Análise de Resultados</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link border" id="recursos-tab" data-toggle="tab" href="#recursos-utilidados" role="tab" aria-controls="recursos-utilidados" aria-selected="false">Recursos Utilizados</a>
                            </li>
                        </ul>
                        <div class="tab-content p-4" id="myTabContent">

                            <div class="tab-pane fade show active" id="insersao" role="tabpanel" aria-labelledby="home-tab">
                                <?php $this->load->view('cgmab/Relatorios/paginas/relatorioAgua/insercao'); ?>
                            </div>
                            <div class="tab-pane fade" id="observacao" role="tabpanel" aria-labelledby="profile-tab">
                                <?php $this->load->view('cgmab/Relatorios/paginas/relatorioAgua/observacaoCampanha'); ?>
                            </div>
                            <div class="tab-pane fade" id="analise-dados" role="tabpanel" aria-labelledby="contact-tab">
                                <?php $this->load->view('cgmab/Relatorios/paginas/relatorioAgua/analiseDados'); ?>
                            </div>
                            <div class="tab-pane fade" id="analise-resultados" role="tabpanel" aria-labelledby="contact-tab">
                                <?php $this->load->view('cgmab/Relatorios/paginas/relatorioAgua/analiseResultados'); ?>
                            </div>
                            <div class="tab-pane fade" id="recursos-utilidados" role="tabpanel" aria-labelledby="recursos-tab">
                                <?php $this->load->view('cgmab/Relatorios/paginas/relatorioAgua/recursosUtilizados'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<?php $this->load->view('cgmab/Relatorios/scripts/app'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAgua/appInsercao'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAgua/appInsercaoMedicoes'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAgua/appInsercaoObservacoes'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAgua/appInsercaoDocumentos'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAgua/appObservacoesCamapanha'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAgua/appAnaliseDados'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAgua/appAnaliseResultados'); ?>
<?php $this->load->view('cgmab/Relatorios/scripts/relatorioAgua/recursosUtilizados'); ?>