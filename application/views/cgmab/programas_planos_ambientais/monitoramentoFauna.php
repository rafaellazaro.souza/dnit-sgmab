<!-- css mapa  -->
<?php $this->load->view('../../template_lte3/plugins/leatleft/css/leatleft.php') ?>
<!-- js mapa  -->
<?php $this->load->view('../../template_lte3/plugins/leatleft/js/leatleft.php') ?>
<!-- css pagina -->
<?php $this->load->view('cgmab/programas_planos_ambientais/css.php') ?>
<!-- light box  -->
<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/ekko-lightbox/ekko-lightbox.css') ?>">

<div id="vueApp">
    <div class="container-fluid">
        <section class="content-header">

            <div class="row mb-4 mt-4">
                <div class="col-sm-10">
                    <h1>Conf. de Programas Ambientais Padronizados - Monitoramento de Fauna</h1>
                </div>
                <div class="col-sm-2 text-right">
                    <a href="javascript:;" v-if="tamanhoBotoesAno != ''" @click="tamanhoSectionsMapa = 'content mb-5 col-12', tamanhoSectionsTabelas = 'content mb-5 col-12', tamanhoBotoesAno = ''"><i class="fas fa-grip-lines"></i></a>
                    <!-- <a href="javascript:;" v-else @click="tamanhoSectionsTabelas = 'content mb-5 col-6', tamanhoSectionsTabelasArmadilhas = 'content mb-5 col-6'"><i class="fas fa-columns"></i></a> -->
                    <a href="<?php echo site_url('cgmab/configuracao') ?>" class="ml-3"><i class="fa fa-times"></i></a>
                </div>

            </div>

        </section>



        <!-- MAPA -->
        <div class="row">
            <section v-bind:class="tamanhoSectionsMapa">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fa fa-map mr-3"></i> Mapa</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                        </div>
                    </div>
                    <div class="card-body p-4">
                        <!--mapa-->
                        <div id="mapid" class="map-height mb-5"></div>
                        <!--mapa-->


                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </section>
            <!-- FIM MAPA -->

            <!-- Cadastro de módulos de amostragem  -->
            <section v-bind:class="tamanhoSectionsTabelas">
                <div class="card">
                    <div class="card-header">

                        <h3 class="card-title"><i class="fa fa-list mr-3"></i>
                            Cadastro Módulos de Amostragem
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                        </div>
                    </div>
                    <div class="card-body p-4">

                        <nav>
                            <div class="nav nav-tabs mb-2" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#geoRefCadModulosAmostragem" role="tab" aria-controls="nav-home" aria-selected="true">Georeferenciamento</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#cadFotoModAmostragem" role="tab" aria-controls="nav-profile" aria-selected="false">Cadastro Fotográfico</a>
                            </div>
                        </nav>

                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="geoRefCadModulosAmostragem" role="tabpanel" aria-labelledby="nav-home-tab">


                                <div class="row">
                                    <div class="col-8 mt-2 p-4">
                                        <div class="alert alert-info row p-3 mb-4">
                                            <div class="col-1"><i class="fas fa-info-circle fs-50"></i></div>
                                            <div class="col-11"><b>Georeferenciamento dos Módulos de Amostragem</b>
                                                Arquivo do ShapeFile incluindo o poligonal e pontos (o arquivo DBR deve conter os atribultos definidos no arquivo exel).
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-4 mt-2">
                                        <label for="exampleInputFile">Anexar</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="exampleInputFile">
                                                <label class="custom-file-label" for="exampleInputFile">Procurar no pc</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="">Enviar</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="h-450r w-100">
                                    <table class="table table-striped">
                                        <thead>
                                            <th>Id ShapeFile</th>
                                            <th>Poligonal</th>
                                            <th>Nome Módulo Amostragem</th>
                                            <th>Área</th>
                                            <th>Fitofisionomia</th>
                                            <th>Município</th>
                                            <th>Observações</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td>123</td>
                                                <td>XXX</td>
                                                <td>Modulo Teste</td>
                                                <td>44 Km</td>
                                                <td>XXXXXX</td>
                                                <td>Correntina</td>
                                                <td>-</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="cadFotoModAmostragem" role="tabpanel" aria-labelledby="nav-profile-tab">


                                <div class="row">
                                    <div class="col-12 col-sm-8 ">
                                        <div class="alert alert-warning p-4 m-3 mb-3">
                                            <div class="ic-lft">
                                                <i class="fas fa-exclamation-triangle"></i>
                                            </div>
                                            <div>
                                                <p>
                                                    <b>Atenção!</b>
                                                    Arquivo do Shape File incluindo os pontos das fotos do mês corrente (Id do Shape File na tabela é o Id do ponto da foto). O Arquivo
                                                    .DBF deve conter os atribultos definidos no Excel.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4">
                                        <div class="form-group mt-4">
                                            <label for="exampleInputFile">Arquivo Shape File</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="exampleInputFile">
                                                    <label class="custom-file-label" for="exampleInputFile">Clique aqui para buscar o arquivo</label>
                                                </div>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="">Enviar</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="h-450r w-100 mt-4">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th colspan="4"></th>
                                                <th colspan="3" class="bg-4">UTM (Data SIRGAS 2000)</th>
                                                <th colspan="3"></th>
                                            </tr>
                                            <tr>
                                                <th>Id Shape File</th>
                                                <th>Nome da Foto</th>
                                                <th>Latitude</th>
                                                <th>Longitude</th>
                                                <th class="bg-4">Zona</th>
                                                <th class="bg-4">CN</th>
                                                <th class="bg-4">CE</th>
                                                <th>Elevação (m)</th>
                                                <th>Observações</th>
                                                <th>Nome da foto no repositório de documentos</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>


                                <div class="row mt-4">
                                    <div class="col-12 col-sm-8">
                                    <div class="alert alert-info p-4 ">
                                    <div class="ic-lft">
                                        <i class="far fa-question-circle"></i>
                                    </div>
                                    <div>
                                        <p>
                                            <b>Observação!</b>
                                            As fotos deverão ser salvas com o mesmo nome da foto no repositório de documentos. Para que
                                            possa ser identificada automaticamente pelo sistema. Essa gravação deverá ser realizada clicando no botão "salvar fotos",
                                            e selecionado as fotos produzidas pela CONTRATADA e enviando para o repositório de documentos da CGMAB. Assim em uma ação
                                            a CONTRATADA consegue carregar todas as fotos. Caso queira apagar basta clicar no botão "excluir".
                                        </p>
                                    </div>
                                </div>
                                    </div>
                                    <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                    <label for="exampleInputFile">Fotos</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="exampleInputFile">
                                            <label class="custom-file-label" for="exampleInputFile">Clique aqui para buscar suas fotos</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Enviar</span>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                </div>


                                

                                
                                <!-- listgem de fotos  -->
                                <div class="row fotos-programa">

                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>

                                </div><!-- listgem de fotos  -->
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- Fim Cadastro de módulos de amostragem  -->
            
            <!-- Cadastro de Armadilhas e Pontos de Observação  -->
            <section v-bind:class="tamanhoSectionsTabelasArmadilhas">
                <div class="card">
                    <div class="card-header">

                        <h3 class="card-title"><i class="fa fa-list mr-3"></i>
                            Cadastro de Armadilhas e Pontos de Observação
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                        </div>
                    </div>
                    <div class="card-body p-4">

                        <nav>
                            <div class="nav nav-tabs mb-2" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#geoRefCadArmadilhasPontos" role="tab" aria-controls="nav-home" aria-selected="true">Georeferenciamento</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#cadFotoArmadilhasPontos" role="tab" aria-controls="nav-profile" aria-selected="false">Cadastro Fotográfico</a>
                            </div>
                        </nav>

                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="geoRefCadArmadilhasPontos" role="tabpanel" aria-labelledby="nav-home-tab">


                                <div class="row">
                                    <div class="col-8 mt-2 p-4">
                                        <div class="alert alert-info row p-3 mb-4">
                                            <div class="col-1"><i class="fas fa-info-circle fs-50"></i></div>
                                            <div class="col-11"><b>Georeferenciamento dos Módulos de Amostragem</b>
                                                Arquivo do ShapeFile incluindo o poligonal e pontos (o arquivo DBR deve conter os atribultos definidos no arquivo exel).
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-4 mt-2">
                                        <label for="exampleInputFile">Anexar</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="exampleInputFile">
                                                <label class="custom-file-label" for="exampleInputFile">Procurar no pc</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="">Enviar</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="h-450r w-100">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th colspan="7"></th>
                                                <th colspan="4">UTM (data SIRGAS 2000)</th>
                                                <th colspan="2"></th>
                                            </tr>
                                            <tr>
                                            <th>Id ShapeFile</th>
                                            <th>Número do Ponto</th>
                                            <th>Pontos de Armadilha ou Observação</th>
                                            <th>Grupo Faunístico</th>
                                            <th>Quantidade de Armadilhas</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th class="bg-2">Zona</th>
                                            <th class="bg-2">CN</th>
                                            <th class="bg-2">CE</th>
                                            <th class="bg-2">Elevação</th>
                                            <th>Observações</th>
                                            <th>Número das Fotos</th>
                                            </tr>
                                            
                                        </thead>
                                        <tbody>
                                            
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="cadFotoArmadilhasPontos" role="tabpanel" aria-labelledby="nav-profile-tab">


                                <div class="row">
                                    <div class="col-12 col-sm-8">
                                        <div class="alert alert-warning row p-4 m-3 mb-3">
                                            <div class="col-1">
                                                <i class="fas fa-exclamation-triangle"></i>
                                            </div>
                                            <div class="col-11">
                                                <p>
                                                    <b>Atenção!</b>
                                                    Arquivo do Shape File incluindo os pontos das fotos do mês corrente (Id do Shape File na tabela é o Id do ponto da foto). O Arquivo
                                                    .DBF deve conter os atribultos definidos no Excel.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4">
                                        <div class="form-group mt-4">
                                            <label for="exampleInputFile">Arquivo Shape File</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="exampleInputFile">
                                                    <label class="custom-file-label" for="exampleInputFile">Clique aqui para buscar o arquivo</label>
                                                </div>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="">Enviar</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="h-450r w-100 mt-4">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th colspan="4"></th>
                                                <th colspan="4" class="bg-4">UTM (Data SIRGAS 2000)</th>
                                                <th colspan="2"></th>
                                            </tr>
                                            <tr>
                                                <th>Id Shape File</th>
                                                <th>Nome da Foto</th>
                                                <th>Latitude</th>
                                                <th>Longitude</th>
                                                <th class="bg-4">Zona</th>
                                                <th class="bg-4">CN</th>
                                                <th class="bg-4">CE</th>
                                                <th class="bg-4">Elevação (m)</th>
                                                <th>Observações</th>
                                                <th>Nome da foto no repositório de documentos</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                            <tr>
                                                <td>465</td>
                                                <td>Foto 2xx</td>
                                                <td>-4566</td>
                                                <td>45661</td>
                                                <td>10</td>
                                                <td>CNXXXXX</td>
                                                <td>100</td>
                                                <td>50</td>
                                                <td>-</td>
                                                <td>foto2xxx.jpg</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>


                                <div class="row mt-4">
                                    <div class="col-12 col-sm-8">
                                    <div class="alert alert-info p-4 ">
                                    <div class="ic-lft">
                                        <i class="far fa-question-circle"></i>
                                    </div>
                                    <div>
                                        <p>
                                            <b>Observação!</b>
                                            As fotos deverão ser salvas com o mesmo nome da foto no repositório de documentos. Para que
                                            possa ser identificada automaticamente pelo sistema. Essa gravação deverá ser realizada clicando no botão "salvar fotos",
                                            e selecionado as fotos produzidas pela CONTRATADA e enviando para o repositório de documentos da CGMAB. Assim em uma ação
                                            a CONTRATADA consegue carregar todas as fotos. Caso queira apagar basta clicar no botão "excluir".
                                        </p>
                                    </div>
                                </div>
                                    </div>
                                    <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                    <label for="exampleInputFile">Fotos</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="exampleInputFile">
                                            <label class="custom-file-label" for="exampleInputFile">Clique aqui para buscar suas fotos</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Enviar</span>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                </div>


                                

                                
                                <!-- listgem de fotos  -->
                                <div class="row fotos-programa">

                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-12 col-sm-2">
                                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                Nome da foto aqui Nome da foto aqui
                                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                            </li>
                                        </ul>
                                    </div>

                                </div><!-- listgem de fotos  -->
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- Fim Cadastro de Armadilhas e Pontos de Observação -->


        </div>
    </div>
</div>
<?php $this->load->view('cgmab/programas_planos_ambientais/scripts/monitoramentoFauna-script.php') ?>
<script src="<?= base_url('template_lte3/plugins/ekko-lightbox/ekko-lightbox.min.js') ?>"></script>


