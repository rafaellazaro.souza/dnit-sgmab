<script>
    var monitoramentoFauna = new Vue({
        el: '#vueApp',
        data() {
            return {
                mymap: null,
                tamanhoSectionsMapa: 'content mb-5 col-12',
                tamanhoSectionsTabelas: 'content mb-5 col-12',
                tamanhoSectionsTabelasCadFotografico: 'content mb-5 col-12',
                tamanhoSectionsTabelasArmadilhas: 'content mb-5 col-12',
                tamanhoBotoesAno: ''
            }
        },
        mounted() {
            this.gerarMapa();
        },
        methods: {
            gerarMapa() {
                this.mymap = L.map('mapid').setView(['-10.6007767', '-63.6037797'], 3.5);
                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 18,
                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
                    id: 'mapbox.streets'
                }).addTo(this.mymap);
            },
        }
    });

    //lightbox
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            alwaysShowClose: true
        });
    });
</script>
