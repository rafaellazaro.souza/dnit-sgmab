<script>
    var execucaoAmbiental = new Vue({
        el: "#vueApp",
        data() {
            return {
                mymap: null,
                cronograma: true,
                listaPrograma: false,
                formPrograma: false,
                ano: '',
                mes: '',
                idPrograma: '',
                atividade: '',
                tipoPrograma: '',
                tamanhoSectionsMapa: 'content mb-5 col-12',
                tamanhoSectionsTabelas: 'content mb-5 col-12',
                tamanhoBotoesAno: ''
            }

        },
        watch: {
            ano(a) {
                if (this.mes != '')
                    this.abrirJanela(this.mes, this.ano)
            },
            idPrograma(a) {
                if (this.idPrograma != '')
                    this.abrirFormulario(this.idPrograma);
            },
            atividade() {
                if (this.atividade != '')
                    $('#modalAlteraStatusAtividade').modal();
            }
        },
        mounted() {
            this.gerarMapa();

        },

        methods: {
            gerarMapa() {
                this.mymap = L.map('mapid').setView(['-10.6007767', '-63.6037797'], 3.5);
                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 18,
                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
                    id: 'mapbox.streets'
                }).addTo(this.mymap);
            },
            abrirJanela(mes, ano) {
                this.cronograma = false
                this.listaPrograma = true
                personalizarDataTable('#tableListaProgramasMes');
            },
            abrirFormulario(id) {
                this.formPrograma = true
                this.cronograma = false
                this.listaPrograma = false
            },

        },
    });

</script>

<?php $this->load->view('cgmab/js/comum.php');?>