<style>
    .map-height {
        height: 700px;
    }

    .btn-filtros {
        width: 7.9% !important;
    }

    .btn>.badge {
        top: -13px;
    }

    .bg-1 {
        background-color: #f7fdf6;
    }

    .bg-2 {
        background-color: #f4f4f4;
    }

    .bg-3 {
        background-color: #e9f1f4;
    }

    .bg-4 {
        background-color: #e4e4e4;
    }

    .bg-5 {
        background-color: #a0666b;
    }

    .bg-6 {
        background-color: #dff6df;
    }

    .bg-7 {
        background-color: #e6e6ff;
    }

    .cl-1 {
        color: #fff !important;
    }

    div.dataTables_wrapper div.dataTables_filter {
        position: relative !important;
    }

    .jqte {
        margin: 0px !important;
    }

    .jqte_tool.jqte_tool_1 .jqte_tool_label {
        height: 22px;
    }

    .h-200r {
        height: 200px;
        overflow: auto;
    }

    .h-450r {
        height: 450px;
        overflow: auto;
    }

    .steps {
        height: 300px;
        max-height: 300px;
        overflow: auto;
        border-right: 2px solid #fff;
        border-radius: 6px;
    }

    .steps>div {
        padding: 5px;
        border: 1px solid #fff;
        margin-bottom: 8px;
        background: #fff;
    }

    .steps>div:hover {
        border: 1px dashed #9f9f9f;
        cursor: pointer;
    }

    .add-atv {
        position: absolute;
        bottom: 14px;
        right: 15px;
    }
    .marcador {
    position: absolute;
    right: 19px;
    margin: 3px 0 0 0;
    border-radius: 50%;
    width: 16px;
    height: 16px;
}
.ic-lft {
    width: 58px;
    float: left;
    font-size: 35px;
}
.fotos-programa >div {
    margin-bottom: 18px;
}

tbody > tr > td {
    vertical-align: middle !important;
}
.map-height {
    height: 700px;
}
.nav-tabs .nav-link {
    background-color: #e9ecef;
}

// Small devices (landscape phones, 576px and up)
@media (min-width: 576px) { 

 }

// Medium devices (tablets, 768px and up)
@media (min-width: 768px) {
    
 }

// Large devices (desktops, 992px and up)
@media (min-width: 992px) { 
    
 }

// Extra large devices (large desktops, 1200px and up)
@media (min-width: 1200px) { 
    
 }
</style>