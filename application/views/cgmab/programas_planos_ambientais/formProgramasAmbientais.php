<div v-show="formPrograma && idPrograma != ''">

    <div class="row">
        <div class="col-11"> </div>
        <div class="col-1 text-right">
            <a href="javascript:;" v-on:click="formPrograma = !formPrograma, listaPrograma = !listaPrograma, idPrograma = ''"><i class="fa fa-times"></i></a>
        </div>

        <div class="col-4 border-right">
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><span class="font-weight-bold">Programa 01</li>
                <li class="list-group-item"><span class="font-weight-bold">Empreendimento:</span> Duplicação BR 101 MT</li>
                <li class="list-group-item"><span class="font-weight-bold">Lotes:</span> 01/02</li>
                <li class="list-group-item"><span class="font-weight-bold">Licença Ambiental:</span> Licença de Instalação</li>
                <li class="list-group-item"><span class="font-weight-bold">Número:</span> 123454</li>
                <li class="list-group-item"><span class="font-weight-bold">IBAMA</li>
            </ul>
        </div>
        <div class="col-4 border-right">
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><span class="font-weight-bold">Tipo:</span> Programa</li>
                <li class="list-group-item"><span class="font-weight-bold">Sub Tipo:</span> -</li>
                <li class="list-group-item"><span class="font-weight-bold">Tema:</span> Fauna</li>
                <li class="list-group-item"><span class="font-weight-bold">Nome Padrão DNIT:</span> XXXXXXXXXXXXXX</li>
                <li class="list-group-item"><span class="font-weight-bold">Descrição:</span> Texto aqui da descrição descrevendo o que tem queser descrito.Texto aqui da descrição descrevendo o que tem queser descrito. </li>
            </ul>
        </div>
        <div class="col-4 border-right">
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><span class="font-weight-bold">Data de início:</span> 22/11/2019</li>
                <li class="list-group-item"><span class="font-weight-bold">N. Meses:</span> 7/ 24</li>
                <li class="list-group-item"><span class="font-weight-bold">Status:</span> <select class="form-control"></select></li>
                <li class="list-group-item"><span class="font-weight-bold">Cronograma:</span> <select class="form-control"></select></li>
                <li class="list-group-item"><span class="font-weight-bold">Percentual Concluído:</span> 82%</li>
            </ul>
        </div>

        <div class="col-12 mt-4">
            <ul class="nav nav-tabs bg-3" id="myTab" role="tablist">
                <li class="nav-item border">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#descricao" role="tab" aria-controls="home" aria-selected="true">01 - Descrição</a>
                </li>
                <li class="nav-item border">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#cronograma" role="tab" aria-controls="profile" aria-selected="false">02 - Cronograma do Programa</a>
                </li>
                <li class="nav-item border">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#acompanhamento" role="tab" aria-controls="contact" aria-selected="false">03 - Acompanhamento/ Relatório Técnico</a>
                </li>

                <li class="nav-item border">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#relfot" role="tab" aria-controls="contact" aria-selected="false">04 - Rel. Fotográfico</a>
                </li>
                <li class="nav-item border">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#rh" role="tab" aria-controls="contact" aria-selected="false">05 - Lista de Recursos Humanos para Realização do Programa</a>
                </li>
                <li class="nav-item border">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#infraestrutura" role="tab" aria-controls="contact" aria-selected="false">06 - Infraestrutura p/ Realização do Programa</a>
                </li>
                <li class="nav-item border">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#parecerTecnico" role="tab" aria-controls="contact" aria-selected="false">07 - Parecer Técnico</a>
                </li>
            </ul>
            <!-- tabs  -->
            <div class="tab-content pt-4" id="myTabContent">
                <!-- descrição  -->
                <div class="tab-pane fade show active" id="descricao" role="tabpanel" aria-labelledby="profile-tab">

                    <h4 class="mb-4 w-100">1 - Descrição</h4>

                    <form class="row">
                        <div class="input-group col-12 col-sm-6">
                            <label class="w-100">Descrição</label>
                            <textarea class="textarea"></textarea>
                        </div>
                        <div class="input-group col-12 col-sm-6">
                            <label class="w-100">Justificativa</label>
                            <textarea class="textarea"></textarea>
                        </div>
                        <div class="input-group col-12 col-sm-6 mt-5">
                            <label class="w-100">Objetivo</label>
                            <textarea class="textarea"></textarea>
                        </div>
                        <div class="input-group col-12 col-sm-6 mt-5 ">
                            <label class="w-100">Metas</label>
                            <div class="w-100 h-200r">
                                <table class="table table-striped ">
                                    <thead>
                                        <tr>
                                            <th>Num</th>
                                            <th>Meta</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Meta 1</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Meta 2</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Meta 3</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Meta 4</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Meta 5</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <a href="javascript:;" class="btn btn-outline-secondary mt-3"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="input-group col-12 mt-5 ">
                            <label class="w-100">Indicadores</label>
                            <div class="w-100 h-200r">
                                <table class="table table-striped ">
                                    <thead>
                                        <tr>
                                            <th>Num</th>
                                            <th>Indicador</th>
                                            <th>Descrição</th>
                                            <th>Fórmula de Cálculo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Indicador 1</td>
                                            <td>Descrição 1</td>
                                            <td>Realizado ou não</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Indicador 2</td>
                                            <td>Descrição 2</td>
                                            <td>Fórmula 1</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Indicador 3</td>
                                            <td>Descrição 3</td>
                                            <td>Fórmula 1</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Indicador 3</td>
                                            <td>Descrição 3</td>
                                            <td>Fórmula 1</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Indicador 4</td>
                                            <td>Descrição 4</td>
                                            <td>Fórmula 1</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Indicador 5</td>
                                            <td>Descrição 5</td>
                                            <td>Fórmula 1</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <a href="javascript:;" class="btn btn-outline-secondary mt-3"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="input-group col-12 col-sm-6 mt-5">
                            <label class="w-100">Público Alvo</label>
                            <textarea class="textarea"></textarea>
                        </div>
                        <div class="input-group col-12 col-sm-6 mt-5">
                            <label class="w-100">Metodologia</label>
                            <textarea class="textarea"></textarea>
                        </div>
                        <div class="input-group col-12 text-right mt-5">
                            <button type="button" class="btn btn-outline-secondary mr-2">Cancelar</button>
                            <button type="button" class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div><!-- fim descrição  -->
                <!-- Cronograma do Programa  -->
                <div class="tab-pane fade" id="cronograma" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <div class="col-12 col-sm-6 border-right p-4">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <h4>Cronograma Programa 1</h4>
                                </li>
                                <li class="list-group-item">
                                    <span class="font-weight-bold ">Duração do Programa (meses):</span> 12
                                </li>
                                <li class="list-group-item">
                                    <span class="font-weight-bold ">Previsão de Início:</span> Abril - 2019
                                </li>
                                <li class="list-group-item">
                                    <span class="font-weight-bold ">Mês Corrente:</span> 8
                                </li>
                                <li class="list-group-item">
                                    <span class="font-weight-bold ">Períodicidade:</span> Trimestral
                                </li>
                                <li class="list-group-item">
                                    <span class="font-weight-bold ">Prazo:</span> 30 dias antes do início da obra
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-6 p-4">
                            <h4>Status Cronograma Programa Ambiental</h4>
                            Previsto 60%
                            <div class="progress mt-2">

                                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 60%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            Atacado 15%
                            <div class="progress mt-2">

                                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 15%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            Concluído 15%
                            <div class="progress mt-2">

                                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 15%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>

                    <div class="w-100 h-450r mt-5">
                        <table class="table table-striped">
                            <thead>
                                <th>Num.</th>
                                <th>Mês/ Ano</th>
                                <th>Periodicidade dos Relatórios</th>
                                <th>Atividades Previstas no mês</th>
                                <th>Atividades Concluídas</th>
                                <th>Atividades Pendentes</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Abril-2019</td>
                                    <td>Não</td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Abril-2019</td>
                                    <td>Não</td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Abril-2019</td>
                                    <td>Não</td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Abril-2019</td>
                                    <td>Não</td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Abril-2019</td>
                                    <td>Não</td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Abril-2019</td>
                                    <td>Não</td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>Abril-2019</td>
                                    <td>Não</td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>Abril-2019</td>
                                    <td>Não</td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td>
                                        Preparação <br>
                                        Mobilização de Equipe
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>

                </div><!-- Fim Cronograma do Programa  -->
                <!-- acompanhamento de atividades  -->
                <div class="tab-pane fade" id="acompanhamento" role="tabpanel" aria-labelledby="contact-tab">
                    <h4>Quadro de Atividades</h4>
                    <div class="row mt-5">
                        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-5  p-3 steps">
                            <h5 class="cl-1">Pendetes</h5>
                            <div class="w-100" @click="atividade = '1'">Atividade 01
                                <span class="badge badge-warning marcador">.</span>
                            </div>
                            <div class="w-100" @click="atividade='3'">Atividade 03
                                <span class="badge badge-danger marcador">.</span>
                            </div>

                        </div>
                        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-4 p-3 steps" v-if="tipoPrograma === 'adm'">
                            <h5>Previstas</h5>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatoriotecnicopadrao') ?>">Atividade 09</a></div>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatoriotecnicopadrao') ?>">Atividade 10</a></div>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatoriotecnicopadrao') ?>">Atividade 11</a></div>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatoriotecnicopadrao') ?>">Atividade 12</a></div>
                            <button class="btn btn-sm btn-default add-atv" data-toggle="modal" data-target="#modalAddAtividadeQuadro"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-4 p-3 steps" v-if="tipoPrograma === 'amb-atro'">
                            <h5>Previstas</h5>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatorioMonitoramentoAtropelamentoFauna') ?>">Atividade 09</a></div>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatorioMonitoramentoAtropelamentoFauna') ?>">Atividade 10</a></div>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatorioMonitoramentoAtropelamentoFauna') ?>">Atividade 11</a></div>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatorioMonitoramentoAtropelamentoFauna') ?>">Atividade 12</a></div>
                            <button class="btn btn-sm btn-default add-atv" data-toggle="modal" data-target="#modalAddAtividadeQuadro"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-4 p-3 steps" v-else>
                            <h5>Previstas</h5>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatorioMonitoramentoFauna') ?>">Atividade 09</a></div>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatorioMonitoramentoFauna') ?>">Atividade 10</a></div>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatorioMonitoramentoFauna') ?>">Atividade 11</a></div>
                            <div class="w-100"><a href="<?php echo site_url('cgmab/subprogramas/relatorioMonitoramentoFauna') ?>">Atividade 12</a></div>
                            <button class="btn btn-sm btn-default add-atv" data-toggle="modal" data-target="#modalAddAtividadeQuadro"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-7 p-3 steps">
                            <h5>Atacadas</h5>
                            <div class="w-100" @click="atividade='7'">Atividade 08</div>
                            <button class="btn btn-sm btn-default add-atv" data-toggle="modal" data-target="#modalAddAtividadeQuadro"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-6 p-3 steps">
                            <h5>Realizadas</h5>
                            <div class="w-100" @click="atividade='8'">Atividade 02</div>
                            <button class="btn btn-sm btn-default add-atv" data-toggle="modal" data-target="#modalAddAtividadeQuadro"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-4 p-3 steps">
                            <h5>P/ Mês Seg.</h5>

                            <div class="w-100" @click="atividade='9'">Atividade 04</div>
                            <div class="w-100" @click="atividade='10'">Atividade 05</div>
                            <div class="w-100" @click="atividade='11'">Atividade 06</div>
                            <button class="btn btn-sm btn-default add-atv" data-toggle="modal" data-target="#modalAddAtividadeQuadro"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-12 col-sm-4 col-md-3 col-xl-2 bg-4 p-3 steps">
                            <h5>Planejadas Mês Seg.</h5>
                            <div class="w-100" @click="atividade='12'">Relatório do status 3</div>
                            <button class="btn btn-sm btn-default add-atv" data-toggle="modal" data-target="#modalAddAtividadeQuadro" data-toggle="modal" data-target="#modalAddAtividadeQuadro"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div><!-- Fim Relatório técnico de execução  -->
                <!-- relatório fotográfico de execução  -->
                <div class="tab-pane fade" id="relfot" role="tabpanel" aria-labelledby="contact-tab">
                    <h4 class="mb-4"> Relatório Fotográfico</h4>
                    <div class="alert alert-warning p-4">
                        <div class="ic-lft">
                            <i class="fas fa-exclamation-triangle"></i>
                        </div>
                        <div>
                            <p>
                                <b>Atenção!</b>
                                Arquivo do Shape File incluindo os pontos das fotos do mês corrente (Id do Shape File na tabela é o Id do ponto da foto). O Arquivo
                                .DBF deve conter os atribultos definidos no Excel.
                            </p>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Arquivo Shape File</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">Clique aqui para buscar o arquivo</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="">Enviar</span>
                            </div>
                        </div>
                    </div>
                    <div class="h-450r w-100 mt-4">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th colspan="3"></th>
                                    <th colspan="3" class="bg-4">UTM (Data SIRGAS 2000)</th>
                                    <th colspan="4"></th>
                                </tr>
                                <tr>
                                    <th>Id Shape File</th>
                                    <th>Id Atividade</th>
                                    <th>Nome da Foto</th>
                                    <th class="bg-4">Zona</th>
                                    <th class="bg-4">CN</th>
                                    <th class="bg-4">CE</th>
                                    <th>Elevação (m)</th>
                                    <th>Data</th>
                                    <th>Observações</th>
                                    <th>Nome da foto no repositório de documentos</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr></tr>
                                <tr></tr>
                                <tr></tr>
                                <tr></tr>
                                <tr></tr>
                                <tr></tr>
                                <tr></tr>
                                <tr></tr>
                                <tr></tr>
                                <tr></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="alert alert-info p-4">
                        <div class="ic-lft">
                            <i class="far fa-question-circle"></i>
                        </div>
                        <div>
                            <p>
                                <b>Observação!</b>
                                As fotos deverão ser salvas com o mesmo nome da foto no repositório de documentos. Para que
                                possa ser identificada automaticamente pelo sistema. Essa gravação deverá ser realizada clicando no botão "salvar fotos",
                                e selecionado as fotos produzidas pela CONTRATADA e enviando para o repositório de documentos da CGMAB. Assim em uma ação
                                a CONTRATADA consegue carregar todas as fotos. Caso queira apagar basta clicar no botão "excluir".
                            </p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputFile">Fotos</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">Clique aqui para buscar suas fotos</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="">Enviar</span>
                            </div>
                        </div>
                    </div>
                    <!-- listgem de fotos  -->
                    <div class="row fotos-programa">

                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>

                    </div><!-- listgem de fotos  -->
                </div><!-- fim relatório fotográfico de execução  -->
                <!-- lista de recursos para realização do programa  -->
                <div class="tab-pane fade" id="rh" role="tabpanel" aria-labelledby="contact-tab">
                    <h4>Lista de Recursos para Realização do Programa</h4>
                    <div class="h-450r w-100">
                        <table class="table table-striped">
                            <tr>
                                <thead>
                                    <th>Foto</th>
                                    <th>id</th>
                                    <th>Nome</th>
                                    <th>C.P.F</th>
                                    <th>Cargo DNIT</th>
                                    <th>Cod. DNIT</th>
                                    <th>CTF</th>
                                    <th>Profissão</th>
                                    <th>Reg. Conselho de Classe</th>
                                    <th>Documentação</th>
                                    <th>Observação</th>
                                    <th>Lotação</th>
                                    <th>Serviços Associados</th>
                                    <th>Status Mobilização</th>
                                    <th>Tipo Mobilização</th>
                                    <th>Ações</th>
                                </thead>
                            </tr>

                            <tbody>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/85" alt="">
                                    </td>
                                    <td>1</td>
                                    <td>Nome do Profissional Aqui</td>
                                    <td>321.487-154-10</td>
                                    <td>Engenheiro Florestal</td>
                                    <td>8220</td>
                                    <td>12345</td>
                                    <td>Engenheiro Florestal</td>
                                    <td>12345DF</td>
                                    <td>
                                        Diploma <br>
                                        Termo de serviço 1 <br>
                                        Termo de serviço 2 <br>
                                        Termo de serviço 3 <br>
                                        Declaração 1 <br>
                                        Declaração 2
                                    <td>Texto da observação aqui</td>
                                    <td>Escritório Brasília</td>
                                    <td>
                                        Gerenciamento Ambiental <br>
                                        Programa 1 <br>
                                        Subprograma 1 <br>
                                    </td>
                                    <td>Mobilizado</td>
                                    <td>Permanente</td>
                                    <td>
                                        <a href="javascritp:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/85" alt="">
                                    </td>
                                    <td>1</td>
                                    <td>Nome do Profissional Aqui</td>
                                    <td>321.487-154-10</td>
                                    <td>Engenheiro Florestal</td>
                                    <td>8220</td>
                                    <td>12345</td>
                                    <td>Engenheiro Florestal</td>
                                    <td>12345DF</td>
                                    <td>
                                        Diploma <br>
                                        Termo de serviço 1 <br>
                                        Termo de serviço 2 <br>
                                        Termo de serviço 3 <br>
                                        Declaração 1 <br>
                                        Declaração 2
                                    <td>Texto da observação aqui</td>
                                    <td>Escritório Brasília</td>
                                    <td>
                                        Gerenciamento Ambiental <br>
                                        Programa 1 <br>
                                        Subprograma 1 <br>
                                    </td>
                                    <td>Mobilizado</td>
                                    <td>Permanente</td>
                                    <td>
                                        <a href="javascritp:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-3 my-1 mt-4">
                        <label class="sr-only" for="inlineFormInputGroupUsername">Username</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">CPF</div>
                            </div>
                            <input type="text" class="form-control" placeholder="Digite o CPF">
                            <button class="btn btn-sm btn-outline-secondary"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                </div><!-- fim lista de recursos para realização do programa  -->

                <!-- Infraestutura para realização do programa  -->
                <div class="tab-pane fade" id="infraestrutura" role="tabpanel" aria-labelledby="contact-tab">
                    <h4 class="mb-4">Infraestutura para Realização do Programa</h4>
                    <!-- fim veículos  -->
                    <div class="row border mb-4 mt-5 p-4 bg-2">
                        <h5 class="w-100">Veículo</h5>
                        <div class="col-12 col-sm-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Marca</span>
                                </div>
                                <select class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Modelo</span>
                                </div>
                                <select class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <button class="btn btn-outline-secondary">Salvar</button>
                        </div>
                    </div>
                    <div class="h-450r w-100 border">
                        <table class="table table-striped">
                            <tbody>
                                <th>Foto</th>
                                <th>Id</th>
                                <th>Codigo SICRO</th>
                                <th>Veículo</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>Placa</th>
                                <th>Chassi</th>
                                <th>Categoria/Potência</th>
                                <th>Lotação</th>
                                <th>Observações</th>
                                <th>Serviços Associados</th>
                                <th>Ações</th>
                            </tbody>
                            <tbody>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/80" class="img-thumbnail">
                                    </td>
                                    <td>122</td>
                                    <td>465789</td>
                                    <td>Hylux 4WD 2014/2015</td>
                                    <td>Toyota</td>
                                    <td>SRV 2.8 4x4</td>
                                    <td>XXX487</td>
                                    <td>TXXXXXXXXXXXXXXXX</td>
                                    <td>Caminhonete/ 220 CV</td>
                                    <td>Brasília - DF</td>
                                    <td> - </td>
                                    <td>
                                        Supervisão Ambiental <br>
                                        Programa 1 <br>
                                        Estudo
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/80" class="img-thumbnail">
                                    </td>
                                    <td>122</td>
                                    <td>465789</td>
                                    <td>Hylux 4WD 2014/2015</td>
                                    <td>Toyota</td>
                                    <td>SRV 2.8 4x4</td>
                                    <td>XXX487</td>
                                    <td>TXXXXXXXXXXXXXXXX</td>
                                    <td>Caminhonete/ 220 CV</td>
                                    <td>Brasília - DF</td>
                                    <td> - </td>
                                    <td>
                                        Supervisão Ambiental <br>
                                        Programa 1 <br>
                                        Estudo
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/80" class="img-thumbnail">
                                    </td>
                                    <td>122</td>
                                    <td>465789</td>
                                    <td>Hylux 4WD 2014/2015</td>
                                    <td>Toyota</td>
                                    <td>SRV 2.8 4x4</td>
                                    <td>XXX487</td>
                                    <td>TXXXXXXXXXXXXXXXX</td>
                                    <td>Caminhonete/ 220 CV</td>
                                    <td>Brasília - DF</td>
                                    <td> - </td>
                                    <td>
                                        Supervisão Ambiental <br>
                                        Programa 1 <br>
                                        Estudo
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/80" class="img-thumbnail">
                                    </td>
                                    <td>122</td>
                                    <td>465789</td>
                                    <td>Hylux 4WD 2014/2015</td>
                                    <td>Toyota</td>
                                    <td>SRV 2.8 4x4</td>
                                    <td>XXX487</td>
                                    <td>TXXXXXXXXXXXXXXXX</td>
                                    <td>Caminhonete/ 220 CV</td>
                                    <td>Brasília - DF</td>
                                    <td> - </td>
                                    <td>
                                        Supervisão Ambiental <br>
                                        Programa 1 <br>
                                        Estudo
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!-- fim veículos  -->
                    <!-- Equipamentos  -->
                    <div class="row border mb-4 mt-5 p-4 bg-2">
                        <h5 class="w-100">Equipamentos</h5>

                        <div class="col-12 col-sm-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Equipamento</span>
                                </div>
                                <select class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <button class="btn btn-outline-secondary">Salvar</button>
                        </div>
                    </div>
                    <div class="h-450r w-100 border">
                        <table class="table table-striped">
                            <tbody>
                                <th>Foto</th>
                                <th>Id</th>
                                <th>Codigo SICRO</th>
                                <th>Equipamento</th>
                                <th>Descrição/ Modelo</th>
                                <th>Número de Série</th>
                                <th>Serviços Associados</th>
                                <th>Observações</th>
                                <th>Ações</th>
                            </tbody>
                            <tbody>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/80" class="img-thumbnail">
                                    </td>
                                    <td>122</td>
                                    <td>465789</td>
                                    <td>Armadilha Fauna</td>
                                    <td>Pitfall</td>
                                    <td>45123132</td>
                                    <td>
                                        Programa 1 <br>
                                        Estudo 1</td>
                                    <td></td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/80" class="img-thumbnail">
                                    </td>
                                    <td>122</td>
                                    <td>465789</td>
                                    <td>Armadilha Fauna</td>
                                    <td>Pitfall</td>
                                    <td>45123132</td>
                                    <td>
                                        Programa 1 <br>
                                        Estudo 1</td>
                                    <td></td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/80" class="img-thumbnail">
                                    </td>
                                    <td>122</td>
                                    <td>465789</td>
                                    <td>Armadilha Fauna</td>
                                    <td>Pitfall</td>
                                    <td>45123132</td>
                                    <td>
                                        Programa 1 <br>
                                        Estudo 1</td>
                                    <td></td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/80" class="img-thumbnail">
                                    </td>
                                    <td>122</td>
                                    <td>465789</td>
                                    <td>Armadilha Fauna</td>
                                    <td>Pitfall</td>
                                    <td>45123132</td>
                                    <td>
                                        Programa 1 <br>
                                        Estudo 1</td>
                                    <td></td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/80" class="img-thumbnail">
                                    </td>
                                    <td>122</td>
                                    <td>465789</td>
                                    <td>Armadilha Fauna</td>
                                    <td>Pitfall</td>
                                    <td>45123132</td>
                                    <td>
                                        Programa 1 <br>
                                        Estudo 1</td>
                                    <td></td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/80" class="img-thumbnail">
                                    </td>
                                    <td>122</td>
                                    <td>465789</td>
                                    <td>Armadilha Fauna</td>
                                    <td>Pitfall</td>
                                    <td>45123132</td>
                                    <td>
                                        Programa 1 <br>
                                        Estudo 1</td>
                                    <td></td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="http://via.placeholder.com/80" class="img-thumbnail">
                                    </td>
                                    <td>122</td>
                                    <td>465789</td>
                                    <td>Armadilha Fauna</td>
                                    <td>Pitfall</td>
                                    <td>45123132</td>
                                    <td>
                                        Programa 1 <br>
                                        Estudo 1</td>
                                    <td></td>
                                    <td>
                                        <a href="javascript:;" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!-- Fim Equipamentos  -->
                </div><!-- fimInfraestutura para realização do programa  -->
                <!-- Parecer Técnico  -->

                <div class="tab-pane fade" id="parecerTecnico" role="tabpanel" aria-labelledby="contact-tab">
                    <h4>Parecer Técnico</h4>
                    <div class="row">


                        <div class="col-12 col-sm-8">
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Paracer Técnico</span>
                                </div>
                                <textarea class="form-control textarea"></textarea>
                            </div>
                            <div class="h-450r w-100">
                                <table class="table table-striped">
                                    <thead>
                                        <th>Critério de Aprovação</th>
                                        <th>Status</th>
                                        <th>Observação</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Check list 1</td>
                                            <td>
                                                <select class="form-control">
                                                    <option value="1">Aprovado</option>
                                                    <option value="1">Reprovado</option>
                                                </select>
                                            </td>
                                            <td>
                                                Texto da observação aqui
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Check list 1</td>
                                            <td>
                                                <select class="form-control">
                                                    <option value="1">Aprovado</option>
                                                    <option value="1">Reprovado</option>
                                                </select>
                                            </td>
                                            <td>
                                                Texto da observação aqui
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Check list 1</td>
                                            <td>
                                                <select class="form-control">
                                                    <option value="1">Aprovado</option>
                                                    <option value="1">Reprovado</option>
                                                </select>
                                            </td>
                                            <td>
                                                Texto da observação aqui
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Check list 1</td>
                                            <td>
                                                <select class="form-control">
                                                    <option value="1">Aprovado</option>
                                                    <option value="1">Reprovado</option>
                                                </select>
                                            </td>
                                            <td>
                                                Texto da observação aqui
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Check list 1</td>
                                            <td>
                                                <select class="form-control">
                                                    <option value="1">Aprovado</option>
                                                    <option value="1">Reprovado</option>
                                                </select>
                                            </td>
                                            <td>
                                                Texto da observação aqui
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Check list 1</td>
                                            <td>
                                                <select class="form-control">
                                                    <option value="1">Aprovado</option>
                                                    <option value="1">Reprovado</option>
                                                </select>
                                            </td>
                                            <td>
                                                Texto da observação aqui
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Check list 1</td>
                                            <td>
                                                <select class="form-control">
                                                    <option value="1">Aprovado</option>
                                                    <option value="1">Reprovado</option>
                                                </select>
                                            </td>
                                            <td>
                                                Texto da observação aqui
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Check list 1</td>
                                            <td>
                                                <select class="form-control">
                                                    <option value="1">Aprovado</option>
                                                    <option value="1">Reprovado</option>
                                                </select>
                                            </td>
                                            <td>
                                                Texto da observação aqui
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Nome do Fiscal</span>
                                </div>
                                <input type="text" class="form-control">
                            </div>
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">N. Portaria</span>
                                </div>
                                <input type="text" class="form-control">
                            </div>
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Tipo de Fiscal</span>
                                </div>
                                <select class="form-control"></select>
                            </div>
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">E-mail DNIT</span>
                                </div>
                                <input type="text" class="form-control">
                            </div>
                            <div class="input-group mt-2 mb-5">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Telefone DNIT</span>
                                </div>
                                <input type="text" class="form-control">
                            </div>


                        </div>
                        <div class="col-12 text-right">
                            <button class="btn btn-outline-secondary">Cancelar</button>
                            <button class="btn btn-primary">Salvar</button>
                        </div>


                    </div>
                </div>


            </div><!-- Fim Parecer Técnico  -->
        </div>
        <!-- fim tabs  -->
    </div>
</div>
</div>