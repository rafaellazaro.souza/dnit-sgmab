<!-- css mapa  -->
<?php $this->load->view('../../template_lte3/plugins/leatleft/css/leatleft.php') ?>
<!-- js mapa  -->
<?php $this->load->view('../../template_lte3/plugins/leatleft/js/leatleft.php') ?>
<!-- js Editor  -->
<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/jquery-te/jquery-te-1.4.0.css') ?>">

<!-- light box  -->
<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/ekko-lightbox/ekko-lightbox.css') ?>">

<!-- css local -->
<?php $this->load->view('cgmab/programas_planos_ambientais/css.php') ?>
<div id="vueApp">
    <div class="container-fluid">
        <section class="content-header">

            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Execução de Programas Ambientais</h1>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="javascript:;" v-if="tamanhoBotoesAno != ''" @click="tamanhoSectionsMapa = 'content mb-5 col-12', tamanhoSectionsTabelas = 'content mb-5 col-12', tamanhoBotoesAno = ''"><i class="fas fa-grip-lines"></i></a>
                    <a href="javascript:;" v-else  @click="tamanhoSectionsMapa = 'content mb-5 col-4', tamanhoSectionsTabelas = 'content mb-5 col-8', tamanhoBotoesAno = 'width: 13.9% !important'"><i class="fas fa-columns"></i></a>
                    <a href="<?php echo site_url('cgmab/configuracao') ?>" class="ml-3"><i class="fa fa-times"></i></a>
                </div>

            </div>

        </section>

        <!-- MAPA -->
        <div class="row">
           <section v-bind:class="tamanhoSectionsMapa">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa fa-map mr-3"></i> Mapa</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body p-4">
                    <!--mapa-->
                    <div id="mapid" class="map-height mb-5"></div>
                    <!--mapa-->


                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

        </section>
        <!-- FIM MAPA -->

        <?php $this->load->view('cgmab/programas_planos_ambientais/listaProgramasAmbientais.php') ?> 
        </div>
        



    </div>
</div>


<?php $this->load->view('cgmab/programas_planos_ambientais/modais/modalAddAtividadeQuadro.php') ?>
<?php $this->load->view('cgmab/programas_planos_ambientais/modais/modalAlteraStatusAtividade.php') ?>
<?php $this->load->view('cgmab/programas_planos_ambientais/modais/modalAddRelatorioExecucao.php') ?>

<script src="<?= base_url('template_lte3/plugins/jquery-te/jquery-te-1.4.0.min.js') ?>"></script>
<script src="<?= base_url('template_lte3/plugins/ekko-lightbox/ekko-lightbox.min.js') ?>"></script>
<?php $this->load->view('cgmab/programas_planos_ambientais/scripts/execucao-script.php') ?>