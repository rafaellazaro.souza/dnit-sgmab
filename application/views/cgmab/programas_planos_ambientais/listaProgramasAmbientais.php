<section v-bind:class="tamanhoSectionsTabelas">
    <div class="card">
        <div class="card-header">

            <h3 class="card-title"><i class="fa fa-list mr-3"></i>
                <!-- @click="cronograma = !cronograma, listaPrograma = !listaPrograma, formPrograma = !formPrograma, mes = '', ano = ''" -->
                <!-- @click="listaPrograma = !listaPrograma"  -->
                <!-- @click="formPrograma = !formPrograma"  -->
                <a v-show="cronograma || formPrograma ||listaPrograma" href="javascript:;">{{mes != '' ? mes+'/ '+ano : 'Filtro'}}</a>
                <a v-if="listaPrograma || formPrograma" href="javascript:;"><i class="fas fa-caret-right ml-2 mr-3"></i>Lista de Programas Ambientais</a>
                <a v-if="formPrograma && !cronograma && !listaPrograma  " href="javascript:;"><i class="fas fa-caret-right ml-2 mr-3"></i>Editar</a>
            </h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
            </div>
        </div>
        <div class="card-body p-4">
            <div class="row pt-5 pb-5 bg-2" v-show="cronograma">
                <div class="col-9 p-2">
                    <div class="row p-3">
                        <div class="col-1">
                            <h4> 2019 </h4>
                        </div>
                        <div class="col-11">
                            <button class="btn btn-success btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes= 'Jan', ano = '2019'">Jan</button>
                            <button class="btn btn-success btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Fev', ano = '2019'">Fev</button>
                            <button class="btn btn-danger btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Mar', ano = '2019'">Mar</button>
                            <button class="btn btn-success btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Abr', ano = '2019'">Abr</button>
                            <button class="btn btn-success btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Mai', ano = '2019'">Mai</button>
                            <button class="btn btn-success btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Jun', ano = '2019'">Jun</button>
                            <button class="btn btn-danger btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Jul', ano = '2019'">Jul </button>
                            <button class="btn btn-danger btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Ago', ano = '2019'">Ago </button>
                            <button class="btn btn-success btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Set', ano = '2019'">Set</button>
                            <button class="btn btn-warning btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Out', ano = '2019'">Out</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Nov', ano = '2019'">Nov</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Dez', ano = '2019'">Dez</button>
                        </div>
                    </div>
                    <div class="row p-3">
                        <div class="col-1">
                            <h4> 2020 </h4>
                        </div>
                        <div class="col-11">
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Jan', ano = '2019'">Jan</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Fev', ano = '2019'">Fev</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Mar', ano = '2019'">Mar</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Abr', ano = '2019'">Abr</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Mai', ano = '2019'">Mai</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Jun', ano = '2019'">Jun</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Jul', ano = '2019'">Jul</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Ago', ano = '2019'">Ago</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Set', ano = '2019'">Set</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Out', ano = '2019'">Out</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Nov', ano = '2019'">Nov</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Dez', ano = '2019'">Dez</button>
                        </div>
                    </div>
                    <div class="row p-3">
                        <div class="col-1">
                            <h4> 2021 </h4>
                        </div>
                        <div class="col-11">
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Jan', ano = '2019'">Jan</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Fev', ano = '2019'">Fev</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Mar', ano = '2019'">Mar</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Abr', ano = '2019'">Abr</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Mai', ano = '2019'">Mai</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Jun', ano = '2019'">Jun</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Jul', ano = '2019'">Jul</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Ago', ano = '2019'">Ago</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Set', ano = '2019'">Set</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Out', ano = '2019'">Out</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Nov', ano = '2019'">Nov</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Dez', ano = '2019'">Dez</button>
                        </div>
                    </div>
                    <div class="row p-3">
                        <div class="col-1">
                            <h4> 2022 </h4>
                        </div>
                        <div class="col-11">
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Jan', ano = '2019'">Jan</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Fev', ano = '2019'">Fev</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Mar', ano = '2019'">Mar</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Abr', ano = '2019'">Abr</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Mai', ano = '2019'">Mai</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Jun', ano = '2019'">Jun</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Jul', ano = '2019'">Jul</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Ago', ano = '2019'">Ago</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Set', ano = '2019'">Set</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Out', ano = '2019'">Out</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Nov', ano = '2019'">Nov</button>
                            <button class="btn btn-primary btn-filtros" v-bind:style="tamanhoBotoesAno" v-on:click="mes = 'Dez', ano = '2019'">Dez</button>
                        </div>
                    </div>
                </div>
                <div class="col-3 border-left bg-1 p-5">
                    <h5>Legenda</h5>
                    <div class="w-100 m-3">
                        <button class="p-2 btn btn-success btn-sm mr-2"></button> Mês validado
                    </div>
                    <div class="w-100 m-3">
                        <button class="p-2 btn btn-warning btn-sm mr-2"></button> Mês corrente
                    </div>
                    <div class="w-100 m-3">
                        <button class="p-2 btn btn-danger btn-sm mr-2"></button> Mês com pendência
                    </div>
                    <div class="w-100 m-3">
                        <button class="p-2 btn btn-primary btn-filtros btn-sm mr-2"></button> Mês Planejado
                    </div>

                </div>
            </div>

            <div class="row mt-4" v-show="listaPrograma">
                <div class="col-8">
                </div>
                <div class="col-4 text-right">
                    <a href="javascript:;" @click="listaPrograma = !listaPrograma, cronograma = !cronograma, mes = '', ano = ''"><i class="fa fa-times"></i></a>
                </div>
                <div class="col-12">
                <!-- id="tableListaProgramasMes" -->
                    <table class="table table-striped w-100 " >
                        <thead>
                            <tr>
                                <th>Ações</th>
                                <th>Tipo</th>
                                <th>Programa</th>
                                <th>SubPrograma</th>
                                <th>Lotes</th>
                                <th>Status</th>
                                <th>Cronograma</th>
                                <th>% Concluído</th>
                                <th>Ativ. Previstas</th>
                                <th>Prof. Alocados</th>
                                <th>Equip. Alocados</th>
                            </tr>

                        </thead>
                        <tbody>
                           
                           
                            <tr>
                                <td>
                                    <a href="javascript:;" class="btn btn-outline-secondary btn-sm" v-on:click="idPrograma = '1', tipoPrograma='ambiental'"><i class="fa fa-eye"></i></a>
                                </td>
                                <td>Programa</td>
                                <td>Programa de Fauna</td>
                                <td>Monitoramento de Fauna</td>
                                <td>01/02 </td>
                                
                                <td>Em andamento</td>
                                <td>Adiantado</td>
                                <td>82</td>
                                <td>
                                    Atividade 1 <br />
                                    Atividade 2 <br />
                                    Pendente 20
                                </td>
                                <td>
                                    Profissional 1 <br />
                                    Profissional 2 <br />
                                </td>
                                <td>
                                    Equipamento 1 <br />
                                    Veículo 1 <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="javascript:;" class="btn btn-outline-secondary btn-sm" v-on:click="idPrograma = '1', tipoPrograma='amb-atro'"><i class="fa fa-eye"></i></a>
                                </td>
                                <td>Programa</td>
                                <td>Programa de Fauna</td>
                                <td>Monitoramento de Atropelamento de Fauna</td>
                                <td>01/02 </td>
                                
                                <td>Em andamento</td>
                                <td>Adiantado</td>
                                <td>82</td>
                                <td>
                                    Atividade 1 <br />
                                    Atividade 2 <br />
                                    Pendente 20
                                </td>
                                <td>
                                    Profissional 1 <br />
                                    Profissional 2 <br />
                                </td>
                                <td>
                                    Equipamento 1 <br />
                                    Veículo 1 <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="javascript:;" class="btn btn-outline-secondary btn-sm" v-on:click="idPrograma = '1', tipoPrograma='adm'"><i class="fa fa-eye"></i></a>
                                </td>
                                <td>Administrativo</td>
                                <td>Fechar Parceria para executar xxxxxx</td>
                                <td>-</td>
                                <td>01/02 </td>
                                
                                <td>Em andamento</td>
                                <td>Adiantado</td>
                                <td>82</td>
                                <td>
                                    Atividade 1 <br />
                                    Atividade 2 <br />
                                    Pendente 20
                                </td>
                                <td>
                                    Profissional 1 <br />
                                    Profissional 2 <br />
                                </td>
                                <td>
                                    Equipamento 1 <br />
                                    Veículo 1 <br />
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>

            <?php $this->load->view('cgmab/programas_planos_ambientais/formProgramasAmbientais.php') ?>
        </div>
    </div>
</section>