<!-- Modal -->
<div class="modal fade" id="modalAddRelatorioExecucao" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Adicionar Relatório Técnico do Programa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="alert alert-info">OBS: Selecione a atividade desejada, e logo após você pode copiar do word e colar no campo "Atividade"</div>
      <form action="#">
          <div class="row">

            <div class="input-group col-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Atividade</span>
              </div>
              <select class="form-control">
                  <option value="">Atividade x</option>
                  <option value="">Atividade y</option>
                  <option value="">Atividade z</option>
              </select>
            </div>
            <div class="input-group col-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Atividade</span>
              </div>
              <textarea class="form-control textarea"></textarea>
            </div>
            <div class="input-group col-12 mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Anexar Relatório</span>
              </div>
              <input type="file" class="form-control">
            </div>
          </div>
      </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>