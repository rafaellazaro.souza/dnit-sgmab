<!-- Modal -->
<div class="modal fade" id="modalDescricaoProgramas" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Descrição para Programa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="divFormDescPrograma">
          <form action="#">
            <div class="row">

              <div class="input-group col-6 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Descrição</span>
                </div>
                <textarea class="form-control"></textarea>
              </div>



              <div class="input-group col-12 col-sm-6 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Justificativa</span>
                </div>
                <textarea class="form-control"></textarea>
              </div>

              <div class="input-group col-12 col-sm-12 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Identificação da Localização do Programa</span>
                </div>
                <select class="form-control"></select>
              </div>

              <div class="input-group col-12 col-sm-12 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Objetivos</span>
                </div>
                <textarea class="form-control"></textarea>
              </div>

              <div class="input-group col-12 col-sm-12 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Metas</span>
                </div>

                <table class="table table-striped">
                  <thead>
                    <th>Num</th>
                    <th>Meta</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Meta 1</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Meta 2</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Meta 3</td>
                    </tr>
                  </tbody>
                </table>
                <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-plus"></i></a>
              </div>

              <div class="input-group col-12 col-sm-12 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Indicadores</span>
                </div>

                <table class="table table-striped">
                  <thead>
                    <th>Num</th>
                    <th>Indicador</th>
                    <th>Descrição</th>
                    <th>Fórmula de Cálculo</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Indicador 1</td>
                      <td>Descrição 1</td>
                      <td>Formula 1</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Indicador 2</td>
                      <td>Descrição 2</td>
                      <td>Formula 2</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Indicador 3</td>
                      <td>Descrição 3</td>
                      <td>Formula 3</td>
                    </tr>
                  </tbody>
                </table>
                <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-plus"></i></a>
              </div>

              <div class="input-group col-12 col-sm-6 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Público Alvo</span>
                </div>
                <textarea class="form-control"></textarea>
              </div>

              <div class="input-group col-12 col-sm-6 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Metodologia</span>
                </div>
                <textarea class="form-control"></textarea>
              </div>

              <div class="input-group col-12 col-sm-6 mt-4">
                <h5>Equipe Técnica: Lisa do que foi Cadastrado de:</h5>
                <div class="input-group-prepend">
                  <span class="input-group-text">Recursos Humanos</span>
                </div>
                <select class="form-control"></select>
              </div>

              <div class="input-group col-12 col-sm-6 mt-5">
                <div class="input-group-prepend">
                  <span class="input-group-text">Responsável Pelo Programa</span>
                </div>
                <select class="form-control"></select>
              </div>

              <div class="input-group col-12 col-sm-8 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Atividades Previstas</span>
                </div>
                <textarea class="form-control w-100"></textarea>

                <a href="#" class="btn btn-outline-secondary btn-sm mt-3"><i class="fa fa-plus"></i></a>
              </div>

              <div class="input-group col-12 col-sm-4 mt-4 border p-2">
                <div class="w-100" style="height: 100px ;overflow: auto">
                  <table class="table-striped w-100 p-4">
                    <h6>Check List</h6>
                    <tbody>
                      <tr>
                        <td>Ítem 1</td>
                        <td><a href="#"><i class="fa fa-times"></i></a></td>
                      </tr>
                      <tr>
                        <td>Ítem 2</td>
                        <td><a href="#"><i class="fa fa-times"></i></a></td>
                      <tr>
                        <td>Ítem 3</td>
                        <td><a href="#"><i class="fa fa-times"></i></a></td>
                      </tr>
                      <tr>
                        <td>Ítem 4</td>
                        <td><a href="#"><i class="fa fa-times"></i></a></td>
                      </tr>
                      <tr>
                        <td>Ítem 5</td>
                        <td><a href="#"><i class="fa fa-times"></i></a></td>
                      </tr>
                      <tr>
                        <td>Ítem 6</td>
                        <td><a href="#"><i class="fa fa-times"></i></a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="input-group mt-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Nome</span>
                  </div>
                  <input type="text" class="form-control"><button><i class="fa fa-plus"></i></button>
                </div>
              </div>


              <div class="input-group col-12 col-sm-12 mt-2">
                <div class="alert alert-warning m-3">
                  <h5>Delimitação do Programa</h5>
                  <p>Georeferenciamento da delimitação do Programa Ambiental(definição de sua area de influencia).
                    Arquivo do ShapeFile (o arquivo DBF deve conter os atribultos definodos no excel)</p>
                </div>
                <div class="input-group-prepend">
                  <span class="input-group-text">Arquivo Georeferenciamento</span>
                </div>
                <input type="file" class="form-control">
              </div>

            </div>
          </form>
        </div>
        <div id="divTopicos" class="invisivel"></div>
        <div id="divAddCheckList" class="invisivel"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>