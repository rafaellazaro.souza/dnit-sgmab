<?php $this->load->view('cgmab/programas_planos_ambientais/css.php'); ?>
<div id="vueApp">
    <div class="container-fluid">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{tituloPagina[0]}}</h1>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a href="<?php echo site_url('cgmab/configuracao') ?>"><i class="fa fa-times"></i></a>
                    </div>

                </div>
            </div>
        </section>

        <section class="content mb-5">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa fa-copy mr-3"></i>{{tituloPagina[1]}}</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <th>Id</th>
                            <th>Licença/ Autorização</th>
                            <th>Número</th>
                            <th>Nome</th>
                            <th>Localização</th>
                            <th>tipo</th>
                            <th>SubTipo</th>
                            <th></th>
                            <th>Descrição</th>
                            <th>Check List</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                            <tr v-for="item in listagem1">
                                <td>{{item.id}}</td>
                                <td>{{item.licenca}}</td>
                                <td>{{item.numero}}</td>
                                <td>{{item.nome}}</td>
                                <td>{{item.localizacao}}</td>
                                <td>{{item.tipo}}</td>
                                <td>{{item.subTipo}}</td>
                                <td><a href="#" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#modalDescricaoProgramas"><i class="fa fa-plus"></i></a></td>
                                <td>{{item.descricao}}</td>
                                <td>{{item.check}}</td>
                                <td>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div> <!-- /.card-body -->
        </section>
    </div>
</div>
<?php $this->load->view('cgmab/programas_planos_ambientais/modais/modalDescricaoProgramas.php'); ?>
<?php $this->load->view('cgmab/programas_planos_ambientais/scripts/index-script.php'); ?>
