<script>
    VmListagem = new Vue({
        el: '#Listagem',
        data: {
            listaDados: [],
        },
        mounted() {
            this.getDadosLista();
        },
        methods: {
            async getDadosLista() {

                var params = {
                    table: 'pessoas',
                    joinMult: [{
                                table: 'perfis',
                                on: 'CodigoPerfil'
                            },
                        ],
                }
                this.listaDados = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#tblLista', true)
            },
            async alterarStatus(codigo, status){
                var params = {
                    table: 'pessoas',
                    data: {
                        StatusPessoa: status
                    },
                    where: {
                        CodigoPessoa: codigo,
                    }
                }
                await vmGlobal.updateFromAPI(params, 'cgmab');
                VmListagem.getDadosLista();
            }

        }
    });

    var vmControle = new Vue({
        el: '#controle',
        data: {
            mostrarFormAdd: false,
            dados: {},
            listaPerfis: ''
        },
        async mounted() {
            this.getPerfis()
        },
        methods: {
            async getPerfis() {
                params = {
                    table: 'perfis',
                    // where: {
                    //     Status: 1
                    // }
                }
                this.listaPerfis = await vmGlobal.getFromAPI(params, 'cgmab');
            },
            async salvar() {
                var params = {
                    table: 'pessoas',
                    data: this.dados
                }
                await vmGlobal.insertFromAPI(params, null, 'cgmab', true);
                vmGlobal.detroyDataTable('#tblLista');
                VmListagem.getDadosLista();
                this.mostrarFormAdd = false;

            }
        }
    });
</script>