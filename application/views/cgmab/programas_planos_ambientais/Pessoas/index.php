<?php $this->load->view('cgmab/pessoas/css/css'); ?>
<div class="container-fluid">
    <section class="content-header">
        <div class="row mb-2" id="controle">
            <div class="col-sm-6">
                <h1>Usuarios</h1>
            </div>
            <div class="col-sm-6 text-right">
                <a href="<?php echo site_url('cgmab/home') ?>"><i class="fa fa-times"></i></a>
            </div>
            <div class="col-12 text-right">
                <button @click="mostrarFormAdd = !mostrarFormAdd" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Adicionar Usuario</button>
            </div>

            <div class="form-add card p-4 sombra-1" v-show="mostrarFormAdd">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Perfil</span>
                    </div>
                    <select class="form-control" v-model="dados.CodigoPerfil">
                        <option selected> Selecione </option>
                        <option :value="i.CodigoPerfil" v-for="i in listaPerfis">{{i.NomePerfil}}</option>
                    </select>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Nome</span>
                    </div>
                    <input type="text" class="form-control" v-model="dados.Nome" >
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Email</span>
                    </div>
                    <input type="text" class="form-control" v-model="dados.Email" >
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">CNPJ</span>
                    </div>
                    <input type="text" class="form-control" v-model="dados.CNPJ" >
                </div>
               
                <div class="input-group mb-3 text-right">
                    <button @click="mostrarFormAdd = false, dados = {}" class="btn btn-sm btn-outline-secondary mr-2">Cancelar</button>
                    <button @click="salvar()" class="btn btn-sm btn-secondary">Salvar</button>
                </div>
            </div>

        </div>
    </section>

    <section class="content mb-5">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-list mr-3"></i> Lista de Cadastros</h3>

                <div class="card-tools">

                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <!-- botão expandir -->
                    <button type="button" class="btn btn-tool" v-if="vmGlobal.expandSection === ''" @click="vmGlobal.expandirCards('listaLicencas') ">
                        <i class="fas fa-expand"></i></button>
                    <button type="button" class="btn btn-tool" v-else @click="vmGlobal.expandirCards('') ">
                        <i class="fas fa-expand"></i></button>
                    <!-- botão expandir -->

                </div>
            </div>
            <div class="card-body">
                <div class="row" id="Listagem">
                    <table class="table table-striped" id="tblLista">
                        <thead>
                            <tr>
                               <th>Nome</th>
                               <th>Email</th>
                               <th>Pefil</th>
                               <th>CNPJ Vinculado</th>
                               <th>Status</th>
                               <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in listaDados">
                                <td>{{i.Nome}}</td>
                                <td>{{i.Email}}</td>
                                <td>{{i.NomePerfil}}</td>
                                <td>{{i.CNPJ ? i.CNPJ : 'DNIT'}}</td>
                                <td>{{i.StatusPessoa === 1 || i.StatusPessoa === '1' ? 'Ativo' : 'Inativo'}}</td>
                                <td v-if="i.StatusPessoa === 1 || i.StatusPessoa === '1'">
                                    <button @click="alterarStatus(i.CodigoPessoa, '0')" class="btn btn-danger btn-sm" title="Desativar"><i class="fa fa-times"></i></button>
                                </td>
                                <td v-else>
                                    <button @click="alterarStatus(i.CodigoPessoa, '1')" class="btn btn-success btn-sm" title="Ativar"><i class="fa fa-check"></i></button>
                                </td>
                            </tr>
                        </tbody>

                    </table>
                </div>




            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>

</div>

<?php $this->load->view('cgmab/pessoas/scripts/app'); ?>