<div id="vueApp">
    <div class="container-fluid">
        <section class="content-header">

            <div class="row mt-4 mb-4">
                <div class="col-sm-8">
                    <h1 class="w-100">Configuração Programas Ambientais Padronizados</h1>
                </div>
                <div class="col-sm-4 text-right">
                    <a href="javascript:;" v-if="tamanhoBotoesAno != ''"><i class="fas fa-grip-lines"></i></a>
                    <a href="javascript:;" v-else><i class="fas fa-columns"></i></a>
                    <a href="<?php echo site_url('cgmab/configuracao') ?>" class="ml-3"><i class="fa fa-times"></i></a>
                </div>

            </div>

        </section>

        <div class="row">

            <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">

                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        Recursos Hídricos

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                        </div>
                    </div>
                    <div class="card-body h-450r">

                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') ?>"> Monitoramento da Qualidade da Água</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                            </div>
                        </div>



                    </div>
                    <!-- /.card-body -->

                </div>
                <!-- /.card -->

            </section>

            <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">

                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        Fauna

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                        </div>
                    </div>
                    <div class="card-body h-450r">

                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php //echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') ?>"> Monitoramento de Fauna Atropelada *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-success fa fa-check mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') ?>"> Monitoramento de Fauna</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php //echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') ?>"> Monitoramento de Passagem de Fauna *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-success fa fa-check mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php //echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') ?>"> Afugentamento e Resgate de Fauna *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-success fa fa-check mr-3"></i>
                            </div>

                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        Recursos Hídricos

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                        </div>
                    </div>
                    <div class="card-body h-450r">

                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php //echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') ?>"> Monitoramento da Qualidade da Água</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                            </div>
                        </div>



                    </div>
                    <!-- /.card-body -->

                </div>
                <!-- /.card -->

            </section>

            <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">

                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        Fauna

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                        </div>
                    </div>
                    <div class="card-body h-450r">

                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php //echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>"> Monitoramento de Fauna Atropelada *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-success fa fa-check mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') ?>"> Monitoramento de Fauna</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php //echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>"> Monitoramento de Passagem de Fauna *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-success fa fa-check mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php //echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>"> Afugentamento e Resgate de Fauna *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-success fa fa-check mr-3"></i>
                            </div>
>>>>>>> 1256b5f5978de1b6b35c7e0f845daa9d88a58bd4
                        </div>
                    </div>



                </div>
               

                <!-- /.card -->

            </section>
           

            <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">

                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        Flora

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                        </div>
                    </div>
                    <div class="card-body h-450r">

                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php // echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>"> Supressão de Vegetação *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-success fa fa-check mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php // echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>"> Resgate de Germoplasma *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-success fa fa-check mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php // echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>"> Plantio Compensatório *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-success fa fa-check mr-3"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.card-footer-->

                <!-- /.card -->

            </section>

            <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">

                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        Sociais

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                        </div>
                    </div>
                    <div class="card-body h-450r">

                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php // echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>"> Programa de Comunicação Social *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-success fa fa-check mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php // echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>"> Programa de Educação Ambiental *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-success fa fa-check mr-3"></i>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- /.card -->

            </section>
            <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">

                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        PRAD

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                        </div>
                    </div>
                    <div class="card-body h-450r">

                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php // echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>">Recuperação de Áreas Degradadas *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php // echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>"> Recuperação de Áreas das Áreas de Intervenção de Obras *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php // echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>"> Recuperação de Passivos Ambientais *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- /.card -->

            </section>
            <section class="content mb-5 col-12 col-sm-6 col-md-4 col-xl-3">

                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        Bens Culturais Acautelados

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>-->
                        </div>
                    </div>
                    <div class="card-body h-450r">

                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php // echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>">Acompanhamento Arqueológico *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                            </div>
                        </div>
                        <div class="row border mb-3 p-2 btn-conf">
                            <div class="col-10">
                                <a href="<?php // echo site_url('cgmab/programasplanosambientais/monitoramentoFauna') 
                                            ?>"> Avaliação de Potencial de Impacto ao Patrimônio Arqueológico *</a>
                            </div>
                            <div class="col-2">
                                <i class="btn btn-outline-danger fa fa-times mr-3"></i>
                            </div>
                        </div>


                    </div>

                </div>
                <!-- /.card -->

            </section>

        </div>

        <?php $this->load->view('cgmab/programas_planos_ambientais/scripts/configuracao-script.php'); ?>
