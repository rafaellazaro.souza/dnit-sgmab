      <?php $this->load->view('cgmab/ExecucaoProgramas/css/css'); ?>
      <div id="siuacaoExecucao" v-if="mostrarExecucao">
          <button class="btn btn-outline-secondary btn-sm btn-r" @click="fecharExecucao"><i class="fas fa-angle-left"></i> Voltar para lista de programas</button>
          <div class="row mt-4">
              <div class="col-lg-7 col-md-6 col-sm-12">
                  <h4>Situação de Execução de Programas</h4>
              </div>

          </div>

          <div class="row mb-4 ">

              <div class="mb-3 col-12 col-sm-8">
                  <ul class="list-group">
                      <li class="list-group-item">Condicionante: {{DadosCondicionante.Titulo}}</li>
                      <li class="list-group-item">Tipo: {{DadosCondicionante.NomeTipo}}</li>
                      <li class="list-group-item">Tema: {{DadosCondicionante.NomeTema}}</li>
                      <li class="list-group-item">SubTipo: {{DadosCondicionante.NomeSubtipo}}</li>
                  </ul>

              </div>
              <div class="mb-3 col-12 col-sm-4">
                  <ul class="list-group">
                      <li class="list-group-item">Percentual Concluído: 50%
                          <div class="progress">
                              <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                      </li>
                      <li class="list-group-item">Prazo (meses): {{DadosCondicionante.Prazo}}</li>
                      <li class="list-group-item">Recorrência : {{DadosCondicionante.Recorrencia}}</li>
                      <li class="list-group-item">
                          <button @click="vmCondicionantes.iniciarDescricao(DadosCondicionante.CodigoEscopoPrograma)" class="btn btn-outline-defaut  btn-sm">Ver Descrição do Programa</button>
                          <button @click="vmCondicionantes.iniciarCronograma(DadosCondicionante.CodigoEscopoPrograma)" class="btn btn-outline-defaut btn-sm">Ver Cronograma Físico do Programa</button>
                      </li>
                  </ul>

              </div>

              <div class="input-group mb-3 col-12 col-sm-6 col-md-6 mb-4 mt-5">
                  <div class="input-group-prepend">
                      <span id="basic-addon1" class="input-group-text">
                          Alterar Ano
                      </span>
                  </div>
                  <select class="form-control show-tick ms select2" @change="getAndamentoAtividadesPrevistasRealizadas" v-model="AnoFiltro.AnoAtividade">
                      <?php
                        $ano = date('Y');
                        $anoFim = 1980;
                        for ($i = $ano; $i >= $anoFim; $i--) { ?>
                          <option value="<?= $i ?>"><?= $i ?></option>
                      <?php } ?>
                  </select>
              </div>


              <div class="col-12 col-sm-9">
                  <table class="table table-striped mt-4">
                      <thead>
                          <tr>
                              <th>Mês</th>
                              <th>Atividades Previstas</th>
                              <th>Ativides Realizadas</th>
                              <th>Status</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr v-for="i in ListaAtividadesPrevistasRealizadas">
                              <td v-text="converterMes(parseInt(i.MesAtividade))+'/'+ i.AnoAtividade"></td>
                              <td v-text="i.Previsto"></td>
                              <td v-text="i.pendente"></td>
                              <td>
                                  <div class="btn-group">
                                      <template v-if="parseInt(i.Previsto) === parseInt(i.pendente)">
                                          <button type="button" class="btn btn-success"><i class="fa fa-check"></i></button>
                                          <button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <span class="sr-only">Mens Validado</span>
                                          </button>
                                      </template>
                                      <template v-else>
                                          <button type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                          <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <span class="sr-only">Mens com Pendencias</span>
                                          </button>
                                      </template>
                                      <div class="dropdown-menu">
                                          <a class="dropdown-item" href="#">Selecionar mês</a>
                                          <a class="dropdown-item" href="#">Parecer técnico</a>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                          <!-- <tr>
                              <td>02- Fevereiro</td>
                              <td>12</td>
                              <td>9</td>
                              <td>
                                  
                                  <div class="btn-group">
                                      <button type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                      <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          <span class="sr-only">Envio Pendente</span>
                                      </button>
                                      <div class="dropdown-menu">
                                          <a class="dropdown-item" href="#">Selecionar mês</a>
                                          <a class="dropdown-item" href="#">Solicitar aprovação</a>
                                          <a class="dropdown-item" href="#">Comunicação com fiscals</a>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                          <tr>
                              <td>03- Março</td>
                              <td>5</td>
                              <td>4</td>
                              <td>
                               
                                  <div class="btn-group">
                                      <button type="button" class="btn btn-warning"><i class="far fa-calendar-alt"></i></button>
                                      <button type="button" class="btn btn-warning dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          <span class="sr-only">Mês validado</span>
                                      </button>
                                      <div class="dropdown-menu">
                                          <a class="dropdown-item" href="#">Selecionar mês</a>
                                          <a class="dropdown-item" href="#">Solicitar aprovação</a>

                                      </div>
                                  </div>
                              </td>
                          </tr>-->

                      </tbody>
                  </table>
              </div>
              <div class="col-12 col-sm-3 border">
                  <div class="p-3">
                      <h5>Legenda</h5>
                      <div class="w-100 m-3">
                          <button class="p-2 btn btn-success btn-sm mr-2 w-10"><i class="fa fa-check"></i></button> Mês validado
                      </div>
                      <div class="w-100 m-3">
                          <button class="p-2 btn btn-warning btn-sm mr-2 w-10"><i class="far fa-calendar-alt"></i></button> Mês corrente
                      </div>
                      <div class="w-100 m-3">
                          <button class="p-2 btn btn-danger btn-sm mr-2 w-10"><i class="fa fa-times"></i></button> Mês com pendência
                      </div>
                      <div class="w-100 m-3">
                          <button class="p-2 btn btn-primary btn-filtros btn-sm mr-2 w-10"><i class="fa fa-minus"></i></button> Mês Planejado
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <?php $this->load->view('cgmab/ExecucaoProgramas/scripts/app'); ?>