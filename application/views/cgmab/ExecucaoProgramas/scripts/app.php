<script>
    let vmSituacaoExecucao = new Vue({
        el: '#siuacaoExecucao',
        data: {
            DadosCondicionante: {},
            mostrarExecucao: false,
            AnoFiltro: {
                AnoAtividade: new Date().getFullYear()
            },
            mesFiltro: new Date().getMonth(),
            ListaAtividadesPrevistasRealizadas: {},
        },
     
        async mounted() {
            await this.getAndamentoAtividadesPrevistasRealizadas();
        },
        methods: {
            fecharExecucao() {
                this.mostrarExecucao = false;
                vmCondicionantes.mostrarListaCondicionantes = true;
                vmControle.mostrarDadosContrato = true;
                vmControle.mostrarCaracterizacao = true;
            },
            async getAndamentoAtividadesPrevistasRealizadas() {
                await axios.post(base_url + 'ExecucaoProgramas/getAndamentoAtividadesPrevistasRealizadas', $.param(this.AnoFiltro))
                    .then((resp) => {
                        this.ListaAtividadesPrevistasRealizadas = resp.data;
                    })
            },
            converterMes(mes) {
            switch (mes) {
                case 1:
                    return 'Jan';
                    break;
                case 2:
                    return 'Fev';
                    break;
                case 3:
                    return 'Mar';
                    break;
                case 4:
                    return 'Abr';
                    break;
                case 5:
                    return 'Mai';
                    break;
                case 6:
                    return 'Jun';
                    break;
                case 7:
                    return 'Jul';
                    break;
                case 8:
                    return 'Ago';
                    break;
                case 9:
                    return 'Set';
                    break;
                case 10:
                    return 'Out';
                    break;
                case 11:
                    return 'Nov';
                    break;
                case 12:
                    return 'Dez';
                    break;

            }

        }
           
        }
    });
</script>