<template id="diario-form-template">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <ul class="nav nav-tabs" id="diario-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="diario-tab" data-toggle="tab" href="#form" role="tab" aria-controls="home" aria-selected="true">Formulário de Registro DA</a>
                        </li>
                        <li class="nav-item" v-if="!isEmptyDiario ">
                            <a class="nav-link" id="ocorrencia-tab" data-toggle="tab" href="#registroDA" role="tab" aria-controls="home" aria-selected="true" ref="tabOcorrencia">+ Ocorrência</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="form" role="tabpanel" aria-labelledby="diario">
                            <?php $this->load->view('cgmab/DiarioAmbiental/paginas/formularioDA.php') ?>
                        </div>
                        <div class="tab-pane fade" id="registroDA" role="tabpanel" aria-labelledby="ocorrencia-tab" v-if="!isEmptyDiario">
                            <?php $this->load->view('cgmab/DiarioAmbiental/paginas/ocorrenciaDA.php') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>