<template id="template-table-arquivos">
    <div class="col-12 mt-4">
        <div class="card" style="min-height: auto;">
            <div class="body">
                <table class="table table-striped " id="tblListaDeArquivos">
                    <thead>
                        <tr>
                            <th v-if="withOcorrencia">Tipo Ocorrencia</th>
                            <th>Arquivo</th>
                            <th>Data</th>
                            <th>Excluir</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="file in arquivos">
                            <td v-if="withOcorrencia">{{file.TipoOcorrencia}}</th>
                            <td>
                                <a :href="file.Arquivo" target="_black">
                                    {{file.Nome}}
                                </a>
                            </td>
                            <td>{{formatDataArquivo(file.DataCriacao)}}</td>
                            <td>
                                <button class="btn btn-outline-danger btn-sm" v-if="!disabled" @click="excluirArquivo(file)" type="button"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</template>