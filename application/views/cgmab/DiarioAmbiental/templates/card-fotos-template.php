<template id="template-card-fotos">
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card " :class="[!isEdit() ? 'borderGreen' : 'borderRed']">
            <div class="file">
                <div class="hover">
                    <button v-if="!disabled" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger" @click="excluirFoto()">
                        <i class="zmdi zmdi-delete"></i>
                    </button>
                </div>
                <div class="image">
                    <img :src="foto.CaminhoFoto" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
                </div>
                <div class="file-name">
                    <small v-if="withOcorrencia">Tipo de Ocorrência<span class="date text-info">{{foto.TipoOcorrencia}}</span></small>
                    <small>Latitude<span class="date text-info">{{foto.Lat}}<i v-if="isEmpty(foto.Lat)" class="text-danger fas fa-exclamation-triangle"></i></span></small>
                    <small>Longitude<span class="date text-info">{{foto.Long}}<i v-if="isEmpty(foto.Lat)" class=" text-danger fas fa-exclamation-triangle"></i></span></small>
                    <small>Data da fotografia <span class="date text-info">{{formatDataFoto(foto.DataFoto)}}</span></small>
                </div>
                <form action="" @submit.prevent="salvarFoto()">
                    <div class="file-name">
                        <label cla>UTM (Data Sirgas 2000)</label><br>
                        <div class="row">
                            <div class=" col-md-3 pr-1 pl-1">
                                <label for="email_address">Zona</label>
                                <div class="form-group">
                                    <input :disabled="disabled" class="form-control" placeholder="" v-model="foto.Zona">
                                </div>
                            </div>
                            <div class=" col-md-3 pr-1 pl-1">
                                <label for="email_address">CN</label>
                                <div class="form-group  ">
                                    <input :disabled="disabled" class="form-control" placeholder="" v-model="foto.CN">
                                </div>
                            </div>
                            <div class=" col-md-3 pr-1 pl-1">
                                <label for="email_address">CE</label>
                                <div class="form-group  ">
                                    <input :disabled="disabled" class="form-control" placeholder="" v-model="foto.CE">
                                </div>
                            </div>
                            <div class=" col-md-3 pr-0 pl-0">
                                <label for="email_address">Elevação (m)</label>
                                <div class="form-group ">
                                    <input :disabled="disabled" class="form-control" placeholder="" v-model="foto.Elevacao">
                                </div>
                            </div>
                            <div class=" col-md-12 pr-0 pl-0">
                                <label for="email_address">Observação</label>
                                <div class="form-group ">
                                    <textarea :disabled="disabled" class="form-control" v-model="foto.Observacao"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="file-name">
                        <button v-if="!disabled" class="btn btn-success" type="submit">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</template>