<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }

    .btn-rejeitado.disabled {
        background-color: #e8846d;
        color: #fff;
    }

    a.btn-status:link {
        text-decoration: underline;
    }

    a.btn-status:hover {
        text-decoration: none;
    }
</style>
<template id="ocorrencia-main-template">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="form-tab" data-toggle="tab" href="#form" role="tab" aria-controls="home" aria-selected="true">Cadastro de Ocorrência</a>
                        </li>
                        <li class="nav-item" v-if="!isEmptyOcorrencia">
                            <a class="nav-link" id="ocorrencias-info-tab" data-toggle="tab" href="#registroDA" role="tab" aria-controls="home" aria-selected="true">Informações Gerais</a>
                        </li>
                        <li class="nav-item" v-if="!isEmptyOcorrencia">
                            <a class="nav-link" id="ocorrencias-parecer-tab" data-toggle="tab" href="#parecerTecnico" role="tab" aria-controls="parecerTecnico" aria-selected="true">Parecer Técnico</a>
                        </li>
                    </ul>
                    <div class=" tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="form" role="tabpanel" aria-labelledby="mapa">
                            <div class="body">
                                <?php $this->load->view('cgmab/DiarioAmbiental/paginas/cadastroOcorrenciaForm') ?>
                                <template v-if="hasFases">
                                    <hr class="border border-secondary">
                                    <h3>Historico de Ocorrências</h3>
                                    <table class="table table-sm table-hoover">
                                        <thead>
                                            <tr>
                                                <th>Tipo de Ocorrência</th>
                                                <th>Data do Registro</th>
                                                <th>Prazo de Solução</th>
                                                <th>-</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="fase in fases">
                                                <td>{{filtraTipoOcorrencia(fase.CodigoTipoOcorrencia)}}</td>
                                                <td>{{formataData(fase.DataRegistro)}}</td>
                                                <td>{{formataData(fase.PrazoSolucao)}}</td>
                                                <td>
                                                    <button class="btn btn-sm btn-outline-secondary" @click="selectFase(fase)"><i class="zmdi zmdi-eye"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="border border-secondary p-2 mt-2" v-if="isFaseSelected">
                                        <button class="float-right btn btn-sm btn-primary" @click="isFaseSelected = false">x</button>
                                        <p><strong>Tipo de Ocorrência:</strong> {{filtraTipoOcorrencia(faseSeleted.CodigoTipoOcorrencia)}}</p>
                                        <p><strong>Descrição:</strong> {{faseSeleted.Descricao}}</p>
                                        <p><strong>Localização: </strong> {{faseSeleted.Localizacao}}</p>
                                        <p><strong>Prazo de Solução:</strong> {{formataData(faseSeleted.PrazoSolucao)}}</p>
                                        <p><strong>Status:</strong> {{filtraStatus(faseSeleted.CodigoStatus)}}</p>
                                    </div>
                                </template>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="registroDA" role="tabpanel" aria-labelledby="home-tab">
                            <informacoes-gerais :codigo-ocorrencia="CodigoOcorrencia" :em-validacao="ocorrencia.EviadoValidacao" :prazo-solucao="ocorrenciaFase.PrazoSolucao" v-if="!isEmptyOcorrencia"></informacoes-gerais>
                        </div>
                        <div class="tab-pane fade" id="parecerTecnico" role="tabpanel" aria-labelledby="parecerTecnico-tab">
                            <div class="body">
                                <h3>Histórico de Validações</h3>
                                <div class="row" v-if="showParecer">
                                    <div class="col-md-12">
                                        <button class="float-right btn btn-sm btn-primary" @click="showParecer=false">x</button>
                                        <div class="form-group mt-2">
                                            <p class="p-0 m-0"><strong>Tipo de Ocorrência:</strong>{{parecer.TipoOcorrencia}}</p>
                                            <p class="p-0 m-0"><strong>Data do Parecer:</strong>{{parecer.DataParecer}}</p>
                                            <label for="">Parecer Técnico:</label>
                                            <textarea class="form-control" rows="4" id="txtParecerTecnicoView"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-striped no-footer dataTable" id="tblParecer">
                                    <thead>
                                        <tr>
                                            <th>Tipo Ocorrência</th>
                                            <th>Data Registro</th>
                                            <th>Prazo de Solução</th>
                                            <th>Prazo Envio Validação</th>
                                            <th>Parecer</th>
                                            <th>Data Parecer</th>
                                            <th>Data Correção</th>
                                            <th>-</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="fase in parecerTecnico">
                                            <td>{{fase.TipoOcorrencia}}</td>
                                            <td>{{formataData(fase.DataRegistro)}}</td>
                                            <td>{{formataData(fase.PrazoSolucao)}}</td>
                                            <td>{{formataData(fase.DataEnvioValidacao)}}</td>
                                            <td>
                                                <a class="btn btn-sm disabled" href="javascript:void(0)" :class="fase.SituacaoClass">{{fase.Situacao}}</a>
                                            </td>
                                            <td>{{formataData(fase.DataParecer)}}</td>
                                            <td>{{formataData(fase.NovoPrazoSolucao)}}</td>
                                            <td>
                                                <button class="btn btn-sm btn-outline-secondary" @click="loadParecer(fase)"><i class="zmdi zmdi-eye"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php if ($this->session->PerfilLogin == 'Fiscal') : ?>
                                    <hr style="border: 1px solid;">
                                    <form action="" id="form-parecer" v-if="isEmValidacao && !isConcluido">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Tipo de Ocorrência:</span>
                                                    </div>
                                                    <input type="text" class="form-control dont-enable" disabled :value="parecerTipoOcorrencia">
                                                </div>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Prazo de Solução:</span>
                                                    </div>
                                                    <input type="date" class="form-control dont-enable" disabled :value="ocorrenciaFase.PrazoSolucao">
                                                </div>
                                                <div class="input-group mb-3" v-if="showInputPrazoCorrecao">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Prazo de Correção:</span>
                                                    </div>
                                                    <input type="date" class="form-control is-invalid" v-model="newParecerTecnico.NovoPrazoSolucao">
                                                    <div class="invalid-feedback">
                                                        <strong>Informe o prazo para correção, e clique em "Parcialmente Aprovado" novamente para finalizar o parecer.</strong>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <label for="">Parecer Técnico:</label>
                                                    <textarea class="form-control" rows="4" id="txtPparecerTecnico"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <br>
                                                <div class="btn-group-vertical">
                                                    <button class="btn btn-sm btn-success dont-disabled" type="button" @click="newParecer('aprovado')">Aprovado</button>
                                                    <button class=" btn btn-sm btn-danger mt-2 dont-disabled" type="button" @click="newParecer('rejeitado')">Rejeitado</button>
                                                    <button class="btn btn-sm btn-rejeitado mt-2 dont-disabled" type="button" @click="newParecer('correcao')">Parcialmente Aprovado</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>