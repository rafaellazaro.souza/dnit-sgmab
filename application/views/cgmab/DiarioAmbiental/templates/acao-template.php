<template id="acao-corretiva-template">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <form @submit.prevent="salvarAcao()">
                        <div class="row mt-3">
                            <div class="col-12 P-4">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Tipo de Ocorrência</span>
                                    </div>
                                    <input type="text" class="form-control" disabled v-model="ocorrencia.TipoOcorrencia">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Ação Corretiva</span>
                                    </div>
                                    <input type="text" class="form-control" v-model="acao.AcaoCorretiva" required>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Descrição da Ação</span>
                                    </div>
                                    <textarea type="number" class="form-control" v-model="acao.DescricaoAcao" required></textarea>
                                </div>
                                <div class="input-group mb-3">
                                    <template v-if="!isEmptyAcao">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Data de Início</span>
                                        </div>
                                        <input type="date" class="form-control" disabled v-model="acao.DataInicio">
                                    </template>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Prazo de Conclusão</span>
                                    </div>
                                    <input type="date" class="form-control" :min="ocorrencia.DataOcorrencia" :max="ocorrencia.PrazoSolucao" v-model="acao.PrazoConclusao" required>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Status</span>
                                    </div>
                                    <select class="form-control show-tick ms select2" :disabled="isEmptyAcao" v-model="acao.CodigoStatus">
                                        <option v-for="st in status" :value="st.CodigoStatus">{{st.Status}}</option>
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Indicadores</span>
                                    </div>
                                    <textarea type="text" class="form-control" v-model="acao.Indicadores"></textarea>
                                </div>
                                <div class="input-group mb-3" v-if="!isEmptyAcao">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Soluções Adotadas</span>
                                    </div>
                                    <textarea type="text" class="form-control" v-model="acao.SolucoesAdotadas" required></textarea>
                                </div>
                            </div>
                            <div class="col-12 text-right">
                                <button type="button" class="btn btn-outline-secondary" @click="cancelar()" id="cancelarAcaoCorretiva">Cancelar</button>
                                <button type="submit" id="btn-salvar-profissional" class="btn btn-secondary ml-2">Salvar</button>
                            </div>

                            <import-files v-if="!isEmptyAcao" :codigo-key="importFile.codigoKey" :codigo-value="importFile.codigoValue" @buscar-arquivos="getArquivosAndFotos()"></import-files>

                            <table-arquivos v-if="!isEmptyAcao" :arquivos="arquivos" :disabled="isEmValidacao" @buscar-arquivos="getArquivosAndFotos();"></table-arquivos>

                            <div class="col-12 container-fluid">
                                <div class="row file_manager">
                                    <template v-for="foto in fotos">
                                        <card-fotos :foto="foto" @excluir-foto="getArquivosAndFotos()" :disabled="isEmValidacao"></card-fotos>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</template>