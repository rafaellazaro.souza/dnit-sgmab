<template id="comunicacao-template">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <form @submit.prevent="salvarNotificacao()">
                        <div class="row mt-3">
                            <div class="col-12 p-4">
                                <div class="input-group mb-3" v-if="!isEmptyNotificacao">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Data da Notificação</span>
                                    </div>
                                    <input type="date" class="form-control" disabled v-model="notificacao.DataNotificacao">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Tipo de Ocorrência</span>
                                    </div>
                                    <input type="text" class="form-control" disabled v-model="ocorrencia.TipoOcorrencia">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Destinatários</span>
                                    </div>
                                    <textarea type="text" class="form-control" v-model="notificacao.Destinatarios"></textarea>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Mensagem</span>
                                    </div>
                                    <textarea type="text" class="form-control" v-model="notificacao.Mensagem"></textarea>
                                </div>
                            </div>
                            <div class="col-12 text-right">
                                <button class="btn btn-outline-secondary pt-2 pb-2" type="button" @click="cancelar()" id="cancelarNotificacao">Cancelar</button>
                                <button class="btn btn-secondary pt-2 pb-2" type="submit">Salvar</button>
                            </div>
                            <import-files v-if="!isEmptyNotificacao" :codigo-key="importFile.codigoKey" :codigo-value="importFile.codigoValue" @buscar-arquivos="getArquivosAndFotos()"></import-files>

                            <table-arquivos v-if="!isEmptyNotificacao" :arquivos="arquivos" :disabled="isEmValidacao" @buscar-arquivos="getArquivosAndFotos();"></table-arquivos>

                            <div class="col-12 container-fluid">
                                <div class="row file_manager">
                                    <template v-for="foto in fotos">
                                        <card-fotos :foto="foto" @excluir-foto="getArquivosAndFotos()" :disabled="isEmValidacao"></card-fotos>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</template>