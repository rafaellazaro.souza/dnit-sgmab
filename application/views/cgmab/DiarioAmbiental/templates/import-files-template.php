<template id="import-files-template">
    <div class="col-12">
        <div class="row">
            <div class="col-6">
                <div class="card" style="min-height: auto;">
                    <div class="header">
                        <h2><strong></strong> fotos <i class="fas fa-images"></i></h2>
                    </div>
                    <form enctype="multipart/form-data" method="POST">
                        <div class="body border border-secondary">
                            <input type="file" class="dropify" multiple ref="diarioFotos">
                        </div>
                    </form>
                    <button class="btn btn-primary float-right" type="button" @click="importarFotos()">IMPORTAR</button>
                </div>
            </div>
            <div class="col-6">
                <div class="card" style="min-height: auto;">
                    <div class="header">
                        <h2><strong></strong> Arquivos <i class="fas fa-archive"></i></h2>
                    </div>
                    <form enctype="multipart/form-data" method="POST">
                        <div class="body border border-secondary">
                            <input type="file" class="dropify" multiple ref="diarioDocs">
                        </div>
                    </form>
                    <button class="btn btn-primary float-right" type="button" @click="importarDocumentos()">Anexar</button>
                </div>
            </div>
        </div>
    </div>
</template>