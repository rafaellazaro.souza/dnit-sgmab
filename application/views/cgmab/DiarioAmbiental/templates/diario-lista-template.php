<template id="diario-lista-template">
    <div class="row p-4 mt-4">
        <div class="card">
            <div class="body">
                <div class="col-12">
                    <h4 class="w-100 mb-4 border p-3">Diário Ambiental</h4>
                    <a href="javascript:void(0);" @click="novoRegistro()">
                        <button class="btn btn-primary">Novo Registro</button>
                    </a>
                </div>
                <div class="col-12 mt-4">
                    <table class="table table-striped " id="listaDeProfissionaisNoEscopo">
                        <thead>
                            <tr>
                                <th>Código DA</th>
                                <th>BR</th>
                                <th>Ocorrências</th>
                                <th>Data</th>
                                <th>Editar</th>
                            </tr>

                        </thead>
                        <tbody>
                            <tr v-for="dir in diarios">
                                <td>{{dir.CodigoDiario}}</td>
                                <td>BR-{{dir.BR}} {{dir.UF}}</td>
                                <td>{{dir.Ocorrencias}}</td>
                                <td>{{dir.fmtDataCadastro}}</td>
                                <td>
                                    <button class="btn btn-outline-info btn-sm" type="button" @click="editarDiario(dir)"><i class="fa fa-edit"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</template>