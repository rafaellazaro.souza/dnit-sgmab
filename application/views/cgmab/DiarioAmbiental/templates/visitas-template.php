<template id="visitas-template">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    <form action="" @submit.prevent="salvarVisita()">
                        <div class="row">
                            <div class="col-12  pl-4 pr-4 pt-4">
                                <div class="input-group mb-3" v-if="!isEmptyVisita">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Data da visita</span>
                                    </div>
                                    <input type="date" class="form-control" disabled v-model="visita.DataVisita">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Tipo de Ocorrência</span>
                                    </div>
                                    <input type="text" class="form-control" disabled v-model="ocorrencia.TipoOcorrencia">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Assunto</span>
                                    </div>
                                    <input type="text" class="form-control" v-model="visita.Assunto">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Considerações e Soluções adotadas</span>
                                    </div>
                                    <textarea type="text" class="form-control" v-model="visita.SolucoesAdotadas"></textarea>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Observações</span>
                                    </div>
                                    <textarea type="text" class="form-control" v-model="visita.Observacoes"></textarea>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Participantes</span>
                                    </div>
                                    <textarea type="text" class="form-control" v-model="visita.Participantes"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <button class="btn btn-outline-secondary" type="button" @click="cancelar()" id="cancelButtonVisita">Cancelar</button>
                            <button class="btn btn-secondary pt-2 pb-2" type="submit">Salvar</button>
                        </div>
                        <import-files v-if="!isEmptyVisita" :codigo-key="importFile.codigoKey" :codigo-value="importFile.codigoValue" @buscar-arquivos="getArquivosAndFotos()"></import-files>

                        <table-arquivos v-if="!isEmptyVisita" :arquivos="arquivos" :disabled="isEmValidacao" @buscar-arquivos="getArquivosAndFotos();"></table-arquivos>

                        <div class="col-12 container-fluid">
                            <div class="row file_manager">
                                <template v-for="foto in fotos">
                                    <card-fotos :foto="foto" @excluir-foto="getArquivosAndFotos()" :disabled="isEmValidacao"></card-fotos>
                                </template>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</template>