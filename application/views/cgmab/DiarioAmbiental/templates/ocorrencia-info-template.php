<template id="ocorrencia-info-template">
    <div class="body">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link" id="form-tab" data-toggle="tab" href="#formRegistro" role="tab" aria-controls="home" aria-selected="true">Formulário de Registro</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="Acoes-tab" ref="acoes" data-toggle="tab" href="#acoesCorretivas" role="tab" aria-controls="home" aria-selected="true">Ações Corretivas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="historico-tab" ref="visitas" data-toggle="tab" href="#historico" role="tab" aria-controls="home" aria-selected="true">Histórico Visitas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="comunicacao-tab" ref="comunicacao" data-toggle="tab" href="#comunicacao" role="tab" aria-controls="home" aria-selected="true">Comunicações Construtora</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade active show" id="formRegistro" role="tabpanel" aria-labelledby="mapa">
                <?php $this->load->view('cgmab/DiarioAmbiental/paginas/formularioRegistro') ?>
            </div>
            <div class="tab-pane fade" id="acoesCorretivas" role="tabpanel" aria-labelledby="home-tab">
                <?php $this->load->view('cgmab/DiarioAmbiental/paginas/AcaoCorretivaLista') ?>
            </div>
            <div class="tab-pane fade" id="historico" role="tabpanel" aria-labelledby="home-tab">
                <?php $this->load->view('cgmab/DiarioAmbiental/paginas/HistoricoVisitasLista') ?>
            </div>
            <div class="tab-pane fade" id="comunicacao" role="tabpanel" aria-labelledby="home-tab">
                <?php $this->load->view('cgmab/DiarioAmbiental/paginas/ComunicacaoConstrutoraLista') ?>
            </div>
        </div>
    </div>
</template>