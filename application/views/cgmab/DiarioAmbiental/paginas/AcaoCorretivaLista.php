<div class="container-fluid" id="diarioAmbiental">
    <div class="row p-2 mt-2">
        <div class="col-12">
            <a href="javascript:void(0)" @click="novaAcao()" class="btn btn-primary text-white">
                Novo Registro
            </a>
        </div>
        <div class="col-12 mt-2">
            <table class="table table-striped " id="tblAcoes">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tipo de Ocorrência</th>
                        <th>Ação Corretiva</th>
                        <th>Prazo de Conclusão</th>
                        <th>Data Início</th>
                        <th>Data Fim</th>
                        <th>Status</th>
                        <th>Ações</th>
                    </tr>

                </thead>
                <tbody>
                    <tr v-for="acao in acoes">
                        <th>{{acao.CodigoAcaoCorretiva}}</th>
                        <th>{{acao.TipoOcorrencia}}</th>
                        <td>{{acao.AcaoCorretiva}}</td>
                        <td>{{formataData(acao.PrazoConclusao)}}</td>
                        <td>{{formataData(acao.DataInicio)}}</td>
                        <td>{{formataData(acao.DataFim)}}</td>
                        <td>{{getStatus(acao.CodigoStatus)}}</td>
                        <td>
                            <button class="btn btn-outline-info editButton btn-sm" @click="updateAcao(acao)"><i class="fa fa-edit"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>