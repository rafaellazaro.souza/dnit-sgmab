<form action="" @submit.prevent="salvarInfoGerais()">
    <div class="row mt-3">
        <div class="col-12">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Tipo Ocorrência</span>
                </div>
                <input type="text" class="form-control" disabled v-model="fase.TipoOcorrencia"></input>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Fiscal</span>
                </div>
                <input type="text" class="form-control" disabled v-model="diario.NomeFiscal"></input>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Dados Construtora</span>
                </div>
                <select class="form-control show-tick ms select2" v-model="info.DadosConstrutora" required>
                    <option v-for="ctr in construtoras" :value="ctr.CodigoContrutorasContratos">{{ctr.fCNPJ}} - {{ctr.NomeEmpresa}}</option>
                </select>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Modo</span>
                </div>
                <input type="text" class="form-control" v-model="diario.Modal" required disabled>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">UF</span>
                </div>
                <select class="form-control show-tick ms select2" v-model="info.UF" required>
                    <option value="">-- Selecione --</option>
                    <option v-for="uf in ufs" :value="uf.nome" v-if="!uf.nome.includes('SEDE')">{{uf.nome}}</option>
                </select>
                <template v-if="isRodoviario">
                    <div class="input-group-prepend">
                        <span class="input-group-text">BR</span>
                    </div>
                    <v-select class="form-control show-tick ms select2" placeholder="Selecione ao  menos uma BR" :disabled="isEmValidacao || isConcluido" :options="brsContrato" v-model="selectedBr" multiple="true">
                        <template #search="{attributes, events}">
                            <input class="vs__search" :required="!isSelected" v-bind="attributes" v-on="events" />
                        </template>
                    </v-select>
                </template>
            </div>
            <hr style="border: 1px solid;">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Localidade</span>
                </div>
                <textarea class="form-control" v-model="info.Localidade" required></textarea>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Latitude</span>
                </div>
                <input type="text" class="form-control" v-model="info.Lat">
                <div class="input-group-prepend">
                    <span class="input-group-text">Longitude</span>
                </div>
                <input type="text" class="form-control" v-model="info.Long">
            </div>
            <template v-if="showFields">
                <div class="input-group mb-3" v-if="isFerrovia">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Nome Ferrovia</span>
                    </div>
                    <input type="text" class="form-control" :disabled="!isFerrovia" v-model="info.NomeFerrovia">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Km Inicial</span>
                    </div>
                    <input type="number" class="form-control" v-model="info.KmInicial">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Km Final</span>
                    </div>
                    <input type="number" class="form-control" v-model="info.KmFinal">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Estaca</span>
                    </div>
                    <input type="number" class="form-control" v-model="info.Estaca">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Lado</span>
                    </div>
                    <select class="form-control show-tick ms select2" v-model="info.Lado">
                        <option value="Direito">Direito</option>
                        <option value="Esquerdo">Esquerdo</option>
                        <option value="Centro">Centro</option>
                    </select>
                </div>
            </template>
            <div class="input-group mb-3" v-if="isAquaviario">
                <div class="input-group-prepend">
                    <span class="input-group-text">Descrição AQV</span>
                </div>
                <textarea type="text" class="form-control" v-model="info.DescricaoAquaviario"></textarea>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Programas Associados</span>
                </div>
                <v-select class="form-control show-tick ms select2" placeholder="Selecione ao  menos um programa" :disabled="isEmValidacao || isConcluido" :options="programasContrato" label="TemaSubTema" v-model="selectedProgramas" multiple="true">
                </v-select>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Descrição Detalhada Ocorrência</span>
                </div>
                <textarea type="text" class="form-control" v-model="info.DescricaoOcorrencia"></textarea>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Normas ou Atividades Ambientais de Referência </span>
                </div>
                <textarea type="text" class="form-control" v-model="info.NormasAtividasAmbientaisReferencia"></textarea>
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Observações Gerais</span>
                </div>
                <textarea type="text" class="form-control" v-model="info.ObservacoesGerais"></textarea>
            </div>
        </div>

        <div class="col-12 text-right">
            <button class="btn btn-success pt-2 pb-2" type="submit">Salvar</button>
        </div>

        <import-files :codigo-key="importFile.codigoKey" :codigo-value="importFile.codigoValue" @buscar-arquivos="getArquivosAndFotos()"></import-files>

        <table-arquivos :arquivos="arquivosInfo" with-ocorrencia="true" :disabled="isEmValidacao || isConcluido" @buscar-arquivos="getArquivosAndFotos();"></table-arquivos>

        <div class="col-12 container-fluid">
            <div class="row file_manager">
                <template v-for="foto in fotos">
                    <card-fotos :foto="foto" with-ocorrencia="true" @excluir-foto="getArquivosAndFotos()" :disabled="isEmValidacao || isConcluido" v-if="foto.CodigoOcorrenciaFase == info.CodigoOcorrenciaFase"></card-fotos>
                </template>
            </div>
        </div>
    </div>
</form>

<template v-if="fases.length != 0">
    <h3>Historico de Ocorrências</h3>
    <table class="table table-sm table-hoover">
        <thead>
            <tr>
                <th>Tipo de Ocorrência</th>
                <th>Localidade</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>-</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="fase in fases">
                <td>{{fase.TipoOcorrencia}}</td>
                <td>{{fase.Localidade}}</td>
                <td>{{fase.Lat}}</td>
                <td>{{fase.Long}}</td>
                <td>
                    <button class="btn btn-sm btn-outline-secondary" @click="selectFase(fase)"><i class="zmdi zmdi-eye"></i></button>
                </td>
            </tr>
        </tbody>
    </table>

    <div class="row border border-secondary p-2 mt-2" v-if="isFaseSelected">
        <div class="col-md-6">
            <p><strong>Tipo de Ocorrência:</strong> {{infoSelect.TipoOcorrencia}}</p>
            <p><strong>ID Fiscal: </strong> {{infoSelect.Localidade}}</p>
            <p><strong>UF: </strong> {{infoSelect.UF}}</p>
            <p><strong>BR: </strong> {{infoSelect.BR}}</p>
            <p><strong>Construtora: </strong>{{returnConstrutora(infoSelect.DadosConstrutora)}}</p>
            <p><strong>Latitude:</strong>{{infoSelect.Lat}}</p>
        </div>
        <div class="col-md-6">
            <button class="float-right btn btn-sm btn-primary" @click="isFaseSelected = false">x</button>
            <p><strong>Localidade</strong> {{infoSelect.Localidade}}</p>
            <p><strong>KM Inical: </strong> {{infoSelect.KmInicial}}</p>
            <p><strong>KM Final: </strong> {{infoSelect.KmFinal}}</p>
            <p><strong>Estaca: </strong> {{infoSelect.Estaca}}</p>
            <p><strong>Lado:</strong>{{infoSelect.Lado}}</p>
            <p><strong>Longitude: </strong>{{infoSelect.Long}}</p>
        </div>
        <div class="col-md-12">
            <p><strong>Nome da Ferrovia:</strong> {{infoSelect.NomeFerrovia}}</p>
            <p><strong>Descricao do Aquaviario: </strong> {{infoSelect.DescricaoAquaviario}}</p>
            <p><strong>Descrição Ocorrênca: </strong> {{infoSelect.DescricaoOcorrencia}}</p>
            <p><strong>Normas: </strong> {{infoSelect.NormasAtividasAmbientaisReferencia}}</p>
            <p><strong>Observacoes Gerais: </strong>{{infoSelect.ObservacoesGerais}}</p>
        </div>
        <div class="col-md-12">
            <table-arquivos :arquivos="arquivosFase" with-ocorrencia="true" disabled="true"></table-arquivos>
        </div>
        <div class="col-md-12 container-fluid">
            <div class="row file_manager">
                <template v-for="foto in fotosFase">
                    <card-fotos :foto="foto" with-ocorrencia="true" disabled="true"></card-fotos>
                </template>
            </div>
        </div>
    </div>
</template>