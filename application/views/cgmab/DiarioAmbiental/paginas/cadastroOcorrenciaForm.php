    <div class="row mt-3">
        <div class="col-12">
            <form action="" @submit.prevent="salvarOcorrencia()">
                <div class=" input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Nivel do Risco</span>
                    </div>
                    <select class="form-control show-tick ms select2" v-model="ocorrencia.CodigoRisco" :disabled="!isEmptyOcorrencia">
                        <option value="">-- Selecione --</option>
                        <option v-for="rc in nivelRisco" :value="rc.CodigoRisco">{{rc.Risco}}</option>
                    </select>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Tipo Ocorrencia</span>
                    </div>
                    <select class="form-control show-tick ms select2" v-model="ocorrenciaFase.CodigoTipoOcorrencia" disabled="true">
                        <option v-for="tpo in tipoOcorrencia" :value="tpo.CodigoTipoOcorrencia">{{tpo.TipoOcorrencia}}</option>
                    </select>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Data Ocorrência</span>
                    </div>
                    <input type="date" class="form-control" :disabled="!isEmptyOcorrencia" :max="todayDate" v-model="ocorrenciaFase.DataRegistro" />
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Descrição</span>
                    </div>
                    <textarea class="form-control" v-model="ocorrenciaFase.Descricao" :disabled="isEmValidacao"></textarea>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Localização</span>
                    </div>
                    <textarea type="text" class="form-control" v-model="ocorrenciaFase.Localizacao" :disabled="isEmValidacao"></textarea>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Prazo para Solução</span>
                    </div>
                    <input type="date" class="form-control" required v-model="DtPrazoSolucao" id="prazo-conclusao" :min="minimalDate" :disabled="isEmValidacao || !isNullPrazoSolucao">
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Status</span>
                    </div>
                    <select class="form-control show-tick ms select2" v-model="ocorrenciaFase.CodigoStatus" disabled="true">
                        <option value="">-- Selecione --</option>
                        <option v-for="st in status" :value="st.CodigoStatus">{{st.Status}}</option>
                    </select>
                </div>
                <div class="col-12 text-right p-0">
                    <template v-if="!isEmptyOcorrencia">
                        <button class="btn btn-success float-left" type="button" @click="enviarParaValidacao()" v-if="!isEmValidacao && isContratada ">Enviar para Validação</button>
                    </template>
                    <button type="button" class="btn btn-outline-secondary" @click="cancelar()" id="cancelButton">Cancelar</button>
                    <button type="submit" id="btn-salvar-profissional" class="btn btn-secondary ml-2">Salvar</button>
                </div>
            </form>
        </div>
    </div>