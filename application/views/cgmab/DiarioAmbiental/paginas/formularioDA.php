<div class="" id="form-registro-da">
    <form action="" @submit.prevent="salvarDadosDA">
        <div class="row mt-3">
            <div class="col-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">UF</span>
                    </div>
                    <select class="form-control show-tick ms select2" v-model="registroDa.UF" disabled required>
                        <option value="">-- Selecione --</option>
                        <option v-for="uf in ufs" :value="uf.nome" v-if="!uf.nome.includes('SEDE')">{{uf.nome}}</option>
                    </select>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Modo</span>
                    </div>
                    <select class="form-control show-tick ms select2" v-model="registroDa.Modal" required>
                        <option value="">-- Selecione --</option>
                        <option v-for="md in modal" :value="md">{{md}}</option>
                    </select>
                </div>

                <div class="input-group mb-3" v-show="isRodoviario">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">BR</span>
                    </div>
                    <v-select class="form-control show-tick ms select2" placeholder="Selecione ao  menos uma BR" :disabled="diarioDisabled" :options="brsContrato" v-model="selectedBr" multiple="true">
                        <template #search="{attributes, events}">
                            <input class="vs__search" :required="!isSelected" v-bind="attributes" v-on="events" />
                        </template>
                    </v-select>
                </div>

                <div class="input-group mb-3" v-if="isFerrovia">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">FER</span>
                    </div>
                    <input type="text" class="form-control show-tick" min="1" v-model="registroDa.FER" required>
                </div>

                <div class="input-group mb-3" v-if="isAquaviario">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">AQV</span>
                    </div>
                    <input type="text" require min="1" class="form-control show-tick" v-model="registroDa.AQV" required>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Data do Registro</span>
                    </div>
                    <input type="date" class="form-control" :max="toDayDate" required v-model="registroDa.DataCadastro"></textarea>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Descrição detalhada Atividade</span>
                    </div>
                    <textarea type="text" class="form-control" required v-model="registroDa.Descricao"></textarea>
                </div>
            </div>

            <div class="col-12 text-right">
                <button class="btn btn-outline-secondary" type="button" @click="cancelar()" id="btnCancelarDiario">Cancelar</button>
                <button type="submit" id="btn-salvar-profissional" class="btn btn-secondary ml-2">Salvar</button>
            </div>


            <import-files v-if="!isEmptyDiario" :codigo-key="importFile.codigoKey" :codigo-value="importFile.codigoValue" @buscar-arquivos="getArquivosAndFotos()"></import-files>

            <table-arquivos v-if="!isEmptyDiario" :arquivos="arquivos" @buscar-arquivos="getArquivosAndFotos();"></table-arquivos>

            <!-- CARD FOTOS -->
            <div class="col-12 container-fluid">
                <div class="row file_manager">
                    <template v-for="foto in fotos">
                        <card-fotos :foto="foto" @excluir-foto="getArquivosAndFotos()"></card-fotos>
                    </template>
                </div>
            </div>
            <!-- fim Card fotos -->

        </div>
    </form>
</div>