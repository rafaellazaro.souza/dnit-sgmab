<div class="container-fluid" id="diarioAmbiental">
    <div class="row p-2 mt-2">
        <div class="col-12">
            <a href="javascript:void(0)" @click="novaNotificacao()" class="btn btn-primary text-white">
                Novo Registro
            </a>
        </div>
        <div class="col-12 mt-2">
            <table class="table table-striped " id="tblNotificacao">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tipo de Ocorrência</th>
                        <th>Data da Notificação</th>
                        <th>Destinatários</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="not in notificacoes">
                        <td>{{not.CodigoNotificacaoFiscal}}</td>
                        <td>{{not.TipoOcorrencia}}</td>
                        <td>{{formataData(not.DataNotificacao)}}</td>
                        <td>{{not.Destinatarios}}</td>
                        <td>
                            <button class="btn btn-outline-info btn-sm editButton" @click="updateNotificacao(not)"><i class="fa fa-edit"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>