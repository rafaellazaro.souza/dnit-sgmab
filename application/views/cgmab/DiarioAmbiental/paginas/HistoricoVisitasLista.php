<div class="container-fluid" id="diarioAmbiental">
    <div class="row p-2 mt-2">
        <div class="col-12">
            <a href="javascript:void(0)" @click="novaVisita()" class="btn btn-primary text-white">
                Novo Registro
            </a>
        </div>
        <div class="col-12 mt-2">
            <table class="table table-striped " id="tblVisitas">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Data Visita</th>
                        <th>Tipo de Ocorrência</th>
                        <th>Assunto</th>
                        <th>Considerações e Ações Adotadas</th>
                        <th>Editar</th>
                    </tr>

                </thead>
                <tbody>
                    <tr v-for="visita in visitas">
                        <td>{{visita.CodigoVisitas}}</td>
                        <td>{{formataData(visita.DataVisita)}}</td>
                        <td>{{visita.TipoOcorrencia}}</td>
                        <td>{{visita.Assunto}}</td>
                        <td>{{visita.SolucoesAdotadas}}</td>
                        <td>
                            <button class="btn btn-outline-info btn-sm editButton" @click="updateVisita(visita)"><i class="fa fa-edit"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>