<div class="container-fluid" id="diarioAmbiental">
    <div class="row p-2 mt-2">
        <div class="col-12">
            <a href="javascript:void(0);" @click="novaOcorrencia()" class="btn btn-primary waves-effect m-r-20 text-white">Nova Ocorrência</a>
        </div>
        <div class="col-12 mt-4">
            <table class="table table-striped " id="listaOcorrenciaDA">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tipo de Registro</th>
                        <th>Data do Registro</th>
                        <th>Prazo para Solução</th>
                        <th>Status</th>
                        <th>Localidade</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="ocorrencia in ocorrencias">
                        <td>{{ocorrencia.CodigoOcorrencia}}</td>
                        <td>{{ocorrencia.TipoOcorrencia}}</td>
                        <td>{{formataData(ocorrencia.DataOcorrencia)}}</td>
                        <td>{{formataData(ocorrencia.PrazoSolucao)}}</td>
                        <td>{{ocorrencia.Status}}</td>
                        <td>{{ocorrencia.Localizacao}}</td>
                        <td><button class="btn btn-outline-info btn-sm" @click="editarOcorrencia(ocorrencia)"><i class="fa fa-edit"></i></button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>