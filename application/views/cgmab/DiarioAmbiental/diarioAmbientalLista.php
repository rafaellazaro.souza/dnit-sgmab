<link rel="stylesheet" href="<?= base_url('webroot/arquivos/plugins/vue/vue-select/vue-select.min.css') ?>">
<script src="<?= base_url('webroot/arquivos/plugins/vue/vue-select/vue-select.min.js') ?>"></script>

<?php $this->load->view('cgmab/Contratos/css/cssDados'); ?>
<?php $this->load->view('cgmab/DiarioAmbiental/css/cssCadastroOcorrencia') ?>
<?php $this->load->view('cgmab/Contratos/css/cssCaracterizacao') ?>

<div class="body_scroll" id="diario-content-app">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Registro DA</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('DiarioAmbiental') ?>">Diário Ambiental</a>
                    </li>
                    <li v-for="(bread, index) in breadcrumbs" class="breadcrumb-item" :class="(bread.active)?'active':''">
                        <span>{{bread.name}}</span>
                    </li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="diarioAmbiental">
        <router-view></router-view>
    </div>
</div>

<?php $this->load->view('cgmab/DiarioAmbiental/templates/import-files-template') ?>
<?php $this->load->view('cgmab/DiarioAmbiental/templates/table-arquivos-template') ?>
<?php $this->load->view('cgmab/DiarioAmbiental/templates/card-fotos-template') ?>
<?php $this->load->view('cgmab/DiarioAmbiental/templates/diario-form-template') ?>
<?php $this->load->view('cgmab/DiarioAmbiental/templates/diario-lista-template') ?>
<?php $this->load->view('cgmab/DiarioAmbiental/templates/ocorrencia-info-template') ?>
<?php $this->load->view('cgmab/DiarioAmbiental/templates/ocorrencia-main-template') ?>
<?php $this->load->view('cgmab/DiarioAmbiental/templates/acao-template.php') ?>
<?php $this->load->view('cgmab/DiarioAmbiental/templates/visitas-template.php') ?>
<?php $this->load->view('cgmab/DiarioAmbiental/templates/comunicacao-template.php') ?>

<script src="<?= base_url('webroot/arquivos/plugins/vue/vue-router.js') ?>"></script>
<script src="<?= base_url('webroot/arquivos/js/app/diario-ambiental/import-files-component.js') ?>"></script>
<script src="<?= base_url('webroot/arquivos/js/app/diario-ambiental/table-arquivos-component.js') ?>"></script>
<script src="<?= base_url('webroot/arquivos/js/app/diario-ambiental/card-fotos-component.js') ?>"></script>
<script src="<?= base_url('webroot/arquivos/js/app/diario-ambiental/diario-component.js') ?>"></script>
<script src="<?= base_url('webroot/arquivos/js/app/diario-ambiental/ocorrencia-component.js') ?>"></script>
<script src="<?= base_url('webroot/arquivos/js/app/diario-ambiental/acoes-component.js') ?>"></script>
<script src="<?= base_url('webroot/arquivos/js/app/diario-ambiental/visitas-component.js') ?>"></script>
<script src="<?= base_url('webroot/arquivos/js/app/diario-ambiental/comunicacao-component.js') ?>"></script>
<script src="<?= base_url('webroot/arquivos/js/app/diario-ambiental/diario-router.js') ?>"></script>