<style>
	.bars {
		font-size: 24px;
		color: #17a2b8;
	}

	.bg-light {
		background-color: rgba(248, 249, 250, 0) !important;
	}

	/*menu mobile*/
	.dropdown-submenu {
		position: relative;
	}

	.dropdown-submenu>.dropdown-menu {
		top: 0;
		right: 100%;
		margin-top: -6px;
		margin-left: -1px;
		-webkit-border-radius: 0 6px 6px 6px;
		-moz-border-radius: 0 6px 6px;
		border-radius: 0 6px 6px 6px;
	}

	.dropdown-submenu:hover>.dropdown-menu {
		display: block;
	}

	.dropdown-submenu>a:after {
		display: block;
		content: " ";
		float: right;
		width: 0;
		height: 0;
		border-color: transparent;
		border-style: solid;
		border-width: 5px 0 5px 5px;
		border-left-color: #ccc;
		margin-top: 5px;
		margin-right: -10px;
	}

	.dropdown-submenu:hover>a:after {
		border-left-color: #fff;
	}

	.dropdown-submenu.pull-right {
		float: none;
	}

	.dropdown-submenu.pull-right>.dropdown-menu {
		right: -100%;
		margin-right: 10px;
		-webkit-border-radius: 6px 0 6px 6px;
		-moz-border-radius: 6px 0 6px 6px;
		border-radius: 6px 0 6px 6px;
	}

	/*menu mobile*/
	.icon-color {
		color: #17a2b8;
	}

	a {
		color: black !important;
	}

	@media (min-width: 350px) and (max-width: 992px) {
		.mobile-hide {
			display: none;
		}

		/*    menu mobile*/
		.dropdown-submenu {
			position: relative;
		}

		.dropdown-submenu>.dropdown-menu {
			top: 0;
			left: 100%;
			margin-top: -6px;
			margin-left: -1px;
			-webkit-border-radius: 0 6px 6px 6px;
			-moz-border-radius: 0 6px 6px;
			border-radius: 0 6px 6px 6px;
		}

		i .dropdown-submenu:hover>.dropdown-menu {
			display: block;
		}

		.dropdown-submenu>a:after {
			display: block;
			content: " ";
			float: right;
			width: 0;
			height: 0;
			border-color: transparent;
			border-style: solid;
			border-width: 5px 0 5px 5px;
			border-left-color: #ccc;
			margin-top: 5px;
			margin-right: -10px;
		}

		.dropdown-submenu:hover>a:after {
			border-left-color: #fff;
		}

		.dropdown-submenu.pull-left {
			float: none;
		}

		.dropdown-submenu.pull-left>.dropdown-menu {
			left: -100%;
			margin-left: 10px;
			-webkit-border-radius: 6px 0 6px 6px;
			-moz-border-radius: 6px 0 6px 6px;
			border-radius: 6px 0 6px 6px;
		}

		/*menu mobile*/
		.dropdown-menu li:hover .dropdown-menu {
			position: absolute;
			width: 188px;
		}
	}

	@media (min-width: 992px) and (max-width: 2900px) {
		.menu-mobile {
			display: none;
		}

		/*    menu mobile*/
		.dropdown-submenu {
			position: relative !important;
		}

		.dropdown-submenu>.dropdown-menu {
			top: 0;
			left: 100%;
			margin-top: -6px;
			margin-left: -1px;
			-webkit-border-radius: 0 6px 6px 6px;
			-moz-border-radius: 0 6px 6px;
			border-radius: 0 6px 6px 6px;
		}

		.dropdown-submenu:hover>.dropdown-menu {
			display: block;
		}

		.dropdown-submenu>a:after {
			display: block;
			content: " ";
			float: right;
			width: 0;
			height: 0;
			border-color: transparent;
			border-style: solid;
			border-width: 5px 0 5px 5px;
			border-left-color: #ccc;
			margin-top: 5px;
			margin-right: -10px;
		}

		.dropdown-submenu:hover>a:after {
			border-left-color: #fff;
		}

		.dropdown-submenu.pull-left {
			float: none;
		}

		.dropdown-submenu.pull-left>.dropdown-menu {
			left: -100%;
			margin-left: 10px;
			-webkit-border-radius: 6px 0 6px 6px;
			-moz-border-radius: 6px 0 6px 6px;
			border-radius: 6px 0 6px 6px;
		}

		/*menu mobile*/
		.dropdown-menu li:hover .dropdown-menu {
			position: absolute;
		}
	}

	/**/
</style>

	<ul id="menu" class="navbar-nav mobile-hide">
		<li class="nav-item btnSidemenu">
			<a style="color: silver" class="nav-link" data-widget="pushmenu" href="#"><i style="color: #17a2b8" class="fas fa-bars"></i></a>
		</li>
		<li class="nav-item" style="margin-left: 10px">
			<a title="Voltar para página inicial" style="color: black" class="nav-link" href="<?= base_url('home') ?>"><i class="fas fa-home icon-color"></i> Home</a>
		</li>
		
	</ul>

	<nav class="navbar navbar-expand-lg navbar-light bg-light menu-mobile">

		<div class="collapse navbar-collapse " id="navbarNavDropdown">
			<ul class="navbar-nav">

				<li class="nav-item dropdown">
					<a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
						<i class="fas fa-bars bars"></i>
					</a>
					<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a title="Voltar para página inicial" href="<?= base_url('home') ?>" class="dropdown-item"><i class="fa fa-home icon-color"></i> Home </a>
						<div class="dropdown-divider"></div>
					

					</ul>
				</li>
			</ul>
		</div>
	</nav>


<ul id="msg" class="navbar-nav ml-auto ">
	<li class="nav-item dropdown show">
		<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="nav-link dropdown-toggle"><?= $this->session->nome ?></a>
		<ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow ">
			<li><a href="<?= base_url('DPP/logout') ?>" class="dropdown-item"><strong>Sair</strong></a></li>
		</ul>
	</li>


	<li>
		<a href="javascritp:;" onclick="toggleFullScreen()" class="nav-link"><i class="fas fa-expand"></i></a>
	</li>


</ul>