<script>
    vmListaEmpreendimentos = new Vue({
        el: '#ListagemEmpreendimentos',
        data: {
            listaEmpreendimentos: [],
        },
        mounted() {
            this.getEmpreendimentos();
        },
        methods: {
            async getEmpreendimentos() {
                
                var params = {
                    table: 'empreendimentos',
                    where: {
                        Status: 1
                    }
                }
                this.listaEmpreendimentos = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#tblListaEmpreendimentos', true)
            },

        }
    });

    var vmControle = new Vue({
        el: '#addEmpreendimento',
        data: {

            dadosEmpreendimento: {
                Status: 1
            },
            mostrarFormEmpreendimento: false
        },
        methods: {
            async salvarEmpreendimento() {
                
                var params = {
                    table: 'empreendimentos',
                    data: this.dadosEmpreendimento
                }
                await vmGlobal.insertFromAPI(params, null, 'cgmab', true);
                vmGlobal.detroyDataTable('#tblListaEmpreendimentos');
                vmListaEmpreendimentos.getEmpreendimentos();
                this.mostrarFormEmpreendimento = false;
                
            }
        }
    });
</script>