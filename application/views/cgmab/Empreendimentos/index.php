<?php $this->load->view('cgmab/Empreendimentos/css/css'); ?>
<div class="container-fluid">
    <section class="content-header">
        <div class="row mb-2" id="addEmpreendimento">
            <div class="col-sm-6">
                <h1>Empreendimentos</h1>
            </div>
            <div class="col-sm-6 text-right">
                <a href="<?php echo site_url('cgmab/home') ?>"><i class="fa fa-times"></i></a>
            </div>
            <div class="col-12 text-right">
                <button @click="mostrarFormEmpreendimento = !mostrarFormEmpreendimento" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Adicionar Empreendimento</button>
            </div>

            <div class="form-add-empreendimento card p-4 sombra-1" v-show="mostrarFormEmpreendimento">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Nome</span>
                    </div>
                    <input type="text" class="form-control" v-model="dadosEmpreendimento.NomeEmpreendimento" aria-label="Username" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Código DNIT</span>
                    </div>
                    <input type="text" class="form-control" v-model="dadosEmpreendimento.CodigoDNIT" aria-label="Username" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Descrição</span>
                    </div>
                    <textarea v-model="dadosEmpreendimento.Descricao" class="form-control" aria-label="Username" aria-describedby="basic-addon1"></textarea>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">CD MT</span>
                    </div>
                    <input type="text" v-model="dadosEmpreendimento.Cd_MT" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Modal</span>
                    </div>
                    <select v-model="dadosEmpreendimento.Modal" class="form-control">
                        <option :value="'Aquaviario'">Aquaviário</option>
                        <option :value="'Ferroviario'">Ferroviário</option>
                        <option :value="'Rodoviario'">Rodoviário</option>
                    </select>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">UF</span>
                    </div>
                    <select v-model="dadosEmpreendimento.UF" class="form-control">
                        <option :value="'AC'">Acre</option>
                        <option :value="'AL'">Alagoas</option>
                        <option :value="'AP'">Amapá</option>
                        <option :value="'AM'">Amazonas</option>
                        <option :value="'BA'">Bahia</option>
                        <option :value="'CE'">Ceará</option>
                        <option :value="'DF'">Distrito Federal</option>
                        <option :value="'ES'">Espírito Santo</option>
                        <option :value="'GO'">Goiás</option>
                        <option :value="'MA'">Maranhão</option>
                        <option :value="'MT'">Mato Grosso</option>
                        <option :value="'MS'">Mato Grosso do Sul</option>
                        <option :value="'MG'">Minas Gerais</option>
                        <option :value="'PA'">Pará</option>
                        <option :value="'PB'">Paraíba</option>
                        <option :value="'PR'">Paraná</option>
                        <option :value="'PE'">Pernambuco</option>
                        <option :value="'PI'">Piauí</option>
                        <option :value="'RJ'">Rio de Janeiro</option>
                        <option :value="'RN'">Rio Grande do Norte</option>
                        <option :value="'RS'">Rio Grande do Sul</option>
                        <option :value="'RO'">Rondônia</option>
                        <option :value="'RR'">Roraima</option>
                        <option :value="'SC'">Santa Catarina</option>
                        <option :value="'SP'">São Paulo</option>
                        <option :value="'SE'">Sergipe</option>
                        <option :value="'TO'">Tocantins</option>
                        <option :value="'EX'">Estrangeiro</option>
                    </select>
                </div>
               
                <div class="input-group mb-3 text-right">
                    <button @click="salvarEmpreendimento()" class="btn btn-sm btn-outline-secondary">Salvar</button>
                </div>
            </div>

        </div>
    </section>

    <section class="content mb-5">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-list mr-3"></i> Lista de Empreendimentos Cadastrados</h3>

                <div class="card-tools">

                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <!-- botão expandir -->
                    <button type="button" class="btn btn-tool" v-if="vmGlobal.expandSection === ''" @click="vmGlobal.expandirCards('listaLicencas') ">
                        <i class="fas fa-expand"></i></button>
                    <button type="button" class="btn btn-tool" v-else @click="vmGlobal.expandirCards('') ">
                        <i class="fas fa-expand"></i></button>
                    <!-- botão expandir -->

                </div>
            </div>
            <div class="card-body">
                <div class="row" id="ListagemEmpreendimentos">
                    <table class="table table-striped" id="tblListaEmpreendimentos">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Descricao</th>
                                <th>Código DNIT</th>
                                <th>Código CD MT</th>
                                <th>UF</th>
                                <th>Status</th>
                                <!-- <th>Ações</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in listaEmpreendimentos">
                                <td>{{i.NomeEmpreendimento}}</td>
                                <td>{{i.Descricao}}</td>
                                <td>{{i.CodigoDNIT}}</td>
                                <td>{{i.Cd_MT}}</td>
                                <td>{{i.UF}}</td>
                                <td>{{i.Status === '1' ? 'Ativo' : 'Inativo'}}</td>
                                <!-- <td>
                                    <button onclick="return confirm('Tem Certeza que deseja desativar este registro?')" class="btn btn-sm btn-outline-danger"><i class="fa fa-times"></i></button>
                                    <button class="btn btn-sm btn-outline-primary"><i class="fa fa-pen"></i></button>
                                </td> -->
                            </tr>
                        </tbody>

                    </table>
                </div>




            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>

</div>

<?php $this->load->view('cgmab/Empreendimentos/scripts/app'); ?>