<script>
    vmAppDados = new Vue({
        el: '#dadosUsuario',
        data() {
            return {
                dadosUsuario: {},
                errors: [],
                cssCPF: 'form-control',
                codigoUsuario: <?php echo $_SESSION['Logado']['CodigoUsuario']; ?>,
            }
        },
        async mounted() {
            await this.getDadosUsuario();
        },
        methods: {
            async getDadosUsuario() {
                let params = {
                    table: 'usuariosSistema',
                    join: {
                        table: 'perfis',
                        on: 'CodigoPerfil'
                    },
                    where: {
                        CodigoUsuario: this.codigoUsuario
                    }
                }
                let dados = await vmGlobal.getFromAPI(params);
                this.dadosUsuario = dados[0];
                this.dadosUsuario.Senha = '';
            },
            async checkFormUsuario() {
                if (this.dadosUsuario.Senha.length < 5) {
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Senha inválida',
                        text: "A senha deve ter no mínimo 6 dígitos!",
                        showConfirmButton: true,

                    });
                } else {
                    this.salvar();
                }

            },
            async salvar() {
                await this.CodificarSenhaUsuario(this.dadosUsuario.Senha);
                let params = {
                    table: 'usuariosSistema',
                    data: {Senha: this.dadosUsuario.Senha},
                    where: {
                        CodigoUsuario: this.codigoUsuario
                    }
                }
                if (this.dadosUsuario.Senha != '') {
                    await vmGlobal.updateFromAPI(params, null);
                }
                this.dadosUsuario.Senha = '';

            },
            async CodificarSenhaUsuario(senha) {

                await axios.post(base_url + 'Login/codificar', $.param({
                        Senha: senha
                    }))
                    .then((resp) => {
                        this.dadosUsuario.Senha = resp.data;
                    })
            },
            validarCPF(event) {
                if (vmGlobal.validarCPF(event.target.value)) {
                    this.cssCPF = 'form-control success-color';
                    this.dadosUsuario.cpfValidado = true
                } else {
                    this.dadosUsuario.cpfValidado = false
                    this.cssCPF = 'form-control danger-color';
                }

            }
        }
    });
</script>