<script>
    const vmUsuarios = new Vue({
        el: '#usuarios',
        data: {
            listaUsuariosCadastrados: {},
            listaPerfilsUsuario: {},
            listaContratadas: {},
            dadosUsuarioCadastrado: {},
            CodigoUsuarioSelecionado: 0,
            cssCPF: 'form-control',
            mostrarAdd: false,
            dadosUsuario: {
                Senha: ''
            },
            update: false,
            errors: 0
        },
        async mounted() {
            await this.getUsuarios();
        },
        watch: {
            update: function(val) {
                delete this.dadosUsuario.Senha;
                setTimeout(() => {
                    this.validarCPF({
                        target: {
                            value: this.dadosUsuario.CPF
                        }
                    });
                }, 1000);

            }
        },
        methods: {
            async getUsuarios() {
                $("#listaUsuariosCadastrados").DataTable().destroy();
                const params = {
                    table: 'usuariosSistema',
                    join: {
                        table: 'perfis',
                        on: 'CodigoPerfil'
                    }
                }
                this.listaUsuariosCadastrados = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#listaUsuariosCadastrados')
            },
            async getPerfis() {
                const params = {
                    table: 'perfisUsuarios',
                    where: {
                        StatusPerfil: 1
                    }
                }
                this.listaPerfilsUsuario = await vmGlobal.getFromAPI(params, 'cgmab');
                this.mostrarAdd = true;
            },
            async getContratadas() {
                const params = {
                    table: 'contratada',
                    where: {
                        Status: 1
                    }
                }
                this.listaContratadas = await vmGlobal.getFromAPI(params, 'cgmab');
            },
            async insertUsuarios() {

                if (this.update === true) {
                    if (parseInt(this.dadosUsuario.CodigoPerfil) == 3 || parseInt(this.dadosUsuario.CodigoPerfil) == 1) {
                        delete this.dadosUsuario.CodigoContratada
                    }
                    this.dadosUsuario.Status = 1;
                    this.dadosUsuario.CodigoUsuario = parseInt(this.CodigoUsuarioSelecionado);
                    this.dadosUsuario.Acao = 'updated';
                }
                // Criptografar Senha
                if (this.dadosUsuario.Senha) {
                    await this.CodificarSenhaUsuario(this.dadosUsuario.Senha);
                }else{
                    delete this.dadosUsuario.Senha
                    }
                
                this.dadosUsuario.Created = new Date();
                this.dadosUsuario.Updated = new Date();
                delete(this.dadosUsuario.cpfValidado);
                await axios.post(base_url + 'Usuarios/insert', $.param(this.dadosUsuario))
                    .then((resp) => {
                        if (resp.data.status === true) {
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Salvo!',
                                text: 'Dados Inseridos com sucesso.',
                                showConfirmButton: true,
                            });
                        } else {
                            Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Salvo!',
                                text: 'Dados Inseridos com sucesso.',
                                showConfirmButton: true,
                            });
                            // Swal.fire({
                            //     position: 'center',
                            //     type: 'error',
                            //     title: 'Erro!',
                            //     text: "Erro ao Inserir os dados. Verifique os dados",
                            //     showConfirmButton: true,

                            // });
                        }
                    }).finally(() => {
                        Swal.fire({
                                position: 'center',
                                type: 'success',
                                title: 'Salvo!',
                                text: 'Dados Inseridos com sucesso.',
                                showConfirmButton: true,
                            });

                            window.location.reload();
                        // this.getUsuarios();
                        // this.mostrarAdd = false;
                     
                        // this.cssCPF = 'form-control';
                        // this.errors = [];
                    })

            },
            async viewUsuario() {
                const params = {
                    table: 'usuariosSistema',
                    where: {}
                }
                this.dadosUsuarioCadastrado = await vmGlobal.getFromAPI(params, 'cgmab')
            },
            async desativarUsuario(status) {
                const params = {
                    table: 'usuariosSistema',
                    data: {
                        Status: status === false || status === 'false' ? 1 : 0
                    },
                    where: {
                        CodigoUsuario: this.CodigoUsuarioSelecionado
                    }
                }
                await vmGlobal.updateFromAPI(params, null, 'cgmab');
                await this.getUsuarios();

            },
            async checkFormUsuario() {
                this.errors = [];
                if (this.update) {
                    if (this.dadosUsuario.CodigoPerfil && this.dadosUsuario.NomeUsuario && this.dadosUsuario.Email && parseInt(this.dadosUsuario.CPF) && parseInt(this.dadosUsuario.RG) && this.dadosUsuario.cpfValidado) {
                        if (this.errors == 0) {
                            await this.insertUsuarios();
                        }

                    }
                    if (!this.dadosUsuario.CodigoPerfil) {
                        vmUsuarios.errors.push('O Perfil é obrigatório.');
                    }
                    if (!this.dadosUsuario.NomeUsuario) {
                        vmUsuarios.errors.push('Nome do Usuario é obrigatória.');
                    }
                    if (!this.dadosUsuario.Email) {
                        vmUsuarios.errors.push('Email é obrigatória.');
                    }
                    if (!this.dadosUsuario.CPF) {
                        vmUsuarios.errors.push('CPF é obrigatório.');
                    }
                    if (!this.dadosUsuario.RG) {
                        vmUsuarios.errors.push('RG é obrigatório.');
                    }
                    if (!this.dadosUsuario.cpfValidado) {
                        vmUsuarios.errors.push('CPF incorreto');
                    }
                } else {
                    if (this.dadosUsuario.CodigoPerfil && this.dadosUsuario.NomeUsuario && this.dadosUsuario.Email && this.dadosUsuario.Senha && parseInt(this.dadosUsuario.CPF) && parseInt(this.dadosUsuario.RG) && this.dadosUsuario.cpfValidado) {
                        if (this.errors == 0) {
                            await this.insertUsuarios();
                        }

                    }



                    if (!this.dadosUsuario.CodigoPerfil) {
                        vmUsuarios.errors.push('O Perfil é obrigatório.');
                    }
                    if (!this.dadosUsuario.NomeUsuario) {
                        vmUsuarios.errors.push('Nome do Usuario é obrigatória.');
                    }
                    if (!this.dadosUsuario.Email) {
                        vmUsuarios.errors.push('Email é obrigatória.');
                    }
                    if (!this.dadosUsuario.Senha) {
                        vmUsuarios.errors.push('A Senha é obrigatória.');
                    }
                    if (!this.dadosUsuario.CPF) {
                        vmUsuarios.errors.push('CPF é obrigatório.');
                    }
                    if (!this.dadosUsuario.RG) {
                        vmUsuarios.errors.push('RG é obrigatório.');
                    }
                    if (!this.dadosUsuario.cpfValidado) {
                        vmUsuarios.errors.push('CPF incorreto');
                    }
                }





            },
            async CodificarSenhaUsuario(senha) {

                await axios.post(base_url + 'Login/codificar', $.param({
                        Senha: senha
                    }))
                    .then((resp) => {
                        this.dadosUsuario.Senha = resp.data;
                    })
            },
            validarCPF(event) {
                if (vmGlobal.validarCPF(event.target.value)) {
                    this.cssCPF = 'form-control success-color';
                    this.dadosUsuario.cpfValidado = true
                } else {
                    this.dadosUsuario.cpfValidado = false
                    this.cssCPF = 'form-control danger-color';
                }

            }
        }
    });
</script>