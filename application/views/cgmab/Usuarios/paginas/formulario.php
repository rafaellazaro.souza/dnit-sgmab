<div class="modal-w98-h50 sombra-2 p-4" v-if="mostrarAdd">
    <button @click="dadosUsuario = {}; mostrarAdd = false" class="btn btn-secondary btn-r mb-2"><i class="fa fa-times"></i></button>
    <h4 class="col-12 mb-4 mt-4 text-left">Formulário Gestão Usuários</h4>
    <div class="row p-4">
        <div class="input-group col-6 ml-0 mb-3">
            <div class="input-group-prepend">
                <span id="basic-addon1" class="input-group-text">Perfil</span>
            </div>
            <select v-model="dadosUsuario.CodigoPerfil" class="form-control show-tick ms select2">
                <option value="" selected>Selecione o pefil</option>
                <option :value="i.CodigoPerfil " v-for="i in listaPerfilsUsuario" v-text="i.NomePerfil"></option>
            </select>
        </div>
        <div class="input-group col-6 ml-0 mb-3" v-if="dadosUsuario.CodigoPerfil === 2|| dadosUsuario.CodigoPerfil === '2'">
            <div class="input-group-prepend ml-2">
                <span id="basic-addon1" class="input-group-text">Empresa/ Órgão</span>
            </div>
            <select v-model="dadosUsuario.CodigoContratada" class="form-control show-tick ms select2">
                <option :value="i.CodigoContratada " v-for="i in listaContratadas" v-text="i.NomeContratada" v-if="dadosUsuario.CodigoPerfil === 2|| dadosUsuario.CodigoPerfil === '2'"></option>
            </select>
        </div>
        <div class="input-group col-12 mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Nome</span>
            </div>
            <input v-model="dadosUsuario.NomeUsuario" type="text" class="form-control">
        </div>
        <div class="input-group col-12 col-sm-6 mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">E-mail</span>
            </div>
            <input v-model="dadosUsuario.Email" type="email" class="form-control">
        </div>
        <div class="input-group col-12 col-sm-6 mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Telefone</span>
            </div>
            <input v-model="dadosUsuario.Telefone" type="tel" class="form-control">
        </div>
        <div class="input-group col-12 col-sm-6 mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">CPF (apenas números)</span>
            </div>
            <input @keyup="validarCPF($event)" v-model="dadosUsuario.CPF" :value="dadosUsuario.CPF" id="cpf-usuario" type="int" :class="cssCPF">
        </div>
        <div class="input-group col-12 col-sm-6 mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">RG</span>
            </div>
            <input v-model="dadosUsuario.RG" type="text" class="form-control">
        </div>
   
        <div class="input-group col-12 col-sm-6 mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Senha</span>
            </div>
            <input v-model="dadosUsuario.Senha" type="password" class="form-control">
        </div>

        <div class="alert alert-warning p-4 text-left" v-show="errors.length > 1">
            <h5>Verifique se os campos abaixo foram preenchidos!</h5>
            <ul class="mt-4">
                <li v-for="i in errors" v-text="i"></li>
            </ul>
        </div>

        <div class="w-100 border-top mt-5 text-right">
            <button @click="dadosUsuario = {}; mostrarAdd = false; errors= 0" class="btn btn-outline-secondary mr-2"> Cancelar</button>
            <button @click="checkFormUsuario" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
        </div>
    </div>
</div>