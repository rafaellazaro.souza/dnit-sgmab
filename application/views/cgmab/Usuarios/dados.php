<div class="body_scroll">
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Usuários</h2>
                <ul class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Usuários</li>
                    <li class="breadcrumb-item active">Dados</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid" id="dadosUsuario">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                    </div>
                    <div class="body">
                        <h5>Dados do usuário</h5>
                        <div class="row p-4">
                          
                            <div class="input-group col-12 mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Nome (*)</span>
                                </div>
                               <div class="form-control">{{dadosUsuario.NomeUsuario}}</div>
                            </div>
                            <div class="input-group col-12 col-sm-6 mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Email (*)</span>
                                </div>
                                <div class="form-control">{{dadosUsuario.Email}}</div>
                            </div>
                            <div class="input-group col-12 col-sm-6 mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Telefone (*)</span>
                                </div>
                                <div class="form-control">{{dadosUsuario.Telefone}}</div>
                            </div>
                            <div class="input-group col-12 col-sm-6 mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">CPF (*)</span>
                                </div>
                                <div class="form-control">{{dadosUsuario.CPF}}</div>
                            </div>
                            <div class="input-group col-12 col-sm-6 mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RG (*)</span>
                                </div>
                                <div class="form-control">{{dadosUsuario.RG}}</div>
                                
                            </div>

                            <div class="input-group col-12 col-sm-6 mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Senha</span>
                                </div>
                                <input v-model="dadosUsuario.Senha" type="password" class="form-control">
                            </div>
                            <div class="alert alert-warning p-4 text-left" v-show="errors.length > 1">
                                <h5>Verifique se os campos abaixo foram preenchidos!</h5>
                                <ul class="mt-4">
                                    <li v-for="i in errors" v-text="i"></li>
                                </ul>
                            </div>
    <div class="p-4 col-12">
        <p>(*) campos bloqueados para edição</p>
    </div>
                            

                            <div class="w-100 border-top mt-5 text-right">
                                <button @click="dadosUsuario = {}; mostrarAdd = false" class="btn btn-outline-secondary mr-2"> Cancelar</button>
                                <button @click="checkFormUsuario" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('cgmab/Usuarios/scripts/appDados');
?>