<div class="body_scroll" id="usuarios">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Gestão de Usuários</h2>
                <ul class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Usuários</a></li>
                    <li class="breadcrumb-item active">Gestão de Usuários</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header text-right">
                        <button @click="getPerfis(); getContratadas(); errors={}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Adicionar Novo</button>
                        <!-- =============================== 
                                modal formulario Add Usuario  
                        =============================== -->
                        <?php $this->load->view('cgmab/Usuarios/paginas/formulario.php');?>
                        <!-- =============================== 
                                Fim do modal formulario Add Usuario  
                        =============================== -->
                    </div>
                    <div class="body">
                        <h5>Lista de Usuários Cadastrados</h5>
                        <table class="table table-striped" id="listaUsuariosCadastrados">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Perfil</th>
                                    <th>Status</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="i in listaUsuariosCadastrados">
                                    <td v-text="i.NomeUsuario"></td>
                                    <td v-text="i.Email"></td>
                                    <td v-text="i.NomePerfil"></td>
                                    <td v-text="i.Status === 1 || i.Status === '1' ? 'Ativo' : 'Inativo'"></td>
                                    <td>
                                        <button @click="update = true; mostrarAdd = true;CodigoUsuarioSelecionado = i.CodigoUsuario; dadosUsuario = i;getPerfis();getContratadas(); errors = 0" class="btn btn-info btn-sm" title="Ver/ Editar"><i class="fa fa-pen"></i></button>
                                        <button @click="CodigoUsuarioSelecionado = i.CodigoUsuario; desativarUsuario(true);" v-if="i.Status === 1 || i.Status === '1'" class="btn btn-danger btn-sm" title="Desativar"><i class="fa fa-minus"></i></button>
                                        <button @click="CodigoUsuarioSelecionado = i.CodigoUsuario; desativarUsuario(false);" v-else class="btn btn-success btn-sm" title="Ativar"><i class="fa fa-check"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('cgmab/Usuarios/scripts/appIndex');?>