<?php $this->load->view('cgmab/Condicionantes/css/css'); ?>

<!-- css mapa  -->
<?php $this->load->view('../../webroot/arquivos/plugins/leatleft/css/leatleft.php') ?>
<!-- js mapa  -->
<?php $this->load->view('../../webroot/arquivos//plugins/leatleft/js/leatleft.php') ?>
<!-- css local -->

<style>
	.table-responsive {
		height: 300px;
		overflow: auto;
		width: 100%;
	}

	.btn-rejeitado {
		background-color: #e8846d;
		color: #fff;
	}

	.no-shadow {
		box-shadow: 0px 0px 0px transparent !important;
	}

	#div1 {
		width: 100px;
		height: 100px;
		border: solid 1px;
		border-radius: 0px 20px 0px 20px;
	}
</style>
<div class="body_scroll">
	<div class="block-header">
		<div class="row">
			<div class="col-lg-7 col-md-6 col-sm-12">
				<h2>Configuração de Supressão de Vegetação</h2>
				<ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
					<li class="breadcrumb-item active">Home</li>
					<li class="breadcrumb-item active">Contratos</li>
					<li class="breadcrumb-item active">Dados do Contrato</li>
					<li class="breadcrumb-item active">Execução de Programas Ambientais - Lista de Condicionantes</li>
				</ul>
				<button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12">
				<div class="card" id="confSupressaoVegetacao">
					<div class="no-shadow body">
						<div class="row">
							<div class="col-md-9">
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" id="supressao-tab" data-toggle="tab" href="#supressao-vegetacao" role="tab" aria-controls="supressao-tab" aria-selected=" true">Passagem de Fauna</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="parecer-tab" data-toggle="tab" href="#parecer-fiscal" role="tab" aria-controls="parecer-tab" aria-selected="true">Parecer</a>
									</li>
								</ul>
							</div>
							<div class="col-md-3 d-flex align-items-center flex-row-reverse">
								<button class="btn btn-sm btn-success d-flex align-items-center" v-if="!isEmValidacaoOuAprovado && !isFiscal" @click="solicitarValidacao()">Enviar para Validação</button>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="tab-content" id="myTabContent">
									<div class="tab-pane fade show active" id="supressao-vegetacao" role="tabpanel" aria-labelledby="passagem-tab">
										<div class="row mb-1">
											<div class="col-12">
												<h4 class="w-100 mb-4 border p-3">Supressão de Vegetação - ASV</h4>
											</div>
										</div>
										<button @click="novaLicenca()" class="btn btn-outline-success float-right btn-lg" v-if="!isEmValidacaoOuAprovado">Adicionar Licença</button>
										<div class="row mb-4">
											<div class="col-12 col-sm-8">
												<div class="input-group mb-3">
													<div class="input-group-prepend">
														<span class="input-group-text" id="basic-addon1">Filtrar p/ Licença</span>
													</div>
													<input type="text" class="form-control" v-model="dadoBusca" placeholder="Filtrar pela licença. Digite o número ou ano aqui!">
													<button @click="getLicencas()" class="btn btn-outline-secondary ml-3" v-if="dadoBusca.length > 3">Buscar</button>
													<div class="lista-licencas sombra-2" v-show="mostarResultados">
														<ul class="list-group mb-4 ">
															<li class="list-group-item bg-1">Selecione a Licença <i @click="mostarResultados = !mostarResultados" class="fa fa-times btn-r mr-3"></i></li>
															<button @click="setLicenca(i), mostarResultados = false, codigoLicenca = i.CodigoLicenca" type="button" class="list-group-item list-group-item-action" v-for="i in resultadoFiltroNumero">{{i.NumeroLicenca}} - {{i.Sigla}} - {{i.NomeOrgaoExpeditor}}</button>
														</ul>
													</div>
												</div>
											</div>
										</div>

										<div class="table-responsive">
											<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
												<thead class="thead-dark">
													<tr>
														<th>N°</th>
														<th>Número ASV</th>
														<th>Órgão Expeditor</th>
														<th>Data de Emissão</th>
														<th>Data de Validade</th>
														<th>Volume m³</th>
														<th>Área em APP(ha)</th>
														<th>Área fora APP(ha)</th>
														<th>Área Total(ha)</th>
														<th>Shapefile</th>
														<th>Ações</th>
													</tr>
												</thead>
												<tbody>
													<tr v-for="(i, index) in listaLicencasAmbientais">
														<td>{{index + 1}}</td>
														<td>{{i.NumeroLicenca}}</td>
														<td>{{i.NomeOrgaoExpeditor}}</td>
														<td>{{i.DataEmissao}}</td>
														<td>{{i.DataVencimento}}</td>
														<td>{{i.Volume}}</td>
														<td>{{i.AreaEmApp}}</td>
														<td>{{i.AreaForaApp}}</td>
														<td>{{i.AreaTotal}}</td>
														<td>
															<template v-if="!isEmValidacaoOuAprovado">
																<button v-if="!i.Coordenada" @click="openModal(i.CodigoLicenca)" class="btn btn-outline-info btn-sm"><i class="fa fa-upload"> </i> Adicionar Shapefile</button>
																<button v-else @click="deletarShapefile(i.CodigoLicenca)" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash-o"> </i> Excluir Shapefile</button>
															</template>
														</td>
														<td>
															<button v-if="!isEmValidacaoOuAprovado" @click="deletar(i.CodigoFloraSupressao)" class="btn btn-outline-danger btn-sm"><i class="fa fa-times"></i></button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>

										<div class="modal-w98-h50 sombra-2 p-4 w-50 h-50" v-if="mostrarModal">
											<button @click="mostrarModal = false" class="btn btn-secondary btn-r"><i class="fa fa-times"></i></button>
											<h4>Adicionar Shapefile</h4>
											<div class="input-group p-3">
												<div class="custom-file">
													<input type="file" ref="ShapeFile" name="ShapeFile" class="custom-file-input" accept=".zip" id="ShapeFile">
													<label class="custom-file-label" id="labelShapeFile" for="ShapeFile">Selecione um arquivo ZIP</label>
												</div>

												<div class="input-group-append">
													<button @click="addShapefile(idCodigoLicenca)" class="btn btn-outline-secondary" type="button"><span class="spinnerShapefile"></span> Importar</button>
												</div>
											</div>
										</div>
										<div class="row">
											<div class=" mb-5 mt-2 p-0" :class="vmGlobal.expandSection === 'mapa' ? 'card full-card' : secaoDadosDaLicenca ? 'col-8' : 'col-12'" v-show="secaoMapa">
												<div class="p-0" :class="expandMapa ">
													<div class="border border-secondary" id="mapid" style="height: 600px; width: 100%"></div>
												</div>
											</div>
											<!-- dados da licença  -->
											<div class="mb-5 mt-2 " :class="secaoDadosDaLicenca ? 'col-4' : ''" v-if="secaoDadosDaLicenca">
												<div class="card border border-dark">
													<div class="card-header">
														<h3 class="card-title"><i class="fa fa-th-list mr-3"></i> Dados da Licença</h3>
														<div class="card-tools">
															<button type="button" class="btn btn-tool" @click="secaoDadosDaLicenca = !secaoDadosDaLicenca">
																<i class="fas fa-times"></i></button>
														</div>
													</div>

													<div class="card-body p-4">
														<ul class="list-group">
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Número / Autorização:</span> {{dadosDaLicensa[0].NumeroLicenca}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">{{dadosDaLicensa[0].TipoVia}}:</span> {{dadosDaLicensa[0].VIA}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">UF:</span> {{dadosDaLicensa[0].UF}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Modal:</span> {{dadosDaLicensa[0].Modal}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Situação:</span> {{dadosDaLicensa[0].SituacaoAtual ? dadosDaLicensa[0].SituacaoAtual : 'Não Informado'}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Status:</span> {{dadosDaLicensa[0].Status}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Km Inicial:</span> {{dadosDaLicensa[0].KmInicial}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Km Inicial:</span> {{dadosDaLicensa[0].KmFinal}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Extensão:</span> {{dadosDaLicensa[0].Extensao}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Início do Sub Trecho:</span> {{dadosDaLicensa[0].InicioDoSubTrecho}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Fim do Sub Trecho:</span> {{dadosDaLicensa[0].FimDoSubTrecho}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Licenciamento/ Autorizacao:</span> {{dadosDaLicensa[0].LicenciamentoAutorizacao}}</li>
															<li class="list-group-item" v-for="i in dadosDaLicensa"><span class="font-weight-bold">Link Documento da Licença:</span> <a class="btn btn-sm btn-outline-secondary" target="_blank" :href="(dadosDaLicensa[0].UsarLincencaAnexo == '1')? dadosDaLicensa[0].NomeLicencaAnexo : dadosDaLicensa[0].NomeArquivoLicencas">Exibir</a></li>

														</ul>
													</div>


												</div>
											</div>
											<!-- fim dados da licença  -->

										</div>
									</div>
									<div class="tab-pane fade" id="parecer-fiscal" role="tabpanel" aria-labelledby="parecer-tab">
										<?php $this->load->view('cgmab/ConfiguracaoProgramas/paginas/parecerTab.php') ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('cgmab/ConfiguracaoSupressaoVegetacao/scripts/appSupressaoVegetacao.php'); ?>