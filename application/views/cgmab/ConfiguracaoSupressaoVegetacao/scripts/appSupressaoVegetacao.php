<script>
	var parametrosEscopo = location.search.slice(1);
	var codigoEscopoPrograma = vmGlobal.codificarDecodificarParametroUrl(parametrosEscopo.split('&')[0].split('=')[1], 'decode');
	var numeroContrato = vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[1].split('=')[1], 'decode');


	var vmConfiguracaoSupressaoVegetacao = new Vue({
		el: "#confSupressaoVegetacao",
		data() {
			return {
				dadoBusca: '',
				resultadoFiltroNumero: [],
				mostarResultados: false,
				mostrarModal: false,
				listaLicencasAmbientais: [],
				codigoLicencaTipo: 6, //ASV => tblLicencasTipo
				idCodigoLicenca: 0,
				dadosDaLicensa: [],
				coordenadasShapeFile: {},
				secaoDadosDaLicenca: false,
				mymap: null,
				layer: null,
				secaoMapa: true,
				expandMapa: '',
				status: {
					CodigoConfiguracaoProgramasStatus: '',
					CodigoEscopoPrograma: '',
					Status: 1,
					DataEnvio: '',
					PrazoCorrecao: ''
				},
				parecer: {
					CodigoParecerFiscal: '',
					CodigoFiscal: '',
					CodigoEscopoPrograma: '',
					StatusFiscal: '',
					Parecer: '',
					DataEnvio: '',
					DataParecer: '',
					AntigaDataCorrecao: null,
					PrazoCorrecao: ''
				},
				listaParecer: [],
				selectedParecer: {},
				usuario: {
					NomeUsuario: '',
					CodigoUsuario: '',
					Perfil: ''
				},
				showCorrecao: false,
				showParecer: false,
				ParecerFiscal: ''
			}
		},
		watch: {
			secaoDadosDaLicenca: function(val) {
				this.secaoDadosDaLicenca = val;
				this.getLi
			},
			'usuario.Perfil'(val) {
				if (this.habilitarParecer) {

					this.$nextTick(() => {
						CKEDITOR.replace('textParecer');

						CKEDITOR.instances['textParecer'].on("change", (evt) => {
							this.ParecerFiscal = evt.editor.getData();
						});
					})
				}
			},
			'usuario.CodigoUsuario'(val) {
				this.parecer.CodigoFiscal = val;
			},
			ParecerFiscal(val) {
				this.parecer.Parecer = String(btoa(val));
			},
			'status.DataEnvio'(val) {
				if (val) {
					this.parecer.DataEnvio = moment(val).format('YYYY-MM-DD');
				}
			},
			'status.PrazoCorrecao'(val) {
				if (val) {
					this.parecer.AntigaDataCorrecao = moment(val).format('YYYY-MM-DD');
				}
			},
		},
		computed: {
			toDayDate() {
				return moment().format('YYYY-MM-DD');
			},
			habilitarParecer() {
				if (this.isFiscal && this.status.Status == 2) {
					return true;
				} else if (this.isFiscal &&
					this.status.Status == 5 &&
					this.contratoAtrazado(this.status.PrazoCorrecao)) {
					return true;
				}
				return false;
			},
			isFiscal() {
				return this.usuario.Perfil.includes('Fiscal');
			},
			isEmValidacaoOuAprovado() {
				if (this.isFiscal) {
					return false;
				} else if (this.isEmAtrazo) {
					return true;
				}
				return (this.status.Status == 2 || this.status.Status == 4);
			},
			isEmAtrazo() {
				return this.contratoAtrazado(this.status.PrazoCorrecao);
			}
		},
		async mounted() {
			this.gerarMapa();
			await this.getConfiguracaoLicencas();
			await this.getStatus();
			await this.getParecer();
		},

		filters: {

		},


		methods: {
			setsecaoDadosDaLicenca(val) {
				this.secaoDadosDaLicenca = val;
			},

			gerarMapa() {
				const style = 'reduced.night';
				this.mymap = L.map('mapid').setView(['-16.7646342', '-61.3179296'], 5.34);
				L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
					maxZoom: 15,
					// attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
					//     '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
					id: 'mapbox.streets',
					fadeAnimation: true,
				}).addTo(this.mymap);
				this.layer = L.geoJSON().addTo(this.mymap);
			},

			aplicarLayer(coord, objectid, color, layer) {
				var array = {
					"type": "FeatureCollection",
					"features": [{
						"type": "Feature",
						"geometry": JSON.parse(coord),
						"properties": {
							"OBJECTID": objectid,

						}
					}]
				}
				L.geoJson(array, {
					style: function(feature) {
						return {
							stroke: true,
							color: color,
							weight: 3
						};
					},
					onEachFeature: function(feature, l) {
						l.bindPopup(feature.properties.OBJECTID);

						l.on('click', function(e) {
							l.setStyle({
								weight: 8,
								outline: 'red'
							});
						});

						l.on("popupclose", function(e) {
							l.setStyle({
								weight: 3,
							});
						});
					}
				}).addTo(this.layer);
			},

			async getConfiguracaoLicencas() {
				//Lista as licenças que estão na configuração
				let params = {
					table: 'licencas',
					joinLeft: [{
							table1: 'licencas',
							table2: 'floraSupressaoConfig',
							on: 'CodigoLicenca',
						},
						{
							table1: 'licencas',
							table2: 'orgaosExpeditores',
							on: 'CodigoOrgaoExpeditor',
						}
					],
				}
				this.listaLicencasAmbientais = await vmGlobal.getFromAPI(params, 'cgmab');
				this.getDadosMapa();
			},

			async getLicencas() {
				//Lista licenças para selecionar na configuração
				let params = {
					table: 'licencas',
					joinLeft: [{
							table1: 'licencas',
							table2: 'orgaosExpeditores',
							on: 'CodigoOrgaoExpeditor',
						},
						{
							table1: 'licencas',
							table2: 'tiposLicencas',
							on: 'CodigoLicencaTipo',

						},
					],
					where: {
						CodigoLicencaTipo: this.codigoLicencaTipo
					},
					like: {
						NumeroLicenca: this.dadoBusca,
					}
				}
				this.resultadoFiltroNumero = await vmGlobal.getFromAPI(params, 'cgmab');
				this.mostarResultados = true;
			},

			getDadosMapa() {
				this.layer.clearLayers();
				// var data = this.listaLicencasAmbientais[0].Coordenada;
				let numeroLicencas = this.listaLicencasAmbientais;

				for (let i = 0; i < numeroLicencas.length; i++) {

					let data = numeroLicencas[i].Coordenada;

					this.coordenadasShapeFile = JSON.parse(data);
					let concatenado = '<div class="text-left" style="color: #888"><h6>INFORMAÇÕES DA LICENÇA</h6> <br>' +
						'<b>Numero da Licença</b>: ' + this.listaLicencasAmbientais[i].NumeroLicenca + '<br>' +
						'<b>Órgão Expeditor</b>: ' + this.listaLicencasAmbientais[i].NomeOrgaoExpeditor + '<br>' +
						'<b>Data de Emissão</b>: ' + this.listaLicencasAmbientais[i].DataEmissao + '<br>' +
						'<b>Data de Validade</b>: ' + this.listaLicencasAmbientais[i].DataVencimento + '<br>' +
						'<button class="mt-4 btn btn-sm btn-outline-primary" onclick="abrirInformacoesLicens(' + this.listaLicencasAmbientais[i].CodigoLicenca + ')">Ver Detalhes</button>';

					const corLayer = '#00008B';
					if (this.coordenadasShapeFile != null) {
						if (Array.isArray(this.coordenadasShapeFile)) {
							for (let index = 0; index < this.coordenadasShapeFile.length; index++) {
								let cd = this.coordenadasShapeFile[index].coordenada

								this.aplicarLayer(cd, concatenado, corLayer, this.layer);
							}
						} else {
							let cd = numeroLicencas[i].Coordenada
							this.aplicarLayer(cd, concatenado, corLayer, this.layer);
						}
					}
				}
			},

			async setLicenca(i) {
				//Listo os IDs do Registros do banco de dados
				let arrayIDs = [];
				let strError = "";

				this.listaLicencasAmbientais.forEach((valor, index) => arrayIDs.push(valor.CodigoLicenca));
				const verificaID = arrayIDs.includes(i.CodigoLicenca)

				if (verificaID) {
					strError += i.NumeroLicenca + "<br>";
				}
				if (strError != "") {
					await Swal.fire({
						type: 'error',
						title: 'Esta Licença já foi cadastrada!',
						html: 'Por favor Corrigir! </br>' + '</br> Licença: ' +
							strError,
					})
					return;
				}

				//Salva as licenças selecionadas
				let params = {
					table: 'floraSupressaoConfig',
					data: {
						CodigoEscopoPrograma: codigoEscopoPrograma,
						CodigoLicenca: i.CodigoLicenca,
						DataCadastro: new Date()
					}
				}
				await vmGlobal.insertFromAPI(params, null, 'cgmab');
				await this.getConfiguracaoLicencas();
			},

			openModal(id) {
				//Rinderiza o modal
				this.mostrarModal = false;
				this.idCodigoLicenca = id;
				this.mostrarModal = true;
			},

			async addShapefile(idCodigoLicenca) {
				$('.spinnerShapefile').addClass('spinner-border spinner-border-sm');
				var myfiles = document.getElementById("ShapeFile");
				var files = myfiles.files;
				var data = new FormData();
				var file = this.$refs.ShapeFile.files[0];
				data.append('CodigoLicenca', parseInt(idCodigoLicenca));
				data.append('Coordenada', file);
				// this.layer.clearLayers();
				$.ajax({
					url: '<?= base_url('ConfiguracaoSupressaoVegetacao/postShapefileSupressao') ?>',

					method: 'POST',
					data: data,
					contentType: false,
					processData: false,
					success: function(data) {
						vmConfiguracaoSupressaoVegetacao.coordenadasShapeFile = JSON.parse(data);
						// const numCoordenadas = vmConfiguracaoSupressaoVegetacao.coordenadasShapeFile.length
						// const concatenado = '<div>Salvando ShapeFile</div>';
						// const corLayer = '#000';
						//
						// for (let index = 0; index < numCoordenadas; index++) {
						// 	const cd = vmConfiguracaoSupressaoVegetacao.coordenadasShapeFile[index].coordenada;
						// 	vmConfiguracaoSupressaoVegetacao.aplicarLayer(cd, concatenado, corLayer, vmConfiguracaoSupressaoVegetacao.layer);
						// }
					},
					error: function(error) {
						console.log('error', error);
						Swal.fire({
							position: 'center',
							type: 'error',
							title: 'Erro!',
							text: "Erro ao tentar salvar arquivo. " + e,
							showConfirmButton: true,

						});
					},
					complete: function() {
						vmConfiguracaoSupressaoVegetacao.salvarShapefile(idCodigoLicenca);
					}
				})
			},

			async salvarShapefile(idCodigoLicenca) {
				//Popula o Shapefile no banco de dados
				$('.spinnerShapefile').addClass('spinner-border spinner-border-sm');
				let params = {
					table: 'licencas',
					data: {
						Coordenada: JSON.stringify(this.coordenadasShapeFile)
					},
					where: {
						CodigoLicenca: idCodigoLicenca
					}
				}
				await vmGlobal.updateFromAPI(params, null);
				this.mostrarModal = false;

				await this.getConfiguracaoLicencas();
			},

			async deletarShapefile(idCodigoLicenca) {
				let params = {
					table: 'licencas',
					data: {
						Coordenada: null

					},
					where: {
						CodigoLicenca: idCodigoLicenca
					}
				}
				await vmGlobal.updateFromAPI(params, null);
				this.layer.clearLayers();
				this.getConfiguracaoLicencas();
			},

			async deletar(codigo) {
				let params = {
					table: 'floraSupressaoConfig',
					where: {
						CodigoFloraSupressao: codigo

					}
				}
				await vmGlobal.deleteFromAPI(params, 'cgmab');
				this.layer.clearLayers();
				this.getConfiguracaoLicencas();
			},

			async getInformacaoLicensa(IdLicensa) {
				var params = {
					table: 'viewLicencas',
					where: {
						CodigoLicenca: IdLicensa
					}
				}

				this.dadosDaLicensa = await vmGlobal.getFromAPI(params, 'cgmab');
				this.setsecaoDadosDaLicenca(true);
			},

			novaLicenca() {
				window.open('http://localhost:8080/sgmab/licenciamentoambiental/?NovaLicenca=VmpKMFYySXhUWGROVm1ScVVtdHdVbFpyVWtKUFVUMDk=', '_blank');
			},

			async getStatus() {
				var status = await vmGlobal.getFromAPI({
					table: 'contratosProgramasStatus',
					where: {
						CodigoEscopoPrograma: codigoEscopoPrograma,
						// CodigoContrato: this.codigoContrato
					}
				})

				if (status.length != 0) {
					this.status = status[0];
				}
			},
			async getParecer() {
				await axios.post(base_url + 'ParecerFiscal/getParecerProgramas', $.param({
						CodigoEscopoPrograma: codigoEscopoPrograma,
						// CodigoContrato: this.codigoContrato
					}))
					.then((response) => {
						this.listaParecer = response.data.parecer;
						this.usuario = response.data.usuario;

						this.refreshTable('#tblParecerFiscal');
					});
			},
			salvarParecer(statusFiscal) {
				var parecer = Object.assign({}, this.parecer);
				delete parecer.CodigoParecerFiscal;
				delete parecer.DataParecer;
				parecer.CodigoEscopoPrograma = codigoEscopoPrograma;
				// parecer.CodigoContrato = this.codigoContrato;
				parecer.StatusFiscal = statusFiscal;

				if (statusFiscal != 5) {
					parecer.PrazoCorrecao = null;
					this.showCorrecao = false;
				} else if (statusFiscal == 5 && parecer.PrazoCorrecao.length == 0) {
					this.showCorrecao = true;
					return;
				}

				Swal.fire({
					title: 'Salvar parecer?',
					text: "Está ação não pode ser alterada.",
					type: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Salvar'
				}).then(async (result) => {
					if (result.value) {
						await vmGlobal.insertFromAPI({
							table: 'parecerProgramas',
							data: parecer
						});

						await vmGlobal.updateFromAPI({
							table: 'contratosProgramasStatus',
							data: {
								Status: statusFiscal,
								PrazoCorrecao: parecer.PrazoCorrecao
							},
							where: {
								// CodigoContrato: this.codigoContrato,
								CodigoEscopoPrograma: codigoEscopoPrograma
							}
						});

						window.location.reload();
					}
				})
			},
			loadParecer(val) {
				this.showParecer = true;
				this.selectedParecer = Object.assign({}, this.parecer);

				this.selectedParecer.StatusFiscal = val.StatusFiscal;
				this.selectedParecer.NomeUsuario = val.NomeUsuario;
				this.selectedParecer.DataParecer = (val.DataParecer) ? moment(val.DataParecer).format('DD/MM/YYYY') : '';
				this.selectedParecer.DataEnvio = (val.DataEnvio) ? moment(val.DataEnvio).format('DD/MM/YYYY') : '';
				this.selectedParecer.AntigaDataCorrecao = (val.AntigaDataCorrecao) ? moment(val.AntigaDataCorrecao).format('DD/MM/YYYY') : '';
				this.selectedParecer.PrazoCorrecao = (val.PrazoCorrecao) ? moment(val.PrazoCorrecao).format('DD/MM/YYYY') : '';

				if (CKEDITOR.instances['txtParecerTecnicoView']) CKEDITOR.instances['txtParecerTecnicoView'].destroy();
				this.$nextTick(() => {
					CKEDITOR.replace('txtParecerTecnicoView', {
						height: '400px',
						readOnly: true
					});
				});

				this.$nextTick(() => {
					CKEDITOR.instances['txtParecerTecnicoView'].setData(String(atob(val.Parecer)));
				});
			},
			constroiTable(id = "", pageLength = 10) {
				$('table' + id, this.$el).DataTable({
					language: translateDataTable,
					lengthChange: false,
					destroy: true,
					pageLength: pageLength
				});
			},
			async refreshTable(id = "") {
				$('table' + id, this.$el).DataTable().destroy();
				await this.$nextTick(() => {
					this.constroiTable();
				});
			},
			formataData(val) {
				if (val) {
					return moment(val).format('DD/MM/YYYY');
				}

				return '';
			},
			contratoAtrazado(val) {
				if (val) {
					var today = new Date();
					today.setHours(0, 0, 0, 0);
					var prazoCorrecao = moment(val).toDate();
					prazoCorrecao.setHours(0, 0, 0, 0);

					return prazoCorrecao.getTime() < today.getTime();
				}

				return false;
			},
			async solicitarValidacao() {
				if (this.status.CodigoConfiguracaoProgramasStatus.length == 0) {
					await vmGlobal.insertFromAPI({
						table: 'contratosProgramasStatus',
						data: {
							CodigoEscopoPrograma: codigoEscopoPrograma,
							// CodigoContrato: this.codigoContrato,
							Status: 2,
							DataEnvio: moment().format('YYYY-MM-DD HH:mm:ss')
						}
					});

					await this.getStatus();

					return;
				}

				await vmGlobal.updateFromAPI({
					table: 'contratosProgramasStatus',
					where: {
						CodigoEscopoPrograma: codigoEscopoPrograma,
						// CodigoContrato: this.codigoContrato,
					},
					data: {
						Status: 2,
						DataEnvio: moment().format('YYYY-MM-DD HH:mm:ss')
					}
				});

				await this.getStatus();
			},
			getClasse(val) {
				return (parseInt(val) == 4) ? 'badge-success' : 'btn-rejeitado';
			},
			getStatusDescricao(val) {
				return (parseInt(val) == 4) ? 'Aprovado' : 'Parcialmente Aprovado';
			},
		}
	});

	function abrirInformacoesLicens(IdLicensa) {
		vmConfiguracaoSupressaoVegetacao.getInformacaoLicensa(IdLicensa);
	}

	$(document).on('change', '.custom-file-input', function(event) {
		$(this).next('.custom-file-label').html(event.target.files[0].name);
	})
</script>