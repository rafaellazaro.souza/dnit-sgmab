<!-- js Editor  -->
<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/jquery-te/jquery-te-1.4.0.css') ?>">
<!-- light box  -->
<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/ekko-lightbox/ekko-lightbox.css') ?>">

<?php $this->load->view('cgmab/css/comum.php'); ?>

<div class="container-fluid" id="vueApp">
    <section class="content-header mb-2">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Relatório Padrão de Monitoramento de Fauna</h1>
            </div>
            <div class="col-sm-6 text-right">
                <a href="<?php echo site_url('cgmab/configuracao') ?>"><i class="fa fa-times"></i></a>
            </div>
        </div>
    </section>

    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Nome da Atividade Aqui</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <!-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button> -->
                </div>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="input-group col-12 col-sm-8 mt-2"> </div>
                    <div class="input-group col-12 col-sm-4 mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Status</span>
                        </div>
                        <select class="form-control">
                            <option value="">Atacada</option>
                            <option value="">Cancelada</option>
                            <option value="">Pendente</option>
                            <option value="">Postergada</option>
                            <option value="">Prevista</option>
                            <option value="">Realizada</option>
                        </select>
                    </div>
                    <div class="input-group col-12 mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Descrição</span>
                        </div>
                        <textarea class="form-control"></textarea>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.card -->

    </section>

    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">1 - Introdução</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                </div>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="input-group col-12 mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Observações Gerais</span>
                        </div>
                        <textarea class="form-control textarea"></textarea>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.card -->
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">2 - Cadastro do Registro Fotográfico do Status das Passagens de Fauna e Flora no Período de Amostragem</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                </div>
            </div>
            <div class="card-body pt-4">



                <div class="form-group col-6">
                    <label for="exampleInputFile">Arquivo Shape File</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Clique aqui para buscar o arquivo</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text" id="">Enviar</span>
                        </div>
                    </div>
                </div>
                <div class="h-450r w-100 mt-4">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>

                                <th class="bg-2 text-center border" colspan="5">Taxonomia</th>

                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class="bg-2 text-center border" colspan="2">Grau de ameaça</th>
                                <th></th>
                                <th></th>
                            </tr>
                            <tr>
                                <th>Id</th>
                                <th class="bg-2 border">Grupo</th>
                                <th class="bg-2 border">Classe</th>
                                <th class="bg-2 border">Ordem</th>
                                <th class="bg-2 border">Família</th>
                                <th class="bg-2 border">Espécie</th>
                                <th>Nome Comum</th>
                                <th>Quantidade Registrada</th>
                                <th>Id Módulo</th>
                                <th>Ambiente</th>
                                <th>Tipo Registro</th>
                                <th class="bg-2 border">IUCN(2018.1)</th>
                                <th class="bg-2 border">ICMBIO (2016)</th>
                                <th>Num. FotoRegistro Fotográfico</th>
                                <th>Observações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>mamíferos</td>
                                <td>Não voador</td>
                                <td>ordem aqui</td>
                                <td>Roedores</td>
                                <td>XYZH-0014</td>
                                <td>Rato</td>
                                <td>10</td>
                                <td>1335</td>
                                <td>Ocasional</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>234;234;236</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>mamíferos</td>
                                <td>Não voador</td>
                                <td>ordem aqui</td>
                                <td>Roedores</td>
                                <td>XYZH-0014</td>
                                <td>Rato</td>
                                <td>10</td>
                                <td>1335</td>
                                <td>Ocasional</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>234;234;236</td>
                                <td>Texto da Observação aqui</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>mamíferos</td>
                                <td>Não voador</td>
                                <td>ordem aqui</td>
                                <td>Roedores</td>
                                <td>XYZH-0014</td>
                                <td>Rato</td>
                                <td>10</td>
                                <td>1335</td>
                                <td>Ocasional</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>234;234;236</td>
                                <td>Texto da Observação aqui</td>
                            </tr>



                        </tbody>
                    </table>
                </div>
                <div class="alert alert-info p-4">
                    <div class="ic-lft">
                        <i class="far fa-question-circle"></i>
                    </div>
                    <div>
                        <p>
                            <b>Observação!</b>
                            As fotos deverão ser salvas com o mesmo nome da foto no repositório de documentos. Para que
                            possa ser identificada automaticamente pelo sistema. Essa gravação deverá ser realizada clicando no botão "salvar fotos",
                            e selecionado as fotos produzidas pela CONTRATADA e enviando para o repositório de documentos da CGMAB. Assim em uma ação
                            a CONTRATADA consegue carregar todas as fotos. Caso queira apagar basta clicar no botão "excluir".
                        </p>
                    </div>
                </div>
                <div class="form-group col-6">
                    <label for="exampleInputFile">Arquivo Shape File</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Clique aqui para buscar o arquivo</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text" id="">Enviar</span>
                        </div>
                    </div>
                </div>

                <div class="h-450r w-100 mt-4">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th colspan="5"></th>
                                <th colspan="4" class="bg-4">UTM (Data SIRGAS 2000)</th>
                                <th colspan="3"></th>
                            </tr>
                            <tr>
                                <th>Id Shape File</th>
                                <th>Número da foto</th>
                                <th>Nome da Foto</th>
                                <th>Latitude</th>
                                <th>Longitude</th>
                                <th class="bg-4">Zona</th>
                                <th class="bg-4">CN</th>
                                <th class="bg-4">CE</th>
                                <th class="bg-4">Elevação (m)</th>
                                <th>Data</th>
                                <th>Observações</th>
                                <th>Nome da foto no repositório de documentos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>233</td>
                                <td>foto-2019-01</td>
                                <td>-44441</td>
                                <td>66666</td>
                                <td>XDDD</td>
                                <td>Xf</td>
                                <td>XXX</td>
                                <td>15</td>
                                <td>01/10/2019</td>
                                <td>-</td>
                                <td>xXXXFDSfsf002112.jpg</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>203</td>
                                <td>foto-2019-01</td>
                                <td>-44441</td>
                                <td>66666</td>
                                <td>XDDD</td>
                                <td>Xf</td>
                                <td>XXX</td>
                                <td>15</td>
                                <td>01/10/2019</td>
                                <td>-</td>
                                <td>xXXXFDSfsf002112.jpg</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>200</td>
                                <td>foto-2019-01</td>
                                <td>-44441</td>
                                <td>66666</td>
                                <td>XDDD</td>
                                <td>Xf</td>
                                <td>XXX</td>
                                <td>15</td>
                                <td>01/10/2019</td>
                                <td>-</td>
                                <td>xXXXFDSfsf002112.jpg</td>
                            </tr>
                        </tbody>
                    </table>
                </div>


                <div class="form-group">
                    <label for="exampleInputFile">Fotos</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Clique aqui para buscar suas fotos</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text" id="">Enviar</span>
                        </div>
                    </div>
                </div>
                <!-- listgem de fotos  -->
                <div class="row fotos-programa">

                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-2">
                        <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                            <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                        </a>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Nome da foto aqui Nome da foto aqui
                                <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                            </li>
                        </ul>
                    </div>

                </div><!-- listgem de fotos  -->

            </div>

        </div>
        <!-- /.card -->


        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">3 - Análise dos Resultados por Grupo Faunístico</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                </div>
            </div>
            <div class="card-body">
                <div class="row mt-5 ">
                    <div class="col-12 col-sm-3 border p-3">

                        <h5>Grupos Faunísticos</h5>

                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">I - Pequenos Mamíferos não Voadores</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">II - Médios e Grandes Mamíferos</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">III - Aves</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">IV - Amfíbios</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">V - Répteis</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">VI - Peixes</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">VI - Invertebrados Bentônicos</button> </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-9 border p-3">
                        <!-- mapa  -->
                        <img src="<?php echo base_url('img_dpp/mapapadrao.jpg'); ?>" class="w-100" alt="">
                        <!-- fim mapa  -->
                    </div>


                    <div class="col-12  border p-3">
                        <h5>Lisa de Espécies Monitoradas</h5>

                        <table class="table table-striped" id="tabGruposFaunisticos">
                            <thead>
                                <tr>
                                    <th colspan="4" class="bg-2 text-center">Taxonomia</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th colspan="2" class="bg-2 text-center">Grau de Ameaça</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th class="bg-2">Classe</th>
                                    <th class="bg-2">Ordem</th>
                                    <th class="bg-2">Família</th>
                                    <th class="bg-2">Espécie</th>
                                    <th>Nome Comum</th>
                                    <th>Quantidade registrada</th>
                                    <th>Id Módulo</th>
                                    <th>Ambiente</th>
                                    <th>Tipo Registro</th>
                                    <th>Numero da Campanha</th>
                                    <th class="bg-2">IUCN (2018.1)</th>
                                    <th class="bg-2">ICMBio (2016)</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Classe 01</td>
                                    <td>25</td>
                                    <td>Roedores</td>
                                    <td>Esp-DE879</td>
                                    <td>Rato</td>
                                    <td>10</td>
                                    <td>14</td>
                                    <td>Selva</td>
                                    <td>Todo dia</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td><button class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#modalGruposFaunisticos"><i class="fa fa-eye"></i></button></td>
                                </tr>
                                <tr>
                                    <td>Classe 01</td>
                                    <td>25</td>
                                    <td>Roedores</td>
                                    <td>Esp-DE879</td>
                                    <td>Capivara</td>
                                    <td>10</td>
                                    <td>14</td>
                                    <td>Selva</td>
                                    <td>Todo dia</td>
                                    <td>-</td>
                                    <td>Médio</td>
                                    <td>Médio</td>
                                    <td><button class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#modalGruposFaunisticos"><i class="fa fa-eye"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">4 - Análise dos Resultados por Módulo</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                </div>
            </div>
            <div class="card-body">
                <div class="row mt-5 ">
                    <div class="col-12 col-sm-3 col-xl-2 border p-3">

                        <h5>Grupos Faunísticos</h5>

                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">Módulo 1</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">Módulo 2</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">Módulo 3</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">Módulo 4</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">Módulo 5</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">Módulo 6</button> </li>
                            <li class="list-group-item"><button class="btn btn-outline-secondary w-100 text-left">Módulo 7</button> </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-9 col-xl-10 border p-3">
                        <!-- mapa  -->
                        <img src="<?php echo base_url('img_dpp/mapapadrao.jpg'); ?>" class="w-100" alt="">
                        <!-- fim mapa  -->
                    </div>


                    <div class="col-12  border p-3">
                        <h5>Lisa de Espécies Monitoradas</h5>

                        <table class="table table-striped" id="tabGruposFaunisticos">
                            <thead>
                                <tr>
                                    <th colspan="4" class="bg-2 text-center">Taxonomia</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th colspan="2" class="bg-2 text-center">Grau de Ameaça</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th class="bg-2">Classe</th>
                                    <th class="bg-2">Ordem</th>
                                    <th class="bg-2">Família</th>
                                    <th class="bg-2">Espécie</th>
                                    <th>Nome Comum</th>
                                    <th>Quantidade registrada</th>
                                    <th>Id Módulo</th>
                                    <th>Ambiente</th>
                                    <th>Tipo Registro</th>
                                    <th>Numero da Campanha</th>
                                    <th class="bg-2">IUCN (2018.1)</th>
                                    <th class="bg-2">ICMBio (2016)</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Classe 01</td>
                                    <td>25</td>
                                    <td>Roedores</td>
                                    <td>Esp-DE879</td>
                                    <td>Rato</td>
                                    <td>10</td>
                                    <td>14</td>
                                    <td>Selva</td>
                                    <td>Todo dia</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td><button class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#modalGruposFaunisticos"><i class="fa fa-eye"></i></button></td>
                                </tr>
                                <tr>
                                    <td>Classe 01</td>
                                    <td>25</td>
                                    <td>Roedores</td>
                                    <td>Esp-DE879</td>
                                    <td>Capivara</td>
                                    <td>10</td>
                                    <td>14</td>
                                    <td>Selva</td>
                                    <td>Todo dia</td>
                                    <td>-</td>
                                    <td>Médio</td>
                                    <td>Médio</td>
                                    <td><button class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#modalGruposFaunisticos"><i class="fa fa-eye"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">5 - Análise dos Resultados da Campanha</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                </div>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="input-group col-12 mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Análise dos Resultados</span>
                        </div>
                        <textarea class="form-control textarea"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="input-group col-12 mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text w-100">Conclusão detalhada</span>
                        </div>
                        <textarea class="form-control textarea"></textarea>
                    </div>
                </div>
                <button class="btn btn-primary">Salvar</button>
            </div>

        </div>
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">6 - Bibliografia e Anexos</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                </div>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="input-group col-12 mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Observações Gerais</span>
                        </div>
                        <textarea class="form-control textarea"></textarea>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="form-group col-6">
                        <label for="exampleInputFile">Anexar Documentos de Suporte</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">Clique aqui para buscar o arquivo</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="">Enviar</span>
                            </div>
                        </div>
                        <div class="h-450r w-100 mt-4">
                            <table class="table table-striped">
                                <tr>
                                    <td>Documento 1</td>
                                    <td class="text-right"><button class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></button></td>
                                </tr>
                                <tr>
                                    <td>Documento 2</td>
                                    <td class="text-right"><button class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></button></td>
                                </tr>
                                <tr>
                                    <td>Documento numero 3</td>
                                    <td class="text-right"><button class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></button></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </div>



    </section>

    <!-- Modal Grupos faunísticos -->
    <div class="modal fade" id="modalGruposFaunisticos" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Registro Fotográfico</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">

                        <table class="table table-striped" id="tabGruposFaunisticos">
                            <thead>

                                <tr>
                                    <th class="bg-2">Classe</th>
                                    <th class="bg-2">Ordem</th>
                                    <th class="bg-2">Família</th>
                                    <th class="bg-2">Espécie</th>
                                    <th>Nome Comum</th>
                                    <th>Quantidade registrada</th>
                                    <th>Id Módulo</th>
                                    <th>Ambiente</th>
                                    <th>Tipo Registro</th>
                                    <th>Numero da Campanha</th>
                                    <th class="bg-2">IUCN (2018.1)</th>
                                    <th class="bg-2">ICMBio (2016)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Classe 01</td>
                                    <td>25</td>
                                    <td>Roedores</td>
                                    <td>Esp-DE879</td>
                                    <td>Rato</td>
                                    <td>10</td>
                                    <td>14</td>
                                    <td>Selva</td>
                                    <td>Todo dia</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>

                            </tbody>
                        </table>

                    </div>

                    <!-- listgem de fotos  -->
                    <div class="row fotos-programa">

                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-2">
                            <a href="http://via.placeholder.com/400" data-toggle="lightbox" data-title="Nome da foto aqui" data-gallery="gallery">
                                <img src="http://via.placeholder.com/200" class="img-fluid img-thumbnail">
                            </a>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Nome da foto aqui Nome da foto aqui
                                    <a href="javascript:;" class="btn btn-sm btn-outline-secondary float-right"><i class="fa fa-times"></i></a>
                                </li>
                            </ul>
                        </div>




                    </div><!-- listgem de fotos  -->
                    <hr class="divider">

                    <div class="col-12 mt-4">
                        <div class="input-group col-12 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Análise e Observações Específicas por Espécie <i class="fas fa-exclamation-circle"></i></span>
                            </div>
                            <textarea class="form-control textarea"></textarea>
                        </div>
                    </div>
                    <hr class="divider">
                    <!-- Default box -->
                    <div class="card mt-5">
                        <div class="card-header">
                            <h3 class="card-title">Resultado Consolidado Grupo Faunístico I</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fas fa-minus"></i></button>
                                <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                        <i class="fas fa-times"></i></button>-->
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <h5 class="w-100 mb-4">Análise dos Resultados Padrão de Abundância Riquesa</h5>
                                <div class="col-12 col-sm-6 mb-4">
                                    <img src="http://via.placeholder.com/450" alt="">
                                </div>
                                <div class="col-12 col-sm-6 mb-4">
                                    <div class="form-group">
                                        <label for="exampleInputFile">Anexa Figura</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="exampleInputFile">
                                                <label class="custom-file-label" for="exampleInputFile">Clique aqui para enviar imagem</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="">Enviar</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group col-12 mt-2">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Análise do Resultado</span>
                                        </div>
                                        <textarea class="form-control textarea"></textarea>
                                    </div>
                                </div>

                            </div>

                            <hr class="divider">

                            <div class="row">
                                <h5 class="w-100 mb-4">Análise dos Resultados Shannon - Wiener e Equitabilidade de Pielou</h5>
                                <div class="col-12 col-sm-6 mb-4">
                                    <img src="http://via.placeholder.com/450" alt="">
                                </div>
                                <div class="col-12 col-sm-6 mb-4">
                                    <div class="form-group">
                                        <label for="exampleInputFile">Anexa Figura</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="exampleInputFile">
                                                <label class="custom-file-label" for="exampleInputFile">Enviar imagem</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="">Enviar</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group col-12 mt-2">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Análise do Resultado</span>
                                        </div>
                                        <textarea class="form-control textarea"></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <h5 class="w-100 mb-4">Análise dos Resultados DendoGrama de Similaridade</h5>
                                <div class="col-12 col-sm-6 mb-4">
                                    <img src="http://via.placeholder.com/450" alt="">
                                </div>
                                <div class="col-12 col-sm-6 mb-4">
                                    <div class="form-group">
                                        <label for="exampleInputFile">Anexa Figura</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="exampleInputFile">
                                                <label class="custom-file-label" for="exampleInputFile">Enviar imagem</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="">Enviar</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group col-12 mt-2">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Análise do Resultado</span>
                                        </div>
                                        <textarea class="form-control textarea"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary">Salvar</button>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- Editor textarea  -->
<script src="<?= base_url('template_lte3/plugins/jquery-te/jquery-te-1.4.0.min.js') ?>"></script>
<!-- light box  -->
<script src="<?= base_url('template_lte3/plugins/ekko-lightbox/ekko-lightbox.min.js') ?>"></script>

<?php $this->load->view('cgmab/js/comum.php'); ?>