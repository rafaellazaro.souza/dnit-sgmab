<!-- js Editor  -->
<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/jquery-te/jquery-te-1.4.0.css') ?>">
<?php $this->load->view('cgmab/css/comum.php'); ?>

<div class="container-fluid" id="vueApp">
    <section class="content-header mb-2">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Relatório Técnico Padrão</h1>
            </div>
            <div class="col-sm-6 text-right">
                <a href="<?php echo site_url('cgmab/configuracao') ?>"><i class="fa fa-times"></i></a>
            </div>
        </div>
    </section>

    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Nome da Atividade Aqui</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <!-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button> -->
                </div>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="input-group col-12 col-sm-8 mt-2"> </div>
                    <div class="input-group col-12 col-sm-4 mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Status</span>
                        </div>
                        <select class="form-control">
                            <option value="">Atacada</option>
                            <option value="">Cancelada</option>
                            <option value="">Pendente</option>
                            <option value="">Postergada</option>
                            <option value="">Prevista</option>
                            <option value="">Realizada</option>
                        </select>
                    </div>
                    <div class="input-group col-12 mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Descrição</span>
                        </div>
                        <textarea class="form-control"></textarea>
                    </div>


                    <div class="input-group col-12 mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Ações Executadas</span>
                        </div>
                        <textarea class="form-control textarea"></textarea>
                    </div>
                    <div class="input-group col-12 mt-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Anexar Relatório</span>
                        </div>
                        <input type="file" class="form-control">
                    </div>

                    <div class="input-group col-12 col-sm-6 mt-2 border p-3">
                        <table class="table table-striped">
                            <tr>
                                <td>ArquivoAnexado.DOC</td>
                                <td><button class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></button></td>
                            </tr>
                            <tr>
                                <td>ArquivoAnexado2.DOC</td>
                                <td><button class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></button></td>
                            </tr>
                            <tr>
                                <td>ArquivoAnexado2.DOC</td>
                                <td><button class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></button></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.card -->

    </section>




</div>
<!-- Editor textarea  -->
<script src="<?= base_url('template_lte3/plugins/jquery-te/jquery-te-1.4.0.min.js') ?>"></script>
<?php $this->load->view('cgmab/js/comum.php'); ?>