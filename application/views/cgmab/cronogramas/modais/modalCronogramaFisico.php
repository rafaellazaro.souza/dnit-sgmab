<!-- Modal -->
<div class="modal fade" id="modalCronogramaFisico" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cronograma</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- formulário inicial -->

        <div id="formAddCronograma">
          <form action="#">
            <div class="row">

              <div class="input-group col-12 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Duração do Programa</span>
                </div>
                <select class="form-control"></select>
              </div>

              <div class="input-group col-12 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Previsão de Início</span>
                </div>
                <select class="form-control"></select>
              </div>

              <div class="input-group col-12 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Periodicidade</span>
                </div>
                <select class="form-control"></select>
              </div>

              <div class="input-group col-12 mt-2">
                <div class="input-group-prepend">
                  <span class="input-group-text">Prazo início (dias)</span>
                </div>
                <input type="number" class="form-control">
              </div>

              <div class="input-group col-12 mt-2">
                <table class="table table-striped">
                  <thead>
                    <th>Num</th>
                    <th>Mês/ Ano</th>
                    <th>Periodicidade dos Relatórios</th>
                    <th>Atividades Previstas no Mês</th>
                    <th>Descrição</th>
                    <th>Ações</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>
                        <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                        <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <a href="#" v-else class="btn btn-outline-secondary btn-sm" id="btnAddAtividades"><i class="fa fa-plus"></i></a>

              </div>



            </div>
          </form>
        </div>
        <!-- fim formulário inicial -->

        <!-- formulario atividades -->
        <div class="invisivel" id="formAddAtividades">

          <form id="formAddAtividade" class="mb-4">

            <div class="input-group mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Nome da Atividade</span>
              </div>
              <input class="form-control" type="text">
            </div>

            <div class="input-group mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Mês</span>
              </div>
              <select class="form-control"></select>
            </div>

            <div class="input-group mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Relatório</span>
              </div>
              <select class="form-control"></select>
            </div>

            <div class="input-group mt-2">
              <div class="input-group-prepend">
                <span class="input-group-text">Descrição</span>
              </div>
              <textarea class="form-control"></textarea>
            </div>

          </form>

          <button class="btn btn-outline-secondary" id="btnCancelarAddAtividades">Cancelar</button>
          <button class="btn btn-primary" id="btnSalvarAtividades">Salvar</button>
        </div>
        <!-- fim formulario atividades -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </div>
</div>