<?php $this->load->view('cgmab/cronogramas/css.php') ?>
<div id="vueApp">
    <div class="container-fluid">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Cronograma Físico</h1>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a href="<?php echo site_url('cgmab/configuracao') ?>"><i class="fa fa-times"></i></a>
                    </div>

                </div>
            </div>
        </section>


        <section class="content mb-5">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa fa-copy mr-3"></i> Programas/ Planos/ Estudos e Projetos</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body">

                    <table class="table table-striped  mt-3 p-3">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Licença/ Autorização</th>
                                <th>Número</th>
                                <th>Numero Cond.</th>
                                <th>Nome</th>
                                <th>Tipo</th>
                                <th>Sub Tipo</th>
                                <th></th>
                                <th>Cronograma</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tr v-for="item in listagem">
                            <td>{{item.id}}</td>
                            <td>{{item.licenca}}</td>
                            <td>{{item.numero}}</td>
                            <td>{{item.numCond}}</td>
                            <td>{{item.nome}}</td>
                            <td>{{item.tipo}}</td>
                            <td>{{item.subTipo}}</td>
                            <td>
                                <a href="#" v-if="item.id === '1' || item.id === '2' || item.id === '3' || item.id === '5'" class="btn btn-success btn-sm" ><i class="fa fa-check"></i></a>
                                <a href="#" v-else class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#modalCronogramaFisico"><i class="fa fa-plus"></i></a>
                            </td>                            
                            <td>{{item.cronograma}}</td>
                            <td>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <tbody>

                        </tbody>
                    </table>

                </div>
            </div> <!-- /.card-body -->
        </section>

    </div>
</div>
<?php $this->load->view('cgmab/cronogramas/modais/modalCronogramaFisico.php') ?>
<?php $this->load->view('cgmab/cronogramas/script.php') ?>