<?php $this->load->view('cgmab/infraestrutura/css.php'); ?>
<div class="vueApp">
    <div class="container-fluid">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Infraestrutura</h1>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a href="<?php echo site_url('cgmab/configuracao') ?>"><i class="fa fa-times"></i></a>
                    </div>

                </div>
            </div>
        </section>
        <section class="content mb-5">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa fa-copy mr-3"></i> Cadastro de Lotação</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <th>Id</th>
                            <th>Lotação</th>
                            <th>UF</th>
                            <th>Município</th>
                            <th>Endereço</th>
                            <th>CEP</th>
                            <th>Telefone(s)</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Tipo de Lotação</th>
                            <th>Observações</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Sede</td>
                                <td>DF</td>
                                <td>Brasília</td>
                                <td>SHIS QL 22 Conj 6 csa 9</td>
                                <td>71625-560</td>
                                <td>6134343334</td>
                                <td>13412312</td>
                                <td>12312312</td>
                                <td>Escritório</td>
                                <td></td>
                                <td>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="javascript:;" class="btn btn-outline-secondary btn-sm mt-3" data-toggle="modal" data-target="#modalAddLotacao"><i class="fa fa-plus"></i></a>

                </div>
            </div> <!-- /.card-body -->
        </section>
        <section class="content mb-5">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa fa-copy mr-3"></i> Cadastro de Material de Escritório</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <th>Foto</th>
                            <th>Código SICRO</th>
                            <th>Tipo</th>
                            <th>Descrição/Modelo</th>
                            <th>Quantidade</th>
                            <th>Num. Série</th>
                            <th>Local de Lotação</th>
                            <th>Observações</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <img src="http://via.placeholder.com/50">
                                </td>
                                <td>3499BH</td>
                                <td>Computador</td>
                                <td>Dell Optplex 350</td>
                                <td>5</td>
                                <td>223123546546</td>
                                <td>
                                    Sede<br/>
                                    Escritório 1
                                </td>
                                <td></td>
                                <td>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="javascript:;" class="btn btn-outline-secondary btn-sm mt-3" data-toggle="modal" data-target="#modalAddMaterialEscritorio"><i class="fa fa-plus"></i></a>

                </div>
            </div> <!-- /.card-body -->
        </section>
        <section class="content mb-5">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa fa-copy mr-3"></i> Cadastro de Veículos</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <th>Foto</th>
                            <th>Código SICRO</th>
                            <th>Tipo</th>
                            <th>Descrição/Modelo</th>
                            <th>Quantidade</th>
                            <th>Num. Série</th>
                            <th>Local de Lotação</th>
                            <th>Observações</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <img src="http://via.placeholder.com/50">
                                </td>
                                <td>3499BH</td>
                                <td>Computador</td>
                                <td>Dell Optplex 350</td>
                                <td>5</td>
                                <td>223123546546</td>
                                <td>
                                    Sede<br/>
                                    Escritório 1
                                </td>
                                <td></td>
                                <td>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="javascript:;" class="btn btn-outline-secondary btn-sm mt-3" data-toggle="modal" data-target="#modalAddMaterialEscritorio"><i class="fa fa-plus"></i></a>

                </div>
            </div> <!-- /.card-body -->
        </section>
        <section class="content mb-5">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa fa-copy mr-3"></i> Cadastro de Equipamentos Técnicos</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <!--                <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fas fa-times"></i></button>-->
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <th>Foto</th>
                            <th>Código SICRO</th>
                            <th>Tipo</th>
                            <th>Descrição/Modelo</th>
                            <th>Quantidade</th>
                            <th>Num. Série</th>
                            <th>Local de Lotação</th>
                            <th>Observações</th>
                            <th>Ações</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <img src="http://via.placeholder.com/50">
                                </td>
                                <td>3499BH</td>
                                <td>Computador</td>
                                <td>Dell Optplex 350</td>
                                <td>5</td>
                                <td>223123546546</td>
                                <td>
                                    Sede<br/>
                                    Escritório 1
                                </td>
                                <td></td>
                                <td>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="javascript:;" class="btn btn-outline-secondary btn-sm mt-3" data-toggle="modal" data-target="#modalAddMaterialEscritorio"><i class="fa fa-plus"></i></a>

                </div>
            </div> <!-- /.card-body -->
        </section>
    </div>
</div>
<?php $this->load->view('cgmab/infraestrutura/modais/modalAddLotacao.php'); ?>
<?php $this->load->view('cgmab/infraestrutura/modais/modalAddMaterialEscritorio.php'); ?>
<?php $this->load->view('cgmab/infraestrutura/script.php'); ?>