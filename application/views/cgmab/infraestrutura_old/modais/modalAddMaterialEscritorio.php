<div class="modal fade" id="modalAddMaterialEscritorio" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Cadastro de Condicionantes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <div id="divCadastroMaterialEscritorio">
                    <form id="form-add-lotacao" class="p-4">
                        <div class="row">
                            <div class="input-group col-12 col-sm-4 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Código SICRO</span>
                                </div>
                                <input type="text" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-8 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Tipo</span>
                                </div>
                                <input type="text" class="form-control">
                            </div>
                            <div class="input-group col-12 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Descrição</span>
                                </div>
                                <textarea class="form-control"></textarea>

                            </div>

                            <div class="input-group col-12 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Foto</span>
                                </div>
                                <input type="file" class="form-control">
                            </div>
                            <div class="input-group col-12 col-sm-3 mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Quantidade</span>
                                </div>
                                <input type="text" class="form-control" disabled value="5">
                            </div>
                            <div class="input-group col-12 mt-2">
                                <table class="table table-striped">
                                    <thead>
                                        <th>#</th>
                                        <th>Num. Série</th>
                                        <th>Local de Lotação</th>
                                        <th>Observações</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>55561223</td>
                                            <td>Sede</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>4564561</td>
                                            <td>Sede</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>9879FR123</td>
                                            <td>Sede</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>14FB4564</td>
                                            <td>Sede</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>113233</td>
                                            <td>Sede</td>
                                            <td></td>
                                        </tr>
                                    </tbody>

                                </table>
                                <a href="javascript:;" class="btn btn-outline-secondary" id="btnAddNumeroSerie"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="invisivel" id="divCadastroNumeroSerie">
                    <form>

                        <div class="input-group col-12 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Número de Série</span>
                            </div>
                            <input type="text" class="form-control">
                        </div>
                        <div class="input-group col-12 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Local da Lotação</span>
                            </div>
                            <select class="form-control"></select>
                        </div>
                        <div class="input-group col-12 mt-2">

                            <div class="input-group-prepend">
                                <span class="input-group-text">Observações</span>
                            </div>
                            <textarea class="form-control"></textarea>
                        </div>
                        <div class="w-100 mt-4 text-right">
                            <button type="button" class="btn btn-secondary" id="btnCancelarAddNumero">Cancelar</button>
                            <button type="button" class="btn btn-primary mr-2">Salvar</button>
                        </div>
                    </form>
                </div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>
</div>