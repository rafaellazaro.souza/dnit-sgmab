<div class="modal fade" id="modalAddLotacao" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Cadastro de Condicionantes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <form id="form-add-lotacao" class="p-4">
                    <div class="row">
                        <div class="input-group col-12 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Lotação</span>
                            </div>
                            <input type="text" class="form-control">
                        </div>
                        <div class="input-group col-12 col-sm-6 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">CEP</span>
                            </div>
                            <input type="number" class="form-control">
                        </div>
                        <div class="input-group col-12 col-sm-6 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">UF</span>
                            </div>
                            <input type="text" class="form-control">
                        </div>
                        <div class="input-group col-12 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Município</span>
                            </div>
                            <select class="form-control"></select>
                        </div>
                        <div class="input-group col-12 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Logradouro</span>
                            </div>
                            <input type="text" class="form-control">
                        </div>
                        <div class="input-group col-12 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Número</span>
                            </div>
                            <input type="number" class="form-control">
                        </div>
                        <div class="input-group col-12 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Bairro</span>
                            </div>
                            <input type="text" class="form-control">
                        </div>
                        <div class="input-group col-12 col-sm-6 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Latitude</span>
                            </div>
                            <input type="number" class="form-control">
                        </div>
                        <div class="input-group col-12 col-sm-6 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Longitude</span>
                            </div>
                            <input type="number" class="form-control">
                        </div>
                        <div class="input-group col-12 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Tipo de Lotação</span>
                            </div>
                            <select class="form-control"></select>
                        </div>
                        <div class="input-group col-12 mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Observação</span>
                            </div>
                            <textarea class="form-control"></textarea>
                        </div>
                    </div>
                </form>
                <!-- form upload Excel Cadastro de condicionantes --->
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>
</div>