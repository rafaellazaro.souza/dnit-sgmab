<script>
    var vmEquipamentos = new Vue({
        el: '#gestaoEquipamentos',
        data: {
            listaEquipamentos: 0,
            listaContratos: [],
            dadosFiltroEquipamentos: {},
            mostrarAddEquipamentos: false,
            erros: 0,
        },
        mounted() {
            this.getEquipamentos();
            this.getContratos();
        },
        methods: {
            async getEquipamentos() {
                $("#listadeEquipamentos").DataTable().destroy();
                const params = {
                    table: 'equipamentos',
                    join: {
                        table: 'contratos',
                        on: 'CodigoContrato'
                    },
                    where: {
                        CodigoContratada: <?= $_SESSION['Logado']['CodigoContratada'] ?>
                    }

                }

                this.listaEquipamentos = await vmGlobal.getFromAPI(params, 'cgmab');
                vmGlobal.montaDatatable('#listadeEquipamentos', true);

            },
            async getContratos() {
                let params = {
                    table: 'contratos',
                    where: {
                        CnpjEmpresa: <?= isset($_SESSION['Logado']['CnpjContratada']) ? $_SESSION['Logado']['CnpjContratada'] : 0; ?>,
                    }
                }
                this.listaContratos = await vmGlobal.getFromAPI(params);
            },
            checkForm: function(e) {
                if (this.dadosFiltroEquipamentos.CodigoContrato && this.dadosFiltroEquipamentos.NomeEquipamento && this.dadosFiltroEquipamentos.DescricaoModelo && this.dadosFiltroEquipamentos.NumeroSerie &&
                    this.dadosFiltroEquipamentos.EspecificacaoTecnica) {
                    this.dadosFiltroEquipamentos.DescricaoModelo = btoa(this.dadosFiltroEquipamentos.DescricaoModelo);
                    this.dadosFiltroEquipamentos.EspecificacaoTecnica = btoa(this.dadosFiltroEquipamentos.EspecificacaoTecnica);
                    this.dadosFiltroEquipamentos.Observacoes = btoa(this.dadosFiltroEquipamentos.Observacoes);
                    this.salvarEquipamento();
                    return true;
                }

                this.erros = [];

                if (!this.dadosFiltroEquipamentos.CodigoContrato) {
                    this.erros.push('Código do contrato é obrigatório!');
                }
                if (!this.dadosFiltroEquipamentos.NomeEquipamento) {
                    this.erros.push('Nome do Equipamento é obrigatório!');
                }
                if (!this.dadosFiltroEquipamentos.DescricaoModelo) {
                    this.erros.push('Descrição/ Modelo é obrigatório!');
                }
                if (!this.dadosFiltroEquipamentos.NumeroSerie) {
                    this.erros.push('Número de Série é obrigatório!');
                }
                if (!this.dadosFiltroEquipamentos.EspecificacaoTecnica) {
                    this.erros.push('Especificação Técnica é obrigatório!');
                }

                e.preventDefault();
            },
            async salvarEquipamento() {
                var file = null;
                if (this.$refs.Foto != null && this.$refs.Foto != undefined) {
                    this.dadosFiltroEquipamentos.NomeFoto = this.$refs.Foto.files[0].name;
                    this.dadosFiltroEquipamentos.FilesRules = {
                        upload_path: 'webroot/uploads/Equipamentos',
                        allowed_types: 'jpg|jpeg|png|gif|pdf',
                    };
                    file = {
                        name: 'Foto',
                        value: this.$refs.Foto.files[0]
                    }
                }


                this.dadosFiltroEquipamentos.CodigoContratada = <?= $_SESSION['Logado']['CodigoContratada'] ?>;
                const params = {
                    table: 'equipamentos',
                    data: this.dadosFiltroEquipamentos
                }
                await vmGlobal.insertFromAPI(params, file, 'cgmab');
                this.mostrarAddEquipamentos = false;
                this.dadosFiltroEquipamentos = {
                    FilesRules: {
                        upload_path: 'webroot/uploads/Equipamentos',
                        allowed_types: 'jpg|jpeg|png|gif|pdf',
                    }
                };
                this.getEquipamentos();
                this.$refs.Foto.value = '';

            },
            async deletar(codigo, index) {
                params = {
                    table: 'equipamentos',
                    where: {
                        CodigoEquipamento: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                this.getEquipamentos();
            },
            resetarform() {
                this.mostrarAddEquipamentos = false;
                this.dadosFiltroEquipamentos = {};
                this.erros = [];
                this.dadosFiltroEquipamentos = {
                    FilesRules: {
                        upload_path: 'webroot/uploads/Equipamentos',
                        allowed_types: 'jpg|jpeg|png|gif|pdf',
                    }
                }
            }
        }
    });
</script>