<div class="body_scroll" id="gestaoEquipamentos">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Gestão de Equipamentos</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url();?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item">Equipamentos</li>
                    <li class="breadcrumb-item active">Listar</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                        class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <button @click="mostrarAddEquipamentos = true" class="btn btn-secondary"><i class="fa fa-plus"></i>
                        Adicionar Equipamento</button>
                    <!-- formulario para adicionar/Editar Equipamentos  -->
                    <div class="modal-w98-h50 sombra-2 p-5 w-50" v-show="mostrarAddEquipamentos">
                        <div class="col-12">
                            <button @click="resetarform()" class="btn btn-secondary btn-r"><i class="fa fa-times"></i></button>
                        </div>
                        <h4>Dados do Equipamentos</h4>

                        <div class="row mt-3">
                            <div class="alert alert-warning p-3 text-left w-100" v-show="erros.length">
                                <b>Por Favor Preencha o(s) campo(s) abaixo:</b>
                                <ul>
                                    <li v-for="i in erros" v-text="i"></li>
                                </ul>
                            </div>
                            <div class="input-group mb-3 col-12  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Contrato</span>
                                </div>
                                <select v-model="dadosFiltroEquipamentos.CodigoContrato" class="form-control show-tick ms select2">
                                    <option value="Selecione o contrato">Selecione um contrato</option>
                                    <option :value="i.CodigoContrato" v-for="i in listaContratos" v-text="i.Numero+ '/'+ i.NomeEmpresa"></option>
                                </select>
                            </div>
                            <div class="input-group mb-3 col-12  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Nome Equipamento</span>
                                </div>
                                <input v-model="dadosFiltroEquipamentos.NomeEquipamento" class="form-control">
                            </div>
                            <div class="input-group mb-3 col-12  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Tipo Equipamento</span>
                                </div>
                                <!-- <input v-model="dadosFiltroEquipamentos.TipoEquipamento" maxlength="20" placeholder="EX: Armadilha Flora" class="form-control"> -->

                                <select v-model="dadosFiltroEquipamentos.TipoEquipamento" class="form-control show-tick ms select2">
                                    <option value="Eletrônico">Eletrônico</option>
                                    <option value="Mecânico">Mecânico</option>
                                    <option value="Outro">Outro</option>
                                </select>
                            </div>
                            <div class="input-group mb-3 col-12  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Patrimonio</span>
                                </div>
                                <input v-model="dadosFiltroEquipamentos.Patrimonio" maxlength="9" placeholder="EX: Armadilha Flora" class="form-control">
                            </div>
                            <div class="input-group mb-3 col-12  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Número Interno</span>
                                </div>
                                <input v-model="dadosFiltroEquipamentos.NumeroEquipamento" maxlength="9" class="form-control">
                            </div>
                            <div class="input-group mb-3 col-12  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Descrição/ Modelo</span>
                                </div>
                                <input v-model="dadosFiltroEquipamentos.DescricaoModelo" class="form-control">
                            </div>
                            
                            <div class="input-group mb-3 col-12   mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Especificação Técnica</span>
                                </div>
                                <textarea v-model="dadosFiltroEquipamentos.EspecificacaoTecnica" class="form-control"></textarea>
                            </div>
                            <div class="input-group mb-3 col-12   mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Observações</span>
                                </div>
                                <textarea  v-model="dadosFiltroEquipamentos.Observacoes" class="form-control"></textarea>
                            </div>
                            <div class="input-group mb-3 col-12 col-sm-6  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Numero de Série</span>
                                </div>
                                <input v-model="dadosFiltroEquipamentos.NumeroSerie" class="form-control">
                            </div>
                            <div class="input-group mb-3 col-12 col-sm-6  mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Código SICRO</span>
                                </div>
                                <input type="number" v-model="dadosFiltroEquipamentos.CodigoSICRO" class="form-control">
                            </div>
                            <div class="input-group mb-3 col-12 mb-4">
                                <div class="input-group-prepend">
                                    <span id="basic-addon1" class="input-group-text">Foto do Equipamentos</span>
                                </div>
                                <input type="file" ref="Foto" name="Foto" required class="form-control">
                            </div>
                            <div class="col-12 text-right">
                                <button @click="resetarform()" class="btn btn-outline-secondary">Cancelar</button>
                                <button @click="checkForm" class="btn btn-secondary">Salvar</button>
                            </div>

                        </div>


                    </div>
                    <!-- formulario para adicionar/Editar Equipamentos  -->


                    <div class="body mt-4">
                        <table class="table table-striped" id="listadeEquipamentos">
                            <thead>
                                <th>Foto</th>
                                <th>Contrato</th>
                                <th>Equipamento</th>
                                <th>Descrição/Modelo</th>
                                <th>Num. Série</th>
                                <th>Especificação</th>
                                <th>Obs</th>
                                <th>Ação</th>
                            </thead>
                            <tbody>
                                <tr v-for="(i, index) in listaEquipamentos">
                                    <td><img :src="i.Foto" class="img-thumbnail"></td>
                                    <td v-text="i.Numero"></td>
                                    <td v-text="i.NomeEquipamento"></td>
                                    <td v-text="atob(i.DescricaoModelo)"></td>
                                    <td v-text="i.NumeroSerie"></td>
                                    <td v-text="atob(i.EspecificacaoTecnica)"></td>
                                    <td v-text="atob(i.Observacoes)"></td>
                                    <td><button @click="deletar(i.CodigoEquipamento, i.index)" class="btn btn-outline-danger btn-sm"><i class="fa fa-times"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('cgmab/Equipamentos/scripts/appListar')?>