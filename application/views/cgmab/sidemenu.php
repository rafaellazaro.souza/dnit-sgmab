<div class="sidebar">

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview">
                <a href="<?= base_url('home') ?>" class="nav-link">
                    <i class="fa fa-home nav-icon"></i>
                    <p>Início</p>
                </a>
            </li>
            
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">

                    <p>
                        Gestão de Contratos
                        <i class="fas fa-angle-left right"></i>
                        <!--                <span class="badge badge-info right">6</span>-->
                    </p>
                </a>
                <ul class="nav nav-treeview" style="display: none;">
                    <li class="nav-item <?php echo $this->session->userdata('PerfilLogin') === 'DNIT' ? '' : 'invisivel'?>">
                        <a  href="<?= base_url('contratos') ?>" class="nav-link">

                            <p>Adicionar Contratos</p>
                        </a>
                    </li>
                    <li class="nav-item  <?php echo $this->session->userdata('PerfilLogin') === 'DNIT' ? '' : 'invisivel'?>">
                        <a href="<?= base_url('empreendimentos') ?>" class="nav-link">

                            <p>Empreendimentos</p>
                        </a>
                    </li>
                    <li class="nav-item  <?php echo $this->session->userdata('PerfilLogin') === 'Contratada' ? '' : 'invisivel'?>">
                        <a  href="<?= base_url('contratos/listar') ?>" class="nav-link">

                            <p>Meus Contratos</p>
                        </a>
                    </li>
                


                </ul>
            </li>

            
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">

                    <p>
                        Getsão de Licenças Ambientais
                        <i class="fas fa-angle-left right"></i>
                        <!--                <span class="badge badge-info right">6</span>-->
                    </p>
                </a>
                <ul class="nav nav-treeview" style="display: none;">
                <li class="nav-item  <?php echo $this->session->userdata('PerfilLogin') === 'DNIT' ? '' : 'invisivel'?>">
                        <a href="<?= base_url('licenciamentoambiental') ?>" class="nav-link">

                            <p>Licenças</p>
                        </a>
                    </li>
                    
                    <li class="nav-item  <?php echo $this->session->userdata('PerfilLogin') === 'DNIT' ? '' : 'invisivel'?>">
                        <a href="<?= base_url('orgaolicenciamentos') ?>" class="nav-link">

                            <p>Órgãos de Licenciamento</p>
                        </a>
                    </li>
                    <li class="nav-item  <?php echo $this->session->userdata('PerfilLogin') === 'DNIT' ? '' : 'invisivel'?>">
                        <a href="<?= base_url('orgaosexpeditores') ?>" class="nav-link">
                            <p>Órgãos Expeditores</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('condicionantes?CodigoLicenca=0') ?>" class="nav-link">

                            <p>Codicionantes</p>
                        </a>
                    </li>


                </ul>
            </li>

            <li class="nav-item has-treeview  <?php echo $this->session->userdata('PerfilLogin') === 'Contratada' ? '' : 'invisivel'?>">
                <a href="#" class="nav-link">

                    <p>
                        Gestão de Cadastros
                        <i class="fas fa-angle-left right"></i>
                        <!--                <span class="badge badge-info right">6</span>-->
                    </p>
                </a>
                <ul class="nav nav-treeview" style="display: none;">
                    <li class="nav-item">
                       <a href="<?= base_url('RecursosHumanos') ?>" class="nav-link">

                            <p>Recursos Humanos</p>
                        </a>
                    </li>
                    <li class="nav-item">
                       <a href="<?php //echo base_url('pessoas') ?>" class="nav-link">

                            <p>Materiais</p>
                        </a>
                    </li>
                    <li class="nav-item">
                       <a href="<?php //echo base_url('pessoas') ?>" class="nav-link">

                            <p>Equipamentos</p>
                        </a>
                    </li>
                    <li class="nav-item">
                       <a href="<?php //echo base_url('pessoas') ?>" class="nav-link">

                            <p>Veículos</p>
                        </a>
                    </li>


                </ul>
            </li>






        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>