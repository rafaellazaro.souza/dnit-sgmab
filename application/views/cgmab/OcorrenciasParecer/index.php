<style>
    .badge-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }

    .badge-warning {
        color: #212529;
        background-color: #ffc107;
    }
</style>
<div class="body_scroll" id="diario-content-app">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Registro de Ocorrências</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('HistoricoValidacao') ?>">Histórico de Validação</a>
                    </li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>
    <div class="container-fluid table-responsive" id="app-historico">
        <table id="tblOcorrencias" class="table table-hoover table-striped table-sm display compact" style="width: 100;">
            <thead>
                <tr>
                    <th>Contratos</th>
                    <th>Registro</th>
                    <th>BR</th>
                    <th>Tipo de Ocorrência</th>
                    <th>Data do Registro</th>
                    <th>Prazo de Solução</th>
                    <th>Data de Envio de Validação</th>
                    <th>Parecer</th>
                    <th>Data do Parecer</th>
                    <th>Data de Correção</th>
                    <td>-</td>
                </tr>
            </thead>
            <tbody>
                <tr v-for="ocrr in ocorrencias">
                    <td>{{ocrr.Numero}}</td>
                    <td>{{ocrr.CodigoOcorrencia}}</td>
                    <td>BR-{{ocrr.BR}} {{ocrr.UF}}</td>
                    <td>{{ocrr.TipoOcorrencia}} <span class="badge  bg-danger text-white" v-if="getDiferencaEmDias(ocrr)">Dias Vencidos: {{getDiferencaEmDias(ocrr)}}</span></td>
                    <td>{{formataData(ocrr.DataRegistro)}}</td>
                    <td>{{formataData(ocrr.PrazoSolucao)}}</td>
                    <td>{{formataData(ocrr.DataEnvioValidacao)}}</td>
                    <td class="text-white"><span class="badge " :class="getClass(ocrr)">{{getTextStatus(ocrr)}} <span></td>
                    <td>{{formataData(ocrr.DataParecer)}}</td>
                    <td>{{formataData(ocrr.NovoPrazoSolucao)}}</td>
                    <td>
                        <a href="javascript:void(0)" class="btn btn-outline-info btn-sm" @click="openOcorrencia(ocrr)"><i class="fa fa-edit"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script>
    const historioOcorrencai = new Vue({
        el: '#app-historico',
        data() {
            return {
                ocorrencias: []
            }
        },
        methods: {
            async getOcorrencias() {
                await axios.post(base_url + 'DiarioAmbiental/getOcorrenciasDiario', $.param({
                        OrdenaPorUltimaFaseInserida: 1,
                    }))
                    .then((response) => {
                        this.ocorrencias = response.data.ocorrencias;
                        this.refreshTable('#tblOcorrencias');
                    });
            },
            constroiTable(id = "") {
                $('table' + id, this.$el).DataTable({
                    language: translateDataTable,
                    lengthChange: false,
                    destroy: true
                });
            },
            async refreshTable(id = "") {
                $('table' + id, this.$el).DataTable().destroy();
                await this.$nextTick(() => {
                    this.constroiTable();
                });
            },
            formataData(data) {
                if (data == null || data.length == 0) {
                    return '';
                }
                return vmGlobal.frontEndDateFormat(data);
            },
            async openOcorrencia(ocorrenca) {
                await this.verificaDiario(ocorrenca.CodigoDiario);
                window.location.href = base_url + 'Ocorrencia/update/' + vmGlobal.codificarDecodificarParametroUrl(ocorrenca.CodigoOcorrencia, 'encode');
            },
            getClass(ocorrencia) {
                var situacao = this.getTextStatus(ocorrencia);
                situacao = situacao.replace(/\n\r\t/g, '');
                if (situacao === 'Aprovado') {
                    return 'bg-success';
                }

                if (situacao.includes('Não Realizado') || situacao.includes('Rejeitado')) {
                    return 'bg-danger';
                }

                if (situacao.includes('Parcialmente Aprovado')) {
                    return 'badge-rejeitado';
                }

                if (situacao.includes('Em Validação')) {
                    return 'badge-primary';
                }

                if (situacao.includes('Em Andamento')) {
                    return 'badge-warning';
                }

                return 'badge-default';
            },
            getDiferencaEmDias(ocorrencia) {
                if (ocorrencia.TipoOcorrencia == 'RNC') {
                    var data = ocorrencia.PrazoSolucao.split('-');
                    var dt1 = new Date(data[1] + '/' + data[2] + '/' + data[0]);
                    var today = new Date();

                    if (dt1.getTime() < today.getTime()) {
                        const diffTime = Math.abs(dt1 - today);
                        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

                        return diffDays;
                    }
                }

                return ''
            },
            async verificaDiario(diario) {
                await axios.post(base_url + '/DiarioAmbiental/verificaOcorrencias', $.param({
                    CodigoDiario: diario
                }));
            },
            getTextStatus(ocorrencia) {
                if (ocorrencia.Status.includes('Em Andamento')) {
                    return ocorrencia.Status;
                }

                if (ocorrencia.Status.includes('Em Validação')) {
                    return ocorrencia.Status;
                }

                return ocorrencia.SituacaoPerecer;
            }
        },
        mounted() {
            this.constroiTable('#tblOcorrencias');
            this.getOcorrencias();
        },
    })
</script>