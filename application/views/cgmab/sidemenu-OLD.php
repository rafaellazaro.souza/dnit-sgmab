<div class="sidebar">

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview">
                <a href="<?= base_url('home') ?>" class="nav-link">
                    <i class="fa fa-home nav-icon"></i>
                    <p>Início</p>
                </a>
            </li>
            <li class="nav-item has-treeview">
                <a href="<?= base_url('licenciamentoambiental') ?>" class="nav-link">
                    
                    <p>Licenciamento Amb.</p>
                </a>
            </li>
            <li class="nav-item has-treeview">
                <a href="<?= base_url('configuracao') ?>" class="nav-link">
                    
                    <p>Configuração</p>
                </a>
            </li>
            <li class="nav-item has-treeview">
                <a href="<?= base_url('gestaoambiental') ?>" class="nav-link">
                    
                    <p>Gestão Ambiental</p>
                </a>
            </li>
         
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    
                    <p>
                        Gerenciamento Amb.
                        <i class="fas fa-angle-left right"></i>
        <!--                <span class="badge badge-info right">6</span>-->
                    </p>
                </a>
                <ul class="nav nav-treeview" style="display: none;">
                    <li class="nav-item">
                        <a href="<?= base_url('gerenciamentoambiental/relatorio_mobilizacao') ?>" class="nav-link">
                            
                            <p>Relatório de Mobilização</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('gerenciamentoambiental/relatorio_zero') ?>" class="nav-link">
                            
                            <p>Relatório Zero</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('gerenciamentoambiental/relatorio_mensais') ?>" class="nav-link">
                            
                            <p>Relatórios Mensais</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('gerenciamentoambiental/relatorio_periodicos') ?>" class="nav-link">
                            
                            <p>Relatórios periódicos</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('gerenciamentoambiental/relatorio_final') ?>" class="nav-link">
                            
                            <p>Relatórios Final</p>
                        </a>
                    </li>
                  

                </ul>
            </li>

            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    
                    <p>
                        Supervisão Ambiental
                        <i class="fas fa-angle-left right"></i>
        <!--                <span class="badge badge-info right">6</span>-->
                    </p>
                </a>
                <ul class="nav nav-treeview" style="display: none;">
                    <li class="nav-item">
                        <a href="<?= base_url('') ?>" class="nav-link">
                            
                            <p>Diário Ambiental</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('') ?>" class="nav-link">
                            
                            <p>ROA</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('') ?>" class="nav-link">
                            
                            <p>ROC</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('') ?>" class="nav-link">
                            
                            <p>RNC</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('') ?>" class="nav-link">
                            
                            <p>Relatório de Supervisão</p>
                        </a>
                    </li>

                </ul>
            </li>


            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    
                    <p>
                        Execução de ambientes
                        <i class="fas fa-angle-left right"></i>
        <!--                <span class="badge badge-info right">6</span>-->
                    </p>
                </a>
                <ul class="nav nav-treeview" style="display: none;">
                    <li class="nav-item">
                        <a href="<?php echo site_url('cgmab/ProgramasPlanosAmbientais/configuracoes') ?>" class="nav-link">
                            
                            <p>Configuração</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo site_url('cgmab/ProgramasPlanosAmbientais/execucao') ?>" class="nav-link">
                            
                            <p>Programas Ambientais</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('') ?>" class="nav-link">
                            
                            <p>PAC</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('') ?>" class="nav-link">
                            
                            <p>Outras Condicionalidades</p>
                        </a>
                    </li>

                </ul>
            </li>







        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>