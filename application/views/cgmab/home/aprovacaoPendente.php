<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }
</style>
<div class="body_scroll" id="aprovacaoPendente">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Lista de Contratos Aprovação</h2>
                <ul class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item">Contratos</li>
                    <li class="breadcrumb-item active">Aprovação</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <table class="table table-striped" id="tblListagem">
                            <thead>
                                <tr>

                                    <th>Código</th>
                                    <th>Numero</th>
                                    <th>Empresa</th>
                                    <th>CNPJ</th>
                                    <th>Edital</th>
                                    <th>Empreendimento</th>
                                    <th>Município</th>
                                    <th>Status</th>
                                    <th>Selecionar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="i in listaDados">

                                    <td>{{i.CodigoContrato}}</td>
                                    <td>{{i.Numero}}</td>
                                    <td>{{i.NomeEmpresa}}</td>
                                    <td>{{i.CnpjEmpresa}}</td>
                                    <td>{{i.Edital}}</td>
                                    <td>{{i.Sigla}}</td>
                                    <td>{{i.Municipio}}</td>
                                    <td>
                                        <template v-if="i.Status === '2' || i.Status === 2">
                                            <button class="btn btn-danger">Pendente validação de cadastro</button>
                                        </template>
                                        <template v-if="i.Status === '4' || i.Status === 4">
                                            <button class="btn btn-success">Cadastro Deferido</button>
                                        </template>
                                        <template v-if="i.Status === '5' || i.Status === 5">
                                            <button class="btn btn-rejeitado w-100 pt-2 pb-2">Parcialmente Aprovado</button>
                                            <span>Prazo: {{formataData(i.PrazoCorrecao)}}</span>
                                        </template>
                                    </td>
                                    <td>
                                        <a :href="'<?php echo base_url('contratos/dados/') ?>' + vmGlobal.codificarDecodificarParametroUrl(i.CodigoContrato, 'encode')+'/'+i.CodigoContratada" class="btn btn-sm btn-outline-secondary"><i class="far fa-folder-open"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('cgmab/home/scripts/appAprovacaoPendente'); ?>