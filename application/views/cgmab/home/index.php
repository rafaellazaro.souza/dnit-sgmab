<?php $this->load->view('cgmab/home/css.php') ?>


<section class="content-header">
    <div class="container-fluid">

        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>CGMAB</h1>
            </div>

        </div>

        <div class="row mt-5">
            <div class="col-12 col-sm-4  border text-center">
                <div class="card p-4">
                <a href="<?= base_url('home/login?Perfil=DNIT') ?>">
                        <i class="fa fa-list mb-4"></i>
                        <h5>Perfil DNIT</h5>
                    </a>

                </div>

            </div>
            <div class="col-12 col-sm-4  border text-center">
                <div class="card p-4">
                <a href="<?= base_url('home/login?Perfil=Fiscal') ?>">
                        <i class="fa fa-list mb-4"></i>
                        <h5>Fiscal</h5>
                    </a>

                </div>

            </div>
            <div class="col-12 col-sm-4 border text-center">
                <div class="card p-4">
                    <a href="<?= base_url('home/login?Perfil=Contratada') ?>">
                        <i class="far fa-file mb-4"></i>
                        <h5>Perfil Contratada</h5>
                    </a>

                </div>
            </div>
            <!-- <div class="col-12 col-sm-4  text-center">
                <div class="card p-4">
                    <a href="#">
                        <i class="fas fa-tachometer-alt mb-4"></i>
                        <h5>Ambiente de Gestão</h5>
                    </a>

                </div>
            </div> -->

        </div>
    </div>
</section>


