<script>
    var vmAprovacaoPendente = new Vue({
        el: '#aprovacaoPendente',
        data: {
            listaDados: [],
        },
        async mounted() {
            await this.getContratos();
            $('#preloader').hide();
        },
        methods: {
            async getContratos() {
                $(".page-loader-wrapper").css('display', 'block')
                $('#tblListagem').DataTable().destroy();
                // var params = {
                //     table: 'contratos',
                //     join: {
                //         table: 'contratadas',
                //         on: ''
                //     },
                //     where: {
                //         Status: 2
                //     }
                // }
                // this.listaDados = await vmGlobal.getFromAPI(params, 'cgmab');
                await axios.post(base_url + 'Contratos/getContratadasContrato')
                    .then((resp) => {
                        this.listaDados = resp.data;
                    })
                vmGlobal.montaDatatable('#tblListagem', true);
            },
            habilitarEdicao(val) {
                if (parseInt(val.Status) == 5 && this.contratoAtrazado(val.PrazoCorrecao)) {
                    return true;
                }

                return [2, 4].includes(parseInt(val));
            },
            contratoAtrazado(val) {
                if (val) {
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var prazoCorrecao = moment(val).toDate();
                    prazoCorrecao.setHours(0, 0, 0, 0);

                    return prazoCorrecao.getTime() < today.getTime();
                }

                return false;
            },
            formataData(val) {
                if (val) {
                    return moment(val).format('DD/MM/YYYY');
                }
                return '';
            }
        }
    })
</script>