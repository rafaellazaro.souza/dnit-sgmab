<style>
    .btn-rejeitado {
        background-color: #e8846d;
        color: #fff;
    }
</style>
<div class="body_scroll">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Atividades</h2>
                <ul class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Atividades</li>
                    <li class="breadcrumb-item">Atividades do Mês</li>
                </ul>
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>

        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-9">
                        <ul class="nav nav-tabs border-bottom" id="tabsAtropelamentoFauna" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link border active" id="atividades-tab" data-toggle="tab" href="#atividades-lista" role="tab" aria-controls="atividades-lista" aria-selected="true">Atividades</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link border" id="parecer-tab" data-toggle="tab" href="#parecer-lista" role="tab" aria-controls="parecer-lista" aria-selected="false">Parecer Técnico</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="tab-content p-4" id="myTabContent">
                    <div class="tab-pane fade show active" id="atividades-lista" role="tabpanel" aria-labelledby="atividades-tab">
                        <div class="card">
                            <div class="body" id="AtividadesMes">
                                <div class="row">
                                    <div class="offset-md-9 col-md-3 pr-4" v-if="false">
                                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Prazo para Envio:</strong> '' </p>
                                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Data de Envio:</strong> ''</p>
                                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Hora de Envio</strong> ''</p>
                                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Colaborador:</strong> ''</p>
                                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Status:</strong> <span class="badge" :class="''">''</span></p>
                                        <p class="pb-0 pt-0 mt-0 mb-0"><strong>Data de Correção:</strong> ''</p>
                                    </div>
                                    <div class="col-12">
                                        <h5>Atividades do Mês</h5>
                                        <div class="row mb-3 mt-3 border-left ml-1">

                                            <div class="form-group col-12 col-md-4">
                                                <label>Situação</label>
                                                <select v-model="status" class="form-control show-tick ms select2">
                                                    <option value="todas">Todas</option>
                                                    <option value="1">Iniciadas</option>
                                                    <option value="3">Concluídas</option>
                                                    <option value="0">Pendente Mes Atual</option>
                                                </select>
                                            </div>
                                        </div>
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Atividade</th>
                                                    <th>Tipo</th>
                                                    <th>Mes Cronograma</th>
                                                    <th>Início</th>
                                                    <th>Conclusão</th>
                                                    <th>Status</th>
                                                    <th>Ações</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="i in listaAtividades">
                                                    <td v-text="i.Atividade"></td>
                                                    <td>
                                                        <template v-if="i.PossuiRelatorio === 2 || i.PossuiRelatorio === '2'">Ação</template>
                                                        <template v-else>Relatório</template>
                                                    </td>
                                                    <td v-text="i.MesAtividade+'/'+i.AnoAtividade"></td>
                                                    <td>
                                                        <template v-if="i.DataAtacado">{{vmGlobal.frontEndDateFormat(i.DataAtacado)}}</template>
                                                        <template v-else>Não Iniciado</template>
                                                    </td>
                                                    <td>
                                                        <template v-if="i.DataRealizado">{{vmGlobal.frontEndDateFormat(i.DataRealizado)}}</template>
                                                        <template v-else>Não Concluído</template>
                                                    </td>
                                                    <td>
                                                        <!-- prevista -->
                                                        <template v-if="!i.DataAtacado">Pendente</template>
                                                        <!-- Iniciada -->
                                                        <template v-if="i.DataAtacado && !i.DataRealizado">Iniciada</template>
                                                        <!-- pendente -->
                                                        <template v-if="i.DataRealizado">Realizada</template>
                                                    </td>
                                                    <td>
                                                        <div class="dropdown">
                                                            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Ações
                                                            </a>

                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                                                                <!-- Não iniciado  -->
                                                                <template v-if="!i.DataAtacado && !i.DataRealizado">
                                                                    <a @click="statusAtividade(i.CodigoAtividadeCronogramaFisico, 'iniciar')" class="dropdown-item" href="#">Iniciar Atividade</a>
                                                                </template>

                                                                <!-- Iniciada -->
                                                                <template v-if="i.DataAtacado || i.DataRealizado">
                                                                    <a v-if="parseInt(i.PossuiRelatorio) == 1" href="javascript:;" class="dropdown-item" @click="iniciarRelatorio(i.CodigoEscopoPrograma, i.CodigoAtividadeCronogramaFisico, i.AnoAtividade, i.MesAtividade)">Editar</a>

                                                                    <a v-else @click="statusAtividade(i.CodigoAtividadeCronogramaFisico, 'iniciarForm', 'acao', i)" class="dropdown-item" href="#">Editar</a>
                                                                </template>

                                                                <!-- pendente
                                                                        <template v-if="i.Status === 3 || i.Status === '3'">
                                                                            <a class="dropdown-item" href="#">Exibir Dados</a>
                                                                        </template> -->
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <!-- **************************************************************
                                                    formulario finalizar atividade do tipo ação 
                                                 **************************************************************-->
                                        <?php $this->load->view('cgmab/Atividades/paginas/formFinalizarAcao'); ?>
                                        <!-- **************************************************************
                                                    Fim formulario finalizar atividade do tipo ação 
                                                 **************************************************************-->


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="parecer-lista" role="tabpanel" aria-labelledby="parecer-tab">
                        <div class="card" style="min-height: auto;" id="app-parecer">
                            <div class="body">
                                <?php $this->load->view('cgmab/Atividades/paginas/parecer'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('cgmab/Atividades/scripts/app'); ?>

<script>
    const parecerFiscal = new Vue({
        el: '#app-parecer',
        data() {
            return {
                selectedParecer: {
                    CodigoParecerFiscal: '',
                    CodigoFiscal: '',
                    CodigoRelatorioMensalPrograma: '',
                    StatusFiscal: '',
                    ParecerRelatorio: '',
                    ParecerAtividades: '',
                    DataEnvio: '',
                    DataParecer: '',
                    AntigaDataCorrecao: '',
                    PrazoCorrecao: ''
                },
                parecer: [],
                selectedParecer: {},
                showParecer: false,
                usuario: {
                    NomeUsuario: '',
                    CodigoUsuario: '',
                    Perfil: ''
                },
                hasRascunho: false
            }
        },
        watch: {
            'usuario.Perfil'(val) {
                if (val.includes('Fiscal')) {
                    var el = document.querySelector('#btnParecerFiscal');
                    $(el).css({
                        display: 'block'
                    });
                }
            }
        },
        computed: {
            isFiscal() {
                return true;
            },
            isConcluido() {
                return false;
            },
            isEmValidacao() {
                return false;
            },
            classParecer() {
                if (this.selectedParecer.StatusFiscal == 3) {
                    return 'badge-success';
                } else if (this.selectedParecer.StatusFiscal == 4) {
                    return 'badge-danger';
                } else if (this.selectedParecer.StatusFiscal == 2) {
                    return 'btn-rejeitado';
                }
                return ''
            },
            txtSituacao() {
                if (this.selectedParecer.StatusFiscal == 3) {
                    return 'Aprovado';
                } else if (this.selectedParecer.StatusFiscal == 4) {
                    return 'Rejeitado';
                } else if (this.selectedParecer.StatusFiscal == 2) {
                    return 'Parcialmente Aprovado';
                }
                return ''
            }
        },
        methods: {
            openParecer() {
                var param = parametrosEscopo.split('&')[0].split('=')[0]
                var relatorio = vmGlobal.codificarDecodificarParametroUrl(codigoRelatorio, 'encode');
                window.open(base_url + 'ParecerFiscal/validarAtividades?' + param + '=' + relatorio, 'parecerFiscal',
                    "width=800, height = 600, directories = no, location = no, menubar = no, scrollbars = no, status = no, toolbar = no, resizable = no "
                );
            },
            async getParecer() {
                await axios.post(base_url + 'ParecerFiscal/getParecerAtividades', $.param({
                        CodigoRelatorioMensalPrograma: codigoRelatorio
                    }))
                    .then((response) => {
                        var parecer = response.data.parecer.filter((el) => {
                            return el.StatusFiscal != 0;
                        });

                        var rascunho = response.data.parecer.filter((el) => {
                            return el.StatusFiscal == 0;
                        });

                        if (rascunho.length != 0) {
                            this.hasRascunho = true;
                        }

                        this.parecer = parecer;
                        this.usuario = response.data.usuario;
                        this.refreshTable('#tblParecerAtividades');
                    });
            },
            salvarParecer(status) {

            },
            async loadParecer(parecer) {
                this.selectedParecer = Object.assign({}, parecer);
                this.showParecer = true;

                for (index in Object.keys(this.selectedParecer)) {
                    var key = Object.keys(this.selectedParecer)[index];
                    if (['DataEnvio', 'DataParecer', 'AntigaDataCorrecao', 'PrazoCorrecao'].includes(key) && this.selectedParecer[key]) {
                        this.selectedParecer[key] = moment(parecer[key]).format('DD/MM/YYYY');
                    }
                }

                if (CKEDITOR.instances['txtParecerRelatorioView']) CKEDITOR.instances['txtParecerRelatorioView'].destroy();
                if (CKEDITOR.instances['txtParecerAtividadesView']) CKEDITOR.instances['txtParecerAtividadesView'].destroy();
                await this.$nextTick(() => {
                    CKEDITOR.replace('txtParecerRelatorioView', {
                        height: '400px',
                        readOnly: true
                    });
                    CKEDITOR.replace('txtParecerAtividadesView', {
                        height: '400px',
                        readOnly: true
                    });
                });

                await this.$nextTick(() => {
                    CKEDITOR.instances['txtParecerRelatorioView'].setData(String(atob(parecer.ParecerRelatorio)));
                    CKEDITOR.instances['txtParecerAtividadesView'].setData(String(atob(parecer.ParecerAtividades)));
                })
            },
            styleParecer(val) {
                if (val.StatusFiscal == 3) {
                    return 'badge-success';
                } else if (val.StatusFiscal == 4) {
                    return 'badge-danger';
                } else if (val.StatusFiscal == 2) {
                    return 'btn-rejeitado';
                }
                return '';
            },
            constroiTable(id, pageLength = 5) {
                $(id, this.$el).DataTable({
                    language: translateDataTable,
                    pageLength: pageLength,
                    lengthChange: false,
                    destroy: true
                });
            },
            refreshTable(id, pageLength = 5) {
                $(id, this.$el).DataTable().destroy();
                this.$nextTick(() => {
                    this.constroiTable(id, pageLength);
                });
            },
            formatarData(data) {
                if (data) {
                    var date = data.split(' ');
                    date[0] = date[0].split('-').reverse().join('/');
                    return date[0];
                }

                return '';
            },
            txtParecer(parecer) {
                if (parecer.StatusFiscal == 3) {
                    return 'Aprovado';
                } else if (parecer.StatusFiscal == 4) {
                    return 'Rejeitado';
                } else if (parecer.StatusFiscal == 2) {
                    return 'Parcialmente Aprovado';
                }
                return ''
            }
        },
        mounted() {
            this.getParecer();
        },
    });
</script>