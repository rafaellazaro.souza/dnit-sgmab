    <div class="offset-md-9 col-md-3 d-flex align-items-center">
        <button class="btn btn-sm btn-success" id="btnParecerFiscal" @click="openParecer()" style="display: none;">{{(hasRascunho)? "Retomar Parecer" : "Novo Parecer"}}</button>
    </div>
    <h3>Histórico de Validações</h3>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm table-bordered text-center no-footer compact" id="tblParecerAtividades">
                <thead>
                    <tr>
                        <th>Fiscal</th>
                        <th>Parecer</th>
                        <th>Data do Parecer</th>
                        <th>Data de Correção</th>
                        <th>-</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="par in parecer">
                        <td>{{par.NomeUsuario}}</td>
                        <td><span class="badge" :class="styleParecer(par)">{{txtParecer(par)}}</span></td>
                        <td>{{formatarData(par.DataParecer)}}</td>
                        <td>{{formatarData(par.PrazoCorrecao)}}</td>
                        <td>
                            <button class="btn btn-sm btn-outline-secondary" @click="loadParecer(par)"><i class="zmdi zmdi-eye"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row border border-secondary p-2 mt-2" v-if="showParecer">
        <div class="col-md-12">
            <button class="float-right btn btn-sm btn-primary" @click="showParecer=false">x</button>
            <div class="form-group mt-2">
                <p class="p-0 m-0"><strong>Fiscal:</strong> {{selectedParecer.NomeUsuario}}</p>
                <p class="p-0 m-0"><strong>Data do Parecer:</strong> {{selectedParecer.DataParecer}}</p>
                <p class="p-0 m-0"><strong>Prazo de Correção Anterior:</strong> {{selectedParecer.AntigaDataCorrecao}}</p>
                <p class="p-0 m-0"><strong>Prazo de Correção:</strong> {{selectedParecer.PrazoCorrecao}}</p>
                <p class="pb-0 pt-0 mt-0 mb-0"><strong>Status:</strong> <span class="badge" :class="classParecer">{{txtSituacao}}</span></p>
                <label for="">Parecer Atividade:</label>
                <textarea class="form-control" rows="4" id="txtParecerAtividadesView"></textarea>
                <label for="">Parecer Relatório:</label>
                <textarea class="form-control" rows="4" id="txtParecerRelatorioView"></textarea>
            </div>
        </div>
    </div>