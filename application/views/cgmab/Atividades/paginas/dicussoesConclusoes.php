<div class="row" id="discucoes">
    <div class="col-12">
        <h4>Análises dos Resultados e Discussões</h4>
        <textarea name="analiseResultadosDiscucoes" id="txTanaliseResultadosDiscucoes"></textarea>
        <div class="col-12 text-right">
            <button @click="salvarTexto('AnalisesResultados')" class="btn btn-success">Atualizar</button>
        </div>
        <h4>Conclusões</h4>
        <textarea name="analiseResultadosConclusoes" id="txTanaliseResultadosConclusoes"></textarea>
        <div class="col-12 text-right">
            <button @click="salvarTexto('Conclusoes')" class="btn btn-success">Atualizar</button>
        </div>
        <h4>Bibliografia</h4>
        <textarea name="analiseResultadosBibliografia" id="txTanaliseResultadosBibliografia"></textarea>
        <div class="col-12 text-right">
            <button @click="salvarTexto('Bibliografia')" class="btn btn-success">Atualizar</button>
        </div>
    </div>
</div>