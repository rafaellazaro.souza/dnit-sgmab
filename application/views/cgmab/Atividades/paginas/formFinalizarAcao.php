<div class="modal-w98-h50 sombra-2 p-5" v-show="mostrarFormFinalizar">
    <div class="row">
        <div class="col-8">
            <h4>{{atividadeSelecionadaDados.Atividade}} Mes: {{atividadeSelecionadaDados.MesAtividade}}/{{atividadeSelecionadaDados.AnoAtividade}}</h4>
        </div>
        <div class="col-4 text-right">
            <button @click="mostrarFormFinalizar = false" class="btn btn-secondary "><i class="fa fa-times"></i></button>
            <button @click="statusAtividade(atividadeSelecionada, 'finalizar')" class="btn btn-success ml-4">Finalizar Atividade</button>
        </div>
    </div>
    <ul class="nav nav-tabs" role="tablist" id="myTab">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#intro" role="tab" aria-controls="home" aria-selected="true">Introdução</a>
        </li>
        <li class="nav-item" role="presentation" v-show="atividadeDados.AcoesExecutadas">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#fotos" role="tab" aria-controls="profile" aria-selected="false">Registro Fotográfico</a>
        </li>
        <li class="nav-item" role="presentation" v-show="atividadeDados.AcoesExecutadas  && tipoAtivideadeSelecionada != 'acao'">
            <!-- && tipoAtivideadeSelecionada != 'acao'-->
            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#conclusoes" role="tab" aria-controls="contact" aria-selected="false">Discussões e Conclusões</a>
        </li>
        <li class="nav-item" role="presentation" v-show="atividadeDados.AcoesExecutadas  && tipoAtivideadeSelecionada != 'acao' && vmGlobal.dadosDoUsuario.CodigoPerfil != 3">
            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#parecer" role="tab" aria-controls="contact" aria-selected="false">Parecer Técnico</a>
        </li>
        <li class="nav-item" role="presentation" v-show="atividadeDados.AcoesExecutadas">
            <a @click="getRecursosServico('profisionais')" class="nav-link" id="contact-tab" data-toggle="tab" href="#recursos" role="tab" aria-controls="contact" aria-selected="false">Recursos Utilizados</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <!-- introdução      -->
        <div class="tab-pane fade show active" id="intro" role="tabpanel" aria-labelledby="home-tab">
            <div class="row">
                <div class="col-12 col-sm-4 border-right pr-3 ">
                    <div class="card">

                        <div class="bloqueio text-center" v-show="bloqueioUpload">
                            <i data-wow-duration="1s" data-wow-offset="2" data-wow-iteration="2" class="fas fa-spinner wow bounce" style="visibility: visible; animation-duration: 1s; animation-iteration-count: 10; animation-name: bounce;"></i>
                            <h5 class="mt-4">Aguarde ...</h5>
                        </div>
                        <div class="header">
                            <h5>Documentação Auxiliar</h5>
                            <h2> Documentos <i class="fas fa-file-alt"></i></h2>
                        </div>
                        <div class="body no-shadow border">
                            <form enctype="multipart/form-data" method="POST">
                                <input type="file" id="documentosMaterial" ref="documentos" name="documentos[]" class="dropify" multiple>
                            </form>
                        </div>
                        <button class="btn btn-outline-secondary mt-2 float-right" @click="addDocumentos(); bloqueioUpload = true">Importar</button>
                        <div class="h450-r mt-5">
                            <table class="table table-striped mt-2">
                                <tbody>
                                    <tr v-for="i in listaDocumentos">
                                        <td v-text="i.Descricao"></td>
                                        <td><a :href="i.Caminho"><i class="fa fa-download"></i></a></td>
                                        <td><button @click="deletarArquivo(i.CodigoAnexo, i.Caminho)" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-sm-8">
                    <div class="border p-3" v-if="!atividadeDados.AcoesExecutadas">
                        <p>
                            Preencha a descrição das atividades realizadas e aperte o botão Salvar, em seguida outras abas serão apresentadas para devido preenchimento
                        </p>
                    </div>
                    <h4 class="mb-4">Descrição das atividades realizadas</h4>
                    <textarea name="finalizar"></textarea>

                    <div class="col-12 text-right">
                        <button class="btn btn-lg btn-infos" @click="atualizarDados(atividadeSelecionada, 'Atividade')">Salvar Descrição</button>
                    </div>
                </div>
            </div>

        </div>
        <!-- fotos  -->
        <div class="tab-pane fade" id="fotos" role="tabpanel" aria-labelledby="profile-tab">
            <form enctype="multipart/form-data" method="POST" v-show="atividadeDados.AcoesExecutadas">
                <h4 class="mt-4 mb-4">Fotos da atividade</h4>
                <div class="body">
                    <input type="file" id="files" ref="files" multiple @change="arquivosSelecionados()" class="dropify">
                </div>
                <button type="button" class="btn btn-primary float-right mb-4" @click="enviarArquivos()">IMPORTAR</button>
            </form>


            <div class="row file_manager mt-5">
                <div v-for="i in listaFotosAtividade" class="col-lg-4 col-md-4 col-sm-12">
                    <div class="card " :class="'borderGreen'">
                        <div class="file">
                            <div class="hover">
                                <button @click="deletarFoto(i.CodigoAtivCronFisicoFotos, i.Caminho)" type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                            </div>
                            <div class="image">
                                <img :src="i.Caminho" alt="img" style="height: 270px!important; width: 100%;" class="img-fluid">
                            </div>
                            <div class="file-name mb-3">
                                <div class="row">
                                    <div class="col-12 col-sm-6">Latitude: {{i.Lat}}</div>
                                    <div class="col-12 col-sm-6 text-right">Longitude: {{i.Long}}</div>
                                </div>
                            </div>
                            <div class="file-name">
                                <label cla>UTM (Data Sirgas 2000)</label><br>
                                <div class="row">
                                    <div class=" col-md-3">
                                        <label for="email_address">Zona</label>
                                        <div class="form-group">
                                            <input v-if="i.Zona" :value="i.Zona" disabled class="form-control" placeholder="">
                                            <input v-else v-model="modelFotos.Zona" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class=" col-md-3">
                                        <label for="email_address">CN</label>
                                        <div class="form-group  ">
                                            <input v-if="i.CN" :value="i.CN" disabled class="form-control" placeholder="">
                                            <input v-else v-model="modelFotos.CN" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class=" col-md-3">
                                        <label for="email_address">CE</label>
                                        <div class="form-group  ">
                                            <input v-if="i.CE" :value="i.CE" disabled class="form-control" placeholder="">
                                            <input v-else v-model="modelFotos.CE" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class=" col-md-3">
                                        <label for="email_address  ">Elev. (m)</label>
                                        <div class="form-group ">
                                            <input v-if="i.Elevacao" :value="i.Elevacao" disabled class="form-control" placeholder="">
                                            <input v-else v-model="modelFotos.Elevacao" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class=" col-md-12">
                                        <label for="email_address  ">Observações</label>
                                        <div class="form-group ">
                                            <input v-if="i.Observacoes" :value="i.Observacoes" disabled class="form-control" placeholder="">
                                            <textarea v-else v-model="modelFotos.Observacoes" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="file-name">
                            <label for="email_address">Observações</label>
                            <div class="form-group">
                                <textarea v-if="i.Lat && i.DataFoto" rows="4" class="form-control" placeholder="">{{i.Observacoes}}</textarea>
                                <textarea v-else v-model="modelFotos.Observacoes" rows="4" disabled class="form-control" placeholder="A foto não está de acordo com o manual"></textarea>
                            </div>
                        </div> -->
                            <div class="file-name">
                                <!-- <div class="alert alert-warning" v-show="!i.Lat">
                                    <p>Foto sem coordenadas! Deletar e efetuar o upload de fotos georeferenciadas!</p>
                                </div> -->
                                <button @click="atualizarDados(i.CodigoAtivCronFisicoFotos, 'Foto')" class="btn btn-success">Salvar</button>
                                <button @click="deletarFoto(i.CodigoAtivCronFisicoFotos, i.Caminho)" v-else class="btn btn-danger">Excluir</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- discurções e conclusões  -->
        <div class="tab-pane fade" id="conclusoes" role="tabpanel" aria-labelledby="contact-tab">
            <?php $this->load->view('cgmab/Atividades/paginas/dicussoesConclusoes'); ?>
        </div>
        <!-- Parecer Técnico  -->
        <div class="tab-pane fade" id="parecer" role="tabpanel" aria-labelledby="contact-tab">
            <h4>Parecer Técnico</h4>
            <textarea name="parecerTecnico" id="txTparecerTecnico"></textarea>
            <div class="col-12 text-right">

                <button @click="salvarTexto('Parecer', )" class="btn btn-success">Atualizar</button>
            </div>
        </div>
        <!-- Recursos Utilizados  -->
        <div class="tab-pane fade" id="recursos" role="tabpanel" aria-labelledby="contact-tab">
            <?php $this->load->view('cgmab/Atividades/paginas/recursos'); ?>
        </div>
    </div>




</div>

<script>

</script>