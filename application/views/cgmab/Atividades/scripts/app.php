<script>
    // var parametrosEscopo = location.search.slice(1);
    // var codigoEscopoPrograma = vmGlobal.codificarDecodificarParametroUrl(parametrosEscopo.split('=')[1], 'decode');
    var parametrosEscopo = location.search.slice(1);
    var codigoEscopoPrograma = vmGlobal.codificarDecodificarParametroUrl(parametrosEscopo.split('&')[0].split('=')[1], 'decode');
    var numeroContrato = vmGlobal.codificarDecodificarParametroUrl(location.search.slice(1).split('&')[1].split('=')[1], 'decode');
    var codigoRelatorio = vmGlobal.codificarDecodificarParametroUrl(parametrosEscopo.split('&')[2].split('=')[1], 'decode');

    var vmAtividadesMes = new Vue({
        el: '#AtividadesMes',
        data() {
            return {
                files: '',
                atividadeSelecionada: '',
                atividadeSelecionadaDados: '',
                codigoRelatorio: codigoRelatorio,
                listaFotosAtividade: [],
                modelFotos: {},
                mostrarFormFinalizar: false,
                listaAtividades: {},
                atividadeDados: {},
                pendentes: 0,
                listaAnos: [],
                tipoAtivideadeSelecionada: '',
                // mes: new Date().getMonth() + 1,
                // ano: new Date().getFullYear(),
                status: null,
                bloqueioUpload: false,

                //recursos utilizados
                mostrarProfissionaisServico: false,
                listaRecursosPrograma: [],
                listaRecursosServico: [],
                contrato: '<?= $this->session->userdata('codigoContratoSelecionado'); ?>',
                opcao: '',

                //discussoes conclusoes, bibliografia
                analiseResultadosDiscucoes: '',
                analiseResultadosConclusoes: '',
                analiseResultadosBibliografia: '',

                //anexos
                introducao: [],
                listaDocumentos: [],
                totalDocumentos: 0,
                qtdDocumentos: 0,
            }
        },
        watch: {

            status: function(val) {
                if (val === 'todas') {
                    this.getAtividadesTodos();
                    this.status = '';
                } else {
                    this.getAtividades();
                }
            },
            opcao: function(val) {
                this.getRecursosRelatorio();
            }
        },
        async mounted() {
            // await this.gerarAnos();
            await this.getAtividades();
            // await this.calcularPendentes();
            await vmGlobal.dadosUsuario();

        },
        methods: {
            async getAtividades(codigo = null) {
                // this.listaAtividades = {};
                var params;
                params = {
                    CodigoRelatorio: codigoRelatorio,
                }

                if (codigo) {
                    params = {
                        CodigoAtividadeCronogramaFisico: codigo
                    }
                }

                if (this.status != null) {
                    params = {
                        CodigoRelatorio: codigoRelatorio,
                        Status: this.status
                    }
                }
                await axios.post(base_url + 'atividades/getAtividadesMes', $.param(params))
                    .then((resp) => {
                        if (codigo) {
                            this.atividadeDados = resp.data[0];
                            console.log(codigo);
                        } else {
                            this.listaAtividades = resp.data;
                        }
                    });

            },
            async getAtividadesTodos(totais = null) {
                let params = {
                    CodigoRelatorio: codigoRelatorio,
                }
                await axios.post(base_url + 'atividades/getAtividadesMes', $.param(params))
                    .then((resp) => {
                        this.listaAtividades = resp.data;
                        // if (totais === true) {
                        //     this.pendentes = resp.data.length;
                        // } else {
                        //     this.listaAtividades = resp.data;
                        // }
                    });

            },
            async calcularPendentes() {
                await this.getAtividadesStatus(true);
                this.pendentes = this.pendentes - this.listaAtividades.length;
            },
            async gerarAnos() {
                for (var i = (this.ano + 30); i >= 1980; i--) {
                    await this.listaAnos.push({
                        ano: i
                    });
                }
            },
            async statusAtividade(codigoAtividade, acao = null, tipo = null, dados = null) {
                this.tipoAtivideadeSelecionada = tipo;
                this.atividadeSelecionada = codigoAtividade;
                if (dados) this.atividadeSelecionadaDados = dados;
                if (acao === 'iniciar') {
                    let params = {
                        table: 'atividadesDoCronograma',
                        data: {
                            DataAtacado: new Date(),
                            Status: 1
                        },
                        where: {
                            CodigoAtividadeCronogramaFisico: codigoAtividade
                        }
                    }
                    Swal.fire({
                        title: 'Confirmação requerida!',
                        text: "Tem certeza que deseja iniciar a atividade? Esta ação não poderá ser desfeita!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sim, Iniciar'
                    }).then(async (result) => {
                        if (result.value) {
                            await vmGlobal.updateFromAPI(params, null, 'cgmab');
                            Swal.fire(
                                'OK Confirmado',
                                'Atividade iniciada!',
                                'success'
                            )
                            await this.getAtividades();
                        }
                    })
                    return;
                } else if (acao === 'iniciarForm') {
                    //iniciar anexos
                    this.getAnexos()
                    //finalizar: iniciar campos para preenchimetno
                    CKEDITOR.replace('finalizar', {
                        height: '400px',
                    });
                    CKEDITOR.replace('analiseResultadosDiscucoes', {
                        height: '400px',
                    });
                    CKEDITOR.replace('analiseResultadosConclusoes', {
                        height: '400px',
                    });
                    CKEDITOR.replace('analiseResultadosBibliografia', {
                        height: '400px',
                    });
                    CKEDITOR.replace('parecerTecnico', {
                        height: '400px',
                    });

                    //busca imagens da atividade
                    this.getFotosAtividade();
                    //buscar descricaoAtividade();
                    await this.getAtividades(this.atividadeSelecionada);
                    setTimeout(() => {
                        if (this.atividadeDados.AcoesExecutadas) CKEDITOR.instances['finalizar'].setData(atob(this.atividadeDados.AcoesExecutadas));
                        $('#myTab a[href="#intro"]').tab('show')
                        this.iniciarTextAreas();
                    }, 1000);
                    this.mostrarFormFinalizar = true;
                } else {
                    //Finaliza alterando status para  3
                    let params = {
                        table: 'atividadesDoCronograma',
                        data: {
                            DataRealizado: new Date(),
                            Status: 3
                        },
                        where: {
                            CodigoAtividadeCronogramaFisico: codigoAtividade
                        }
                    }
                    Swal.fire({
                        title: 'Confirmação requerida!',
                        text: "Tem certeza que deseja Finalizar a atividade? Esta ação não poderá ser desfeita!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sim, Finalizar'
                    }).then(async (result) => {
                        if (result.value) {
                            await vmGlobal.updateFromAPI(params, null, 'cgmab');
                            Swal.fire(
                                'OK Confirmado',
                                'Atividade finalizada com sucesso!',
                                'success'
                            )
                        }
                    })
                    await this.getAtividades();
                    this.mostrarFormFinalizar = false;
                }
            },
            arquivosSelecionados() {
                this.files = this.$refs.files.files;
            },
            enviarArquivos() {
                let formData = new FormData();
                formData.append('codigoAtividade', this.atividadeSelecionada);
                for (var i = 0; i < this.files.length; i++) {
                    let file = this.files[i];
                    formData.append('files[' + i + ']', file);
                }

                axios.post(base_url + '/atividades/uploadFotos',
                        formData, {
                            headers: {
                                'Content-Type': 'multipart/form-data'
                            }
                        }
                    ).then(() => {
                        this.$refs.files.value = '';
                        console.log('SUCCESSO!!');
                    })
                    .catch(() => {
                        console.log('ERRO!!');
                        this.$refs.files.value = '';
                    })
                    .finally(() => {
                        this.getFotosAtividade();
                    })
            },
            async getFotosAtividade() {
                this.listaFotosAtividade = {}
                let params = {
                    table: 'atividadesFotos',
                    where: {
                        CodigoAtividadeCronogramaFisico: this.atividadeSelecionada
                    }
                }
                this.listaFotosAtividade = await vmGlobal.getFromAPI(params, null);

            },
            async atualizarDados(codigo, acao) {
                var params;
                if (acao == 'Foto') {
                    params = {
                        table: 'atividadesFotos',
                        data: this.modelFotos,
                        where: {
                            CodigoAtivCronFisicoFotos: codigo
                        }
                    }

                } else {
                    this.modelFotos.AcoesExecutadas = btoa(CKEDITOR.instances['finalizar'].getData());
                    params = {
                        table: 'atividadesDoCronograma',
                        data: this.modelFotos,
                        where: {
                            CodigoAtividadeCronogramaFisico: codigo
                        }
                    }
                }

                Swal.fire({
                        title: 'Confirmação requerida!',
                        text: "Tem certeza que deseja atualizar este dado? Esta ação não poderá ser desfeita!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sim'
                    })
                    .then(async (result) => {
                        if (result.value) {
                            await vmGlobal.updateFromAPI(params, null);
                            if (acao == 'Foto') {
                                this.modelFotos.Zona = '';
                                this.modelFotos.CN = '';
                                this.modelFotos.CE = '';
                                this.modelFotos.Elevacao = '';
                            }
                            this.getFotosAtividade();
                            if (acao === 'Atividade') {
                                await this.getAtividades(codigo);
                                CKEDITOR.instances['finalizar'].setData(atob(this.atividadeDados.AcoesExecutadas));
                            }
                        }


                    });


            },
            async deletarFoto(codigoFoto, caminho) {
                let params = {
                    table: 'atividadesFotos',
                    where: {
                        CodigoAtivCronFisicoFotos: codigoFoto
                    }
                }
                await vmGlobal.deleteFromAPI(params, null);
                await vmGlobal.deleteFile(caminho);
                await this.getFotosAtividade();
            },
            async iniciarRelatorio(programa, codigoAtividadeCronogramaFisico, anoAtividade, mesAtividade) {
                //verificar qual relatorio direcionar pelo tema/subtema da condicionante
                axios.post(base_url + 'Atividades/verificaSubtipoTema/', $.param({
                        CodigoEscopoPrograma: programa
                    }))
                    .then((resp) => {
                        //relatorio de monitoramento de agua
                        if (parseInt(resp.data[0].CodigoSubtipo) === 5) {
                            window.location.href = base_url + 'Relatorios/monitoramentoAgua?99cfb275a87d383fa2912331e3117e48=' + vmGlobal.codificarDecodificarParametroUrl(programa, 'encode') + '&99cfb275a87d383fa2912331e3117e48=' + vmGlobal.codificarDecodificarParametroUrl(codigoAtividadeCronogramaFisico, 'encode') + '&99cfb275a87d383fa2912331e3117e48=' + vmGlobal.codificarDecodificarParametroUrl(mesAtividade, 'encode') + '&99cfb275a87d383fa2912331e3117e48=' + vmGlobal.codificarDecodificarParametroUrl(anoAtividade, 'encode');
                        }
                        //relatorio de monitoramento de Fauna
                        else if (parseInt(resp.data[0].CodigoSubtipo) === 3) {
                            window.location.href = base_url + 'Relatorios/monitoramentoFauna?codigoEscopoPrograma=' + vmGlobal.codificarDecodificarParametroUrl(programa, 'encode') + '&numeroContrato=' + vmGlobal.codificarDecodificarParametroUrl(numeroContrato, 'encode') + '&codigoAtividadeCronogramaFisico=' + vmGlobal.codificarDecodificarParametroUrl(codigoAtividadeCronogramaFisico, 'encode') + '&ano=' + vmGlobal.codificarDecodificarParametroUrl(anoAtividade, 'encode') + '&mes=' + vmGlobal.codificarDecodificarParametroUrl(mesAtividade, 'encode');
                        }
                        //relatorio de atropelamento de Fauna
                        else if (parseInt(resp.data[0].CodigoSubtipo) === 1) {
                            window.location.href = base_url + 'Relatorios/atropelamentoFauna?codigoEscopoPrograma=' + vmGlobal.codificarDecodificarParametroUrl(programa, 'encode') + '&numeroContrato=' + vmGlobal.codificarDecodificarParametroUrl(numeroContrato, 'encode') + '&codigoAtividadeCronogramaFisico=' + vmGlobal.codificarDecodificarParametroUrl(codigoAtividadeCronogramaFisico, 'encode') + '&ano=' + vmGlobal.codificarDecodificarParametroUrl(anoAtividade, 'encode') + '&mes=' + vmGlobal.codificarDecodificarParametroUrl(mesAtividade, 'encode');
                        }
                        // Relatorio Passagem de fauna 
                        else if (parseInt(resp.data[0].CodigoSubtipo) === 4) {
                            window.location.href = base_url + 'Relatorios/passagemFauna?QCWVedRdGjYRseiuwWEwQEdvBNMEOZASrdwWFRw=' + vmGlobal.codificarDecodificarParametroUrl(programa, 'encode') + '&QCWVedRdGjYRseiuwWEwQEdvBNMEOZASrdwWFRw=' + vmGlobal.codificarDecodificarParametroUrl(numeroContrato, 'encode') + '&QCWVedRdGjYRseiuwWEwQEdvBNMEOZASrdwWFRw=' + vmGlobal.codificarDecodificarParametroUrl(codigoAtividadeCronogramaFisico, 'encode') + '&QCWVedRdGjYRseiuwWEwQEdvBNMEOZASrdwWFRw=' + vmGlobal.codificarDecodificarParametroUrl(anoAtividade, 'encode') + '&QCWVedRdGjYRseiuwWEwQEdvBNMEOZASrdwWFRw=' + vmGlobal.codificarDecodificarParametroUrl(mesAtividade, 'encode');
                        }
                        // Relatorio de Resgate de Fauna 
                        else if (parseInt(resp.data[0].CodigoSubtipo) === 6) {
                            window.location.href = base_url + 'Relatorios/afugentamentoFauna?codigoEscopoPrograma=' + vmGlobal.codificarDecodificarParametroUrl(programa, 'encode') + '&numeroContrato=' + vmGlobal.codificarDecodificarParametroUrl(numeroContrato, 'encode') + '&codigoAtividadeCronogramaFisico=' + vmGlobal.codificarDecodificarParametroUrl(codigoAtividadeCronogramaFisico, 'encode') + '&ano=' + vmGlobal.codificarDecodificarParametroUrl(anoAtividade, 'encode') + '&mes=' + vmGlobal.codificarDecodificarParametroUrl(mesAtividade, 'encode');
                        }
                        // Relatorio de Flora - Supressao de Vegetação 
                        else if (parseInt(resp.data[0].CodigoSubtipo) === 2) {

                            window.location.href = base_url + 'RelatorioSupressaoVegetacao/supressaoVegetacao?codigoEscopoPrograma=' + vmGlobal.codificarDecodificarParametroUrl(programa, 'encode') + '&numeroContrato=' + vmGlobal.codificarDecodificarParametroUrl(numeroContrato, 'encode') + '&codigoAtividadeCronogramaFisico=' + vmGlobal.codificarDecodificarParametroUrl(codigoAtividadeCronogramaFisico, 'encode') + '&ano=' + vmGlobal.codificarDecodificarParametroUrl(anoAtividade, 'encode') + '&mes=' + vmGlobal.codificarDecodificarParametroUrl(mesAtividade, 'encode');
                        } else {
                            this.statusAtividade(codigoAtividadeCronogramaFisico, 'iniciarForm', 'relatorioTecnico');
                        }

                    })

            },
            // recursos utilizados
            async getRecursosRelatorio() {
                if (this.opcao == 'profisionais') {
                    this.listaRecursosPrograma = await vmGlobal.getFromController(
                        'Atividades/getRecursosAtividadesContrato', {
                            atividade: this.atividadeSelecionada,
                            tipo: this.opcao
                        }
                    );
                }

                if (this.opcao == 'veiculos') {
                    this.listaRecursosPrograma = await vmGlobal.getFromController(
                        'Atividades/getRecursosAtividadesContrato', {
                            atividade: this.atividadeSelecionada,
                            tipo: this.opcao
                        }
                    );

                }
                if (this.opcao == 'equipamentos') {
                    this.listaRecursosPrograma = await vmGlobal.getFromController(
                        'Atividades/getRecursosAtividadesContrato', {
                            atividade: this.atividadeSelecionada,
                            tipo: this.opcao
                        }
                    );

                }


            },
            async getRecursosServico(opcao) {
                this.opcao = opcao;
                if (opcao == 'profisionais') {
                    this.listaRecursosServico = await vmGlobal.getFromController(
                        'RecursosHumanos/getRecursosHumanosContrato', {
                            contrato: '<?= $this->session->userdata('codigoContratoSelecionado'); ?>',
                            escopo: this.atividadeDados.CodigoEscopoPrograma
                        }
                    );
                }

                if (opcao == 'veiculos') {
                    this.listaRecursosServico = await vmGlobal.getFromController(
                        'Veiculos/getRecursosVeiculosContrato', {
                            contrato: '<?= $this->session->userdata('codigoContratoSelecionado'); ?>',
                            escopo: this.atividadeDados.CodigoEscopoPrograma
                        }
                    );

                }
                if (opcao == 'equipamentos') {
                    this.listaRecursosServico = await vmGlobal.getFromController(
                        'Equipamentos/getRecursosEquipamentosContrato', {
                            contrato: '<?= $this->session->userdata('codigoContratoSelecionado'); ?>',
                            escopo: this.atividadeDados.CodigoEscopoPrograma
                        }
                    );

                }


                // this.listaRecursosHumanosServico = await vmGlobal.getFromAPI(params);
                // var controller = 'EscopoServicos/getRecurosHumanosServico';
                // this.listaRecursosHumanosServico = await vmGlobal.getFromController(controller, params);
            },
            async salvarProfissionalRelatorio(codigoRecurso, tipo) {
                let params = {
                    table: 'recursosAtividade',
                    data: {
                        CodigoAtividadeCronogramaFisico: this.atividadeSelecionada,
                        CodigoRecurso: codigoRecurso,
                        TipoRecurso: tipo,
                        DataCadastro: new Date()
                    }
                }
                await vmGlobal.insertFromAPI(params, null);
                this.getRecursosRelatorio();

            },
            async deletarRecurso(codigo) {
                let params = {
                    table: 'recursosAtividade',
                    where: {
                        CodigoAtividadeRecurso: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(params);
                this.getRecursosRelatorio();
            },

            //discursoes e conclusões
            iniciarTextAreas() {
                CKEDITOR.instances['txTanaliseResultadosDiscucoes'].setData('Carregando ...'); //limpar textarea
                CKEDITOR.instances['txTanaliseResultadosConclusoes'].setData('Carregando ...'); //limpar textarea
                CKEDITOR.instances['txTanaliseResultadosBibliografia'].setData('Carregando ...'); //limpar textarea
                CKEDITOR.instances['txTparecerTecnico'].setData('Carregando ...'); //limpar textarea
                setTimeout(() => {
                    this.getTextos('AnalisesResultados');
                    this.getTextos('Conclusoes');
                    this.getTextos('Bibliografia');
                    this.getTextos('ParecerTecnico');
                }, 3000);



            },
            getTextos(coluna) {


                if (coluna == 'AnalisesResultados') {
                    CKEDITOR.instances['txTanaliseResultadosDiscucoes'].setData(atob(this.atividadeDados.AnalisesResultados)); ///preencher textarea
                }
                if (coluna == 'Conclusoes') {
                    CKEDITOR.instances['txTanaliseResultadosConclusoes'].setData(atob(this.atividadeDados.Conclusoes)); //preencher textarea
                }
                if (coluna == 'Bibliografia') {
                    CKEDITOR.instances['txTanaliseResultadosBibliografia'].setData(atob(this.atividadeDados.Bibliografia));
                }
                if (coluna == 'ParecerTecnico') {
                    CKEDITOR.instances['txTparecerTecnico'].setData(atob(this.atividadeDados.ParecerTecnico));
                }


                // axios.post(base_url + 'Relatorios/getTextos', $.param({
                //         coluna: coluna,
                //         atividade: this.atividadeSelecionada
                //     }))
                //     .then((resp) => {

                //         if (coluna === 'AnaliseResultadoDiscussao') {
                //             this.analiseResultadosDiscucoes = resp.data;
                //             CKEDITOR.instances['txTanaliseResultadosDiscucoes'].setData(this.atividadeDados.AnalisesResultados); ///preencher textarea

                //         }
                //         if (coluna === 'Conclusao') {
                //             this.analiseResultadosConclusoes = resp.data;
                //             CKEDITOR.instances['txTanaliseResultadosConclusoes'].setData(this.atividadeDados.Conclusoes); //preencher textarea

                //         }
                //         if (coluna === 'Bibliografia') {
                //             this.analiseResultadosBibliografia = resp.data;
                //             CKEDITOR.instances['txTanaliseResultadosBibliografia'].setData(this.atividadeDados.Bibliografia);
                //         }
                //         if (coluna === 'ParecerTecnico') {//// <textarea name="parecerTecnico" id="txTparecerTecnico"></textarea>
                //             this.analiseResultadosParecer = resp.data;
                //             CKEDITOR.instances['txTparecerTecnico'].setData(this.atividadeDados.ParecerTecnico);
                //         }

                //     })
                //     .catch((e) => {
                //         console.log(e)
                //     })


            },
            async salvarTexto(campo) {

                var params;

                if (campo == 'AnalisesResultados') {

                    params = {
                        table: 'atividadesDoCronograma',
                        data: {
                            AnalisesResultados: btoa(CKEDITOR.instances['txTanaliseResultadosDiscucoes'].getData()),
                        },
                        where: {
                            CodigoAtividadeCronogramaFisico: this.atividadeSelecionada
                        }
                    }
                    await vmGlobal.updateFromAPI(params);


                }
                if (campo == 'Conclusoes') {

                    params = {
                        table: 'atividadesDoCronograma',
                        data: {
                            Conclusoes: btoa(CKEDITOR.instances['txTanaliseResultadosConclusoes'].getData()),
                        },
                        where: {
                            CodigoAtividadeCronogramaFisico: this.atividadeSelecionada
                        }
                    }
                    await vmGlobal.updateFromAPI(params);


                }
                if (campo == 'Bibliografia') {

                    params = {
                        table: 'atividadesDoCronograma',
                        data: {
                            Bibliografia: btoa(CKEDITOR.instances['txTanaliseResultadosBibliografia'].getData()),
                        },
                        where: {
                            CodigoAtividadeCronogramaFisico: this.atividadeSelecionada
                        }
                    }
                    await vmGlobal.updateFromAPI(params);


                }
                if (campo == 'Parecer') {

                    params = {
                        table: 'atividadesDoCronograma',
                        data: {
                            ParecerTecnico: btoa(CKEDITOR.instances['txTparecerTecnico'].getData()),
                        },
                        where: {
                            CodigoAtividadeCronogramaFisico: this.atividadeSelecionada
                        }
                    }
                    await vmGlobal.updateFromAPI(params);


                }
            },

            //anexos
            async getAnexos() { //buscarAnexos
                let params = {
                    table: 'anexos',
                    where: {
                        CodigoAtividadeCronogramaFisico: this.atividadeSelecionada
                    }
                }
                this.listaDocumentos = await vmGlobal.getFromAPI(params);
            },
            async addDocumentos() {
                var myfiles = document.getElementById("documentosMaterial");
                var files = myfiles.files;
                var data = new FormData();

                for (i = 0; i < files.length; i++) {
                    data.append(i, files[i]);
                }

                data.append('codigoAtividade', this.atividadeSelecionada);

                $.ajax({
                    url: '<?= base_url('Atividades/salvarAnexos') ?>',
                    method: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Salvo!',
                            text: 'Fotos salvas com sucesso.',
                            showConfirmButton: true,
                        });
                        document.getElementById('documentosMaterial').value = null;
                    },
                    error: function(error) {
                        console.log(error);
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao tentar salvar arquivo. " + e,
                            showConfirmButton: true,

                        });
                    },
                    complete: function() {
                        vmAtividadesMes.getAnexos();
                        vmAtividadesMes.totalDocumentos = 0
                        $('.dropify-clear').click();
                        vmAtividadesMes.bloqueioUpload = false;
                    }
                });
            },
            async deletarArquivo(codigo, caminho) {
                await vmGlobal.deleteFromAPI({
                    table: 'anexos',
                    where: {
                        CodigoAnexo: codigo
                    }
                });
                await vmGlobal.deleteFile(caminho);
                await this.getAnexos();
            }

        }
    });
</script>