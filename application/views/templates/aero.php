<!DOCTYPE html>
<html lang="pt-br">

<head>
  <title>SGMAB</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('elements/css') ?>
  <script>
    var base_url = '<?= base_url() ?>';
  </script>
  <!-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script> -->
  <script src="<?= base_url() ?>webroot/arquivos/plugins/vue/vue.js"></script>
  <script src="<?= base_url() ?>webroot/arquivos/plugins/axios/axios.min.js"></script>
  <?php $this->load->view('elements/js') ?>



</head>

<body class="theme-blush">
  <!-- Page Loader -->
  <div class="page-loader-wrapper">
    <div class="loader">
      <div class="m-t-30"><img  src="<?= base_url() ?>webroot/arquivos/images/loader.gif" w alt="gif"></div>
      <p>Por favor aguarde...</p>
    </div>
  </div>
  <?php $this->load->view($sidebar) ?>
  <section class="content">
    <div class="col-lg-5 col-md-6 col-sm-12">
      <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
    </div>
    <?php $this->load->view($pagina) ?>
  </section>

  <script>
    // Configurações iniciais do template 
    $(document).ready(function() {
      //minimizar sidebats left e right
      $('body').addClass('ls-toggle-menu');
      // $('body').addClass('right_icon_toggle');

      //pegar altura do monitor
      var altura = screen.height;
      $('.content').css('min-height', altura)


      //setar cor do tema
      $('body').removeClass('theme-blush');
      $('body').addClass(localStorage.getItem('ThemeOption'));
      $('body').addClass(localStorage.getItem('ThemeOptionCor'));


      //checar radio de acordo com a cor do tema
      if (localStorage.getItem('ThemeOption') === 'theme-dark') {
        $('#darktheme').prop("checked", true);
      } else {
        $('#lighttheme').prop("checked", true);
      }
    });
    //cor de fundo do tema
    $('#darktheme').click(function() {

      localStorage.setItem('ThemeOption', 'theme-dark');
    });
    $('#lighttheme').click(function() {
      localStorage.setItem('ThemeOption', '');
    });


    //cor dos textos do tema
    $('.blue').click(function() {
      localStorage.setItem('ThemeOptionCor', '');
      localStorage.setItem('ThemeOptionCor', 'theme-purple');

    });
    $('.purple').click(function() {
      localStorage.setItem('ThemeOptionCor', '');
      localStorage.setItem('ThemeOptionCor', 'theme-blue');

    });
    $('.cyan').click(function() {
      localStorage.setItem('ThemeOptionCor', '');
      localStorage.setItem('ThemeOptionCor', 'theme-cyan');

    });
    $('.green').click(function() {
      localStorage.setItem('ThemeOptionCor', '');
      localStorage.setItem('ThemeOptionCor', 'theme-green');

    });
    $('.orange').click(function() {
      localStorage.setItem('ThemeOptionCor', '');
      localStorage.setItem('ThemeOptionCor', 'theme-orange');

    });
    $('.blush').click(function() {
      localStorage.setItem('ThemeOptionCor', '');
      localStorage.setItem('ThemeOptionCor', 'theme-blush');

    });


    //animações
    new WOW().init();
    // Fim Configurações iniciais do template 
  </script>
</body>

</html>