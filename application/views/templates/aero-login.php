<!DOCTYPE html>
<html lang="pt-br">

<head>
  <title>SGMAB - ACESSO</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('elements/css') ?>
  <script>
    var base_url = '<?= base_url() ?>';
  </script>
  <script src="<?= base_url() ?>webroot/arquivos/plugins/vue/vue.js"></script>
  <script src="<?= base_url() ?>webroot/arquivos/plugins/axios/axios.min.js"></script>
  <?php $this->load->view('elements/js') ?>
<style>
body {
    background-image: url(webroot/arquivos/images/bglogin.jpg);
    background-repeat: no-repeat;
    background-blend-mode: luminosity;
    
}

.authentication .auth_form{
  background: hsla(0, 0%, 0%, 0.56);
  box-shadow: 0 0 113px 21px rgba(0, 0, 0, 0.74), 0 6px 20px rgb(41, 42, 51);
}
.authentication .auth_form h5{
  color: #fff;
}
.form-control { 
    background: rgba(255, 255, 255, 0.7);
}
a {
    color: #ffffff;
}
.card .header{
  padding: 0
}
.copyright {
    color: #fff;
}
.card {
    box-shadow: 0px 0px 95px 33px #0000001a;
    min-height: 285px;
    padding: 0;
}
</style>


</head>

<body class="theme-blush">
  <!-- Page Loader -->
  <div class="page-loader-wrapper">
    <div class="loader">
      <div class="m-t-30"><img class="zmdi-hc-spin" src="<?= base_url() ?>webroot/arquivos/images/loader.svg" width="48" height="48" alt="Aero"></div>
      <p>Por favor aguarde...</p>
    </div>
  </div>

  <?php $this->load->view($pagina) ?>

  <script>
    // Configurações iniciais do template 
    $(document).ready(function() {
      //minimizar sidebats left e right
      // $('body').addClass('ls-toggle-menu');
      // $('body').addClass('right_icon_toggle');

      //pegar altura do monitor
      var altura = screen.height;
      $('.content').css('min-height', altura)


      //setar cor do tema
      $('body').removeClass('theme-blush');
      $('body').addClass(localStorage.getItem('ThemeOption'));
      $('body').addClass(localStorage.getItem('ThemeOptionCor'));
    });
  </script>
</body>

</html>