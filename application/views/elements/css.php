
<?php if ($this->router->fetch_class() === "Login" || $this->router->fetch_class() === "login"){?>
    <link rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/css/style.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/css/comum.css">
<?php }else{?>
<link rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<link rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/charts-c3/plugin.css"/>
<!-- datatables  -->
<link rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/datatables/dataTables.bootstrap4.css"/>
<link rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/datatables/jquery.dataTables.css">
<link rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/datatables/buttons.dataTables.min.css">

<!-- fontawesome  -->
<link  rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/fontawesome/css/all.css">
<link  rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/fontawesome/css/brands.css">
<link  rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/fontawesome/css/regular.css">
<link  rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/fontawesome/css/solid.css">
<link  rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/fontawesome/css/svg-with-js.css">
<link  rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/fontawesome/css/v4-shims.css">
<link  rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/fontawesome/css/v4-shims.min.css">
<link  rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/plugins/dropify/css/dropify.min.css">
<!-- animate  -->
<link  rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/css/animate.min.css">

<!-- Custom Css -->
<link rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/css/style.min.css">
<link rel="stylesheet" href="<?= base_url() ?>webroot/arquivos/css/comum.css">
<?php }?>


