<!-- Jquery Core Js -->
<script src="<?= base_url() ?>webroot/arquivos/plugins/bootstrap/js/popper.js"></script>
<script src="<?= base_url() ?>webroot/arquivos/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->

<script src="<?= base_url() ?>webroot/arquivos/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="<?= base_url() ?>webroot/arquivos/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="<?= base_url() ?>webroot/arquivos/bundles/sparkline.bundle.js"></script> <!-- Sparkline Plugin Js -->
<script src="<?= base_url() ?>webroot/arquivos/bundles/c3.bundle.js"></script>

<script src="<?= base_url() ?>webroot/arquivos/bundles/mainscripts.bundle.js"></script>
<script src="<?= base_url() ?>webroot/arquivos/js/pages/index.js"></script>

<script src="<?= base_url() ?>webroot/arquivos/plugins/datatables/jquery_dataTables.js"></script>
 <!-- <script src="<?= base_url() ?>webroot/arquivos/plugins/datatables/Buttons-1.6.1/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>webroot/arquivos/plugins/datatables/Buttons-1.6.1/js/buttons.bootstrap4.js"></script>
<script src="<?= base_url() ?>webroot/arquivos/plugins/datatables/Buttons-1.6.1/js/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>webroot/arquivos/plugins/datatables/Buttons-1.6.1/js/buttons.print.min.js"></script>
<script src="<?= base_url() ?>webroot/arquivos/plugins/datatables/Buttons-1.6.1/js/buttons.flash.min.js"></script>  -->

<script src="<?= base_url() ?>webroot/arquivos/plugins/ckeditor/js/ckeditor.js"></script>
<script src="<?= base_url() ?>webroot/arquivos/plugins/moment/moment.min.js"></script>
<script src="<?= base_url() ?>webroot/arquivos/js/app.js"></script>
<script src="<?= base_url() ?>webroot/arquivos/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script src="<?= base_url() ?>webroot/arquivos/plugins/dropify/js/dropify.min.js"></script>
<script src="<?= base_url() ?>webroot/arquivos/js/wow.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script> 



