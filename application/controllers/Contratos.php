<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Contratos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/ContratoModel', 'contratos');
        $this->load->model('cgmab/LoginModel', 'login');
        $this->load->model('cgmab/UsuarioModel', 'usuario');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function index()
    {
        $dados['pagina'] = 'cgmab/Contratos/index';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function lisar()
    {
        $dados['pagina'] = 'cgmab/Contratos/listar';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    /*
    * buscar licenças no banco sim dnit
    */
    public function getContratos()
    {
        $dados = $this->input->post(NULL, true);
        $result = $this->contratos->getContratos();
        echo json_encode($result);
    }
    /*
    * buscar Aditivos no banco sim dnit
    */
    public function getContratosAditivos()
    {
        $dados = $this->input->get(NULL, true);
        $result = $this->contratos->getContratosAditivos($dados);
        echo json_encode($result);
    }
    /*
    * buscar Reajustes no banco sim dnit
    */
    public function getContratosReajustes()
    {
        $dados = $this->input->get(NULL, true);
        $result = $this->contratos->getContratosReajustes($dados);
        echo json_encode($result);
    }
    /*
    * buscar licenças no banco sim dnit
    */
    public function getContratosAditivosSalvar()
    {
        $dados = $this->input->post('Dados', true);
        $array = (array) json_decode($dados, True);
        $array = json_decode(json_decode(json_encode($dados)), True);
        $result = $this->contratos->getContratosAditivosSalvar($array);
    }
    /*
    * buscar licenças no banco sim dnit
    */
    public function getContratosReajustesSalvar()
    {
        $dados = $this->input->post('Dados', true);
        $array = (array) json_decode($dados, True);
        $array = json_decode(json_decode(json_encode($dados)), True);
        $result = $this->contratos->getContratosReajustesSalvar($array);
    }

    /*
    * Salvar um  contrato
    */

    public function salvarContrato()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->contratos->salvarContrato($dados));
    }

    public function salvarFiscais()
    {
        $contrato = $this->input->post(null, true);

        $fiscais = $this->contratos->getFiscaisContrato($contrato['CodigoContrato']);

        if (empty($fiscais)) {
            die(json_encode(['result' => true]));
        }

        $fiscalContrato = [];
        $response = [];
        foreach ($fiscais as $index => $fiscal) {
            $fiscal['Senha'] = $this->login->codificar('DnitSgmab' . date('Y'));

            $user = $this->usuario->get(['SkFiscal' => $fiscal['SkFiscal']]);
            if (empty($user)) {
                $response = $this->usuario->insert($fiscal);
            } else {
                $response['result'] = $user['CodigoUsuario'];
            }

            $key = ($index == 0) ? 'FiscalTecnico' : 'FiscalSubstituto';
            $fiscalContrato[$key] = $response['result'];
        }

        $response = $this->contratos->updateContrato($fiscalContrato, ['SkContrato' => $contrato['CodigoContrato']]);
        die(json_encode(['result' => $response]));
    }

    public function home()
    {
        $dados['pagina'] = 'cgmab/Contratos/home';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function listar()
    {
        $dados['pagina'] = 'cgmab/Contratos/listar';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function dados($contrato = null, $Contratada = null)
    {
        $dados['pagina'] = 'cgmab/Contratos/dados';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $dados['CodigoContrato'] = $contrato;
        if (isset($Contratada)) {
            $_SESSION['Logado']['CodigoContratada'] = $Contratada;
        }
        $this->load->view('templates/aero', $dados);
    }

    public function getVerificaConfiguracaoInicialContrato()
    {
        $dados = (array) $this->input->post(null, true);
        echo json_encode($this->contratos->getVerificaConfiguracaoInicialContrato($dados));
    }

    public function getContratadasContrato()
    {
        echo json_encode($this->contratos->getContratadasContrato());
    }

    public function listarServicosParecerPendente()
    {
        $contrato = $this->input->post('Contrato', true);
        echo json_encode($this->contratos->listarServicosParecerPendente($contrato));
    }

    public function gerarSessaoContrato()
    {

        $contrato = $this->input->post('contrato', true);
        $this->session->set_userdata('codigoContratoSelecionado', $contrato);
    }

    public function getFiscaisContrato()
    {
        $filtro = $this->input->post(null, true);
        $result = $this->contratos->getFiscais($filtro['CodigoContrato']);
        die(json_encode(['fiscais' => $result]));
    }

    public function salvarFiscal()
    {
        $dados = $this->input->post(null, true);

        $dados['Senha'] = $this->login->codificar('DnitSgmab' . date('Y'));
        $dados['Status'] = 1;
        $dados['CodigoPerfil'] = 3;
        $dados['RG']  = '';

        $response = $this->usuario->insert($dados);
        die(json_encode($response));
    }
}
