<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Veiculos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/VeiculoModel', 'Veiculo');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');

    }

    public function listar()
    {
        $dados['pagina'] = 'cgmab/Veiculos/listar';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function getRecursosVeiculosContrato(){
        $dados = $this->input->post(null, true);
        echo json_encode($this->Veiculo->getRecursosVeiculosContrato($dados));
    }

}
