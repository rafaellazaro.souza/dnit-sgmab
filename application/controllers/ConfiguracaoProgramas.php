<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ConfiguracaoProgramas extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/ConfiguracaoProgramasModel', 'ConfiguracaoPrograma');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function recursosHidricos()
    {
        $dados['pagina'] = 'cgmab/ConfiguracaoProgramas/recursosHidricos';
        $dados['sidebar'] = 'elements/sidebar';
        $this->load->view('templates/aero', $dados);
    }

    public function recursosFauna()
    {
        $dados['pagina'] = 'cgmab/ConfiguracaoProgramas/paginas/recursosFaunaPontos';
        $dados['sidebar'] = 'elements/sidebar';
        $this->load->view('templates/aero', $dados);
    }

    public function recursosFaunaModulos()
    {
        $dados['pagina'] = 'cgmab/ConfiguracaoProgramas/paginas/recursosFaunaModulos';
        $dados['sidebar'] = 'elements/sidebar';
        $this->load->view('templates/aero', $dados);
    }

    public function getGps()
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");

        $i = 0;
        foreach ($_FILES as $key) {
            $temp_name = $key['tmp_name'];
            $exif = exif_read_data($temp_name, 0, true);
            if (isset($exif['IFD0'])) {
                $d = explode(":", $exif['IFD0']['DateTime']);
                $dadaFoto = $d[0] . '-' . $d[1] . '-' . $d[2] . ':' . $d[3] . ':' . $d[4];
            } else {
                $dadaFoto = date('Y-m-d H:i:s');
            }
            if (!isset($exif['GPS'])) {
                $nome = $key['name'];
                $lat_ = '';
                $lng_ = '';
            } else {
                $lat_ref = $exif['GPS']['GPSLatitudeRef'];
                $lat = $exif['GPS']['GPSLatitude'];
                list($num, $dec) = explode('/', $lat[0]);
                $lat_s = $num / $dec;
                list($num, $dec) = explode('/', $lat[1]);
                $lat_m = $num / $dec;
                list($num, $dec) = explode('/', $lat[2]);
                $lat_v = $num / $dec;
                $lon_ref = $exif['GPS']['GPSLongitudeRef'];
                $lon = $exif['GPS']['GPSLongitude'];
                list($num, $dec) = explode('/', $lon[0]);
                $lon_s = $num / $dec;
                list($num, $dec) = explode('/', $lon[1]);
                $lon_m = $num / $dec;
                list($num, $dec) = explode('/', $lon[2]);
                $lon_v = $num / $dec;
                $nome = $key['name'];
                $lat_ = '-' . ($lat_s + $lat_m / 60.0 + $lat_v / 3600.0);
                $lng_ = '-' . ($lon_s  + $lon_m / 60.0 + $lon_v / 3600.0);
            }

            $cord[$i]['NomeFoto'] = $nome;
            $tipoAmostra = explode('_',  $nome)[1];
            switch ($tipoAmostra) {
                case 'J':
                    $tipoAmostra = 'Jusante';
                    break;
                case 'M':
                    $tipoAmostra = 'Montante';
                    break;
                case 'U':
                    $tipoAmostra = 'Único';
                    break;
                case 'C':
                    $tipoAmostra = 'Composta';
                    break;

                default:
                    $tipoAmostra = '';
                    break;
            }
            $cord[$i]['CodigoConfiguracaoRecursosHidricosPontoColeta'] =  preg_replace("/[^0-9]/", "", explode('_',  $nome)[0]);
            $cord[$i]['NomePontoColeta'] = explode('_',  $nome)[2];
            $cord[$i]['TipoPontoAmostra'] =  $tipoAmostra;
            $cord[$i]['Lat'] =  (string) $lat_;
            $cord[$i]['Lng'] = (string) $lng_;
            $cord[$i]['Data'] =  $data;
            $cord[$i]['DataFoto'] =  $dadaFoto;

            $NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'webroot/uploads/fotos_recursos_hidricos/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/fotos_recursos_hidricos/') . $novoNomeRandom;

            $config['file_name'] = $novoNomeRandom;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $cord[$i]['Caminho'] = $caminho;
            $i++;
        }
        echo $this->ConfiguracaoPrograma->inserirFotos($cord);
    }

    public function getGpsFauna()
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");

        $i = 0;
        $CodigoModulo = $_REQUEST['CodigoModulo'];
        foreach ($_FILES as $key) {
            $temp_name = $key['tmp_name'];
            $exif = exif_read_data($temp_name, 0, true);
            if (isset($exif['IFD0'])) {
                $d = explode(":", $exif['IFD0']['DateTime']);
                $dadaFoto = $d[0] . '-' . $d[1] . '-' . $d[2] . ':' . $d[3] . ':' . $d[4];
            } else {
                $dadaFoto = date('Y-m-d H:i:s');
            }
            if (!isset($exif['GPS'])) {
                $nome = $key['name'];
                $lat_ = '';
                $lng_ = '';
            } else {
                $lat_ref = $exif['GPS']['GPSLatitudeRef'];
                $lat = $exif['GPS']['GPSLatitude'];
                list($num, $dec) = explode('/', $lat[0]);
                $lat_s = $num / $dec;
                list($num, $dec) = explode('/', $lat[1]);
                $lat_m = $num / $dec;
                list($num, $dec) = explode('/', $lat[2]);
                $lat_v = $num / $dec;
                $lon_ref = $exif['GPS']['GPSLongitudeRef'];
                $lon = $exif['GPS']['GPSLongitude'];
                list($num, $dec) = explode('/', $lon[0]);
                $lon_s = $num / $dec;
                list($num, $dec) = explode('/', $lon[1]);
                $lon_m = $num / $dec;
                list($num, $dec) = explode('/', $lon[2]);
                $lon_v = $num / $dec;
                $nome = $key['name'];
                $lat_ = '-' . ($lat_s + $lat_m / 60.0 + $lat_v / 3600.0);
                $lng_ = '-' . ($lon_s  + $lon_m / 60.0 + $lon_v / 3600.0);
            }

            $cord[$i]['NomeFoto'] = $nome;
            $cord[$i]['CodigoModulo'] = $CodigoModulo;;
            $cord[$i]['Data'] =  $data;
            $cord[$i]['NomePontoMonitoramento'] = explode('_',  $nome)[1];
            $cord[$i]['Lat'] =  (string) $lat_;
            $cord[$i]['Lng'] = (string) $lng_;
            $cord[$i]['DataFoto'] =  $dadaFoto;

            $NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'webroot/uploads/fotos-recursos-fauna/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/fotos-recursos-fauna/') . $novoNomeRandom;
            $cord[$i]['Caminho'] = $caminho;
            $cord[$i]['Ativo'] = 1;

            $config['file_name'] = $novoNomeRandom;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $i++;
        }
        echo $this->ConfiguracaoPrograma->inserirFotosPadrao($cord, 'tblConfiguracaoMonitoramentoFaunaFotos');
    }


    public function getGpsPassagemFauna()
    {

        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");

        $i = 0;
        // echo '<pre>';
        // print_r($_FILES);
        // exit;
        $areaApoio = $_REQUEST['codigoAreaApoio'];
        foreach ($_FILES as $key) {
            $temp_name = $key['tmp_name'];
            $exif = exif_read_data($temp_name, 0, true);
            if (isset($exif['IFD0'])) {
                $d = explode(":", $exif['IFD0']['DateTime']);
                $dadaFoto = $d[0] . '-' . $d[1] . '-' . $d[2] . ':' . $d[3] . ':' . $d[4];
            } else {
                $dadaFoto = date('Y-m-d H:i:s');
            }
            if (!isset($exif['GPS'])) {
                $nome = $key['name'];
                $lat_ = '';
                $lng_ = '';
            } else {
                $lat_ref = $exif['GPS']['GPSLatitudeRef'];
                $lat = $exif['GPS']['GPSLatitude'];
                list($num, $dec) = explode('/', $lat[0]);
                $lat_s = $num / $dec;
                list($num, $dec) = explode('/', $lat[1]);
                $lat_m = $num / $dec;
                list($num, $dec) = explode('/', $lat[2]);
                $lat_v = $num / $dec;
                $lon_ref = $exif['GPS']['GPSLongitudeRef'];
                $lon = $exif['GPS']['GPSLongitude'];
                list($num, $dec) = explode('/', $lon[0]);
                $lon_s = $num / $dec;
                list($num, $dec) = explode('/', $lon[1]);
                $lon_m = $num / $dec;
                list($num, $dec) = explode('/', $lon[2]);
                $lon_v = $num / $dec;
                $nome = $key['name'];
                $lat_ = '-' . ($lat_s + $lat_m / 60.0 + $lat_v / 3600.0);
                $lng_ = '-' . ($lon_s  + $lon_m / 60.0 + $lon_v / 3600.0);
            }
            $dadosPonto = explode('.',  $nome);
            $cord[$i]['CodigoAreaApoio'] = $areaApoio;
            $cord[$i]['Lat'] =  (string) $lat_;
            $cord[$i]['Long'] = (string) $lng_;
            $cord[$i]['DataFoto'] =  $dadaFoto;
            $cord[$i]['DataCadastro'] =  $data;

            $NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'webroot/uploads/fotos_passagem_fauna/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/fotos_passagem_fauna/') . $novoNomeRandom;
            $cord[$i]['Caminho'] = $caminho;

            $config['file_name'] = $novoNomeRandom;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $i++;
        }
        echo $this->ConfiguracaoPrograma->inserirFotosPadrao($cord, 'tblConfiguracaoPassagemFaunaArmadilhas');
    }

    public function getFotos()
    {
        echo json_encode($this->ConfiguracaoPrograma->getFotos());
    }

    public function deletarFotos()
    {
        $codigo = $this->input->post("Codigo", true);
        echo $this->ConfiguracaoPrograma->deletarFotos($codigo);
    }
    public function deletarFotosPassagem()
    {
        $dados = $this->input->post(null, true);
        echo $this->ConfiguracaoPrograma->deletarFotosPassagem($dados);
    }

    public function deletarFotosFauna()
    {
        $codigo = $this->input->post("Codigo", true);
        echo $this->ConfiguracaoPrograma->deletarPadrao('CodigoConfiguracaoMonitoramentoFaunaFotos', $codigo, 'tblConfiguracaoMonitoramentoFaunaFotos');
    }

    public function criarSessaoCodigoPrograma()
    {
        $this->session->set_userdata('CodigoPrograma', $this->input->post('CodigoEscopoPrograma', false));
    }

    public function getListas()
    {
        $dados = $this->input->post('CodigoEscopoPrograma', true);
        $retorno = $this->ConfiguracaoPrograma->getListas($dados);
        echo json_encode($retorno);
    }

    public function getListasEquipamentoFauna()
    {
        $codigoPrograma = $this->input->get("CodigoPrograma", true);
        $retorno = $this->ConfiguracaoPrograma->getListasEquipamentoFauna($codigoPrograma);
        echo json_encode($retorno);
    }

    public function salvarParametrosNaLista()
    {
        $dados = json_decode($this->input->post('Dados', true), true);
        $array =  (array)  $dados;
        $retorno = $this->ConfiguracaoPrograma->salvarParametrosNaLista($array);
        echo json_encode($retorno);
    }
    public function salvarPontosNaLista()
    {
        $dados = json_decode($this->input->post('Dados', true), true);
        $array =  (array)  $dados;
        $retorno = $this->ConfiguracaoPrograma->salvarPontosNaLista($array);
        echo json_encode($retorno);
    }
    public function passagemFauna()
    {
        $dados['pagina'] = 'cgmab/ConfiguracaoProgramas/passagemFauna';
        $dados['sidebar'] = 'elements/sidebar';
        $this->load->view('templates/aero', $dados);
    }


    public function getArmadilhasPassagem(){
        $codigo = $this->input->post('CodigoAreaApoio', true);
        echo json_encode($this->ConfiguracaoPrograma->getArmadilhasPassagem($codigo));
    }
   
}
