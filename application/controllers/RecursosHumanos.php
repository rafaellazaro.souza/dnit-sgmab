<?php

defined('BASEPATH') or exit('No direct script access allowed');

class RecursosHumanos extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/RecursosHumanosModel', 'RecursoHumano');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function index()
    {
        $dados['pagina'] = 'cgmab/recursoshumanos/index';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function getRecursosHumanosContrato()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->RecursoHumano->getRecursosHumanosContrato($dados));
    }

    public function deleteFile()
    {

        // print_r($_FILES);exit;

        // $path = 'webroot/uploads/fotos-recursos-fauna/78068b611f489b7f2bf9.jpg';
        // path: http://localhost:8080/sgmab/webroot/uploads/recursosHumanos/13bfd4a380df6d1ff310.jpg                                                           
        $path = str_replace(" ", "", str_replace(base_url(), "", $this->input->post('path', true)));
        echo $path;
        if (!unlink($path)) {
            echo ("$path cannot be deleted due to an error");
        } else {
            echo ("$path has been deleted");
        }
    }

    public function insertAnexos()
    { {
            $i = 0;
            $dadosAnexo = [];
            print_r($_FILES);
            $codigoRH = $_REQUEST['CodigoRecursosHumanos'];
            foreach ($_FILES as $key) {
                $nome = $key['name'];
                $dadosAnexo[$i]['Descricao'] = $nome;
                $dadosAnexo[$i]['CodigoRecursosHumanos'] = $codigoRH;
                $NomeArquivo = $_FILES[$i]['name'];
                $ext = @end(explode(".", $NomeArquivo));
                $config['upload_path'] = 'webroot/uploads/recursosHumanos/documentos/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx';
                $novoNome = random_bytes(10);
                $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
                $caminho = base_url('webroot/uploads/recursosHumanos/documentos/') . $novoNomeRandom;
                $dadosAnexo[$i]['Caminho'] = $caminho;
                $dadosAnexo[$i]['Status'] = 1;
                $dadosAnexo[$i]['Data'] = date('Y-m-d  H:i:s');
                $config['file_name'] = $novoNomeRandom;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload($i)) {
                    echo $this->upload->display_errors();
                } else {
                    $result['result'] = 'Salvo com sucesso!';
                }
                $i++;
            }
            echo json_encode($this->RecursoHumano->insertAnexos($dadosAnexo));
        }
    }

    public function inconsistencias(){
        
        $dados['pagina'] = 'cgmab/recursoshumanos/paginas/relatorioInconsistencia';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function getRelatorioRecursos(){
        echo json_encode($this->RecursoHumano->getRelatorioRecursos());
    }

    public function exportarRelatorioRecurso(){
                
        $dados['pagina'] = 'cgmab/recursoshumanos/paginas/exportarRelatorioRecurso';
        $dados['sidebar'] = 'elements/blank.php';
        $dados['header'] = 'elements/blank.php';
        $dados['listaRecursos'] =  $this->RecursoHumano->getRelatorioRecursos();
        $this->load->view('templates/aero', $dados);
    }


}
