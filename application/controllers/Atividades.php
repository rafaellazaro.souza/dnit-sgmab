<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Atividades extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/AtividadeModel', 'Atividade');
        $this->load->helper('form');
        $this->load->helper('url');
    }
    public function listaAtividadesMes()
    {

        $dados['pagina'] = 'cgmab/Atividades/listaAtividadesMes';
        $dados['sidebar'] = 'elements/sidebar';
        //$dados['header'] = 'template/header';
        $this->load->view('templates/aero', $dados);
    }

    public function getAtividadesMes()
    {
        echo json_encode($this->Atividade->getAtividadesMes($this->input->post(null, true)));
    }

    public function getAtividadesEscopoMes(){
        echo json_encode($this->Atividade->getAtividadesEscopoMes($this->input->post(null, true)));
    }
    public function uploadFotos()
    {
        $atividade = $this->input->post('codigoAtividade', true);

        $data = [];
        $count = count($_FILES['files']['name']);
        for ($i = 0; $i < $count; $i++) {
            $temp_name = $_FILES['files']['tmp_name'][$i];
            if (!empty($_FILES['files']['name'][$i])) {
                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $temp_name;
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                $config['upload_path'] = './webroot/uploads/atividades';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                //$config['max_size'] = '1000';
                $config['file_name'] = $_FILES['files']['name'][$i];
                $caminho =  base_url($config['upload_path'] . '/' . $_FILES['file']['name']);

                //pega data da foto

                $exif = exif_read_data($temp_name, 0, true);
                if (isset($exif['IFD0'])) {
                    $d = explode(":", $exif['IFD0']['DateTime']);
                    $dadaFoto = $d[0] . '-' . $d[1] . '-' . $d[2] . ':' . $d[3] . ':' . $d[4];
                } else {
                    $dadaFoto = date('Y-m-d H:i:s');
                }
                //pega dados do gps
                if (!isset($exif['GPS'])) {
                    $nome = $_FILES['file']['name'];
                    $latitude = '';
                    $longitude = '';
                } else {
                    $lat_ref = $exif['GPS']['GPSLatitudeRef'];
                    $lat = $exif['GPS']['GPSLatitude'];
                    list($num, $dec) = explode('/', $lat[0]);
                    $lat_s = $num / $dec;
                    list($num, $dec) = explode('/', $lat[1]);
                    $lat_m = $num / $dec;
                    list($num, $dec) = explode('/', $lat[2]);
                    $lat_v = $num / $dec;
                    $lon_ref = $exif['GPS']['GPSLongitudeRef'];
                    $lon = $exif['GPS']['GPSLongitude'];
                    list($num, $dec) = explode('/', $lon[0]);
                    $lon_s = $num / $dec;
                    list($num, $dec) = explode('/', $lon[1]);
                    $lon_m = $num / $dec;
                    list($num, $dec) = explode('/', $lon[2]);
                    $lon_v = $num / $dec;
                    $latitude = '-' . ($lat_s + $lat_m) / (60.0 + $lat_v) / 3600.0;
                    $longitude = '-' . ($lon_s  + $lon_m) / (60.0 + $lon_v) / 3600.0;
                }

                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                //envia foto para diretório
                if ($this->upload->do_upload('file')) {
                    // $uploadData = $this->upload->data();
                    // $filename = $uploadData['file_name'];
                    //salva informações no banco
                    $this->Atividade->uploadFotos($atividade, $caminho, $dadaFoto,  $latitude, $longitude);
                    // $data['totalFiles'][] = $filename;
                } else {
                    echo "Erroooo";
                }
            }
        }
    }

    public function verificaSubtipoTema(){
        echo json_encode($this->Atividade->verificaSubtipoTema($this->input->post(null, true)));
    }

    public function salvarAtividadesRelatorioMes(){
        $dados = json_decode($this->input->post('Dados', true), true);
        $array =  (array)  $dados;
        // print_r($array);exit;
        $retorno = $this->Atividade->salvarAtividadesRelatorioMes($array);
        echo json_encode($retorno);
    }

    public function verificarAtividadeNoRelatorio(){
        $dados = $this->input->post(null, true);
        echo json_encode($this->Atividade->verificarAtividadeNoRelatorio($dados));
    }
    
    public function getRecursosAtividadesContrato(){
        $dados = $this->input->post(null, true);
        echo json_encode($this->Atividade->getRecursosAtividadesContrato($dados));
    }


    public function salvarAnexos()
    {//Descricao, Data, Caminho, Status
        $i = 0;

        $codigoAtividade = $_REQUEST['codigoAtividade'];
        foreach ($_FILES as $key) {
            $nome = $key['name'];
            $cord[$i]['Descricao'] = $nome;
            $cord[$i]['CodigoAtividadeCronogramaFisico'] = $codigoAtividade;
            $NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'webroot/uploads/anexos/atividades/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx|xls|xlsx';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/anexos/atividades/') . $novoNomeRandom;
            $cord[$i]['Caminho'] = $caminho;
            $cord[$i]['Status'] = 1;
            $cord[$i]['Data'] = date('Y-m-d  H:i:s');
            $config['file_name'] = $novoNomeRandom;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $i++;
        }
        echo $this->Atividade->salvarAnexos($cord);
    }
}
