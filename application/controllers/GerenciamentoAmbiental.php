<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GerenciamentoAmbiental extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        // $this->load->model('dpp/Dpp_model', 'dpp');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function index() {
        $dados['pagina'] = 'cgmab/gerenciamento_ambiental/index';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function relatorio_mobilizacao() {
        $dados['pagina'] = 'cgmab/gerenciamento_ambiental/relatorio_mobilizacao';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function relatorio_zero() {
        $dados['pagina'] = 'cgmab/gerenciamento_ambiental/relatorio_zero';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function relatorio_mensais() {
        $dados['pagina'] = 'cgmab/gerenciamento_ambiental/relatorio_mensais';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function relatorio_periodicos() {
        $dados['pagina'] = 'cgmab/gerenciamento_ambiental/relatorio_periodicos';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function relatorio_final() {
        $dados['pagina'] = 'cgmab/gerenciamento_ambiental/relatorio_final';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

}
