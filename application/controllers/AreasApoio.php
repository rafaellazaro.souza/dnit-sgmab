<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AreasApoio extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/AreaApoioModel', 'AreaApoio');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');
    }

    public function salvar()
    {
        json_encode($this->AreaApoio->salvar($this->input->get('CodigoContrato', true)));
    }

    public function getDadosPassagemGrupoFaunistico(){
        echo json_encode($this->AreaApoio->getDadosPassagemGrupoFaunistico($this->input->post(null, true)));
    }
    public function dadosRiquezaAbundancia(){
        echo json_encode($this->AreaApoio->dadosRiquezaAbundancia($this->input->post(null, true)));
    }

   
}
