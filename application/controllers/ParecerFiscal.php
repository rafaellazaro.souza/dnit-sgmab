<?php

class ParecerFiscal extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/ParecerModel', 'parecer');
        $this->load->helper(['form', 'url']);
    }

    public function getParecerConfigAdmContrato()
    {
        $filtro = $this->input->post(null, true);

        $dados = [
            'contrato' => $this->parecer->get('tblContratos', false, $filtro, ['Status', 'DataEnvioValidacao', 'PrazoCorrecao']),
            'parecer' => $this->parecer->getParecerWithFiscal(true, $filtro, 'tblParecerFiscal'),
            'usuario' => [
                'NomeUsuario' => $this->session->Logado['NomeUsuario'],
                'CodigoUsuario' => $this->session->Logado['CodigoUsuario'],
                'Perfil' => $this->session->PerfilLogin
            ]
        ];

        die(json_encode($dados));
    }

    public function getParecerProgramas()
    {
        $filtro = $this->input->post(null, true);

        $dados = [
            'parecer' => $this->parecer->getParecerWithFiscal(true, $filtro, 'tblParecerFiscalProgramas'),
            'usuario' => [
                'NomeUsuario' => $this->session->Logado['NomeUsuario'],
                'CodigoUsuario' => $this->session->Logado['CodigoUsuario'],
                'Perfil' => $this->session->PerfilLogin
            ]
        ];

        die(json_encode($dados));
    }

    public function getParecerAtividades()
    {
        $filtro = $this->input->post(null, true);

        $dados = [
            'parecer' => $this->parecer->getParecerWithFiscal(true, $filtro, 'tblParecerFiscalAtividades'),
            'usuario' => [
                'NomeUsuario' => $this->session->Logado['NomeUsuario'],
                'CodigoUsuario' => $this->session->Logado['CodigoUsuario'],
                'Perfil' => $this->session->PerfilLogin
            ]
        ];

        die(json_encode($dados));
    }

    public function getRelatoriosDeAtividades()
    {
        $codigoFiscal = $this->session->Logado['CodigoUsuario'];

        $relatorios = $this->parecer->getRelatoriosDeAtividades($codigoFiscal);

        die(json_encode(['relatorios' => $relatorios]));
    }

    public function validacaoProgramas()
    {
        $dados['pagina'] = 'cgmab/ParecerFiscal/parecerProgramas';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function validarAtividades()
    {
        if ($this->session->PerfilLogin !=  'Fiscal') {
            redirect('/');
        }

        $dados['pagina'] = 'cgmab/ParecerFiscal/parecerFiscaAtividades';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function relatoriosMensais()
    {
        if ($this->session->PerfilLogin !=  'Fiscal') {
            redirect('/');
        }

        $dados['pagina'] = 'cgmab/ParecerFiscal/parecerAtividadesLista';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function getListaProgramasContrato()
    {
        $filtro = $this->input->post(null, true);

        if ($this->session->PerfilLogin == 'Fiscal') {
            $filtro['CodigoFiscal'] = $this->session->Logado['CodigoUsuario'];
        }

        $programas = $this->parecer->getProgramasParaValidacao($filtro);

        die(json_encode(['programas' => $programas]));
    }
}
