<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/UsuarioModel', 'Usuario');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');

    }

    public function index()
    {
        $dados['pagina'] = 'cgmab/Usuarios/index';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }
    public function dados()
    {
        $dados['pagina'] = 'cgmab/Usuarios/dados';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    /*
    * Inserir Dados do usuário
    */
    public function insert(){
        $dados = $this->input->post(NULL, true);
        echo json_encode($this->Usuario->insert($dados));

    }


}
