<?php

defined('BASEPATH') or exit('No direct script access allowed');

class LicenciamentoAmbiental extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('cgmab/LicenciamentoAmbientalModel', 'licencasModel');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library(['ImportExcelPhpOffice']);

		$this->load->helper('shapefile_helper');
	}

	public function index()
	{
		$dados['pagina'] = 'cgmab/licencasAmbientais/index';
		$dados['sidebar'] = 'elements/sidebar';
		$dados['header'] = 'template_adminlte3/header';
		$this->load->view('templates/aero', $dados);
	}

    public function getLicencas()
    {
        echo json_encode($this->licencasModel->getLicencas());
    }

    //upload arquivo xslx
    public function uploadCsv()
    {

        if (!empty($_FILES['file']['name'])) {
            $arquivo = $_FILES['file'];
            //$dadosDaPlanilha = $this->importexcelphpoffice->lerArquivo($arquivo);
            $dadosDaPlanilha = $this->importexcelphpoffice->lerArquivo($arquivo);
            //armazena lista em sessao
            $this->session->unset_userdata('listaLicencas');

            
            $this->licencasModel->gravarLicencas($dadosDaPlanilha);
        } else {
            die(false);
        }
    }

	public function postShapefile()
	{
		//		$codigoLicenca = $this->input->post("CodigoLicenca", true);
		$rotas = null;
		//		$infoShapefile = $this->ConfiguracaoSupressaoVegetacao->getInfoShapeFile($codigoLicenca);
		$cord = [];

		if (pathinfo($_FILES['Coordenada']['name'], PATHINFO_EXTENSION) == 'zip') {
			$rotas = $this->readShapefile('Coordenada');

			$cord = [
				'Coordenada' => json_encode($rotas)
			];
			//				'CodigoLicenca' => $codigoLicenca];
			echo json_encode($rotas);

			// echo $this->Relatorio->saveShapeFile('tblAtropelamentoFaunaRelatorioAnalises', $cord, $infoShapefile);

		}
	}

	private function readShapefile($nomeCampo)
	{
		$rotas = [];
		$path = './webroot/uploads/configuracao-supresao-vegetacao/shapefile/' . uniqid(date('dmYHis'));

		extractZip($_FILES[$nomeCampo]['tmp_name'], $path);

		$this->load->library('readshapefile', [
			'path' => $path,
			'file' => returnFilePathByExtension($path, 'shp'),
		]);

		$rotas = $this->readshapefile->read();

		$this->readshapefile->clean();
		if ($this->readshapefile->fail() && empty($rotas)) {
			http_response_code(500);
			die(json_encode($this->readshapefile->getErrors()));
		}

		return $rotas;
	}

	public function postNovaLicenca()
	{
		$dados = $this->input->post(null, true);
		$array =  (array)  $dados;
		echo json_encode($this->licencasModel->postNovaLicenca($array));
	}
}
