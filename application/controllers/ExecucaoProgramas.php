<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ExecucaoProgramas extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/ExecucaoProgramaModel', 'ExecucaoPrograma');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');
    }

    public function index()
    {
        $dados['pagina'] = 'cgmab/ExecucaoProgramas/index';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'elements/header';
        $this->load->view('templates/aero', $dados);
    }

    public function salvarPontosColeta()
    {

        $dados = json_decode($this->input->post('Dados', true), true);
        $array =  (array)  $dados;


        $dados = [];
        $i = 0;
        foreach ($array as $dado) {
            $dados[$i]['CodigoEscopoPrograma'] = $this->session->CodigoPrograma;
            $dados[$i]['NomePontoColeta'] = $dado['NomePontoColeta'];
            $dados[$i]['Classe1'] = $dado['Classe1'];
            $dados[$i]['Classe2'] = $dado['Classe2'];
            $dados[$i]['Classe3'] = $dado['Classe3'];
            $dados[$i]['Classe4'] = $dado['Classe4'];
            $dados[$i]['MassaAgua'] = $dado['MassaAgua'];
            $dados[$i]['TipoMassaAgua'] = $dado['TipoMassaAgua'];
            $dados[$i]['ClasseHidrica'] = $dado['ClasseHidrica'];
            $dados[$i]['TipoAmostra'] = $dado['TipoAmostra'];
            $dados[$i]['TipoAmbiente'] = $dado['TipoAmbiente'];
            $dados[$i]['UF'] = $dado['UF'];
            $dados[$i]['Municipio'] = $dado['Municipio'];
            $dados[$i]['Bacia'] = $dado['Bacia'];
            $dados[$i]['KmInicial'] = $dado['KmInicial'];
            $dados[$i]['KmFinal'] = $dado['KmFinal'];
            $dados[$i]['Estaca'] = $dado['Estaca'];
            $dados[$i]['Observacoes'] = $dado['Observacoes'];
            $i++;
        }
        $retorno = $this->ExecucaoPrograma->salvarPontosColeta($dados);
        
        echo json_encode($retorno);
    }

    public function getAndamentoAtividadesPrevistasRealizadas()
    {
        $dados = $this->input->post('AnoAtividade', false);
        $retorno = $this->ExecucaoPrograma->getAndamentoAtividadesPrevistasRealizadas($dados);
        echo json_encode($retorno);
    }
}
