<?php

defined('BASEPATH') or exit('No direct script access allowed');
class RelatorioMonitoramentoAgua extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/RelatorioMonitoramentoAguaModel', 'RelatorioAgua');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function getParametrosPonto()
    {
        $ponto = $this->input->post(null, true);
        echo json_encode($this->RelatorioAgua->getParametrosPonto($ponto));
    }

    public function uploadAnexo()
    {
        $retorno = [];
        $caminho =  'webroot/uploads/anexos/' . $_FILES['file']['name'];
        if (move_uploaded_file($_FILES["file"]["tmp_name"], $caminho)) {
            $retorno['Status'] = true;
            return json_encode($retorno);
        } else {
            $retorno['Status'] = false;
            return json_encode($retorno);
        }
    }

    public function deleteFile()
    {
        $caminho = explode('//webroot', $this->input->post('Caminho', true))[1];
        unlink('webroot' . $caminho);
    }

    public function getParametroAnalizado()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->RelatorioAgua->getParametroAnalizado($dados));
    }

    public function getDadosGraficoParametros()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->RelatorioAgua->getDadosGraficoParametros($dados));
    }

    public function getAnaliseResultadosPontos()
    {
        $dados = $this->input->post(null, true);

        $consultaAnaliseResultados = $this->RelatorioAgua->getAnaliseResultadosPontos($dados);
        $consultaConclusao = $this->RelatorioAgua->getConclusaoIqaPontos($dados);
        $consultaBibliografia = $this->RelatorioAgua->getBibliografiaIqaPontos($dados);

        echo json_encode([
            'AnaliseResultadoIQA' => count($consultaAnaliseResultados) > 0 ? $consultaAnaliseResultados[0]['AnaliseResultadoIQA'] . "-" . $consultaAnaliseResultados[0]['CodigoAnaliseResultados'] : null,
            'ConclusoesIQA' => count($consultaConclusao) > 0 ? $consultaConclusao[0]['ConclusoesIQA'] . "-" . $consultaConclusao[0]['CodigoAnaliseResultados'] : null,
            'BibliografiaIQA' => count($consultaBibliografia) > 0 ? $consultaBibliografia[0]['BibliografiaIQA'] . "-" . $consultaBibliografia[0]['CodigoAnaliseResultados'] : null,
        ]);
        
    }

    public function getDadosGraficoIqa()
    {
        $dados = $this->input->post(null, true);
        
        $result = $this->RelatorioAgua->getDadosGraficoIqa($dados);
        $resultadoGrafico = [];
        // echo '<pre>';
        // print_r($result);
        // exit;

        foreach ($result as $i) {
            $valorAnterior = $this->RelatorioAgua->atividadesAnteriores($i['MesAtividade'], $i['CodigoEscopoPrograma'], $i['CodigoConfiguracaoRecursosHidricosPontoColeta'], $i['TipoPontoAmostra']);
          array_push( $resultadoGrafico,  [
                'CodigoConfiguracaoRecursosHidricosPontoColeta' => $i['CodigoConfiguracaoRecursosHidricosPontoColeta'],
                'TipoPontoAmostra' => $i['TipoPontoAmostra'],
                'ValorAtual' => $i['ValorAtual'],
                'DataMesAtividade' => $i['DataMesAtividade'],
                'ValorAnterior' => @intval($valorAnterior[0]['Valor']),
                'DataAnterior' => @$valorAnterior[0]['DataMesAtividade']
            ]);
            //$result[$i]['ValorAnterior'] = 1; //$valorAnterior[0]['Valor'];
        }

        echo json_encode($resultadoGrafico);

        
    }
}
