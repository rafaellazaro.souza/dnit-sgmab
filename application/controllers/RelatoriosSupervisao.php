<?php

class RelatoriosSupervisao extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('cgmab/RelatorioSupervisaoModel');
        $this->load->helper(['form', 'url', 'helpersFunctions']);
    }

    public function index()
    {
        $dados['pagina'] = 'cgmab/RelatoriosSupervisao/index';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function relatorio($codigoRelatorio = null)
    {
        if (is_null($codigoRelatorio)) {
            redirect('RelatoriosSupervisao', 'refresh');
        }

        $dados['pagina'] = 'cgmab/RelatoriosSupervisao/relatorio';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function getAllFromRelatorio()
    {
        $filtro = $this->input->post(null, true);

        $dados = [
            'relatorio' => $this->RelatorioSupervisaoModel->getRelatorio($filtro),
            'contrato' => $this->RelatorioSupervisaoModel->getContratos($filtro),
            'diarios' => $this->RelatorioSupervisaoModel->getDiarios($filtro),
            'ocorrencias' => $this->RelatorioSupervisaoModel->getOcorrencias($filtro),
            'parecer' => $this->RelatorioSupervisaoModel->getParecerTecnico($filtro),
            'discussoes' => $this->RelatorioSupervisaoModel->getFrom('tblRelatorioSupervisaoDiscussoesConclusoes', $filtro),
            'introducao' => $this->RelatorioSupervisaoModel->getFrom('tblRelatorioSupervisaoIntroducao', $filtro),
            'arquivos' => $this->RelatorioSupervisaoModel->getFrom('tblRelatorioSupervisaoAnexos', $filtro),
            'programas' => $this->RelatorioSupervisaoModel->getProgramas($filtro),
            'usuario' => [
                'CodigoUsuario' => $this->session->Logado['CodigoUsuario'],
                'NomeUsuario' => $this->session->Logado['NomeUsuario'],
                'Perfil' =>  $this->session->PerfilLogin
            ]
        ];

        die(json_encode($dados));
    }

    public function uploadDocumentos()
    {
        $codigo = $this->input->post(null, true);

        if (!hasMultipleFile('anexos')) {
            responseJSON(500, false, 'Não foram enviados Arquivos.');
        }

        $files = groupMultFiles('anexos');

        $config['upload_path'] = 'webroot/uploads/relatorio_anexos/';
        $config['allowed_types'] = 'xlsx|xls|doc|docx|pdf|csv|jpg|jpeg|png|gif';

        $erro = 0;
        foreach ($files as $key => $file) {
            $config['file_name'] = bin2hex(random_bytes(10)) . '.' . @end(explode('.', $file['name']));

            $arquivo['Nome'] =  $file['name'];
            $arquivo['Arquivo'] = base_url($config['upload_path'] . $config['file_name']);
            $arquivo = array_merge($arquivo, $codigo);

            if (!$this->uploadFiles($config, $file)) {
                continue;
                $erro += 1;
            }

            if (!$this->RelatorioSupervisaoModel->insertDocumentos($arquivo)) {
                unlink($config['upload_path'] . $config['file_name']);
                $erro += 1;
            }
        }

        die(json_encode(['result' => true, 'erro' => $erro]));
    }

    private function uploadFiles($config, $file)
    {
        $_FILES['file'] = $file;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('file')) {
            return true;
        }

        return false;
    }

    public function saveProgramas()
    {
        $programas = $this->input->post(null, true);

        $update = true;
        $insert = true;

        if (array_key_exists('insert', $programas)) {
            $insert = $this->RelatorioSupervisaoModel->insertProgramas($programas['insert']);
        }

        if (array_key_exists('update', $programas)) {
            $update = $this->RelatorioSupervisaoModel->updateProgramas($programas['update']);
        }

        die(json_encode([
            'insert' => $insert,
            'update' => $update
        ]));
    }

    public function getProgramas()
    {
        $filtro = $this->input->post(null, true);
        $programas = $this->RelatorioSupervisaoModel->getProgramas($filtro);
        die(json_encode(['programas' => $programas]));
    }

    public function getRelatorios()
    {
        $filtro = ($this->session->has_userdata('ContratoSelecionando')) ?  ['cont.CodigoContrato' => $this->session->ContratoSelecionando] : [];
        $dados = [
            'relatorios' => $this->RelatorioSupervisaoModel->getRelatorio($filtro),
            'contrato' => [],
            'usuario' => [
                'CodigoUsuario' => $this->session->Logado['CodigoUsuario'],
                'NomeUsuario' => $this->session->Logado['NomeUsuario'],
                'Perfil' =>  $this->session->PerfilLogin
            ]
        ];

        if ($this->session->has_userdata('ContratoSelecionando')) {
            $dados['contrato'] = $this->RelatorioSupervisaoModel->getContratos($filtro)[0];
        }

        die(json_encode($dados));
    }
}
