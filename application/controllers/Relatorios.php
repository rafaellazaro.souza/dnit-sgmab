<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Relatorios extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/RelatorioModel', 'Relatorio');
        $this->load->helper('form');
        $this->load->helper('url');

        $this->load->helper('shapefile_helper');
    }

    public function atividades()
    {

        $dados['pagina'] = 'cgmab/Relatorios/atividades';
        $dados['sidebar'] = 'elements/sidebar';
        //$dados['header'] = 'template/header';
        $this->load->view('templates/aero', $dados);
    }
    public function monitoramentoAgua()
    {

        $dados['pagina'] = 'cgmab/Relatorios/relatorioMensalMonitoramentoAgua';
        $dados['sidebar'] = 'elements/sidebar';
        //$dados['header'] = 'template/header';
        $this->load->view('templates/aero', $dados);
    }


    public function getPontos()
    {
        echo json_encode($this->Relatorio->getPontos($this->input->post(null, true)));
    }

    public function monitoramentoFauna()
    {
        $dados['pagina'] = 'cgmab/Relatorios/monitoramentoFauna';
        $dados['sidebar'] = 'elements/sidebar';
        //$dados['header'] = 'template/header';
        $this->load->view('templates/aero', $dados);
    }
    public function passagemFauna()
    {
        $dados['pagina'] = 'cgmab/Relatorios/passagemFauna';
        $dados['sidebar'] = 'elements/sidebar';
        //$dados['header'] = 'template/header';
        $this->load->view('templates/aero', $dados);
	}
	
    // public function pasagemFauna(){
    // $dados['pagina'] = 'cgmab/Relatorios/monitoramentoFauna';
    // $dados['sidebar'] = 'elements/sidebar';
    // //$dados['header'] = 'template/header';
    // $this->load->view('templates/aero', $dados);
    // }
	
	public function relatorioAtropelamentoFauna()
    {
        $dados['pagina'] = 'cgmab/Relatorios/relatorioAtropelamentoFauna';
        $dados['sidebar'] = 'elements/sidebar';
        //$dados['header'] = 'template/header';
        $this->load->view('templates/aero', $dados);
	}
	
	public function listarRelatoriosMensais()
    {
        $dados['pagina'] = 'cgmab/Relatorios/mensais';
        $dados['sidebar'] = 'elements/sidebar';
        //$dados['header'] = 'template/header';
        $this->load->view('templates/aero', $dados);
    }

    public function atropelamentoFauna()
    {
        $dados['pagina'] = 'cgmab/Relatorios/atropelamentoFauna';
        $dados['sidebar'] = 'elements/sidebar';
        //$dados['header'] = 'template/header';
        $this->load->view('templates/aero', $dados);
	}
	
	public function afugentamentoFauna()
    {
        $dados['pagina'] = 'cgmab/Relatorios/afugentamentoFauna';
        $dados['sidebar'] = 'elements/sidebar';
        //$dados['header'] = 'template/header';
        $this->load->view('templates/aero', $dados);
    }

    public function postMonitoramentoFaunaDocumentos()
    {
        $i = 0;

        $codigoAtividade = $_REQUEST['codigoAtividade'];
        foreach ($_FILES as $key) {
            $nome = $key['name'];
            $cord[$i]['Nome'] = $nome;
            $cord[$i]['CodigoAtividadeCronogramaFisico'] = $codigoAtividade;
            $NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'webroot/uploads/relatorio-monitoramento-fauna/documentos-introducao/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|xlsx|docx|pptx';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/relatorio-monitoramento-fauna/documentos-introducao/') . $novoNomeRandom;
            $cord[$i]['Caminho'] = $caminho;
            $cord[$i]['Ativo'] = 1;

            $config['file_name'] = $novoNomeRandom;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $i++;
        }
        echo $this->Relatorio->inserirDocumentosPadrao($cord, 'tblMonitoramentoFaunaRelatorioAnexos');
    }
    public function postPassagemFaunaDocumentos()
    {
        $i = 0;

        $codigoAtividade = $_REQUEST['codigoAtividade'];
        foreach ($_FILES as $key) {
            $nome = $key['name'];
            $cord[$i]['Nome'] = $nome;
            $cord[$i]['CodigoAtividadeCronogramaFisico'] = $codigoAtividade;
            $NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'webroot/uploads/relatorio_passagem_fauna/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/relatorio_passagem_fauna/') . $novoNomeRandom;
            $cord[$i]['Caminho'] = $caminho;
            $cord[$i]['Ativo'] = 1;
            $cord[$i]['DataCadastro'] = date('Y-m-d  H:i:s');
            $config['file_name'] = $novoNomeRandom;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $i++;
        }
        echo $this->Relatorio->inserirDocumentosPadrao($cord, 'tblPassagemFaunaDocumentos');
    }

    public function salvarDadosInsercaoFauna()
    {
        $CodigoAtividadeCronogramaFisico = $this->input->post('CodigoAtividadeCronogramaFisico', true);
        $dados = json_decode($this->input->post('Dados', true), true);

        $array =  (array)  $dados;

        $dados = [];
        $i = 0;
        foreach ($array as $dado) {
            $dados[$i]['CodigoAtividadeCronogramaFisico'] = intval($CodigoAtividadeCronogramaFisico);
            // $dados[$i]['CodigoConfiguracaoMonitoramentoFaunaFotos'] = $dado['IDArmadilha'];
            $dados[$i]['TipoAvistamento'] = $dado['TipoIdentificacao'];
            $dados[$i]['Grupo'] = $dado['Grupo'];
            $dados[$i]['Classe'] = $dado['Classe'];
            $dados[$i]['Ordem'] = $dado['Ordem'];
            $dados[$i]['Familia'] = $dado['Familia'];
            $dados[$i]['Especie'] = $dado['Especie'];
            $dados[$i]['NomeComum'] = $dado['NomeComum'];
            $dados[$i]['IUCN'] = $dado['IUCN'];
            $dados[$i]['ICMBio'] = $dado['ICMBio'];
            $dados[$i]['Observacao'] = $dado['Observacoes'];
            $i++;
        }

        $retorno = $this->Relatorio->salvarDadosFaunaPadrao($dados, 'tblMonitoramentoFaunaRelatorioAnimais');

        echo json_encode($retorno);
    }


    public function deletarRegistrosFauna()
    {
        $codigo = $this->input->post("Codigo", true);
        echo $this->Relatorio->deletarFaunaPadrao('CodigoRelatorioAnimais', $codigo, 'tblMonitoramentoFaunaRelatorioAnimais');
    }

    public function getGpsFotosFaunaAnimais()
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");

        $i = 0;
        foreach ($_FILES as $key) {
            $temp_name = $key['tmp_name'];
            $exif = exif_read_data($temp_name, 0, true);
            if (isset($exif['IFD0'])) {
                $d = explode(":", $exif['IFD0']['DateTime']);
                $dadaFoto = $d[0] . '-' . $d[1] . '-' . $d[2] . ':' . $d[3] . ':' . $d[4];
            } else {
                $dadaFoto = date('Y-m-d H:i:s');
            }
            if (!isset($exif['GPS'])) {
                $nome = $key['name'];
                $lat_ = '';
                $lng_ = '';
            } else {
                $lat_ref = $exif['GPS']['GPSLatitudeRef'];
                $lat = $exif['GPS']['GPSLatitude'];
                list($num, $dec) = explode('/', $lat[0]);
                $lat_s = $num / $dec;
                list($num, $dec) = explode('/', $lat[1]);
                $lat_m = $num / $dec;
                list($num, $dec) = explode('/', $lat[2]);
                $lat_v = $num / $dec;
                $lon_ref = $exif['GPS']['GPSLongitudeRef'];
                $lon = $exif['GPS']['GPSLongitude'];
                list($num, $dec) = explode('/', $lon[0]);
                $lon_s = $num / $dec;
                list($num, $dec) = explode('/', $lon[1]);
                $lon_m = $num / $dec;
                list($num, $dec) = explode('/', $lon[2]);
                $lon_v = $num / $dec;
                $nome = $key['name'];
                $lat_ = '-' . ($lat_s + $lat_m / 60.0 + $lat_v / 3600.0);
                $lng_ = '-' . ($lon_s  + $lon_m / 60.0 + $lon_v / 3600.0);
            }

            $cord[$i]['NomeFoto'] = $nome;
            $cord[$i]['Lat'] =  (string) $lat_;
            $cord[$i]['Long'] = (string) $lng_;
            $cord[$i]['DataFoto'] =  $dadaFoto;

            $NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $CodigoAnimal = explode("_",$NomeArquivo)[0];
            $cord[$i]['CodigoAnimalCapturado'] = $CodigoAnimal;
            $config['upload_path'] = 'webroot/uploads/relatorio_passagem_fauna/fotos_animais/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/relatorio_passagem_fauna/fotos_animais/') . $novoNomeRandom;
            $cord[$i]['CaminhoFotoAnimaisCapturados'] = $caminho;
            $cord[$i]['DataCadastro'] = $data;
            $cord[$i]['EmailCriador'] = $_SESSION['Logado']['Email'];
            $config['file_name'] = $novoNomeRandom;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $i++;
        }
        echo $this->Relatorio->inserirFotosFaunaPadrãoBach($cord, 'tblPassagemFaunaAnimaisCapturadosFoto');
    }
    public function getGpsFotosFauna()
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");

        $i = 0;
        foreach ($_FILES as $key) {
            $temp_name = $key['tmp_name'];
            $exif = exif_read_data($temp_name, 0, true);
            if (isset($exif['IFD0'])) {
                $d = explode(":", $exif['IFD0']['DateTime']);
                $dadaFoto = $d[0] . '-' . $d[1] . '-' . $d[2] . ':' . $d[3] . ':' . $d[4];
            } else {
                $dadaFoto = date('Y-m-d H:i:s');
            }
            if (!isset($exif['GPS'])) {
                $nome = $key['name'];
                $lat_ = '';
                $lng_ = '';
            } else {
                $lat_ref = $exif['GPS']['GPSLatitudeRef'];
                $lat = $exif['GPS']['GPSLatitude'];
                list($num, $dec) = explode('/', $lat[0]);
                $lat_s = $num / $dec;
                list($num, $dec) = explode('/', $lat[1]);
                $lat_m = $num / $dec;
                list($num, $dec) = explode('/', $lat[2]);
                $lat_v = $num / $dec;
                $lon_ref = $exif['GPS']['GPSLongitudeRef'];
                $lon = $exif['GPS']['GPSLongitude'];
                list($num, $dec) = explode('/', $lon[0]);
                $lon_s = $num / $dec;
                list($num, $dec) = explode('/', $lon[1]);
                $lon_m = $num / $dec;
                list($num, $dec) = explode('/', $lon[2]);
                $lon_v = $num / $dec;
                $nome = $key['name'];
                $lat_ = '-' . ($lat_s + $lat_m / 60.0 + $lat_v / 3600.0);
                $lng_ = '-' . ($lon_s + $lon_m / 60.0 + $lon_v / 3600.0);
            }

            $cord[$i]['NomeFoto'] = $nome;
            $cord[$i]['CodigoRelatorioAnimais'] = preg_replace("/[^0-9]/", "", explode('_', $nome)[0]);
            $cord[$i]['NomePontoMonitoramento'] = explode('_', $nome)[1];
            $cord[$i]['Lat'] = (string) $lat_;
            $cord[$i]['Lng'] = (string) $lng_;
            $cord[$i]['DataFoto'] = $dadaFoto;

            $NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'webroot/uploads/relatorio-monitoramento-fauna/fotos-animais/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/relatorio-monitoramento-fauna/fotos-animais/') . $novoNomeRandom;
            $cord[$i]['Caminho'] = $caminho;

            $config['file_name'] = $novoNomeRandom;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $i++;
        }
        echo $this->Relatorio->inserirFotosFaunaPadrao($cord, 'tblMonitoramentoFaunaRelatorioAnimais', 'CodigoRelatorioAnimais');
    }

    public function getListaMonitoramentoFaunaSemFoto()
    {
        $codigo = $this->input->get("codigo", true);
        $retorno = $this->Relatorio->getListaMonitoramentoFaunaSemFoto($codigo);
        echo json_encode($retorno);
    }

    public function getListaPassagemFaunaArmadilhas(){
        echo $this->Relatorio->getListaPassagemFaunaArmadilhas();
    }

    
    public function getInformacaoRelatorioMonitoramentoFauna()
    {
        $codigoAtividade = $this->input->get("codigoAtividade", true);
        $codigoDoRegistro = $this->input->get("codigoDoRegistro", true);
        $defineColuna = $this->input->get("defineColuna", true);
        $retorno = $this->Relatorio->getInformacaoRelatorioMonitoramentoFauna($codigoAtividade, $codigoDoRegistro, $defineColuna);
        echo json_encode($retorno);
    }


    public function postAtropelamentoFaunaDocumentos()
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");

        $i = 0;

        $codigoAtividade = $_REQUEST['codigoAtividade'];
        foreach ($_FILES as $key) {
            $nome = $key['name'];
            $cord[$i]['Nome'] = $nome;
            $cord[$i]['CodigoAtividadeCronogramaFisico'] = $codigoAtividade;
            $NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'webroot/uploads/relatorio-atropelamento-fauna/documentos-introducao/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|xlsx|docx|pptx';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/relatorio-atropelamento-fauna/documentos-introducao/') . $novoNomeRandom;
            $cord[$i]['Caminho'] = $caminho;
            $cord[$i]['Ativo'] = 1;
            $cord[$i]['DataCadastro'] = $data;

            $config['file_name'] = $novoNomeRandom;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $i++;
        }
        echo $this->Relatorio->inserirDocumentosPadrao($cord, 'tblAtropelamentoFaunaAnalisesAnexos');
    }

    public function salvarAtropelamentoFaunaDadosInsercao()
    {
        $CodigoEscopoPrograma = $this->input->post('CodigoEscopoPrograma', true);
        $dados = json_decode($this->input->post('Dados', true), true);

        $array = (array) $dados;

        $dados = [];
        $i = 0;
        foreach ($array as $dado) {
            $dados[$i]['CodigoEscopoPrograma'] = intval($CodigoEscopoPrograma);
            $dados[$i]['Grupo'] = $dado['Grupo'];
            $dados[$i]['Classe'] = $dado['Classe'];
            $dados[$i]['Ordem'] = $dado['Ordem'];
            $dados[$i]['Familia'] = $dado['Familia'];
            $dados[$i]['Especie'] = $dado['Especie'];
            $dados[$i]['NomeComum'] = $dado['NomeComum'];
            $dados[$i]['TipoRegistro'] = $dado['TipoRegistro'];
            $dados[$i]['TipoMonitoramento'] = $dado['TipoMonitoramento'];
            $dados[$i]['Zona'] = $dado['Zona'];
            $dados[$i]['CN'] = $dado['CN'];
            $dados[$i]['CE'] = $dado['CE'];
            $dados[$i]['Elevacao'] = $dado['Elevacao'];
            $dados[$i]['Observacoes'] = $dado['Observacoes'];
            $i++;
        }

        $retorno = $this->Relatorio->salvarDadosFaunaPadrao($dados, 'tblAtropelamentoFaunaRelatorioAnimais');

        echo json_encode($retorno);
    }

    public function getListaAtropelamentoFaunaSemFoto()
    {
        $codigo = $this->input->get("codigo", true);
        $retorno = $this->Relatorio->getListaAtropelamentoFaunaSemFoto($codigo);
        echo json_encode($retorno);
    }

    public function deletarRegistroAtropelamentoFauna()
    {
        $codigo = $this->input->post("Codigo", true);
        echo $this->Relatorio->deletarFaunaPadrao('CodigoAtropelamentoAnimais', $codigo, 'tblAtropelamentoFaunaRelatorioAnimais');
    }

    public function getGpsFotosAtropelamentoFauna()
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");

		$i = 0;
		echo "<pre>";
        foreach ($_FILES as $key) {
            $temp_name = $key['tmp_name'];
            $exif = exif_read_data($temp_name, 0, true);
            if (isset($exif['IFD0'])) {
                $d = explode(":", $exif['IFD0']['DateTime']);
                $dadaFoto = $d[0] . '-' . $d[1] . '-' . $d[2] . ':' . $d[3] . ':' . $d[4];
            } else {
                $dadaFoto = date('Y-m-d H:i:s');
            }
            if (!isset($exif['GPS'])) {
                $nome = $key['name'];
                $lat_ = '';
                $lng_ = '';
            } else {
                $lat_ref = $exif['GPS']['GPSLatitudeRef'];
                $lat = $exif['GPS']['GPSLatitude'];
                list($num, $dec) = explode('/', $lat[0]);
                $lat_s = $num / $dec;
                list($num, $dec) = explode('/', $lat[1]);
                $lat_m = $num / $dec;
                list($num, $dec) = explode('/', $lat[2]);
                $lat_v = $num / $dec;
                $lon_ref = $exif['GPS']['GPSLongitudeRef'];
                $lon = $exif['GPS']['GPSLongitude'];
                list($num, $dec) = explode('/', $lon[0]);
                $lon_s = $num / $dec;
                list($num, $dec) = explode('/', $lon[1]);
                $lon_m = $num / $dec;
                list($num, $dec) = explode('/', $lon[2]);
                $lon_v = $num / $dec;
                $nome = $key['name'];
                $lat_ = '-' . ($lat_s + $lat_m / 60.0 + $lat_v / 3600.0);
                $lng_ = '-' . ($lon_s + $lon_m / 60.0 + $lon_v / 3600.0);
            }

            $cord[$i]['NomeFoto'] = $nome;
            $cord[$i]['CodigoAtropelamentoAnimais'] = preg_replace("/[^0-9]/", "", explode('_', $nome)[0]);
            $cord[$i]['NomeFoto'] = explode('_', $nome)[1];
            $cord[$i]['Lat'] = (string) $lat_;
            $cord[$i]['Long'] = (string) $lng_;
            $cord[$i]['DataFoto'] = $dadaFoto;

			$NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'webroot/uploads/relatorio-atropelamento-fauna/fotos-animais/';

            $config['allowed_types'] = 'jpg|jpeg|png';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/relatorio-atropelamento-fauna/fotos-animais/') . $novoNomeRandom;
            $cord[$i]['CaminhoFoto'] = $caminho;

			$config['file_name'] = $novoNomeRandom;
			$this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $i++;
        }
        echo $this->Relatorio->inserirFotosFaunaPadrao($cord, 'tblAtropelamentoFaunaRelatorioAnimais', 'CodigoAtropelamentoAnimais');
    }

    public function postShapefileAtropelamento()
    {
        $codigoAtividadeCronogramaFisico = $this->input->post("CodigoAtividadeCronogramaFisico", true);
        $rotas = null;
        $infoShapefile = $this->Relatorio->getInfoShapeFile($codigoAtividadeCronogramaFisico);
        $cord = [];

        if (pathinfo($_FILES['GraficoHotSpots']['name'], PATHINFO_EXTENSION) == 'zip') {
            $rotas = $this->readShapefile('GraficoHotSpots');

            $cord = [
                'GraficoHotSpots' => json_encode($rotas),
                'CodigoAtividadeCronogramaFisico' => $codigoAtividadeCronogramaFisico];
			echo json_encode($rotas);
            // echo $this->Relatorio->saveShapeFile('tblAtropelamentoFaunaRelatorioAnalises', $cord, $infoShapefile);

        }
    }

    private function readShapefile($nomeCampo)
    {
        $rotas = [];
        $path = './webroot/uploads/relatorio-atropelamento-fauna/hot-spots/' . uniqid(date('dmYHis'));
		
		extractZip($_FILES[$nomeCampo]['tmp_name'], $path);

        $this->load->library('readshapefile', [
            'path' => $path,
            'file' => returnFilePathByExtension($path, 'shp'),
        ]);

		$rotas = $this->readshapefile->read();
		
        $this->readshapefile->clean();
        if ($this->readshapefile->fail() && empty($rotas)) {
            http_response_code(500);
            die(json_encode($this->readshapefile->getErrors()));
        }

        return $rotas;
	}
	
	public function fichaAtropelamento()
	{
		if(isset($_FILES['files']['name'])){
			$dados = $this->input->post(null, true);
			$nomeArquivo = random_bytes(10);
			$ext = @end(explode(".",  $_FILES['files']['name']));
            $novoNomeRandom = bin2hex($nomeArquivo) . "." . $ext;		
			$dados['NomeFichaAtropelamento'] = $novoNomeRandom;
			$dados['CaminhoFicha'] = base_url('webroot/uploads/relatorio-atropelamento-fauna/ficha-atropelamento/').$dados['NomeFichaAtropelamento'];
			
			$result = $this->Relatorio->fichaAtropelamento($dados);
			if($result)
			{
				$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docx';
				$config['upload_path'] = 'webroot/uploads/relatorio-atropelamento-fauna/ficha-atropelamento/';
				$config['file_name'] = $novoNomeRandom;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('files')) {
                    echo $this->upload->display_errors();
                } else {
					echo 'salvo';
                }
			}
		};
			
	}
    public function salvarRelatorioMensal()
    {

        $dados = $this->input->post(null, true);
        $array =  (array)  $dados;
        $retorno = $this->Relatorio->salvarRelatorioMensal($array);

        echo json_encode($retorno);
    }

    public function insertAnimaisPassagem(){
        $dados = json_decode($this->input->post('Dados', true), true);
        $array =  (array)  $dados;

        // echo '<pre>';
        // print_r($array);exit;
        echo json_encode($this->Relatorio->insertAnimaisPassagem($array));
    }

    public function getTextos(){
        $dados = $this->input->post(null, true);
        echo json_encode($this->Relatorio->getTextos($dados));
	}
	
	public function postAfugentamentoFaunaDocumentos()
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");

        $i = 0;

        $codigoAtividade = $_REQUEST['codigoAtividade'];
        foreach ($_FILES as $key) {
            $nome = $key['name'];
            $cord[$i]['Nome'] = $nome;
            $cord[$i]['CodigoAtividadeCronogramaFisico'] = $codigoAtividade;
            $NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'webroot/uploads/relatorio-afugentamento-fauna/documentos-introducao/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|xlsx|docx|pptx';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/relatorio-afugentamento-fauna/documentos-introducao/') . $novoNomeRandom;
            $cord[$i]['Caminho'] = $caminho;
            $cord[$i]['DataCadastro'] = $data;

            $config['file_name'] = $novoNomeRandom;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $i++;
        }
        echo $this->Relatorio->inserirDocumentosPadrao($cord, 'tblAfugentamentoFaunaAnexos');
	}
	
	public function salvarAfugentamentoFaunaDadosInsercao()
    {
		date_default_timezone_set("Brazil/East");
		$data = date("Y-m-d H:i:s");
		
        $CodigoAtividadeCronogramaFisico = $this->input->post('CodigoAtividadeCronogramaFisico', true);
        $dados = json_decode($this->input->post('Dados', true), true);

        $array = (array) $dados;

        $dados = [];
        $i = 0;
        foreach ($array as $dado) {
            $dados[$i]['CodigoAtividadeCronogramaFisico'] = intval($CodigoAtividadeCronogramaFisico);
            $dados[$i]['GrupoFaunistico'] = $dado['Grupo'];
            $dados[$i]['Classe'] = $dado['Classe'];
            $dados[$i]['Ordem'] = $dado['Ordem'];
            $dados[$i]['Familia'] = $dado['Familia'];
            $dados[$i]['Especie'] = $dado['Especie'];
            $dados[$i]['NomeComum'] = $dado['NomeComum'];
            $dados[$i]['IUCN'] = $dado['IUCN'];
            $dados[$i]['ICMBIO'] = $dado['ICMBio'];
            $dados[$i]['Zona'] = $dado['Zona'];
            $dados[$i]['CN'] = $dado['CN'];
            $dados[$i]['CE'] = $dado['CE'];
            $dados[$i]['Elevacao'] = $dado['Elevacao'];
            $dados[$i]['Registro'] = $dado['Registro'];
            $dados[$i]['TipoRegistro'] = $dado['TipoRegistro'];
            $dados[$i]['Destinacao'] = $dado['Destinacao'];
            $dados[$i]['Observacoes'] = $dado['Observacoes'];
            $dados[$i]['DataCadastro'] = $data;
            $i++;
        }

        $retorno = $this->Relatorio->salvarDadosFaunaPadrao($dados, 'tblAfugentamentoFaunaPontos');

        echo json_encode($retorno);
	}
	
	public function getListaAfugentamentoFaunaSemFoto()
    {
        $codigo = $this->input->get("codigo", true);
        $retorno = $this->Relatorio->getListaAfugentamentoFaunaSemFoto($codigo);
        echo json_encode($retorno);
	}
	
	public function deletarRegistroAfugentamentoFauna()
    {
        $codigo = $this->input->post("Codigo", true);
        echo $this->Relatorio->deletarFaunaPadrao('CodigoPontoAfugentamentoPontos', $codigo, 'tblAfugentamentoFaunaPontos');
	}
	
	public function getGpsFotosAfugentamentoFauna()
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");

		$i = 0;
		echo "<pre>";
        foreach ($_FILES as $key) {
            $temp_name = $key['tmp_name'];
            $exif = exif_read_data($temp_name, 0, true);
            if (isset($exif['IFD0'])) {
                $d = explode(":", $exif['IFD0']['DateTime']);
                $dadaFoto = $d[0] . '-' . $d[1] . '-' . $d[2] . ':' . $d[3] . ':' . $d[4];
            } else {
                $dadaFoto = date('Y-m-d H:i:s');
            }
            if (!isset($exif['GPS'])) {
                $nome = $key['name'];
                $lat_ = '';
                $lng_ = '';
            } else {
                $lat_ref = $exif['GPS']['GPSLatitudeRef'];
                $lat = $exif['GPS']['GPSLatitude'];
                list($num, $dec) = explode('/', $lat[0]);
                $lat_s = $num / $dec;
                list($num, $dec) = explode('/', $lat[1]);
                $lat_m = $num / $dec;
                list($num, $dec) = explode('/', $lat[2]);
                $lat_v = $num / $dec;
                $lon_ref = $exif['GPS']['GPSLongitudeRef'];
                $lon = $exif['GPS']['GPSLongitude'];
                list($num, $dec) = explode('/', $lon[0]);
                $lon_s = $num / $dec;
                list($num, $dec) = explode('/', $lon[1]);
                $lon_m = $num / $dec;
                list($num, $dec) = explode('/', $lon[2]);
                $lon_v = $num / $dec;
                $nome = $key['name'];
                $lat_ = '-' . ($lat_s + $lat_m / 60.0 + $lat_v / 3600.0);
                $lng_ = '-' . ($lon_s + $lon_m / 60.0 + $lon_v / 3600.0);
            }

            $cord[$i]['NomeFoto'] = $nome;
            $cord[$i]['CodigoPontoAfugentamentoPontos'] = preg_replace("/[^0-9]/", "", explode('_', $nome)[0]);
            $cord[$i]['NomeFoto'] = explode('_', $nome)[1];
            $cord[$i]['Lat'] = (string) $lat_;
            $cord[$i]['Long'] = (string) $lng_;
            $cord[$i]['DataFoto'] = $dadaFoto;

			$NomeArquivo = $_FILES[$i]['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'webroot/uploads/relatorio-afugentamento-fauna/fotos-animais/';

            $config['allowed_types'] = 'jpg|jpeg|png';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('webroot/uploads/relatorio-afugentamento-fauna/fotos-animais/') . $novoNomeRandom;
            $cord[$i]['CaminhoFoto'] = $caminho;

			$config['file_name'] = $novoNomeRandom;
			$this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($i)) {
                echo $this->upload->display_errors();
            } else {
                $result['result'] = 'Salvo com sucesso!';
            }
            $i++;
        }
        echo $this->Relatorio->inserirFotosFaunaPadrao($cord, 'tblAfugentamentoFaunaPontos', 'CodigoPontoAfugentamentoPontos');
    }
}
