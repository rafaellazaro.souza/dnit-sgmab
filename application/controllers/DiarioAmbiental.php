<?php

defined('BASEPATH') or exit('No direct script access allowed');

class DiarioAmbiental extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(['form', 'url', 'helpersFunctions']);
        $this->load->model('cgmab/DiarioAmbientalModel');
        $this->load->model('cgmab/EscopoServicoModel');
    }

    public function index()
    {
        if (!$this->session->has_userdata('ContratoSelecionando')) {
            redirect('contratos/listar');
        }

        $contrato = ['CodigoContrato' => $this->session->ContratoSelecionando];
        $servicos = $this->EscopoServicoModel->getEscopoServicoContrato($contrato);

        $escopo = [];
        $escopo = array_map(function ($val) {
            if (array_key_exists('NomeServico', $val) && trim($val['NomeServico']) == 'Supervisão Ambiental') {
                return $val['CodigoEscopo'];
            }
        }, $servicos);

        $escopo = array_values(array_filter($escopo));

        if (empty($escopo)) {
            redirect('contratos/listar');
        }

        redirect('DiarioAmbiental/diarioAmbientalLista/' . codificarDecodificarParametroUrl($escopo[0], true));
    }

    public function diarioAmbientalLista($codigoEscopo = null)
    {
        $this->diarioBaseUrlRedirect($codigoEscopo);
    }

    public function getDiarios()
    {
        $filtro = $this->input->post(null, true);
        $result = $this->DiarioAmbientalModel->getDiarios($filtro);
        die(json_encode(['diarios' => $result]));
    }

    public function uploadFotos()
    {
        $codigo = $this->input->post(null, true);

        if (!hasMultipleFile('fotos')) {
            responseJSON(500, false, 'Não foram enviados Arquivos.');
        }

        $files = groupMultFiles('fotos');

        $config['upload_path'] = 'webroot/uploads/fotos_diario/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';

        $erro = 0;
        foreach ($files as $key => $file) {
            $config['file_name'] = bin2hex(random_bytes(10)) . '.' . @end(explode('.', $file['name']));
            $dadosFoto = extrairDadosFotos($file['tmp_name']);
            $dadosFoto['CaminhoFoto'] = base_url($config['upload_path'] . $config['file_name']);
            $dadosFoto = array_merge($dadosFoto, $codigo);


            if (!$this->uploadFiles($config, $file)) {
                continue;
                $erro += 1;
            }

            if (!$this->DiarioAmbientalModel->insertFoto($dadosFoto)) {
                unlink($config['upload_path'] . $config['file_name']);
                $erro += 1;
            }
        }

        die(json_encode(['result' => true, 'erro' => $erro]));
    }

    public function uploadDocumentos()
    {
        $codigo = $this->input->post(null, true);

        if (!hasMultipleFile('docs')) {
            responseJSON(500, false, 'Não foram enviados Arquivos.');
        }

        $files = groupMultFiles('docs');

        $config['upload_path'] = 'webroot/uploads/documentos_diario/';
        $config['allowed_types'] = 'xlsx|xls|doc|docx|pdf|csv';

        $erro = 0;
        foreach ($files as $key => $file) {
            $config['file_name'] = bin2hex(random_bytes(10)) . '.' . @end(explode('.', $file['name']));

            $arquivo['DataCriacao'] = date('Y-m-d H:i:s');
            $arquivo['Nome'] =  $file['name'];
            $arquivo['Arquivo'] = base_url($config['upload_path'] . $config['file_name']);
            $arquivo = array_merge($arquivo, $codigo);

            if (!$this->uploadFiles($config, $file)) {
                continue;
                $erro += 1;
            }

            if (!$this->DiarioAmbientalModel->insertDocumentos($arquivo)) {
                unlink($config['upload_path'] . $config['file_name']);
                $erro += 1;
            }
        }

        die(json_encode(['result' => true, 'erro' => $erro]));
    }

    private function uploadFiles($config, $file)
    {
        $_FILES['file'] = $file;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload('file')) {
            return true;
        }

        return false;
    }

    private function diarioBaseUrlRedirect($parametro = null)
    {
        if (is_null($parametro)) {
            redirect('contratos/listar');
        }

        $dados['pagina'] = 'cgmab/DiarioAmbiental/diarioAmbientalLista';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function verificaOcorrencias()
    {
        $filtro = $this->input->post(null, true);
        if (empty($filtro) && !array_key_exists('CodigoDiario', $filtro)) {
            responseJSON();
        }
        $result = $this->DiarioAmbientalModel->executeVerificacaoOcorrencia($filtro['CodigoDiario']);
        die(json_encode(['return' => $result]));
    }

    public function getOcorrenciasDiario()
    {
        $dados = $this->input->post(null, true);
        $result = [];
        if (array_key_exists('OrdenaPorUltimaFaseInserida', $dados) && $dados['OrdenaPorUltimaFaseInserida'] == 0) {
            unset($dados['OrdenaPorUltimaFaseInserida']);
            $result = $this->DiarioAmbientalModel->getOcorrenciasDiario($dados);
            die(json_encode(['ocorrencias' => $result]));
        }

        $dados['OrdenaPorUltimaFaseInserida'] = 1;
        $result = $this->DiarioAmbientalModel->getOcorrenciasDiario($dados);
        die(json_encode(['ocorrencias' => $result]));
    }

    public function getCombos()
    {
        $dados = [
            'risco' => $this->DiarioAmbientalModel->getFrom('tblSupervisaoAmbientalRisco'),
            'status' => $this->DiarioAmbientalModel->getFrom('tblSupervisaoAmbientalStatus'),
            'tipo' => $this->DiarioAmbientalModel->getFrom('tblSupervisaoAmbientalTipoOcorrencia'),
        ];

        die(json_encode($dados));
    }

    public function getFotosEdocumentos()
    {
        $dados = $this->input->post(null, true);
        $dados = [
            'fotos' => $this->DiarioAmbientalModel->getFrom('viewSupervisaoAmbientalFotos', $dados),
            'arquivos' =>   $this->DiarioAmbientalModel->getFrom('viewSupervisaoAmbientalArquivos', $dados),
        ];

        die(json_encode($dados));
    }

    public function getAllFromOcorrencia()
    {
        $filtro = $this->input->post(null, true);

        $busca = [
            'construtoras' => $this->DiarioAmbientalModel->getFrom('viewContrutorasContrato', $filtro),
            'acoes' => $this->DiarioAmbientalModel->getFrom('viewOcorrencaAcoes', $filtro),
            'visitas' =>   $this->DiarioAmbientalModel->getFrom('viewOcorrencaVisitas', $filtro),
            'notificacao' =>   $this->DiarioAmbientalModel->getFrom('viewOcorrencaNotificacao', $filtro),
            'infos' =>   $this->DiarioAmbientalModel->getFrom('viewOcorrencaInfo', $filtro),
            'diario' => $this->DiarioAmbientalModel->getFrom('viewDiarioOcorrencias', $filtro),
            'fiscal' => $this->session->Logado['CodigoUsuario'],
            'programas' => $this->DiarioAmbientalModel->getProgramasContrato($filtro)
        ];

        $filtro['OrdenaPorUltimaFaseInserida'] = 1;
        $busca['fase'] = $this->DiarioAmbientalModel->getOcorrenciasDiario($filtro);

        die(json_encode($busca));
    }

    public function getOcorrenciaAndFases()
    {
        $dados = $this->input->post(null, true);
        $dados = [
            'ocorrencia' => $this->DiarioAmbientalModel->getFrom('tblSupervisaoAmbientalOcorrencias', $dados),
            'fases' =>   $this->DiarioAmbientalModel->getFrom('tblSupervisaoAmbientalOcorrenciasFase', $dados),
            'parecer' =>   $this->DiarioAmbientalModel->getFrom('viewOcorrenciaParecerTecnico', $dados),
        ];
        die(json_encode($dados));
    }

    public function controleOcorrencias()
    {
        if (!$this->session->PerfilLogin == 'Fiscal') {
            redirect('/');
        }

        $dados['pagina'] = 'cgmab/OcorrenciasParecer/index';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }
}
