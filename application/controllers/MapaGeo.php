<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MapaGeo extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/MapaGeoModel', 'model');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('ImportExcelPhpOffice');
    }

    public function index()
    {
        $dados['pagina'] = 'cgmab/MapaGeo/index';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function getMonitoramentoFauna()
    {
        echo json_encode($this->model->getMonitoramentoFauna($this->input->post(null, true)));
    }
    public function getRecursosHidricos()
    {
        echo json_encode($this->model->getRecursosHidricos($this->input->post(null, true)));
    }
    public function buscarMedicoesParametros()
    {
        echo json_encode($this->model->buscarMedicoesParametros($this->input->post(null, true)));
    }
    public function getLicencas()
    {
        echo json_encode($this->model->getLicencas());
    }
    public function getFiltros()
    {
        $dados['contratos'] = $this->model->getContatosFiltro();
        $dados['meses'] = $this->model->getMesFiltro();
        echo json_encode($dados);
    }


}
