<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Equipamentos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/EquipamentosModel', 'Equipamento');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');

    }

    public function listar()
    {
        $dados['pagina'] = 'cgmab/Equipamentos/listar';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function getEquipamentosPassagem(){
        $dados = $this->input->post(null, true);
        echo json_encode($this->Equipamento->getEquipamentosPassagem($dados));
    }

    public function getArmadilhasPassagem(){
        echo json_encode($this->Equipamento->getArmadilhasPassagem( $this->session->userdata('codigoContratoSelecionado')));
    }

    public function getRecursosEquipamentosContrato(){
        $dados = $this->input->post(null, true);
        echo json_encode($this->Equipamento->getRecursosEquipamentosContrato($dados));
    }

}
