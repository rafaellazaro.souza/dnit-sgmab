<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ControleAcesso extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/ControleAcessoModel', 'ControleAcesso');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');

    }

    public function areas()
    {
        $dados['pagina'] = 'cgmab/ControleAcesso/areas';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function perfis(){
        $dados['pagina'] = 'cgmab/ControleAcesso/perfis';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados); 
    }


    /*
    * Inserir Permissões do Perfil nas áreas do sistema
    */
    public function salvarPermissoes(){
        $dados = json_decode($this->input->post('Dados', true), true);
        echo json_encode($this->ControleAcesso->salvarPermissoes($dados));

    }


}
