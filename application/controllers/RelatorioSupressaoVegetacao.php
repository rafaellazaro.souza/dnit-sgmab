<?php

defined('BASEPATH') or exit('No direct script access allowed');

class RelatorioSupressaoVegetacao extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('cgmab/RelatorioSupressaoVegetacaoModel', 'RelatorioSupressaoVegetacao');
		$this->load->helper(['form', 'url', 'helpersFunctions', 'shapefile_helper']);
	}

	public function supressaoVegetacao()
	{
		$dados['pagina'] = 'cgmab/Relatorios/supressaoVegetacao';
		$dados['sidebar'] = 'elements/sidebar';
		//$dados['header'] = 'template/header';
		$this->load->view('templates/aero', $dados);
	}

	public function controleSemanal()
	{
		$dados['pagina'] = 'cgmab/Relatorios/paginas/relatorioSupressaoVegetacao/controleSemanal';
		$dados['sidebar'] = 'elements/sidebar';
		//$dados['header'] = 'template/header';
		$this->load->view('templates/aero', $dados);
	}

	public function postDocumentos()
	{
		date_default_timezone_set("Brazil/East");
		$data = date("Y-m-d H:i:s");

		$i = 0;

		$codigoAtividade = $_REQUEST['codigoAtividade'];
		foreach ($_FILES as $key) {
			$nome = $key['name'];
			$cord[$i]['Nome'] = $nome;
			$cord[$i]['CodigoAtividadeCronogramaFisico'] = $codigoAtividade;
			$NomeArquivo = $_FILES[$i]['name'];
			$ext = @end(explode(".", $NomeArquivo));
			$config['upload_path'] = 'webroot/uploads/relatorio-atropelamento-fauna/documentos-introducao/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|xlsx|docx|pptx';
			$novoNome = random_bytes(10);
			$novoNomeRandom = bin2hex($novoNome) . "." . $ext;
			$caminho = base_url('webroot/uploads/relatorio-supressao-vegetacao/documentos-introducao/') . $novoNomeRandom;
			$cord[$i]['Caminho'] = $caminho;
			$cord[$i]['Ativo'] = 1;
			$cord[$i]['DataCadastro'] = $data;

			$config['file_name'] = $novoNomeRandom;
			$this->upload->initialize($config);
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload($i)) {
				echo $this->upload->display_errors();
			} else {
				$result['result'] = 'Salvo com sucesso!';
			}
			$i++;
		}
		echo $this->RelatorioSupressaoVegetacao->inserirDocumentosPadrao($cord, 'tblFloraSupressaoAnexos');
	}

	public function uploadFotos()
	{
		$codigo = $this->input->post(null, true);

		if (!hasMultipleFile('fotos')) {
			responseJSON(500, false, 'Não foram enviados Arquivos.');
		}

		$files = groupMultFiles('fotos');

		$config['upload_path'] = 'webroot/uploads/supressao_pilhas_fotos/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';

		$erro = 0;
		foreach ($files as $key => $file) {
			$config['file_name'] = bin2hex(random_bytes(10)) . '.' . @end(explode('.', $file['name']));
			$dadosFoto = extrairDadosFotos($file['tmp_name']);
			$dadosFoto['Arquivo'] = base_url($config['upload_path'] . $config['file_name']);
			$dadosFoto = array_merge($dadosFoto, $codigo);


			if (!$this->uploadFiles($config, $file)) {
				continue;
				$erro += 1;
			}

			if (!$this->RelatorioSupressaoVegetacao->insertFoto($dadosFoto)) {
				unlink($config['upload_path'] . $config['file_name']);
				$erro += 1;
			}
		}

		die(json_encode(['result' => true, 'erro' => $erro]));
	}

	
	private function uploadFiles($config, $file)
	{
		$_FILES['file'] = $file;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('file')) {
			return true;
		}

		return false;
	}
	public function uploadDocumentos()
	{
		$codigo = $this->input->post(null, true);

		if (!hasMultipleFile('docs')) {
			responseJSON(500, false, 'Não foram enviados Arquivos.');
		}

		$files = groupMultFiles('docs');

		$config['upload_path'] = 'webroot/uploads/supressao_pilhas_docs/';
		$config['allowed_types'] = 'xlsx|xls|doc|docx|pdf|csv';

		$erro = 0;
		foreach ($files as $key => $file) {
			$config['file_name'] = bin2hex(random_bytes(10)) . '.' . @end(explode('.', $file['name']));

			$arquivo['DataCriacao'] = date('Y-m-d H:i:s');
			$arquivo['Nome'] =  $file['name'];
			$arquivo['Arquivo'] = base_url($config['upload_path'] . $config['file_name']);
			$arquivo = array_merge($arquivo, $codigo);

			if (!$this->uploadFiles($config, $file)) {
				continue;
				$erro += 1;
			}

			if (!$this->RelatorioSupressaoVegetacao->insertDocumentos($arquivo)) {
				unlink($config['upload_path'] . $config['file_name']);
				$erro += 1;
			}
		}

		die(json_encode(['result' => true, 'erro' => $erro]));
	}

	public function postShapefile()
	{
		//		$codigoAtividadeCronogramaFisico = $this->input->post("CodigoAtividadeCronogramaFisico", true);
		$codigoFloraSupressao = $this->input->post("CodigoFloraSupressao", true);
		$rotas = null;
		//		$infoShapefile = $this->Relatorio->getInfoShapeFile($codigoFloraSupressao);
		$cord = [];

		if (pathinfo($_FILES['Shapefile']['name'], PATHINFO_EXTENSION) == 'zip') {
			$rotas = $this->readShapefile('Shapefile');

			$cord = [
				'Shapefile' => json_encode($rotas),
				'CodigoAtividadeCronogramaFisico' => $codigoFloraSupressao
			];
			echo json_encode($rotas);
			// echo $this->Relatorio->saveShapeFile('tblAtropelamentoFaunaRelatorioAnalises', $cord, $infoShapefile);

		}
	}

	private function readShapefile($nomeCampo)
	{
		$rotas = [];
		$path = './webroot/uploads/relatorio-supressao-vegetacao/shapefile/' . uniqid(date('dmYHis'));

		extractZip($_FILES[$nomeCampo]['tmp_name'], $path);

		$this->load->library('readshapefile', [
			'path' => $path,
			'file' => returnFilePathByExtension($path, 'shp'),
		]);

		$rotas = $this->readshapefile->read();

		$this->readshapefile->clean();
		if ($this->readshapefile->fail() && empty($rotas)) {
			http_response_code(500);
			die(json_encode($this->readshapefile->getErrors()));
		}

		return $rotas;
	}

	public function salvarDadosControleSemanal()
	{
		$CodigoSupressaoArea = $this->input->post('CodigoSupressaoArea', true);
		$dados = json_decode($this->input->post('Dados', true), true);

		$array =  (array)  $dados;

		$dados = [];
		$i = 0;
		foreach ($array as $dado) {
			$dados[$i]['CodigoSupressaoArea'] = intval($CodigoSupressaoArea);
			$dados[$i]['Semana'] = $dado['Semana'];
			$dados[$i]['DataInicial'] = $dado['DataInicial'];
			$dados[$i]['DataFinal'] = $dado['DataFinal'];
			$dados[$i]['CoberturaVegetal'] = $dado['CoberturaVegetal'];
			$dados[$i]['Tipologia'] = $dado['Tipologia'];
			$dados[$i]['Fitofisionomia'] = $dado['Fitofisionomia'];
			$dados[$i]['EstagioSucessional'] = $dado['EstagioSucessional'];
			$dados[$i]['AreaEmApp'] = $dado['AreaEmApp'];
			$dados[$i]['AreaForaApp'] = $dado['AreaForaApp'];
			$dados[$i]['AreaTotal'] = $dado['AreaTotal'];
			$dados[$i]['DataCadastro'] = date('Y-m-d  H:i:s');;
			$dados[$i]['Observacoes'] = $dado['Observacoes'];
			$i++;
		}

		$retorno = $this->RelatorioSupressaoVegetacao->salvarDadosPadrao($dados, 'tblFloraControleSemanalSupressaoArea');

		echo json_encode($retorno);
	}

	public function getGpsFotos()
	{
		date_default_timezone_set("Brazil/East");
		$data = date("Y-m-d H:i:s");

		$i = 0;
		foreach ($_FILES as $key) {
			$temp_name = $key['tmp_name'];
			$exif = exif_read_data($temp_name, 0, true);
			if (isset($exif['IFD0'])) {
				$d = explode(":", $exif['IFD0']['DateTime']);
				$dadaFoto = $d[0] . '-' . $d[1] . '-' . $d[2] . ':' . $d[3] . ':' . $d[4];
			} else {
				$dadaFoto = date('Y-m-d H:i:s');
			}
			if (!isset($exif['GPS'])) {
				$nome = $key['name'];
				$lat_ = '';
				$lng_ = '';
			} else {
				$lat_ref = $exif['GPS']['GPSLatitudeRef'];
				$lat = $exif['GPS']['GPSLatitude'];
				list($num, $dec) = explode('/', $lat[0]);
				$lat_s = $num / $dec;
				list($num, $dec) = explode('/', $lat[1]);
				$lat_m = $num / $dec;
				list($num, $dec) = explode('/', $lat[2]);
				$lat_v = $num / $dec;
				$lon_ref = $exif['GPS']['GPSLongitudeRef'];
				$lon = $exif['GPS']['GPSLongitude'];
				list($num, $dec) = explode('/', $lon[0]);
				$lon_s = $num / $dec;
				list($num, $dec) = explode('/', $lon[1]);
				$lon_m = $num / $dec;
				list($num, $dec) = explode('/', $lon[2]);
				$lon_v = $num / $dec;
				$nome = $key['name'];
				$lat_ = '-' . ($lat_s + $lat_m / 60.0 + $lat_v / 3600.0);
				$lng_ = '-' . ($lon_s + $lon_m / 60.0 + $lon_v / 3600.0);
			}

			$cord[$i]['NomeFoto'] = $nome;
			$cord[$i]['CodigoControleSemanal'] = preg_replace("/[^0-9]/", "", explode('_', $nome)[0]);
			$cord[$i]['NomeFoto'] = explode('_', $nome)[1];
			$cord[$i]['Lat'] = (string) $lat_;
			$cord[$i]['Long'] = (string) $lng_;
			$cord[$i]['DataFoto'] = $dadaFoto;
			$cord[$i]['DataCadastro'] = date('Y-m-d H:i:s');

			$NomeArquivo = $_FILES[$i]['name'];
			$ext = @end(explode(".", $NomeArquivo));
			$config['upload_path'] = 'webroot/uploads/relatorio-supressao-vegetacao/fotos-supressao/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
			$novoNome = random_bytes(10);
			$novoNomeRandom = bin2hex($novoNome) . "." . $ext;
			$caminho = base_url('webroot/uploads/relatorio-supressao-vegetacao/fotos-supressao/') . $novoNomeRandom;
			$cord[$i]['CaminhoFoto'] = $caminho;

			$config['file_name'] = $novoNomeRandom;
			$this->upload->initialize($config);
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload($i)) {
				echo $this->upload->display_errors();
			} else {
				$result['result'] = 'Salvo com sucesso!';
			}
			$i++;
		}

		echo $this->RelatorioSupressaoVegetacao->inserirFotosSupressao($cord, 'tblFloraControleSemanalSupressaoAreaFotos');
	}

	public function deletarFotos()
	{
		$codigo = $this->input->post("Codigo", true);
		echo $this->RelatorioSupressaoVegetacao->deletarFotos($codigo);
	}






	public function getPilhas()
	{
		$dados = $this->input->post(null, true);
		$dados = $this->RelatorioSupressaoVegetacao->getPilhas($dados);
		die(json_encode(['pilhas' => $dados]));
	}
}
