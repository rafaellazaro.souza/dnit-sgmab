<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ConfiguracaoSupressaoVegetacao extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('cgmab/ConfiguracaoSupressaoVegetacaoModel', 'ConfiguracaoSupressaoVegetacao');
		$this->load->helper('form');
		$this->load->helper('url');

		$this->load->helper('shapefile_helper');

	}

	public function supressaoVegetacao()
	{
		$dados['pagina'] = 'cgmab/ConfiguracaoSupressaoVegetacao/configuracaoSupressaoVegetacao';
		$dados['sidebar'] = 'elements/sidebar';
		$this->load->view('templates/aero', $dados);
	}

	public function postShapefileSupressao()
	{
		$codigoLicenca = $this->input->post("CodigoLicenca", true);
		$rotas = null;
		$infoShapefile = $this->ConfiguracaoSupressaoVegetacao->getInfoShapeFile($codigoLicenca);
		$cord = [];

		if (pathinfo($_FILES['Coordenada']['name'], PATHINFO_EXTENSION) == 'zip') {

			$rotas = $this->readShapefile('Coordenada');

			$cord = [
				'Coordenada' => json_encode($rotas),
				'CodigoLicenca' => $codigoLicenca];
			echo json_encode($rotas);
			// echo $this->Relatorio->saveShapeFile('tblAtropelamentoFaunaRelatorioAnalises', $cord, $infoShapefile);

		}
	}

	private function readShapefile($nomeCampo)
	{
		$rotas = [];
		$path = './webroot/uploads/configuracao-supresao-vegetacao/shapefile/' . uniqid(date('dmYHis'));

		extractZip($_FILES[$nomeCampo]['tmp_name'], $path);

		$this->load->library('readshapefile', [
			'path' => $path,
			'file' => returnFilePathByExtension($path, 'shp'),
		]);

		$rotas = $this->readshapefile->read();

		$this->readshapefile->clean();
		if ($this->readshapefile->fail() && empty($rotas)) {
			http_response_code(500);
			die(json_encode($this->readshapefile->getErrors()));
		}

		return $rotas;
	}
}
