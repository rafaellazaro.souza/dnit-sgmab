<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ProgramasPlanosAmbientais extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        // $this->load->model('dpp/Dpp_model', 'dpp');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function index()
    {
        $dados['pagina'] = 'cgmab/programas_planos_ambientais/index';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function execucao()
    {
        $dados['pagina'] = 'cgmab/programas_planos_ambientais/execucao';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function configuracoes()
    {
        $dados['pagina'] = 'cgmab/programas_planos_ambientais/configuracoes';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function monitoramentoFauna()
    {
        $dados['pagina'] = 'cgmab/programas_planos_ambientais/monitoramentoFauna';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }
}
