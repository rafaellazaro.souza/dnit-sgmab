<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Subprogramas extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        // $this->load->model('dpp/Dpp_model', 'dpp');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    // public function index() {
    //     $dados['pagina'] = 'cgmab/configuracao/index';
    //     $dados['sidebar'] = 'elements/sidebar';
    //     $dados['header'] = 'template_adminlte3/header';
    //     $this->load->view('templates/aero', $dados);
    // }

    public function relatorioTecnicoPadrao(){
        $dados['pagina'] = 'cgmab/subprogramas/relatorios/relatorioTecnicoPadrao';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function relatorioMonitoramentoFauna()
    {
            $dados['pagina'] = 'cgmab/subprogramas/relatorios/relatorioMonitoramentoFauna';
            $dados['sidebar'] = 'elements/sidebar';
            $dados['header'] = 'template_adminlte3/header';
            $this->load->view('templates/aero', $dados); 
    }
    public function relatorioMonitoramentoAtropelamentoFauna()
    {
            $dados['pagina'] = 'cgmab/subprogramas/relatorios/relatorioMonitoramentoAtropelamentosFauna';
            $dados['sidebar'] = 'elements/sidebar';
            $dados['header'] = 'template_adminlte3/header';
            $this->load->view('templates/aero', $dados); 
    }
}
