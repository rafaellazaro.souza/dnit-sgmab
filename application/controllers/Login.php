<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{



    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/LoginModel', 'Login');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');
    }

    public function index()
    {
        $dados['page_title'] = 'Login';
        $dados['pagina'] = 'cgmab/Login/index';
        $this->load->view('templates/aero-login', $dados);
    }

    public function login()
    {

        $dados = $this->input->post(NULL, true);
        echo $this->Login->login($dados);
    }


    public function logout()
    {
        $this->session->unset_userdata('PerfilLogin');
        $this->session->unset_userdata('Logado');
        redirect('Login/index');
    }

    public function codificar()
    {

        $dados = $this->input->post('Senha', true);
        echo $this->Login->codificar($dados);
    }

    public function dadosUsuario()
    {
        echo json_encode($_SESSION['Logado'], true);
    }

    public function gerarCodigoContratada()
    {
        $_SESSION['Logado']['CodigoContratada'] = $this->input->post('codigo', true);
    }




    public function getPerfil()
    {
        echo json_encode([
            'perfil' => $_SESSION['PerfilLogin']
        ], true);
    }

    public function verificar()
    {
        if (!isset($this->session->Logado['CodigoUsuario'])) {
            echo json_encode([
                'status' => false
            ], true);
        }else{
            true;
        }
        		
    }
}
