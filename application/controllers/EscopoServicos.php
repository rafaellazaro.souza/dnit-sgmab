<?php

defined('BASEPATH') or exit('No direct script access allowed');

class EscopoServicos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/EscopoServicoModel', 'EscopoServico');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    /**
     * Listar Condicionantes de um escopo
     */
    public function getCondiconantesEscopo()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->EscopoServico->getCondiconantesEscopo($dados));
    }


    /*
    * Listar escopos de serviço de um contrato
    */
    public function getEscopoServicoContrato()
    {
        $dados = $this->input->get(null, true);
        $this->session->set_userdata('ContratoSelecionando', $dados['CodigoContrato']);
        echo json_encode($this->EscopoServico->getEscopoServicoContrato($dados));
    }
    /* buscar licenças no banco sim dnit
    */
    public function salvarEscopoServicoContrato()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->EscopoServico->salvarEscopoServicoContrato($dados));
    }
    /*
    * Associar condicionantes a um serviço
    */
    public function salvarCondicionantes()
    {
        $dados = $this->input->get(null, true);
        echo json_encode($this->EscopoServico->salvarCondicionantes($dados));
    }

    /*
    * Associar Pessoa a condicionante quando for Execução de serviços
    */
    public function salvarPessoaExecucao()
    {
        $dados = json_decode($this->input->post('Dados', true), true);
        echo json_encode($this->EscopoServico->salvarPessoaExecucao($dados));
    }

    /*
    * Associar Pessoa a condicionante quando for Execução de serviços
    */
    public function getRecurosHumanosServico()
    {

        echo json_encode($this->EscopoServico->getRecurosHumanosServico($this->input->post(null, true)));
    }
    /*
    * buscar todo mundo que foi associado no serviço execução de programas 
    */
    public function getPessoasEmExecucaoProgramas()
    {
        $dados = $this->input->get('CodigoEscopoPrograma', true);
        echo json_encode($this->EscopoServico->getPessoasEmExecucaoProgramas($dados));
    }

    public function getRecursosServicos()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->EscopoServico->getRecursosServicos($dados));
    }
    public function verifica_infraestrutura_veiculo()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->EscopoServico->verifica_infraestrutura_veiculo($dados));
    }
}
