<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GestaoAmbiental extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        // $this->load->model('dpp/Dpp_model', 'dpp');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function index() {
        $dados['pagina'] = 'cgmab/gestao_ambiental/index';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    

}
