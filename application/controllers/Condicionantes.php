<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Condicionantes extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/CondicionanteModel', 'Condicionantes');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');

        $this->load->library('ImportExcelPhpOffice');
    }

    public function index()
    {
        $dados['pagina'] = 'cgmab/Condicionantes/index';
        $dados['sidebar'] = 'elements/sidebar';
        $dados['header'] = 'template_adminlte3/header';
        $this->load->view('templates/aero', $dados);
    }

    public function getCondicionantes()
    {
        $retorno = $this->Condicionantes->getCondicionantes($this->input->post(NULL, true));
        echo json_encode($retorno);
    }

    /*
    * Listar condiconantes de um empreendimento/ licença
    */

    public function listarCondicionantesEmpreendimento()
    {

        $retorno = $this->Condicionantes->listarCondicionantesEmpreendimento($this->input->post(NULL, true));
        echo json_encode($retorno);
    }
    //armazena dado dda condicionante
    public function armazenaDadosCondicionante()
    {

        $this->session->unset_userdata('dadosCondiconante');
        $this->session->set_userdata('dadosCondiconante', $_GET);
        print_r($this->session->userdata('dadosCondiconante'));
    }

    //upload arquivo xslx
    public function uploadCsv()
    {
        if (!empty($_FILES['file']['name'])) {
            $arquivo = $_FILES['file'];
            //$dadosDaPlanilha = $this->importexcelphpoffice->lerArquivo($arquivo);
            $dadosDaPlanilha = $this->importexcelphpoffice->lerArquivo($arquivo);
            //armazena lista em sessao
            $this->session->unset_userdata('listaCondicionantes');
            $this->session->set_userdata('listaCondicionantes', $dadosDaPlanilha);


            die(json_encode($dadosDaPlanilha));
        } else {
            die(false);
        }
    }

    //Gravar novas condicionantes no banco de dados
    public function gravarCondicionantes()
    {
        $dados = $this->session->userdata('listaCondicionantes');
        $this->Condicionantes->gravarCondicionantes($dados);
    }
}
