<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('cgmab/homeModel', 'Home');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function index()
    {
        // echo $this->session->Logado['NomeUsuario'];
        // echo "<br>";
        // print_r($this->session->userdata('Logado'));
        // exit;
        $dados['pagina'] = 'cgmab/licencasAmbientais/index';
        $dados['sidebar'] = 'elements/sidebar';
        //$dados['header'] = 'template/header';
        $this->load->view('templates/aero', $dados);
    }

    public function aprovacoesPendentes()
    {
        $dados['pagina'] = 'cgmab/home/aprovacaoPendente';
        $dados['sidebar'] = 'elements/sidebar';
        $this->load->view('templates/aero', $dados);
    }

    /*
    * Login provisório
    */
    public function login()
    {
        $this->session->set_userdata('PerfilLogin', $this->input->get('Perfil', false));
        // $_SESSION['PerfilLogin'] = $this->input->get('Perfil', false);
        if ($this->session->userdata('PerfilLogin') === 'DNIT') {
            redirect('LicenciamentoAmbiental');
        } else if ($this->session->userdata('PerfilLogin') === 'Fiscal') {
            redirect('home/aprovacoesPendentes');
        } else {
            redirect('contratos/listar');
        }
    }

    /*
    Consulta para montar menus
    */

    public function menu()
    {
        $tipo = $this->input->post('Tipo', null);
        if ($tipo == 'm') {
            $dados = $this->Home->menu();
        } else {
            $dados = $this->Home->submenu();
        }
        echo json_encode($dados);
    }
}
