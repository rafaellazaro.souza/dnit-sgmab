<?php

class ConfiguracaoProgramasModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function inserirFotos($dados)
    {
        $this->db2->insert_batch('tblConfiguracaoRecursosHidricosFotos', $dados);
        $this->db2->trans_complete();
        if ($this->db2->trans_status() === false) {
            $this->db2->trans_rollback();
            return false;
        } else {
            $this->db2->trans_commit();
            return true;
        }
    }

    public function inserirFotosPadrao($dados, $tabela)
    {
        $this->db2->insert_batch($tabela, $dados);
        $this->db2->trans_complete();
        if ($this->db2->trans_status() === false) {
            $this->db2->trans_rollback();
            return false;
        } else {
            $this->db2->trans_commit();
            return true;
        }
    }

    public function getFotos()
    {
        $this->db2->select("*, FORMAT(DataFoto , 'dd/MM/yyyy HH:mm:ss') DataFotoF");
        $this->db2->from('tblConfiguracaoRecursosHidricosFotos');
        $query = $this->db2->get();
        return $query->result_array();
    }

    public function deletarFotos($codigo)
    {
        $this->db2->where('CodigoConfiguracaoRecursosHidricosFotos', $codigo);
        $this->db2->delete('tblConfiguracaoRecursosHidricosFotos');
        $this->db2->trans_complete();
        if ($this->db2->trans_status() === false) {
            $this->db2->trans_rollback();
            return false;
        } else {
            $this->db2->trans_commit();
            return true;
        }
    }
    public function deletarFotosPassagem($dados)
    {
        if(isset($dados['fotosAnimais'])){
            $where = 'CodigoFotoAnimalCapturado';
            $tabela = 'tblPassagemFaunaAnimaisCapturadosFoto';
        }else{
            $where = 'CodigoConfiguracaoPassagemFaunaArmadilha';
            $tabela = 'tblConfiguracaoPassagemFaunaArmadilhas';
        }
        $this->db2->where($where, $dados['Codigo']);
        $this->db2->delete($tabela);
        $this->db2->trans_complete();
        if ($this->db2->trans_status() === false) {
            $this->db2->trans_rollback();
            return false;
        } else {
            $caminho = $dados['Caminho'];
             unlink($caminho);//remove do diretori
            $this->db2->trans_commit();
            return true;
        }
    }

    public function deletarPadrao($codigoConfiguracao, $codigo, $tabela)
    {
        $this->db2->where($codigoConfiguracao, $codigo);
        $this->db2->delete($tabela);
        $this->db2->trans_complete();
        if ($this->db2->trans_status() === false) {
            $this->db2->trans_rollback();
            return false;
        } else {
            $this->db2->trans_commit();
            return true;
        }
    }


    public function getListas($dados)
    {
        $query = $this->db2->query("select l.CodigoLista, l.NomeLista,
        count(DISTINCT(lp.CodigoConfiguracaoRecursosHidricosPontoColeta)) as TotalPontos,
        count(DISTINCT(lhp.CodigoParametrosMedicaoAgua)) as TotalParametros
        from tblRecursosHidricosListas l
        left join tblRecursosHidricosListasPontos lp on lp.CodigoLista = l.CodigoLista
        left join tblRecursosHidricosParametros lhp on lhp.CodigoLista = l.CodigoLista
        where l.CodigoEscopoPrograma = " . $dados . "
        group by l.NomeLista, l.CodigoLista
        order by l.CodigoLista ASC", false);
        return $query->result_array();
        
    }

    public function getListasEquipamentoFauna($codigo){
        $query = $this->db2->query(
            "SELECT E.NomeEquipamento, ER.CodigoRecurso 
            FROM tblEscopoRecursos ER
            LEFT JOIN tblEquipamentos E 
            ON ER.CodigoEquipamento = E.CodigoEquipamento
            WHERE ER.CodigoEscopoPrograma = " . $codigo . " 
            AND ER.CodigoEquipamento IS NOT NULL"
        );
        return $query->result_array();

    }

    public function salvarParametrosNaLista($dados)
    {
        //print_r($dados);exit;
        $this->db2->insert_batch('tblRecursosHidricosParametros', $dados);
        if ($this->db2->trans_status() === true) {
            return [
                'status' => true,
                'result' => 'Dados Salvos Com Sucesso'
            ];
        } else {
            return [
                'status' => false,
                'result' => 'Erro ao Inserir dados'
            ];
        }
    }
    public function salvarPontosNaLista($dados)
    {
        //print_r($dados);exit;
        $this->db2->insert_batch('tblRecursosHidricosListasPontos', $dados);
        if ($this->db2->trans_status() === true) {
            return [
                'status' => true,
                'result' => 'Dados Salvos Com Sucesso'
            ];
        } else {
            return [
                'status' => false,
                'result' => 'Erro ao Inserir dados'
            ];
        }
    }

    public function getArmadilhasPassagem($codigo){
        $query = $this->db2->query("    select * from tblConfiguracaoPassagemFaunaArmadilhas A
        left join tblEscopoRecursos E on E.CodigoRecurso = A.CodigoRecurso
        left join tblEquipamentos EQ on EQ.CodigoEquipamento = E.CodigoEquipamento
        where A.CodigoAreaApoio = ".$codigo."");
        return $query->result_array();
    }


}
