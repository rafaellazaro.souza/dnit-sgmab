<?php

class ControleAcessoModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function salvarPermissoes($dados)
    {
        $this->db2->insert_batch('tblControleAcessoPerfis', $dados);
        if ($this->db2->trans_status() === true) {
            return [
                'status' => true,
                'result' => 'Dados Salvos Com Sucesso'
            ];
        } else {
            return [
                'status' => false,
                'result' => 'Erro ao Inserir dados'
            ];
        }
    }
}
