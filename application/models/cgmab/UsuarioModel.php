<?php

class UsuarioModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function insert($dados)
    {
        try {
            if (isset($dados['CodigoContratada'])) {
                $this->db2->set('CodigoContratada', $dados['CodigoContratada']);
            }
            if (isset($dados['SkFiscal'])) {
                $this->db2->set('SkFiscal', $dados['SkFiscal']);
            }
            $this->db2->set('CodigoPerfil', $dados['CodigoPerfil']);
            $this->db2->set('NomeUsuario', $dados['NomeUsuario']);
            $this->db2->set('Telefone', $dados['Telefone']);
            $this->db2->set('CPF', $dados['CPF']);
            $this->db2->set('RG', $dados['RG']);
            isset($dados['Email']) ? $this->db2->set('Email', $dados['Email']) : '';
            $this->db2->set('Senha', $dados['Senha']);

            if (isset($dados['Acao'])) {
                $this->db2->where('CodigoUsuario', $dados['CodigoUsuario']);
                $this->db2->update('tblUsuarios');
            } else {
                $this->db2->insert('tblUsuarios');
            }

            $this->db2->trans_complete();

            if ($this->db2->trans_status() === FALSE) {
                $this->db2->trans_rollback();
                throw new Exception("Erro ao tentar inserir os dados.\n" . $this->db2->get_compiled_insert());
            } else {
                $this->db2->trans_commit();
                return [
                    'status' => true,
                    'result' => $this->db2->insert_id()

                ];
            }
        } catch (Exception $e) {
            return [
                'status' => false,
                'result' => $e->getMessage()
            ];
        }
    }

    public function get($where)
    {
        $query = $this->db2->select('CodigoUsuario')
            ->from('tblUsuarios')
            ->where($where)
            ->get();

        return $query->row_array();
    }
}
