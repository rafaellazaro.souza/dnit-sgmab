<?php

class LoginModel extends CI_Model
{

    private $db2;
    private $CHAVE;
    private $DNIT;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
        $this->CHAVE = md5('key');
        $this->DNIT = md5('dnit');
    }


    public function login($post)
    {
        //consulta dados do usuario
        $tipoPerfil = '';
        $this->db2->select('*')->from('tblUsuarios U');
        $this->db2->join('tblPerfis P', 'P.CodigoPerfil = U.CodigoPerfil');
        if (explode("@", $post['Email'])[1] != 'dnit.gov.br') {
            $this->db2->join("tblContratadas C", "C.CodigoContratada = U.CodigoContratada");
            $tipoPerfil = 'contratada';
        } else {
            $tipoPerfil = 'dnit';
        }

        $this->db2->where(['U.Email' => $post['Email'], 'U.Status' => 1]);
        $query = $this->db2->get();
        $response = $query->result_array();

        if (empty($response)) {
            return json_encode([
                'status' => false,
                'result' => 'Usuário não encontrado.'
            ]);
        } else {
            $senhaBanco = $this->decodificar($response[0]['Senha']);
            $senhaPost = md5($post['Senha']);

            if ($senhaBanco === $senhaPost) {
                unset($response[0]['Senha']);
                $arrNome = explode(' ', trim($response[0]['NomeUsuario']));
                $response['PrimeiroNome'] = $arrNome[0];
                $response[0]['Status'] = true;
                //define na tipo de usuario logado
                if (!empty($response[0]['CodigoContratada'])) {
                    $this->session->set_userdata('PerfilLogin', 'Contratada');
                } else {
                    if ($response[0]['CodigoPerfil'] == 3) {
                        $this->session->set_userdata('PerfilLogin', 'Fiscal');
                    } else {
                        $this->session->set_userdata('PerfilLogin', 'DNIT');
                    }
                }

                //criaSessao Com os dados do usuário
                $this->session->set_userdata('Logado', $response[0]);
                return json_encode([
                    'status' => true,
                    'result' => $response,
                    'tipoPerfil' => $tipoPerfil
                ]);
            } else {
                return json_encode([
                    'status' => false,
                    'result' => 'Senha inválida.'
                ]);
            }
        }
    }


    public function validar($dados)
    {
        print_r($dados);
        exit;
    }

    public function codificar($valor)
    {
        $numero_de_bytes = 15;
        $restultado_bytes1 = random_bytes($numero_de_bytes);
        $numero_de_bytes1 = 5;
        $restultado_bytes2 = random_bytes($numero_de_bytes1);
        return bin2hex($restultado_bytes1) . $this->CHAVE . md5($valor) . $this->DNIT . bin2hex($restultado_bytes2);
    }

    public function decodificar($valor)
    {
        $decodifica1 = explode($this->CHAVE, $valor)[1];
        return explode($this->DNIT, $decodifica1)[0];
    }
}
