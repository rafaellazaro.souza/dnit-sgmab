<?php

class RecursosHumanosModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function getRecursosHumanosContrato($dados)
    {
        $query = $this->db2->query("
        SELECT * FROM tblEscopoRecursos ES
        left join tblEscopoServicos S on s.CodigoEscopo = ES.CodigoEscopo
        left join tblRecursosHumanos RH on RH.CodigoRecursosHumanos = ES.CodigoRecursosHumanos
        where S.CodigoContrato = " . $dados['contrato'] . " and ES.CodigoEscopoPrograma = " . $dados['escopo'] . " and ES.CodigoRecursosHumanos is not null
        ");
        return $query->result_array();
    }

    public function insertAnexos($dados)
    { {
            //print_r($dados);exit;
            $this->db2->insert_batch('tblAnexos', $dados);
            if ($this->db2->trans_status() === true) {
                return [
                    'status' => true,
                    'result' => 'Dados Salvos Com Sucesso'
                ];
            } else {
                return [
                    'status' => false,
                    'result' => 'Erro ao Inserir dados'
                ];
            }
        }
    }


    public function getRelatorioRecursos()
    {
        $query = $this->db2->query("
        SELECT 
H.NomePessoa,
H.CPF, C.Numero,
C.DataTerminoPrevista,
S.NomeServico,
CD.Titulo,
T.NomeTema,
SB.NomeSubtipo 
FROM tblEscopoRecursos E
LEFT JOIN tblRecursosHumanos H ON E.CodigoRecursosHumanos = H.CodigoRecursosHumanos
LEFT JOIN tblEscopoServicosProgramas ESP ON E.CodigoEscopoPrograma = ESP.CodigoEscopoPrograma
LEFT JOIN tblCondicionantes CD ON ESP.CodigoCondicionante = CD.CodigoCondicionante
LEFT JOIN tblSubTipoTemas SB ON CD.CodigoSubtipo = SB.CodigoSubtipo
LEFT JOIN tblTemas T ON SB.CodigoTema = T.CodigoTema
LEFT JOIN tblEscopoServicos ES ON ESP.CodigoEscopo = ES.CodigoEscopo
LEFT JOIN tblServicos S ON ES.CodigoServico = S.CodigoServico
LEFT JOIN tblContratos C ON ES.CodigoContrato = C.CodigoContrato
WHERE H.NomePessoa IS NOT NULL
ORDER BY H.NomePessoa 
        ");
        return $query->result_array();
    }
}
