<?php

class ConfiguracaoSupressaoVegetacaoModel extends CI_Model
{
	private $db2;

	public function __construct()
	{
		parent::__construct();
		$this->db2 = $this->load->database('CGMAB', true);
	}

	public function getInfoShapeFile($codigo)
	{
		$query = $this->db2->query(
			"SELECT *
            FROM tblLicencasAmbientais
            WHERE CodigoLicenca = " . $codigo . ""
		);
		return $query->result_array();
	}
}
