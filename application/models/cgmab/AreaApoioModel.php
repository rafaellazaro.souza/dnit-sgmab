<?php

class AreaApoioModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function salvar($contrato)
    {
        $dadosExcel = $this->session->userdata('listaDados');
        foreach ($dadosExcel as $i) {
            $this->db2->set('CodigoContrato', $contrato);
            $this->db2->set('Estrutura', $i['Estrutura']);
            $this->db2->set('TipoEstrutura', $i['TipoEstrutura']);
            $this->db2->set('Nome', $i['Nome']);
            $this->db2->set('Altura', $i['Altura']);
            $this->db2->set('Largura', $i['Largura']);
            $this->db2->set('Diametro', $i['AltuDiametroa']);
            $this->db2->set('UF', $i['UF']);
            $this->db2->set('BR', $i['BR']);
            $this->db2->set('TipoTrecho', $i['TipoTrecho']);
            $this->db2->set('KmInicial', $i['KmInicial']);
            $this->db2->set('KmFinal', $i['KmFinal']);
            $this->db2->set('NumeroEstaca', $i['NumeroEstaca']);
            $this->db2->set('Longitude', $i['Longitude']);
            $this->db2->set('Latitude', $i['Latitude']);
            $this->db2->set('Observacao', $i['Observacao']);
            $this->db2->insert('tblAreasApoio');
        }

        return [
            'status' => true,
            'result' => 'Dados Salvos Com Sucesso'
        ];
    }

    public function getDadosPassagemGrupoFaunistico($dados)
    {
        $query = $this->db2->query("select 
        AC.CodigoGrupoFaunistico,
        AP.CodigoAreaApoio,
        AP.Estrutura,
        AP.TipoEstrutura,
        AP.Diametro,
        AP.Largura,
        AP.Altura,
        AP.Nome,
        AP.UF,
        AP.BR,
        
        (select count(*) from tblPassagemFaunaAnimaisCapturados AC 
        left join  tblGruposFaunisticos GF on GF.CodigoGrupoFaunistico = AC.CodigoGrupoFaunistico
        left join tblConfiguracaoPassagemFaunaArmadilhas AR on AR.CodigoConfiguracaoPassagemFaunaArmadilha = AC.CodigoConfiguracaoPassagemFaunaArmadilha
        where AC.CodigoAtividadeCronogramaFisico = " . $dados['CodigoAtividadeCronogramaFisico'] . ") as TotalRegistros,
        
        (select count(*) from tblPassagemFaunaAnimaisCapturados ACP 
        left join  tblGruposFaunisticos GF on GF.CodigoGrupoFaunistico = ACP.CodigoGrupoFaunistico
        left join tblConfiguracaoPassagemFaunaArmadilhas AR on AR.CodigoConfiguracaoPassagemFaunaArmadilha = ACP.CodigoConfiguracaoPassagemFaunaArmadilha
        where AR.CodigoAreaApoio = AP.CodigoAreaApoio  and GF.CodigoGrupoFaunistico = " . $dados['CodigoGrupoFaunistico'] . ") as TotalRegistrosGrupoFaunistico
        
        from tblPassagemFaunaAnimaisCapturados AC 
        left join  tblGruposFaunisticos GF on GF.CodigoGrupoFaunistico = AC.CodigoGrupoFaunistico
        left join tblConfiguracaoPassagemFaunaArmadilhas AR on AR.CodigoConfiguracaoPassagemFaunaArmadilha = AC.CodigoConfiguracaoPassagemFaunaArmadilha
        left join tblAreasApoio AP on AP.CodigoAreaApoio = AR.CodigoAreaApoio
        where AC.CodigoAtividadeCronogramaFisico = " . $dados['CodigoAtividadeCronogramaFisico'] . " and AC.CodigoGrupoFaunistico = " . $dados['CodigoGrupoFaunistico'] . "
        group by AP.CodigoAreaApoio, AC.CodigoGrupoFaunistico, AP.Estrutura, AP.TipoEstrutura, AP.Diametro, AP.Largura, AP.Altura, AP.Nome, AP.UF, AP.BR
        order by TotalRegistrosGrupoFaunistico
        ");
        return $query->result_array();
    }


    public function dadosRiquezaAbundancia($dados)//,  $dados['codigoAtividade']; $dados['codigoGrupoFaunistico'] 
    {
        $query = $this->db2->query("
        select 
A.CodigoAnimalCapturado,
A.CodigoGrupoFaunistico,
A.Especie,

--total animais caputrados anteriormente
(select count(*) from tblPassagemFaunaAnimaisCapturados A 
left join tblAtividadesCronogramasFisicos AT on AT.CodigoAtividadeCronogramaFisico = A.CodigoAtividadeCronogramaFisico
left join tblConogramaFisoProgramas CR on CR.CodigoCronogramaFisicoProgramas =  AT.CodigoCronogramaFisicoProgramas
where AT.MesAtividade = 1) as TotalRegistrosAnterior,
-- total riqueza atual
(SELECT COUNT (DISTINCT AN.Especie) FROM tblPassagemFaunaAnimaisCapturados AN
where A.CodigoAtividadeCronogramaFisico = ".$dados['codigoAtividade']." and AN.CodigoGrupoFaunistico = ".$dados['codigoGrupoFaunistico']." 
) as TotalRiquesaAtual,

CASE
	WHEN CR.Periodicidade = 'Mensal' 
		THEN (
			SELECT COUNT (DISTINCT ANC.Especie) FROM tblPassagemFaunaAnimaisCapturados ANC
			left join tblAtividadesCronogramasFisicos ATV on ATV.CodigoAtividadeCronogramaFisico = ANC.CodigoAtividadeCronogramaFisico
			left join tblConogramaFisoProgramas CRO on CRO.CodigoCronogramaFisicoProgramas =  ATV.CodigoCronogramaFisicoProgramas
			where ANC.CodigoGrupoFaunistico = ".$dados['codigoGrupoFaunistico']." and ATV.DataMesAtividade = dateadd(month,-1, AT.DataMesAtividade) 
			)
	WHEN CR.Periodicidade = 'Bimestral' 
		THEN (
			SELECT COUNT (DISTINCT ANC.Especie) FROM tblPassagemFaunaAnimaisCapturados ANC
			left join tblAtividadesCronogramasFisicos ATV on ATV.CodigoAtividadeCronogramaFisico = ANC.CodigoAtividadeCronogramaFisico
			left join tblConogramaFisoProgramas CRO on CRO.CodigoCronogramaFisicoProgramas =  ATV.CodigoCronogramaFisicoProgramas
			where ANC.CodigoGrupoFaunistico = ".$dados['codigoGrupoFaunistico']." and ATV.DataMesAtividade = dateadd(month,-2, AT.DataMesAtividade) 
			)
	WHEN CR.Periodicidade = 'Trimestral' 
		THEN (
			SELECT COUNT (DISTINCT ANC.Especie) FROM tblPassagemFaunaAnimaisCapturados ANC
			left join tblAtividadesCronogramasFisicos ATV on ATV.CodigoAtividadeCronogramaFisico = ANC.CodigoAtividadeCronogramaFisico
			left join tblConogramaFisoProgramas CRO on CRO.CodigoCronogramaFisicoProgramas =  ATV.CodigoCronogramaFisicoProgramas
			where ANC.CodigoGrupoFaunistico = ".$dados['codigoGrupoFaunistico']." and ATV.DataMesAtividade = dateadd(month,3, AT.DataMesAtividade) 
			)
	WHEN CR.Periodicidade = 'Quadrimestral' 
		THEN (
			SELECT COUNT (DISTINCT ANC.Especie) FROM tblPassagemFaunaAnimaisCapturados ANC
			left join tblAtividadesCronogramasFisicos ATV on ATV.CodigoAtividadeCronogramaFisico = ANC.CodigoAtividadeCronogramaFisico
			left join tblConogramaFisoProgramas CRO on CRO.CodigoCronogramaFisicoProgramas =  ATV.CodigoCronogramaFisicoProgramas
			where ANC.CodigoGrupoFaunistico = ".$dados['codigoGrupoFaunistico']." and ATV.DataMesAtividade = dateadd(month,-4, AT.DataMesAtividade) 
			)
	WHEN CR.Periodicidade = 'Semestral' 
		THEN (
			SELECT COUNT (DISTINCT ANC.Especie) FROM tblPassagemFaunaAnimaisCapturados ANC
			left join tblAtividadesCronogramasFisicos ATV on ATV.CodigoAtividadeCronogramaFisico = ANC.CodigoAtividadeCronogramaFisico
			left join tblConogramaFisoProgramas CRO on CRO.CodigoCronogramaFisicoProgramas =  ATV.CodigoCronogramaFisicoProgramas
			where ANC.CodigoGrupoFaunistico = ".$dados['codigoGrupoFaunistico']." and ATV.DataMesAtividade = dateadd(month,-6, AT.DataMesAtividade) 
			)
	WHEN CR.Periodicidade = 'Anual' 
		THEN (
			SELECT COUNT (DISTINCT ANC.Especie) FROM tblPassagemFaunaAnimaisCapturados ANC
			left join tblAtividadesCronogramasFisicos ATV on ATV.CodigoAtividadeCronogramaFisico = ANC.CodigoAtividadeCronogramaFisico
			left join tblConogramaFisoProgramas CRO on CRO.CodigoCronogramaFisicoProgramas =  ATV.CodigoCronogramaFisicoProgramas
			where ANC.CodigoGrupoFaunistico = ".$dados['codigoGrupoFaunistico']." and ATV.DataMesAtividade = dateadd(month,-12, AT.DataMesAtividade) 
			)

END as TotalRiquezaAnterior

from tblPassagemFaunaAnimaisCapturados A 
left join tblAtividadesCronogramasFisicos AT on AT.CodigoAtividadeCronogramaFisico = A.CodigoAtividadeCronogramaFisico
left join tblConogramaFisoProgramas CR on CR.CodigoCronogramaFisicoProgramas =  AT.CodigoCronogramaFisicoProgramas
where A.CodigoAtividadeCronogramaFisico = ".$dados['codigoAtividade']." and A.CodigoGrupoFaunistico = ".$dados['codigoGrupoFaunistico'].";
        ");
        return $query->result_array();
    }
}
