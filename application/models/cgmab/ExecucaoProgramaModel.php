<?php

class ExecucaoProgramaModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }


    public function salvarPontosColeta($dados)
    {
        $this->db2->insert_batch('tblConfiguracaoRecursosHidricosPontos', $dados);

        if ($this->db2->trans_status() === true) {
            return [
                'status' => true,
                'result' => 'Dados Salvos Com Sucesso'
            ];
        } else {
            return [
                'status' => false,
                'result' => 'Erro ao Inserir dados'
            ];
        }
    }

    public function getAndamentoAtividadesPrevistasRealizadas($ano)
    {
        $query = $this->db2->query("select 
        F.MesAtividade,
        F.AnoAtividade,
        Pendente.pendente,
        Previsto.Previsto
    from tblAtividadesCronogramasFisicos as F
    
    outer apply(
        select 
            COUNT(X.Andamento) as Pendente
        from tblAtividadesCronogramasFisicos as X
        where X.MesAtividade = F.MesAtividade
        and X.Andamento = 'Pendente'
    )as Pendente
    
    outer apply(
        select 
            COUNT(X.Andamento) as Previsto
        from tblAtividadesCronogramasFisicos as X
        where X.MesAtividade = F.MesAtividade
        and X.Andamento = 'Previsto'
    )as Previsto
    
    where F.AnoAtividade = ".$ano."
    group by F.MesAtividade, Pendente.pendente,Previsto.Previsto, F.AnoAtividade", false);
        return $query->result_array();
    }
}
