<?php

class HomeModel extends CI_Model
{
	private $db2;

	public function __construct()
	{
		parent::__construct();
		$this->db2 = $this->load->database('CGMAB', true);
	}

	public function menu()
	{
		$query = $this->db2->query("
		SELECT * From tblAreasSistema A
		left join  tblControleAcessoPerfis C on C.CodigoAreaSistema = A.CodigoAreaSistema
		WHERE A.CodigoAreaSistema IN (SELECT CodigoSubAreaSistema FROM tblAreasSistema where CodigoSubAreaSistema is not null)
		And CodigoPerfil = " . $_SESSION['Logado']['CodigoPerfil'] . "
		order by CodigoSubAreaSistema
		");
		return $query->result_array();
	}

	public function submenu()
	{
		$query  = $this->db2->query("
		SELECT * FROM tblAreasSistema A 
   left join  tblControleAcessoPerfis C on C.CodigoAreaSistema = A.CodigoAreaSistema
  Where CodigoSubAreaSistema IN (SELECT CodigoSubAreaSistema From tblAreasSistema Where CodigoSubAreaSistema is not null)
   And CodigoPerfil = " . $_SESSION['Logado']['CodigoPerfil'] . "
   order by A.NomeArea
		");
		return $query->result_array();
	}
}
