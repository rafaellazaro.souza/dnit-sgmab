<?php

class EquipamentosModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function getArmadilhasPassagem($contrato)
    {
        $query = $this->db2->query("select * from tblConfiguracaoPassagemFaunaArmadilhas A
        left join tblAreasApoio  AR on AR.CodigoAreaApoio = A.CodigoAreaApoio
left join tblEscopoRecursos ER on ER.CodigoRecurso = A.CodigoRecurso
left join tblEquipamentos EQ on EQ.CodigoEquipamento = ER.CodigoEquipamento
        where AR.CodigoContrato = " . $contrato . "  and A.CodigoRecurso is not null
        ");
        return $query->result_array();
    }



    public function getRecursosEquipamentosContrato($dados)
    {

        $query = $this->db2->query("
        SELECT * FROM tblEscopoRecursos ES
        left join tblEscopoServicos S on s.CodigoEscopo = ES.CodigoEscopo
        left join tblEquipamentos E on E.CodigoEquipamento = ES.CodigoEquipamento
            where S.CodigoContrato = " . $dados['contrato'] . "and ES.CodigoEscopoPrograma = ".$dados['escopo']." and ES.CodigoEquipamento is not null
            ");
        return $query->result_array();
    }
}
