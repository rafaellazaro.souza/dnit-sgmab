<?php

class RelatorioSupervisaoModel extends CI_Model
{
    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('default', true);
    }

    public function getFrom($table, $where = [])
    {
        $query = $this->db2->select(['*'])
            ->from($table)
            ->where(array_filter($where))
            ->get();

        return $query->result_array();
    }

    public function getContratos($where)
    {
        $query = $this->db2->select(['cont.CodigoContrato', 'cont.Numero', 'cont.DataInicioContrato', 'cont.DataTerminoPrevista'])
            ->from('tblContratos cont')
            ->join('tblRelatorioSupervisao rel', 'rel.CodigoContrato=cont.CodigoContrato', 'left')
            ->where(array_filter($where))
            ->get();

        return $query->result_array();
    }

    public function getDiarios($filtro)
    {
        $query = $this->db2->select([
            'dir.*',
            'fmtDataCadastro = CONVERT(VARCHAR(10),"dir"."DataCadastro",103)',
            'Ocorrencias =  (SELECT count(*) FROM "tblSupervisaoAmbientalOcorrencias" "ocrr" WHERE "ocrr"."CodigoDiario" = "dir"."CodigoDiario")'
        ])
            ->from('tblRelatorioSupervisao rel')
            ->join('tblEscopoServicos escopo', 'escopo.CodigoContrato=rel.CodigoContrato')
            ->join('tblSupervisaoAmbienalDiarios dir', 'escopo.CodigoEscopo=dir.CodigoEscopo')
            ->where(array_filter($filtro))
            ->where('MONTH(dir.DataCadastro) = rel.Mes')
            ->where('YEAR(dir.DataCadastro) = rel.Ano')
            ->get();

        return $query->result_array();
    }

    public function getOcorrencias($where)
    {
        $query = $this->db2->select(['vw.*'])
            ->from('tblRelatorioSupervisao rel')
            ->join('viewControleDeOcorrencia vw', 'rel.CodigoContrato=vw.CodigoContrato', 'left')
            ->where(array_filter($where))
            ->where('MONTH(vw.DataRegistro) = rel.Mes')
            ->where('YEAR(vw.DataRegistro) = rel.Ano')
            ->get();

        return $query->result_array();
    }

    public function getParecerTecnico($where)
    {
        $query = $this->db2->select(['pa.*', 'user.NomeUsuario', 'sit.Situacao'])
            ->from('tblRelatorioSupervisaoParecerTecnico pa')
            ->join('tblUsuarios user', 'user.CodigoUsuario = pa.CodigoUsuario', 'left')
            ->join('tblSupervisaoAmbientalSituaccaoParecer sit', 'sit.CodigoSituacaoParecer = pa.CodigoSituacaoParecer', 'left')
            ->where(array_filter($where))
            ->get();

        return $query->result_array();
    }

    public function insertDocumentos($dados)
    {
        return $this->db2->insert('tblRelatorioSupervisaoAnexos', $dados);
    }

    public function getProgramas($filtro)
    {

        $query = $this->db2->select(['sub.*', 'tem.NomeTema', 'prog.CodigoRelatorioProgramas', 'prog.AvaliacaoProgramas'])
            ->from('tblSubTipoTemas sub')
            ->join('tblTemas tem', 'sub.CodigoTema = tem.CodigoTema', 'inner')
            ->join('tblCondicionantes cond', 'cond.CodigoSubtipo = sub.CodigoSubtipo', 'inner')
            ->join('tblEscopoServicosProgramas esp', 'esp.CodigoCondicionante = cond.CodigoCondicionante', 'inner')
            ->join('tblEscopoServicos es', 'es.CodigoEscopo = esp.CodigoEscopo', 'inner')
            ->join('tblRelatorioSupervisao rel', 'rel.CodigoContrato = es.CodigoContrato', 'inner')
            ->join('tblRelatorioSupervisaoProgramas prog', 'prog.CodigoSubtipo = sub.CodigoSubtipo', 'left')
            ->where('tem.NomeTema', 'PAC')
            ->where('rel.CodigoRelatorio', $filtro['CodigoRelatorio'])
            ->get();

        return $query->result_array();
    }

    public function insertProgramas($dados)
    {
        $this->db2->trans_begin();

        $this->db2->insert_batch('tblRelatorioSupervisaoProgramas', $dados);

        if ($this->db2->trans_status() === FALSE) {
            $this->db2->trans_rollback();
            return false;
        }

        $this->db2->trans_commit();
        return true;
    }

    public function updateProgramas($dados)
    {
        $this->db2->trans_begin();

        $this->db2->update_batch('tblRelatorioSupervisaoProgramas', $dados, 'CodigoRelatorioProgramas');

        if ($this->db2->trans_status() === FALSE) {
            $this->db2->trans_rollback();
            return false;
        }

        $this->db2->trans_commit();
        return true;
    }

    public function getRelatorio($filtro = [])
    {
        if ($this->session->PerfilLogin == 'Fiscal') {
            $user = $this->session->Logado['CodigoUsuario'];
            $where = "(cont.FiscalTecnico = {$user} OR cont.FiscalSubstituto = {$user} OR cont.FiscalAdministrativo = {$user})";
            $this->db2->where($where);
        }


        $query = $this->db2->select(['rel.*', 'cont.Numero', 'userCreate.NomeUsuario as usario', 'userEnvio.NomeUsuario as usuarioEnvio'])
            ->from('tblRelatorioSupervisao rel')
            ->join('tblUsuarios userCreate', 'userCreate.CodigoUsuario = rel.CodigoUsuarioEnvio', 'left')
            ->join('tblUsuarios userEnvio', 'userEnvio.CodigoUsuario = rel.CodigoUsuarioEnvio', 'left')
            ->join('tblContratos cont', 'cont.CodigoContrato = rel.CodigoContrato', 'inner')
            ->where(array_filter($filtro))
            ->get();

        return $query->result_array();
    }
}
