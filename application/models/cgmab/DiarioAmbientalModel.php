<?php

class DiarioAmbientalModel extends CI_Model
{
    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('default', true);
    }

    public function getDiarios($filtro)
    {
        $query = $this->db2->select([
            'dir.*',
            'fmtDataCadastro = CONVERT(VARCHAR(10),"dir"."DataCadastro",103)',
            'Ocorrencias =  (SELECT count(*) FROM "tblSupervisaoAmbientalOcorrencias" "ocrr" WHERE "ocrr"."CodigoDiario" = "dir"."CodigoDiario")'
        ])
            ->from('tblSupervisaoAmbienalDiarios dir')
            ->where(array_filter($filtro))
            ->get();
        return $query->result_array();
    }

    public function insertFoto($dados)
    {
        return $this->db2->insert('tblSupervisaoAmbientalFotos', $dados);
    }

    public function insertDocumentos($dados)
    {
        return $this->db2->insert('tblSupervisaoAmbientalDocumentos', $dados);
    }

    public function executeVerificacaoOcorrencia($codigoDiario)
    {
        if ($this->db2->simple_query("EXEC dbo.checkarExpiracaoOcorrencia @CodigoDiario = {$codigoDiario}")) {
            return true;
        }

        return false;
    }

    public function getOcorrenciasDiario($dados = [])
    {
        if ($this->session->PerfilLogin == 'Fiscal') {
            $user = $this->session->Logado['CodigoUsuario'];
            $where = "(FiscalTecnico = {$user} OR FiscalSubstituto = {$user} OR FiscalAdministrativo = {$user})";
            $this->db2->where($where);
        }

        if ($this->session->has_userdata('ContratoSelecionando')) {
            $this->db2->where('CodigoContrato', $this->session->ContratoSelecionando);
        }

        $this->db2->select(['*']);
        $this->db2->from('viewControleDeOcorrencia');
        $this->db2->where(array_filter($dados));
        $query  = $this->db2->get();

        return $query->result_array();
    }

    public function getFrom($table, $where = [])
    {
        $query = $this->db2->select(['*'])
            ->from($table)
            ->where(array_filter($where))
            ->get();

        return $query->result_array();
    }

    public function getProgramasContrato($where = [])
    {
        $query = $this->db2->select(['CodigoSubtipo', 'NomeTema', 'NomeSubtipo', "CONCAT(NomeTema,' - ', NomeSubtipo) AS TemaSubTema"])
            ->from('viewCondicionateTipoTema prog')
            ->join('viewDiarioOcorrencias cont', 'cont.CodigoContrato=prog.CodigoContrato', 'inner')
            ->where(array_filter($where))
            ->get();

        return $query->result_array();
    }
}
