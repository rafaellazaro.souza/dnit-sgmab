<?php

class LicenciamentoAmbientalModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function getLicencas()
    {

        
        $this->db2->select('*');
        $this->db2->from('viewLicencasAmbietais');

        if (isset($_GET['NumeroLicenca'])) {
            $this->db2->like('NumeroLicenca', $_GET['NumeroLicenca']);
        } else {
            $this->db2->where($_GET);
        }
        
        $query = $this->db2->get();
        return $query->result_array();
    }


    public function gravarLicencas($dados)
    {

        foreach ($dados as $inseir) {
            //$this->db2->set('CodigoLicenca',  $inseir['CodigoLicenca']);
            //$this->db2->set('CodigoOrgaoExpeditor',  $inseir['CodigoOrgaoExpeditor']);
            //$this->db2->set('CodigoUF',  $inseir['CodigoUF']);
            //$this->db2->set('CodigoRodovia',  $inseir['CodigoRodovia']);
            //$this->db2->set('CodigoEmpreendimento',  $inseir['CodigoEmpreendimento']);
            $this->db2->set('NumeroLicenca',  $inseir['NumeroLicenca']);
            $this->db2->set('KmInicial',  $inseir['KmInicial']);
            $this->db2->set('KmFinal',  $inseir['KmFinal']);
            $this->db2->set('Extensao',  $inseir['Extensao']);
            $this->db2->set('Modal',  $inseir['Modal']);
            $this->db2->set('InicioDoSubTrecho',  $inseir['InicioDoSubTrecho']);
            $this->db2->set('FimDoSubTrecho',  $inseir['FimDoSubTrecho']);
            $this->db2->set('Municipio',  $inseir['Municipio']);
            $this->db2->set('ProcessoAdministrativoInterveniente',  $inseir['ProcessoAdministrativoInterveniente']);
            $this->db2->set('Acao',  $inseir['Acao']);
            $this->db2->set('Status',  $inseir['Status']);
            $this->db2->set('SituacaoAtual',  $inseir['SituacaoAtual']);
            $this->db2->set('PrazoValidade',  $inseir['PrazoValidade']);
            $this->db2->set('DataEmissao',  $inseir['DataEmissao']);
            $this->db2->set('DataVencimento',  $inseir['DataVencimento']);
            $this->db2->set('ProcessoSei',  $inseir['ProcessoSei']);
            $this->db2->set('LinkProcessoSei',  $inseir['LinkProcessoSei']);
            $this->db2->set('NumeroProcessoSei',  $inseir['NumeroProcessoSei']);
            $this->db2->set('LinkNumeroSei',  $inseir['LinkNumeroSei']);
            $this->db2->set('Objeto',  $inseir['Objeto']);
            $this->db2->set('Observacoes',  $inseir['Observacoes']);
            $this->db2->set('NomeArquivoShapeFile',  $inseir['NomeArquivoShapeFile']);
            $this->db2->set('NomeArquivoLicencas',  $inseir['NomeArquivoLicencas']);
            $this->db2->set('Coordenada',  $inseir['Coordenada']);
            $this->db2->set('SituacaoGeo',  $inseir['SituacaoGeo']);

            $this->db2->insert('tblLicencasAmbientais');
        }

        $this->session->unset_userdata('dadosCondiconante');
        $this->session->unset_userdata('listaCondicionantes');
    }

    public function postNovaLicenca($dados)
	{
    	try {
    		$this->db2->insert('tblLicencasAmbientais', $dados);
			$this->db2->trans_complete();

			if ($this->db2->trans_status() === FALSE) {
				$this->db2->trans_rollback();
				throw new Exception("Erro ao tentar inserir os dados.\n" . $this->db2->get_compiled_insert());
			} else {
				$this->db2->trans_commit();
				return [
					'status' => true,
					'result' => $this->db2->insert_id()
				];
			}
		} catch (Exception $e) {
			return [
				'status' => false,
				'result' => $e->getMessage()
			];
		}
	}
}
