<?php

class CondicionanteModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    /*
    * Buscar condiconantes pelo codigo da licença
    */
    public function getCondicionantes($dados = null)
    {
        $whereStatus = '';
        if (isset($dados['StatusCondicionante'])) {
            $whereStatus = "StatusCondicionante = {$dados['StatusCondicionante']} and";
        }
        $query = $this->db2->query("
        select * from tblCondicionantes  CD
        left join tblLicencasAmbientais LA 
			on LA.CodigoLicenca = CD.CodigoLicenca
        left join tblSubTipoTemas ST 
			on ST.CodigoSubtipo = CD.CodigoSubtipo
        left join tbltipos TP 
			on TP.CodigoTipo = CD.CodigoTipo
        left join tblTemas TM 
            on TM.CodigoTema = ST.CodigoTema
            where {$whereStatus}
            CD.CodigoLicenca = {$dados['CodigoLicenca']}
        ");
        return $query->result_array();
    }
    /*
    * Buscar condiconantes pelo codigo da licença
    */
    public function listarCondicionantesEmpreendimento($dados)
    {


        $this->db2->select('*');
        $this->db2->from('tblCondicionantes C');
        $this->db2->join('tblLicencasAmbientais LA', 'LA.CodigoLicenca = C.CodigoLicenca');


        // if (empty($dados['StatusCondicionante'])) {
        //     $this->db2->where('tblCondicionantes.CodigoLicenca', $dados['CodigoLicenca']);
        // }
        // if (!empty($dados['StatusCondicionante'])) {
        //     $this->db2->where('tblCondicionantes.CodigoLicenca', $dados['StatusCondicionante']);
        // }


        $query = $this->db2->get();
        return $query->result_array();
    }

    public function gravarCondicionantes($dados)
    {

        $codigo = $this->session->userdata('dadosCondiconante');
        foreach ($dados as $inseir) {
            $this->db2->set('CodigoLicenca', $codigo['CodigoLicenca']);
            $this->db2->set('CodigoTipo', $codigo['CoditoTipo']);
            $this->db2->set('CodigoSubtipo', $codigo['CodigoSubtipo']);
            $this->db2->set('NumeroCondicional', $inseir['NumeroCondicional']);
            $this->db2->set('Titulo', $inseir['Titulo']);
            $this->db2->set('Descricao', $inseir['Descricao']);
            $this->db2->set('Recorrencia', $inseir['Recorrencia']);
            $this->db2->set('Prazo', $inseir['Prazo']);
            $this->db2->set('Responsavel', $inseir['Responsavel']);
            $this->db2->set('EmailResponsavel', $inseir['EmailResponsavel']);
            $this->db2->set('TelefoneResponsavel', $inseir['TelefoneResponsavel']);
            $this->db2->set('StatusCondicionante', 0);
            $this->db2->insert('tblCondicionantes');
        }
        $this->session->unset_userdata('dadosCondiconante');
        $this->session->unset_userdata('listaCondicionantes');
    }
}
