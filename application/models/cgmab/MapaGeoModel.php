<?php

class MapaGeoModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }
    public function getMonitoramentoFauna($dados)
    {
        $this->db->select("ATR.*");
        $this->db->select("SE.NomeServico");
        $this->db->select("CT.Numero as Contrato");
        $this->db->select("EMP.Rodovia");
        $this->db->select("EMP.UF");
        $this->db->select("RLP.*");
        $this->db->from('tblAtropelamentoFaunaRelatorioAnimais as ATR');
        $this->db->join('tblEscopoServicosProgramas as SP', 'ATR.CodigoEscopoPrograma = SP.CodigoEscopoPrograma ', 'left');
        $this->db->join('tblEscopoServicos as ES', 'SP.CodigoEscopo = ES.CodigoEscopo', 'left');
        $this->db->join('tblContratos  as CT', 'ES.CodigoContrato = CT.CodigoContrato', 'left');
        $this->db->join('tblServicos as SE', 'ES.CodigoServico = SE.CodigoServico', 'left');
        $this->db->join('tblEmpreendimentos as EMP', 'CT.CodigoEmpreendimento = EMP.CodigoEmpreendimento', 'left');
        $this->db->join('tblRelatoriosMensaisProgramas as RLP', 'RLP.CodigoEscopoPrograma = ATR.CodigoEscopoPrograma', 'left');

        if (!empty($dados['mes'])) {
            $this->db->where("RLP.MesReferencia", $dados['mes']);
        }
        if (!empty($dados['contrato'])) {
            $this->db->where("CT.CodigoContrato", $dados['contrato']);
        }
        if (!empty($dados['uf'])) {
            $this->db->where("EMP.UF", $dados['uf']);
        }

        $query = $this->db->get();
        return $query->result_array();
    }
    public function getRecursosHidricos($dados)
    {
        $this->db->select("ATR.*");
        $this->db->select("HP.*");
        $this->db->select("CT.Numero as Contrato");
        $this->db->select("EMP.Rodovia");
        $this->db->select("EMP.UF");
        $this->db->select("RLP.*");
        $this->db->from('tblConfiguracaoRecursosHidricosFotos as ATR');
        $this->db->join('tblConfiguracaoRecursosHidricosPontos as HP', 'ATR.CodigoConfiguracaoRecursosHidricosPontoColeta = HP.CodigoConfiguracaoRecursosHidricosPontoColeta', 'left');
        $this->db->join('tblEscopoServicosProgramas as SP', 'HP.CodigoEscopoPrograma = SP.CodigoEscopoPrograma ', 'left');
        $this->db->join('tblEscopoServicos as ES', 'SP.CodigoEscopo = ES.CodigoEscopo', 'left');
        $this->db->join('tblContratos  as CT', 'ES.CodigoContrato = CT.CodigoContrato', 'left');
        $this->db->join('tblServicos as SE', 'ES.CodigoServico = SE.CodigoServico', 'left');
        $this->db->join('tblEmpreendimentos as EMP', 'CT.CodigoEmpreendimento = EMP.CodigoEmpreendimento', 'left');
        $this->db->join('tblRelatoriosMensaisProgramas as RLP', 'RLP.CodigoEscopoPrograma = HP.CodigoEscopoPrograma', 'left');

        if (!empty($dados['mes'])) {
            $this->db->where("RLP.MesReferencia", $dados['mes']);
        }
        if (!empty($dados['contrato'])) {
            $this->db->where("CT.CodigoContrato", $dados['contrato']);
        }
        if (!empty($dados['uf'])) {
            $this->db->where("EMP.UF", $dados['uf']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    public function buscarMedicoesParametros($dados)
    {
        /*
select RP.NomeParametro, RM.Valor from tblRecursosHidricosRelatorioMedicao RM
left join tblRecursosHidricosParametros RP on RP.CodigoRecursosHidricosParametros = RM.CodigoRecursosHidricosParametros
left join tblParametrosMedicaoAgua P on P.CodigoParametrosMedicaoAgua = RP.CodigoParametrosMedicaoAgua
 where CodigoConfiguracaoRecursosHidricosFotos = 5
 */
        $query = $this->db->query("select RP.NomeParametro as nomeP, RM.Valor from tblRecursosHidricosRelatorioMedicao RM
        left join tblRecursosHidricosParametros RP on RP.CodigoRecursosHidricosParametros = RM.CodigoRecursosHidricosParametros
        left join tblParametrosMedicaoAgua P on P.CodigoParametrosMedicaoAgua = RP.CodigoParametrosMedicaoAgua
         where CodigoConfiguracaoRecursosHidricosFotos = {$dados['CodigoConfiguracaoRecursosHidricosFotos']}");

        //         $this->db->select("*");
        //         $this->db->from('tblRecursosHidricosRelatorioMedicao RM');
        //         $this->db->join('tblRecursosHidricosParametros RP', 'RP.CodigoRecursosHidricosParametros = RM.CodigoRecursosHidricosParametros', 'left');
        //         $this->db->join('tblParametrosMedicaoAgua P', 'P.CodigoParametrosMedicaoAgua =  RP.CodigoParametrosMedicaoAgua', 'left');
        //         $this->db->where("RM.CodigoConfiguracaoRecursosHidricosFotos", $dados['CodigoConfiguracaoRecursosHidricosFotos']);

        //         $query = $this->db->get();
        return $query->result_array();
    }

    public function getLicencas()
    {
        $this->db->select("*");
        $this->db->from('tblLicencasMapa');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getContatosFiltro()
    {
        $this->db->select("CodigoContrato, Numero, Lote");
        $this->db->from('tblContratos');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getMesFiltro()
    {
        $this->db->select("distinct MesReferencia, AnoReferencia", false);
        $this->db->from('tblRelatoriosMensaisProgramas');
        $query = $this->db->get();
        return $query->result_array();
    }
}
