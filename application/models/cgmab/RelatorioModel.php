<?php

class RelatorioModel extends CI_Model
{
    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function getPontos($dados)
    {
        $query = $this->db2->query("select * from tblConfiguracaoRecursosHidricosFotos RF
		left join tblConfiguracaoRecursosHidricosPontos CP on CP.CodigoConfiguracaoRecursosHidricosPontoColeta =  RF.CodigoConfiguracaoRecursosHidricosPontoColeta
		left join tblRecursosHidricosListasPontos LP on LP.CodigoConfiguracaoRecursosHidricosPontoColeta = RF.CodigoConfiguracaoRecursosHidricosPontoColeta
		where CP.CodigoEscopoPrograma = " . $dados['CodigoEscopoPrograma'] . "");
        return $query->result_array();
    }

    public function inserirDocumentosPadrao($dados, $tabela)
    {
        $this->db2->insert_batch($tabela, $dados);
        $this->db2->trans_complete();
        if ($this->db2->trans_status() === false) {
            $this->db2->trans_rollback();
            return false;
        } else {
            $this->db2->trans_commit();
            return true;
        }
    }

    public function salvarDadosFaunaPadrao($dados, $tabela)
    {
        $this->db2->insert_batch($tabela, $dados);

        if ($this->db2->trans_status() === true) {
            return [
                'status' => true,
                'result' => 'Dados Salvos Com Sucesso',
            ];
        } else {
            return [
                'status' => false,
                'result' => 'Erro ao Inserir dados',
            ];
        }
    }

    public function deletarFaunaPadrao($codigoConfiguracao, $codigo, $tabela)
    {
        $this->db2->where($codigoConfiguracao, $codigo);
        $this->db2->delete($tabela);
        $this->db2->trans_complete();
        if ($this->db2->trans_status() === false) {
            $this->db2->trans_rollback();
            return false;
        } else {
            $this->db2->trans_commit();
            return true;
        }
    }

    public function inserirFotosFaunaPadrao($dados, $tabela, $codigoID)
    {
        $this->db2->update_batch($tabela, $dados, $codigoID);

        $this->db2->trans_complete();
        if ($this->db2->trans_status() === false) {
            $this->db2->trans_rollback();
            return false;
        } else {
            $this->db2->trans_commit();
            return true;
        }
    }
    public function inserirFotosFaunaPadrãoBach($dados, $tabela)
    {

        $this->db2->insert_batch($tabela, $dados, 'CodigoRelatorioAnimais');
        $this->db2->trans_complete();
        if ($this->db2->trans_status() === false) {
            $this->db2->trans_rollback();
            return false;
        } else {
            $this->db2->trans_commit();
            return true;
        }
    }

    public function getListaMonitoramentoFaunaSemFoto($codigo)
    {
        $query = $this->db2->query(
            "SELECT *
            FROM tblMonitoramentoFaunaRelatorioAnimais
            WHERE CodigoAtividadeCronogramaFisico = " . $codigo . "
            AND Caminho IS NULL"
        );
        return $query->result_array();
    }

    public function getInformacaoRelatorioMonitoramentoFauna($codigoAtividade, $codigoDoRegistro, $defineColuna)
    {
        $query = $this->db2->query(
            "SELECT *
            FROM tblMonitoramentoFaunaAnaliseGrupoModulo
            WHERE CodigoAtividadeCronogramaFisico = " . $codigoAtividade . "
            AND $defineColuna = " . $codigoDoRegistro . ""
        );
        return $query->result_array();
    }

    public function salvarRelatorioMensal($dados)
    {
        $resp = [];
        $this->db2->insert('tblRelatoriosMensaisProgramas', $dados);
        $this->db2->trans_complete();
        return $resp['CodigoRelatorioMensalPrograma'] = $this->db2->insert_id();
    }


    public function getListaPassagemFaunaArmadilhas()
    {
        $query = $this->db2->query("select * from tblConfiguracaoPassagemFaunaEquipamentosUtilizados E
        left join tblConfiguracaoPassagemFauna AN on AN.CodigoConfiguracaoPassagemFauna = E.CodigoConfiguracaoPassagemFauna
        left join tblPassagemFaunaAnimaisCapturados C on C.CodigoConfiguracaoPassagemFauna = AN.CodigoConfiguracaoPassagemFauna
        ");
        return $query->result_array();
    }


    public function insertAnimaisPassagem($dados)
    {
        $this->db2->insert_batch('tblPassagemFaunaAnimaisCapturados', $dados);
    }

    public function getListaAtropelamentoFaunaSemFoto($codigo)
    {
        $query = $this->db2->query(
            "SELECT *
            FROM tblAtropelamentoFaunaRelatorioAnimais
            WHERE CodigoEscopoPrograma = " . $codigo . "
            AND CaminhoFoto IS NULL"
        );
        return $query->result_array();
    }

    public function getListaAfugentamentoFaunaSemFoto($codigo)
    {
        $query = $this->db2->query(
            "SELECT *
            FROM tblAfugentamentoFaunaPontos
            WHERE CodigoAtividadeCronogramaFisico = " . $codigo . "
            AND CaminhoFoto IS NULL"
        );
        return $query->result_array();
    }

    public function getInfoShapeFile($codigo)
    {
        $query = $this->db2->query(
            "SELECT *
            FROM tblAtropelamentoFaunaRelatorioAnalises
            WHERE CodigoAtividadeCronogramaFisico = " . $codigo . ""
        );
        return $query->result_array();
    }

    public function saveShapeFile($tabela, $cord, $infoShapefile)
    {
        $data[0] = $cord;
        if ($infoShapefile) {

            // print_r($data);
            $this->db2->update_batch($tabela, $data, $infoShapefile[0]['CodigoRelatorioAnalises']);
        } else {
            $this->db2->insert_batch('tblAtropelamentoFaunaRelatorioAnalises', $data);
        }
    }



    public function fichaAtropelamento($dados)
    {
        $this->db->where('CodigoAtropelamentoAnimais', $dados['CodigoAtropelamentoAnimais']);
        unset($dados['CodigoAtropelamentoAnimais']);
        $this->db->update('tblAtropelamentoFaunaRelatorioAnimais', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getTextos($dados)
    {

        $query = $this->db2->select('CodigoIntroducaoAnalise, ' . $dados['coluna'] . '')
            ->from("tblPassagemFaunaIntroducoesConclusoesAnalises")
            ->where('CodigoAtividadeCronogramaFisico', $dados['atividade'])
            ->where("" . $dados['coluna'] . " is not null");
        $query = $this->db2->get();
        return  $query->result_array();
    }
}
