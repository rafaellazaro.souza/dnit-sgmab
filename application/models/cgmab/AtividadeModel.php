<?php

class AtividadeModel extends CI_Model
{
    private $db2;
    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function getAtividadesMes($dados)
    {
        if (isset($dados['CodigoAtividadeCronogramaFisico'])) {
            $query =  $this->db2->query("SELECT *
            FROM tblRelatoriosMensaisProgramasAtividades A
            left join tblAtividadesCronogramasFisicos AT on AT.CodigoAtividadeCronogramaFisico = A.CodigoAtividadeCronogramaFisico
            left join tblConogramaFisoProgramas C on C.CodigoCronogramaFisicoProgramas = AT.CodigoCronogramaFisicoProgramas
            where AT.CodigoAtividadeCronogramaFisico = " . $dados['CodigoAtividadeCronogramaFisico'] . "");
        } else if (isset($dados['Status'])) {
            $query =  $this->db2->query("SELECT *
            FROM tblRelatoriosMensaisProgramasAtividades A
            left join tblAtividadesCronogramasFisicos AT on AT.CodigoAtividadeCronogramaFisico = A.CodigoAtividadeCronogramaFisico
            left join tblConogramaFisoProgramas C on C.CodigoCronogramaFisicoProgramas = AT.CodigoCronogramaFisicoProgramas
            where A.CodigoRelatorioMensalPrograma = " . $dados['CodigoRelatorio'] . " and AT.Status = " . $dados['Status'] . "");
        } else {
            $query =  $this->db2->query("SELECT *
            FROM tblRelatoriosMensaisProgramasAtividades A
            left join tblAtividadesCronogramasFisicos AT on AT.CodigoAtividadeCronogramaFisico = A.CodigoAtividadeCronogramaFisico
            left join tblConogramaFisoProgramas C on C.CodigoCronogramaFisicoProgramas = AT.CodigoCronogramaFisicoProgramas
            where A.CodigoRelatorioMensalPrograma = " . $dados['CodigoRelatorio'] . " ");
        }

        return $query->result_array();
    }
    // public function getAtividadesMes($dados)
    // {
    //     // print_r($dados);
    //     // echo $dados['MesAtividade'];
    //     // exit;
    //     if (isset($dados['CodigoAtividadeCronogramaFisico'])) {
    //         $query = $this->db2->query("
    //         select * from tblAtividadesCronogramasFisicos A
    //         left join tblConogramaFisoProgramas C on A.CodigoCronogramaFisicoProgramas = C.CodigoCronogramaFisicoProgramas
    //         where A.CodigoAtividadeCronogramaFisico = " . $dados['CodigoAtividadeCronogramaFisico'] . "
    //         ");
    //     } else {
    //         if (isset($dados['MesAtividade'])) {
    //             $query = $this->db2->query("
    //             select * from tblAtividadesCronogramasFisicos A
    //             left join tblConogramaFisoProgramas C on A.CodigoCronogramaFisicoProgramas = C.CodigoCronogramaFisicoProgramas
    //             where A.MesAtividade = " . $dados['MesAtividade'] . " and A.AnoAtividade = " . $dados['AnoAtividade'] . " 
    //             and C.CodigoEscopoPrograma = " . $dados['CodigoEscopoPrograma'] . "
    //             and A.Status = " . $dados['Status'] . "
    //             ");
    //         } else {
    //             $query = $this->db2->query("
    //             select * from tblAtividadesCronogramasFisicos A
    //             left join tblConogramaFisoProgramas C on A.CodigoCronogramaFisicoProgramas = C.CodigoCronogramaFisicoProgramas
    //             where 
    //             C.CodigoEscopoPrograma = " . $dados['CodigoEscopoPrograma'] . "
    //             and A.Status = " . $dados['Status'] . "
    //             ");
    //         }
    //     }


    //     return $query->result_array();
    // }

    public function getAtividadesEscopoMes($dados)
    {
        $query = $this->db2->query("select C.CodigoEscopoPrograma, C.CodigoCronogramaFisicoProgramas, A.CodigoAtividadeCronogramaFisico, A.Atividade, A.MesAtividade, A.AnoAtividade, A.PossuiRelatorio, A.Descricao from tblEscopoServicosProgramas E 
        left join tblConogramaFisoProgramas C on C.CodigoEscopoPrograma =  E.CodigoEscopoPrograma
        left join tblAtividadesCronogramasFisicos A on A.CodigoCronogramaFisicoProgramas = C.CodigoCronogramaFisicoProgramas
        where C.CodigoEscopoPrograma = " . $dados['codigoEscopoPrograma'] . " and A.Status = 0");
        return $query->result_array();
    }

    public function uploadFotos($atividade, $caminho, $dadaFoto, $latitude, $longitude)
    {
        $data = [
            'CodigoAtividadeCronogramaFisico' => $atividade,
            'Caminho' => $caminho,
            'DataFoto' => $dadaFoto,
            'Lat' => $latitude,
            'Long' => $longitude,
        ];
        if ($this->db2->insert('tblAtividadesCronogramasFisicosFotos', $data)) {
            return true;
        }
    }

    public function verificaSubtipoTema($dados)
    {
        $query = $this->db2->query("
        select ST.CodigoSubtipo from tblEscopoServicosProgramas P
left  join tblCondicionantes C on C.CodigoCondicionante = P.CodigoCondicionante
left join tblSubTipoTemas ST on ST.CodigoSubtipo = C.CodigoSubtipo
where P.CodigoEscopoPrograma = " . $dados['CodigoEscopoPrograma'] . "
        ");
        return $query->result_array();
    }

    public function salvarAtividadesRelatorioMes($dados)
    {
        
        try {
            $this->db2->db_debug = false;
           
            $this->db2->insert_batch('tblRelatoriosMensaisProgramasAtividades', $dados);
        
           
            if ($this->db2->trans_status()) {
                return [
                    'status' => true,
                    'result' => 'Dados Salvos Com Sucesso'
                ];
            }
            throw new Exception("Error Processing Request");
        } catch (Exception $e) {
            return [
                'status' => false,
                'result' => 'Erro ao Inserir dados'
            ];
        }
        //print_r($dados);exit;
    }

    public function getRecursosAtividadesContrato($dados)
    {

        $tabela = '';
        $coluna = '';
        $codigo = '';
        $join = '';
        if ($dados['tipo'] == 'profisionais') {
            $tabela = 'tblRecursosHumanos';
            $coluna = "RH";
            $codigo = 'CodigoRecursosHumanos';
        }
        if ($dados['tipo'] == 'veiculos') {
            $tabela = 'tblVeiculos';
            $coluna = "V";
            $codigo = 'CodigoVeiculo';
            $join =  'left Join tblModeloVeiculos M on M.CodigoModelo = V.CodigoModelo';
        }
        if ($dados['tipo'] == 'equipamentos') {
            $tabela = 'tblEquipamentos';
            $coluna = "EQ";
            $codigo = 'CodigoEquipamento';
        }


        $query = $this->db2->query("
        select * from tblAtividadesCronogramasFisicosRecursosUtilizados R
        left join tblEscopoRecursos E on E.CodigoRecurso = R.CodigoRecurso 
        left join " . $tabela . ' ' . $coluna . "  on " . $coluna . "." . $codigo . " = E." . $codigo . "
        " . $join . "
       
        where E." . $codigo . " is not null and R.CodigoAtividadeCronogramaFisico = " . $dados['atividade'] . "
        ");
        return $query->result_array();
    }

    public function salvarAnexos($dados)
    {
        $this->db2->insert_batch('tblAnexos', $dados);
        $this->db2->trans_complete();
        if ($this->db2->trans_status() === false) {
            $this->db2->trans_rollback();
            return false;
        } else {
            $this->db2->trans_commit();
            return true;
        }
    }

    public function verificarAtividadeNoRelatorio($dados){
        $query = $this->db2->query("");
        return $query->result_array();
    }
}
