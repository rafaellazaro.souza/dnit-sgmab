<?php

class ContratoModel extends CI_Model
{

    private $db2;
    private $db3;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
        // $this->db3 = $this->load->database('SIMDNIT', true);
    }

    /*
    listar contratos cadastrados no SIMDNIT
    */
    public function getContratos()
    {

        $this->db3->select("
        SK_CONTRATO,
        SK_EMPRESA,
        NU_CON_FORMATADO,
        NU_CNPJ_CPF,
        NO_EMPRESA,
        DS_MODAL,
        NO_MUNICIPIO,
        NU_EDITAL,
        SG_UND_GESTORA,
        DS_FAS_CONTRATO,
        NU_LOTE_LICITACAO,
        DT_ASSINATURA,
        DT_TER_PRV,
        TIPO_LICITACAO,
        NU_PROCESSO,
        DT_PUBLICACAO,
        DT_INICIO,
        DS_OBJETO,
        Valor_Inicial_Adit_Reajustes,
        ");
        $this->db3->from('dados_contrato');
        $this->db3->like('NU_CON_FORMATADO', $_GET['numero']);

        if ($_GET['TipoConsulta'] == 'ContratosAssociados') {
            //DS_FAS_CONTRATO = 'ATIVO' ===> colocar status do contrato no futuro
            $this->db3->where("(NU_CON_FORMATADO like '%" . $_GET['numero'] . "%')");
        } else {
            $this->db3->where('SG_UND_GESTORA', 'CGMAB');
            $this->db3->where("(DS_FAS_CONTRATO = 'ATIVO' or DS_FAS_CONTRATO = 'PARALISADO' and NU_CON_FORMATADO like '%" . $_GET['numero'] . "%'  and  SG_UND_GESTORA = 'CGMAB')");
        }
        $query = $this->db3->get();
        return  $query->result_array();
    }


    public function getFiscaisContrato($codigoContrato)
    {
        $sql = "SELECT 
                    NomeUsuario,
                    Email,
                    Telefone,
                    CPF,
                    RG = '',
                    CodigoPerfil = 3,
                    Status = 1,
                    SkFiscal
                FROM (
                SELECT 
                    [SK_CONTRATO],
                    [SK_FISCAL] as SkFiscal,
                    [NM_FISCAL] as NomeUsuario,
                    [EMAIL_FISCAL] as Email,
                    [TEL_FISCAL] as Telefone,
                    [NU_CPF] as CPF
                FROM Dados_Fiscais_Contrato
                UNION
                SELECT 
                    [SK_CONTRATO],
                    [SK_FISCAL_SUBS] as SkFiscal,
                    [NM_FISCAL_SUBS] as NomeUsuario,
                    [EMAIL_FISCAL_SUBS] as Email,
                    [TEL_FISCAL_SUBS] as Telefone,
                    [NU_CPF_SUBS] as CPF
                FROM Dados_Fiscais_Contrato
                ) as Dados_Fiscasis
                WHERE 1=1
                AND SK_CONTRATO = {$codigoContrato} ";

        $query = $this->db3->query($sql);
        return  $query->result_array();
    }

    public function getFiscais($codigoContrato)
    {
        $sql = "SELECT 
                    *
                from (
                select 
                    cont.CodigoContrato,
                    fiscal.CodigoUsuario,
                    fiscal.NomeUsuario,
                    fiscal.Email,
                    fiscal.CPF,
                    fiscal.Telefone,
                    Tipo = 'Fiscal Técnico'
                from tblContratos cont
                INNER join tblUsuarios fiscal
                on fiscal.CodigoUsuario = cont.FiscalTecnico
                union 
                select 
                    cont.CodigoContrato,
                    fiscalSubstituto.CodigoUsuario,
                    fiscalSubstituto.NomeUsuario,
                    fiscalSubstituto.Email,
                    fiscalSubstituto.CPF,
                    fiscalSubstituto.Telefone,
                    Tipo = 'Fiscal Substituto'
                from tblContratos cont
                INNER join tblUsuarios fiscalSubstituto
                on fiscalSubstituto.CodigoUsuario = cont.FiscalSubstituto
                union 
                select 
                    cont.CodigoContrato,
                    fiscalAdm.CodigoUsuario,
                    fiscalAdm.NomeUsuario,
                    fiscalAdm.Email,
                    fiscalAdm.CPF,
                    fiscalAdm.Telefone,
                    Tipo = 'Fiscal Administrativo'
                from tblContratos cont
                INNER join tblUsuarios fiscalAdm
                on fiscalAdm.CodigoUsuario = cont.FiscalAdministrativo
                
                ) as Fiscais_Contrato
                where CodigoContrato = {$codigoContrato}
        ";

        $query = $this->db2->query($sql);
        return  $query->result_array();
    }

    /*
    * Salvar dados da Contratada
    */

    public function salvarContratada($dados)
    {
        print_r($dados);
        exit;
    }

    /*
    * Listar Aditivos Cadastrados SIMDNIT
    */
    public function getContratosAditivos($dados)
    {

        $this->db3
            ->select("*")
            ->from("Dados_Aditivo")
            ->where("SK_CONTRATO", $dados['skContrato']);

        $query = $this->db3->get();
        return $query->result_array();
    }

    /*
    * Listar Aditivos Cadastrados SIMDNIT
    */
    public function getContratosReajustes($dados)
    {
        $this->db3
            ->select("*")
            ->from("Dados_Reajuste")
            ->where("SK_CONTRATO", $dados['skContrato']);

        $query = $this->db3->get();
        return $query->result_array();
    }
    /*
     * Salvar Aditivos
     */

    public function getContratosAditivosSalvar($dados)
    {

        $this->db2->insert_batch('tblAditivosContratos', $dados);
        if ($this->db2->trans_status() === true) {
            echo 'Sucesso';
        } else {
            echo 'Erro';
        }
    }

    /*
     * Salvar Aditivos
     */

    public function getContratosReajustesSalvar($dados)
    {

        $this->db2->insert_batch('tblApostilamentos', $dados);
        if ($this->db2->trans_status() === true) {
            echo 'Sucesso';
        } else {
            echo 'Erro';
        }
    }

    /*
    * Salvar dados do contrato
    */
    public function salvarContrato($dados)
    {

        try {
            $this->db2->insert('tblContratos', (array) $dados);
            $this->db2->trans_complete();

            if ($this->db2->trans_status() === FALSE) {
                $this->db2->trans_rollback();
                throw new Exception("Erro ao tentar inserir os dados.\n" . $this->db2->get_compiled_insert());
            } else {
                $this->db2->trans_commit();
                return [
                    'status' => true,
                    'result' => $this->db2->insert_id()

                ];
            }
        } catch (Exception $e) {
            return [
                'status' => false,
                'result' => $e->getMessage()
            ];
        }
    }

    public function getVerificaConfiguracaoInicialContrato($dados)
    {
        // print_r($dados);exit;

        $query = $this->db2->query("select Mes, Ano, ConfiguracaoInicial,tblEscopoServicos.CodigoEscopo, tblEscopoServicos.CodigoContrato, tblParecerFiscal.Ano, tblParecerFiscal.StatusFiscal 
        from tblEscopoServicos 
        left join tblParecerFiscal on tblParecerFiscal.CodigoContrato = tblEscopoServicos.CodigoContrato 
        where CodigoContrato = " . $dados['CodigoContrato'] . "  and Mes = " . $dados['Mes'] . " and Ano = " . $dados['Ano'] . "  
        group by Mes, tblEscopoServicos.CodigoEscopo, tblEscopoServicos.CodigoContrato, tblParecerFiscal.Ano,  tblParecerFiscal.ConfiguracaoInicial,tblParecerFiscal.StatusFiscal");
        return $query->result_array();
    }

    public function getContratadasContrato()
    {
        $usuario = $this->session->Logado['CodigoUsuario'];
        $query = $this->db2->query("SELECT 
                c.CodigoContrato,
                c.Numero,
                c.NomeEmpresa,
                c.CnpjEmpresa,
                c.Modal,
                c.Municipio,
                c.Edital,
                c.Lote,
                c.Status,
                ct.CodigoContratada,
                c.PrazoCorrecao,
                emp.Sigla
            FROM tblContratos c
            left join tblContratadas ct on ct.CnpjContratada = c.CnpjEmpresa
            left join tblEmpreendimentos emp on emp.CodigoEmpreendimento = c.CodigoEmpreendimento 
            left join tblUsuarios fiscal on c.FiscalTecnico = fiscal.CodigoUsuario 
            left join tblUsuarios substituto on c.FiscalSubstituto = substituto.CodigoUsuario
            left join tblUsuarios adm on c.FiscalAdministrativo = adm.CodigoUsuario 
            where c.status IN (2, 4, 5)
            AND (c.FiscalTecnico = {$usuario} OR c.FiscalTecnico = {$usuario} OR c.FiscalAdministrativo = {$usuario})
        ");
        return $query->result_array();
    }

    public function listarServicosParecerPendente($contrato)
    {
        $query = $this->db2->query("select * from tblParecerFiscal P
        left join tblEscopoServicos E on E.CodigoEscopo = p.CodigoEscopo
        left join tblServicos ES on ES.CodigoServico = E.CodigoServico 
        where E.CodigoContrato = " . $contrato . "");
        return $query->result_array();
    }

    public function updateContrato($data, $where)
    {
        $this->db2->where($where);
        $response = $this->db2->update('tblContratos', $data);
        return $response;
    }
}
