<?php

class ParecerModel extends CI_Model
{
    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function get($table, $all = true, $where = [], $select = ['*'])
    {
        $query = $this->db2->select($select)
            ->from($table)
            ->where(array_filter($where))
            ->get();

        if ($all) {
            return $query->result_array();
        }

        return $query->row_array();
    }

    public function getParecerWithFiscal($all = true, $where = [], $table = '')
    {
        $query = $this->db2->select(['p.*', 'user.NomeUsuario'])
            ->from($table . ' p')
            ->join('tblUsuarios user', 'user.CodigoUsuario=p.CodigoFiscal', 'left')
            ->where(array_filter($where))
            ->order_by('p.CodigoParecerFiscal DESC')
            ->get();

        if ($all) {
            return $query->result_array();
        }

        return $query->row_array();
    }

    public function getProgramasParaValidacao($filtro = [])
    {
        $selectFiscal = "SELECT top 1
                            NomeUsuario
                        from tblUsuarios us
                        inner join tblParecerFiscalProgramas prog
                        on prog.CodigoFiscal = us.CodigoUsuario
                        where prog.CodigoEscopoPrograma = tm.CodigoEscopoPrograma
                        order by prog.CodigoEscopoPrograma DESC ";
        $status = "(CASE WHEN st.Status IS NULL THEN 1 ELSE st.Status END) AS Status";
        $dataParecer = "SELECT top 1
                        prog.DataParecer
                    from tblParecerFiscalProgramas prog
                    where prog.CodigoEscopoPrograma = tm.CodigoEscopoPrograma
                    order by prog.CodigoEscopoPrograma DESC";

        $this->db2->select(['tm.*', "Fiscal = ({$selectFiscal})", "DataParecer = ({$dataParecer})", 'st.DataEnvio', 'st.PrazoCorrecao', $status]);
        $this->db2->from('viewCondicionateTipoTema tm');
        $this->db2->join('tblConfiguracaoProgramasStatus st', 'st.CodigoEscopoPrograma = tm.CodigoEscopoPrograma', 'left');
        $this->db2->where_in('CodigoSubtipo', [2, 3, 4, 5]);

        if (isset($filtro['CodigoFiscal'])) {
            $where = "(FiscalAdministrativo = {$filtro['CodigoFiscal']} OR FiscalSubstituto = {$filtro['CodigoFiscal']} OR  FiscalTecnico = {$filtro['CodigoFiscal']})";
            $this->db2->where($where);
            unset($filtro['CodigoFiscal']);
        }

        $this->db2->where(array_filter($filtro));

        $query = $this->db2->get();
        return $query->result_array();
    }

    public function getRelatoriosDeAtividades($codigoFiscal)
    {
        $dataParecer = "SELECT TOP 1 PA.DataParecer 
                FROM tblParecerFiscalAtividades PA
                where pa.CodigoRelatorioMensalPrograma = REL.CodigoRelatorioMensalPrograma
                ORDER BY PA.CodigoParecerFiscal DESC
            ";

        if ($this->session->PerfilLogin == 'Fiscal') {
            $user = $this->session->Logado['CodigoUsuario'];
            $where = "(CONT.FiscalTecnico = {$user} OR CONT.FiscalSubstituto = {$user} OR CONT.FiscalAdministrativo = {$user})";
            $this->db2->where($where);
        }

        $query = $this->db2->select([
            'CONT.CodigoContrato',
            'REL.CodigoRelatorioMensalPrograma',
            'ESP.CodigoEscopoPrograma',
            'CONT.Numero',
            'TM.NomeTema',
            'SBT.NomeSubtipo',
            "CONCAT(REL.MesReferencia,'/',REL.AnoReferencia) as Periodo",
            'REL.Situacao',
            'REL.DataEnvioFiscal',
            'PrazoEnvio',
            'DataCorrecao',
            "DataParecer = ({$dataParecer})"
        ])
            ->from('tblContratos CONT')
            ->join('tblEscopoServicos ES', 'CONT.CodigoContrato = ES.CodigoContrato', 'left')
            ->join('tblEscopoServicosProgramas ESP', 'ES.CodigoEscopo = ESP.CodigoEscopo', 'left')
            ->join('tblCondicionantes CD', 'CD.CodigoCondicionante = ESP.CodigoCondicionante', 'left')
            ->join('tblTipos TP', 'TP.CodigoTipo = CD.CodigoTipo', 'inner')
            ->join('tblSubTipoTemas SBT', 'SBT.CodigoSubtipo = CD.CodigoSubtipo', 'left')
            ->join('tblTemas TM', 'TM.CodigoTema = SBT.CodigoTema', 'left')
            // ->join('tblLicencasAmbientais L', 'CD.CodigoLicenca = L.CodigoLicenca', 'inner')
            // ->join('tblLicencasTipo TPL', 'TPL.CodigoLicencaTipo = L.CodigoLicencaTipo', 'inner')
            ->join('tblConfiguracaoProgramasStatus ST', 'ST.CodigoEscopoPrograma = ESP.CodigoEscopoPrograma', 'left')
            ->join('tblRelatoriosMensaisProgramas REL', 'REL.CodigoEscopoPrograma = ESP.CodigoEscopoPrograma', 'left')
            // ->where('ST.Status', 4)
            // ->where_in('TM.CodigoTema', [1, 2, 3, 11])
            ->get();

        return $query->result_array();
    }
}
