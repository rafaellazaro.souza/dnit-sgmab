<?php
class RelatorioMonitoramentoAguaModel extends CI_Model
{
    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function getParametrosPonto($ponto)
    {


        $query =  $this->db2->query("
        select distinct(PM.CodigoParametrosMedicaoAgua) as CodigoParametro, RP.CodigoRecursosHidricosParametros ,P.CodigoEscopoPrograma, F.CodigoConfiguracaoRecursosHidricosFotos, PM.NomeParametro, PM.UnidadeMedida, RP.Limite, RP.ValorLimite
from tblRecursosHidricosListasPontos LP
left join tblConfiguracaoRecursosHidricosPontos P on P.CodigoConfiguracaoRecursosHidricosPontoColeta = LP.CodigoConfiguracaoRecursosHidricosPontoColeta
left join tblConfiguracaoRecursosHidricosFotos F on F.CodigoConfiguracaoRecursosHidricosPontoColeta = P.CodigoConfiguracaoRecursosHidricosPontoColeta
left join tblRecursosHidricosParametros RP on RP.CodigoLista = LP.CodigoLista
left join tblParametrosMedicaoAgua PM on PM.CodigoParametrosMedicaoAgua = RP.CodigoParametrosMedicaoAgua
WHERE F.CodigoConfiguracaoRecursosHidricosFotos = " . $ponto['CodigoPonto'] . "

        ");
        return $query->result_array();
    }
    public function getParametroAnalizado($dados)
    {
        $query = $this->db2->query("
        select 
        M.CodigoRecursosHidricosRelatorioMedicao,
        F.CodigoConfiguracaoRecursosHidricosPontoColeta,
        F.CodigoConfiguracaoRecursosHidricosFotos,
        AT.CodigoAtividadeCronogramaFisico,
        M.CodigoRecursosHidricosParametros,
        PMA.NomeParametro,
        PMA.UnidadeMedida,
        RP.Limite,
        RP.ValorLimite,
        M.Valor,
        M.Status,
        RP.MetodologiaAnalise,
        RP.ObservacoesEspecificas,
        AT.MesAtividade,
        AT.AnoAtividade
        from tblConfiguracaoRecursosHidricosFotos F
        left join tblRecursosHidricosRelatorioMedicao M on M.CodigoConfiguracaoRecursosHidricosFotos = F.CodigoConfiguracaoRecursosHidricosFotos
        left join tblAtividadesCronogramasFisicos AT on AT.CodigoAtividadeCronogramaFisico = M.CodigoAtividadeCronogramaFisico
        left join tblRecursosHidricosParametros RP on RP.CodigoRecursosHidricosParametros = M.CodigoRecursosHidricosParametros
        Left Join tblParametrosMedicaoAgua PMA on PMA.CodigoParametrosMedicaoAgua = RP.CodigoParametrosMedicaoAgua
        where F.CodigoConfiguracaoRecursosHidricosPontoColeta = " . $dados['CodigoConfiguracaoRecursosHidricosPontoColeta'] . " and F.TipoPontoAmostra = '" . $dados['TipoPontoAmostra'] . "' and AT.MesAtividade = " . $dados['MesAtividade'] . " and AT.AnoAtividade = " . $dados['AnoAtividade'] . "
        order By PMA.NomeParametro
    ");

        return $query->result_array();
    }

    public function getDadosGraficoParametros($dados)
    {

        $query = $this->db2->query("
        select 
        AT.MesAtividade, 
        AT.AnoAtividade, 
        MD.Valor as ValorJusante,
        (select MD.Valor from tblRecursosHidricosRelatorioMedicao MD
        left join tblAtividadesCronogramasFisicos AT on AT.CodigoAtividadeCronogramaFisico = MD.CodigoAtividadeCronogramaFisico
        left join tblRecursosHidricosParametros RP on RP.CodigoRecursosHidricosParametros = MD.CodigoRecursosHidricosParametros
        left join tblConfiguracaoRecursosHidricosFotos CF on CF.CodigoConfiguracaoRecursosHidricosFotos = MD.CodigoConfiguracaoRecursosHidricosFotos
        where CF.TipoPontoAmostra = 'Montante' and CF.CodigoConfiguracaoRecursosHidricosPontoColeta = " . $dados['CodigoConfiguracaoRecursosHidricosPontoColeta'] . " and MD.CodigoRecursosHidricosParametros = " . $dados['CodigoRecursosHidricosParametros'] . " and MD.CodigoAtividadeCronogramaFisico =  " . $dados['CodigoAtividade'] . "
        ) as ValorMontante,
        RP.Limite,
        RP.ValorLimite
        from tblRecursosHidricosRelatorioMedicao MD
        left join tblAtividadesCronogramasFisicos AT on AT.CodigoAtividadeCronogramaFisico = MD.CodigoAtividadeCronogramaFisico
        left join tblRecursosHidricosParametros RP on RP.CodigoRecursosHidricosParametros = MD.CodigoRecursosHidricosParametros
        left join tblConfiguracaoRecursosHidricosFotos CF on CF.CodigoConfiguracaoRecursosHidricosFotos = MD.CodigoConfiguracaoRecursosHidricosFotos
        where CF.TipoPontoAmostra = 'Jusante' and CF.CodigoConfiguracaoRecursosHidricosPontoColeta = " . $dados['CodigoConfiguracaoRecursosHidricosPontoColeta'] . " and MD.CodigoRecursosHidricosParametros = " . $dados['CodigoRecursosHidricosParametros'] . " and MD.CodigoAtividadeCronogramaFisico = " . $dados['CodigoAtividade'] . ";
        ");
        return $query->result_array();
    }
    public function getAnaliseResultadosPontos($dados)
    {
        $query = $this->db2->query("select CodigoAnaliseResultados, AnaliseResultadoIQA from tblRecursosHidricosAnaliseResultados
        where CodigoAtividadeCronogramaFisico = " . $dados['CodigoAtividade'] . " and AnaliseResultadoIQA is not null");
        return $query->result_array();
    }
    public function getConclusaoIqaPontos($dados)
    {
        $query = $this->db2->query("select CodigoAnaliseResultados, ConclusoesIQA from tblRecursosHidricosAnaliseResultados
        where CodigoAtividadeCronogramaFisico = " . $dados['CodigoAtividade'] . " and ConclusoesIQA is not null");
        return $query->result_array();
    }
    public function getBibliografiaIqaPontos($dados)
    {
        $query = $this->db2->query("select CodigoAnaliseResultados, BibliografiaIQA from tblRecursosHidricosAnaliseResultados
        where CodigoAtividadeCronogramaFisico = " . $dados['CodigoAtividade'] . " and BibliografiaIQA is not null");
        return $query->result_array();
    }

    public function getDadosGraficoIqa($dados)
    {

        $query = $this->db2->query("select
        PT.CodigoConfiguracaoRecursosHidricosPontoColeta,
        RHF.TipoPontoAmostra,
        
        R.Valor as ValorAtual,
        A.DataMesAtividade,
        A.MesAtividade,
        CR.CodigoEscopoPrograma
        from tblRecursosHidricosRelatorioMedicao R
        left join tblRecursosHidricosParametros RHP on RHP.CodigoRecursosHidricosParametros = R.CodigoRecursosHidricosParametros
        left join tblParametrosMedicaoAgua P on P.CodigoParametrosMedicaoAgua = RHP.CodigoParametrosMedicaoAgua
        left join tblConfiguracaoRecursosHidricosFotos RHF on RHF.CodigoConfiguracaoRecursosHidricosFotos = R.CodigoConfiguracaoRecursosHidricosFotos
        left join tblAtividadesCronogramasFisicos A on A.CodigoAtividadeCronogramaFisico = R.CodigoAtividadeCronogramaFisico
        left join tblConfiguracaoRecursosHidricosPontos PT on PT.CodigoConfiguracaoRecursosHidricosPontoColeta = RHF.CodigoConfiguracaoRecursosHidricosPontoColeta
        left join tblConogramaFisoProgramas CR on CR.CodigoCronogramaFisicoProgramas = A.CodigoCronogramaFisicoProgramas
        where R.CodigoAtividadeCronogramaFisico = " . $dados['CodigoAtividade'] . " and P.CodigoParametrosMedicaoAgua = 4");
        return $query->result_array();
    }


    public function atividadesAnteriores($mes, $escopoPrograma, $ponto, $tipoAmostra)
    {
        $query = $this->db2->query("select 
        RM.Valor,
        A.DataMesAtividade
        from tblConogramaFisoProgramas C 
        left join tblAtividadesCronogramasFisicos A on A.CodigoCronogramaFisicoProgramas = C.CodigoCronogramaFisicoProgramas
        left join tblRecursosHidricosRelatorioMedicao RM on RM.CodigoAtividadeCronogramaFisico = A.CodigoAtividadeCronogramaFisico
        left join tblRecursosHidricosParametros RHP on RHP.CodigoRecursosHidricosParametros = RM.CodigoRecursosHidricosParametros
        left join tblParametrosMedicaoAgua P on P.CodigoParametrosMedicaoAgua = RHP.CodigoParametrosMedicaoAgua
        left join tblConfiguracaoRecursosHidricosFotos RHF on RHF.CodigoConfiguracaoRecursosHidricosFotos = RM.CodigoConfiguracaoRecursosHidricosFotos
        left join tblConfiguracaoRecursosHidricosPontos PT on PT.CodigoConfiguracaoRecursosHidricosPontoColeta = RHF.CodigoConfiguracaoRecursosHidricosPontoColeta
        where  C.CodigoEscopoPrograma = ".$escopoPrograma." and A.PossuiRelatorio = 1 and A.Status = 1 and A.MesAtividade = ".($mes -1)." and P.CodigoParametrosMedicaoAgua = 4 and PT.CodigoConfiguracaoRecursosHidricosPontoColeta = ".$ponto." and RHF.TipoPontoAmostra = '".$tipoAmostra."'");
        return $query->result_array();
    }
}
