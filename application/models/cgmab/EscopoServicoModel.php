<?php

class EscopoServicoModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    /*
     * Listar condicionantes de um escopo
     */

    public function getCondiconantesEscopo($dados)
    {

       
        $query = $this->db2->query( "SELECT *,
        StatusPrograma = CASE WHEN (SELECT TOP 1 STATUS
                from tblConfiguracaoProgramasStatus ST
                where ST.CodigoEscopoPrograma = ES.CodigoEscopoPrograma
                ORDER BY ST.CodigoEscopoPrograma DESC) IS NULL
                THEN 1
                ELSE (SELECT TOP 1 STATUS
                from tblConfiguracaoProgramasStatus ST
                where ST.CodigoEscopoPrograma = ES.CodigoEscopoPrograma
                ORDER BY ST.CodigoEscopoPrograma DESC)
                END
  FROM tblEscopoServicosProgramas ES
        left join tblCondicionantes CD on CD.CodigoCondicionante = ES.CodigoCondicionante
        left join tblTipos TP on TP.CodigoTipo = CD.CodigoTipo
        left join tblSubTipoTemas SBT on SBT.CodigoSubtipo = CD.CodigoSubtipo
        left join tblTemas TM on TM.CodigoTema = SBT.CodigoTema
        left join tblLicencasAmbientais L on CD.CodigoLicenca = L.CodigoLicenca
        left join tblLicencasTipo TPL on TPL.CodigoLicencaTipo = L.CodigoLicencaTipo
        --left join tblConfiguracaoProgramasStatus ST on ST.CodigoEscopoPrograma = ES.CodigoEscopoPrograma
        where ES.CodigoEscopo = {$dados['CodigoEscopo']} ");

        return $query->result_array();
    }
    /*
     * Salvar Aditivos
     */
    public function getEscopoServicoContrato($dados)
    {

        $query = $this->db2->query("select * from 
        tblEscopoServicos ES
        left join tblServicos SR on SR.CodigoServico = ES.CodigoServico
        left join tblContratos CT on CT.CodigoContrato = ES.CodigoContrato
        left join tblEmpreendimentos EP on EP.CodigoEmpreendimento = CT.CodigoEmpreendimento
        where ES.CodigoContrato = {$dados['CodigoContrato']} order by SR.NomeServico");
        return $query->result_array();
    }
    /*
     * Salvar Aditivos
     */
    public function salvarEscopoServicoContrato($dados)
    {

        try {
            $this->db2->insert('tblEscopoServicos', (array) $dados);
            $this->db2->trans_complete();

            if ($this->db2->trans_status() === FALSE) {
                $this->db2->trans_rollback();
                throw new Exception("Erro ao tentar inserir os dados.\n" . $this->db2->get_compiled_insert());
            } else {
                $this->db2->trans_commit();
                return [
                    'status' => true,
                    'result' => $this->db2->insert_id()

                ];
            }
        } catch (Exception $e) {
            return [
                'status' => false,
                'result' => $e->getMessage()
            ];
        }
    }

    /*
     * Salvar condicionantes em um serviço 
     */

    public function salvarCondicionantes($dados)
    {
        $this->db2->insert('tblEscopoServicosProgramas', $dados);
        if ($this->db2->trans_status() === true) {
            return [
                'status' => true,
                'result' => 'Dados Inseridos com Sucesso'

            ];
        } else {
            return [
                'status' => false,
                'result' => $this->db2->get_compiled_insert()

            ];
        }
    }

    /*
     * Salvar Recursos humanos a condicionantes em um serviço de execução programas
     */

    public function salvarPessoaExecucao($dados)
    {
        //rint_r($dados);exit;
        $dadosSalvar = [];
        foreach ($dados as $i) {
            $dadosSalvar['CodigoEscopo'] = $i['CodigoEscopo'];
            if (isset($i['CodigoRecursosHumanos'])) {
                $dadosSalvar['CodigoRecursosHumanos'] = $i['CodigoRecursosHumanos'];
            }
            if (isset($i['CodigoVeiculo'])) {
                $dadosSalvar['CodigoVeiculo'] = $i['CodigoVeiculo'];
            }
            if (isset($i['CodigoEquipamento'])) {
                $dadosSalvar['CodigoEquipamento'] = $i['CodigoEquipamento'];
            }
            $dadosSalvar['CodigoEscopoPrograma'] = $i['CodigoEscopoPrograma'];
            $this->db2->insert('tblEscopoRecursos', $dadosSalvar);
        }
        if ($this->db2->trans_status() === true) {
            return [
                'status' => true,
                'result' => 'Dados Inseridos com Sucesso'

            ];
        } else {
            return [
                'status' => false,
                'result' => $this->db2->get_compiled_insert()

            ];
        }
    }

    /*
    * Busbar Pessoas em um serviço
    */

    public function getRecurosHumanosServico($dados)
    {
        $sql = '
        select RH.CodigoRecursosHumanos, ER.CodigoRecurso, RH.NomePessoa, RH.CPF, RH.Profissao, NomeSubtipo, NomeServico, SV.CodigoServico, ES.CodigoContrato  
        from tblEscopoRecursos ER
        left join tblEscopoServicosProgramas SP on SP.CodigoEscopoPrograma = ER.CodigoEscopoPrograma
        left join tblCondicionantes C on C.CodigoCondicionante = SP.CodigoCondicionante
        left join tblSubTipoTemas ST on ST.CodigoSubtipo = C.CodigoSubtipo
        left join tblTemas T on T.CodigoTema = ST.CodigoTema
        left join tblRecursosHumanos RH on RH.CodigoRecursosHumanos = ER.CodigoRecursosHumanos
        left join tblEscopoServicos ES on ES.CodigoEscopo = ER.CodigoEscopo
        left join tblServicos SV on SV.CodigoServico = ES.CodigoServico
       ';

        if (isset($dados['CodigoServico'])) {
            $sql .= 'WHERE SV.CodigoServico = ' . $dados['CodigoServico'] . ' AND ES.CodigoContrato =' . $dados['CodigoContrato'] . '';
        } else {
            $sql .= 'WHERE ES.CodigoContrato = ' . $dados['CodigoContrato'] . '';
        }

        $sql .= ' and RH.CodigoContratada = ' . $_SESSION['Logado']['CodigoContratada'] . '';
        $sql .= ' and ER.CodigoRecursosHumanos IS NOT NULL';

        $query = $this->db2->query($sql);
        return $query->result_array();
    }

    /*
    * Busbar Pessoas em um serviço
    */

    public function getPessoasEmExecucaoProgramas($dados)
    {
        $query = $this->db2->query('
            SELECT *
            FROM tblEscopoRecursos E
            join tblRecursosHumanos R on R.CodigoRecursosHumanos = E.CodigoRecursosHumanos
            where E.CodigoEscopoPrograma = ' . $dados . '');

        return $query->result_array();
    }


    public function getRecursosServicos($dados)
    {
        if ($dados['consultar'] == 'veiculos') {
            $query = $this->db2->query("SELECT *
            FROM tblEscopoRecursos r
            left join tblVeiculos v on v.CodigoVeiculo = r.CodigoVeiculo
            left join tblModeloVeiculos mv on mv.CodigoModelo = v.CodigoModelo
            left join tblMarcas m on m.CodigoMarca = mv.CodigoMarca
            left join tblEscopoServicosProgramas esp on esp.CodigoEscopoPrograma = r.CodigoEscopoPrograma
            left join tblCondicionantes c on c.CodigoCondicionante = esp.CodigoCondicionante
            left join tblSubTipoTemas s on s.CodigoSubtipo = c.CodigoSubtipo
            left join tblTemas t on t.CodigoTema = s.CodigoTema
            where r.CodigoEscopo = " . $dados['codigoEscopo'] . " and r.CodigoVeiculo is not null");
        } else {
            $query = $this->db2->query("select * from tblEscopoRecursos r
            left join tblEquipamentos e on e.CodigoEquipamento = r.CodigoEquipamento
            left join tblEscopoServicosProgramas esp on esp.CodigoEscopoPrograma = r.CodigoEscopoPrograma
              left join tblCondicionantes c on c.CodigoCondicionante = esp.CodigoCondicionante
              left join tblSubTipoTemas s on s.CodigoSubtipo = c.CodigoSubtipo
              left join tblTemas t on t.CodigoTema = s.CodigoTema
               where r.CodigoEscopo = " . $dados['codigoEscopo'] . "  and r.CodigoEquipamento is not null");
        }
        return $query->result_array();
    }

    public function verifica_infraestrutura_veiculo($dados)
    {
        $query = $this->db2->query("
        DECLARE @SAIDA INT
        EXEC VERIFICAR_INFRAESTRUTURA_VEICULO_DUPLICADO {$dados['programa']}, {$dados['veiculo']}, @TOTAL = @SAIDA OUTPUT
        SELECT @SAIDA
        IF @SAIDA >= 1
				BEGIN
					RAISERROR('Não é Possível inserir o mesmo veículo em mais de um programa', 16,1)
				END
        ");
        return $query->result_array();
    }
}
