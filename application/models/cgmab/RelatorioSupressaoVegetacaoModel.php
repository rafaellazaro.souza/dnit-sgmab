<?php


class RelatorioSupressaoVegetacaoModel extends CI_Model
{
	private $db2;

	public function __construct()
	{
		parent::__construct();
		$this->db2 = $this->load->database('CGMAB', true);
	}

	public function inserirDocumentosPadrao($dados, $tabela)
	{
		$this->db2->insert_batch($tabela, $dados);
		$this->db2->trans_complete();
		if ($this->db2->trans_status() === false) {
			$this->db2->trans_rollback();
			return false;
		} else {
			$this->db2->trans_commit();
			return true;
		}
	}

	public function salvarDadosPadrao($dados, $tabela)
	{
		$this->db2->insert_batch($tabela, $dados);

		if ($this->db2->trans_status() === true) {
			return [
				'status' => true,
				'result' => 'Dados Salvos Com Sucesso',
			];
		} else {
			return [
				'status' => false,
				'result' => 'Erro ao Inserir dados',
			];
		}
	}

	public function inserirFotosSupressao($dados, $tabela)
	{
		$this->db2->insert_batch($tabela, $dados);
		$this->db2->trans_complete();
		if ($this->db2->trans_status() === false) {
			$this->db2->trans_rollback();
			return false;
		} else {
			$this->db2->trans_commit();
			return true;
		}
	}

	public function deletarFotos($codigo)
	{
		$this->db2->where('CodigoControleSemanalFotos', $codigo);
		$this->db2->delete('tblFloraControleSemanalSupressaoAreaFotos');
		$this->db2->trans_complete();
		if ($this->db2->trans_status() === false) {
			$this->db2->trans_rollback();
			return false;
		} else {
			$this->db2->trans_commit();
			return true;
		}
	}

	public function insertDocumentos($dados)
	{
		return $this->db2->insert('tblSupressaoVegetacaoPilhasDocumentos', $dados);
	}

	public function insertFoto($dados)
	{
		return $this->db2->insert('tblSupressaoVegetacaoPilhasFotos', $dados);
	}


	public function getPilhas($where = [])
	{
		$mes = "";
		$ano = "";

		if (isset($where['Mes'])) {
			$mes = " MONTH(DataCriacao) <= {$where['Mes']} ";
			unset($where['Mes']);
		}

		if (isset($where['Ano'])) {
			$ano = " YEAR(DataCriacao) <= {$where['Ano']} ";
			unset($where['Ano']);
		}

		$query = $this->db2->select('p.*')
			->from('tblSupressaoVegetacaoRelatorioPilhas p')
			->join('tblAtividadesCronogramasFisicos atv', 'p.CodigoAtividadeCronogramaFisico = atv.CodigoAtividadeCronogramaFisico', 'left')
			->join('tblConogramaFisoProgramas cro', 'cro.CodigoCronogramaFisicoProgramas = atv.CodigoCronogramaFisicoProgramas', 'left')
			->where("1=1")
			->where(array_filter($where))
			->where($mes)
			->where($ano)
			->order_by('DataCriacao DESC')
			->get();

		return $query->result_array();
	}
}
