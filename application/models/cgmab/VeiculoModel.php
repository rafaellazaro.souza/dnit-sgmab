<?php

class VeiculoModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('CGMAB', true);
    }

    public function getRecursosVeiculosContrato($dados)
    {

        $query = $this->db2->query("
            SELECT * FROM tblEscopoRecursos ES
            left join tblEscopoServicos S on s.CodigoEscopo = ES.CodigoEscopo
            left join tblVeiculos V on V.CodigoVeiculo = ES.CodigoVeiculo
            left Join tblModeloVeiculos M on M.CodigoModelo = V.CodigoModelo
            where S.CodigoContrato = " . $dados['contrato'] . " and ES.CodigoEscopoPrograma = ".$dados['escopo']." and ES.CodigoVeiculo is not null
            ");
        return $query->result_array();
    }
}
