
<?php
function logged()
{

  $ci = &get_instance(); //Instância do CodeIgniter
  $method = $ci->router->fetch_class() . '/' . $ci->router->fetch_method(); //Método atual
  $usuario_logado = $ci->session->userdata('Logado');

  if (is_null($usuario_logado) && $ci->router->fetch_class() != 'Login') {
    $url = base_url('Login');
    redirect($url);
  }

  // if (!$usuario_logado['CodigoUsuario'] && $ci->router->fetch_class() != 'Login') {
  //   $url = base_url('Login');
  //   redirect($url);
  // }
}

?>