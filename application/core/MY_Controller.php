<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Name:  MY_Controller
 * 
 * Author:  Ben Edmunds
 * Created:  7.21.2009 
 * 
 * Description:  Class to extend the CodeIgniter Controller Class.  All controllers should extend this class.
 * 
 */
class MY_Controller extends CI_Controller
{

    public $API_Model;

    /**
     * Funcao global get
     * 
     * 
     * @param POST
     * @example HTTP/POST params{
     *   table: 'UF',
     *   column: 'Sigla',
     *   where: {Id: 4}
     * }
     * 
     * @return JSON
     * @example {
     *    status: true,
     *    response: {0:{Sigla:'BA'}}
     * }
     * @uses API_Model get()
     */
    public function get()
    {
        echo json_encode(
            $this->API_Model->get(
                $this->input->post(NULL, true)
            )
        );
    }


    /**
     * Funcao global insert
     * @api DPP/API/insert
     * 
     * 
     * @param POST
     * @example HTTP/POST params{
     *   table: 'UF',
     *   data: {
     *      Nome: 'Maranhão',
     *      Sigla: 'MA'
     *   }
     * }
     * 
     * @return JSON
     * @example {
     *    status: true,
     *    response: 'dados inseridos com suceeso'
     * }
     * @uses API_Model insert()
     */
    public function insert()
    {

        $params = (array) json_decode($this->input->post('Dados', true));
        $params['data'] = (array) $params['data'];

        if (sizeof($_FILES) > 0) {
            $this->uploadFiles($params);
        }

        echo json_encode($this->API_Model->insert($params));
        die;
    }

    /**
     * Funcao global insert
     * @api DPP/API/insert
     * 
     * 
     * @param POST
     * @example HTTP/POST params{
     *   table: 'UF',
     *   data: {
     *      Nome: 'Maranhão',
     *      Sigla: 'MA'
     *   }
     * }
     * 
     * @return JSON
     * @example {
     *    status: true,
     *    response: 'dados inseridos com suceeso'
     * }
     * @uses API_Model insert()
     */
    public function insertAndReturnCreatedId()
    {

        $params = (array) json_decode($this->input->post('Dados', true));
        $params['data'] = (array) $params['data'];

        if (sizeof($_FILES) > 0) {
            $this->uploadFiles($params);
        }

        echo json_encode($this->API_Model->insertAndReutrnId($params));
        die;
    }


    /**
     * Funcao global update
     * @api APP/API/update
     * 
     * POST Request
     * @example HTTP/POST params{
     *   table: 'UF',
     *   data: {
     *      Nome: 'Maranhão',
     *      Sigla: 'MA'
     *   }
     * }
     * 
     * @return JSON
     * @example {
     *    status: true,
     *    response: 'dados atualizados com suceeso'
     * }
     * @uses API_Model update()
     */
    public function update()
    {
        $params = (array) json_decode($this->input->post('Dados', true));
        $params['data'] = (array) $params['data'];
        $params['where'] = (array) $params['where'];

        if (sizeof($_FILES) > 0) {
            $this->uploadFiles($params);
        }

        echo json_encode($this->API_Model->update($params));
        die;
    }

    /**
     * Funcao global uploadFile
     * 
     * @param Array  by reference
     * 
     * @return void
     */
    public function uploadFiles(&$params)
    {


        if (!isset($params['data']['FilesRules'])) {
            echo json_encode([
                'status' => false,
                'result' => 'As regras de upload não foram definidas.'
            ]);
            die;
        }

        $rules = $params['data']['FilesRules'];
        unset($params['data']['FilesRules']);

        foreach ($_FILES as $index => $value) {

            if (isset($_FILES[$index]['name'])) {

                $NomeArquivo = $_FILES[$index]['name'];
                $ext = @end(explode(".", $NomeArquivo));

                // verifica se o diretorio existe, se não ele cria
                if (!is_dir($rules->upload_path)) {
                    mkdir($rules->upload_path, 0755, true);
                }
                $config['upload_path'] = $rules->upload_path;
                $config['allowed_types'] = $rules->allowed_types;

                if (!function_exists('random_bytes')) {
                    $novoNome = $this->random_bytes(10);
                } else {

                    $novoNome = random_bytes(10);
                }
                $novoNomeRandom = bin2hex($novoNome) . "." . $ext;

                $pathFile = base_url($rules->upload_path) . '/' . $novoNomeRandom;

                $config['file_name'] = $novoNomeRandom;

                $this->upload->initialize($config);
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload($index)) {
                    echo json_encode([
                        'status' => false,
                        'result' => $index . '->' . $this->upload->display_errors()
                    ]);
                    die;
                } else {
                    $params['data'][$index] = $pathFile;
                }
            }
        }
    }

    public function delete()
    {
        echo json_encode($this->API_Model->delete($this->input->post(NULL, true)));
        die;
    }

    public function deleteFile()
    {
        $path = $this->input->post('path', true);
        $path = str_replace(" ", "", str_replace(base_url(), "", $path));
        

        if (file_exists($path)) {
            // Remove file 
       
            unlink($path);
            echo 'exists';
            // Set status 
            echo json_encode(true);
        } else {
            // Set status 
            echo json_encode(false);
        }
        die;
    }
}
