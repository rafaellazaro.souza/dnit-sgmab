<?php

use Shapefile\Shapefile;
use Shapefile\ShapefileException;
use Shapefile\ShapefileReader;

class ReadShapeFile
{
	private $fail = false;
	private $errors = [];
    private $Shapefile;
    private $path;

    public function __construct($params)
    {
        $this->path = $params['path'];
        $this->Shapefile = new ShapefileReader($params['file']);
    }

    public function read()
    {
        return iterator_to_array($this->iterator());
    }

    public function iterator()
    {
        try {
            while ($Geometry = $this->Shapefile->fetchRecord()) {
                if ($Geometry->isDeleted()) {
                    continue;
                }

                yield ['coordenada' => $Geometry->getGeoJSON()];
            }
        } catch (ShapefileException $e) {
			$this->fail = true;
			$this->errors[] = $e->getMessage();
        }
    }

    public function clean()
    {
        $this->Shapefile = null;
        removeDir($this->path);
    }

    public function fail()
    {
        return $this->fail;
	}
	
	public function getErrors()
    {
        return $this->errors;
    }
}
