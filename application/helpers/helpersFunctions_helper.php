<?php

/**
 * Agrupa arquivos quando vem de um input file multiple
 * @param string nameInput Recebe o name do Input vindo do Front
 * @return array file_ary de arquivos agrupados
 */
function groupMultFiles($nameInput)
{

    $file_post = $_FILES[$nameInput];
    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i = 0; $i < $file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

/**
 * Checa se existi arquivos de um input multiple
 * @param string nameInput Recebe o name do Input vindo do Front
 * @return boolean exist ou nao
 */
function hasMultipleFile($nameInput)
{
    return isset($_FILES[$nameInput]) &&  !empty($_FILES[$nameInput]['name'][0]);
}

/**
 * Checa se existi arquivos
 * @param string nameInput Recebe o name do Input vindo do Front
 * @return boolean  exist ou nao
 */
function hasFile($nameInput)
{
    return isset($_FILES[$nameInput]) &&  !empty($_FILES[$nameInput]['name']);
}

/**
 * @param int code Response status code
 * @param boolean result Response Result
 * @return void
 */
function responseJSON($code = 200, $result = true, $message = "")
{
    http_response_code($code);
    die(json_decode([
        'result' => $result,
        'message' => $message
    ]));
}

function extrairDadosFotos($caminhoFoto)
{
    date_default_timezone_set("Brazil/East");
    $dataFoto = date('Y-m-d H:i:s');
    $latitude = '';
    $longitude = '';

    //pega dados do gps
    $exif = @exif_read_data($caminhoFoto, 0, true);

    if (isset($exif['IFD0']) && array_key_exists('DateTime', $exif['IFD0'])) {
        $d = explode(":", $exif['IFD0']['DateTime']);
        $dataFoto = $d[0] . '-' . $d[1] . '-' . $d[2] . ':' . $d[3] . ':' . $d[4];
    }

    if (isset($exif['GPS'])) {
        $lat = $exif['GPS']['GPSLatitude'];
        list($num, $dec) = explode('/', $lat[0]);
        $lat_s = $num / $dec;
        list($num, $dec) = explode('/', $lat[1]);
        $lat_m = $num / $dec;
        list($num, $dec) = explode('/', $lat[2]);
        $lat_v = $num / $dec;
        $lon = $exif['GPS']['GPSLongitude'];
        list($num, $dec) = explode('/', $lon[0]);
        $lon_s = $num / $dec;
        list($num, $dec) = explode('/', $lon[1]);
        $lon_m = $num / $dec;
        list($num, $dec) = explode('/', $lon[2]);
        $lon_v = $num / $dec;
        $latitude = '-' . ($lat_s + ($lat_m / 60.0) + ($lat_v / 3600.0));
        $longitude = '-' . ($lon_s  + ($lon_m / 60.0) + ($lon_v / 3600.0));
    }

    return [
        'Lat' => $latitude,
        'Long' => $longitude,
        'DataFoto' => $dataFoto
    ];
}

function codificarDecodificarParametroUrl($param, $encode = false)
{
    $dadoCriptografado = '';
    foreach (range(0, 5) as $num) {
        if ($num == 0) {
            $dadoCriptografado = codeDecode($param, $encode);
            continue;
        }

        $dadoCriptografado = codeDecode($dadoCriptografado, $encode);
    }

    return $dadoCriptografado;
}

function codeDecode($param, $encode = false)
{
    if ($encode) {
        return base64_encode($param);
    }

    return base64_decode($param);
}
