const eventBus = new Vue();
const translateDataTable = {
	"sEmptyTable": "Nenhum registro encontrado na tabela",
	"sInfo": "Exibindo _START_ até o _END_ do _TOTAL_ Registros Encontrados",
	"sInfoEmpty": "",
	"sInfoFiltered": "(Filtrar de _MAX_ total registros)",
	"sInfoPostFix": "",
	"sInfoThousands": ".",
	"sLengthMenu": "Mostrar _MENU_ registros/ pagina",
	"sLoadingRecords": "Carregando...",
	"sProcessing": "Processando...",
	"sZeroRecords": "Nenhum registro encontrado",
	"sSearch": "Busca: ",
	"oPaginate": {
		"sNext": "<i class=\"fas fa-chevron-right\" ></i>",
		"sPrevious": "<i class=\"fas fa-chevron-left\" ></i>",
		"sFirst": "Primeiro",
		"sLast": "Ultimo"
	},
	"oAria": {
		"sSortAscending": ": Ordenar colunas de forma ascendente",
		"sSortDescending": ": Ordenar colunas de forma descendente"
	}
};

const showLoading = function (show = false) {
	if (show) {
		$('.page-loader-wrapper').css({
			display: 'block'
		});
		return
	}

	$('.page-loader-wrapper').css({
		display: 'none'
	});
}

var vmGlobal = new Vue({
	data() {
		return {
			expandSection: '',
			attr: {
				disabled: true
			},
			loading: false,
			dadosDoUsuario: [],
			dadosContrato: [],
			userPerfil: ''
		}
	},

	methods: {
		async getFromController(controller, params) {
			var output = '';
			await axios.post(base_url + controller, $.param(params))
				.then((response) => {
					output = response.data;
				})
				.catch((e) => {
					console.log(e)
					Swal.fire({
						position: 'center',
						type: 'error',
						title: 'Erro!',
						text: "Erro ao consultar dados. " + e,
						showConfirmButton: true,
					});
				});
			return output;
		},
		async getFromAPI(params) {
			// console.log('paramsGetfromAPI');
			//  console.log('hola', base_url + '/API/get', $.param(params));
			var output = '';
			await axios.post(base_url + '/API/get', $.param(params))
				.then((response) => {
					if (response.data.status == false)
						throw new Error(response.data.result);

					output = response.data.result;
				})
				.catch((e) => {
					console.log(e)
					Swal.fire({
						position: 'center',
						type: 'error',
						title: 'Erro!',
						text: "Erro ao consultar dados. ",
						showConfirmButton: true,

					});
				});
			return output;
		},
		async updateFromAPI(params, file = null, escondeMsg = true) {

			var formData = new FormData();
			formData.append('Dados', JSON.stringify(params));

			if (file !== null) {
				formData.append(file.name, file.value);
			}

			return await axios.post(base_url + 'API/update', formData)
				.then((response) => {

					if (response.data.status == false)
						throw new Error(response.data.result);
					if (escondeMsg) {
						Swal.fire({
							position: 'center',
							type: 'success',
							title: 'Salvo!',
							text: 'Alteração executada com sucesso.',
							showConfirmButton: true,
						});
					}

					return true;
				})
				.catch((e) => {
					console.log(e)
					Swal.fire({
						position: 'center',
						type: 'error',
						title: 'Erro!',
						html: "Erro ao consultar dados. <br/> Causa: <br/>- Duplicação de dados<br/>-Bloqueio do sisteman <br/>",
						showConfirmButton: true,

					});

					return true;
				});
		},
		async insertFromAPI(params, file = null, base = null, msg = null) {

			var formData = new FormData();
			formData.append('Dados', JSON.stringify(params));

			if (file !== null) {
				formData.append(file.name, file.value);
			}

			return await axios.post(base_url + 'API/insert', formData)
				.then((response) => {

					if (response.data.status == false && msg == true)
						throw new Error(response.data.result);

					Swal.fire({
						position: 'center',
						type: 'success',
						title: 'Salvo!',
						text: 'Dados inseridos com sucesso.',
						showConfirmButton: true,
					});

					return true;
				})
				.catch((e) => {
					console.log(e)
					Swal.fire({
						position: 'center',
						type: 'error',
						title: 'Erro!',
						html: "Erro ao tentar inserir os dados. <br/> Causa: <br/>- Duplicação de dados<br/>-Bloqueio do sistema <br/>" ,
						showConfirmButton: true,

					});

					return false;
				});
		},
		async deleteFromAPI(params) {
			return await axios.post(base_url + 'API/delete', $.param(params))
				.then((response) => {
					if (response.data.status == false)
						throw new Error(response.data.result);

					Swal.fire({
						position: 'center',
						type: 'success',
						title: 'Salvo!',
						html: 'Dados deletados com sucesso.',
						showConfirmButton: true,
					});

					return true;
				})
				.catch((e) => {
					console.log(e)
					Swal.fire({
						position: 'center',
						type: 'error',
						title: 'Erro!',
						html: "Erro ao deletar os dados.<br>Causas:<br/> - Vinculo com outro dado<br/>Bloqueio do sistema <br/>" ,
						showConfirmButton: true,

					});

					return false;
				});
		},
		async deleteFile(path) {
			var params = {
				path: path
			};
			await axios.post(base_url + 'API/deleteFile', $.param(params))
				.then((response) => {

					if (response == false)
						throw new Error("Arquivo não encontrado");

					return true;
				})
				.catch((e) => {
					console.log(e)
					Swal.fire({
						position: 'center',
						type: 'error',
						title: 'Erro!',
						text: "Erro ao tentar deletar arquivo. " + e,
						showConfirmButton: true,

					});
					return false;
				});
		},
		montaDatatable(id, sort = false) {
			try {
				this.$nextTick(function () {
					$(id).DataTable({
						"destroy": true,
						/*
						"scrollX": true,
						dom: 'Bfrtip',
						buttons: [
						    'excel'
						],
						"bSort": sort,
						"bSortable": false,
						"autoWidth": false,*/
						"oLanguage": {
							"sEmptyTable": "Nenhum registro encontrado na tabela",
							"sInfo": "Exibindo _START_ até o _END_ do _TOTAL_ Registros Encontrados",
							"sInfoEmpty": "",
							"sInfoFiltered": "(Filtrar de _MAX_ total registros)",
							"sInfoPostFix": "",
							"sInfoThousands": ".",
							"sLengthMenu": "Mostrar _MENU_ registros/ pagina",
							"sLoadingRecords": "Carregando...",
							"sProcessing": "Processando...",
							"sZeroRecords": "Nenhum registro encontrado",
							"sSearch": "Busca: ",
							"oPaginate": {
								"sNext": "<i class=\"fas fa-chevron-right\" ></i>",
								"sPrevious": "<i class=\"fas fa-chevron-left\" ></i>",
								"sFirst": "Primeiro",
								"sLast": "Ultimo"
							},
							"oAria": {
								"sSortAscending": ": Ordenar colunas de forma ascendente",
								"sSortDescending": ": Ordenar colunas de forma descendente"
							}
						},
					});
				});
			} catch (err) {
				console.error(err);
			}
		},
		detroyDataTable(id) {
			$(id).DataTable().destroy();
		},
		//função global para formatar data eng -> pt
		frontEndDateFormat(date) {
			var dataConvertida = moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY');
			return dataConvertida;
		},
		//função global para formatar data pt -> eng
		backEndDateFormat(date) {
			var dataConvertida = moment(date, 'DD/MM/YYYY').format('YYYY-MM-DD');
			return dataConvertida;
		},
		validarCPF(strCPF) {
			var Soma;
			var Resto;
			Soma = 0;

			for (var i = 0; i <= 9; i++) {
				if (strCPF == i + '' + i + '' + i + '' + i + '' + i + '' + i + '' + i + '' + i + '' + i + '' + i + '' + i) return false;
			}

			for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
			Resto = (Soma * 10) % 11;

			if ((Resto == 10) || (Resto == 11)) Resto = 0;
			if (Resto != parseInt(strCPF.substring(9, 10))) return false;

			Soma = 0;
			for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
			Resto = (Soma * 10) % 11;

			if ((Resto == 10) || (Resto == 11)) Resto = 0;
			if (Resto != parseInt(strCPF.substring(10, 11))) return false;
			return true;
		},
		codificarDecodificarParametroUrl(valor, tipo) {
			var dadoInicial = valor;
			for (var i = 0; i <= 5; i++) {
				if (i === 0) {
					var dadoCriptografado = this.codeDecode(dadoInicial, tipo)
				} else {
					dadoCriptografado = this.codeDecode(dadoCriptografado, tipo)
				}
			}
			return dadoCriptografado;
		},
		codeDecode(dado, tipo) {
			if (tipo === 'encode') {
				return String(btoa(dado));
			} else {
				return String(atob(dado));
			}
		},
		async dadosUsuario() {
			await axios.get(base_url + 'login/dadosUsuario')
				.then((resp) => {
					this.dadosDoUsuario = resp.data;
				});
		},


		async insertFromApiAndReturnId(showMessage = true, params, file = null, base = null, msg = null) {

			var formData = new FormData();
			formData.append('Dados', JSON.stringify(params));

			if (file !== null) {
				formData.append(file.name, file.value);
			}

			return await axios.post(base_url + 'API/insertAndReturnCreatedId', formData)
				.then((response) => {

					if (response.data.status == false && msg == true)
						throw new Error(response.data.result);

					if (showMessage) {
						Swal.fire({
							position: 'center',
							type: 'success',
							title: 'Salvo!',
							text: 'Dados inseridos com sucesso.',
							showConfirmButton: true,
						});
					}

					return response.data.id;
				})
				.catch((e) => {
					console.log(e)
					Swal.fire({
						position: 'center',
						type: 'error',
						title: 'Erro!',
						text: "Erro ao tentar inserir os dados. " + e,
						showConfirmButton: true,

					});

					return false;
				});
		},
		async getPerfil() {
			await axios.get(base_url + 'login/getPerfil')
				.then((resp) => {
					this.userPerfil = resp.data.perfil;
				});
		},



	}
});

// $(".page-loader-wrapper").css('display', 'block');
// $(".page-loader-wrapper").css('display', 'none');
