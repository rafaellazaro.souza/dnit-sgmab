	const appControlePilhas = new Vue({
		el: '#app-controlePilhas',
		components: {
			CardFotos
		},
		data() {
			return {
				mes: vmAppSupressaoVegetacao.mesAtividade,
				ano: vmAppSupressaoVegetacao.anoAtividade,
				pilha: {
					CodigoPilha: '',
					CodigoFloraSupressao: '',
					CodigoAtividadeCronogramaFisico: '',
					NumeroPilha: null,
					TipoPilha: null,
					Latitude: null,
					Logitude: null,
					Zona: null,
					CN: null,
					CE: null,
					Elevacao: null,
					DataCriacao: moment().format("YYYY-MM-DD"),
					Observacao: null
				},
				controle: {
					CodigoControlePilhas: '',
					CodigoPilha: '',
					Volume: null,
					Observacaoes: '',
					NumeroDeIndividuos: null,
					Especies: '',
					DataCriacao: null
				},
				destinacao: {
					CodigoDestinacaoPilhas: '',
					CodigoPilha: '',
					NumeroIndividuos: null,
					Especies: '',
					Volume: null,
					DataEnvio: '',
					UsoDaMadeira: '',
					Destinatario: '',
					Observacao: '',
					DataCriacao: null
				},
				listaLicencasAmbientais: [],
				listaDePilhas: [],
				listaControle: [],
				listaDestinacao: [],
				controleFotos: [],
				destinacaoFotos: [],
				destinacaoDocs: [],
				usoMadeira: [
					'Lenha',
					'Serraria',
					'Cerca',
					'Compensados',
					'Escoramento',
					'Laminação',
					'Outros'
				],
				showFormNovaPilha: false,
				showSelectedPilha: false,
				novoControlePilha: false,
				novaDesitnacaoPilha: false
			}
		},
		watch: {
			'pilha.Latitude'(val) {
				if (val) {
					val = val.replace(/,/g, '.');
					this.pilha.Latitude = val.replace(/[A-za-z]/g, '');
				}
			},
			'pilha.Logitude'(val) {
				if (val) {
					val = val.replace(/,/g, '.');
					this.pilha.Logitude = val.replace(/[A-za-z]/g, '');
				}
			},
			'controle.Volume'(val) {
				if (val) {
					val = val.replace(/,/g, '.');
					this.controle.Volume = val.replace(/[A-za-z]/g, '');
				}
			},
			'destinacao.Volume'(val) {
				if (val) {
					val = val.replace(/,/g, '.');
					this.destinacao.Volume = val.replace(/[A-za-z]/g, '');
				}
			},
		},
		computed: {
			isEspeciesAmeacada() {
				return this.pilha.TipoPilha == 2;
			},
			isDestinacaoMesAnterior() {
				if (this.destinacao.DataCriacao) {
					var mes = moment(this.destinacao.DataCriacao).format('MM');
					return parseInt(mes) < this.mes;
				}

				return false;
			},
			isControleMesAnterior() {
				if (this.controle.DataCriacao) {
					var mes = moment(this.controle.DataCriacao).format('MM');
					return parseInt(mes) < this.mes;
				}

				return false;
			},
			isPilhaMesAnterior() {
				if (this.pilha.DataCriacao) {
					var mes = moment(this.pilha.DataCriacao).format('MM');
					return parseInt(mes) < this.mes;
				}

				return false;
			},
			mesPilha() {
				if (this.pilha.DataCriacao) return moment(this.pilha.DataCriacao).format('MM');
				return this.mes;
			},
			dataMaxima() {
				return moment(this.ano + '-' + this.mes, 'YYYY-MM').endOf('month').format('YYYY-MM-DD');
			},
			dataMinima() {
				return moment(this.ano + '-' + this.mes, 'YYYY-MM').startOf('month').format('YYYY-MM-DD');
			}
		},
		methods: {
			async updatePilha(p) {
				this.pilha = Object.assign({}, p);
				this.showSelectedPilha = true;
				this.showFormNovaPilha = true;
				this.getDestinacao(p.CodigoPilha);
				this.getControle(p.CodigoPilha);


				if (this.isPilhaMesAnterior) {
					this.$nextTick(() => {
						$('form#formPilha :input', this.$el).not('.btn-danger').each((el, val) => {
							$(val).attr('disabled', true);
						});
					});
				}

				await this.$nextTick(async () => {
					await vmRelatorioAtividade.closeButtons();
				})
			},
			async salvarPilha() {
				var pilha = Object.assign({}, this.pilha);
				delete pilha.CodigoPilha;
				pilha.CodigoAtividadeCronogramaFisico = atividade;

				if (this.pilha.CodigoPilha.length != 0) {
					await vmGlobal.updateFromAPI({
						table: 'tblSuprecaoPilhas',
						data: pilha,
						where: {
							CodigoPilha: this.pilha.CodigoPilha
						}
					});
					this.showFormNovaPilha = false;
					this.showSelectedPilha = false;
					await this.getPilhas();
					return;
				}

				await vmGlobal.insertFromAPI({
					table: 'tblSuprecaoPilhas',
					data: pilha
				});

				this.showFormNovaPilha = false;
				this.showSelectedPilha = false;
				await this.getPilhas();
			},
			async getPilhas() {
				await axios.post(base_url + 'RelatorioSupressaoVegetacao/getPilhas', $.param({
						Ano: vmAppSupressaoVegetacao.anoAtividade,
						Mes: vmAppSupressaoVegetacao.mesAtividade,
						CodigoEscopoPrograma: vmAppSupressaoVegetacao.CodigoEscopo
					}))
					.then(response => {
						this.listaDePilhas = response.data.pilhas;
						this.refreshTable('#tblPilhas');
					})
					.catch(error => {
						console.log(error);
					});
			},
			async getDestinacao(id) {
				var listaDePilhas = await vmGlobal.getFromAPI({
					table: 'supressaoDestinacao',
					where: {
						CodigoPilha: id
					}
				});

				this.listaDestinacao = listaDePilhas;
				this.destinacaoFotos = [];
				this.refreshTable('#tblDestinacao');
			},
			async getControle(id) {
				var listaDePilhas = await vmGlobal.getFromAPI({
					table: 'supressaoControle',
					where: {
						CodigoPilha: id
					}
				});

				this.controleFotos = [];
				this.listaControle = listaDePilhas;
				this.refreshTable('#tblControle');
			},
			async salvarControle() {
				var controle = Object.assign({}, this.controle);
				delete controle.CodigoControlePilhas;
				delete controle.DataCriacao;
				controle.CodigoPilha = this.pilha.CodigoPilha;

				if (this.controle.CodigoControlePilhas.length != 0) {
					await vmGlobal.updateFromAPI({
						table: 'supressaoControle',
						data: controle,
						where: {
							CodigoControlePilhas: this.controle.CodigoControlePilhas
						}
					});
					await this.importarFotos('CodigoControlePilhas', this.controle.CodigoControlePilhas);

					this.novoControlePilha = false;
					await this.getControle(this.pilha.CodigoPilha);
					return;
				}

				var id = await vmGlobal.insertFromApiAndReturnId(false, {
					table: 'supressaoControle',
					data: controle
				});

				await this.importarFotos('CodigoControlePilhas', id);

				this.novoControlePilha = false;
				await this.getControle(this.pilha.CodigoPilha);
			},
			async salvarDestinacao() {
				var destinacao = Object.assign({}, this.destinacao);
				delete destinacao.CodigoDestinacaoPilhas;
				delete destinacao.DataCriacao;
				destinacao.CodigoPilha = this.pilha.CodigoPilha;

				if (this.destinacao.CodigoDestinacaoPilhas.length != 0) {
					await vmGlobal.updateFromAPI({
						table: 'supressaoDestinacao',
						data: destinacao,
						where: {
							CodigoDestinacaoPilhas: this.destinacao.CodigoDestinacaoPilhas
						}
					});
					this.importarFotos('CodigoDestinacaoPilhas', this.destinacao.CodigoDestinacaoPilhas);
					this.importarDocumentos('CodigoDestinacaoPilhas', this.destinacao.CodigoDestinacaoPilhas);
					this.cancelarDestinacao();
					await this.getDestinacao(this.pilha.CodigoPilha);
					return;
				}

				var id = await vmGlobal.insertFromApiAndReturnId(false, {
					table: 'supressaoDestinacao',
					data: destinacao
				});

				await this.importarFotos('CodigoDestinacaoPilhas', id);
				await this.importarDocumentos('CodigoDestinacaoPilhas', id);

				this.cancelarDestinacao();
				await this.getDestinacao(this.pilha.CodigoPilha);
			},
			async getConfiguracaoLicencas() {
				//Lista as licenças que estão na configuração
				let params = {
					table: 'floraSupressaoConfig',
					columns: ['CodigoFloraSupressao', 'NumeroLicenca', 'NomeOrgaoExpeditor'],
					joinLeft: [{
							table1: 'floraSupressaoConfig',
							table2: 'licencas',
							on: 'CodigoLicenca',
						},
						{
							table1: 'licencas',
							table2: 'orgaosExpeditores',
							on: 'CodigoOrgaoExpeditor',
						}
					],
					where: {
						CodigoEscopoPrograma: vmAppSupressaoVegetacao.CodigoEscopo
					}
				}
				this.listaLicencasAmbientais = await vmGlobal.getFromAPI(params, 'cgmab');
			},
			async getFoto(key, value) {
				var foto = await vmGlobal.getFromAPI({
					table: 'supressaoPilhasFotos',
					where: {
						[key]: value
					}
				});

				if (key == 'CodigoControlePilhas') this.controleFotos = foto;
				if (key == 'CodigoDestinacaoPilhas') this.destinacaoFotos = foto;
			},
			async getDocumentos(key, value) {
				var docs = await vmGlobal.getFromAPI({
					table: 'supressaoPilhasDocs',
					where: {
						[key]: value
					}
				});

				this.destinacaoDocs = docs;
				this.refreshTable('#tblAnexos', 5);
				if (this.isDestinacaoMesAnterior) {
					this.$nextTick(() => {
						var trs = $('table#tblAnexos').dataTable();
						var rows = trs.$("tr");
						$(rows).each((i, val) => {
							$(val).find('td:eq(-1)').children('.btn-danger').css({
								display: 'none'
							});
						});
					});
				}
			},
			constroiTable(id = "", pageLength = 10) {
				$('table' + id, this.$el).DataTable({
					language: translateDataTable,
					lengthChange: false,
					destroy: true,
					pageLength: pageLength
				});
			},
			async refreshTable(id = "", pageLength = 10) {
				$('table' + id, this.$el).DataTable().destroy();
				await this.$nextTick(() => {
					this.constroiTable(id, pageLength);
				});
			},
			novaPilha() {
				this.$nextTick(() => {
					this.showSelectedPilha = false;
					this.showFormNovaPilha = false;
				});

				for (key in this.pilha) {
					if (this.pilha[key]) this.pilha[key] = '';
				}

				this.$nextTick(() => {
					this.showFormNovaPilha = true;
				});
			},
			novoControle() {
				this.$nextTick(() => {
					this.novoControlePilha = false;
				});

				this.controleFotos = [];

				for (key in this.controle) {
					if (this.controle[key]) this.controle[key] = '';
				}

				this.$nextTick(() => {
					this.novoControlePilha = true;
					this.initDropfy();
				});

			},
			novaDestinacao() {
				this.$nextTick(() => {
					this.novaDesitnacaoPilha = false;
				});

				this.destinacaoFotos = [];
				this.destinacaoDocs = [];

				for (key in this.destinacao) {
					if (this.destinacao[key]) this.destinacao[key] = '';
				}
				this.$nextTick(() => {
					this.novaDesitnacaoPilha = true;
					this.initDropfy();
					this.refreshTable('#tblAnexos');
				});
			},
			async editControlePilha(p) {
				this.controleFotos = [];

				this.controle = Object.assign({}, p);
				this.novoControlePilha = true;
				this.initDropfy();
				await this.getFoto('CodigoControlePilhas', p.CodigoControlePilhas);

				if (this.isControleMesAnterior) {
					this.$nextTick(() => {
						$('form#formControle :input', this.$el).not('.btn-danger').each((el, val) => {
							$(val).attr('disabled', true);
						});
					});
				}

				await this.$nextTick(async () => {
					await vmRelatorioAtividade.closeButtons();
				})
			},
			async editDestinacao(p) {
				this.destinacaoFotos = [];
				this.destinacaoDocs = [];

				this.destinacao = Object.assign({}, p);
				this.novaDesitnacaoPilha = true;
				await this.getFoto('CodigoDestinacaoPilhas', p.CodigoDestinacaoPilhas);
				await this.getDocumentos('CodigoDestinacaoPilhas', p.CodigoDestinacaoPilhas);
				this.initDropfy();

				if (this.isDestinacaoMesAnterior) {
					this.$nextTick(() => {
						$('form#formDestinacao :input', this.$el).not('.btn-danger').each((el, val) => {
							$(val).attr('disabled', true);
						});
					});
				}

				await this.$nextTick(async () => {
					await vmRelatorioAtividade.closeButtons();
				})
			},
			getAsvPilha(licenca) {
				var asv = this.listaLicencasAmbientais.filter(el => el.CodigoFloraSupressao == licenca);
				if (asv.length != 0) {
					return asv[0].NumeroLicenca + '-' + asv[0].NomeOrgaoExpeditor;
				}

				return '';
			},
			async initDropfy() {
				await this.$nextTick(() => {
					$('.dropify', this.$el).dropify({
						messages: {
							default: "Solte ou clique para anexar arquivo."
						}
					});
				})
			},
			async importarFotos(key, value) {
				var fotos = '';

				if (this.$refs.controleFotos) fotos = this.$refs.controleFotos.files;
				if (this.$refs.destinacaoFotos) fotos = this.$refs.destinacaoFotos.files;

				if (fotos.length == 0) {
					return;
				}

				dados = this.geraFormDataInputMultifile(fotos, 'fotos');
				dados.append(key, value);

				var response = await this.sendFiles(base_url + 'RelatorioSupressaoVegetacao/uploadFotos', dados);

				if (response) {
					Swal.fire({
						position: 'center',
						type: 'success',
						title: 'Salvo!',
						text: 'Dados inseridos com sucesso.',
						showConfirmButton: true,
					});
				}

				if (this.$refs.controleFotos) this.$refs.controleFotos.value = '';
				if (this.$refs.destinacaoFotos) this.$refs.destinacaoFotos.value = '';
			},
			geraFormDataInputMultifile(files, fieldName) {
				var formData = new FormData();
				$.each(files, (index, file) => {
					formData.append(fieldName + '[' + index + ']', file);
				});
				return formData;
			},
			async sendFiles(url, dados) {
				return await axios.post(url, dados, {
						headers: {
							'Content-Type': 'multipart/form-data'
						},
					})
					.then(response => {
						if (response.data.erro != 0) return false;
						return true;
					})
					.catch(error => {
						return false;
					});
			},
			getFotos(key) {
				if (key == 'controle') this.getFoto('CodigoControlePilhas', this.controle.CodigoControlePilhas);
				if (key == 'destinacao') this.getFoto('CodigoDestinacaoPilhas', this.destinacao.CodigoDestinacaoPilhas);
			},
			async importarDocumentos(key, codigo) {
				var anexos = this.$refs.destinacaoDocs.files;

				if (anexos.length == 0) {
					return;
				}

				dados = this.geraFormDataInputMultifile(anexos, 'docs');
				dados.append(key, codigo);

				var response = await this.sendFiles(base_url + 'RelatorioSupressaoVegetacao/uploadDocumentos', dados);

				if (response) {
					Swal.fire({
						position: 'center',
						type: 'success',
						title: 'Salvo!',
						text: 'Dados inseridos com sucesso.',
						showConfirmButton: true,
					});
				}

				if (this.$refs.destinacaoDocs) this.$refs.destinacaoDocs.value = '';
				if (this.$refs.destinacaoDocs) this.getDocumentos(key, codigo);
			},
			excluirArquivo(file) {
				Swal.fire({
					title: 'Deseja remover o Arquivo?',
					text: 'Está ação não pode ser desfeita.',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#28a745',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Salvar',
					cancelButtonText: 'Cancelar',
				}).then(async (result) => {
					if (result.value) {
						var response = await vmGlobal.deleteFromAPI({
							table: 'supressaoPilhasDocs',
							where: {
								CodigoSupressaoDocumento: file.CodigoSupressaoDocumento,
							}
						});

						if (response) {
							await vmGlobal.deleteFile(file.Arquivo);
							this.getDocumentos('CodigoDestinacaoPilhas', file.CodigoDestinacaoPilhas);



						}
					}
				});

			},
			formatarData(data) {
				if (data) {
					return moment(data).format('DD/MM/YYYY');
				}

				return '';
			},
			cancelarDestinacao() {
				this.$nextTick(() => {
					$('table#tblAnexos').DataTable().destroy();
				});

				this.novaDesitnacaoPilha = false;
				this.destinacaoFotos = [];
			},
			closeAll() {
				this.$nextTick(() => {
					$('table#tblAnexos').DataTable().destroy();
				});

				this.showFormNovaPilha = false;
				this.showSelectedPilha = false;
			}
		},
		async mounted() {
			this.getConfiguracaoLicencas();
			this.constroiTable('#tblPilhas');
			this.getPilhas();
			await vmRelatorioAtividade.closeButtons()
		},
	});
