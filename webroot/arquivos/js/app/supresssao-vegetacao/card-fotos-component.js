const CardFotos = {
	template: '#template-card-fotos',
	props: {
		foto: {
			type: Object,
			required: true
		},
		disabled: {
			type: Boolean,
			default: false
		},
	},
	methods: {
		async salvarFoto() {
			var foto = Object.assign({}, this.foto);
			delete foto.CodigoSupresssaoFotos;

			var response = await vmGlobal.updateFromAPI({
				data: foto,
				table: 'supressaoPilhasFotos',
				where: {
					CodigoFotoSupervisao: this.foto.CodigoFotoSupervisao
				}
			});

			if (response) {
				setTimeout(() => {
					this.getFoto()
				}, 2000);
			}
		},
		async getFoto() {
			var foto = await vmGlobal.getFromAPI({
				table: 'supressaoPilhasFotos',
				where: {
					CodigoSupresssaoFotos: this.foto.CodigoSupresssaoFotos
				}
			});

			this.foto = foto[0];
		},
		excluirFoto() {
			Swal.fire({
				title: 'Deseja remover a foto?',
				text: 'Está ação não pode ser desfeita.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				if (result.value) {
					var response = await vmGlobal.deleteFromAPI({
						table: 'supressaoPilhasFotos',
						where: {
							CodigoSupresssaoFotos: this.foto.CodigoSupresssaoFotos,
						}
					});

					if (response) {
						await vmGlobal.deleteFile(this.foto.Arquivo);
						this.$emit('excluir-foto');
					}
				}
			});

		},
		formatDataFoto(data) {
			if (data) {
				return moment(data).format('DD/MM/YYYY HH:mm:ss');
			}

			return '';
		}
	},
	mounted() {},
};
