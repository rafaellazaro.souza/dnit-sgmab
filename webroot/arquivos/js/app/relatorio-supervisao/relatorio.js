const relatorioSupervisao = new Vue({
	el: '#relatorio-supervisao',
	data() {
		return {
			disabled: false,
			CodigoRelatorio: '',
			introducao: {
				CodigoRelatorioIntroducao: '',
				CodigoRelatorio: '',
				Introducao: '',
			},
			discussoes: {
				CodigoRelatorioDiscussoes: '',
				CodigoRelatorio: '',
				ResultadosDiscussoes: '',
				Conclusoes: '',
				DataCriacao: ''
			},
			newParecer: {
				CodigoSituacaoParecer: '',
				CodigoRelatorio: '',
				CodigoUsuario: '',
				Parecer: '',
				DataCorrecao: '',
				AntigaDataCorrecao: ''
			},
			arquivos: [],
			relatorio: {
				Ano: '',
				Mes: 1,
				PrazoEnvio: '',
				DataEnvio: '',
				EnvioValidacao: 0,
				usuarioEnvio: '',
				CodigoSituacao: 1,
				DataCorrecao: ''
			},
			contrato: {
				Numero: ''
			},
			parecer: [],
			diarios: [],
			ocorrencias: [],
			usuario: {
				CodigoUsuario: '',
				NomeUsuario: '',
				Perfil: ''
			},
			meses: [
				'Janeiro',
				'Fevereiro',
				'Março',
				'Abril',
				'Maio',
				'Junho',
				'Julho',
				'Agosto',
				'Setembro',
				'Outubro',
				'Novembro',
				'Dezembro'
			],
			programasPac: [],
			showInputPrazoCorrecao: false,
			showParecer: false,
			selectedParecer: {
				CodigoSituacaoParecer: '',
				Parecer: '',
				DataParecer: '',
				AntigaDataCorrecao: '',
				DataCorrecao: '',
				NomeUsuario: '',
				Situacao: ''
			}
		}
	},
	computed: {
		peridoRefencia() {
			return this.meses[this.relatorio.Mes - 1] + ' de ' + this.relatorio.Ano;
		},
		isFiscal() {
			return this.usuario.Perfil.includes('Fiscal');
		},
		isEmptyProgramasPac() {
			return this.programasPac == 0;
		},
		dataEnvio() {
			if (this.relatorio.DataEnvio != null && this.relatorio.DataEnvio.length != 0) {
				return moment(this.relatorio.DataEnvio).format('DD/MM/YYYY');
			}

			return '';
		},
		horaEnvio() {
			if (this.relatorio.DataEnvio != null && this.relatorio.DataEnvio.length != 0) {
				return moment(this.relatorio.DataEnvio).format('hh:mm:ss');
			}

			return '';
		},
		classStatus() {
			if (this.relatorio.CodigoSituacao == 1) {
				return 'badge-info';
			} else if (this.relatorio.CodigoSituacao == 2) {
				return 'badge-warning';
			} else if (this.relatorio.CodigoSituacao == 5) {
				return 'badge-success';
			} else if (this.relatorio.CodigoSituacao == 6) {
				return 'badge-danger';
			} else if (this.relatorio.CodigoSituacao == 7) {
				return 'btn-rejeitado';
			}
			return ''
		},
		isEmValidacao() {
			return this.relatorio.EnvioValidacao == 1;
		},
		isConcluido() {
			return this.relatorio.CodigoSituacao == 5;
		},
		situacaoParecer() {
			if (this.newParecer.CodigoSituacaoParecer == 2) {
				return 6;
			} else if (this.newParecer.CodigoSituacaoParecer == 3) {
				return 7;
			}

			return 5;
		},
		isEmptyParecer() {
			return this.newParecer.Parecer.length == 0;
		},
		isEmptyDataCorrecao() {
			return (this.newParecer.DataCorrecao === null || this.newParecer.DataCorrecao.length == 0);
		},
		dataCorrecao() {
			if (this.relatorio.DataCorrecao !== null && this.relatorio.DataCorrecao.length != 0) {
				return moment(this.relatorio.DataCorrecao).format('DD/MM/YYYY');
			}

			return '';
		},
		classParecer() {
			if (this.selectedParecer.CodigoSituacaoParecer == 4) {
				return 'badge-success';
			} else if (this.selectedParecer.CodigoSituacaoParecer == 2) {
				return 'badge-danger';
			} else if (this.selectedParecer.CodigoSituacaoParecer == 3) {
				return 'btn-rejeitado';
			}
			return ''
		}
	},
	watch: {
		CodigoRelatorio(val) {
			this.introducao.CodigoRelatorio = val;
			this.discussoes.CodigoRelatorio = val;
			this.newParecer.CodigoRelatorio = val;
			this.getRelatorio();
		},
		'usuario.CodigoUsuario'(val) {
			this.newParecer.CodigoUsuario = val;
		},
		'usuario.Perfil'(val) {
			if (val.includes('Fiscal')) {
				this.disabledFields();

				if (!this.isEmValidacao) return;
				this.$nextTick(() => {
					CKEDITOR.replace('textParecer');

					CKEDITOR.instances['textParecer'].on("change", (evt) => {
						this.newParecer.Parecer = String(btoa(evt.editor.getData()));
					});
				});
			}
		},
		'relatorio.EnvioValidacao'(val) {
			if (val == 1) {
				this.disabledFields();
			}
		},
		'relatorio.CodigoSituacao'(val) {
			if (val == 5) {
				this.disabledFields();
			}
		}
	},
	methods: {
		getDiario(codigo) {
			var url = base_url + 'DiarioAmbiental/update/' + vmGlobal.codificarDecodificarParametroUrl(codigo, 'encode')
			window.open(url, '_blank');
		},
		getOcorrencia(codigo) {
			var url = base_url + 'Ocorrencia/update/' + vmGlobal.codificarDecodificarParametroUrl(codigo, 'encode')
			window.open(url, '_blank');
		},
		getParamUrl() {
			var url = window.location.href;
			url = url.split('/').reverse();
			this.CodigoRelatorio = vmGlobal.codificarDecodificarParametroUrl(url[0], 'decode');
		},
		async getRelatorio() {
			await this.initEditor();
			if (this.CodigoRelatorio.length == 0) return;
			await axios.post(base_url + 'RelatoriosSupervisao/getAllFromRelatorio', $.param({
				CodigoRelatorio: this.CodigoRelatorio
			})).then(async (response) => {
				this.contrato = response.data.contrato[0];
				this.introducao = (response.data.introducao != 0) ? response.data.introducao[0] : this.introducao;
				this.discussoes = (response.data.discussoes.length != 0) ? response.data.discussoes[0] : this.discussoes;
				this.arquivos = response.data.arquivos;
				this.parecer = response.data.parecer;
				this.diarios = response.data.diarios;
				this.ocorrencias = response.data.ocorrencias;
				this.usuario = response.data.usuario;
				this.programasPac = response.data.programas;

				setTimeout(() => {
					CKEDITOR.instances['textIntroducao'].setData(String(atob(this.introducao.Introducao)));
					CKEDITOR.instances['textDiscussoes'].setData(String(atob(this.discussoes.ResultadosDiscussoes)));
					CKEDITOR.instances['textConclusoes'].setData(String(atob(this.discussoes.Conclusoes)));
				}, 2000);

				this.refreshTable('#tblRelatorioAnexos');
				this.refreshTable('#tblRelatorioDiarios', 10);
				this.refreshTable('#tblRelatorioOcorrencias', 10);
				this.refreshTable('#tblParecer', 10);

				this.relatorio = response.data.relatorio[0];
				await this.initEditorPac();
			});
		},
		async salvarIntroducao() {
			var introducao = Object.assign({}, this.introducao);
			delete introducao.DataCriacao;
			delete introducao.CodigoRelatorioIntroducao;

			if (this.introducao.CodigoRelatorioIntroducao.length == 0) {
				await vmGlobal.insertFromAPI({
					table: 'relatorioIntroducao',
					data: introducao
				});

				await this.getIntroducao();
				return;
			}

			await vmGlobal.updateFromAPI({
				table: 'relatorioIntroducao',
				data: {
					Introducao: this.introducao.Introducao
				},
				where: {
					CodigoRelatorioIntroducao: this.introducao.CodigoRelatorioIntroducao
				}
			});
			await this.getIntroducao();
		},
		async salvarDiscussoesConclusoes() {
			var discussoes = Object.assign({}, this.discussoes);
			delete discussoes.DataCriacao;
			delete discussoes.CodigoRelatorioDiscussoes

			if (this.discussoes.CodigoRelatorioDiscussoes.length == 0) {
				await vmGlobal.insertFromAPI({
					table: 'relatorioDiscussoes',
					data: discussoes
				});

				await this.getDiscussoes();
				return;
			}

			await vmGlobal.updateFromAPI({
				table: 'relatorioDiscussoes',
				data: {
					ResultadosDiscussoes: this.discussoes.ResultadosDiscussoes,
					Conclusoes: this.discussoes.Conclusoes
				},
				where: {
					CodigoRelatorioDiscussoes: this.discussoes.CodigoRelatorioDiscussoes
				}
			});

			await this.getDiscussoes();
		},
		async salvarParecer(val) {
			this.newParecer.CodigoSituacaoParecer = val;
			this.newParecer.AntigaDataCorrecao = this.relatorio.DataCorrecao;

			if ((val == 3 && this.isEmptyParecer) ||
				(val == 3 && this.isEmptyDataCorrecao) ||
				(val == 3 && this.isEmptyDataCorrecao)) {
				this.showInputPrazoCorrecao = true;
				return
			}

			this.showInputPrazoCorrecao = (val != 3) ? false : true;
			this.newParecer.DataCorrecao = (val != 3) ? null : this.newParecer.DataCorrecao;

			Swal.fire({
				title: 'Salvar parecer?',
				text: 'Esta ação não poderá ser desfeita.',
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#04be5b',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				if (result.value) {
					await vmGlobal.insertFromAPI({
						table: 'relatorioParecer',
						data: this.newParecer
					});

					await vmGlobal.updateFromAPI({
						table: 'relatorioSupervisao',
						data: {
							CodigoSituacao: this.situacaoParecer,
							DataCorrecao: this.newParecer.DataCorrecao,
							EnvioValidacao: 0
						},
						where: {
							CodigoRelatorio: this.CodigoRelatorio
						}
					});

					window.location.reload();
				}
			})
		},
		async getIntroducao() {
			var introducao = await vmGlobal.getFromAPI({
				table: 'relatorioIntroducao',
				where: {
					CodigoRelatorio: this.CodigoRelatorio
				}
			});

			if (introducao.length != 0) {
				this.introducao = introducao[0];
				CKEDITOR.instances['textIntroducao'].setData(String(atob(this.introducao.Introducao)));
			}
		},
		async getDiscussoes() {
			var discussoes = await vmGlobal.getFromAPI({
				table: 'relatorioDiscussoes',
				where: {
					CodigoRelatorio: this.CodigoRelatorio
				}
			});

			if (discussoes.length != 0) {
				this.discussoes = discussoes[0];
				CKEDITOR.instances['textDiscussoes'].setData(String(atob(this.discussoes.ResultadosDiscussoes)));
				CKEDITOR.instances['textConclusoes'].setData(String(atob(this.discussoes.Conclusoes)));
			}
		},
		formatarData(data) {
			if (data) {
				var date = data.split(' ');
				if (date.length == 0) return '';
				date[0] = date[0].split('-').reverse().join('/');
				return date[0];
			}
			return '';
		},
		async initEditor() {
			await this.$nextTick(() => {
				$('textarea.editor').each(function () {
					CKEDITOR.replace(this.id);
				});

				CKEDITOR.instances['textIntroducao'].on("change", (evt) => {
					this.introducao.Introducao = String(btoa(evt.editor.getData()));
				});

				CKEDITOR.instances['textDiscussoes'].on("change", (evt) => {
					this.discussoes.ResultadosDiscussoes = String(btoa(evt.editor.getData()));
				});

				CKEDITOR.instances['textConclusoes'].on("change", (evt) => {
					this.discussoes.Conclusoes = String(btoa(evt.editor.getData()));
				});
			});

		},
		async getArquivos() {
			var arquivos = await vmGlobal.getFromAPI({
				table: 'relatorioAnexos',
				where: {
					CodigoRelatorio: this.CodigoRelatorio
				}
			});

			this.arquivos = arquivos;
			await this.refreshTable('#tblRelatorioAnexos');
		},
		async importarDocumentos() {
			var anexos = this.$refs.relatorioAnexos.files;

			if (anexos.length == 0) {
				return;
			}

			dados = this.geraFormDataInputMultifile(anexos, 'anexos');
			dados.append('CodigoRelatorio', this.CodigoRelatorio);

			var response = await this.sendFiles(base_url + 'RelatoriosSupervisao/uploadDocumentos', dados);

			if (response) {
				this.$refs.relatorioAnexos.value = '';
				await this.getArquivos();
				$(this.$refs.relatorioAnexos).next().trigger('click');
			}
		},
		geraFormDataInputMultifile(files, fieldName) {
			var formData = new FormData();
			$.each(files, (index, file) => {
				formData.append(fieldName + '[' + index + ']', file);
			});
			return formData;
		},
		async sendFiles(url, dados) {
			return await axios.post(url, dados, {
					headers: {
						'Content-Type': 'multipart/form-data'
					},
				})
				.then(response => {
					if (response.data.erro != 0) return false;
					return true;
				})
				.catch(error => {
					return false;
				});
		},
		constroiTable(id, pageLength = 5) {
			$(id, this.$el).DataTable({
				language: translateDataTable,
				pageLength: pageLength,
				lengthChange: false,
				destroy: true
			});
		},
		refreshTable(id, pageLength = 5) {
			$(id, this.$el).DataTable().destroy();
			this.$nextTick(() => {
				this.constroiTable(id, pageLength);
			});
		},
		excluirArquivo(file) {
			Swal.fire({
				title: 'Deseja remover o Arquivo?',
				text: 'Está ação não pode ser desfeita.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				if (result.value) {
					var response = await vmGlobal.deleteFromAPI({
						table: 'relatorioAnexos',
						where: {
							CodigoRelatorioAnexos: file.CodigoRelatorioAnexos,
						}
					});

					if (response) {
						await vmGlobal.deleteFile(file.Arquivo);
						this.getArquivos();
					}
				}
			});

		},
		getStatus(codigo) {
			var status = [
				'Em Andamento',
				'Em Validação',
				'Não Realizado',
				'Cancelado',
				'Concluído',
				'Rejeitado',
				'Parcialmente Aprovado'
			];

			return status[codigo - 1];
		},
		calculaPrazoEnvio() {
			var day = moment().format('DD');
			while (!moment(this.relatorio.Ano + '-' + this.relatorio.Mes + '-' + day, 'YYYY-MM-DD').isValid()) {
				day -= 1;
			}

			return moment(this.relatorio.Ano + '-' + this.relatorio.Mes + '-' + day, 'YYYY-MM-DD').endOf('month').businessAdd(7).format('YYYY-MM-DD');
		},
		enviarParaValidacao() {
			Swal.fire({
				title: 'Enviar para validação?',
				text: 'Está ação não pode ser desfeita.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				if (result.value) {
					await vmGlobal.updateFromAPI({
						table: 'relatorioSupervisao',
						where: {
							CodigoRelatorio: this.CodigoRelatorio,
						},
						data: {
							CodigoSituacao: 2,
							EnvioValidacao: 1,
							DataEnvio: moment().format('YYYY-MM-DD hh:mm:ss'),
							CodigoUsuarioEnvio: this.usuario.CodigoUsuario
						}
					});

					window.location.reload();
				}
			});
		},
		async disabledFields() {
			await this.$nextTick();

			$.each(CKEDITOR.instances, function () {
				this.readOnly = true;
			});

			$(':input[type=button]', this.$el).not('.not-hide').attr('disabled', true).css({
				display: 'none'
			});

			$(':input[type=file]', this.$el).attr('disabled', true);

			this.disabled = true;
		},
		async initEditorPac() {
			if (this.programasPac.length != 0) {
				for (prog in this.programasPac) {
					var id = 'txtProgramasPac' + prog;
					await this.$nextTick();

					CKEDITOR.replace(id);

					CKEDITOR.instances[id].on("change", (evt) => {
						this.programasPac[prog].AvaliacaoProgramas = evt.editor.getData();
					});

					CKEDITOR.instances[id].setData(this.programasPac[prog].AvaliacaoProgramas);
				}

				if (this.isFiscal || this.isEmValidacao || this.isConcluido) {
					await this.$nextTick();
					$('textarea.editorPAC').each(function () {
						CKEDITOR.instances[this.id].config.readOnly = true;
					});
				}
			}
		},
		async salvarProgramasPac() {
			var insert = this.programasPac.filter((prog) => {
				return prog.CodigoRelatorioProgramas == null || prog.CodigoRelatorioProgramas.length == 0;
			}).map((el) => {
				return {
					CodigoRelatorio: this.CodigoRelatorio,
					CodigoSubtipo: el.CodigoSubtipo,
					AvaliacaoProgramas: el.AvaliacaoProgramas
				}
			});

			var update = this.programasPac.filter((prog) => {
				return prog.CodigoRelatorioProgramas !== null && prog.CodigoRelatorioProgramas.length != 0;
			}).map((el) => {
				return {
					CodigoRelatorioProgramas: el.CodigoRelatorioProgramas,
					AvaliacaoProgramas: el.AvaliacaoProgramas
				}
			});

			await axios.post(base_url + 'RelatoriosSupervisao/saveProgramas', $.param({
				update: update,
				insert: insert
			})).then(async (response) => {
				if (!response.data.update || !response.data.insert) {
					Swal.fire({
						type: 'error',
						title: 'Oops...',
						text: 'Erro ao salvar os programas.',
					});
				}

				await this.getProgramas();
			});
		},
		async getProgramas() {
			await axios.post(base_url + 'RelatoriosSupervisao/getProgramas', $.param({
				CodigoRelatorio: this.CodigoRelatorio
			})).then(async (response) => {
				this.programasPac = response.data.programas;
				for (prog in this.programasPac) {
					var id = 'txtProgramasPac' + prog;
					await this.$nextTick(function () {
						if (CKEDITOR.instances[id]) {
							CKEDITOR.instances[id].setData(this.programasPac[prog].AvaliacaoProgramas);
						}
					});
				}
			});
		},
		loadParecer(val) {
			this.showParecer = true;

			this.selectedParecer.NomeUsuario = val.NomeUsuario
			this.selectedParecer.DataParecer = moment(val.DataParecer).format('DD/MM/YYYY');
			this.selectedParecer.AntigaDataCorrecao = (val.AntigaDataCorrecao !== null) ? moment(val.AntigaDataCorrecao).format('DD/MM/YYYY') : '-';
			this.selectedParecer.DataCorrecao = (val.DataCorrecao !== null) ? moment(val.DataCorrecao).format('DD/MM/YYYY') : '-';
			this.selectedParecer.Situacao = val.Situacao;
			this.selectedParecer.CodigoSituacaoParecer = val.CodigoSituacaoParecer;

			if (CKEDITOR.instances['txtParecerTecnicoView']) CKEDITOR.instances['txtParecerTecnicoView'].destroy();
			this.$nextTick(() => {
				CKEDITOR.replace('txtParecerTecnicoView', {
					height: '400px',
					readOnly: true
				});
			});

			this.$nextTick(() => {
				CKEDITOR.instances['txtParecerTecnicoView'].setData(String(atob(val.Parecer)));
			})
		},
		styleParecer(val) {
			if (val.CodigoSituacaoParecer == 4) {
				return 'badge-success';
			} else if (val.CodigoSituacaoParecer == 2) {
				return 'badge-danger';
			} else if (val.CodigoSituacaoParecer == 3) {
				return 'btn-rejeitado';
			}
			return '';
		}
	},
	async mounted() {
		$('.dropify').dropify({
			messages: {
				'default': 'Clique ou arraste arquivos aqui.',
				'replace': 'Clique ou arraste arquivos para substituir.',
				'remove': 'Remover',
				'error': 'Ooops, algo de errado aconteceu.'
			}
		});

		await this.getParamUrl();
	},
});
