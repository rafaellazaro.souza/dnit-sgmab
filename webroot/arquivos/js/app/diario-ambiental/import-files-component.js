const ImportFiles = {
	template: '#import-files-template',
	props: {
		codigoValue: {
			type: Number,
			required: true
		},
		codigoKey: {
			type: String,
			required: true
		}
	},
	methods: {
		importarFotos() {
			var fotos = this.$refs.diarioFotos.files;

			if (fotos.length == 0) {
				return;
			}

			dados = this.geraFormDataInputMultifile(fotos, 'fotos');
			dados.append(this.codigoKey, this.codigoValue);

			var response = this.sendFiles(base_url + 'DiarioAmbiental/uploadFotos', dados);

			if (response) {
				setTimeout(() => {
					this.$emit('buscar-arquivos');
				}, 2000);
				this.$refs.diarioFotos.value = '';
			}
		},
		importarDocumentos() {
			var anexos = this.$refs.diarioDocs.files;

			if (anexos.length == 0) {
				return;
			}

			dados = this.geraFormDataInputMultifile(anexos, 'docs');
			dados.append(this.codigoKey, this.codigoValue);

			var response = this.sendFiles(base_url + 'DiarioAmbiental/uploadDocumentos', dados);

			if (response) {
				setTimeout(() => {
					this.$emit('buscar-arquivos');
				}, 2000);
				this.$refs.diarioDocs.value = '';
			}
		},
		geraFormDataInputMultifile(files, fieldName) {
			var formData = new FormData();
			$.each(files, (index, file) => {
				formData.append(fieldName + '[' + index + ']', file);
			});
			return formData;
		},
		async sendFiles(url, dados) {
			return await axios.post(url, dados, {
					headers: {
						'Content-Type': 'multipart/form-data'
					},
				})
				.then(response => {
					if (response.data.erro != 0) return false;
					return true;
				})
				.catch(error => {
					return false;
				});
		},
	},
}
