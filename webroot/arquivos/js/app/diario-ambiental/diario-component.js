const DiarioCompenent = {
	template: '#diario-lista-template',
	data() {
		return {
			CodigoEscopo: '',
			diarios: []
		}
	},
	created() {
		var escopo = this.$route.params.codigoEscopo;
		this.CodigoEscopo = vmGlobal.codificarDecodificarParametroUrl(escopo, 'decode')
	},
	methods: {
		novoRegistro() {
			this.$router.push({
				path: '/DiarioAmbiental/novo/' + this.$route.params.codigoEscopo
			});
		},
		async getDiarios() {
			await axios.post(base_url + 'DiarioAmbiental/getDiarios', $.param({
					CodigoEscopo: this.CodigoEscopo
				}))
				.then(response => {
					this.diarios = response.data.diarios;
				})
		},
		editarDiario(diario) {
			this.verificaDiario(diario.CodigoDiario);
			this.$router.push({
				path: '/DiarioAmbiental/update/' + vmGlobal.codificarDecodificarParametroUrl(diario.CodigoDiario, 'encode')
			});
		},
		verificaDiario(diario) {
			axios.post(base_url + '/DiarioAmbiental/verificaOcorrencias', $.param({
				CodigoDiario: diario
			}));
		}
	},
	mounted() {
		this.getDiarios();
	},
};

const DiarioFormComponent = {
	components: {
		CardFotos: CardFotos,
		TableArquivos: TableArquivos,
		ImportFiles: ImportFiles,
		VSelect: VueSelect.VueSelect
	},
	template: '#diario-form-template',
	data() {
		return {
			diarioDisabled: false,
			previousRoute: '',
			userPerfil: '',
			ocorrencias: [],
			fotos: [],
			ufs: [],
			arquivos: [],
			importFile: {
				codigoKey: 'CodigoDiario',
				codigoValue: ''
			},
			CodigoDiario: '',
			modal: [
				'Rodoviário',
				'Ferroviário',
				'Aquaviário'
			],
			brsContrato: [],
			selectedBr: [],
			registroDa: {
				CodigoEscopo: '',
				UF: '',
				Modal: '',
				BR: '',
				FER: '',
				AQV: '',
				DataCadastro: '',
				Descricao: '',
			},
			modalContrato: null
		}
	},
	watch: {
		$route(to, from) {
			this.previousRoute = from.path;

			if (from.path.includes('DiarioAmbiental/novo/')) {
				this.messageInitial();
			}

			if (to.path.includes('DiarioAmbiental/update/')) {
				this.filtraURL();
				this.$forceUpdate();
			}
		},
		userPerfil() {
			this.disableFormFields();
		},
		'registroDa.Modal'(val) {
			if (this.isRodoviario) {
				this.registroDa.FER = '';
				this.registroDa.AQV = '';
				return
			}

			if (this.isFerrovia) {
				this.registroDa.BR = '';
				this.registroDa.AQV = '';
				return;
			}

			if (this.isAquaviario) {
				this.registroDa.BR = '';
				this.registroDa.FER = '';
				return;
			}
		},
		selectedBr(val) {
			if (val.length == 0) {
				this.registroDa.BR = '';
				return;
			}

			this.registroDa.BR = val.join(',');
		},
	},
	computed: {
		isEmptyDiario() {
			return this.CodigoDiario.length == 0;
		},
		isFerrovia() {
			return this.registroDa.Modal.includes('Ferroviário');
		},
		isAquaviario() {
			return this.registroDa.Modal.includes('Aquaviário');
		},
		isRodoviario() {
			return this.registroDa.Modal.includes('Rodoviário');
		},
		isSelected() {
			return this.selectedBr.length != 0;
		},
		toDayDate() {
			return moment().format('YYYY-MM-DD');
		}
	},
	async created() {
		this.getPerfil();
		this.filtraURL();

		await vmGlobal.getFromAPI({
			table: 'BaseUF'
		}).then(response => {
			this.ufs = response;
		});

		var previusUrl = document.referrer;
		if (previusUrl.includes('RelatoriosSupervisao/relatorio')) {
			this.disableFormFields(true);
		}
	},
	methods: {
		async filtraURL() {
			var url = this.$route.path;
			var param = this.$route.params.codigo;
			param = vmGlobal.codificarDecodificarParametroUrl(param, 'decode');

			if (url.includes('novo')) {
				this.registroDa.CodigoEscopo = param;
				this.getModalContrato();
				return;
			}

			this.CodigoDiario = param;
			this.arquivos.CodigoDiario = this.CodigoDiario;
			this.importFile.codigoValue = parseInt(this.CodigoDiario);
			await this.getDiario();
			await this.getModalContrato();
		},
		novaOcorrencia() {
			if (this.CodigoDiario.length == 0) {
				return;
			}

			this.$router.push({
				path: '/Ocorrencia/nova/' + vmGlobal.codificarDecodificarParametroUrl(this.CodigoDiario, 'encode')
			});
		},
		async getDiario() {
			var diario = await vmGlobal.getFromAPI({
				table: 'diarioAmbiental',
				where: {
					CodigoDiario: this.CodigoDiario
				}
			});

			if (diario.length != 0) {
				this.registroDa = diario[0];
				this.registroDa.DataCadastro = this.tratarDataDataHora(diario[0].DataCadastro);
				if (this.isRodoviario) {
					this.selectedBr = this.registroDa.BR.split(',');
				}
			}
		},
		salvarDadosDA() {
			Swal.fire({
				title: 'Deseja salvar os dados?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				if (result.value) {
					if (this.CodigoDiario.length == 0) {
						this.novoDa();
						return;
					}

					this.updateDa();
				}
			});
		},
		async novoDa() {
			var response = await vmGlobal.insertFromApiAndReturnId(true, {
				table: 'diarioAmbiental',
				data: this.registroDa
			});

			if (response === false) return;

			this.$router.push({
				path: '/DiarioAmbiental/update/' + vmGlobal.codificarDecodificarParametroUrl(response, 'encode')
			});

			//window.location.href = base_url + 'DiarioAmbiental/update/' + vmGlobal.codificarDecodificarParametroUrl(response, 'encode');
		},
		async updateDa() {
			var dados = Object.assign({}, this.registroDa);
			delete dados.CodigoDiario;

			await vmGlobal.updateFromAPI({
				table: 'diarioAmbiental',
				data: dados,
				where: {
					CodigoDiario: this.CodigoDiario
				}
			});

			var possuiOcorrencia = await this.possuiOcorrencias();

			if (possuiOcorrencia) {
				this.novaOcorrencia();
				return;
			}

			setTimeout(() => {
				window.location.reload();
			}, 2000);
		},
		tratarDataDataHora(data) {
			var data = data.replace(/\s\d{2}\:\d{2}\:\d{2}\..*/g, '');
			return data;
		},
		async getOcorrencias() {
			if (this.CodigoDiario.length == 0) return;
			await axios.post(base_url + 'DiarioAmbiental/getOcorrenciasDiario', $.param({
				CodigoDiario: this.CodigoDiario
			})).then(response => {
				this.ocorrencias = response.data.ocorrencias;
				this.refreshTable();
			});
		},
		clickOcorrenciaTab() {
			if (window.location.hash.includes('#ocorrencias') && this.CodigoDiario.length != 0) {
				$(this.$refs.tabOcorrencia).trigger('click');
			}
		},
		constroiTable(id = "") {
			$('table' + id, this.$el).DataTable({
				language: translateDataTable,
				lengthChange: false,
				destroy: true
			});
		},
		async refreshTable(id = "") {
			$('table' + id, this.$el).DataTable().destroy();
			await this.$nextTick(() => {
				this.constroiTable();
			});
		},
		formataData(data) {
			if (data == null || data.length == 0) {
				return '';
			}

			return vmGlobal.frontEndDateFormat(data);
		},
		editarOcorrencia(ocorrencia) {
			this.$router.push({
				path: '/Ocorrencia/update/' + vmGlobal.codificarDecodificarParametroUrl(ocorrencia.CodigoOcorrencia, 'encode')
			});
		},
		async cancelar() {
			var possuiOcorrencia = await this.possuiOcorrencias();

			if (possuiOcorrencia) {
				this.novaOcorrencia();
				return;
			}

			this.$router.push({
				path: '/DiarioAmbiental/diarioAmbientalLista/' + vmGlobal.codificarDecodificarParametroUrl(this.registroDa.CodigoEscopo, 'encode')
			});
		},
		async getArquivosAndFotos() {
			if (this.isEmptyDiario) return;
			await axios.post(base_url + 'DiarioAmbiental/getFotosEdocumentos', $.param({
				CodigoDiario: this.CodigoDiario
			})).then(response => {
				this.fotos = response.data.fotos;
				this.arquivos = response.data.arquivos;
				this.$emit('refresh-table', this.arquivos);
			});
		},
		disableFormFields(disabled = false) {
			if (this.userPerfil.includes('Fiscal') || disabled) {
				$('form :input', this.$el).attr('disabled', true);
				$('textarea', this.$el).attr('disabled', true);
				$('select', this.$el).attr('disabled', true);

				$('#tblListaDeArquivos tbody', this.$el).find(':input[type=button]').each(() => {
					$(this).attr('disabled', true).css({
						display: 'none',
					});
				});

				this.diarioDisabled = true;
			}

			$('#btnCancelarDiario', this.$el).attr('disabled', false);
		},
		async getPerfil() {
			await axios.get(base_url + 'login/getPerfil')
				.then((resp) => {
					this.userPerfil = resp.data.perfil;
				});
		},
		messageInitial() {
			Swal.fire({
				type: 'warning',
				title: 'Atenção',
				text: 'Por favor, insira as fotos e os  arquivos para finalizar o cadastro do diário.',
			})
		},
		async possuiOcorrencias() {
			var teste = false;
			if (this.previousRoute.includes('DiarioAmbiental/novo/')) {
				await Swal.fire({
					title: 'Este diario possui ocorrências?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#28a745',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sim',
					cancelButtonText: 'Não',
				}).then(async (result) => {
					teste = result.value;

					if (result.value) {
						this.novaOcorrencia();
					}
				});
			}

			return teste;
		},
		async getModalContrato() {
			if (this.registroDa.CodigoEscopo == 0) return;
			var modal = await vmGlobal.getFromAPI({
				table: 'modalContrato',
				where: {
					CodigoEscopo: this.registroDa.CodigoEscopo
				}
			});

			if (modal.length != 0) {
				this.modalContrato = modal[0];
				this.registroDa.UF = this.modalContrato.UF;
				this.registroDa.Modal = this.modalContrato.Modal;

				if (this.isRodoviario) {
					this.brsContrato = this.modalContrato.BR.split(',');
				}
			}
		}
	},
	mounted() {
		this.getArquivosAndFotos();
		this.getOcorrencias();
		this.constroiTable();
		this.clickOcorrenciaTab();

		if (!window.location.hash) {
			$('#diario-tab').click();
		}
	},
};
