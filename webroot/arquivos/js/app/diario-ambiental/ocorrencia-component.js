const InfoGerais = {
	template: '#ocorrencia-info-template',
	components: {
		CardFotos: CardFotos,
		TableArquivos: TableArquivos,
		ImportFiles: ImportFiles,
		VSelect: VueSelect.VueSelect
	},
	props: ['codigoOcorrencia', 'emValidacao', 'prazoSolucao'],
	data() {
		return {
			construtoras: [],
			infoSelect: {},
			arquivos: [],
			arquivosInfo: [],
			visitas: [],
			acoes: [],
			notificacoes: [],
			fotos: [],
			isFaseSelected: false,
			fase: {
				TipoOcorrencia: '',
				CodigoOcorrenciaFase: '',
				Status: ''
			},
			diario: {
				Modal: '',
				UF: '',
				BR: '',
				FiscalTecnico: '',
				NomeFiscal: ''
			},
			showFields: false,
			modalidade: [
				'Rodoviário',
				'Ferroviário',
				'Aquaviário'
			],
			info: {
				CodigoOcorrenciaInfoGerais: '',
				CodigoOcorrenciaFase: '',
				CodigoFiscal: '',
				Localidade: '',
				UF: '',
				BR: '',
				KmInicial: null,
				KmFinal: null,
				Estaca: null,
				Lado: '',
				ProgramasPac: '',
				DadosConstrutora: '',
				DescricaoOcorrencia: '',
				NormasAtividasAmbientaisReferencia: '',
				ObservacoesGerais: '',
				NomeFerrovia: '',
				DescricaoAquaviario: '',
				Lat: '',
				Long: ''
			},
			importFile: {
				codigoKey: 'CodigoOcorrenciaFase',
				codigoValue: 0
			},
			ufs: [],
			fases: [],
			selectedBr: [],
			brsContrato: [],
			fotosFase: [],
			arquivosFase: [],
			programasContrato: [],
			selectedProgramas: []
		}
	},
	watch: {
		'info.Modal'(val) {
			if (val.length != 0 && val != this.modalidade[2]) {
				this.showFields = true;
				return;
			}

			this.showFields = false;
		},
		'emValidacao'(val) {
			if (val == 1 || vmGlobal.userPerfil.includes('Fiscal')) {
				this.disableFormFields();
			}
		},
		selectedBr(val) {
			this.info.BR = val.join(',');
		},
		'diario.BR'(val) {
			this.brsContrato = val.split(',');
		},
		'fase.Status'(val) {
			if (val.includes('Concluído')) {
				this.disableFormFields();
			}
		},
		'diario.FiscalTecnico'(val) {
			this.info.CodigoFiscal = val;
		},
		selectedProgramas(val) {
			var programas = [];
			for (prog in val) {
				programas.push(val[prog].CodigoSubtipo);
			}
			this.info.ProgramasPac = programas.join(',');
		},
	},
	methods: {
		limparCampos() {
			this.info.KmInicial = '';
			this.info.KmFinal = '';
			this.info.Estaca = '';
			this.info.DescricaoAquaviario = '';
			this.info.NomeFerrovia = '';
		},
		async getArquivosAndFotos() {
			await axios.post(base_url + 'DiarioAmbiental/getFotosEdocumentos', $.param({
				CodigoOcorrencia: this.codigoOcorrencia
			})).then(response => {
				this.fotos = response.data.fotos;
				this.arquivos = response.data.arquivos;

				this.arquivosInfo = this.arquivos.filter((el) => {
					return el.CodigoOcorrenciaFase == this.info.CodigoOcorrenciaFase;
				});
			});
		},
		async getAllFromOcorrencia() {
			showLoading(true);
			await axios.post(base_url + 'DiarioAmbiental/getAllFromOcorrencia', $.param({
				CodigoOcorrencia: this.codigoOcorrencia
			})).then(response => {
				this.construtoras = response.data.construtoras;
				this.acoes = response.data.acoes;
				this.visitas = response.data.visitas;
				this.notificacoes = response.data.notificacao;
				this.diario = response.data.diario[0];
				this.fase = response.data.fase[0];
				this.importFile.codigoValue = parseInt(this.fase.CodigoOcorrenciaFase);
				this.programasContrato = response.data.programas;

				var fases = response.data.infos;
				if (fases.length != 0) {
					fases = fases.reverse();
					this.fases = (fases.length > 1) ? fases.slice(1, fases.length) : [];
					this.info = fases[0];
				}

				if (this.info.CodigoOcorrenciaFase != this.fase.CodigoOcorrenciaFase) {
					this.info.CodigoOcorrenciaFase = this.fase.CodigoOcorrenciaFase;
					this.info.CodigoOcorrenciaInfoGerais = '';
				}

				this.info.CodigoFiscal = (this.info.CodigoFiscal === null || this.info.CodigoFiscal.length == 0) ? response.data.fiscal : this.info.CodigoFiscal;
				this.info.UF = (this.info.UF === null || this.info.UF.length == 0) ? this.diario.UF : this.info.UF;
				this.selectedBr = (this.info.BR === null || this.info.BR.length == 0) ? [] : this.info.BR.split(',');

				this.populateProgramas();
				this.refreshTable('#tblNotificacao');
				this.refreshTable('#tblVisitas');
				this.refreshTable('#tblAcoes');
				showLoading();
			});
		},
		constroiTable(id = "") {
			$('table' + id, this.$el).DataTable({
				language: translateDataTable,
				lengthChange: false,
				destroy: true
			});
		},
		async refreshTable(id = "") {
			$('table' + id, this.$el).DataTable().destroy();
			await this.$nextTick(() => {
				this.constroiTable();
			});
		},
		async getAll() {
			await this.getAllFromOcorrencia();
			await this.getArquivosAndFotos();
		},
		novaAcao() {
			if (this.emValidacao == 1 && vmGlobal.userPerfil.includes('Contratada')) return;
			if (this.fase.Status.includes('Concluído')) return;

			if (this.isEmptyPrazoSolucao) {
				this.alertEmptyPrazoSolucao();
				return;
			}
			this.$router.push({
				path: '/AcaoCorretiva/nova/' + vmGlobal.codificarDecodificarParametroUrl(this.codigoOcorrencia, 'encode')
			});
		},
		novaVisita() {
			if (this.emValidacao == 1) return;
			if (vmGlobal.userPerfil.includes('Fiscal')) return;
			if (this.fase.Status.includes('Concluído')) return;


			if (this.isEmptyPrazoSolucao) {
				this.alertEmptyPrazoSolucao();
				return;
			}
			this.$router.push({
				path: '/HistoricoVisita/nova/' + vmGlobal.codificarDecodificarParametroUrl(this.codigoOcorrencia, 'encode')
			});
		},
		novaNotificacao() {
			if (this.emValidacao == 1) return;
			if (vmGlobal.userPerfil.includes('Fiscal')) return;
			if (this.fase.Status.includes('Concluído')) return;


			if (this.isEmptyPrazoSolucao) {
				this.alertEmptyPrazoSolucao();
				return;
			}
			this.$router.push({
				path: '/ComunicacaoConstrutora/nova/' + vmGlobal.codificarDecodificarParametroUrl(this.codigoOcorrencia, 'encode')
			});
		},
		openTabs() {
			var hash = window.location.hash;
			if (hash.length == 0) return;

			var tabInfo = document.getElementById('ocorrencias-info-tab');


			var refs = Object.keys(this.$refs);
			hash = hash.replace('#', '');

			if (refs.includes(hash)) {
				$(tabInfo).trigger('click')
				$(this.$refs[hash]).trigger('click');
			}
		},
		formataData(data) {
			if (data === null || data === undefined || data.trim().length == 0) {
				return '';
			}

			return vmGlobal.frontEndDateFormat(data);
		},
		getStatus(val) {
			if (val.length === 0) return '';
			var situcao = [
				'Em Andamento',
				'Em Validação',
				'Não Realizado',
				'Cancelado',
				'Concluído',
				'Rejeitado',
				'Parcialmente Aprovado',
			];

			return situcao[val - 1];
		},
		updateAcao(acao) {
			if (this.isEmptyPrazoSolucao) {
				this.alertEmptyPrazoSolucao();
				return;
			}

			this.$router.push({
				path: '/AcaoCorretiva/update/' + vmGlobal.codificarDecodificarParametroUrl(acao.CodigoAcaoCorretiva, 'encode')
			});
		},
		updateVisita(visita) {
			if (this.isEmptyPrazoSolucao) {
				this.alertEmptyPrazoSolucao();
				return;
			}

			this.$router.push({
				path: '/HistoricoVisita/update/' + vmGlobal.codificarDecodificarParametroUrl(visita.CodigoVisitas, 'encode')
			});
		},
		updateNotificacao(notificacao) {
			if (this.isEmptyPrazoSolucao) {
				this.alertEmptyPrazoSolucao();
				return;
			}
			this.$router.push({
				path: '/ComunicacaoConstrutora/update/' + vmGlobal.codificarDecodificarParametroUrl(notificacao.CodigoNotificacaoFiscal, 'encode')
			});
		},
		salvarInfoGerais() {
			if (this.emValidacao == 1) return;

			if (this.info.CodigoOcorrenciaInfoGerais.length == 0) {
				this.novaInfoGerais();
				return;
			}
			this.updateInfoGerais();
		},
		async novaInfoGerais() {
			var info = Object.assign({}, this.info);
			delete info.CodigoOcorrenciaInfoGerais;
			delete info.CodigoOcorrencia;
			delete info.TipoOcorrencia;

			var response = await vmGlobal.insertFromApiAndReturnId(true, {
				table: 'ocorrenciaInfoGerais',
				data: info
			});

			if (response == false) return;

			await this.getInfoGerais(response);
		},
		async updateInfoGerais() {
			var info = Object.assign({}, this.info);
			delete info.CodigoOcorrenciaInfoGerais;
			delete info.CodigoOcorrenciaFase;
			delete info.TipoOcorrencia;
			delete info.CodigoOcorrencia;

			var response = await vmGlobal.updateFromAPI({
				table: 'ocorrenciaInfoGerais',
				data: info,
				where: {
					CodigoOcorrenciaInfoGerais: this.info.CodigoOcorrenciaInfoGerais
				}
			});

			if (response) {
				await this.getInfoGerais(this.info.CodigoOcorrenciaInfoGerais);
			}
		},
		alertEmptyPrazoSolucao() {
			Swal.fire({
				title: 'Atenção',
				text: 'Informe o prazo de solução antes de tomar essa ação.',
				type: 'warning'
			})
		},
		disableFormFields() {
			$('form :input', this.$el).attr('disabled', true);
			$('textarea', this.$el).attr('disabled', true);
			$('select', this.$el).attr('disabled', true);

			$('form :input[type=button]', this.$el).each(() => {
				$(this).attr('disabled', true).css({
					display: 'none',
				});
			});
		},
		async getInfoGerais(id) {
			var info = await vmGlobal.getFromAPI({
				table: 'viewFaseInfo',
				where: {
					CodigoOcorrencia: this.codigoOcorrencia
				}
			});

			if (info.length != 0) {
				info = info.reverse();
				this.fases = (info.length > 1) ? info.slice(1, info.length) : [];
				this.info = info[0];
			}

			if (this.info.CodigoOcorrenciaFase != this.fase.CodigoOcorrenciaFase) {
				this.info.CodigoOcorrenciaFase = this.fase.CodigoOcorrenciaFase;
				this.info.CodigoOcorrenciaInfoGerais = '';
			}
		},
		selectFase(fase) {
			this.infoSelect = fase;
			this.isFaseSelected = true;

			this.fotosFase = this.fotos.filter(el => {
				return el.CodigoOcorrenciaFase = this.infoSelect.CodigoOcorrenciaFase;
			});

			this.arquivosFase = this.arquivos.filter(el => {
				return el.CodigoOcorrenciaFase == this.infoSelect.CodigoOcorrenciaFase;
			});
		},
		returnConstrutora(CodigoConstrutora) {
			var construtora = this.construtoras.filter((el) => {
				return el.CodigoContrutorasContratos == CodigoConstrutora;
			});

			return (construtora.length == 0) ? '' : construtora[0].fCNPJ + ' - ' + construtora[0].NomeEmpresa;
		},
		populateProgramas() {
			var programas = this.info.ProgramasPac.split(',');
			for (prog in this.programasContrato) {
				if (programas.includes(this.programasContrato[prog].CodigoSubtipo)) {
					this.selectedProgramas.push(this.programasContrato[prog]);
				}
			}
		}
	},
	computed: {
		isFerrovia() {
			return this.diario.Modal.includes('Ferroviário');
		},
		isAquaviario() {
			return this.diario.Modal.includes('Aquaviário');
		},
		isRodoviario() {
			return this.diario.Modal.includes('Rodoviário');
		},
		isEmptyPrazoSolucao() {
			return this.prazoSolucao == null || this.prazoSolucao.length == 0;
		},
		isEmValidacao() {
			return this.emValidacao == 1;
		},
		isSelected() {
			return this.selectedBr.length != 0;
		},
		isConcluido() {
			return this.fase.Status.includes('Concluído');
		}
	},
	async mounted() {
		this.constroiTable('#tblAcoes');
		this.constroiTable('#tblVisitas');
		this.constroiTable('#tblNotificacao');
		await this.getAll();
		this.openTabs();
	},
	async created() {
		await vmGlobal.getFromAPI({
			table: 'BaseUF'
		}).then(response => {
			this.ufs = response;
		});

		this.$parent.$on('get-acoes', (ocorrencia) => {
			var acoes = this.acoes.filter(el => {
				return ocorrencia.CodigoOcorrenciaFase == el.CodigoOcorrenciaFase && ![5, 4].includes(parseInt(el.CodigoStatus));
			});

			eventBus.$emit('acoes-concluidas', acoes);
		})

		this.$parent.$on('disable-fields', () => {
			this.disableFormFields();
		})
	}

}

const Ocorrencia = {
	template: '#ocorrencia-main-template',
	components: {
		InformacoesGerais: InfoGerais
	},
	data() {
		return {
			previousRoute: '',
			isDisabled: true,
			parecerTecnico: [],
			DtPrazoSolucao: '',
			CodigoOcorrencia: '',
			nivelRisco: [],
			tipoOcorrencia: [],
			status: [],
			fases: [],
			isFaseSelected: false,
			faseSeleted: {
				TipoOcorrencia: '',
				Descricao: '',
				Localizacao: '',
				dtPrazoSolucao: '',
				Status: ''
			},
			ocorrencia: {
				CodigoOcorrencia: '',
				CodigoDiario: '',
				CodigoRisco: 1,
				DataOcorrencia: '',
				DataConclusao: '',
				EviadoValidacao: 0
			},
			ocorrenciaFase: {
				CodigoOcorrenciaFase: '',
				CodigoTipoOcorrencia: 1,
				CodigoStatus: 1,
				Localizacao: '',
				DataRegistro: '',
				PrazoSolucao: '',
				Descricao: ''
			},
			newParecerTecnico: {
				CodigoParecerTecnico: '',
				CodigoFiscal: '',
				CodigoOcorrenciaFase: '',
				CodigoSituacaoParecer: '',
				DataParecer: '',
				NovoPrazoSolucao: '',
				AnitgoPrazo: '',
				Parecer: ''
			},
			parecerTipoOcorrencia: '',
			showInputPrazoCorrecao: false,
			showParecer: false,
			parecerSelected: '',
			parecer: {
				TipoOcorrencia: '',
				DataParecer: ''
			},
			acoesConcluidas: false,
		}
	},
	computed: {
		minimalDate() {
			if (this.CodigoOcorrencia.length != 0) {
				return '';
			}

			return this.todayDate;
		},
		todayDate() {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1; //January is 0!
			var yyyy = today.getFullYear();
			if (dd < 10) {
				dd = '0' + dd
			}
			if (mm < 10) {
				mm = '0' + mm
			}

			today = yyyy + '-' + mm + '-' + dd;

			return today;
		},
		isEmptyOcorrencia() {
			return this.CodigoOcorrencia.length == 0;
		},
		isNullPrazoSolucao() {
			return this.ocorrenciaFase.PrazoSolucao == null || this.ocorrenciaFase.PrazoSolucao.length == 0;
		},
		hasFases() {
			return this.fases.length != 0;
		},
		isEmValidacao() {
			return this.ocorrencia.EviadoValidacao == 1;
		},
		isContratada() {
			return vmGlobal.userPerfil.includes('Contratada');
		},
		isConcluido() {
			return this.ocorrenciaFase.CodigoStatus == 5;
		}
	},
	watch: {
		isEmValidacao: function (val) {
			console.log(val, 'Validação')
		},
		isContratada: function (val) {
			console.log(val, 'Contratda')
		},
		isEmptyOcorrencia: function (val) {
			console.log(val, 'Concluído')
		},

		$route(to, from) {
			this.previousRoute = from.path;

			if (from.path.includes('Ocorrencia/nova/')) {
				this.messageInitial();
			}

			if (to.path.includes('Ocorrencia/update/')) {
				this.filtraURL();
				this.$forceUpdate();
			}
		},
		'ocorrenciaFase.PrazoSolucao'(val) {
			this.DtPrazoSolucao = val;
		},
		'ocorrencia.EviadoValidacao'(val) {
			if (this.isEmValidacao && vmGlobal.userPerfil.includes('Fiscal')) {
				if (CKEDITOR.instances['txtParecerTecnicoView']) CKEDITOR.instances['txtParecerTecnicoView'].destroy();
				this.$nextTick(() => {
					CKEDITOR.replace('txtPparecerTecnico', {
						height: '400px',
					});
				});
			}

			if (val == 1 || this.ocorrenciaFase.CodigoStatus == 5) {
				this.disableFormFields();
				return;
			}

			if (val != 1 && vmGlobal.userPerfil.includes('Fiscal')) {
				this.disableFormFields();
				return;
			}
		},
		'ocorrenciaFase.CodigoStatus'(val) {
			if (this.ocorrencia.EviadoValidacao == 1 || val == 5) {
				this.disableFormFields();
			}
		},
		'ocorrencia.CodigoRisco'(val) {
			if (this.CodigoOcorrencia.length != 0) return;
			if (val == 3) {
				this.ocorrenciaFase.CodigoTipoOcorrencia = 4;
				return;
			}
			this.ocorrenciaFase.CodigoTipoOcorrencia = 1;
		},
		'ocorrenciaFase.CodigoOcorrenciaFase'() {
			this.parecerTipoOcorrencia = this.filtraTipoOcorrencia(this.ocorrenciaFase.CodigoTipoOcorrencia);
		},
		showInputPrazoCorrecao(val) {
			if (val) this.newParecerTecnico.NovoPrazoSolucao = '';
		},
	},
	created() {
		this.filtraURL();

		eventBus.$on('acoes-concluidas', (el) => {
			if (el.length == 0) {
				this.acoesConcluidas = true;
				return;
			}
		});

		var previusUrl = document.referrer;
		if (previusUrl.includes('RelatoriosSupervisao/relatorio')) {
			console.log('aqui');
			this.$emit('disable-fields');
			this.disableFormFields();
		}
	},
	methods: {
		async filtraURL() {
			await this.getAll();

			var url = this.$route.path;
			var param = this.$route.params.codigo;
			param = vmGlobal.codificarDecodificarParametroUrl(param, 'decode');

			if (url.includes('nova')) {
				this.ocorrencia.CodigoDiario = param;
				return;
			}

			this.CodigoOcorrencia = param;
			await this.getOcorrenciaAndFases();
		},
		salvarOcorrencia() {
			Swal.fire({
				title: '',
				text: 'Deseja relamente salvar os dados?',
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#04be5b',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				if (result.value) {
					if (this.isEmptyOcorrencia) {
						this.novaOcorrencia();
						return;
					}

					this.updateOcorrencia();
				}
			})
		},
		async novaOcorrencia() {
			var ocorrencia = Object.assign({}, this.ocorrencia);
			delete ocorrencia.CodigoOcorrencia;
			delete ocorrencia.DataOcorrencia;
			ocorrencia.DataConclusao = null;

			var fase = Object.assign({}, this.ocorrenciaFase);
			fase.PrazoSolucao = this.DtPrazoSolucao;
			delete fase.CodigoOcorrenciaFase;

			var response = await vmGlobal.insertFromApiAndReturnId(false, {
				table: 'ocorrenciaDiarioAmbiental',
				data: ocorrencia
			});

			if (response === false) return;

			fase.CodigoOcorrencia = response;
			response = await vmGlobal.insertFromApiAndReturnId(true, {
				table: 'ocorrenciaFase',
				data: fase
			});

			if (response === false) return;

			this.$router.push({
				path: '/Ocorrencia/update/' + vmGlobal.codificarDecodificarParametroUrl(fase.CodigoOcorrencia, 'encode')
			});
		},
		async updateOcorrencia() {
			var fase = Object.assign({}, this.ocorrenciaFase);
			delete fase.CodigoOcorrencia;
			delete fase.CodigoOcorrenciaFase;

			if (this.isNullPrazoSolucao) fase.PrazoSolucao = this.DtPrazoSolucao;


			var response = await vmGlobal.updateFromAPI({
				table: 'ocorrenciaFase',
				data: fase,
				where: {
					CodigoOcorrenciaFase: this.ocorrenciaFase.CodigoOcorrenciaFase
				}
			});

			if (response) {
				await this.getOcorrenciaAndFases();
			}
		},
		async getOcorrenciaAndFases() {
			showLoading(true);
			await axios.post(base_url + 'DiarioAmbiental/getOcorrenciaAndFases', $.param({
				CodigoOcorrencia: this.CodigoOcorrencia
			})).then(response => {
				this.ocorrencia = response.data.ocorrencia[0];
				this.parecerTecnico = response.data.parecer.reverse();

				var fases = response.data.fases;
				if (fases.length != 0) {
					fases = fases.reverse();
					this.fases = (fases.length > 1) ? fases.slice(1, fases.length) : [];
					this.ocorrenciaFase = fases[0];
				}

				showLoading();
				this.refreshTable('#tblParecer');
			});
		},
		formataData(data) {
			if (data == null || data.length == 0) {
				return '';
			}

			return vmGlobal.frontEndDateFormat(data);
		},
		filtraTipoOcorrencia(codigoTipo) {
			var tipo = this.tipoOcorrencia.filter((val) => {
				return val.CodigoTipoOcorrencia == codigoTipo;
			});

			return (tipo.length != 0) ? tipo[0].TipoOcorrencia : '';
		},
		filtraStatus(codigoStatus) {
			var status = this.status.filter((val) => {
				return val.CodigoStatus == codigoStatus;
			});

			return (status.length != 0) ? status[0].Status : '';
		},
		enviarParaValidacao() {
			if (this.isEmptyOcorrencia) return;
			if (this.isNullPrazoSolucao) return;
			if (this.ocorrencia.EviadoValidacao == 1) return;

			this.$emit('get-acoes', {
				CodigoOcorrenciaFase: this.ocorrenciaFase.CodigoOcorrenciaFase
			});

			this.$nextTick(() => {

				if (!this.acoesConcluidas) {
					Swal.fire({
						type: 'warning',
						title: 'Atenção',
						text: 'Por favor, conclua todas as ações corretivas antes de enviar.',
					});
					return;
				}

				Swal.fire({
					title: '',
					text: 'Deseja relamente salvar os dados?',
					type: 'question',
					showCancelButton: true,
					confirmButtonColor: '#04be5b',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Salvar',
					cancelButtonText: 'Cancelar',
				}).then(async (result) => {
					if (result.value) {
						var response = await vmGlobal.updateFromAPI({
							table: 'ocorrenciaDiarioAmbiental',
							data: {
								EviadoValidacao: 1,
							},
							where: {
								CodigoOcorrencia: this.CodigoOcorrencia
							}
						});

						if (this.ocorrenciaFase.length != 0) {
							await vmGlobal.updateFromAPI({
								table: 'ocorrenciaFase',
								data: {
									DataEnvioValidacao: this.todayDate,
									CodigoStatus: 2
								},
								where: {
									CodigoOcorrenciaFase: this.ocorrenciaFase.CodigoOcorrenciaFase
								}
							});
						}

						if (response) {
							await this.getOcorrenciaAndFases();
						}
					}
				});
			});


		},
		disableFormFields() {
			$('form :input', this.$el).attr('disabled', true);
			$('textarea', this.$el).attr('disabled', true);
			$('select', this.$el).attr('disabled', true);
			$('form :input[type=button]', this.$el).not('.dont-disabled').css({
				display: 'none'
			});
			$('form :input[type=submit]', this.$el).css({
				display: 'none'
			});

			$('#form-parecer :input', this.$el).not('.dont-enable').attr('disabled', false);

			$('#cancelButton', this.$el).attr('disabled', false).css({
				display: ''
			});
		},
		cancelar() {
			this.$router.push({
				path: '/DiarioAmbiental/update/' + vmGlobal.codificarDecodificarParametroUrl(this.ocorrencia.CodigoDiario, 'encode') + '#ocorrencias'
			});
		},
		async getAll() {
			await axios.post(base_url + 'DiarioAmbiental/getCombos')
				.then(response => {
					this.status = response.data.status;
					this.nivelRisco = response.data.risco;
					this.tipoOcorrencia = response.data.tipo;
				});
		},
		selectFase(fase) {
			this.faseSeleted = fase;
			this.isFaseSelected = true;
		},
		constroiTable(id = "") {
			$('table' + id, this.$el).DataTable({
				language: translateDataTable,
				lengthChange: false,
				destroy: true
			});
		},
		async refreshTable(id = "") {
			$('table' + id, this.$el).DataTable().destroy();
			await this.$nextTick(() => {
				this.constroiTable();
			});
		},
		newParecer(sitaucao) {
			switch (sitaucao) {
				case 'correcao':
					this.newParecerTecnico.CodigoSituacaoParecer = 3;
					break;
				case 'rejeitado':
					this.newParecerTecnico.CodigoSituacaoParecer = 2;
					this.newParecerTecnico.NovoPrazoSolucao = null;
					break;
				case 'aprovado':
					this.newParecerTecnico.NovoPrazoSolucao = null;
					this.newParecerTecnico.CodigoSituacaoParecer = 4;
					break;
			}

			if (sitaucao.includes('correcao') && (this.newParecerTecnico.NovoPrazoSolucao === null || this.newParecerTecnico.NovoPrazoSolucao.length == 0)) {
				this.showInputPrazoCorrecao = true;
				return
			}

			Swal.fire({
				title: 'Finalizar Parecer?',
				text: 'Após executada, essa ação não pode ser desfeita.',
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#04be5b',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				var parecer = Object.assign({}, this.newParecerTecnico);
				delete parecer.CodigoParecerTecnico;
				delete parecer.DataParecer;

				parecer.CodigoOcorrenciaFase = this.ocorrenciaFase.CodigoOcorrenciaFase;
				parecer.AnitgoPrazo = this.ocorrenciaFase.PrazoSolucao;
				parecer.Parecer = String(btoa(CKEDITOR.instances['txtPparecerTecnico'].getData()));

				var response = vmGlobal.insertFromAPI({
					table: 'parecerTecnicoOcorrencia',
					data: parecer
				});

				if (response) {
					vmGlobal.updateFromAPI({
						table: 'ocorrenciaDiarioAmbiental',
						where: {
							CodigoOcorrencia: this.CodigoOcorrencia
						},
						data: {
							EviadoValidacao: 0

						}
					});

					if (parecer.NovoPrazoSolucao !== null && parecer.NovoPrazoSolucao.length !== 0) {
						vmGlobal.updateFromAPI({
							table: 'ocorrenciaFase',
							where: {
								CodigoOcorrenciaFase: parecer.CodigoOcorrenciaFase
							},
							data: {
								PrazoSolucao: parecer.NovoPrazoSolucao

							}
						});
					}
				}

				setTimeout(() => {
					window.location.reload();
				}, 3000);
			})
		},
		loadParecer(val) {
			this.showParecer = true;

			this.parecer.DataParecer = this.formataData(val.DataParecer);
			this.parecer.TipoOcorrencia = val.TipoOcorrencia;

			if (CKEDITOR.instances['txtParecerTecnicoView']) CKEDITOR.instances['txtParecerTecnicoView'].destroy();
			this.$nextTick(() => {
				CKEDITOR.replace('txtParecerTecnicoView', {
					height: '400px',
					readOnly: true
				});
			});

			this.$nextTick(() => {
				CKEDITOR.instances['txtParecerTecnicoView'].setData(String(atob(val.Parecer)));
			});
		},
		messageInitial() {
			Swal.fire({
				type: 'warning',
				title: 'Atenção',
				text: 'Por favor, insira as fotos e arquivos e tambem preencha as informações gerais para finalizar o cadastro do diário.',
			})
		},
	},
	mounted() {
		$('#form-tab').click();
	}
}
