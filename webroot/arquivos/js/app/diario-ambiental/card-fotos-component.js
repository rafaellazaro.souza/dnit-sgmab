const CardFotos = {
	template: '#template-card-fotos',
	props: {
		foto: {
			type: Object,
			required: true
		},
		disabled: {
			type: Boolean,
			default: false
		},
		withOcorrencia: {
			type: Boolean,
			default: false
		}
	},
	data() {
		return {}
	},
	destroyed() {},
	methods: {
		isEmpty(campo) {
			return campo === null || campo.length == 0;
		},
		isEdit() {
			var edit = false;
			var keys = ['Lat', 'Long', 'CodigoDiario', 'CodigoOcorrencia', 'CodigoAcaoCorretiva', 'CodigoFotoSupervisao'];
			$.each(this.foto, (index, value) => {
				if (keys.includes(index)) return;
				if (value === null || value.length == 0) {
					edit = true;
					return false;
				}
			});

			return edit;
		},
		formatDataFoto(data) {
			if (data.length === null || data.length == 0) return '';

			var dt = data.match(/\d{4}\-\d{2}\-\d{2}/g);
			var dataFormatada = dt[0].split('-').reverse().join('/');

			return dataFormatada + data.replace(dt, '');
		},
		async salvarFoto() {
			var foto = Object.assign({}, this.foto);
			delete foto.CodigoFotoSupervisao;
			delete foto.CodigoOcorrencia;
			delete foto.TipoOcorrencia;
			// if (foto.hasOwnProperty('CodigoOcorrencia')) {}

			var response = await vmGlobal.updateFromAPI({
				data: foto,
				table: 'fotosDiarioAmbiental',
				where: {
					CodigoFotoSupervisao: this.foto.CodigoFotoSupervisao
				}
			});

			if (response) {
				setTimeout(() => {
					this.getFoto()
				}, 2000);
			}
		},
		async getFoto() {
			var foto = await vmGlobal.getFromAPI({
				table: 'fotosDiarioAmbiental',
				where: {
					CodigoFotoSupervisao: this.foto.CodigoFotoSupervisao
				}
			});

			this.foto = foto[0];
		},
		excluirFoto() {
			Swal.fire({
				title: 'Deseja remover a foto?',
				text: 'Está ação não pode ser desfeita.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				if (result.value) {
					var response = await vmGlobal.deleteFromAPI({
						table: 'fotosDiarioAmbiental',
						where: {
							CodigoFotoSupervisao: this.foto.CodigoFotoSupervisao,
						}
					});

					if (response) {
						await vmGlobal.deleteFile(this.foto.CaminhoFoto);
						this.$emit('excluir-foto');
					}
				}
			});

		},
	},
	mounted() {},
};
