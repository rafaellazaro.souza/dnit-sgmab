const AcaoCorretiva = {
	template: '#acao-corretiva-template',
	components: {
		CardFotos: CardFotos,
		TableArquivos: TableArquivos,
		ImportFiles: ImportFiles
	},
	data() {
		return {
			previousRoute: '',
			CodigoAcaoCorretiva: '',
			CodigoOcorrencia: '',
			status: [],
			arquivos: [],
			fotos: [],
			acao: {
				CodigoAcaoCorretiva: '',
				AcaoCorretiva: '',
				PrazoConclusao: '',
				DataFim: null,
				DescricaoAcao: '',
				SolucoesAdotadas: '',
				Indicadores: '',
				CodigoStatus: 1,
				DataInicio: '',
				CodigoOcorrenciaFase: ''
			},
			ocorrencia: {
				DataOcorrencia: '',
				PrazoSolucao: '',
				EviadoValidacao: 0,
				TipoOcorrencia: '',
				Status: ''
			},
			importFile: {
				codigoKey: 'CodigoAcaoCorretiva',
				codigoValue: ''
			},
		}
	},
	computed: {
		isEmptyAcao() {
			return this.CodigoAcaoCorretiva.length == 0;
		},
		isEmValidacao() {
			return this.ocorrencia.EviadoValidacao == 1;
		}
	},
	watch: {
		$route(to, from) {
			this.previousRoute = from.path;

			if (from.path.includes('AcaoCorretiva/nova/')) {
				this.messageInitial();
			}

			if (to.path.includes('AcaoCorretiva/update/')) {
				this.filtraURL();
				this.$forceUpdate();
			}
		},
		isEmValidacao(val) {
			if (val || vmGlobal.userPerfil.includes('Contratada')) {
				this.disableFormFields();
			}
		},
		'ocorrencia.Status'(val) {
			if (val.includes('Concluído')) {
				this.disableFormFields();
			}
		},
	},
	async created() {
		await this.filtraURL();
	},
	methods: {
		async filtraURL() {
			await this.getStatus();

			var url = this.$route.path;
			var param = this.$route.params.codigo;
			param = vmGlobal.codificarDecodificarParametroUrl(param, 'decode');

			if (url.includes('nova')) {
				this.CodigoOcorrencia = param;
				await this.getFaseOcorrencia();
				return;
			}

			this.CodigoAcaoCorretiva = param;
			this.arquivos.CodigoAcaoCorretiva = param;
			this.importFile.codigoValue = param;
			await this.getAcao();
			await this.getArquivosAndFotos();
			await this.getFaseOcorrencia();
		},
		async getAcao() {
			if (this.CodigoAcaoCorretiva.length == 0) return;
			showLoading(true);
			var acao = await vmGlobal.getFromAPI({
				table: 'ocorrenciaAcoes',
				where: {
					CodigoAcaoCorretiva: this.CodigoAcaoCorretiva
				}
			});

			if (acao.length != 0) {
				this.acao = acao[0];
			}
			showLoading();
		},
		async getFaseOcorrencia() {

			param = {};
			if (this.CodigoAcaoCorretiva.length == 0) {
				param = {
					CodigoOcorrencia: this.CodigoOcorrencia
				}
			} else {
				param = {
					CodigoOcorrenciaFase: this.acao.CodigoOcorrenciaFase,
					OrdenaPorUltimaFaseInserida: 0
				};
			}

			await axios.post(base_url + 'DiarioAmbiental/getOcorrenciasDiario', $.param(param)).then(response => {
				var ocorrencia = response.data.ocorrencias;
				this.ocorrencia.TipoOcorrencia = ocorrencia[0].TipoOcorrencia;
				this.ocorrencia.EviadoValidacao = ocorrencia[0].EviadoValidacao;
				this.ocorrencia.PrazoSolucao = ocorrencia[0].PrazoSolucao;
				this.acao.CodigoOcorrenciaFase = (this.acao.CodigoOcorrenciaFase.length == 0) ? ocorrencia[0].CodigoOcorrenciaFase : this.acao.CodigoOcorrenciaFase;
				this.CodigoOcorrencia = ocorrencia[0].CodigoOcorrencia;
				this.ocorrencia.Status = ocorrencia[0].Status;

				if (ocorrencia[0].OrdenaPorUltimaFaseInserida != 1) {
					this.disableFormFields();
				}
			});
		},
		salvarAcao() {
			if (this.isEmValidacao) return;

			Swal.fire({
				title: '',
				text: 'Deseja relamente salvar os dados?',
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#04be5b',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				if (result.value) {
					if (this.CodigoAcaoCorretiva.length == 0) {
						this.novaAcao();
						return;
					}

					this.updateAcao();
				}
			})
		},
		async novaAcao() {
			var acao = Object.assign({}, this.acao);
			delete acao.DataInicio;
			delete acao.CodigoAcaoCorretiva;
			delete acao.DataFim;

			var response = await vmGlobal.insertFromApiAndReturnId(true, {
				table: 'ocorrenciaAcoes',
				data: acao
			});

			if (response === false) return;


			this.$router.push({
				path: '/AcaoCorretiva/update/' + vmGlobal.codificarDecodificarParametroUrl(response, 'encode')
			});
		},
		async updateAcao() {
			var acao = Object.assign({}, this.acao);
			delete acao.CodigoAcaoCorretiva;
			delete acao.CodigoOcorrenciaFase;
			delete acao.DataInicio;

			if (acao.CodigoStatus == 4 || acao.CodigoStatus == 5) {
				acao.DataFim = moment().format('YYYY-MM-DD');
			}

			var response = await vmGlobal.updateFromAPI({
				table: 'ocorrenciaAcoes',
				data: acao,
				where: {
					CodigoAcaoCorretiva: this.CodigoAcaoCorretiva
				}
			});

			if (response) {
				setTimeout(() => {
					this.getAcao()
				}, 2000);
			}
		},
		cancelar() {
			this.$router.push({
				path: '/Ocorrencia/update/' + vmGlobal.codificarDecodificarParametroUrl(this.CodigoOcorrencia, 'encode') + '#acoes'
			});
		},
		async getArquivosAndFotos() {
			await axios.post(base_url + 'DiarioAmbiental/getFotosEdocumentos', $.param({
				CodigoAcaoCorretiva: this.CodigoAcaoCorretiva
			})).then(response => {
				this.fotos = response.data.fotos;
				this.arquivos = response.data.arquivos;
				this.$emit('refresh-table', this.arquivos);
			});
		},
		disableFormFields() {
			$('form :input', this.$el).attr('disabled', true);
			$('textarea', this.$el).attr('disabled', true);
			$('select', this.$el).attr('disabled', true);

			$('#tblListaDeArquivos tbody', this.$el).find(':input[type=button]').each(() => {
				$(this).attr('disabled', true).css({
					display: 'none',
				});
			});

			$('#cancelarAcaoCorretiva', this.$el).attr('disabled', false);
		},
		async getStatus() {
			var status = await vmGlobal.getFromAPI({
				table: 'ocorrenciaStatus',
			});

			if (status.length != 0) {
				var acaoStatus = ['Em Andamento', 'Concluído', 'Não Realizado', 'Cancelado'];
				this.status = status.filter(el => {
					return acaoStatus.includes(el.Status.replace(/\r?\n|\r/g, ""));
				});
			}
		},
		messageInitial() {
			Swal.fire({
				type: 'warning',
				title: 'Atenção',
				text: 'Por favor, insira as fotos e os  arquivos para finalizar o cadastro.',
			})
		},
	},
	mounted() {

	},
}
