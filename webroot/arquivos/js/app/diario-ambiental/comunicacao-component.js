const Comunicacao = {
	template: '#comunicacao-template',
	components: {
		CardFotos: CardFotos,
		TableArquivos: TableArquivos,
		ImportFiles: ImportFiles
	},
	data() {
		return {
			CodigoNotificacaoFiscal: '',
			CodigoOcorrencia: '',
			arquivos: [],
			fotos: [],
			importFile: {
				codigoKey: 'CodigoNotificacaoFiscal',
				codigoValue: ''
			},
			notificacao: {
				CodigoNotificacaoFiscal: '',
				DataNotificacao: '',
				Destinatarios: '',
				Mensagem: '',
				CodigoOcorrenciaFase: ''
			},
			ocorrencia: {
				EviadoValidacao: 0,
				TipoOcorrencia: '',
				PrazoSolucao: '',
				Status: ''
			}
		}
	},
	computed: {
		isEmptyNotificacao() {
			return this.CodigoNotificacaoFiscal.length == 0;
		},
		isEmValidacao() {
			return this.ocorrencia.EviadoValidacao == 1;
		}
	},
	watch: {
		$route(to, from) {
			this.previousRoute = from.path;

			if (from.path.includes('ComunicacaoConstrutora/nova/')) {
				this.messageInitial();
			}

			if (to.path.includes('ComunicacaoConstrutora/update/')) {
				this.filtraURL();
				this.$forceUpdate();
			}
		},
		isEmValidacao(val) {
			if (val && vmGlobal.userPerfil.includes('Fiscal')) {
				this.disableFormFields();
			}
		},
		'ocorrencia.Status'(val) {
			if (val.includes('Concluído')) {
				this.disableFormFields();
			}
		},
	},
	created() {
		this.filtraURL();
	},
	methods: {
		async filtraURL() {
			var url = this.$route.path;
			var param = this.$route.params.codigo;
			param = vmGlobal.codificarDecodificarParametroUrl(param, 'decode');

			if (url.includes('nova')) {
				this.CodigoOcorrencia = param;
				await this.getFaseOcorrencia();
				return;
			}

			this.CodigoNotificacaoFiscal = param;
			this.importFile.codigoValue = param;
			await this.getNotificacao();
			await this.getFaseOcorrencia();
			await this.getArquivosAndFotos();
		},
		async getNotificacao() {
			if (this.CodigoNotificacaoFiscal.length == 0) return;
			showLoading(true);
			var notificacao = await vmGlobal.getFromAPI({
				table: 'ocorrenciaNotificacao',
				where: {
					CodigoNotificacaoFiscal: this.CodigoNotificacaoFiscal
				}
			});

			if (notificacao.length != 0) {
				this.notificacao = notificacao[0];
			}
			showLoading();
		},
		async getFaseOcorrencia() {
			param = {};
			if (this.CodigoNotificacaoFiscal.length == 0) {
				param = {
					CodigoOcorrencia: this.CodigoOcorrencia
				}
			} else {
				param = {
					CodigoOcorrenciaFase: this.notificacao.CodigoOcorrenciaFase,
					OrdenaPorUltimaFaseInserida: 0
				};
			}

			await axios.post(base_url + 'DiarioAmbiental/getOcorrenciasDiario', $.param(param)).then(response => {
				var ocorrencia = response.data.ocorrencias;
				this.ocorrencia.TipoOcorrencia = ocorrencia[0].TipoOcorrencia;
				this.ocorrencia.EviadoValidacao = ocorrencia[0].EviadoValidacao;
				this.ocorrencia.PrazoSolucao = ocorrencia[0].PrazoSolucao;
				this.notificacao.CodigoOcorrenciaFase = ocorrencia[0].CodigoOcorrenciaFase;
				this.CodigoOcorrencia = ocorrencia[0].CodigoOcorrencia;
				this.ocorrencia.Status = ocorrencia[0].Status;

				if (ocorrencia[0].OrdenaPorUltimaFaseInserida != 1) {
					this.disableFormFields();
				}
			});
		},
		salvarNotificacao() {
			if (this.isEmValidacao) return;

			Swal.fire({
				title: '',
				text: 'Deseja relamente salvar os dados?',
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#04be5b',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				if (result.value) {
					if (this.CodigoNotificacaoFiscal.length == 0) {
						this.novaNotificacao();
						return;
					}

					this.updateNotificacao();
				}
			})
		},
		async novaNotificacao() {
			var notificacao = Object.assign({}, this.notificacao);
			delete notificacao.CodigoNotificacaoFiscal;
			delete notificacao.DataNotificacao;

			var response = await vmGlobal.insertFromApiAndReturnId(true, {
				table: 'ocorrenciaNotificacao',
				data: notificacao
			});

			if (response === false) return;

			this.$router.push({
				path: '/ComunicacaoConstrutora/update/' + vmGlobal.codificarDecodificarParametroUrl(response, 'encode')
			});
		},
		async updateNotificacao() {
			var notificacao = Object.assign({}, this.notificacao);
			delete notificacao.CodigoNotificacaoFiscal;
			delete notificacao.CodigoOcorrenciaFase;

			var response = await vmGlobal.updateFromAPI({
				table: 'ocorrenciaNotificacao',
				data: notificacao,
				where: {
					CodigoNotificacaoFiscal: this.CodigoNotificacaoFiscal
				}
			});

			if (response) {
				await this.getNotificacao()
			}
		},
		cancelar() {
			this.$router.push({
				path: '/Ocorrencia/update/' + vmGlobal.codificarDecodificarParametroUrl(this.CodigoOcorrencia, 'encode') + '#comunicacao'
			});
		},
		disableFormFields() {
			$('form :input', this.$el).attr('disabled', true);
			$('textarea', this.$el).attr('disabled', true);
			$('select', this.$el).attr('disabled', true);

			$('#tblListaDeArquivos tbody', this.$el).find(':input[type=button]').each(() => {
				$(this).attr('disabled', true).css({
					display: 'none',
				});
			});

			$('#cancelarNotificacao', this.$el).attr('disabled', false);
		},
		async getArquivosAndFotos() {
			await axios.post(base_url + 'DiarioAmbiental/getFotosEdocumentos', $.param({
				CodigoNotificacaoFiscal: this.CodigoNotificacaoFiscal
			})).then(response => {
				this.fotos = response.data.fotos;
				this.arquivos = response.data.arquivos;
				this.$emit('refresh-table', this.arquivos);
			});
		},
		messageInitial() {
			Swal.fire({
				type: 'warning',
				title: 'Atenção',
				text: 'Por favor, insira as fotos e os  arquivos para finalizar o cadastro.',
			})
		},
	},
}
