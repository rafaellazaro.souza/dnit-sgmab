const TableArquivos = {
	template: '#template-table-arquivos',
	props: {
		arquivos: {
			type: Array,
			required: true
		},
		disabled: {
			type: Boolean,
			default: false
		},
		withOcorrencia: {
			type: Boolean,
			default: false
		}
	},
	data() {
		return {}
	},
	watch: {
		'arquivos'(val) {
			this.refreshTable();
		}
	},
	created() {},
	methods: {
		constroiTable() {
			$('table', this.$el).DataTable({
				language: translateDataTable,
				pageLength: 5,
				lengthChange: false,
				destroy: true
			});
		},
		async refreshTable() {
			$('table', this.$el).DataTable().destroy();
			await this.$nextTick(() => {
				this.constroiTable();
			});
		},
		excluirArquivo(file) {
			Swal.fire({
				title: 'Deseja remover o Arquivo?',
				text: 'Está ação não pode ser desfeita.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				if (result.value) {
					var response = await vmGlobal.deleteFromAPI({
						table: 'arquivosDiarioAmbiental',
						where: {
							CodigoDocumentosSupervisao: file.CodigoDocumentosSupervisao,
						}
					});

					if (response) {
						await vmGlobal.deleteFile(file.Arquivo);

						this.$emit('buscar-arquivos');
					}
				}
			});

		},
		formatDataArquivo(data) {
			if (data.length === null || data.length == 0) return '';

			var dt = data.match(/\d{4}\-\d{2}\-\d{2}/g);
			var dataFormatada = dt[0].split('-').reverse().join('/');

			return dataFormatada + data.replace(dt, '');
		},
	},
	mounted() {
		this.constroiTable();
	},
}
