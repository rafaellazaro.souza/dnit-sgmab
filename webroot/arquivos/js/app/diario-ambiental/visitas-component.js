const Visitas = {
	template: '#visitas-template',
	components: {
		CardFotos: CardFotos,
		TableArquivos: TableArquivos,
		ImportFiles: ImportFiles
	},
	data() {
		return {
			previousRoute: '',
			CodigoVisitas: '',
			CodigoOcorrencia: '',
			arquivos: [],
			fotos: [],
			visita: {
				CodigoVisitas: '',
				Assunto: '',
				SolucoesAdotadas: '',
				Observacoes: '',
				Participantes: '',
				DataVisita: '',
				CodigoOcorrenciaFase: ''
			},
			ocorrencia: {
				EviadoValidacao: 0,
				TipoOcorrencia: '',
				Status: ''
			},
			importFile: {
				codigoKey: 'CodigoVisitas',
				codigoValue: ''
			}
		}
	},
	computed: {
		isEmptyVisita() {
			return this.CodigoVisitas.length == 0;
		},
		isEmValidacao() {
			return this.ocorrencia.EviadoValidacao == 1;
		}
	},
	watch: {
		$route(to, from) {
			this.previousRoute = from.path;

			if (from.path.includes('HistoricoVisita/nova/')) {
				this.messageInitial();
			}

			if (to.path.includes('HistoricoVisita/update/')) {
				this.filtraURL();
				this.$forceUpdate();
			}
		},
		isEmValidacao(val) {
			if (val || vmGlobal.userPerfil.includes('Fiscal')) {
				this.disableFormFields();
			}
		},
		'ocorrencia.Status'(val) {
			if (val.replace(/(\r\n|\n|\r)/g, "").includes('Concluído')) {
				this.disableFormFields();
			}
		},
	},
	created() {
		this.filtraURL();
	},
	methods: {
		async filtraURL() {
			var url = this.$route.path;
			var param = this.$route.params.codigo;
			param = vmGlobal.codificarDecodificarParametroUrl(param, 'decode');

			if (url.includes('nova')) {
				this.CodigoOcorrencia = param;
				await this.getFaseOcorrencia();
				return;
			}

			this.CodigoVisitas = param;
			this.importFile.codigoValue = param;
			await this.getVisita();
			await this.getFaseOcorrencia();
			await this.getArquivosAndFotos();
		},
		async getVisita() {
			if (this.CodigoVisitas.length == 0) return;
			showLoading(true);
			var visita = await vmGlobal.getFromAPI({
				table: 'ocorrenciaVisitas',
				where: {
					CodigoVisitas: this.CodigoVisitas
				}
			});

			if (visita.length != 0) {
				this.visita = visita[0];
				if (this.visita.DataVisita) {
					this.visita.DataVisita = moment(this.visita.DataVisita).format('YYYY-MM-DD');
				}
			}
			showLoading();
		},
		async getFaseOcorrencia() {
			param = {};
			if (this.CodigoVisitas.length == 0) {
				param = {
					CodigoOcorrencia: this.CodigoOcorrencia
				}
			} else {
				param = {
					CodigoOcorrenciaFase: this.visita.CodigoOcorrenciaFase,
					OrdenaPorUltimaFaseInserida: 0
				};
			}

			await axios.post(base_url + 'DiarioAmbiental/getOcorrenciasDiario', $.param(param)).then(response => {
				var ocorrencia = response.data.ocorrencias;
				this.ocorrencia.TipoOcorrencia = ocorrencia[0].TipoOcorrencia;
				this.ocorrencia.EviadoValidacao = ocorrencia[0].EviadoValidacao;
				this.visita.CodigoOcorrenciaFase = ocorrencia[0].CodigoOcorrenciaFase;
				this.CodigoOcorrencia = ocorrencia[0].CodigoOcorrencia;
				this.ocorrencia.Status = ocorrencia[0].Status;

				if (ocorrencia[0].OrdenaPorUltimaFaseInserida != 1) {
					this.disableFormFields();
				}
			});
		},
		salvarVisita() {
			if (this.isEmValidacao) return;
			Swal.fire({
				title: '',
				text: 'Deseja relamente salvar os dados?',
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#04be5b',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Salvar',
				cancelButtonText: 'Cancelar',
			}).then(async (result) => {
				if (result.value) {
					if (this.CodigoVisitas.length == 0) {
						this.novaVisita();
						return;
					}

					this.updateVisita();
				}
			})
		},
		async novaVisita() {
			var visita = Object.assign({}, this.visita);
			delete visita.DataVisita;
			delete visita.CodigoVisitas;

			var response = await vmGlobal.insertFromApiAndReturnId(true, {
				table: 'ocorrenciaVisitas',
				data: visita
			});

			if (response === false) return;

			this.$router.push({
				path: '/HistoricoVisita/update/' + vmGlobal.codificarDecodificarParametroUrl(response, 'encode')
			});
		},
		async updateVisita() {
			var visita = Object.assign({}, this.visita);
			delete visita.CodigoVisitas;
			delete visita.DataVisita;
			delete visita.CodigoOcorrenciaFase;

			var response = await vmGlobal.updateFromAPI({
				table: 'ocorrenciaVisitas',
				data: visita,
				where: {
					CodigoVisitas: this.CodigoVisitas
				}
			});

			if (response) {
				setTimeout(() => {
					this.getVisita();
				}, 2000);
			}
		},
		disableFormFields() {
			$('form :input', this.$el).attr('disabled', true);
			$('textarea', this.$el).attr('disabled', true);
			$('select', this.$el).attr('disabled', true);

			$('#tblListaDeArquivos tbody', this.$el).find(':input[type=button]').each(() => {
				$(this).attr('disabled', true).css({
					display: 'none',
				});
			});

			$('#cancelButtonVisita', this.$el).attr('disabled', false);
		},
		cancelar() {
			this.$router.push({
				path: '/Ocorrencia/update/' + vmGlobal.codificarDecodificarParametroUrl(this.CodigoOcorrencia, 'encode') + '#visitas'
			});
		},
		async getArquivosAndFotos() {
			await axios.post(base_url + 'DiarioAmbiental/getFotosEdocumentos', $.param({
				CodigoVisitas: this.CodigoVisitas
			})).then(response => {
				this.fotos = response.data.fotos;
				this.arquivos = response.data.arquivos;
				this.$emit('refresh-table', this.arquivos);
			});
		},
		messageInitial() {
			Swal.fire({
				type: 'warning',
				title: 'Atenção',
				text: 'Por favor, insira as fotos e os  arquivos para finalizar o cadastro.',
			})
		},

	},
}
