vmGlobal.getPerfil();

const routes = [{
	path: '/DiarioAmbiental/diarioAmbientalLista/:codigoEscopo',
	component: DiarioCompenent,
	beforeEnter: (to, from, next) => {
		if (to.params.codigoEscopo.length == 0) {
			next(false);
			return;
		}
		next();
	},
	meta: {
		breadcrumbs: []
	}
}, {
	path: '/DiarioAmbiental/novo/:codigo',
	component: DiarioFormComponent,
	beforeEnter: (to, from, next) => {
		if (to.params.codigo.length == 0) {
			next(false);
			return;
		}
		next();
	},
	meta: {
		breadcrumbs: [{
			name: 'Novo Registro',
			link: '',
			active: true
		}]
	}
}, {
	path: '/DiarioAmbiental/update/:codigo',
	component: DiarioFormComponent,
	beforeEnter: (to, from, next) => {
		if (to.params.codigo.length == 0) {
			next(false);
			return;
		}
		next(next(vm => {
			vm.prevRoute = from;
		}));
	},
	meta: {
		breadcrumbs: [{
			name: 'Atualizar Diário',
			link: '',
			active: true
		}]
	}
}, {
	path: '/Ocorrencia/nova/:codigo',
	component: Ocorrencia,
	beforeEnter: (to, from, next) => {
		if (to.params.codigo.length == 0) {
			next(false);
			return;
		}
		next();
	},
	meta: {
		breadcrumbs: [{
				name: 'Atualizar Diário',
				link: '',
				active: false
			},
			{
				name: 'Nova Ocorrêcia',
				link: '',
				active: true
			},
		]
	}
}, {
	path: '/Ocorrencia/update/:codigo',
	component: Ocorrencia,
	beforeEnter: (to, from, next) => {
		if (to.params.codigo.length == 0) {
			next(false);
			return;
		}
		next();
	},
	meta: {
		breadcrumbs: [{
				name: 'Atualizar Diário',
				link: '',
				active: false
			},
			{
				name: 'Atualizar Ocorrêcia',
				link: '',
				active: true
			},
		]
	}
}, {
	path: '/AcaoCorretiva/update/:codigo',
	component: AcaoCorretiva,
	beforeEnter: (to, from, next) => {
		if (to.params.codigo.length == 0) {
			next(false);
			return;
		}
		next();
	},
	meta: {
		breadcrumbs: [{
				name: 'Atualizar Diário',
				link: '',
				active: false
			},
			{
				name: 'Atualizar Ocorrêcia',
				link: '',
				active: false
			},
			{
				name: 'Atualizar Ação Corretiva',
				link: '',
				active: true
			}
		]
	}
}, {
	path: '/AcaoCorretiva/nova/:codigo',
	component: AcaoCorretiva,
	beforeEnter: (to, from, next) => {
		if (to.params.codigo.length == 0) {
			next(false);
			return;
		}
		next();
	},
	meta: {
		breadcrumbs: [{
				name: 'Atualizar Diário',
				link: '',
				active: false
			},
			{
				name: 'Atualizar Ocorrêcia',
				link: '',
				active: false
			},
			{
				name: 'Nova Ação Corretiva',
				link: '',
				active: true
			}
		]
	}
}, {
	path: '/HistoricoVisita/nova/:codigo',
	component: Visitas,
	beforeEnter: (to, from, next) => {
		if (to.params.codigo.length == 0) {
			next(false);
			return;
		}
		next();
	},
	meta: {
		breadcrumbs: [{
				name: 'Atualizar Diário',
				link: '',
				active: false
			},
			{
				name: 'Atualizar Ocorrêcia',
				link: '',
				active: false
			},
			{
				name: 'Nova Visita',
				link: '',
				active: true
			}
		]
	}
}, {
	path: '/HistoricoVisita/update/:codigo',
	component: Visitas,
	beforeEnter: (to, from, next) => {
		if (to.params.codigo.length == 0) {
			next(false);
			return;
		}
		next();
	},
	meta: {
		breadcrumbs: [{
				name: 'Atualizar Diário',
				link: '',
				active: false
			},
			{
				name: 'Atualizar Ocorrêcia',
				link: '',
				active: false
			},
			{
				name: 'Atualizar Visita',
				link: '',
				active: true
			}
		]
	}
}, {
	path: '/ComunicacaoConstrutora/nova/:codigo',
	component: Comunicacao,
	beforeEnter: (to, from, next) => {
		if (to.params.codigo.length == 0) {
			next(false);
			return;
		}
		next();
	},
	meta: {
		breadcrumbs: [{
				name: 'Atualizar Diário',
				link: '',
				active: false
			},
			{
				name: 'Atualizar Ocorrêcia',
				link: '',
				active: false
			},
			{
				name: 'Nova Notificação',
				link: '',
				active: true
			}
		]
	}
}, {
	path: '/ComunicacaoConstrutora/update/:codigo',
	component: Comunicacao,
	beforeEnter: (to, from, next) => {
		if (to.params.codigo.length == 0) {
			next(false);
			return;
		}
		next();
	},
	meta: {
		breadcrumbs: [{
				name: 'Atualizar Diário',
				link: '',
				active: false
			},
			{
				name: 'Atualizar Ocorrêcia',
				link: '',
				active: false
			},
			{
				name: 'Atualizar Notificação',
				link: '',
				active: true
			}
		]
	}
}, ];

const router = new VueRouter({
	hashbang: false,
	base: '/sgmab/',
	mode: 'history',
	routes
});

const diario = new Vue({
	router,
	data() {
		return {
			breadcrumbs: []
		}
	},
	watch: {
		'$route'() {
			this.initRoute();
		}
	},
	methods: {
		initRoute() {
			this.breadcrumbs = this.$route.meta.breadcrumbs;
		}
	},
	mounted() {
		this.initRoute();
	},
}).$mount('#diario-content-app');
