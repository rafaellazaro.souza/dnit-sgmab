    vmMenus = new Vue({
        el: '#menus-acesso',
        data: {
            listaMenu: [],
            listaSubmenu:[],
            listaSubmenuFiltro: []
        },
        async mounted(){
            await this.getSubmenus();
            // await this.getMenus();
        },
        methods:{
            async getMenus(){
               await this.buscarMenusSubmenus('m');

            },
            async getSubmenus(){
                await this.buscarMenusSubmenus('sm');
            },
            async buscarMenusSubmenus(tipo){
                 await axios.post(base_url+'Home/menu', $.param({Tipo: tipo}))
                 .then((resp)=>{
                    if(tipo === 'm'){
                        this.listaMenu = resp.data;
                    }else if(tipo === 'sm'){
                        this.listaSubmenu = resp.data;
                    }
                 });
            },
            filtrarSubmenu(CodigoMenu){
                this.listaSubmenuFiltro = this.listaSubmenu.map(x => x.NomeArea).filter(function(elem, index, self) {
                    return index === self.indexOf(elem);
                });
            }
        }
    });
